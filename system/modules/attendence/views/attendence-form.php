<?php $segment		=	$this->uri->segment(1);?>
<?php $text			=	$this->lang->line($segment);?>
<?php $labels		=	$text['users']['form'];?>
<?php $absent		=	$this->config->item('absent_types');?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<style>
.btn-default .caret {
	border-top-color: #000 !important;
}
.btn {
	text-shadow: none !important;
}
.btn-default {
	color: #000 !important;
}
.btn-default {
	background-color: #FFF !important;
	border: 1px solid #029625 !important;
}
.dropdown-menu>.active>a, .dropdown-menu>.active>a:hover, .dropdown-menu>.active>a:focus 
{
	color: #000;
	background-color: #029625 !important;
}
.select-box 
{
	max-height: 100px !important;
	min-height: 28px !important;
}
</style>

<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <form action="<?php echo base_url();?>attendence/add_attendence" method="POST" id="attendence_form" name="attendence_form" enctype="multipart/form-data" >
          <div class="col-md-12">
            <div class="panel panel-default panel-block">
              <div class="list-group">
                <div class="list-group-item" id="input-fields">
                  <h4>نظام حضوره</h4>
                  <div class="col-md-4 form-group">
                    <label class="text-warning">تاريخ</label>
                    <input type="text" class="datepicker form-control required" name="attendence_date"  id="attendence_date"  placeholder="تاريخ" value=""/>
                  </div>
                  <?php if(!empty($all_users)):?>
                  <div class="form-group col-md-4">
                    <label class="text-warning">جميع المستخدمين</label>
                    <select name="userid" id="userid" class="selectpicker form-control required" data-live-search="true" placeholder="جميع المستخدمين" style="display: none;">
                      <option value="">جميع المستخدمين</option>
                      <?php foreach($all_users as $user):?>
                      <option value="<?php echo $user->userid;?>"><?php echo $user->fullname;?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                  <?php endif;?>
                  <div class="form-group  col-md-4">
                    <label class="text-warning">حالة</label>
                    <select name="attendence_status" id="attendence_status" class="form-control required" placeholder="حالة">
                      <option value="">حالة</option>
                      <option value="P" >حاضر</option>
                      <option value="A">غائب</option>
                    </select>
                  </div>
                  <div class="form-group  col-md-4" id="on_time" style="display:none;">
                    <label class="text-warning">في الوقت المحدد</label>
                    <select name="late" id="late" class="form-control">
                      <option value="">اختار</option>
                      <option value="1" >متأخر</option>
                    </select>
                  </div>
                  <div class="form-group col-md-4" id="reason_status" style="display:none;">
                    <label class="text-warning">التفاصيل غائبة</label>
                    <br>
                    <input type="radio"  value="1" name="reason" id="reason_yes" />
                    سبب
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio"  value="0" name="reason" id="reason_no" />
                    بدون سبب </div>
                  <div class="form-group col-md-4" id="reason_types" style="display:none;">
                    <label class="text-warning">أنواع السبب</label>
                    <select name="absent_type" id="absent_type" class="form-control" placeholder="أنواع السبب">
                      <option value="">أنواع السبب</option>
                      <?php foreach($this->haya_model->get_leave_types() as $value):?>
                      <option value="<?php echo $value;?>"><?php echo $value;?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                  <div class="form-group col-md-4" id="document" style="display:none;">
                    <label class="text-warning">وثيقة</label>
                    <input style="border: 0px !important; color: #d09c0d;" type="file" name="document" title='وثيقة'>
                  </div>
                  <div class="form-group col-md-8">
                    <label class="text-warning">ملاحظات</label>
                    <textarea class="form-control" name="reason_text" id="reason_text" style="margin: 0px; height: 177px; width: 763px;"></textarea>
                  </div>
                  <br clear="all" />
                  <input type="button" id="attendence_data_today" class="btn btn-success btn-lrg" name="attendence_data"  value="حفظ" />
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<!-- /.modal-dialog --> 
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		/*yearRange: "-80:+0",*/
		yearRange: new Date().getFullYear() + ':' + new Date().getFullYear(),
		dateFormat:'yy-mm-dd',
		});
		
	$('#attendence_status').change(function(){
		if($('#attendence_status').val() == 'A')
		{
		   $("#reason_status").fadeIn();
		   $("#document").fadeIn();
		   
		   $("#on_time").fadeOut();
		}
		else
		{
			$("#reason_status").fadeOut();
			$("#document").fadeOut();
			 
			$("#on_time").fadeIn();
		}

	});
	
	$('#reason_yes').click(function()
	{
		 $("#reason_types").fadeIn();
	});
	$('#reason_no').click(function()
	{
		 $("#reason_types").fadeOut();
	});
});
</script>
</div>
</body>
</html>
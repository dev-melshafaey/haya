<?php $segment	=	$this->uri->segment(1);?>
<?php $text		=	$this->lang->line($segment);?>
<?php $labels	=	$text['users']['form'];?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $permissions	=	$this->haya_model->check_other_permission(array('131'));?>
<?php //echo '<pre>'; print_r($permissions);?>
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="col-md-12">
          <ul class="nav nav-tabs panel panel-default panel-block">
            <li class="tabsdemo-1 active"><a href="#tabsdemo-1" data-toggle="tab">يوميا</a></li>
            <li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
            <li class="tabsdemo-2"><a href="#tabsdemo-2" data-toggle="tab">شهريا</a></li>
            <li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
            <li class="tabsdemo-3"><a href="#tabsdemo-3" data-toggle="tab">سنويا</a></li>
          </ul>
        </div>
        <div class="col-md-12">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
            
            <?php if($permissions[131]['a']	==	'1'):?>
            	<div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 27px;" href="<?php echo base_url();?>attendence" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>             
            <?php endif;?>
              <!-- -->
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;">اسم</th>
                      <th style="text-align:center;">حالة المستخدم</th>
                      <th style="text-align:center;">سبب</th>
                      <th style="text-align:center;">متأخر</th>
                      <th style="text-align:center;">الإجراءات</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $date	=	date('Y-m-d')?>
                    <?php $attendence_listing	=	$this->attendence->get_attendence_by_day($date);?>
                    
                    <?php //echo '<pre>'; print_r($attendence_listing);?>
                    <?php if(!empty($attendence_listing)):?>
                    <?php foreach($attendence_listing as $listing):?>
                    <?php
						if($permissions[131]['d']	==1)
						{
							$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$listing->attendence_id.'" data-url="'.base_url().'attendence/delete_attendence/'.$listing->attendence_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
						}
						if($permissions[131]['u']	==1)
						{
							//$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'attendence/'.$listing->userid.'/'.$listing->attendence_id.'" id="'.$listing->attendence_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
						}
					?>
                    <tr role="row" id="<?php echo $listing->userid;?>_durar_lm">
                      <td  style="text-align:center;"><?php echo $listing->fullname;?></td>
                      <td  style="text-align:center;"><?php echo ($listing->attendence_status	==	'P' ? 'الحالي' : 'غائب');?></td>
                      <td  style="text-align:center;"><?php echo $listing->absent_type;?></td>
                      <td  style="text-align:center;"><?php echo ($listing->late	==	'1' ? '' : 'متأخر');?></td>
                      <td><?php echo $actions;?></td>
                    </tr>
                    <?php unset($actions); endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-2">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;">اسم</th>
                      <th style="text-align:center;">الحالي</th>
                      <th style="text-align:center;">غائب</th>
                      <!--<th style="text-align:center;">سبب</th>-->
                      <th style="text-align:center;">سبب</th>
                      <th style="text-align:center;">متأخر</th>
                      <th style="text-align:center;">التفاصيل</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $month	=	date('m')?>
                    <?php $attendence_listing	=	$this->attendence->get_attendence_by_month($month);?>
                    <?php if(!empty($attendence_listing)):?>
                    <?php foreach($attendence_listing as $listing):?>
                    <?php 
						if($permissions[131]['v']	==1)
						{
							$actions 	 =  '<a  onclick="alatadad(this);" data-url="'.base_url().'attendence/get_detail/'.$listing->userid.'/month"  href="#"><i class="icon-eye-open"></i></a>';
						}
					?>
                    <tr role="row" id="<?php echo $listing->userid;?>_durar_lm">
                      <td  style="text-align:center;"><?php echo $listing->fullname;?></td>
                      <td  style="text-align:center;"><?php echo ($listing->present);?></td>
                      <td  style="text-align:center;"><?php echo ($listing->absent);?></td>
                      <!--<td  style="text-align:center;"><?php echo $listing->absent_type;?></td>-->
                      <td  style="text-align:center;"><?php echo $listing->reason;?></td>
                      <td  style="text-align:center;"><?php echo ($listing->late);?></td>
                       <td><?php echo $actions;?></td>
                    </tr>
                    <?php endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-3">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;">اسم</th>
                      <th style="text-align:center;">الحالي</th>
                      <th style="text-align:center;">غائب</th>
                      <!--<th style="text-align:center;">سبب</th>-->
                      <th style="text-align:center;">سبب</th>
                      <th style="text-align:center;">متأخر</th>
                      <th style="text-align:center;">التفاصيل</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $year	=	date('Y');?>
                    <?php $attendence_listing	=	$this->attendence->get_attendence_by_year($year);?>
                    <?php if(!empty($attendence_listing)):?>
                    <?php foreach($attendence_listing as $listing):?>
                    <?php 
						if($permissions[131]['v']	==	1)
						{
							$actions 	 =  '<a  onclick="alatadad(this);" data-url="'.base_url().'attendence/get_detail/'.$listing->userid.'"  href="#"><i class="icon-eye-open"></i></a>';
						}
					?>
                    <tr role="row" id="<?php echo $listing->userid;?>_durar_lm">
                      <td  style="text-align:center;"><?php echo $listing->fullname;?></td>
                      <td  style="text-align:center;"><?php echo ($listing->present);?></td>
                      <td  style="text-align:center;"><?php echo ($listing->absent);?></td>
                      <!--<td  style="text-align:center;"><?php echo $listing->absent_type;?></td>-->
                      <td  style="text-align:center;"><?php echo $listing->reason;?></td>
                      <td  style="text-align:center;"><?php echo ($listing->late);?></td>
                      <td><?php echo $actions;?></td>
                    </tr>
                    <?php endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- ---------------------------------------------------------------------- --> 
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<!-- /.modal-dialog --> 
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
		});
	});
$('.nav-tabs a[href="#tabsdemo-<?php echo $table_id;?>"]').tab('show');
</script>
</div>
</body>
</html>
<?php $yearly_vacations	=	$this->haya_model->get_vacations();?>
<?php $total_leaves		=	($yearly_vacations['0']->settingvalue + $yearly_vacations['1']->settingvalue + $yearly_vacations['2']->settingvalue + $yearly_vacations['3']->settingvalue);?>
<?php $user_detail		=	$this->haya_model->get_userattendence_by_year(date('Y'),$userid);?>

<?php
if($month)
{ 
	$user_detail		=	$this->haya_model->get_userattendence_by_year(date('Y'),$userid);
}
else
{
	$user_detail		=	$this->haya_model->get_userattendence_by_month(date('m'),$userid);
}
?>

<div id="printable">
  <div class="row" style="direction:rtl !important;">
    <div class="col-md-12 setMarginx">
      <div class="list-group-item ">
        <h3 class="section-title" align="center">التفریغ النھائی لغیاب و حضور موظفی الھیئتہ العمانیہ للاعمال الخیریتہ لعام</h3>
		<?php if(!isset($month)): ?> 
           <div class="form-group">
              <h4>جميع الاجازات</h4>
              <table class="table table-bordered table-striped dataTable no-footer">
                <thead>
                  <tr>
                    <th class="reporthead">إجازة سنوية</th>
                    <th class="reporthead">إجازة الشهري</th>
                    <th class="reporthead">أجازة مرضية</th>
                    <th class="reporthead">أيام عطل</th>
                    <th class="reporthead"> مجموع الأوراق</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo $yearly_vacations['0']->settingvalue;?></td>
                    <td><?php echo $yearly_vacations['1']->settingvalue;?></td>
                    <td><?php echo $yearly_vacations['2']->settingvalue;?></td>
                    <td><?php echo $yearly_vacations['3']->settingvalue;?></td>
                    <td><?php echo $total_leaves;?></td>
                  </tr>
                </tbody>
              </table>
            </div>
        <?php endif;?>
        <div class="form-group">
          <table class="table table-bordered table-striped dataTable no-footer">
            <thead>
              <tr>
                <th class="reporthead">الاسم الكامل</th>
                <th class="reporthead">الاجازة الاعتيادية</th>
                <th class="reporthead">الاجازة الاضطراية</th>
                <th class="reporthead">الاجازة المرضية</th>
                <th class="reporthead">أخرى بعزر</th>
                <th class="reporthead">بدون عزر</th>
                <th class="reporthead">مهمة عمل</th>
                <th class="reporthead">جميع الأوراق جدوى</th>
                <th class="reporthead">مجموع الأوراق</th>
                <th class="reporthead">المتبقية</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><?php echo $user_detail->fullname;?></td>
                <td><?php echo $user_detail->regular_holiday;?></td>
                <td><?php echo $user_detail->leave_aladtaraah;?></td>
                <td><?php echo $user_detail->sick;?></td>
                <td><?php echo $user_detail->excuse;?></td>
                <td><?php echo $user_detail->no_excuse;?></td>
                <td><?php echo $user_detail->job_assignment;?></td>
                <td><?php echo $avail_leaves	=	($user_detail->no_excuse + $user_detail->excuse + $user_detail->sick + $user_detail->leave_aladtaraah + $user_detail->regular_holiday + $user_detail->job_assignment);?></td>
                <td><?php echo $total_leaves;?></td>
                <td><?php echo ($total_leaves - $avail_leaves);?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row" style="text-align:center !important;">
  <button type="button" id="" onClick="printthepage('printable');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
</div>
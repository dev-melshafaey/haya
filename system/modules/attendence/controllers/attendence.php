<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendence extends CI_Controller {

	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;

//-----------------------------------------------------------------------

	/*
	* Constructor
	*/
	function __construct()
	{
		 parent::__construct();	
	
		// Loade Admin Model
		$this->load->model('attendence_model','attendence');
		
		$this->_data['module']			=	$this->haya_model->get_module();	
		
		// SET Login USER ID
		$this->_login_userid			=	$this->session->userdata('userid');
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);	
		
		$this->_data['login_userid']	=	$this->_login_userid;
	}
//-----------------------------------------------------------------------

	/*
	*	Home Page
	*/
	public function index()
	{
		$this->_data['all_users']		=	$this->haya_model->get_all_users();
		$this->_data['absent_types']	=	$this->haya_model->absent_types();
		
		// Load Users Listing Page
		$this->load->view('attendence-form',$this->_data);
	}

//-----------------------------------------------------------------------

	/*
	*
	* All Banks Listing
	*
	*/	
	public function attendences_listing($userid	=	NULL)
	{	
		// Load Users Listing Page
		$this->load->view('users-attendence-listing',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*	Add / Update Salary
	*	@param $salaryid integer
	*/
	public function add_attendence($userid	=	NULL,$attendence_id	=	NULL)
	{
		$this->_data['userid']	=	$userid;
		
		if($this->input->post())
		{
			// Get all values from POST
			$data	=	$this->input->post();
			
			// UNSET the value from ARRAY
			unset($data['attendence_data']);
			unset($data['absent_type_text']);
			
			if($data['attendence_id'])
			{
				if($_FILES["document"]["tmp_name"])
				{
					$profilepic			=	$this->upload_file($data['userid'],'document','resources/attendence/users');
					$data['document']	=	$profilepic;
				}
				
				// Update data into database
				$this->attendence->update_attendence($data['attendence_id'],$data);
			}
			else
			{
				if($_FILES["document"]["tmp_name"])
				{
					$profilepic			=	$this->upload_file($data['userid'],'document','resources/attendence/users');
					$data['document']	=	$profilepic;
				}
			
				// ADD data into database
				$this->attendence->add_attendence($data);
			}
			
			// Redirect to listing page
			redirect(base_url().'attendence');
			exit();
		}
		else
		{
			$this->_data['all_users']		=	$this->haya_model->get_all_users();
			$this->_data['absent_types']	=	$this->haya_model->absent_types();
			
			
			$this->_data['user_attendence_detail']	=	$this->attendence->get_user_attend_detail($attendence_id);	
			
			// Load Users Listing Page
			$this->load->view('attendence-form',$this->_data);
		}
	}
//-----------------------------------------------------------------------

	/*
	*
	* User Detail
	* @param @userid integer
	*
	*/	
	public function get_detail($userid	=	NULL,$month	=	NULL)
	{	
		$this->_data['userid']	=	$userid;
		$this->_data['month']	=	$month;
		
		// Load Users Listing Page
		$this->load->view('user-detail',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*	File Uploading
	*	@param $userid integer
	*	@param $filefield integer Input File name
	*	@param $folder integer older name where Image Upload
	*	@param $width integer
	*	@param $height integer
	*/
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
		
			
		if (!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
//-----------------------------------------------------------------------

	/*
	*	Delete Branch
	*/	
	public function delete_attendence($attendence_id)
	{
		$this->users->delete_attendence($attendence_id);
		
		redirect(base_url().'attendence');
		exit();
	}
//-----------------------------------------------------------------------

	/*
	*
	*
	*/
	public function remodified_data($data)
	{
		if(!empty($data))
		{
			$redifindArray	=	array();
			
			foreach($data	as $key	=>	$value)
			{
				$redifindArray[$key]	=	$value;
			}
			
			return	$redifindArray;
		}
	}	

//-----------------------------------------------------------------------
/* End of file users.php */
/* Location: ./application/modules/users/users.php */
}

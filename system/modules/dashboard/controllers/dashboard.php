<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller{
	

//-------------------------------------------------------------------------------	

	/*
	* Properties
	*/
	private $_data	=	array();
	private $_login_userid	=	NULL;

//-------------------------------------------------------------------------------

    /*
    * Costructor
    */
	
    public function __construct()
    {
        parent::__construct();
		
		//$this->session->sess_destroy();
		//exit();

        // Load Models
        $this->load->model('dashboard_model', 'dashboard');
		$this->load->model('haya/haya_model','haya');
		
		// SET Login USER ID
		$this->_login_userid		=	$this->session->userdata('userid');
		$this->_data['module']			=	$this->haya_model->get_module();
		$this->_data['user_detail'] =	$this->haya_model->get_user_detail($this->_login_userid);
		$this->haya->get_listmanagment_types();
		
		$this->_data['bernamij_almasadaat']			=	$this->dashboard->bernamij_almasadaat();
		$this->_data['bernamij_almasadaat_branch']	=	$this->dashboard->bernamij_almasadaat_branch();
		$this->_data['yateem_kafeel']				=	$this->dashboard->yateem_kafeel();
		$this->_data['users']						=	$this->dashboard->users();
		$this->_data['bernamij_almasadaat_new']		=	$this->dashboard->bernamij_almasadaat_new();
		$this->_data['users']						=	$this->dashboard->users();
		$this->_data['users_pie_chart']				=	$this->dashboard->users_pie_chart();
		$this->_data['yateem_kafeel']				=	$this->dashboard->yateem_kafeel();
		$this->_data['all_transactions']			=	$this->dashboard->get_all_transactions();
		$this->_data['all_items']					=	$this->dashboard->get_all_items();
		$this->_data['nooh_sherka']					=	$this->dashboard->nooh_sherka();
		$this->_data['nooh_sherka_pie_chart']		=	$this->dashboard->nooh_sherka_pie_chart();
		$this->_data['yateem_kafeel_count_by_branch']		=	$this->dashboard->yateem_kafeel_count_by_branch();

    }

//-------------------------------------------------------------------------------

    /*
    *
    * Main Page
    */
    public function index()
    {
		if($this->_login_userid)
		{
			$this->load->view('dashboard', $this->_data);
		}
		else
		{
			redirect(base_url().'admin/login');
			exit();
		}
    }
	
	public function my_request_form_one()
	{
		$this->load->view("my_request_form_one_view", $this->_data);
	}
	
	public function my_request_form_two()
	{
		$this->load->view("my_request_form_two_view", $this->_data);
	}
	
	public function getRequestList()
	{
		$this->load->view("my_allrequest_view", $this->_data);
	}
	
	public function getAllRequestForManager($moduleid,$status)
	{
		$managerProfile = $this->_data['user_detail']['profile'];	
		$manager_id = $managerProfile->userid;
		$this->_data['request_all'] = $this->dashboard->getReqData($manager_id,$moduleid,$status);
		$this->load->view("allmyrequest", $this->_data);
	}
	
	public function getRequestCount()
	{
		$managerProfile = $this->_data['user_detail']['profile'];
		$manager_id = $managerProfile->userid;
		$this->db->select("requeststatus,requestdate, fordate");		
		$this->db->where("requeststatus",'P');
		$this->db->where("managerid",$manager_id);	
		$query = $this->db->get("ah_request_form");
		echo $query->num_rows();
	}
	
	public function saveRequestFormOne()
	{
		$formData = $this->input->post();
		unset($formData['mybank']);
		$this->db->select("requeststatus,requestdate, fordate");		
		$this->db->where("userid",$formData['userid']);
		$this->db->where("branchid",$formData['branchid']);
		$this->db->where("managerid",$formData['managerid']);
		$this->db->where("fordate",$formData['fordate']);
		$this->db->where("moduleid",$formData['moduleid']);
		$query = $this->db->get("ah_request_form");		
		if($query->num_rows() > 0)
		{	$reqs = $query->row();			
			$requestMessage = '<strong>'.applicant_status($reqs->requeststatus,2).'  '.arabic_date($reqs->requestdate).'</strong>';		
			echo($requestMessage);
		}
		else
		{			
			$this->db->insert('ah_request_form',$formData,json_encode($formData),$this->_login_userid);
			echo('تلقينا طلبك، وسيتم مراجعته قريبا.<br>يرجى تتحمل معنا بينما نحن فحص بجد الطلب. سوف نتصل بك لنأخذ الامور كذلك يجب أن تطابق طلبك متطلباتنا.');
		}		
	}
	
	public function printpaper($moduleid,$requestid)
	{
		$requestFormID = $this->dashboard->get_request_form_data($requestid);
		$this->_data['user_detail'] =	$this->haya_model->get_user_detail($requestFormID->userid, $requestFormID->accountid);
		$this->_data['requestid'] = $requestFormID->requestid;
		$this->_data['requeststatus'] = $requestFormID->requeststatus;
		$this->_data['moduleid'] = $moduleid;
		$this->load->view("printpaper",$this->_data);
	}
	
	public function updateStatus()
	{
		$requestid = $this->input->post("requestid");
		$requeststatus = $this->input->post("status");		
		$json_data	=	json_encode(array('requeststatus'=>$requeststatus,'requestid'=>$requestid,'actiondate'=>date('Y-m-d')));		
		$data	=	array('requeststatus'=>$requeststatus,'requestid'=>$requestid,'actiondate'=>date('Y-m-d'));		
		$this->db->where('requestid', $requestid);
		$this->db->update('ah_request_form',$json_data,$this->session->userdata('userid'),$data);
	}
	
	public function my_request_list()
	{
		$userid = $this->_data['user_detail']['profile']->userid;
		$userroleid = $this->_data['user_detail']['profile']->userroleid;
		
		$this->_data['mod175'] = $this->dashboard->allRequestAccordingToUser($userid,175,$userroleid);
		$this->_data['mod176'] = $this->dashboard->allRequestAccordingToUser($userid,176,$userroleid);
		$this->load->view("request_list_all", $this->_data);
	}
	
	
}

//-----------------------------------------------------------------------
/* End of file dashboard.php */
/* Location: ./application/modules/admin/dashboard.php */
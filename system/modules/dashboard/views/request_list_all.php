<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12" style="background-color: #FFF !important;">
        <ul class="nav nav-tabs panel panel-default panel-block">
            <li class="tabsdemo-175 active"><a href="#tabsdemo-175" data-toggle="tab">خطاب مفردات راتب</a></li>
            <li class="tabsdemo-176"><a href="#tabsdemo-176" data-toggle="tab">خطاب تعريف</a></li>
        </ul>
        <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-175">
               <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:right;">الأسم الكامل</th>
                      <th style="text-align:center;">الرقم المدنية</th>
                      <th style="text-align:center;">الرقم الوظيفي</th>
                      <th style="text-align:right;">مهنة</th>
                      <th style="text-align:center;">تاريخ</th>
                      <th style="text-align:center;">الحالة</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?PHP foreach($mod175 as $res) {
                        $printUrl = base_url().'dashboard/printpaper/175/'.$res->requestid;
                      ?>
                    <tr role="row">
                      <td  style="text-align:right;"><?PHP echo $res->fullname; ?></td>
                      <td  style="text-align:center;"><?PHP echo arabic_date($res->idcardNumber); ?></td>
                      <td  style="text-align:center;"><?PHP echo arabic_date($res->id_number); ?></td>
                      <td  style="text-align:right;"><?PHP echo $res->profession; ?> / <?PHP echo $res->subprofession; ?></td>
                      <td style="text-align:center;"><?PHP echo arabic_date($res->fordate); ?></td>
                      <td style="text-align:center;"><a href="#" data-url="<?PHP echo($printUrl); ?>" data-id="175" onclick="get_print_page_diag(this);"><?php echo applicant_status($res->requeststatus, 1);?> (<?PHP echo arabic_date($res->actiondate); ?>)</a></td>
                    </tr>
                    <?PHP } ?>
                  </tbody>
                </table>
              
            </div>
            <div class="tab-pane list-group" id="tabsdemo-176"><table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:right;">الأسم الكامل</th>
                      <th style="text-align:center;">الرقم المدنية</th>
                      <th style="text-align:center;">الرقم الوظيفي</th>
                      <th style="text-align:right;">مهنة</th>
                      <th style="text-align:center;">تاريخ</th>
                      <th style="text-align:center;">الحالة</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?PHP foreach($mod176 as $m) {
                      $tUrl = base_url().'dashboard/printpaper/175/'.$res->requestid;
                      ?>
                    <tr role="row">
                      <td  style="text-align:right;"><?PHP echo $m->fullname; ?></td>
                      <td  style="text-align:center;"><?PHP echo arabic_date($m->idcardNumber); ?></td>
                      <td  style="text-align:center;"><?PHP echo arabic_date($m->id_number); ?></td>
                      <td  style="text-align:right;"><?PHP echo $m->profession; ?> / <?PHP echo $m->subprofession; ?></td>
                      <td style="text-align:center;"><?PHP echo arabic_date($m->fordate); ?></td>
                      <td style="text-align:center;"><a href="#" data-url="<?PHP echo($tUrl); ?>" data-id="<?PHP echo($m->moduleid); ?>" onclick="get_print_page_diag(this)"><?php echo applicant_status($m->requeststatus, 1);?> (<?PHP echo arabic_date($m->actiondate); ?>)</a></td>
                    </tr>
                    <?PHP } ?>
                  </tbody>
                </table></div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
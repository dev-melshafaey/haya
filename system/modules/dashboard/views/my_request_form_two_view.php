<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
  <?php $this->load->view('common/bodyscript'); ?>
  <?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
    <?php $this->load->view('common/logo'); ?>
    <?php $this->load->view('common/usermenu'); ?>
    <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
    <?php $this->load->view('common/quicklunchbar'); ?>
    <div class="row">
      <
      <div class="col-md-12">
        <?php $this->load->view('common/panel_block', array('module' => $module));
            $gender = $user_detail['profile']->gender;
        ?>
        <div class="col-md-12 from-group" style="background-color:#FFF">         
          <form name="frm_one_request" id="frm_one_request" method="post" autocomplete="off">
            <input type="hidden" id="managerid" name="managerid" value="<?PHP echo $user_detail['profile']->manager_id; ?>">
            <input type="hidden" id="userid" name="userid" value="<?PHP echo $user_detail['profile']->userid; ?>">
            <input type="hidden" id="branchid" name="branchid" value="<?PHP echo $user_detail['profile']->branchid; ?>">
            <input type="hidden" id="fordate" name="fordate" value="<?PHP echo date('Y-m-d'); ?>">
            <input type="hidden" id="requeststatus" name="requeststatus" value="P">
            <input type="hidden" id='accountid' name="accountid" value="0">
            <input type="hidden" id="moduleid" name="moduleid" value="176">
          <div class="col-md-12 from-group" style="text-align: center;"><h2>إلى من يهمه الأمر</h2></div>
           
          <div class="col-md-12 from-group">
             <h2 style="line-height:40px;">
              تشهد الهيئة العمانية للأعمال الخيرية أن <?PHP echo textChange($gender,0,1); ?> <strong><?PHP echo $user_detail['profile']->fullname; ?></strong> <?PHP echo textChange($gender,1,1); ?> بطاقة شخصية رقم <strong><?PHP echo arabic_date($user_detail['profile']->idcardNumber); ?></strong> <?PHP echo textChange($gender,2,1); ?> حاليا بوظيفة <strong><?PHP echo $user_detail['profile']->profession; ?></strong> برئاسة الهيئة بمسقط منذ تاريخ يعيينها في <strong><?PHP echo arabic_date(date('Y/m/d',strtotime($user_detail['profile']->joining_date))); ?></strong>م ومازالت على رأس عملها حتى تاريخه .
             </h2>
          </div>
          <div class="col-md-12 from-group">
             <h2 style="line-height:40px;">
              وأعطيت لها هذة الشهادة بناء علي طلبها دون <?PHP echo textChange($gender,1,1); ?> الهيئة أدنى مسئولية .
             </h2>
          </div>
          <div class="col-md-12 from-group">
             <h2 style=" text-align: center;">وتفضلوا بقبول فائق الاحترام</h2>
          </div>
          <div class="col-md-12 from-group">
             <h2 style=" text-align: left;">على ابراهيم بن شنون الرئيسي<br>الرئيس التنفيذي</h2>
          </div>
          <div class="col-md-12" style="text-align:center !important;"><button name="formOne" type="button" class="btn btn-success" id="formOne">تقديم الطلب</button></div> 
          <br style="clear: both;">
            </form>
        </div>
         
      </div>
    
    
    </div>  
</section>
 <br style="clear: both;"> 

<?php $this->load->view('common/footer'); ?>
<script>
  $(function(){
      $('#mybank').change(function(){
          var thisvalue = $(this).val().split(':');
          $('#accountid').val(thisvalue[0]);
          $('#accountNumber').html(thisvalue[1]);
        });
      
      $('#formOne').click(function(){
        var accountid = $('#accountid').val();
        if (accountid=='')
        {
          show_notification_error_end('حدد رقم الحساب');
        }
        else
        {
            var requestData = $('#frm_one_request').serialize();
			$.ajax({
				url: config.BASE_URL+'dashboard/saveRequestFormOne/',
				type: "POST",
				data:requestData,
				dataType: "html",
				beforeSend: function() {  check_my_session(); },
				success: function(msg) {
                  show_notification(msg);
                  msg = '';
                }
			});
        }
      });
      
    });
</script>
</body>
</html>
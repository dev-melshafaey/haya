<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
  <?php $this->load->view('common/bodyscript'); ?>
  <?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
    <?php $this->load->view('common/logo'); ?>
    <?php $this->load->view('common/usermenu'); ?>
    <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
    <?php $this->load->view('common/quicklunchbar'); ?>
    <div class="row">
      <
      <div class="col-md-12">
        <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
        <div class="col-md-12 from-group" style="background-color:#FFF">         
          <form name="frm_one_request" id="frm_one_request" method="post" autocomplete="off">
            <input type="hidden" id="managerid" name="managerid" value="<?PHP echo $user_detail['profile']->manager_id; ?>">
            <input type="hidden" id="userid" name="userid" value="<?PHP echo $user_detail['profile']->userid; ?>">
            <input type="hidden" id="branchid" name="branchid" value="<?PHP echo $user_detail['profile']->branchid; ?>">
            <input type="hidden" id="fordate" name="fordate" value="<?PHP echo date('Y-m-d'); ?>">
            <input type="hidden" id="requeststatus" name="requeststatus" value="P">
            <input type="hidden" id="moduleid" name="moduleid" value="175">
          <div class="col-md-2 from-group"><h4>الفاضل / مدير :</h4></div>
          <div class="col-md-4 from-group">
            <?PHP
                $bankInfoSize = sizeof($user_detail['bankinfo']);
                if($bankInfoSize <= 1) {
                  //print_r($user_detail['bankinfo']);
                  echo('<input type="hidden" value="'.$user_detail['bankinfo']->bankaccountid.'" id="accountid" name="accountid">');
                  echo('<h4>'.$user_detail['bankinfo']->bankName.' / فرع '.$user_detail['bankinfo']->branchName.'</h4>');
                }
                else
                {
            ?>
            <input type="hidden" id='accountid' name="accountid">
            <select id="mybank" name="mybank" class="form-control">
              <option value="0">اختيار البنك</option>
              <?PHP foreach($user_detail['bankinfo'] as $bank) { ?>
                <option value="<?PHP echo($bank->bankaccountid.':'.arabic_date($bank->accountnumber)); ?>"><?PHP echo($bank->bankName.' / '.$bank->branchName); ?></option>
              <?PHP } 
                $gender = $user_detail['profile']->gender;
                
              ?>
          </select>
          <?PHP } ?>
          </div>
          <div class="col-md-6 from-group"><h4>المحترم</h4></div>
          <div class="col-md-12 from-group"><h3>الفرع الرئيسي</h3></div>
          <div class="col-md-12 from-group"><h5>السلام عليكم ورحمة الله وبركاته :-</h5></div>
          <div class="col-md-12 from-group">
             <h5 style="font-size: 21px !important; font-weight: normal !important; line-height: 37px;">
               تشهد الهيئة العمانية للأعمال الخيرية بأن <?PHP echo textChange($gender,0,1); ?> <strong><?PHP echo $user_detail['profile']->fullname; ?></strong>
              <?PHP echo textChange($gender,1,1); ?> البطاقة الشخصية رقم <strong><?PHP echo arabic_date($user_detail['profile']->idcardNumber); ?></strong>
              <?PHP echo textChange($gender,2,1); ?> لدي الهيئة بوظيفة <strong><?PHP echo $user_detail['profile']->profession; ?></strong>
              منذ تاريخ <strong><?PHP echo arabic_date(date('Y/m/d',strtotime($user_detail['profile']->joining_date))); ?></strong>
              م <?PHP echo textChange($gender,3,1); ?> راتبا شهريا بالدرجة المالية <strong><?PHP echo $user_detail['profile']->level; ?></strong>
              وسوف تستمر الهيئة فى تحويل <?PHP echo textChange($gender,4,1); ?> الي حساب <?PHP echo textChange($gender,5,1); ?> لدي فرعكم رقم <span id="accountNumber" style="font-weight:bold;"><?PHP echo arabic_date($user_detail['bankinfo']->accountnumber); ?></span>
              طالما <?PHP echo textChange($gender,6,1); ?> على رأس <?PHP echo textChange($gender,7,1); ?> وفي حالة إنهاء <?PHP echo textChange($gender,8,1); ?> معنا لأي سبب من الاسباب سيتم تحويل <?PHP echo textChange($gender,9,1); ?> إن وجدت الى حساب <?PHP echo textChange($gender,5,1); ?>,</h5>
          </div>
          <div class="col-md-12 from-group"><h4>ومفردات راتبه كالاتي:</h4></div>
          <?PHP
            $money = $user_detail['money'];
            $total = number_format($money['BASIC']+$money['EXTRA'],2);
            $grandtotal = number_format($total+$money['INSURANCE'],2);
          ?>
          <div class="col-md-2 from-group"><h5 style="font-weight:bold;">الراتب الأساس:</h5></div>
          <div class="col-md-3 from-group"><h5 style="font-weight:bold;"><?PHP echo(arabic_date($money['BASIC'])); ?> ريال عماني</h5></div>
          <div class="col-md-7 from-group"><h5></h5></div>
          <br style="clear: both;"> 
          <div class="col-md-2 from-group"><h5 style="font-weight:bold;">إجمالى العلاوات:</h5></div>
          <div class="col-md-3 from-group"><h5 style="font-weight:bold;"><?PHP echo(arabic_date($money['EXTRA'])); ?> ريال عماني</h5></div>
          <div class="col-md-7 from-group"><h5></h5></div>
          <br style="clear: both;"> 
          <div class="col-md-2 from-group"><h5 style="font-weight:bold;">إجمالى الراتب:</h5></div>
          <div class="col-md-3 from-group"><h5 style="font-weight:bold;"><?PHP echo(arabic_date($total)); ?> ريال عماني</h5></div>
          <div class="col-md-7 from-group"><h5></h5></div>
          <br style="clear: both;"> 
          <div class="col-md-2 from-group"><h5 style="font-weight:bold;">خصم التأمينات:</h5></div>
          <div class="col-md-3 from-group"><h5 style="font-weight:bold;"><?PHP echo(arabic_date($money['INSURANCE'])); ?> ريال عماني</h5></div>
          <div class="col-md-7 from-group"><h5></h5></div>
          <br style="clear: both;"> 
          <div class="col-md-2 from-group"><h5 style="font-weight:bold;">صافى الراتب:</h5></div>
          <div class="col-md-3 from-group"><h5 style="font-weight:bold;"><?PHP echo(arabic_date($grandtotal)); ?> ريال عماني</h5></div>
          <div class="col-md-7 from-group"><h5>&nbsp;</h5></div>
          <br style="clear: both;"> 
          <div class="col-md-12 from-group"><h5 style="font-size: 21px !important; font-weight: normal !important; line-height: 37px;">وتعتبر هذة الشهادة كبيان وليست ضمانا من قبل الهيئة وانما اعطيت له بناءا على طلبه دون تحمل الهيئة أدني مسئولية .</h5></div>
          <div class="col-md-12 from-group"><h5 style="font-size:18px !important; text-align: center; font-weight: normal !important; line-height: 37px;">وتفضلوا بقبول فائق الاحترام </h5></div>
          <div class="col-md-12 from-group"><h5 style="font-size:18px !important; text-align: left; font-weight: normal !important; line-height: 37px;">على ابراهيم بن شنون الرئيسي<br>الرئيس التنفيذي</h5></div>
           <div class="col-md-12" style="text-align:center !important;"><button name="formOne" type="button" class="btn btn-success" id="formOne">تقديم الطلب</button></div> 
           <br style="clear: both;">
            </form>
        </div>
         
      </div>
    
    
    </div>  
</section>
 <br style="clear: both;"> 

<?php $this->load->view('common/footer'); ?>
<script>
  $(function(){
      $('#mybank').change(function(){
          var thisvalue = $(this).val().split(':');
          $('#accountid').val(thisvalue[0]);
          $('#accountNumber').html(thisvalue[1]);
        });
      
      $('#formOne').click(function(){
        var accountid = $('#accountid').val();
        if (accountid=='')
        {
          show_notification_error_end('حدد رقم الحساب');
        }
        else
        {
            var requestData = $('#frm_one_request').serialize();
			$.ajax({
				url: config.BASE_URL+'dashboard/saveRequestFormOne/',
				type: "POST",
				data:requestData,
				dataType: "html",
				beforeSend: function() {  check_my_session(); },
				success: function(msg) {
                  show_notification(msg);
                  msg = '';
                }
			});
        }
      });
      
    });
</script>
</body>
</html>
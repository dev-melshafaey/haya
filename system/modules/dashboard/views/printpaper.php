<?PHP
  $gender = $user_detail['profile']->gender; 
  $money = $user_detail['money'];
  $total = number_format($money['BASIC']+$money['EXTRA'],2);
  $grandtotal = number_format($total-$money['INSURANCE'],2);
  $bankInfo = $user_detail['bankinfo'];
?>
<div class="col-md-12" id="print_ln">
<form name="mystatus_form" id="mystatus_form" method="post">

<table style="width:100% !important; direction: rtl !important;">
<input type="hidden" name="requestid" id="requestid" value="<?PHP echo($requestid); ?>">
<?PHP if($moduleid==175) {  ?>
  <tr>
    <td colspan="2">&nbsp;</td>    
  </tr>
  <tr>
    <td style="width: 50%; text-align: right;"><h4>الفاضل / مدير : <?PHP echo $bankInfo->bankName; ?> / <?PHP echo $bankInfo->branchName; ?></h4></td>
    <td style="width: 50%; text-align: right;"><h4>المحترم</h4></td>
  </tr>
  <tr style="display:none;">
    <td colspan="2" style="text-align: right;"><h4>الفرع الرئيسي</h4></td>    
  </tr>
    <tr>
    <td colspan="2" style="text-align: right;"><h4>السلام عليكم ورحمة الله وبركاته :-</h4></td>    
  </tr>
  <tr>
    <td colspan="2"  style="text-align: right;"><h4 style="line-height: 37px;">
               تشهد الهيئة العمانية للأعمال الخيرية بأن <?PHP echo textChange($gender,0,1); ?> <strong><?PHP echo $user_detail['profile']->fullname; ?></strong>
              <?PHP echo textChange($gender,1,1); ?> البطاقة الشخصية رقم <strong><?PHP echo arabic_date($user_detail['profile']->idcardNumber); ?></strong>
              <?PHP echo textChange($gender,2,1); ?> لدي الهيئة بوظيفة <strong><?PHP echo $user_detail['profile']->profession; ?></strong>
              منذ تاريخ <strong><?PHP echo arabic_date(date('Y/m/d',strtotime($user_detail['profile']->joining_date))); ?></strong>
              م <?PHP echo textChange($gender,3,1); ?> راتبا شهريا بالدرجة المالية <strong><?PHP echo $user_detail['profile']->level; ?></strong>
              وسوف تستمر الهيئة فى تحويل <?PHP echo textChange($gender,4,1); ?> الي حساب <?PHP echo textChange($gender,5,1); ?> لدي فرعكم رقم <span id="accountNumber" style="font-weight:bold;"><?PHP echo arabic_date($user_detail['bankinfo']->accountnumber); ?></span>
              طالما <?PHP echo textChange($gender,6,1); ?> على رأس <?PHP echo textChange($gender,7,1); ?> وفي حالة إنهاء <?PHP echo textChange($gender,8,1); ?> معنا لأي سبب من الاسباب سيتم تحويل <?PHP echo textChange($gender,9,1); ?> إن وجدت الى حساب <?PHP echo textChange($gender,5,1); ?>,</h4>
          </td>    
  </tr>
  <tr>
    <td colspan="2"></td>    
  </tr>
  <tr>
    <td colspan="2" style="text-align: right;"><h4>ومفردات راتبه كالاتي:</h4></td>    
  </tr>
  <tr>
    <td colspan="2">
      <table  width="100%">
        <tr>
          <td style="width: 20% !important; text-align: right; font-weight: bold;">الراتب الأساس:</td>
          <td style="width: 80% !important; text-align: right; font-weight: bold;"><?PHP echo(arabic_date($money['BASIC'])); ?> ريال عماني</td>
        </tr>
        <tr>
          <td style="width: 20% !important; text-align: right; font-weight: bold;">إجمالى العلاوات:</td>
          <td style="width: 80% !important; text-align: right; font-weight: bold;"><?PHP echo(arabic_date($money['EXTRA'])); ?> ريال عماني</td>
        </tr>
        <tr>
          <td style="width: 20% !important; text-align: right; font-weight: bold;">إجمالى الراتب:</td>
          <td style="width: 80% !important; text-align: right; font-weight: bold;"><?PHP echo(arabic_date($total)); ?> ريال عماني</td>
        </tr>
        <tr>
          <td style="width: 20% !important; text-align: right; font-weight: bold;">خصم التأمينات:</td>
          <td style="width: 80% !important; text-align: right; font-weight: bold;"><?PHP echo(arabic_date($money['INSURANCE'])); ?> ريال عماني</td>
        </tr>
         <tr>
          <td style="width: 20% !important; text-align: right; font-weight: bold;">صافى الراتب:</td>
          <td style="width: 80% !important; text-align: right; font-weight: bold;"><?PHP echo(arabic_date($grandtotal)); ?> ريال عماني</td>
        </tr>
      </table>
      
    </td>    
  </tr>
  <tr>
    <td colspan="2"></td>    
  </tr>
  <tr>
    <td colspan="2"><h4 style="text-align: right;">وتعتبر هذة الشهادة كبيان وليست ضمانا من قبل الهيئة وانما اعطيت له بناءا على طلبه دون تحمل الهيئة أدني مسئولية .</h4></td>    
  </tr>
  <tr>
    <td colspan="2"><h4 style="text-align: center;">وتفضلوا بقبول فائق الاحترام </h4></td>    
  </tr>
  <tr>
    <td colspan="2"><h4 style="text-align: left;">على ابراهيم بن شنون الرئيسي<br>الرئيس التنفيذي</h4></td>    
  </tr>
<?PHP } else  { ?>
  <tr>
    <td colspan="2">&nbsp;</td>    
  </tr>
  <tr>
    <td colspan="2" style="text-align: right;"><h3>إلى من يهمه الأمر</h3></td>
  </tr>
  <tr>
    <td colspan="2" style="text-align: right;"><h4>تشهد الهيئة العمانية للأعمال الخيرية أن <?PHP echo textChange($gender,0,1); ?> <strong><?PHP echo $user_detail['profile']->fullname; ?></strong> <?PHP echo textChange($gender,1,1); ?> بطاقة شخصية رقم <strong><?PHP echo arabic_date($user_detail['profile']->idcardNumber); ?></strong> <?PHP echo textChange($gender,2,1); ?> حاليا بوظيفة <strong><?PHP echo $user_detail['profile']->profession; ?></strong> برئاسة الهيئة بمسقط منذ تاريخ يعيينها في <strong><?PHP echo arabic_date(date('Y/m/d',strtotime($user_detail['profile']->joining_date))); ?></strong>م ومازالت على رأس عملها حتى تاريخه .</h4></td>
  </tr>
  <tr>
    <td colspan="2" style="text-align: right;"><h4>وأعطيت لها هذة الشهادة بناء علي طلبها دون <?PHP echo textChange($gender,1,1); ?> الهيئة أدنى مسئولية .</h4></td>    
  </tr>
    <tr>
    <td colspan="2" style="text-align: center;"><h4>وتفضلوا بقبول فائق الاحترام</h4></td>    
  </tr>
  <tr>
    <td colspan="2"></td>    
  </tr>
  <tr>
    <td colspan="2"><h4 style="text-align: left;">على ابراهيم بن شنون الرئيسي<br>الرئيس التنفيذي</h4></td>    
  </tr>
<?PHP } ?>
  <tr>
    <td colspan="2" style="text-align: center;">
      <select id="requeststatus" name="requeststatus">
        <?PHP
          $html = '';
        foreach(applicant_status('',3) as $key => $value) {
            $html .= '<option value="'.$key.'"';
            if($key==$requeststatus)
            { $html .= ' selected '; }
            $html .= '>'.$value.'</option>';          
          }
          echo $html;
          ?>
        
      </select>
      
      
    </td>    
  </tr>
</table>

    </form>
</div>
<div class="col-md-12" style="text-align: center !important;"><button class="btn btn-success" type="button" style="display: none;" onclick="printthepage('print_ln');" id="printxxx">طباعة</button>        </div>
<script>
$(function(){
    $('#requeststatus').change(function(){
      var status = $(this).val();
      var requestid = $("#requestid").val();
      $.ajax({
			url: config.BASE_URL+'dashboard/updateStatus/',
			type: "POST",
			dataType: "html",
            data: {status:status,requestid:requestid},
            beforeSend: function() {
                $('#requeststatus').hide();
              },
			success: function(msg){
               
                $('#printxxx').show();
			}
		});
    });
  });
</script>
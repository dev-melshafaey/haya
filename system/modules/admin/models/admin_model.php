<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
	
	/*
	*  Properties
	*/
	private $_table_users;
//-------------------------------------------------------------------

	/*
	 *  Constructor
	 */
	function __construct()
	{
		parent::__construct();

		//Get Table Names from Config 
		$this->_table_users 				= 	$this->config->item('table_users');
	}
//-------------------------------------------------------------------

	/*
	* 
	* Check Login User 
	* return OBJECT
	* @param $username integer
	* @param $password integer
	*/
	
	function login_user($username,$password)
	{
		$query = $this->db->query("
		SELECT *
		FROM
		`ah_users`
		INNER JOIN `mh_modules` ON (`ah_users`.`landingpage` = `mh_modules`.`moduleid`) WHERE ah_users.`user_name`='".$username."' AND ah_users.`password`=MD5('".$password."');");
		if($query->num_rows() > 0)
		{
			$this->session->set_userdata('id',$query->row()->id);
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	 * For Login
	 * if user axist set session
	 * @param String $username
	 * @param String $pass
	 * Return true or false
	 */
		
	function login_admin($username,$pass)
	{
		$this->db->select("ah_users.userid,mh_modules.module_controller");
		$this->db->from($this->_table_users);
		$this->db->join('mh_modules','mh_modules.moduleid=ah_users.landingpage');
		$this->db->where("ah_users.userlogin",$username);
		$this->db->where("ah_users.userpassword",md5($pass));
		$this->db->where("ah_users.userstatus",1);
		$query = $this->db->get();	
		
		if($query->num_rows() > 0)
		{
			$this->session->set_userdata("userid",$query->row()->userid);
			$this->session->set_userdata("userdata",$query->row());		
			$this->haya_model->activity_monitor($query->row()->userid,'IN');		
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
/*-------------------------------------------------------------------------*/		
/* End of file admin_model.php */
/* Location: ./appliction/modules/admin/admin_model.php */
}

?>
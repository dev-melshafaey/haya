<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>الهيئة العمانية للأعمال الخيرية</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?PHP $this->load->view('common/fonts'); ?>
<link href="<?PHP echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>
<link href="<?PHP echo base_url();?>assets/admin/pages/css/login-rtl.css" rel="stylesheet" type="text/css"/>
<link href="<?PHP echo base_url();?>assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?PHP echo base_url();?>assets/global/css/plugins-rtl.css" rel="stylesheet" type="text/css"/>

<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="<?PHP echo base_url(); ?>">
	<img src="<?PHP echo base_url();?>assets/admin/layout3/img/logo-big.png" alt="الهيئة العمانية للأعمال الخيرية"/>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form class="mws-form" method="post" id="login_form" action="<?php echo base_url();?>admin/login_admin">
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>
			Enter any username and password. </span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label">إسم الدخول</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
                <input type="text" name="username" autocomplete="off" class="form-control placeholder-no-fix" placeholder="Username" id="username">
				
			</div>
		</div>
		<div class="form-group">
			<label class="control-label">كلمة المرور</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password"  id="pass_text" autocomplete="off" placeholder="Password" name="password"/>
			</div>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn green btn-block uppercase">تسجيل الدخول</button>
		</div>
	</form>
	<!-- END LOGIN FORM -->
</div>

<div class="clearfix"></div>
<div class="copyright text-left">
	 جميع الحقوق محفوظة © 2015 الهيئة العمانية للأعمال الخيرية‎ <br />
	 ‎<a href="http://www.durar-it.com"><img src="<?PHP echo base_url();?>assets/admin/layout3/img/c-logo.png" alt="تصميم و برمجة شركة درر للحلول الذكية" /> تصميم و برمجة شركة درر للحلول الذكية</a>
</div>
</body>
<!-- END BODY -->
</html>
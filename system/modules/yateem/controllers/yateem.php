<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Yateem extends CI_Controller {

	/*
	 * Properties
	 */
	private $_data			=	array();
	private $_login_userid	=	NULL;

//-----------------------------------------------------------------------

	/*
	 * Constructor
	 */
	function __construct()
	{
		 parent::__construct();	
	
		// Loade Admin Model
		$this->load->model('yateem_model','yateem');
		
		$this->_data['module']			=	$this->haya_model->get_module();
		$this->_login_userid			=	$this->session->userdata('userid');
		$this->_data['login_userid']	=	$this->_login_userid;	
		$this->_data['user_detail'] 	= 	$this->haya_model->get_user_detail($this->_login_userid);
				
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
		public function getYateemNames()
		{
		
		$term = $this->input->get('term');
		$this->db->select('orphan_id,orphan_name');
		$this->db->from('ah_yateem_info');
		
		if($term !="")
		{
			$where = 'orphan_name like '.$term.' or orphan_id like '.$term.'';
			$this->db->where($where);
		}
		
		$query = $this->db->get();
			
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $data)
			{
				$arr[] = array('id'=>$data->orphan_id,'label'=>$data->orphan_name,'value'=>$data->orphan_name);
			}
			echo json_encode($arr);

			$this->_data['charity_type_id'] = 205;	
			$this->_data['yateem_mother_data'] = $this->yateem->getParentsData($id,2);
			$this->_data['yateem_banks_data'] = $this->yateem->getBanksData($id);
			$this->_data['yateem_others_data'] = $this->yateem->getOthersData($id);
			$this->_data['yateem_edu_data'] = $this->yateem->getYateemDataById('ah_yateem_education',$id);
			$this->_data['yateem_can_data'] = $this->yateem->getYateemDataById('ah_yateem_candidate',$id);
			$this->_data['yateem_docs'] = $this->yateem->getYateemDocument($id);
			$this->load->view('yateem_details',$this->_data);
		
		}
		}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function details($id, $type)
	{		
		switch($type)
		{
			case 'yateem';
						$this->_data['yateem_data'] = $this->yateem->getYateemData($id);
						$this->_data['yateem_sisters'] = $this->yateem->getBroSisNames($id,'sister');
						$this->_data['yateem_brothers'] = $this->yateem->getBroSisNames($id,'brother');
						$this->_data['yateem_father_data'] = $this->yateem->getParentsData($id,1);
						$this->_data['yateem_mother_data'] = $this->yateem->getParentsData($id,2);
						$this->_data['yateem_banks_data'] = $this->yateem->getBanksData($id);
						$this->_data['yateem_others_data'] = $this->yateem->getOthersData($id);
						$this->_data['yateem_edu_data'] = $this->yateem->getYateemDataById('ah_yateem_education',$id);
						$this->_data['yateem_can_data'] = $this->yateem->getYateemDataById('ah_yateem_candidate',$id);
					echo $this->load->view('yateem_details', $this->_data);
			break;
			case 'passport_number';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail', $this->_data);
			break;
			case 'passport_number_wife';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail', $this->_data);
			break;			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	public function getKafeelNames()
	{
		$term = $this->input->get('term');
		$this->db->select('sponser_id, sponser_name, sponser_id_number');
		$this->db->from('ah_sponser_info');
		if(is_numeric($term))
		{
			$this->db->like('sponser_id_number',$term);	
		}
		else
		{
			$this->db->like('sponser_name',$term);
		}
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $data)
			{
				$arr[] = array('id'=>$data->sponser_id,'spid'=>$data->sponser_id_number,'label'=>$data->sponser_name.' ('.arabic_date($data->sponser_id_number).')','value'=>$data->sponser_name);
			}
				echo json_encode($arr);
		}
	}	
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function add_assign_data()
	{		
			$userid 	=	$this->session->userdata('userid');
			$branch_id 	=   $this->haya_model->get_branch_id($userid);			
			$postData 	=	$this->input->post();
			
			foreach($postData['yateem_id'] as $yateemkey => $yateemid)
			{
				$assignData['orphan_id']  =  $yateemid;
				$assignData['userid'] = $userid;
				$assignData['user_branch_id'] = $branch_id;
				$assignData['file_number']  =  $postData['file_number'];
				$assignData['starting_date']  =   $postData['starting_date_'.$yateemid];
				$assignData['sponser_id'] = $postData['sponser_id'];
				$assignData['details']  =  $postData['details'];
				$assignData['warranty']  =  $postData['warranty'];
				$assignData['payment_type']  =  $postData['payment_type'];
				$assignData['sponser_type']  =  $postData['sponser_type'];
				$assignData['monthly_payment']  =  $postData['monthly_payment'];
				$assignData['monthly_payment']  =  $postData['monthly_payment'];
				$assignData['financial_year'] = $postData['financial_year'];			
				$assignData['start_payment_date'] =  $postData['start_payment_date'];
				$assignData['end_payment_date'] =  $postData['end_payment_date'];
				$assignData['delete_record'] =  '0';				
				$yateemQuery = $this->db->query("SELECT assign_id FROM assigned_orphan WHERE `orphan_id`='".$yateemid."' AND `sponser_id`='".$postData['sponser_id']."'");
				if($yateemQuery->num_rows() > 0)
				{
					$yatemmResult = $yateemQuery->row();
					$this->db->where('assign_id',$yatemmResult->assign_id);
					$this->db->update('assigned_orphan',json_encode($assignData),$userid,$assignData);
				}
				else
				{	$this->db->insert('assigned_orphan',$assignData,json_encode($assignData),$userid);	
					
				}
			}
			
			$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
			redirect(base_url().'yateem/yateem_list');
			exit();		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function getKafeellist($orphan_id)
	{
		$this->_data['orphan_detail'] 	= $this->yateem->getYateemFullDetail($orphan_id);
		$this->_data['kafeel_list'] 	= $this->yateem->getKafeellist($orphan_id);
		
		$this->load->view('kafeel_list_orphan',$this->_data);
	}

//-----------------------------------------------------------------------

	/*
	 * 
	 */
	function add_yateem($id='')
	{
		if(isset($id) && $id!="")
		{
			$this->_data['yateem_data'] 		=	$this->yateem->getYateemDataById($id);
			$this->_data['yateem_sisters'] 		=	$this->yateem->getBroSisNames($id,'sister');
			$this->_data['yateem_brothers'] 	=	$this->yateem->getBroSisNames($id,'brother');
			$this->_data['yateem_father_data'] 	=	$this->yateem->getParentsData($id,1);
			$this->_data['yateem_mother_data'] 	=	$this->yateem->getParentsData($id,2);
			$this->_data['yateem_banks_data'] 	=	$this->yateem->getBanksData($id);
			$this->_data['yateem_others_data'] 	=	$this->yateem->getOthersData($id);
			$this->_data['yateem_docs'] 		=	$this->yateem->getYateemDocument($id);
			
			$this->_data['yateem_edu_data'] 	=	$this->yateem->getYateemDataById('ah_yateem_education',$id);
			$this->_data['yateem_can_data'] 	=	$this->yateem->getYateemDataById('ah_yateem_candidate',$id);
		}
		
		$this->load->view('add-yateem-form',$this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function getYateemDetails($id){
		
			$this->_data['yateem_data'] = $this->yateem->getYateemDataById($id);
			//echo "<pre>";
			//print_r($this->_data['yateem_data']);
			
			$this->_data['yateem_sisters'] = $this->yateem->getBroSisNames($id,'sister');
			$this->_data['yateem_brothers'] = $this->yateem->getBroSisNames($id,'brother');
			$this->_data['yateem_father_data'] = $this->yateem->getParentsData($id,1);
			//echo "<pre>";
			//print_r($this->_data['yateem_father_data']);
			$this->_data['charity_type_id'] = 205;	
			$this->_data['yateem_mother_data'] = $this->yateem->getParentsData($id,2);
			$this->_data['yateem_banks_data'] = $this->yateem->getBanksData($id);
			$this->_data['yateem_others_data'] = $this->yateem->getOthersData($id);
			$this->_data['yateem_edu_data'] = $this->yateem->getYateemDataById('ah_yateem_education',$id);
			$this->_data['yateem_can_data'] = $this->yateem->getYateemDataById('ah_yateem_candidate',$id);
			$this->_data['yateem_docs'] = $this->yateem->getYateemDocument($id);
			$this->load->view('yateem_details',$this->_data);	
		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function assignOrphan()
	{	
		$this->_data['yateem'] = $this->yateem->get_all_yateem_list();
		$this->load->view('assign-yateem-form',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function add_kafeel($id)
	{
		if($id)
		{
			$this->_data['kafeel_data'] =   $this->yateem->getKafeelInfo($id);
			$this->_data['yateem_docs'] = 	$this->yateem->getKafeelDocument($id);
		}
			
		$this->load->view('add-kafeel-form',$this->_data);	
	}
//-----------------------------------------------------------------------
	/*
	* Upload Image/File
	*
	*/	
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
			
		if (!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();
			
			if($thumb	==	TRUE)
			{
				$this->load->library('image_lib');
				
				$config['image_library']	= 'gd2';
				$config['source_image'] 	=  $path . $image_data['file_name'];
				$config['maintain_ratio'] 	= FALSE;
				$config['create_thumb'] 	= FALSE;
				$config['width'] 			= $width;
				$config['height'] 			= $height;
				$config['new_image'] 		= $path . 'thumb_'.$image_data['file_name'];
				
				$this->image_lib->initialize($config);
				$this->image_lib->resize_crop();
			}

			return $image_data['file_name'];
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function add_kafeel_data()
	{
		$userid 	=	$this->session->userdata('userid');
		$branch_id 	=   $this->haya_model->get_branch_id($userid);
			 
		$postData 	=	$this->input->post();
		
		$sponserArr['sponser_name'] 			=	$postData['sponser_name'];
		$sponserArr['sponser_id_number'] 		=	$postData['sponser_id_number'];
		$sponserArr['sponser_id_expire'] 		=	$postData['sponser_id_expire'];
		$sponserArr['sponser_designation'] 		=	$postData['sponser_designation'];
		$sponserArr['country_id'] 				=	$postData['country_id'];
		$sponserArr['city_id'] 					=	$postData['city_id'];
		$sponserArr['sponser_town_id'] 			=	$postData['sponser_town_id'];
		$sponserArr['sponser_po_address'] 		=	$postData['sponser_po_address'];
		$sponserArr['sponser_pc_address'] 		=	$postData['sponser_pc_address'];
		$sponserArr['sponser_fax'] 				=	$postData['sponser_fax'];	
		$sponserArr['sponser_phone_number'] 	=	$postData['sponser_phone_number'];
		$sponserArr['sponser_mobile_number'] 	=	$postData['sponser_mobile_number'];
		$sponserArr['bankid'] 					=	$postData['bankid'];	
		$sponserArr['bankbranchid'] 			=	$postData['bankbranchid'];
		$sponserArr['sponser_nationalty'] 		=	$postData['sponser_nationalty'];
		$sponserArr['sponser_work_place'] 		=	$postData['sponser_work_place'];
		$sponserArr['sponser_account_number'] 	=	$postData['sponser_account_number'];	
		$sponserArr['userid'] 					=	$this->session->userdata('userid');
		$sponserArr['user_branch_id'] 			=	$branch_id;

		//sponser_po_address
		$sponser_id  = $this->check_id('sponser_id','ah_sponser_info','sponser_id',$postData,$sponserArr,'');
			
			if(!empty($_FILES))
			{
				if($_FILES["sponser_picture"]["tmp_name"]){
					$profileData['sponser_picture'] 	=	$this->upload_file($sponser_id,'sponser_picture','resources/sponser');
					$json_data =	json_encode(array('data'	=>$profileData));
					$this->db->where('sponser_id',$sponser_id);
					$this->db->update('ah_sponser_info',$json_data,$this->session->userdata('userid'),$profileData);
			}
		}
			
		$alldocs = $this->haya_model->allRequiredDocument(206);
			
			if(!empty($alldocs))
			{
				foreach($alldocs as $doc)
				{
					$docname = 'doclist'.$doc->documentid;
					if(isset($_FILES["".$docname.""]["tmp_name"]) && $_FILES["".$docname.""]["tmp_name"]!="")
					{
						//echo "ifff";
						$docData['document_name'] 	=	$this->upload_file($yateem_id,$docname,'resources/yateem');
						$docData['sponser_id'] 		=	$sponser_id;
						$docData['doc_type_id'] 	=	$doc->documentid;
						$docData['userid'] 			=	$this->session->userdata('userid');
						$docData['user_branch_id'] 	=	$branch_id;
						
						//$this->check_docfile($doc->documentid,$table,$sponser_id,$docData);
						$this->check_docfile($doc->documentid,'ah_kafeel_documents','sponser_id',$sponser_id,$docData);
					}
				}
			}
			
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
		redirect(base_url().'yateem/kafeel_list');
		exit();		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function kafeel_list()
	{
		$this->load->view('kafeel-listing',$this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function getSponserDetails($id)
	{
			$this->_data['kafeel_data'] =   $this->yateem->getKafeelInfo($id);
			$this->load->view('sponser_details',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function all_assignedsponser_list($status){
		
		$all_users	=	$this->yateem->get_assignedsponser_list_status($status);

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				if($status == 'not')
				{
					$yeteem_count_url = '<a href="'.base_url().'yateem/yateem_list/'.$lc->sponser_id.'">(0)</a>';
				}
				else
				{
					$yeteem_count_url = '<a href="'.base_url().'yateem/yateem_list/'.$lc->sponser_id.'">('.$lc->sponserCount.')</a>';
				}
				
				$action = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getSponserDetails/'.$lc->sponser_id.'" ><i class="icon-eye-open"></i></a>';
				$action .= '<a class="iconspace" href="'.base_url().'yateem/add_kafeel/'.$lc->sponser_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';		$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_kafeel/'.$lc->sponser_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				
				$img = '<img src="'.base_url().'/resources/sponser/'.$lc->sponser_id.'/'.$lc->sponser_picture.'" width="50px" height="50px"> <br/>';
				$arr[] = array(
					"DT_RowId"		=>	$lc->sponser_id.'_durar_lm',
					"رقم الكفيل" 	=>	$lc->sponser_id,
					"اسم" 			=>	$img.$lc->sponser_name,
					"الجنسية" 		=>	is_set($this->haya_model->get_name_from_list($lc->sponser_nationalty)),
					"رقم الهوية" 	=>	is_set($lc->sponser_id_number),
					"عدد الأيتام" 	=>	is_set($yeteem_count_url),
					"الإجرائات" 		=>	$action);
					
					unset($action);
					unset($img);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function kafeel_assigned($id)
	{
		$this->_data['sponser_id'] = $id;
		$this->load->view('kafeel-assignedlisting',$this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function check_docfile($doc_type_id,$table,$wherwKey,$parent_id,$tableData){
		
		$json_data	=	json_encode(array('data'=>$tableData));
		//$whereValue = 'orphan_id='.$parent_id.' AND doc_type_id='.$doc_type_id.'';
		$wherClause =  array($wherwKey =>$parent_id, 'doc_type_id' => $doc_type_id);

		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($wherClause);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$this->db->where($wherClause);
			$this->db->update($table,$json_data,$this->session->userdata('userid'),$tableData);
				
		}
		else
		{
			$this->db->insert($table,$tableData,$json_data,$this->session->userdata('userid'));	
			return  $id = $this->db->insert_id();
		}
		
		
	
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function delete_yateeem($id)
	{
		//$postData = $this->input->post();
		$this->yateem->delete_yateem($id);		
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function delete_kafeel($id)
	{
		echo $this->yateem->delete_kafeel($id);	
			
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function all_sponser_list($user_id	=	NULL)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];;
		
		$all_users	=	$this->yateem->get_sponser_list();

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				
				$action = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getSponserDetails/'.$lc->sponser_id.'" ><i class="icon-eye-open"></i></a>';
				$action .= '<a class="iconspace" href="'.base_url().'yateem/add_kafeel/'.$lc->sponser_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';		$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_kafeel/'.$lc->sponser_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				
				$img = '<img src="'.base_url().'/resources/sponser/'.$lc->sponser_id.'/'.$lc->sponser_picture.'" width="50px" height="50px"> <br/>';
				$arr[] = array(
					"DT_RowId"		=>	$lc->sponser_id.'_durar_lm',
					"رقم الكفيل"	=>	$lc->sponser_id,
					"اسم"			=>	$lc->sponser_name,
					"الجنسية"		=>	$this->haya_model->get_name_from_list($lc->sponser_nationalty),
					"رقم الهوية"	=>	$lc->sponser_id_number,
					"عدد الأيتام"	=>	'<a href="'.base_url().'yateem/yateem_list/'.$lc->sponser_id.'">('.$lc->sponserCount.')</a>',
					"الإجرائات"		=>	$action);
					unset($action);
					unset($img);
			}
			
			
			$ex['data'] = $arr;

			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function yateem_assigned($status = '')
	{
		$this->_data['assigned_status'] = $status;
		$this->load->view('yatem-asssignedlisting',$this->_data);	
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	public function all_yateem_list($user_id=NULL,$type)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		$all_users	=	$this->yateem->get_yateeem_list($user_id,$type);

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				
				$action = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				$action .= '<a class="iconspace" href="'.base_url().'yateem/add_yateem/'.$lc->orphan_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';		$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_yateeem/'.$lc->orphan_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				
				$img = '<img src="'.base_url().'">';
				$arr[] = array(
					"DT_RowId"		=> $lc->orphan_id.'_durar_lm',
					"رقم اليتيم"	=> is_set(arabic_date($lc->orphan_id)),
					"اسم اليتيم" 	=> is_set($lc->orphan_name),
					"الجنسية" 		=> is_set($this->haya_model->get_name_from_list($lc->orphan_nationalty)),
					"تاريخ الميلاد" 	=> is_set(arabic_date($lc->date_birth)),
					"الدولة" 		=> is_set($this->haya_model->get_name_from_list($lc->country_id)),
					"المدينة" 		=> is_set($this->haya_model->get_name_from_list($lc->city_id)),
					"الإجرائات" 		=> $action);
					unset($action);
			}
			
			
			$ex['data'] = $arr;

			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function all_yateemassigned_list($yateem_status)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		$all_users	=	$this->yateem->get_assignedyateeem_list_status($yateem_status);

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				
				$action = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				$action .= '<a class="iconspace" href="'.base_url().'yateem/add_yateem/'.$lc->orphan_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';		$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_yateeem/'.$lc->orphan_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				
				$img = '<img src="'.base_url().'">';
				$arr[] = array(
					"DT_RowId"		=>	$lc->orphan_id.'_durar_lm',
					"رقم اليتيم" 	=>	$lc->orphan_id,
					"اسم اليتيم" 	=>	$lc->orphan_name,
					"الجنسية" 		=>	$this->haya_model->get_name_from_list($lc->orphan_nationalty),
					"تاريخ الميلاد" 	=>	$lc->date_birth,
					"الدولة" 		=>	$this->haya_model->get_name_from_list($lc->country_id),
					"المدينة" 		=>	$this->haya_model->get_name_from_list($lc->city_id),
					"الإجرائات"		=>	$action);
					unset($action);
			}
			
			$ex['data'] = $arr;

			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function all_assignedyateem_list($user_id	=	NULL)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		$all_users	=	$this->yateem->get_assignedyateeem_list();

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				
				$action = '<a class="fancybox-button" rel="gallery1" href="#"><i class="icon-eye-open"></i></a>';
				$action .= '<a class="iconspace" href="'.base_url().'yateem/add_yateem/'.$lc->orphan_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';		$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_yateeem/'.$lc->orphan_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				
				$img = '<img src="'.base_url().'">';
				$arr[] = array(
					"DT_RowId"		=>	$lc->orphan_id.'_durar_lm',
					"اسم اليتيم" 	=>	is_set($lc->orphan_name),
					"الجنسية" 		=>	is_set($lc->orphan_nationalty),
					"تاريخ الميلاد" 	=>	is_set(arabic_date($lc->date_birth)),
					"الدولة" 		=>	is_set(arabic_date($lc->country_id)),
					"اسم الكفيل" 	=>	is_set($lc->sponser_name),
					"الإجرائات" 		=>	$action);
					unset($action);
			}
			
			
			$ex['data'] = $arr;

			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function check_id($id,$table,$key,$postData,$tableData,$ind)
	{
		if(isset($postData[$id]) && $postData[$id]!="" )
		{
			$json_data	=	json_encode(array('data'=>$tableData));

				if(is_array($postData[$id]))
				{
					$whereValue = $postData[$id][$ind];
				}
				else
				{
					$whereValue = $postData[$id];
				}
				
				$this->db->where($key,$whereValue);
				$this->db->update($table,$json_data,$this->session->userdata('userid'),$tableData);
				return $postData[$id];
		}
		else
		{
				$this->db->insert($table,$tableData);
				return $yateem_id = $this->db->insert_id();
		}
	
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function assigned_yateem()
	{
		$this->load->view('assigned-yatem-listing',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function yateem_list($id,$type='')
	{
		$this->_data['sponser_id'] 	= $id;
		$this->_data['type'] 		= $type;
		
		$this->load->view('yatem-listing',$this->_data);		
	}
	
	
	
	
//-----------------------------------------------------------------------
/* End of file yateem.php */
/* Location: ./application/modules/yateem/yateem.php */
}

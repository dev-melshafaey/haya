<div class="row">
  <div class="col-md-12 fox leftborder">
    <h4 class="panel-title customhr">نموذج بيانات الأيتام والرعاية</h4>
    <div class="col-md-4 form-group">
      <label class="text-warning">اسم اليتيم:</label>
      <strong><?PHP echo $yateem_data->orphan_name; ?> </strong> </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">الجنسية: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_data->orphan_nationalty); ?> </strong> </div>
     <div class="col-md-4 form-group">
      <label class="text-warning"> صورة):</label>
      <?PHP
          	$statmentURL = base_url().'resources/yateem/'.$yateem_data->orphan_id.'/'.$yateem_data->orphan_picture;
			//echo getFileResult($statmentURL,'',$statmentURL);
		  ?>
          <img src="<?php echo $statmentURL; ?>"  width="100"/>
    </div> 
    <div class="col-md-6 form-group">
      <label class="text-warning">تاريخ الميلاد: </label>
      <strong><?PHP echo show_date($yateem_data->date_birth,5); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الدولة: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_data->country_id); ?> </strong> </div>
    <div class="col-md-12 form-group">
      <label class="text-warning">المدينة: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_data->city_id); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">ص.ب: </label>
      <strong><?PHP echo $yateem_data->po_adress; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الرمز البريدي: </label>
      <strong><?PHP echo $yateem_data->pc_adress; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">اليتيم أخوته ترتيب بين: </label>
      <strong><?PHP echo $yateem_data->arrangment; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">عدد أفراد الأسرة: </label>
      <strong><?PHP echo $yateem_data->orphan_arrangement_brothers; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الهاتف/ منزل: </label>
      <strong><?PHP echo arabic_date($yateem_data->phone_number); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">هاتف ولي الامر: مع رمز الدولة: </label>
      <strong><?PHP echo arabic_date($yateem_data->home_number); ?></strong> </div>
        <div class="col-md-6 form-group">
      <label class="text-warning">الأعمار الإخواة الأكبر: </label>
      <strong><?PHP if(!empty($yateem_brothers)){
		  		foreach($yateem_brothers as $brothers){
					echo "<br/>".$brothers->br_sis_name."<br/>";
				
				}
		  
		  } ?> </strong> </div>
          
          <div class="col-md-6 form-group">
      <label class="text-warning">الإخواة الأصغر: </label>
      <strong><?PHP if(!empty($yateem_sisters)){
		  		foreach($yateem_sisters as $sisters){
					if($sisters->br_sis_name !="")
					echo "<p>".$sisters->br_sis_name."</p>";
				
				}
		  
		  } ?> </strong> </div>
    
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr">بيانات عن ولي الأمر</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">الاسم بالكامل:</label>
      <strong><?PHP echo $yateem_father_data->parent_name; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">صلة القرابة: </label>
      <strong><?PHP echo $yateem_father_data->parent_relationship; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">ص.ب: </label>
      <strong><?PHP echo $yateem_father_data->parent_po_address; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الرمز البريدي: </label>
      <strong><?PHP echo $yateem_father_data->parent_pc_address; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning"> رقم الهاتف: </label>
      <strong><?PHP echo arabic_date($yateem_father_data->phone_number); ?></strong> </div>
     <div class="col-md-6 form-group">
      <label class="text-warning">دولة: </label>
      <strong><?PHP echo $yateem_father_data->parent_country_id; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">منطقة: </label>
      <strong><?PHP echo $yateem_father_data->parent_region_id; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">ولاية: </label>
      <strong><?PHP echo $yateem_father_data->parent_wilaya_id; ?></strong> </div>
        </div>
      </div>
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr">بيانات الأم</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم الأم:</label>
      <strong><?PHP echo $yateem_mother_data->parent_name; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الحالة المادية: </label>
      <strong><?PHP echo $yateem_mother_data->parent_name; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الهاتف: </label>
      <strong><?PHP echo $yateem_mother_data->phone_number; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الرقم الدولي: </label>
      <strong><?PHP echo $yateem_mother_data->parent_country_code; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">  رقم الهوية: </label>
      <strong><?PHP echo $yateem_mother_data->id_no; ?></strong> </div>
     <div class="col-md-6 form-group">
      <label class="text-warning">عنوان: </label>
      <strong><?PHP echo $yateem_mother_data->parent_address; ?></strong> </div>
        </div>
      </div>
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr"> البيانات بنكية</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم البنك:</label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_banks_data->name_bank); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الفرع: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_banks_data->branch_id); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الحساب: </label>
      <strong><?PHP echo $yateem_banks_data->account_number; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">إسم الحساب: </label>
      <strong><?PHP echo $yateem_banks_data->account_title; ?></strong> </div>
       </div>
      </div>
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr"> بيانات أخرى</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">تاريخ الد الموت:</label>
      <strong><?PHP echo arabic_date($yateem_others_data->date_father_death); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">سبب الوفاة: </label>
      <strong><?PHP echo $yateem_others_data->reason_father_death; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">عدد أفراد الأسرة: </label>
      <strong><?PHP echo $yateem_others_data->family_members; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الاخوة: </label>
      <strong><?PHP echo $yateem_others_data->brothers; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الأخوات: </label>
      <strong><?PHP echo $yateem_others_data->sisters; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الظروف المعيششة للأسرة: </label>
      <strong><?PHP echo $yateem_others_data->living_condition_family; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الظروف السكينة: </label>
      <strong><?PHP echo $yateem_others_data->housing_condition; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الظروف الاقتصادية: </label>
      <strong><?PHP echo $yateem_others_data->economic_condition; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">مقدار الدخل الشهري: </label>
      <strong><?PHP echo $yateem_others_data->monthly_income; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">مصدر الدخل: </label>
      <strong><?PHP echo $yateem_others_data->source_income; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">عدد  الأخوة المكفين: </label>
      <strong><?PHP echo $yateem_others_data->number_brothers; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الجهات الكافلة: </label>
      <strong><?PHP echo $yateem_others_data->foster_agencies; ?></strong> </div>
       </div>
      </div>
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr"> بيانات المرحلة الدراسية</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم المدرسة:</label>
      <strong><?PHP echo $yateem_edu_data->school_name; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">المرحلة الدراسية: </label>
      <strong><?PHP echo $yateem_edu_data->grade; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">النتيجة آخر: </label>
      <strong><?PHP echo $yateem_edu_data->last_result; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الصف: </label>
      <strong><?PHP echo $yateem_data->classs; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الترتيب: </label>
      <strong><?PHP echo $yateem_data->order; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الحالة الصحية: </label>
      <strong><?PHP echo $yateem_data->health_status; ?></strong> </div>
       </div>
      </div>
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr"> بيانات المرحلة الدراسية</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم المدرسة:</label>
      <strong><?PHP echo $yateem_edu_data->school_name; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">المرحلة الدراسية: </label>
      <strong><?PHP echo $yateem_edu_data->grade; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">النتيجة آخر: </label>
      <strong><?PHP echo $yateem_edu_data->last_result; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الصف: </label>
      <strong><?PHP echo $yateem_edu_data->classs; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الترتيب: </label>
      <strong><?PHP echo $yateem_edu_data->order; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الحالة الصحية: </label>
      <strong><?PHP echo $yateem_edu_data->health_status; ?></strong> </div>
       </div>
      </div>
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr"> الجهة المرشحة بيانات</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم الجهة:</label>
      <strong><?PHP echo $yateem_can_data->candidate_name; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الدولة: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_can_data->candidate_country_id); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">المحافظة: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_can_data->candidate_region_id); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">المدينة: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_can_data->candidate_city_id); ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الترتيب: </label>
      <strong><?PHP echo $yateem_can_data->candidate_city_id; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">العنوان: </label>
      <strong><?PHP echo $yateem_can_data->candidate_fax; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">صندوق البريد: </label>
      <strong><?PHP echo $yateem_can_data->candidate_pobox; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">بلدة: </label>
      <strong><?PHP echo $yateem_can_data->candidate_town; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">رقم الهاتف: </label>
      <strong><?PHP echo $yateem_can_data->candidate_phone_number; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الرمز: </label>
      <strong><?PHP echo $yateem_can_data->candidate_pincode; ?></strong> </div>
       </div>
      </div>
    </div>
    <div class="col-md-5 form-group"> <strong><?PHP echo arabic_date($yateem_data->ownershiptype_amount); ?></strong> </div>
    
    <div class="form-group col-md-12">
                    <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <h4>اترفق الوثائق والمستندات الازمة لطلب</h4>
                  <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                    <?PHP 
					$doccount = 0;
					///echo "<pre>";
					//print_r($yateem_docs);
					foreach($this->haya_model->allRequiredDocument(205) as $ctid) { $doccount++;
					//	print_r($ctid); 
						//$doc = $ah_applicant_documents[$ctid->documentid];
						$key = $this->haya_model->multidimensional_search($yateem_docs,array('doc_type_id'=>$ctid->documentid));
						//print_r($yateem_docs[$key]);		
						$c_doc = $yateem_docs[$key];				
						$url = 'resources/yateem/'.$c_doc['orphan_id'].'/'.$c_doc['document_name'];
											
					?>
                    <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                      <div class="panel-heading" style="padding:10px 3px;" id="head<?PHP echo $c_doc[$document_id];?>">
                        <h4 class="panel-title" style="font-size:15px;">
                          <?PHP if($c_doc['document_id']!='') { ?>
                          <span class="icons" id="removeicons<?PHP echo $c_doc['document_id']; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> </span>
                          <?PHP } ?>
                          <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $c_doc['document_id'];?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                      </div>
                      <div id="demo-collapse<?PHP echo $c_doc['document_id'];?>" class="panel-collapse collapse">
                        <div class="panel-body" style="text-align:right;">
                        </div>
                      </div>
                    </div>
                    <?PHP } ?>
                  </div>
                </div>
              </div>
            </div>
            		</div>
  </div>
</div>

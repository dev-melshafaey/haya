<?php $segment	=	$this->uri->segment(1);?>
<?php $text		=	$this->lang->line($segment);?>
<?php $labels	=	$text['users']['form'];?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<style type="text/css">
.nav-tabs {
	margin-bottom: 14px !important;
}
</style>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
    <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
    <div class="col-md-12">
    <form action="<?php echo base_url();?>yateem/add_kafeel_data" method="POST" id="user_form" name="user_form" enctype="multipart/form-data" >
      <input type="hidden"  name="sponser_id" id="sponser_id" value="<?php echo $yateem_data->orphan_id; ?>"/>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="list-group">
            <div class="list-group-item" id="input-fields">
              <div class="col-md-6 panel panel-default panel-block">
                <h4 style="border-bottom: 1px solid #EEE;"> الكفيل بيانات
                  <?PHP if($kafeel_data->sponser_id) { echo ' - رقم الكفيل : '.arabic_date($kafeel_data->sponser_id); } ?>
                </h4>
                <div class="form-group col-md-6">
                  <label class="text-warning">عدد الأيتام</label>
                  <input type="text" class="form-control"  readonly placeholder="" value="<?php echo $kafeel_data->total_yateem; ?>"/>
                  <input type="hidden" id="sponser_id" name="sponser_id" value="<?php echo $kafeel_data->sponser_id; ?>">
                </div>
                <div class="form-group col-md-6"> 
                  <!--<h4>Orphans Data Form the desired Sponsorship</h4>-->
                  <label class="text-warning">اسم</label>
                  <input type="text" class="form-control req" name="sponser_name" id="sponser_name" placeholder="اسم " value="<?php echo $kafeel_data->sponser_name; ?>"/>
                </div>
                <div class="form-group  col-md-6">
                  <label class="text-warning">الجنسية</label>
                  <?php //$yateem_data->orphan_nationalty ?>
                  <?php echo $this->haya_model->create_dropbox_list('sponser_nationalty','nationality',$kafeel_data->sponser_nationalty,'req'); ?> </div>
                <div class="form-group col-md-6">
                  <label class="text-warning">رقم الهوية</label>
                  <input type="text" class="form-control req"  id="sponser_id_number" name="sponser_id_number" placeholder="رقم الهوية" value="<?php echo $kafeel_data->sponser_id_number; ?>" onKeyUp="only_numeric(this);"/>
                </div>
                <div class="form-group col-md-6">
                  <label class="text-warning">تاريخ انتها</label>
                  <input type="text" class="form-control dp req" name="sponser_id_expire" id="sponser_id_expire" placeholder="تاريخ انتها" value="<?php echo $kafeel_data->sponser_id_expire; ?>"/>
                </div>
                <div class="form-group  col-md-6">
                   <label class="text-warning">الوظيفة</label>
                   <?php $all_sub_professions	=	$this->haya_model->get_professions('profession');?>
                   <select class="form-control req" name="sponser_designation" id="sponser_designation">
                        <option>الوظيفة</option>
                       <?php foreach($all_sub_professions	as	$prof):?>
                        <option value="<?php echo $prof['list_id'];?>" <?php if($prof['list_id']	==	$kafeel_data->sponser_designation):?> selected="selected" <?php endif;?>><?php echo $prof['list_name'];?></option>
                       <?php endforeach;?>
                   </select>
                   </div>
                <div class="form-group col-md-6">
                  <label class="text-warning">رقم الهاتف</label>
                  <input type="text" class="form-control req" name="sponser_phone_number" id="sponser_phone_number" placeholder="رقم الهاتف" value="<?php echo $kafeel_data->sponser_phone_number; ?>" onKeyUp="only_numeric(this);"/>
                </div>
                <div class="form-group col-md-6">
                  <label class="text-warning">مكان العمل</label>
                  <input type="text" class="form-control req"  id="sponser_work_place" name="sponser_work_place" placeholder="مكان العمل" value="<?php echo $kafeel_data->sponser_work_place; ?>" />
                </div>
                <br clear="all" />
                <h4 style="border-bottom: 1px solid #EEE;">باالكامل العنوان</h4>
                <div class="form-group col-md-3">
                  <label class="text-warning">الدولة</label>
                  <?PHP  echo $this->haya_model->create_dropbox_list('country_id','issuecountry',$kafeel_data->country_id,'0','req'); ?>
                </div>
                <div class="form-group col-md-3">
                  <label class="text-warning">المدينة</label>
                  <?PHP echo $this->haya_model->create_dropbox_list('city_id','issuecountry',$kafeel_data->city_id,$kafeel_data->country_id,'0'); ?> </div>
                <div class="form-group col-md-3">
                  <label class="text-warning">البلدة</label>
                  <?PHP //echo $this->haya_model->create_dropbox_list('','sponser_town_id',$item->town,$yateem_data->sponser_town_id,''); ?>
                  <input type="text" class="form-control" name="sponser_town_id" id="sponser_town_id" placeholder="" value="<?php echo $kafeel_data->sponser_town_id; ?>"/>
                </div>
                <div class="form-group col-md-3">
                  <label class="text-warning">ص.ب</label>
                  <input type="text" class="form-control" name="sponser_po_address" id="sponser_po_address" placeholder="ص.ب" value="<?php echo $kafeel_data->sponser_po_address; ?>" onKeyUp="only_numeric(this);"/>
                </div>
                <div class="form-group col-md-12">
                  <label class="text-warning">الرمز البريدي</label>
                  <input type="text" class="form-control" name="sponser_pc_address" id="sponser_pc_address" placeholder="الرمز البريدي" value="<?php echo $kafeel_data->sponser_pc_address; ?>"/>
                </div>
                <div class="form-group col-md-4">
                  <label class="text-warning">االفاكس</label>
                  <input type="text" class="form-control req" name="sponser_fax" id="sponser_fax" placeholder="االفاكس" value="<?php echo $kafeel_data->sponser_fax; ?>" onKeyUp="only_numeric(this);"/>
                </div>
                
                <div class="form-group col-md-4">
                  <label class="text-warning"> النتقال</label>
                  <input type="text" class="form-control req NumberInput" name="sponser_mobile_number" id="sponser_mobile_number" placeholder="النتقال" value="<?php echo $kafeel_data->sponser_mobile_number; ?>" onKeyUp="only_numeric(this);"/>
                </div>
                <div class="form-group col-md-4">
                  <label for="basic-input">الصورة</label>
                  <input style="border: 0px !important; color: #d09c0d;" type="file" name="sponser_picture" title='تحميل'>
                </div>
                <div class="col-md-12">
                  <input type="button" id="user_data" class="btn btn-success btn-lrg" name="user_data"  value="حفظ" />
                </div>
              </div>
              <div class="col-md-6  panel panel-default panel-block" style="border-right: 2px solid #eee;">
                <div class="col-md-13">
                  <ul class="nav nav-tabs panel panel-default panel-block">
                    <li class="tabsdemo-1 active"><a href="#tabsdemo-1"  data-toggle="tab">تفاصيل البنك </a></li>
                    <!-- <li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
                        <li class="tabsdemo-2"><a href="#tabsdemo-2"  data-toggle="tab">بيانات الأم</a></li>
                       -->
                  </ul>
                </div>
                <div class="tab-content panel panel-default panel-block">
                  <div class="tab-pane list-group active" id="tabsdemo-1">
                    <div class="form-group col-md-4">
                      <label class="text-warning">اسم بنك</label>
                      <?PHP echo $this->haya_model->create_dropbox_list('bankid','bank',$kafeel_data->bankid,0,'req'); ?> </div>
                    <div class="form-group col-md-4">
                      <label class="text-warning">الفرع</label>
                      <?PHP //echo $this->haya_model->create_dropbox_list('sponser_bank_branch_id',$sponser_data->branch_id,$item->province,0,$kafeel_data->sponser_bank_branch_id); ?>
                      <?PHP echo $this->haya_model->create_dropbox_list('bankbranchid','bank_branch',$kafeel_data->bankbranchid,$kafeel_data->bankid,'0'); ?> </div>
                    <div class="form-group col-md-4">
                      <label class="text-warning">رقم الحساب</label>
                      <input type="text" class="form-control req " name="sponser_account_number" id="sponser_account_number" placeholder="" value="<?php echo $kafeel_data->sponser_account_number; ?>" onKeyUp="only_numeric(this);"/>
                    </div>
                  </div>
                </div>
                <div class="form-group col-md-12">
                  <div class="tab-pane list-group active" id="tabsdemo-1">
                    <div class="list-group-item">
                      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                        <h4>اترفق الوثائق والمستندات الازمة لطلب</h4>
                        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                          <?PHP 
						  $doccount = 0;
						  foreach($this->haya_model->allRequiredDocument(206) as $ctid) 
						  {
							  $doccount++;
							  if(!empty($yateem_docs))
							  {
								  $key = $this->haya_model->multidimensional_search($yateem_docs,array('doc_type_id'=>$ctid->documentid));
								  $c_doc = $yateem_docs[$key];
								  $url = 'resources/yateem/'.$c_doc['orphan_id'].'/'.$c_doc['document_name'];
							  }
							  else
							  {	$c_doc = '';	}
											
					?>
                          <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                            <div class="panel-heading" style="padding:10px 3px;" id="head<?PHP echo $c_doc[$document_id];?>">
                              <h4 class="panel-title" style="font-size:15px;">
                                <?PHP if($c_doc['document_id']!='') { ?>
                                <span class="icons" id="removeicons<?PHP echo $c_doc['document_id']; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> <i onClick="removeDocument(this);" data-id="<?PHP echo $c_doc['document_id']; ?>" data-remove="<?PHP echo $ctid->documentid; ?>" class="icon-remove-sign" style="color:#FF0000; cursor:pointer;"></i> </span>
                                <?PHP } ?>
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $c_doc['document_id'];?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                            </div>
                            <div id="demo-collapse<?PHP echo $c_doc['document_id'];?>" class="panel-collapse collapse">
                              <div class="panel-body" style="text-align:right;">
                                <input type="file" name="doclist<?PHP echo $ctid->documentid;?>" placeholder="<?PHP echo $ctid->documenttype;?>" class="form-control <?PHP if($ctid->isrequired==1 && $c_doc['document_name']=='') { echo 'req'; } ;?>">
                              </div>
                            </div>
                          </div>
                          <?PHP } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <br clear="all" />
        </div>
      </div>
      </div>
      </div>
    </form>
  </div>
  </div>
  </div>
  
</section>
<?php $this->load->view('common/footer'); ?>
<script type="text/javascript">
$(document).ready(function (){
	//alert('ready');
	$('.dp').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
		dateFormat:'yy-mm-dd',
        onClose: function(dateText, inst) { 
          //  var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
          //  var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            //$(this).datepicker('setDate', new Date(year, month, 1));
        }
    });
});
</script> 
<!-- /.modal-dialog -->

</div>
</body>
</html>
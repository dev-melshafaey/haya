<?php ?>
<?php $text		=	$this->lang->line('orpahan');?>
<?php $labels	=	$text['orpahan']['listing'];?>
<?php //echo '<pre>'; print_r($text);?>

<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <?PHP $this->load->view("common/globalfilter",array('type'=>'users')); ?>
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"> <a class="btn btn-success" style="float:right;" href="<?php echo base_url();?>users/add_user" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th><?php echo $labels['orphan_name'];?></th>
                        <th><?php echo $labels['ophan_gender'];?></th>
                        <th><?php echo $labels['orphan_dob'];?></th>
                        <th><?php echo $labels['country'];?></th>
                        <th><?php echo $labels['city'];?></th>
                        <th><?php echo $labels['actions'];?></th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'yateem/all_yateem_list/',
'columns_array'=>'{"data":"‫'.$labels['orphan_name'].'"},
				  {"data":"'.$labels['ophan_gender'].'"},
				  {"data":"'.$labels['orphan_dob'].'"},
				  {"data":"'.$labels['country'].'"},
				  {"data":"'.$labels['city'].'"},
				  {"data":"'.$labels['actions'].'"}')); ?>
</div>
</body>
</html>
<div class="row">
  <div class="col-md-12 fox leftborder">
    <h4 class="panel-title customhr">بيانات الكفيل</h4>
    <div class="col-md-4 form-group">
      <label class="text-warning">عدد الأيتام:</label>
      <strong><?PHP echo  $kafeel_data->total_yateem; ?> </strong> </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">اسم: </label>
      <strong><?PHP echo $kafeel_data->sponser_name; ?> </strong> </div>
      <div class="col-md-4 form-group">
      <label class="text-warning">الجنسية: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($kafeel_data->sponser_nationalty); ?> </strong> </div>
      <div class="col-md-4 form-group">
      <label class="text-warning">تاريخ انتها: </label>
      <strong><?PHP echo arabic_date($kafeel_data->sponser_id_expire); ?> </strong> </div>
      <div class="col-md-4 form-group">
      <label class="text-warning">الوظيفة : </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($kafeel_data->sponser_designation); ?> </strong> </div>
     <div class="col-md-4 form-group">
      <label class="text-warning"> صورة):</label>
      <?php $statmentURL = base_url().'resources/sponser/'.$kafeel_data->sponser_id.'/'.$kafeel_data->sponser_picture;?>
      <?php if($kafeel_data->sponser_picture):?>
          <img src="<?php echo $statmentURL; ?>"  width="100"/>
      <?php endif;?>
    </div> 
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr">باالكامل العنوان</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">الدولة:</label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($kafeel_data->country_id); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">المدينة: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($kafeel_data->city_id); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">البلدة: </label>
      <strong><?PHP echo $kafeel_data->sponser_town_id; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">ص.ب: </label>
      <strong><?PHP echo $kafeel_data->sponser_po_address; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning"> الرمز البريدي: </label>
      <strong><?PHP echo $kafeel_data->sponser_pc_address; ?></strong> </div>
     <div class="col-md-6 form-group">
      <label class="text-warning">االفاكس: </label>
      <strong><?PHP echo $kafeel_data->sponser_fax; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">رقم الهاتف: </label>
      <strong><?PHP echo $kafeel_data->sponser_phone_number; ?></strong> </div>
     <div class="col-md-6 form-group">
      <label class="text-warning">مكان العمل: </label>
      <strong><?PHP echo $kafeel_data->sponser_work_place; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">النتقال: </label>
      <strong><?PHP echo $kafeel_data->sponser_mobile_number; ?></strong> </div>
        </div>
      </div>
    </div>
    
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr"> البيانات بنكية</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم البنك:</label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($kafeel_data->bankid); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الفرع: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($kafeel_data->bankbranchid); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الحساب: </label>
      <strong><?PHP echo $kafeel_data->account_number; ?></strong> </div>
       </div>
      </div>
    </div>
    <div class="form-group col-md-12">
                    <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <h4>اترفق الوثائق والمستندات الازمة لطلب</h4>
                  <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                    <?PHP 
					$doccount = 0;
					///echo "<pre>";
					//print_r($yateem_docs);
					foreach($this->haya_model->allRequiredDocument(206) as $ctid) { $doccount++;
					//	print_r($ctid); 
						//$doc = $ah_applicant_documents[$ctid->documentid];
						$key = $this->haya_model->multidimensional_search($yateem_docs,array('doc_type_id'=>$ctid->documentid));
						//print_r($yateem_docs[$key]);		
						$c_doc = $yateem_docs[$key];				
						$url = 'resources/yateem/'.$c_doc['orphan_id'].'/'.$c_doc['document_name'];
											
					?>
                    <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                      <div class="panel-heading" style="padding:10px 3px;" id="head<?PHP echo $c_doc[$document_id];?>">
                        <h4 class="panel-title" style="font-size:15px;">
                          <?PHP if($c_doc['document_id']!='') { ?>
                          <span class="icons" id="removeicons<?PHP echo $c_doc['document_id']; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> </span>
                          <?PHP } ?>
                          <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $c_doc['document_id'];?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                      </div>
                      <div id="demo-collapse<?PHP echo $c_doc['document_id'];?>" class="panel-collapse collapse">
                        <div class="panel-body" style="text-align:right;">
                        </div>
                      </div>
                    </div>
                    <?PHP } ?>
                  </div>
                </div>
              </div>
            </div>
            		</div>
  </div>
</div>

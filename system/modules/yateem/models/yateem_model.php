<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Yateem_model extends CI_Model
{
	
	/*
	*  Properties
	*/
	
	private $_table_users;
	private $_table_users_documents;
	private $_table_listmanagement;
	private $_table_modules;
	private $_table_userprofile;
	private $_table_users_bankaccount;
	private $_table_users_communications;
	private $_table_users_family;
	private $_table_users_salary;
	private $_table_users_salary_detail;
	private $_table_branchs;
	private $_login_userid;
	
//-------------------------------------------------------------------

	/*
	 *  Constructor
	 */
	function __construct()
	{
		parent::__construct();

		//Get Table Names from Config 
		$this->_table_users 					= 	$this->config->item('table_users');
		$this->_table_users_documents 			= 	$this->config->item('table_users_documents');
		$this->_table_listmanagement 			= 	$this->config->item('table_listmanagement');
		$this->_table_modules 					= 	$this->config->item('table_modules');
		$this->_table_userprofile 				= 	$this->config->item('table_userprofile');
		$this->_table_users_bankaccount 		= 	$this->config->item('table_users_bankaccount');
		$this->_table_users_communications 		= 	$this->config->item('table_users_communications');
		$this->_table_users_family 				= 	$this->config->item('table_users_family');
		$this->_table_users_salary 				= 	$this->config->item('table_users_salary');
		$this->_table_users_salary_detail 		= 	$this->config->item('table_users_salary_detail');
		$this->_table_branchs 					= 	$this->config->item('table_branchs');
		$this->_login_userid					=	$this->session->userdata('userid');
		
		// If SESSION is empty SET by defult Arabic
		if($this->session->userdata('lang')	==	NULL)
		{
			$this->session->set_userdata('lang', 'arabic');
			$this->session->set_userdata('lang_code', 'ar');
		}
		        
        // Change language
       if($this->session->userdata('lang') == 'english' || $this->uri->segment(3) == 'en')
       {
		   // Load ENGLISH Language File
		   $this->lang->load('project', 'english');
		   
		   // Set SESSION for English File name
		   $this->session->set_userdata('lang', 'english');
       }
       else 
       {
		   // Load ENGLISH Language File
        	$this->lang->load('project', 'arabic');
       }
       
	}
//-------------------------------------------------------------------
	/**
	 * Valadting & Checking user session
	 *
	 * @access	public
	 * @return	Array with complete info of the user
	 */	
	public function get_user_detail($userid=0)
	{
		$this->db->select('`ah_users`.`userid`,`ah_users`.`branchid`,`ah_users`.`userroleid`,`ah_users`.`userstatus`,`mh_modules`.`module_controller`,`ah_userprofile`.`fullname`,`ah_userprofile`.`email`,`ah_userprofile`.`profilepic`,`ah_branchs`.`branchname`,`ah_branchs`.`branchaddress`,`ah_listmanagement`.`list_name`,`ah_listmanagement_1`.`list_name`');
		$this->db->from('ah_users');
		$this->db->join('mh_modules','mh_modules.moduleid = ah_users.landingpage','left');
		$this->db->join('ah_userprofile', 'ah_userprofile.userid = ah_users.userid','left');
		$this->db->join('ah_branchs', 'ah_branchs.branchid = ah_users.branchid','left');
		$this->db->join('ah_listmanagement', 'ah_listmanagement.list_id = ah_branchs.provinceid','left');
		$this->db->join('alhaya.ah_listmanagement AS ah_listmanagement_1', 'ah_listmanagement_1.list_id = ah_branchs.wiliayaid','left');
		$this->db->where('`ah_listmanagement`.`delete_record`','0');
		
		if($userid!=0 && $userid!='')
		{	
			$this->db->where('`ah_users`.`userid`',$userid);
		}
		else
		{	$this->db->where('`ah_users`.`userid`',$this->_login_userid);	}
			$query = $this->db->get();
			
			if($query->num_rows() <= 0 )
			{
				redirect(base_url().'admin/login');
			}					
			$userinfo = array('_userid'=>$this->_login_userid,'profile'=>$query->row());
			return $userinfo;
	}
	
	function get_yateeem_list($id,$type)
	{
		$this->db->select('ai.*');
        $this->db->from('ah_yateem_info as ai');
		$this->db->where('ai.delete_record','0');
		if($id !=""){
			//$this->db->join('ah_listmanagement','ah_listmanagement.list_id = ai.orphan_nationalty','Inner');
			if($type =='branch'){
				$this->db->where('ai.user_branch_id',$id);
			}
			else
			{
				$this->db->join('assigned_orphan ao','ao.orphan_id = ai.orphan_id','Inner');
				$this->db->where('ao.sponser_id',$id);
			}
		}
		 $this->db->order_by("orphan_id", "DESC");
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
	
	function get_assignedyateeem_list_status($status)
	{
		$this->db->select('ai.*');
        $this->db->from('ah_yateem_info as ai');
		$this->db->where('ai.delete_record','0');
		if($status == 'not')
			$this->db->join('assigned_orphan ao','ao.orphan_id != ai.orphan_id','Inner');
		else
			$this->db->join('assigned_orphan ao','ao.orphan_id = ai.orphan_id','Inner');
		$this->db->order_by("orphan_id", "DESC");
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
	
		function get_assignedyateeem_list()
	{
		$this->db->select('ai.*,si.sponser_name');
        $this->db->from('ah_yateem_info as ai');
		$this->db->where('ai.delete_record','0');
			//$this->db->join('ah_listmanagement','ah_listmanagement.list_id = ai.orphan_nationalty','Inner');
		$this->db->join('assigned_orphan ao','ao.orphan_id = ai.orphan_id','Inner');
		$this->db->join('ah_sponser_info si','si.sponser_id = ao.sponser_id','Inner');
		$this->db->order_by("orphan_id", "DESC");
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
	
	function getYateemDocument($yateem_id){
					$this->db->select('*');
			$this->db->from('ah_yateem_documents');
			$this->db->where('orphan_id',$yateem_id);
			$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			//echo "if";
			return  $query->result_array();
		}
		else{
			//echo "else";
			return false;
		}
	}
	
	function get_assignedsponser_list_status($status)
	{
		//if($status == 'not')
		//$select_sub = '0 as sponserCount';	
		//else
		$select_sub = 'count(ao.sponser_id) as sponserCount';	
		
		$this->db->select('ai.*,'.$select_sub.'');
        $this->db->from('ah_sponser_info as ai');
		$this->db->where('ai.delete_record','0');
		if($status == 'not')
		$this->db->join('assigned_orphan ao','ao.sponser_id != ai.sponser_id','Inner');
		else
		$this->db->join('assigned_orphan ao','ao.sponser_id = ai.sponser_id','Inner');
		
		$this->db->group_by("ai.sponser_id");
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
	
	
	function getKafeelDocument($document_id){
					$this->db->select('*');
			$this->db->from('ah_kafeel_documents');
			$this->db->where('document_id',$document_id);
			$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			//echo "if";
			return  $query->result_array();
		}
		else{
			//echo "else";
			return false;
		}
	}
	
	function getYateemData($id){
		$this->db->select('*');
        $this->db->from('ah_yateem_info as ai');
		$this->db->join('ah_listmanagement','ah_listmanagement.list_id = ai.orphan_nationalty','Inner');
		$this->db->join('ah_yateem_banksinfo` AS bi ','bi.yateem_id = ai.orphan_id','Inner');
		$this->db->join('ah_yateem_candidate AS ac ','ac.yateem_id = ai.orphan_id','Inner');
		$this->db->join('ah_yateem_education AS ae ','ae.yateem_id = ai.orphan_id','Inner');
		$this->db->join('ah_yateem_other_data AS at ','at.yateem_id = ai.orphan_id','Inner');
		 $this->db->order_by("orphan_id", "DESC");
		$this->db->where('ai.delete_record','0');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
	
	function getKafeellist($orphanid)
	{		
		$this->db->select('assigned_orphan.starting_date, assigned_orphan.financial_year, assigned_orphan.monthly_payment,
		assigned_orphan.warranty, assigned_orphan.start_payment_date, assigned_orphan.end_payment_date
		, sponsertype.list_name AS sponser_type
		, paymenttype.list_name AS payment_type
		, ah_sponser_info.sponser_name
		, ah_sponser_info.sponser_id_number
		, ah_sponser_info.sponser_designation');
        $this->db->from('assigned_orphan');
		$this->db->join('ah_listmanagement as paymenttype','paymenttype.list_id = assigned_orphan.payment_type');
		$this->db->join('ah_listmanagement` AS sponsertype ','sponsertype.list_id = assigned_orphan.sponser_type');
		$this->db->join('ah_sponser_info','ah_sponser_info.sponser_id = assigned_orphan.sponser_id');		
		$this->db->where('assigned_orphan.orphan_id',$orphanid);
		$query = $this->db->get();
		
		return $query->result();
	}
	
	
	function getKafeelInfo($id){
			$this->db->select('ai.*,count(ao.sponser_id) as total_yateem');
			$this->db->from('ah_sponser_info ai');
			$this->db->join('assigned_orphan ao','ao.sponser_id = ai.sponser_id','left');
			$this->db->where('ai.sponser_id',$id);
			$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			//echo "if";
			return $return = $query->row();
		}
		else{
			//echo "else";
		}
	}
	
	
	function getBroSisNames($id,$type){
			$this->db->select('*');
			$this->db->from('ah_yateem_brothers_sisters as abs');
			$this->db->where('yateem_id',$id);
			$this->db->where('delete_record','0');
			$this->db->where('br_sis_type',$type);
			
			$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
	function delete_yateem($id){
			
		$json_data	=	json_encode(array('record'=>'delete','orpahn_id'	=>	$id));
		
		$data	=	array('delete_record'=>'1');
		
		$this->db->where('orphan_id',$id);

		$this->db->update('ah_yateem_info',$json_data,$this->session->userdata('userid'),$data);
		
	
	}
	
	function delete_kafeel($id){
			
		$json_data	=	json_encode(array('record'=>'delete','sponser_id'	=>	$id));
		
		$data	=	array('delete_record'=>'1');
		
		$this->db->where('sponser_id',$id);

		$this->db->update('ah_sponser_info',$json_data,$this->session->userdata('userid'),$data);
		
	
	}
	function getParentsData($id,$type){
			$this->db->select('*');
			$this->db->from('ah_yateem_parents as ayp');
			$this->db->where('ah_yateem_id',$id);
			$this->db->where('parent_relationship',$type);
			$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
	
		function getBanksData($id){
			$this->db->select('*');
			$this->db->from('ah_yateem_banksinfo  AS ab');
			$this->db->where('yateem_id',$id);
			$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
	
	function getOthersData($id){
	$this->db->select('*');
	$this->db->from('ah_yateem_other_data  AS ao');
	$this->db->where('yateem_id',$id);
	$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
	
	
	function getYateemDataById($id){
	$this->db->select('*');
	$this->db->from('ah_yateem_info');
	$this->db->where('orphan_id',$id);
	$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
	
	function getYateemFullDetail($orphanid)
	{
		$this->db->select('ah_yateem_info.orphan_name, ah_yateem_info.orphan_id, ah_yateem_info.orphan_picture, ah_yateem_info.date_birth,
		ah_yateem_info.home_number, ah_yateem_info.phone_number, country.list_name AS CountryName, city.list_name AS CityName');
        $this->db->from('ah_yateem_info');
		$this->db->join('ah_listmanagement AS country','country.list_id = ah_yateem_info.country_id');
		$this->db->join('ah_listmanagement` AS city ','city.list_id = ah_yateem_info.city_id');	
		$this->db->where('ah_yateem_info.orphan_id',$orphanid);		
		$query = $this->db->get();		
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
	
	
	function get_sponser_list()
	{
		$this->db->select('ai.*,count(ao.sponser_id) as sponserCount');
        $this->db->from('ah_sponser_info as ai');
		$this->db->join('assigned_orphan ao','ao.sponser_id = ai.sponser_id','left');
		$this->db->where('ai.delete_record','0');
		$this->db->order_by("ai.sponser_id", "DESC");
		$this->db->group_by("ai.sponser_id");
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
	
	

//-------------------------------------------------------------------

	/*
	 *  Get ALL Types
	 *  return ARRAY
	 */
	function get_listmanagment_types()
	{
		$query = $this->db->query("SHOW COLUMNS FROM ".$this->_table_listmanagement." LIKE 'list_type'");
		$result = $query->row();
		
		if(!empty($result))
		{
			 return $option_array = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $result->Type));
		}
	}
//-------------------------------------------------------------------

	/*
	 *  Get ALL Types
	 *  return ARRAY
	 */	
	function get_sub_category($provinceid,$willayaid	=	NULL)
	{
		$this->db->select('list_id,list_name,list_type,list_parent_id');
		$this->db->from($this->_table_listmanagement);
		$this->db->where('delete_record','0');	
		$this->db->where('list_parent_id',$provinceid);
		$this->db->order_by("list_order", "ASC"); 
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$dropdown  = '<label>Select a Willaya:&nbsp;';
			$dropdown .= '<select name="wiliayaid" id="wiliayaid" placeholder="البرنامج المطلوب‎">';
			$dropdown .= '<option value="">البرنامج المطلوب‎</option>';
		
			foreach($query->result() as $row)
			{
				$dropdown .= '<option value="'.$row->list_id.'" ';
					
				if($willayaid !="")
				{
					if($willayaid	==	$row->list_id)
					{
						$dropdown .= 'selected="selected"';
					}
				}
						
						$dropdown .= '>'.$row->list_name.'</option>';
			}
				$dropdown .= '</select>';
				$dropdown .= '</label>';
				return $dropdown;
					
		}
		else
		{
			return 'No record is available;';
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Province Name
	 * Return OBJECT
	 */
		
	function get_willaya_province_name($id)
	{
		$this->db->select('list_name');
		$this->db->where('list_id',$id);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_listmanagement);
		
		if($query->num_rows() > 0)
		{
			return $query->row()->list_name;
		}

	}
//-------------------------------------------------------------------	
	function get_dropbox_list_value($listtype='', $parent=0)
	{
		$this->db->select('list_id,list_name,list_type,list_other');
		$this->db->from($this->_table_listmanagement);	
		if($parent!='' && $parent!=0)
		{	$this->db->where('list_parent_id',$parent);	 }
		
		if($listtype!='' && $listtype!='0' && $listtype!='subcategory')
		{	$this->db->where('list_type',$listtype);	}
		
			$this->db->where('delete_record','0');
			$this->db->order_by("list_order", "ASC"); 			
			$query = $this->db->get();
			return $query->result();
	}
	
	
//-----------------------------------------------------------------	
	function get_name_from_list($id)
	{	
		$this->db->select('list_name');
		$this->db->where('list_id',$id);
		$this->db->where('delete_record','0');
		$this->db->from($this->_table_listmanagement);	 			
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row()->list_name;
		}
	}	
//-------------------------------------------------------------------	
	function get_type_name($listype)
	{
		$lang_code = $this->session->userdata('lang_code');
		$type_all_list = $this->config->item('list_types');
		$list_type = $type_all_list[$listype];		
		return $list_type[$lang_code];
	}
//-------------------------------------------------------------------
	/*
	* Checking drop box for all list type 
	* Return complete dropbox with input field
	*/	
	function create_dropbox_list($name,$listtype,$selectedvalue='0',$parent='0',$isrequired='',$othervalue='',$index='')
	{		
		$heading = $this->get_type_name($listtype);
		if($index!='')
		{	$dropdown = '<select onChange="check_other_value(this);" name="'.$name.'[]" id="'.$name.$index.'" class="form-control '.$isrequired.'" placeholder="'.$heading.'">';	}
		else
		{	$dropdown = '<select onChange="check_other_value(this);" name="'.$name.'" id="'.$name.'" class="form-control '.$isrequired.'" placeholder="'.$heading.'">'; 		}
		$dropdown .= '<option value="">'.$heading.'</option>';		
		foreach($this->get_dropbox_list_value($listtype,$parent) as $row)
		{
				$dropdown .= '<option dataid="'.$row->list_other.'" value="'.$row->list_id.'" ';
				if($selectedvalue==$row->list_id)
				{	$dropdown .= 'selected="selected"';	}
				$dropdown .= '>'.$row->list_name.'</option>';
		}
		$dropdown .= '</select>';
		if($index!='')
		{	$dropdown .= '<input name="'.$name.'_text[]" id="'.$name.'_text'.$index.'" value="'.$othervalue.'" placeholder="'.$heading.'" type="text" class="otherinput form-control">';	}
		else
		{	$dropdown .= '<input name="'.$name.'_text" id="'.$name.'_text" value="'.$othervalue.'" placeholder="'.$heading.'" type="text" class="otherinput form-control">';	}
		return $dropdown;
	}

//-------------------------------------------------------------------
		/*
		* Checking view permission 
		* Return permission status 0,1
		*/
		function check_view_permission($moduleid)
		{
			$this->db->select('permissionjson');
			$this->db->from('ah_users');
			$this->db->where('userid',$this->_login_userid);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				$permissionResult = $query->row();
				$permission = json_decode($permissionResult->permissionjson,TRUE);
				$perm = $permission[$moduleid];
				return $perm['v'];				
			}
			else
			{
			   return 0;
			}
		}
//-------------------------------------------------------------------
	/*
	* Creating Parent Menu 
	* Return Parent menu html 
	*/
	function parentMenu()
	{
		$this->db->select('moduleid,module_name,module_icon,module_controller,module_parent');
		$this->db->from('mh_modules');
		$this->db->where('module_parent',0); 
		$this->db->where('module_status','A');
		$this->db->order_by("module_order", "ASC");
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{		
			$html = '<nav class="quick-launch-bar">';
			$html .= '<ul>';
			foreach($query->result() as $parentMenu)
			{				
				if($this->check_view_permission($parentMenu->moduleid)==1)
                {
                    $html .= '<li><a href="'.base_url().$parentMenu->module_controller.'"><i class="'.$parentMenu->module_icon.'"></i><span>'.$parentMenu->module_name.'</span></a></li>';
                }				
			}
			$query->free_result();
			$html .= '</ul>';
			$html .= '</nav>';
			echo $html;
		}
	}
//-------------------------------------------------------------------
	/*
	* Creating Child Menu 
	* Return child menu html
	* Parameter parent module id 
	*/
	function childMenu($parentid)
	{		
		$this->db->select('moduleid,module_parent,module_name,module_icon,module_controller');
		$this->db->from('mh_modules');
		$this->db->where('module_parent',$parentid); 
		$this->db->where('module_status','A');
		$this->db->where('show_on_menu','1');
		$this->db->order_by("module_order", "ASC");
		$childQuery = $this->db->get();		
		if($childQuery->num_rows() > 0)
		{	$childMenux = '';
			foreach($childQuery->result() as $childMenu)
			{
                if($this->check_view_permission($childMenu->moduleid)==1)
                {
					if($childMenu->moduleid==34)
					{
						$childMenux .= '<a class="add re_design_a" href="#globalDiag" onClick="alatadad(this);" data-url="'.base_url().'lease_programe/add_data_for_barnamij/" id="0" data-icon="'.$childMenu->module_icon.'" data-heading="'.$childMenu->module_name.'" data-color="#4096EE"><i class="'.$childMenu->module_icon.' resizeicon"><br><span class="resizeheading">'.$childMenu->module_name.'</span></i></a>';
					}
					
					if($childMenu->moduleid==2)
					{
						$childMenux .= '<a class="add re_design_a" href="#globalDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment/addlistingview/" id="0" data-icon="'.$childMenu->module_icon.'" data-heading="'.$childMenu->module_name.'" data-color="#4096EE"><i class="'.$childMenu->module_icon.' resizeicon"><br><span class="resizeheading">'.$childMenu->module_name.'</span></i></a>';
					}
					else
					{
						$childMenux .= '<a href="'.base_url().$childMenu->module_controller.'" class="add re_design_a"> <i class="'.$childMenu->module_icon.' resizeicon"><br><span class="resizeheading">'.$childMenu->module_name.'</span></i></a>';
					}
					
				}
			}			
		}
		return $childMenux;
	}
//-------------------------------------------------------------------
	/**
	 * Get All Branches
	 *
	 * @access	public
	 * @return	object
	 */
	function get_all_branches()
	{
		$this->db->select('`ah_branchs`.`branchid`,`ah_branchs`.`branchname`,`ah_branchs`.`branchaddress`,`ah_listmanagement`.`list_name`,`ahl1`.`list_name` AS wilaya');
		$this->db->from('ah_branchs');
		$this->db->join('ah_listmanagement','`ah_listmanagement`.`list_id` = `ah_branchs`.`provinceid`','left');
		$this->db->join('ah_listmanagement AS ahl1','`ahl1`.`list_id` = `ah_branchs`.`wiliayaid`','left');
		$this->db->where('`ah_branchs`.`branchstatus`','1');
		$this->db->where('`ah_listmanagement`.`delete_record`','0');
		$this->db->order_by("`ah_branchs`.`branchname`", "ASC");
		return $q->result();		
	}
//-------------------------------------------------------------------
	/**
	 * Get Module Inforamtion
	 *
	 * @access	public
	 * @return	object
	 */
	function get_module($firstURL='',$secondURL='')
	{		
		$this->db->select('moduleid,module_parent,module_name,module_text,module_controller,module_icon');
		$this->db->from('mh_modules');
        if($firstURL=='')
		{	$firstURL = $this->uri->segment(1); 	}
        
		if($secondURL=='')
		{	$secondURL = $this->uri->segment(2);	}
		
        if($firstURL!='' && $secondURL=='')
        {	$this->db->where('module_controller',$firstURL);	}
        else if($secondURL!='')
        {   $fullURL = $firstURL.'/'.$secondURL;
            $this->db->like('module_controller',$fullURL);	}
			
            $this->db->where('module_status','A');
			$this->db->limit(1);
			$query = $this->db->get();			
			foreach($query->result() as $menuData)
			{
				$ppx['moduleid'] = $menuData->moduleid;
				if($menuData->module_parent==0)
				{	$ppx['module_parent'] = $menuData->moduleid;	}
				else
				{	$ppx['module_parent'] = $menuData->module_parent;	}
				$ppx['module_name'] = $menuData->module_name;
				$ppx['module_text'] = $menuData->module_text;
				$ppx['module_controller'] = $menuData->module_controller;
				$ppx['module_icon'] = $menuData->module_icon;
			}			
			$query->free_result();
			//Child Menu Results 
			$this->db->select('moduleid,module_parent,module_name,module_icon,module_controller');
			$this->db->from('mh_modules');
			$this->db->where('module_parent',$menuData->moduleid); 
			$this->db->where('module_status','A');
			$this->db->where('show_on_menu','1');
			$this->db->order_by("module_order", "ASC");
			$childQuery = $this->db->get();	
			$ppx['childMenus'] = $childQuery->result();	
			
			return $ppx;
	}
//-------------------------------------------------------------------	
	/**
	 * Get Module Inforamtion
	 *
	 * @access	public
	 * @return	object
	 */
	function get_module_name_icon($id)
	{
		$this->db->select('moduleid,module_name,module_icon');
		$this->db->from('mh_modules');
        $this->db->where('moduleid',$id);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->row();		
	}
//-------------------------------------------------------------------
	/**
	 * Checking Permission
	 *
	 * @access	public
	 * @return	object
	 */	
 	function check_permission($moduledata,$permtype='v')
    {       
        $moduleid = $moduledata['moduleid'];		  
        $this->db->select('permissionjson');
        $this->db->from('ah_users');
        $this->db->where('userid',$this->_login_userid);
        $query = $this->db->get();
		$userDetail = $this->get_user_detail($this->_login_userid);
		$url_to_go = base_url().$userDetail['profile']->module_controller;		
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $sp)
            {
                $perm = json_decode($sp->permissionjson,TRUE);
                $newperm = $perm[$moduleid];
                if($newperm['v']==0 && $permtype=='v')
                {	redirect($url_to_go);
                }
                else
                {	return $newperm[$permtype];	}	//It will return 0 or 1
            }
        }
        else
        {
            redirect(base_url());
        }
    }
//-------------------------------------------------------------------	
	/**
	 * Checking Other Permission
	 *
	 * @access	public
	 * @return	0/1
	 */	
 	function check_other_permission($moduleid,$type)
    {
        $this->db->select('permissionjson');
        $this->db->from('ah_users');
        $this->db->where('userid',$this->_login_userid);
        $query = $this->db->get();			
        if($query->num_rows() > 0)
        {  	$permResult = $query->row();
			$perm = json_decode($permResult->permissionjson,TRUE);
			$newperm = $perm[$moduleid];			
			return $newperm[$type];//It will return 0 or 1			
        }
        else
        {
           return 0;
        }
    }
//-------------------------------------------------------------------
	/**
	 * Checking Count from list management
	 *
	 * @access	public
	 * @return	number of count
	 */	
 	function dataCountFromList($type=0,$parent=0)
    {
        $this->db->select('list_id');
        $this->db->from('ah_listmanagement');
		if($type!=0 && $type!='')
		{	$this->db->where('list_type',$type);	}
		
		if($parent!=0 && $parent!='')
		{	$this->db->where('list_parent_id',$parent);	}
		
		$this->db->where('delete_record','0');
		
		$query = $this->db->get();
		return $query->num_rows();
    }
//-------------------------------------------------------------------
	/**
	 * Checking Count from list management
	 *
	 * @access	public
	 * @return	number of count
	 */		
	function module_dropbox_permission($name,$value)
    {
        $this->db->select('module_controller,module_name,moduleid');
        $this->db->from('mh_modules');
        $this->db->where('show_on_menu','1');
		$this->db->where('module_controller !=','#');
        $this->db->order_by("module_parent", "ASC");
		if($value!='')
		{	$valuex = $value; }
		else
		{	$valuex = 8; }
            $query = $this->db->get();
            $dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req" placeholder="صفحة الترحيب" >';
           
            foreach($query->result() as $row)
            {
                $dropdown .= '<option value="'.$row->moduleid.'" ';
                if($valuex==$row->moduleid)
                {
                    $dropdown .= 'selected="selected"';
                }
                $dropdown .= '>'.$row->module_name.'</option>';
            }
            $dropdown .= '</select>';
            echo($dropdown);
      }
//-------------------------------------------------------------------
	/**
	 * Checking Count from list management
	 *
	 * @access	public
	 * @return	number of count
	 */	
	 function module_dropbox($name,$value)
		{
			$this->db->select('moduleid,module_name');
			$this->db->from('mh_modules');
			$this->db->where('module_parent','0');
			$this->db->order_by("module_order", "ASC");
				$query = $this->db->get();
				$dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req" placeholder="اختر القسم" >';
				$dropdown .= '<option value="0">اختر القسم</option>';
				foreach($query->result() as $row)
				{
					$dropdown .= '<option value="'.$row->moduleid.'" ';
					if($value==$row->moduleid)
					{
						$dropdown .= 'selected="selected"';
					}
					$dropdown .= '>'.$row->module_name.'</option>';
				}
				$dropdown .= '</select>';
				echo($dropdown);
			
		}
//-------------------------------------------------------------------
	/**
	 * Activity Monitor of user
	 *
	 * @access	public
	 * @return	void
	 */	
	function activity_monitor($userid,$logtype)
	{
		$logArray = array('userid'=>$userid,'acttype'=>$logtype,'logip'=>$_SERVER['REMOTE_ADDR'],'logagent'=>$_SERVER['HTTP_USER_AGENT']);
		$this->db->insert($this->config->item('table_ahul'),$logArray);		
	}
//-------------------------------------------------------------------
	/**
	 * Module List
	 *
	 * @access	public
	 * @return	void
	 */	
	function allModule($parentid=0)
    {
        $this->db->select('moduleid,module_name,module_icon');
        $this->db->from('mh_modules');
        if($parentid!='' && $parentid!='0')
		{	$this->db->where('module_parent',$parentid);	}
		else
		{	$this->db->where('module_parent',0);
        	$this->db->where('module_status','A');			}
			$this->db->order_by("module_order", "ASC");
			$query = $this->db->get();
			return $query->result();
    }
//-------------------------------------------------------------------	
	/**
	 * Uploading Photos
	 *
	 * @access	public
	 * @return	photo name
	 */	
	function upload_file($filefield,$folderpath,$thumb=false,$w=0,$h=0)
	{		
		if (!is_dir($folderpath))
		{	mkdir($folderpath, 0777, true);	}
		
		$config['upload_path'] = $folderpath;
		$config['allowed_types'] = '*';
		$config['max_size']	= '5000';
		$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload($filefield))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
//-------------------------------------------------------------------
	/**
	 * Checking Count from tables
	 *
	 * @access	public
	 * @return	number of count
	 */	
 	function dataCount($table,$column,$value,$type,$sumcolumn=0)
    {
        switch($type)
		{
			case 'SUM';
				$SUMQuery = $this->db->query("SELECT SUM(".$sumcolumn.") as sum FROM `".$table."` WHERE `".$column."`='".$value."'");
				$SUMResult = $SUMQuery->row();
				return $SUMResult->sum;
			break;
			
			case 'COUNT';
				$SUMQuery = $this->db->query("SELECT ".$column." FROM `".$table."` WHERE `".$column."`='".$value."'");				
				return $SUMQuery->num_rows;
			break;
		}	
    }
//----------------------------------------------------------------------
	/*
	* Insert Banner Record
	* @param array $data
	* return True
	*/
	function add_banner($data)
	{
		$this->db->insert('system_images',$data);

		return TRUE;
	}
//-------------------------------------------------------------
	public function get_banner_detail($image_id)
	{
		$this->db->where('imageid',$image_id);
		
		$query = $this->db->get('system_images');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}	
	}
//--------------------------------------------------------------------------	
	public function update_status($image_id,$data)
	{
		$json_data	=	json_encode(array('record'=>'delete','imageid'	=>	$image_id));
		
		$data	=	array('delete_record'=>'1');
		
		$this->db->where('imageid', $image_id);

		$this->db->update('system_images',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------
	public function update_banner($image_id,$data)
	{
		$json_data	=	json_encode(array('data'=>$data));
		
		$this->db->where('imageid',$image_id);
		$this->db->update('system_images',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
	
	public function get_multi_select_box($name,$case,$type='')
	{
		switch($case)
		{	
			case 'Branch';				
				$query = $this->db->query("SELECT branchid,branchname FROM ah_branchs ORDER by branchname ASC;");
				foreach($query->result() as $nurn)
				{
					$html .= '<li class="checklist"><input type="checkbox" name="'.$name.'[]" value="'.$nurn->branch_id.'"> '.$nurn->branch_name.'</li>';
				}
				return array('c'=>arabic_date($query->num_rows()),'h'=>$html);
			break;
			/*case 'Province';
				$query = $this->db->query("SELECT REIGONID, REIGONNAME FROM election_reigons ORDER by REIGONNAME ASC;");
				foreach($query->result() as $nurn)
				{
					$html .= '<li class="checklist"><input class="electionreigons" type="checkbox" name="'.$name.'[]" value="'.$nurn->REIGONID.'"> '.$nurn->REIGONNAME.'</li>';
				}
				return array('c'=>arabic_date($query->num_rows()),'h'=>$html);
			break;*/
			case 'FirstList';
				$query = $this->db->query("SELECT list_id, list_name FROM ah_listmanagement WHERE list_type='".$type."' AND list_status='1' ORDER by list_order ASC;");
				foreach($query->result() as $nurn)
				{
					$html .= '<li class="checklist"><input type="checkbox" name="'.$name.'[]" value="'.$nurn->list_id.'"> '.$nurn->list_name.'</li>';
				}
				return array('c'=>arabic_date($query->num_rows()),'h'=>$html);
			break;
			case 'SMSTemplate';
				$query = $this->db->query("SELECT templatesubject, template FROM system_sms_template WHERE templatestatus='1' ORDER by templatesubject ASC;");
				foreach($query->result() as $nurn)
				{
					$html .= '<li class="checklist" onClick="setTemplateInBulk(\''.$nurn->template.'\');">'.$nurn->templatesubject.'</li>';
				}
				return array('c'=>arabic_date($query->num_rows()),'h'=>$html);
			break;
		}
	}
	
//-------------------------------------------------------------------
	/*
	 * Get All Bank Accounts Detail
	 * Return OBJECT
	 */
		
	function get_accnumber_by_id($accountid)
	{
		$this->db->select('accountnumber');
		$this->db->where('bankaccountid',$accountid);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_users_bankaccount);
		
		if($query->num_rows() > 0)
		{
			return $query->row()->accountnumber;
		}

	}
/*-------------------------------------------------------------------------*/		
/* End of file haya_model.php */
/* Location: ./appliction/modules/haya/haya_model.php */

	function get_all_yateem_list()
	{
		$q = $this->db->query("SELECT ya.orphan_id, ya.orphan_name, ya.orphan_picture, ya.date_birth, al.`list_name`, (SELECT COUNT(assign_id) FROM assigned_orphan WHERE orphan_id=ya.`orphan_id`) AS kafeelcount FROM ah_yateem_info AS ya, ah_listmanagement AS al WHERE ya.`country_id`=al.`list_id` ORDER BY kafeelcount ASC");
		return $q->result();
	}

}

?>
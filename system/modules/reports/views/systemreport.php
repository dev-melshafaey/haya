<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="nav nav-tabs panel panel-default panel-block">
          <div class="col-md-12">
            <h4 class="text-warning">أولا: الوضع الإجمالي لتمويل القروض ./……../ …… 2014 م</h4>
          </div>
          <div class="col-md-12 setMarginx">
            <div class="list-group-item ">
              <h4 class="section-title">1-	إجمالي الطلبات والموافقات والقروض المعتمدة:-</h4>
              <div class="form-group">
                <table class="table table-bordered table-striped dataTable no-footer">
                  <thead>
                    <tr>
                      <th class="reporthead">البيان</th>
                      <th class="reporthead">قل من 20 ألف ر.ع</th>
                      <th class="reporthead">بين 20 و 100 ألف ر.ع</th>
                      <th class="reporthead">الإجمالي</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>طلبات القروض</td>
                      <td>22</td>
                      <td>64</td>
                      <td>86<!--<span class="label label-sm label-success">Approved</span>--></td>
                    </tr>
                    <tr>
                      <td>الموافقات الأولية على القروض</td>
                      <td>55</td>
                      <td>123</td>
                      <td>178<!--<span class="label label-sm label-info">Pending</span>--></td>
                    </tr>
                    <tr>
                      <td>نسبة الموافقة مقارنة بالطلبات(%)</td>
                      <td>78</td>
                      <td>88</td>
                      <td>166<!--<span class="label label-sm label-warning">Suspended</span>--></td>
                    </tr>
                    <tr>
                      <td>المبالغ المعتمدة</td>
                      <td>44</td>
                      <td>32</td>
                      <td>76<!--<span class="label label-sm label-danger">Blocked</span>--></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-12 setMarginx">
            <div class="list-group-item ">
              <h4 class="section-title">2-توزيع الموافقات حسب القطاعات الاقتصادية:-</h4>
              <div class="form-group">
                <table class="table table-bordered table-striped dataTable no-footer">
                  <thead bgcolor="#FBD4B4">
                    <tr>
                      <th class="reporthead">القطاعات الاقتصادية</th>
                      <th class="reporthead">الموافقات الأولية على القروض</th>
                      <th class="reporthead">النسبة المئوية</th>
                      <th class="reporthead">المبالغ المعتمدة</th>
                      <th class="reporthead">النسبة المئوية</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>القطاع التجاري</td>
                      <td>654</td>
                      <td>54%</td>
                      <td>1108</td>
                      <td>90%</td>
                    </tr>
                    <tr>
                      <td>القطاع الخدمي</td>
                      <td>77</td>
                      <td>88%</td>
                      <td>165</td>
                      <td>10%</td>
                    </tr>
                    <tr>
                      <td>القطاع الصناعي</td>
                      <td>23</td>
                      <td>55%</td>
                      <td>78<!--<span class="label label-sm label-warning">Suspended</span>--></td>
                      <td>20%</td>
                    </tr>
                    <tr>
                      <td>القطاع الحرفي</td>
                      <td>32</td>
                      <td>14%</td>
                      <td>146<!--<span class="label label-sm label-danger">Blocked</span>--></td>
                      <td>40%</td>
                    </tr>
                    <tr>
                      <td>القطاع السياحي</td>
                      <td>63</td>
                      <td>79%</td>
                      <td>142<!--<span class="label label-sm label-danger">Blocked</span>--></td>
                      <td>23%</td>
                    </tr>
                    <tr>
                      <td>القطاع الزراعي والحيواني والسمكي</td>
                      <td>103</td>
                      <td>14%</td>
                      <td>117<!--<span class="label label-sm label-danger">Blocked</span>--></td>
                      <td>55%</td>
                    </tr>
                    <tr>
                      <td>قطاع النقل</td>
                      <td>222</td>
                      <td>65%</td>
                      <td>876<!--<span class="label label-sm label-danger">Blocked</span>--></td>
                      <td>67%</td>
                    </tr>
                    <tr>
                      <td>الإجمالي</td>
                      <td>126</td>
                      <td>78%</td>
                      <td>915</td>
                      <td>76%</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-12 setMarginx">
            <div class="list-group-item ">
              <h4 class="section-title">3-	إجمالي الموافقات والمبالغ المعتمدة حسب البرامج التمويلية:-</h4>
              <div class="form-group">
                <table class="table table-bordered table-striped dataTable no-footer">
                  <thead bgcolor="#FBD4B4">
                    <tr>
                      <th rowspan="2" class="reporthead">البرنامج التمويلي</th>
                      <th colspan="4" class="reporthead">الموافقات الأولية</th>
                      <th colspan="4" class="reporthead">المبالغ المعتمدة</th>
                    </tr>
                    <tr>
                      <th class="reportsubhead">مشاريع تأسيس</th>
                      <th class="reportsubhead">مشاريع تطوير</th>
                      <th class="reportsubhead">مشروع شراء</th>
                      <th class="reportsubhead">الإجمالي</th>
                      <th class="reportsubhead">مشاريع تأسيس</th>
                      <th class="reportsubhead">مشاريع تطوير</th>
                      <th class="reportsubhead">مشروع شراء</th>
                      <th class="reportsubhead">الإجمالي</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>مورد</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                    </tr>
                    <tr>
                      <td>تأسيس</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                    </tr>
                    <tr>
                      <td>ريادة</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                    </tr>
                    <tr>
                      <td>تعزيز</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                    </tr>
                    <tr>
                      <td>الإجمالي</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-12 setMarginx">
            <div class="list-group-item ">
              <h4 class="section-title">4-	إجمالي الموافقات والمبالغ المعتمدة حسب نوع المشاريع:-</h4>
              <div class="form-group">
                <table class="table table-bordered table-striped dataTable no-footer">
                  <thead>
                    <tr>
                      <th rowspan="2" class="reporthead">المؤشرات</th>
                      <th rowspan="2" class="reporthead">عدد الموافقات</th>
                      <th colspan="3" class="reporthead">المستفيدين</th>
                      <th rowspan="2" class="reporthead">المبالغ المعتمدة</th>
                    </tr>
                    <tr>
                      <th class="reportsubhead">ذكور</th>
                      <th class="reportsubhead">إناث</th>
                      <th class="reportsubhead">الإجمالي</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>المؤشرات
                        
                        المشاريع الفردية</td>
                      <td>1</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                      <td>8</td>
                    </tr>
                    <tr>
                      <td>المؤشرات المشاريع الفردية</td>
                      <td>1</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                      <td>8</td>
                    </tr>
                    <tr>
                      <td>المشاريع المشتركة</td>
                      <td>1</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                      <td>8</td>
                    </tr>
                    <tr>
                      <td>الإجمالي </td>
                      <td>1</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                      <td>8</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-12 setMarginx">
            <div class="list-group-item ">
              <h4 class="section-title">5-	المشاريع الممولة فعليا والتي تم صرف دفعاتها:-</h4>
              <div class="form-group">
                <table class="table table-bordered table-striped dataTable no-footer">
                  <thead>
                    <tr>
                      <th rowspan="2" class="reporthead">البرنامج التمويلي</th>
                      <th rowspan="2" class="reporthead">عدد المشاريع</th>
                      <th colspan="3" class="reporthead">فرص العمل المستحدثة بما فيها إجمالي المشاريع</th>
                      <th rowspan="2" class="reporthead">المبالغ المصروفة فعليا</th>
                    </tr>
                    <tr>
                      <th class="reportsubhead">ذكور</th>
                      <th class="reportsubhead">إناث</th>
                      <th class="reportsubhead">إجمالي</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>مورد</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                    </tr>
                    <tr>
                      <td>تأسيس</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>7</td>
                    </tr>
                    <tr>
                      <td>ريادة</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>7</td>
                    </tr>
                    <tr>
                      <td>تعزيز</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>7</td>
                    </tr>
                    <tr>
                      <td>الإجمالي</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>7</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="nav nav-tabs panel panel-default panel-block">
          <div class="col-md-12">
            <h4 class="text-warning">ثانيا: النشاط التمويلي حسب المحافظات خلال ................ للعام 2014 م</h4>
          </div>
          <div class="col-md-12 setMarginx">
            <div class="list-group-item ">
              <h4 class="section-title">1-	إجمالي الطلبات والموافقات والمبالغ المعتمدة حسب المحافظات:</h4>
              <div class="form-group">
                <table class="table table-bordered table-striped dataTable no-footer">
                  <thead bgcolor="#FBD4B4">
                    <tr>
                      <th rowspan="2" class="reporthead">المحافظة</th>
                      <th colspan="3" class="reporthead">طلبات القروض</th>
                      <th colspan="3" class="reporthead">الموافقات</th>
                      <th colspan="3" class="reporthead">نسب الموافقات مقارنة بالطلبات</th>
                      <th colspan="3" class="reporthead">المبالغ المعتمدة</th>
                    </tr>
                    <tr>
                      <th class="reportsubhead">فوق 20 ألف</th>
                      <th class="reportsubhead">تحت 20 ألف</th>
                      <th class="reportsubhead">إجمالي</th>
                      <th class="reportsubhead">فوق 20 ألف</th>
                      <th class="reportsubhead">تحت 20 ألف</th>
                      <th class="reportsubhead">إجمالي</th>
                      <th class="reportsubhead">فوق 20 ألف</th>
                      <th class="reportsubhead">تحت 20 ألف</th>
                      <th class="reportsubhead">إجمالي</th>
                      <th class="reportsubhead">فوق 20 ألف</th>
                      <th class="reportsubhead">تحت 20 ألف</th>
                      <th class="reportsubhead">إجمالي</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>مسقط</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>6</td>
                      <td>7</td>
                      <td>2</td>
                      <td>4</td>
                      <td>5</td>
                    </tr>
                    <tr>
                      <td>مسقط</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>6</td>
                      <td>7</td>
                      <td>2</td>
                      <td>4</td>
                      <td>5</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-12 setMarginx">
            <div class="list-group-item ">
              <h4 class="section-title">2-	إجمالي الموافقات والمبالغ المعتمدة حسب البرامج التمويلية:-</h4>
              <div class="form-group">
                <table class="table table-bordered table-striped dataTable no-footer">
                  <thead>
                    <tr>
                      <th rowspan="2" class="reporthead">المحافظات</th>
                      <th colspan="5" class="reporthead">الموافقات</th>
                      <th colspan="5" class="reporthead">المبالغ المعتمدة</th>
                    </tr>
                    <tr>
                      <th class="reportsubhead">مورد</th>
                      <th class="reportsubhead">تأسيس</th>
                      <th class="reportsubhead">ريادة</th>
                      <th class="reportsubhead">تعزيز</th>
                      <th class="reportsubhead">إجمالي</th>
                      <th class="reportsubhead">مورد</th>
                      <th class="reportsubhead">تأسيس</th>
                      <th class="reportsubhead">ريادة</th>
                      <th class="reportsubhead">تعزيز</th>
                      <th class="reportsubhead">إجمالي</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>مسقط</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>3</td>
                      <td>6</td>
                      <td>7</td>
                      <td>2</td>
                      <td>3</td>
                      <td>3</td>
                      <td>3</td>
                    </tr>
                    <tr>
                      <td>مسقط</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>3</td>
                      <td>6</td>
                      <td>7</td>
                      <td>2</td>
                      <td>3</td>
                      <td>3</td>
                      <td>3</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-12 setMarginx">
            <div class="list-group-item ">
              <h4 class="section-title">3-	إجمالي المشاريع و فرص العمل حسب المحافظات:-</h4>
              <div class="form-group">
                <table class="table table-bordered table-striped dataTable no-footer">
                  <thead bgcolor="#FBD4B4">
                    <tr>
                      <th rowspan="1" class="reporthead">المحافظات</th>
                      <th colspan="12" class="reporthead">المحافظات	الموافقات الأولية على القروض</th>
                      <th colspan="4" class="reporthead" rowspan="2">الإجمالي</th>
                    </tr>
                    <tr>
                      <th rowspan="3" class="reporthead"></th>
                      <th colspan="4" class="reportsubhead">مشاريع تأسيس</th>
                      <th colspan="4" class="reportsubhead">مشاريع توسعة</th>
                      <th colspan="4" class="reportsubhead">مشاريع شراء</th>
                    </tr>
                    <tr>
                      <th rowspan="2" class="reporthead">العدد</th>
                      <th colspan="3" class="reporthead">أصحاب المشاريع</th>
                      <th rowspan="2" class="reporthead">العدد</th>
                      <th colspan="3" class="reporthead">أصحاب المشاريع</th>
                      <th rowspan="2" class="reporthead">العدد</th>
                      <th colspan="3" class="reporthead">أصحاب المشاريع</th>
                      <th rowspan="2" class="reporthead">العدد</th>
                      <th colspan="3" class="reporthead">أصحاب المشاريع</th>
                    </tr>
                    <tr>
                      <th class="reportsubhead">ذكور</th>
                      <th class="reportsubhead">إناث</th>
                      <th class="reportsubhead">إجمالي</th>
                      <th class="reportsubhead">ذكور</th>
                      <th class="reportsubhead">إناث</th>
                      <th class="reportsubhead">إجمالي</th>
                      <th class="reportsubhead">ذكور</th>
                      <th class="reportsubhead">إناث</th>
                      <th class="reportsubhead">إجمالي</th>
                      <th class="reportsubhead">ذكور</th>
                      <th class="reportsubhead">إناث</th>
                      <th class="reportsubhead">إجمالي</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>مسقط</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                    </tr>
                    <tr>
                      <td>مسقط</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>7</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-12 setMarginx">
            <div class="list-group-item ">
              <h4 class="section-title">4-	المشاريع الممولة فعليا حسب المحافظات:-</h4>
              <div class="form-group">
                <table class="table table-bordered table-striped dataTable no-footer">
                  <thead bgcolor="#FBD4B4">
                    <tr>
                      <th rowspan="2" class="reporthead">المحافظات</th>
                      <th rowspan="2" class="reporthead">عدد المشاريع</th>
                      <th colspan="3" class="reporthead">فرص العمل بما فيها أصحاب المشاريع</th>
                      <th rowspan="2" class="reporthead">المبالغ المصروفة فعليا</th>
                    </tr>
                    <tr>
                      <th class="reportsubhead">ذكور</th>
                      <th class="reportsubhead">إناث</th>
                      <th class="reportsubhead">إجمالي</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>مسقط</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>3</td>
                      <td>6</td>
                    </tr>
                    <tr>
                      <td>مسقط</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>3</td>
                      <td>6</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="nav nav-tabs panel panel-default panel-block">
          <div class="col-md-12">
            <h4 class="text-warning">ثالثا: تطور النشاط التمويلي :</h4>
          </div>
          <div class="col-md-12 setMarginx">
            <div class="list-group-item ">
              <h4 class="section-title">1-	تطور النشاط التمويلي حسب برامج التمويل:-</h4>
              <div class="form-group">
                <table class="table table-bordered table-striped dataTable no-footer">
                  <thead bgcolor="#FBD4B4">
                    <tr>
                      <th rowspan="2" class="reporthead">البرنامج التمويلي</th>
                      <th colspan="3" class="reporthead">يناير</th>
                      <th colspan="3" class="reporthead">فبراير</th>
                      <th colspan="3" class="reporthead">مارس</th>
                    </tr>
                    <tr>
                      <th class="reportsubhead">عدد الطلبات</th>
                      <th class="reportsubhead">عدد الموافقات</th>
                      <th class="reportsubhead">المبالغ المعتمدة</th>
                      <th class="reportsubhead">عدد الطلبات</th>
                      <th class="reportsubhead">عدد الموافقات</th>
                      <th class="reportsubhead">المبالغ المعتمدة</th>
                      <th class="reportsubhead">عدد الطلبات</th>
                      <th class="reportsubhead">عدد الموافقات</th>
                      <th class="reportsubhead">المبالغ المعتمدة</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>مورد</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                    </tr>
                    <tr>
                      <td>تأسيس</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                    </tr>
                    <tr>
                      <td>ريادة</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                    </tr>
                    <tr>
                      <td>تعزيز</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                    </tr>
                    <tr>
                      <td>الإجمالي</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-12 setMarginx">
            <div class="list-group-item ">
              <h4 class="section-title">2-	تطور النشاط التمويلي حسب المحافظات:-</h4>
              <div class="form-group">
                <table class="table table-bordered table-striped dataTable no-footer">
                  <thead bgcolor="#FBD4B4">
                    <tr>
                      <th rowspan="2" class="reporthead">المحافظات</th>
                      <th colspan="3" class="reporthead">يناير</th>
                      <th colspan="3" class="reporthead">فبراير</th>
                      <th colspan="3" class="reporthead">مارس</th>
                      <th rowspan="2" class="reporthead">إجمالي عدد الموافقات</th>
                      <th rowspan="2" class="reporthead">إجمالي المبالغ المعتمدة</th>
                    </tr>
                    <tr>
                      <th class="reportsubhead">عدد الطلبات</th>
                      <th class="reportsubhead">عدد الموافقات</th>
                      <th class="reportsubhead">المبالغ المعتمدة</th>
                      <th class="reportsubhead">عدد الطلبات</th>
                      <th class="reportsubhead">عدد الموافقات</th>
                      <th class="reportsubhead">المبالغ المعتمدة</th>
                      <th class="reportsubhead">عدد الطلبات</th>
                      <th class="reportsubhead">عدد الموافقات</th>
                      <th class="reportsubhead">المبالغ المعتمدة</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>مسقط</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>3</td>
                      <td>7</td>
                    </tr>
                    <tr>
                      <td>مسقط</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>5</td>
                      <td>2</td>
                      <td>1</td>
                      <td>2</td>
                      <td>3</td>
                      <td>4</td>
                      <td>3</td>
                      <td>7</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
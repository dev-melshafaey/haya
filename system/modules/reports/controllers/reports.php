<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller 
{
//-------------------------------------------------------------------------------	
	/*
	* Properties
	*/
	private $_data = array();
	private $_user_info	=	array();
//-------------------------------------------------------------------------------

	/*
	* Costructor
	*/
	
	public function __construct()
	{
		parent::__construct();
		// Load Models
		$this->_data['module'] = get_module();
		$this->_data['user_info'] =	userinfo();		
		$this->load->model('reports_model', 'reports');		
	}	
	
    public function index()
    {
        check_permission($this->_data['module'],'v');
		$this->load->view('systemreport', $this->_data);
    }
	
}

?>
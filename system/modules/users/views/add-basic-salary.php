<?php $this->uri->segment(3);?>
<?php $all_accounts	=	$this->users->get_accounts_by_userid($userid);?>
<?php $payments_type	=	$this->users->get_paymenttype_list('salarypaymenttype');?>
<?php $slary_type_values	=	array();?>
<div class="row">
  <div class="row col-md-12">
    <form action="" method="POST" id="add_basic_salaray" name="add_basic_salaray">

      <div class="col-md-3 form-group">
        <label class="text-warning">مبلغ</label>
        <input type="text" class="form-control req" name="amount"  id="amount"  placeholder="مبلغ" value="<?php echo $salary_data->amount;?>" onkeyup="only_numeric(this);"/>
      </div>
      <div class="col-md-3 form-group">
        <label class="text-warning">سبب</label>
        <input type="text" class="form-control req" name="notes"  id="notes"  placeholder="سبب" value="<?php echo $salary_data->notes;?>"/>
      </div>
      <input type="hidden" name="salaryid" id="salaryid" value="<?php echo $salary_data->salaryid;?>"/>
      <input type="hidden" name="data_table_id" id="data_table_id" value="<?php echo '9';?>"/>
      <input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>"/>
      
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group  col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="submit_basic_Salary();" value="حفظ" />
    </div>
  </div>
</div>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
		});
	});
</script>
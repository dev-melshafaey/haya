<!doctype html>
<?php $userid	=	$this->uri->segment(3);?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $this->load->view('common/user-tabs'); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <p>
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <?php $permissions	=	$this->haya_model->check_other_permission(array('133'));?>
          		  <?php if($permissions[133]['a']	==	1):?>
                  	<div class="row table-header-row"> <a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url();?>users/add_document/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                  <?php endif;?>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">نوع الوثيقة</th>
                        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">بلد</th>
                        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">تاريخ الإصدار</th>
                        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">تاريخ انتهاء الصلاحية</th>
                        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view(
'common/footer',array('ajax_url'=>base_url().'users/documents_listing/'.$userid,
'columns_array'=>'{"data":"نوع الوثيقة"},
				  {"data":"بلد"},
				  {"data":"تاريخ الإصدار"},
				  {"data":"تاريخ انتهاء الصلاحية"},
				  {"data":"الإجراءات"}')); ?>
<!-- /.modal-dialog -->
</div>
</body>
</html>
<div class="row">
  <div class="row col-md-12">
    <form action="" method="POST" id="add_communication" name="add_communication" enctype="multipart/form-data" autocomplete="off">
      <div class="col-md-3 form-group">
        <label class="text-warning">نوع الاتصال</label>
        <?php echo $this->haya_model->create_dropbox_list('communicationtype','communicationtype',$communicationtype,0,'req'); ?> </div>
      <div class="col-md-3 form-group">
        <label class="text-warning">رقم الاتصالات‎</label>
        <input type="text" name="communicationvalue" id="communicationvalue" class="form-control req" placeholder="القيمة الاتصالات" value="<?php echo $communicationvalue;?>" />
      </div>
      <div class="form-group col-md-3">
        <label class="text-warning">وثيقة</label>
        <input style="border: 0px !important; color: #d09c0d;" type="file" name="communication_doc" id="communication_doc" title='وثيقة'>
      </div>
      <!--<div class="col-md-3 form-group">
        <label class="text-warning">تاريخ</label>
        <input type="text" name="addingdate" id="addingdate" class="form-control req" placeholder="القيمة الاتصالات" value="<?php echo $addingdate;?>" />
      </div>-->
      <input type="hidden" name="communicationid" id="communicationid" value="<?php echo $communicationid;?>"/>
      <input type="hidden" name="data_table_id" id="data_table_id" value="<?php echo '3';?>"/>
      <input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>"/>
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group  col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="add_communic();" value="حفظ" />
    </div>
  </div>
</div>

<?php $total_leaves		=	$this->users->get_total_leaves($user_detail['profile']->level_id);?>
<?php $total_absents	=	$this->users->get_total_absents($this->session->userdata('userid'));?>
<?php $remaining_leaves	=	($total_leaves-$total_absents);?>
<div class="row">
  <h4 class="modal-title"><span style="float: left; margin-left: 8px;">مجموع أوراق (<?php echo $total_leaves;?>) أوراق المتبقية (<?php echo $remaining_leaves;?>)</span><i style="float: right; margin-left: 8px;"></i><span>إن حضور ( <?php echo 'الدرجة:'.$user_detail['profile']->level;?> )</span></h4>
  <div class="row col-md-12">
    <form class="mws-form" method="post" action="<?php echo current_url();?>" id="user_leave_request" name="user_leave_request" enctype="multipart/form-data">
      <div class="col-md-4 form-group">
        <label class="text-warning">سبب</label>
        <select name="reason" id="reason" class="form-control req">
          <option value="">أنواع السبب</option>
          <?php foreach($this->haya_model->get_leave_types() as $value):?>
          <option value="<?php echo $value;?>" <?php  echo($reason	==	$value ? 'selected="selected"' : NULL); ?>><?php echo $value;?></option>
          <?php endforeach;?>
        </select>
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ البدء</label>
        <input type="text" name="start_date" id="start_date" class="datepicker form-control req" placeholder="تاريخ البدء‎" value="<?php if($start_date){ echo date('Y-m-d',strtotime($start_date)); }?>" />
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ الانتهاء</label>
        <input type="text" name="end_date" id="end_date" class="datepicker form-control req" placeholder="تاريخ الانتهاء" value="<?php if($end_date){ echo date('Y-m-d',strtotime($end_date)); }?>" />
      </div>
     <!-- <div class="form-group  col-md-4" >
        <label class="text-warning">الجنس</label>
        <select name="gender" class="form-control req">
          <option value="">الجنس</option>
          <option value="ذكر" <?php if($gender	==	'ذكر'):?>selected="selected"<?php endif;?> >ذكر</option>
          <option value="أنثى" <?php if($gender	==	'أنثى'):?>selected="selected"<?php endif;?> >أنثى</option>
        </select>
      </div> -->
      <div class="form-group col-md-4">
        <label class="text-warning">وثيقة</label>
        <input style="border: 0px !important; color: #d09c0d;" type="file" name="leave_application" id="leave_application" title='وثيقة'>
      </div>
      <br clear="all" />
      <div class="col-md-8 form-group">
        <label class="text-warning">ملاحظات</label>
        <textarea tabindex="8" class="form-control" name="notes" id="notes" style="margin: 0px; height: 177px; width: 763px;"><?php echo $notes;?></textarea>
      </div>
      <div class="col-md-12 form-group">
            <label for="basic-input"></label>
            <input type="hidden" name="userid" id="userid" value="<?php echo $this->session->userdata('userid');?>"/>
            <input type="hidden" name="leaveid" id="leaveid" value="<?php echo $leaveid;?>"/>
            <input type="hidden" name="manager_id" id="manager_id" value="<?php echo $manager_id;?>"/>
            <input type="hidden" name="leave_application_old" id="leave_application_old" value="<?php echo $leave_application;?>"/>
        <?php if($remaining_leaves	==	0 || $remaining_leaves < 0):?>
        لا يمكنك تطبيق لطلب إجازة لأن برنامجك مجموع الأوراق وقد تم الانتهاء
        <?php else:?>
        	<input type="button" value="Submit" name="submit" class="btn btn-success mws-login-button" onclick="add_leave();"/>
		<?php endif;?>
      </div>
    </form>
  </div>
</div>
<script>
$(function(){
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});
function add_leave()
{
	//alert('fsafasdf'); return;
	check_my_session();
	$('#user_leave_request .req').removeClass('parsley-error');
    $('#user_leave_request .req').each(function (index, element) {
        if ($.trim($(this).val()) == '') {
            $(this).addClass('parsley-error');
        }
    });
    var len = $('#user_leave_request .parsley-error').length;

	if(len<=0)
	{
		var str_data	=	$('#user_leave_request').serialize();
		var fd 			=	new FormData(document.getElementById("user_leave_request"));
		
		var request = $.ajax({
		  url: config.BASE_URL+'users/add_leave_request',
		  type: "POST",
		  data: fd,
		  enctype: 'multipart/form-data',
		  dataType: "html",
		  processData: false,  // tell jQuery not to process the data
          contentType: false ,  // tell jQuery not to set contentType
		  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(msg)
		  {
			  //window.location.href = msg;
			  
		     $('#addingDiag').modal('hide');	  
			 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
			 $('.dataTable').dataTable().fnDestroy();
			 $('#ajax_action').hide();	
			 create_data_table(1);
		  }
		});
	}
}
</script>
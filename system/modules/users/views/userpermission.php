<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
<?php $this->load->view('common/logo'); ?>
<?php $this->load->view('common/usermenu'); ?>
<?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
<?php $this->load->view('common/quicklunchbar'); ?>
<div class="row">
  <div class="col-md-12">
    <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
    <!----------------->
    <form name="frm_user_permission" id="frm_user_permission" method="post" autocomplete="off"> 
    <input type="hidden" id="user" name="user[]" value="">   
      <div class="col-md-7">
        <div class="panel panel-default panel-block">
         <div class="list-group-item" id="checkboxes-and-radios">
              <h4 class="section-title preface-title text-warning" >اسم وحدة <span id="all_box"><input type="checkbox" id="selectall" /> تحديد كافة </span></h4>
              
            </div>
            <div class="row col-md-12 first_row">
                 	<div class="row col-md-6">&nbsp;</div>
                    <div class="row col-md-2">مشاهده <input type="checkbox" id="view" class="allmydata" /></div>
                    <div class="row col-md-2">إضافة <input type="checkbox" id="add" class="allmydata" /> </div>
                    <div class="row col-md-2">تحديث <input type="checkbox" id="edit" class="allmydata" /> </div>
                    <div class="row col-md-2">حذف <input type="checkbox" id="delete" class="allmydata" />  </div>
                </div>
            <?PHP foreach($m_list as $m) { 
				$mxid = $m->moduleid; ?>
                
            <div class="list-group">
              <div class="row col-md-12 second_row perm_module">
                <input type="hidden" name="module[]" value="<?PHP echo $mxid; ?>" />
                	<div class="row col-md-6 text-warning" style="font-weight:bold;"><?PHP echo $m->module_name; ?></div>
                     <div class="row col-md-2">مشاهده <input type="checkbox" data-id="<?PHP echo $mxid; ?>" class="vcheck view pcount mongo<?PHP echo $mxid; ?>" id="vx<?PHP echo $mxid; ?>" name="v_<?PHP echo $mxid; ?>" value="1" /></div>
                    <div class="row col-md-2">إضافة <input type="checkbox" data-id="<?PHP echo $mxid; ?>" class="vcheck add pcount mongo<?PHP echo $mxid; ?>" id="vx<?PHP echo $mxid; ?>" name="a_<?PHP echo $mxid; ?>" value="1" /></div>
                    <div class="row col-md-2">تحديث <input type="checkbox" data-id="<?PHP echo $mxid; ?>" class="vcheck edit pcount mongo<?PHP echo $mxid; ?>" id="vx<?PHP echo $mxid; ?>" name="u_<?PHP echo $mxid; ?>" value="1" /></div>
                    <div class="row col-md-2">حذف <input type="checkbox" data-id="<?PHP echo $mxid; ?>"  class="vcheck delete pcount mongo<?PHP echo $mxid; ?>" id="vx<?PHP echo $mxid; ?>" name="d_<?PHP echo $mxid; ?>" value="1" /></div>
               </div>
                <?PHP 
				foreach($this->haya_model->allModule($m->moduleid) as $mc) 
				{		$mid = $mc->moduleid;	?>
                <div class="row col-md-12 third_row">
                <input type="hidden" name="module[]" value="<?PHP echo $mid; ?>" />
                	<div class="row col-md-6"><input type="checkbox" class="allm mushahida allundermodule<?PHP echo $mxid; ?>" id="<?PHP echo $mid; ?>"/> <?PHP echo $mc->module_name; ?></div>
                    <div class="row col-md-2">مشاهده <input type="checkbox" class="pcount view allundermodule<?PHP echo $mxid; ?>" data-id="<?PHP echo $mid; ?>" id="vx<?PHP echo $mid; ?>" name="v_<?PHP echo $mid; ?>" value="1" /></div>
                    <div class="row col-md-2">إضافة <input type="checkbox" onclick="checkback('<?PHP echo $mid; ?>');" class="vcheck add pcount mongo<?PHP echo $mid; ?> allundermodule<?PHP echo $mxid; ?>" id="ax<?PHP echo $mid; ?>" name="a_<?PHP echo $mid; ?>" value="1" /> </div>
                    <div class="row col-md-2">تحديث <input type="checkbox" onclick="checkback('<?PHP echo $mid; ?>');" class="vcheck edit pcount mongo<?PHP echo $mid; ?> allundermodule<?PHP echo $mxid; ?>" id="ux<?PHP echo $mid; ?>" name="u_<?PHP echo $mid; ?>" value="1" /> </div>
                    <div class="row col-md-2">حذف <input type="checkbox" onclick="checkback('<?PHP echo $mid; ?>');" class="vcheck delete pcount mongo<?PHP echo $mid; ?> allundermodule<?PHP echo $mxid; ?>" id="dx<?PHP echo $mid; ?>" name="d_<?PHP echo $mid; ?>" value="1" />  </div>
                </div>
                <?PHP } ?>
             
            </div>
            <?PHP } ?>
        </div>
      </div>
      <div class="col-md-5">
        <div class="panel panel-default panel-block">
          <div class="list-group">
            <div class="list-group-item" id="checkboxes-and-radios">             
              <div class="form-group"> 
                 
                  <?PHP $this->haya_model->module_dropbox_permission('landingpage'); ?>              
              </div>            
           
            <div class="list-group-item" id="checkboxes-and-radios" style="padding: 15px 0px; direction:rtl;">
              <h4 class="section-title preface-title text-warning mmxx" >الموظفين <span id="ux_count"></span></h4>
              <div class="form-group" id="user_role" class="mmxx" style="overflow-y: scroll; overflow-x: hidden; height: 100em !important;">
                <table class="table table-bordered table-striped dataTable" id="basicTable">
                    <thead>
                      <tr role="row">
                        <th class="right">اسم</th>
                        <th class="center">الإجراءات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?PHP foreach($this->users->get_all_users() as $u) { ?>
                     <tr role="row">
                        <td class="right"><strong><?PHP echo $u->fullname; ?> (<?PHP echo $u->list_name; ?>)</strong><br><small><?PHP echo $u->branchname; ?></small></td>
                        <td class="center"><i onclick="editpermission(this,'<?PHP echo $u->userid; ?>');" data-landing="<?PHP echo $u->landingpage; ?>" data-json='<?PHP echo $u->permissionjson; ?>' style="color:#3699d2;"  class="icon-edit"></i></td>
                      </tr>
                    <?PHP } ?>  
                    </tbody>
                  </table>
              </div>
            
            <?PHP if($this->haya_model->check_permission($module,'u')==1 || $this->haya_model->check_permission($module,'a')==1) { ?><span class="error_ping"></span>
              <div class="form-group">                
                  
                  <button type="button" id="save_user_perm"  style="font-size: 16px; display:none;" class="btn btn-success icon-save">&nbsp;&nbsp;حفظ</button>           
              </div>
              <?PHP } ?>
            </div>         
           
          </div>
        </div>
      </div>
    </form>
    <!-----------------> 
  </div>
</div>
<?php $this->load->view('common/footer');?>
</body>
</html>
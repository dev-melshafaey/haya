<div class="row">
  <div class="row col-md-12">
    <form action="" method="POST" id="add_document" name="add_document" enctype="multipart/form-data" autocomplete="off">
      <div class="col-md-4 form-group">
        <label class="text-warning">نوع الوثيقة</label>
        <?php echo $this->haya_model->create_dropbox_list('documenttype','document_type',$documenttype,0,'req'); ?> </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">بلد</label>
        <?php echo $this->haya_model->create_dropbox_list('issuecountry','issuecountry',$issuecountry,0,'req'); ?> </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ الإصدار‎</label>
        <input type="text" name="issuedate" id="issuedate" class="datepicker form-control req" placeholder="تاريخ الإصدار‎" value="<?php echo $issuedate;?>" />
      </div>
     <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ انتهاء الصلاحية</label>
        <input type="text" name="expirydate" id="expirydate" class="datepicker form-control req" placeholder="تاريخ انتهاء الصلاحية" value="<?php echo $expirydate;?>" />
      </div>
   <div class="form-group col-md-4">
       <label class="text-warning">وثيقة</label>
       <input style="border: 0px !important; color: #d09c0d;" type="file" name="document" id="document" title='تحميل'>
    </div>
      <input type="hidden" name="udocid" id="udocid" value="<?php echo $udocid;?>"/>
      <input type="hidden" name="data_table_id" id="data_table_id" value="<?php echo '5';?>"/>
      <input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>"/>
      
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group  col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="add_doc();" value="حفظ" />
    </div>
  </div>
</div>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
		});
	});
</script>
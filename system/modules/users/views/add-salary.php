<?php $this->uri->segment(3);?>
<?php $all_accounts			=	$this->users->get_accounts_by_userid($userid);?>
<?php $payments_type		=	$this->users->get_paymenttype_list('salarypaymenttype');?>
<?php $slary_type_values	=	array();?>
<?php $basic_salary			=	$this->users->get_basic_salary($userid);?>
<div class="row">
  <div class="row col-md-12">
    <form action="" method="POST" id="add_salaray" name="add_salaray" enctype="multipart/form-data" autocomplete="off">
      <div class="col-md-4 form-group">
        <label class="text-warning">رقم الحساب:</label>
        <select class="form-control req" name="bankaccoundid" id="bankaccoundid">
        <?php if(!empty($all_accounts)):?>
        	<option value="">اختيار رقم الحساب</option>
			<?php foreach($all_accounts	as $account):?>
                <option value="<?php echo $account->bankaccountid;?>" <?php if(isset($salary_data->bankaccoundid) AND $salary_detail->bankaccoundid	==	$account->bankaccoundid):?> selected="selected" <?php endif;?>><?php echo $account->accountnumber;?></option>
            <?php endforeach;?>
        <?php endif;?>
        </select>
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">نوع الراتب:</label>
        <?PHP echo $this->haya_model->create_dropbox_list('salary_type','salary_type',$salary_data->salary_type,0,'req'); ?>
      </div>

      <div class="col-md-4 form-group">
        <label class="text-warning">مبلغ  (<strong><span id="remaining" style="font-size:12px; color:#C00;">0</span></strong>)</label>
        <input type="text" class="form-control req NumberInput" name="amount"  id="amount"  placeholder="مبلغ" value="<?php echo $salary_data->amount;?>"/>
      </div>
     <div class="col-md-4 form-group">
        <label class="text-warning">الدرجة</label>
      	<select class="form-control req" name="level_id"  id="level_id"  placeholder="الدرجة">
        <?php foreach($this->users->get_all_levels() as $level):?>
        	<option value="<?php echo $level->level_id;?>"><?php echo $level->level_name;?></option>
        <?php endforeach;?>
      </select>
      </div>
     <div class="col-md-4 form-group">
        <label class="text-warning">الراتب الأساسي</label>
      	<input type="text" class="form-control req" name="basic_salary"  id="basic_salary"  placeholder="الراتب الأساسي"  value="<?php echo $salary_data->basic_salary;?>" onkeyup="only_numeric(this);">
      </div>
     <div class="col-md-4 form-group">
        <label class="text-warning">شهر الراتب</label>
        <input type="text" class="datepicker form-control req" name="salarydate"  id="salarydate"  placeholder="شهر الراتب" value="<?php echo $salary_data->salarydate;?>"/>
      </div>

      <?php if(!empty($payments_type)):?>
      <?php $total	=	 count($payments_type);?>
      <?php $counter	=	1;?>
		  <?php foreach($payments_type	as $payment):?>
          
          <?php 

		  if(isset($salary_data->salaryid))
		  {
			$payments_type_data	=	$this->users->get_paymenttype_data($salary_data->salaryid,$payment->list_id); 
		  }
		  			 
		  ?>
          
              <div class="col-md-6 form-group">
                <label class="text-warning"><?php echo $payment->list_name;?></label>
                <input type="text" class="form-control NumberInput req paymenttype-amount" name="amount_<?php echo $payment->list_id;?>" value="<?php echo (isset($salary_data->salaryid) ? $payments_type_data->amount : NULL);?>" placeholder="مبلغ" onkeyup="only_numeric(this);"/>
                <input type="hidden" name="salarypaymenttype[]" id="salarypaymenttype" value="<?php echo $payment->list_id;?>"/>
              </div>
               <div class="col-md-6 form-group">
                    <label class="text-warning">ملاحظات</label>
                    <input type="text" class="form-control" name="notes_<?php echo $payment->list_id;?>" value="<?php echo (isset($salary_data->salaryid) ? $payments_type_data->notes : NULL);?>" placeholder="ملاحظات"/>
                </div>
             <?php $counter ++;?>
          <?php endforeach;?>
      <?php endif;?>
      <div class="form-group col-md-4">
         <label class="text-warning">وثيقة</label>
         <input style="border: 0px !important; color: #d09c0d;" type="file" name="certificate" id="certificate">
      </div>
      <input type="hidden" name="salaryid" id="salaryid" value="<?php echo $salary_data->salaryid;?>"/>
      <input type="hidden" name="data_table_id" id="data_table_id" value="<?php echo '6';?>"/>
      <input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>"/>
      
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group  col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit-salary" onclick="submit_Salary();" value="حفظ" />
    </div>
  </div>
</div>
<script>
function submit_Salary()
{
	check_my_session();
	$('#add_salaray .req').removeClass('parsley-error');
    $('#add_salaray .req').each(function (index, element) {
    if ($.trim($(this).val()) == '') 
	{
		$(this).addClass('parsley-error');
    }
    });
	
    var len = $('#add_salaray .parsley-error').length;
	if(len<=0)
	{
		var str_data = 	$('#add_salaray').serialize();
		var fd = new FormData(document.getElementById("add_salaray"));
		
		var total_amount	=	$("#amount").val();
		var basic_salary	=	$("#basic_salary").val();
		var type_amount		=	0;
	  
		$('.paymenttype-amount').each(function(index, element) {

			type_amount += Number($(this).val());
			
			var remaining	=	(total_amount-type_amount-basic_salary);
			$("#remaining").html(remaining);

		});
		
		var diff	=	parseInt(type_amount)+parseInt(basic_salary);
		
		  if(total_amount == diff)
		  {
			  $('#submit-salary').attr('disabled','disabled');
		  
		  	  $("#show-loader").show();
		  
			var request = $.ajax({
			  url: config.BASE_URL+'users/add_salary',
			  type: "POST",
			  data: fd,
			  enctype: 'multipart/form-data',
			  dataType: "html",
			  processData: false,  // tell jQuery not to process the data
			  contentType: false ,  // tell jQuery not to set contentType
			  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
			  success: function(msg)
			  {
				  window.location.href = msg;
			  }
			});
		  }
		  else
		  {
			  show_notification_error_end('Your Total Amount is not equal to Other amount.');
		  }
		
	}
}

$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
		});
		
$( ".paymenttype-amount" ).keyup(function() {
	
		var total_amount	=	$("#amount").val();
		var basic_salary	=	$("#basic_salary").val();
		var type_amount		=	0;
	  
		$('.paymenttype-amount').each(function(index, element) {
		//if($(this).val()!='')
		//{
			type_amount += Number($(this).val());
			
			var remaining	=	(total_amount-type_amount-basic_salary);
			$("#remaining").html(remaining);
			//console.log(remaining);
		//}
		});
		
		if(total_amount < type_amount)
		{
		  show_notification_error_end('Your Total amount is less than your Other Amounts.');
		}	
	});
});
</script>
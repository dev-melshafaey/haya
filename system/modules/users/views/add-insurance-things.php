<div class="row">
  <div class="row col-md-12">
    <form action="" method="POST" id="add_extra_things" name="add_extra_things" enctype="multipart/form-data" autocomplete="off">
      <div class="col-md-4 form-group">
        <label class="text-warning">اسم‎</label>
        <input type="text" name="title" id="title" class="form-control req" placeholder="اسم العلاوة" value="<?php echo $title;?>" />
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">القيمة‎‎</label>
        <input type="text" name="amount" id="amount" class="form-control req" placeholder="القيمة‎" value="<?php echo $amount;?>" onkeyup="only_numeric(this);"/>
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ</label>
        <input type="text" name="submit_date" id="submit_date" class="datepicker form-control req" placeholder="تاريخ‎" value="<?php echo $submit_date;?>" />
      </div>
      <div class="form-group col-md-4">
         <label class="text-warning">وثيقة</label>
         <input style="border: 0px !important; color: #d09c0d;" type="file" name="certificate" id="certificate">
      </div>
      <div class="col-md-12 form-group">
        <label class="text-warning">الملاحظات‎</label>
       <textarea name="notes" id="notes" placeholder="الملاحظات" style="margin-top: 0px; margin-bottom: 0px; height: 219px;"><?php echo $notes;?></textarea>
      </div>
      	<input type="hidden" name="extra_id" id="extra_id" value="<?php echo $extra_id;?>"/>
      	<input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>"/>
      	<input type="hidden" name="record_type" id="record_type" value="INSURANCE"/>
      	<input type="hidden" name="data_table_id" id="data_table_id" value="<?php echo '12';?>"/>
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="add_extra();" value="حفظ" />
    </div>
  </div>
</div>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});

function add_extra()
{
	check_my_session();
	$('#add_extra_things .req').removeClass('parsley-error');
    $('#add_extra_things .req').each(function (index, element) {
        if ($.trim($(this).val()) == '') {
            $(this).addClass('parsley-error');
        }
    });
    var len = $('#add_extra_things .parsley-error').length;

	if(len<=0)
	{
		var str_data = 	$('#add_extra_things').serialize();
		var fd = new FormData(document.getElementById("add_extra_things"));
		
		var request = $.ajax({
		  url: config.BASE_URL+'users/add_extra_things',
		  type: "POST",
		  data: fd,
		  enctype: 'multipart/form-data',
		  dataType: "html",
		  processData: false,  // tell jQuery not to process the data
          contentType: false ,  // tell jQuery not to set contentType
		  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(msg)
		  {
			  window.location.href = msg;
		  }
		});
	}
}
</script>
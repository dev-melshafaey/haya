<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;

//-----------------------------------------------------------------------

	/*
	* Constructor
	*/
	
	function __construct()
	{
		 parent::__construct();	
	
		// Loade Admin Model
		$this->load->model('users_model','users');
		
		$this->_data['module']			=	$this->haya_model->get_module();	
		
		// SET Login USER ID
		$this->_login_userid			=	$this->session->userdata('userid');
			
		
		$this->_data['login_userid']	=	$this->_login_userid;
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);
		
		// Load all types
		$this->_data['list_types']		=	$this->haya_model->get_listmanagment_types();
		
	}
//-----------------------------------------------------------------------

	/*
	*	Home Page
	*/
	public function index()
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		// Load Users Listing Page
		$this->load->view('users-listing',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*	For File Downloading
	*	@param $url string
	*	@param $docname
	*/	
	function FileDownload($url,$docname)
	{
		$this->load->helper('download');
		
		$url 	=	'./resources/'.$url;
		$data 	=	file_get_contents($url); // Read the file's contents
		$name 	=	'myphoto.xlsx';
		
		echo force_download($docname, $data);
	}
//-----------------------------------------------------------------------

	/*
	*	Get Current User Detail
	*/		
	function getUsersDetails($userid)
	{
		$this->_data['user_data']	=	$this->users->getAllUserInfo($userid);
		$this->_data['user_id']		=	$userid;
		
		$this->load->view('users_details',$this->_data);
	}
	
//-----------------------------------------------------------------------

	/*
	*	Home Page
	*/	
	public function all_users_list($roleid	=	NULL)
	{
		$text		=	$this->lang->line('users');
		$labels		=	$text['users']['listing'];
		
		$all_users	=	$this->users->get_all_users($roleid);

		if(!empty($all_users))
		{
			$permissions	=	$this->haya_model->check_other_permission(array('129','130','131','132','133','134','142'));
			
			foreach($all_users as $users)
			{
				$actions 	 =  '	<a  onclick="alatadad(this);" data-url="'.base_url().'users/getUsersDetails/'.$users->userid.'"  href="#"><i class="icon-eye-open"></i></a>';
				
				if($permissions[130]['a']	==	1)
				{
					$actions	.=	'	<a href="'.base_url().'users/add_user/'.$users->userid.'/2" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> ';
				}
				if($permissions[131]['a']	==	1)
				{
					$actions	.=	'	<a href="'.base_url().'users/add_user/'.$users->userid.'/3" data-icon="icon-user" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-user"></i></a> ';
				}
				if($permissions[132]['a']	==	1)
				{
					$actions	.=	'	<a href="'.base_url().'users/add_user/'.$users->userid.'/4" data-icon="icon-female" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-female"></i></a> ';
				}
				if($permissions[133]['a']	==	1)
				{
					$actions	.=	'	<a href="'.base_url().'users/add_user/'.$users->userid.'/5" data-icon="icon-book" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-book"></i></a> ';
				}
				if($permissions[129]['a']	==	1)
				{
					$actions	.=	'	<a href="'.base_url().'users/add_user/'.$users->userid.'/6" data-icon="icon-money" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-money"></i></a> ';
				}
				if($permissions[134]['a']	==	1)
				{
					$actions	.=	'	<a href="'.base_url().'users/add_user/'.$users->userid.'/7" data-icon="icon-briefcase" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-briefcase"></i></a> ';
				}
				if($permissions[142]['u']	==	1)
				{
					$actions 	.=	'	<a class="iconspace" href="'.base_url().'users/add_user/'.$users->userid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a> ';
				}
				if($permissions[142]['d']	==	1)
				{
					$actions	.=	'	<a href="#deleteDiag" onclick="show_delete_diag(this);" id="'.$users->userid.'" data-url="'.base_url().'users/delete_user/'.$users->userid.'" data-icon="icon-remove-sign"><i style="color:#CC0000; " class="icon-remove-sign"></i></a>';
				}
				
				$arr[] = array(
				"DT_RowId"		=>	$users->userid,
				"الرقم الوظيفي" =>	$users->id_number,
				"الأسم الكامل" 	=>	$users->fullname,
				"اسم الفرع"		=>	$users->branchname,
               	"قائمة الاسم" 	=>	$users->list_name,
                "الإجرائات" 		=>	$actions);
				
				unset($actions);
			}

			$ex['data'] = $arr;

			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------

	/*
	*	Home Page
	*/	
	public function userpermission()
    {
        $this->haya_model->check_permission($this->_data['module'],'v');
		
        if($this->input->post())
        {
			 foreach($this->input->post('user') as $ux)
            {
                $userid = $ux;               
                foreach($this->input->post('module') as $mx)
                {
                    if($this->input->post('v_'.$mx))
                    {
						$m[$mx]['v'] = 1;
					}
                    else
                    {
						$m[$mx]['v'] = 0;
					}

                    if($this->input->post('a_'.$mx))
                    {
						$m[$mx]['a'] = 1;
					}
                    else
                    {
						$m[$mx]['a'] = 0;
					}

                    if($this->input->post('u_'.$mx))
                    {
						$m[$mx]['u'] = 1;
					}
                    else
                    {
						$m[$mx]['u'] = 0;
					}

                    if($this->input->post('d_'.$mx))
                    {
						$m[$mx]['d'] = 1;
					}
                    else
                    {
						$m[$mx]['d'] = 0;
					}
                }
				
                $permission	=	json_encode($m);
				$udata 		=	array(
								'landingpage'		=>	$this->input->post('landingpage'),
								'permissionjson'	=>	$permission
								);
								
				$this->db->where('userid', $userid);
				$this->db->update('ah_users',json_encode($udata),$this->_login_userid,$udata); 
            }
			
			// Redirect to user permission Page
            redirect(base_url().'users/userpermission');
        }
        else
        {
			$this->_data['m_list'] = $this->haya_model->allModule();
			
			// Load Permission Page
            $this->load->view('userpermission', $this->_data);
        }
    }
//-----------------------------------------------------------------------
	/*
	*	Add New User Form
	*/
    public function all_users($userroleid='101')
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['userrole']	=	$userroleid;
		$this->load->view('users-listing', $this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	*	Add New User Form
	*/
	public function add_user($userid	=	NULL,$table_id	=	1)
	{
		$this->_data['table_id']	=	$table_id;

		if($this->input->post())
		{
			
			$data	=	$this->input->post();

			$user_auth_info		=	array();
			$user_detail_info	=	array();
			
			$user_auth_info['userlogin']	=	$data['userlogin'];
			
			$user_auth_info['userroleid']	=	$data['userroleid'];
			
			$user_auth_info['id_number']	=	$data['id_number'];
			$user_auth_info['level_id']		=	$data['level_id'];
			
			if($data['branchid'])
			{
				$user_auth_info['branchid']		=	$data['branchid'];
			}
			
			$user_auth_info['isemploy']		=	'1';
			
			if(isset($data['userid']))
			{
				if($data['user_password'])
				{
					$user_auth_info['userpassword']	=	md5($data['user_password']);
					//unset($user_auth_info['userpassword']);
				}
				
				// UPDATE User Login information into database 
				$user_auth_info_id	=	$this->users->update_user_auth_info($data['userid'],$user_auth_info);
			}
			else
			{
				$user_auth_info['userpassword']	=	md5($data['user_password']);
							
				// ADD User Login information into database 
				$user_auth_info_id	=	$this->users->add_user_auth_info($user_auth_info);
			}
			
			$profilepic	=	NULL;
			$resume		=	NULL;
			
			if($_FILES["profilepic"]["tmp_name"])
			{
				$profilepic	=	$this->upload_file($user_auth_info_id,'profilepic','resources/users');
			}
			
			if($_FILES["resume"]["tmp_name"])
			{
				$resume		=	$this->upload_file($user_auth_info_id,'resume','resources/users');
			}

			$user_detail_info['userid']					=	$user_auth_info_id;
			$user_detail_info['fullname']				=	$data['fullname'];
			$user_detail_info['fathername']				=	$data['fathername'];
			$user_detail_info['email']					=	$data['email'];
			$user_detail_info['gender']					=	$data['gender'];
			$user_detail_info['maritialstatus']			=	$data['maritialstatus'];
			$user_detail_info['maritialstatus_text']	=	$data['maritialstatus_text'];
			$user_detail_info['profession']				=	$data['profession'];
			$user_detail_info['profession_text']		=	$data['profession_text'];
			$user_detail_info['nationality']			=	$data['nationality'];
			$user_detail_info['nationality_text']		=	$data['nationality_text'];
			$user_detail_info['numberofdependens']		=	$data['numberofdependens'];
			$user_detail_info['date_of_birth']			=	$data['date_of_birth'];
			$user_detail_info['joining_date']			=	$data['joining_date'];
			$user_detail_info['idcardNumber']			=	$data['idcardNumber'];
			$user_detail_info['sub_profession']			=	$data['sub_profession'];
			$user_detail_info['sub_profession_text']	=	$data['sub_profession_text'];
			$user_detail_info['manager_id']				=	$data['manager_id'];
			
			
			if($_FILES["profilepic"]["tmp_name"])
			{
				$user_detail_info['profilepic']			=	$profilepic;
			}
			if($_FILES["resume"]["tmp_name"])
			{
				$user_detail_info['resume']				=	$resume;
			}
			
						
			if(isset($data['userid']))
			{
				// UPDATE User Detail information into database
				$this->users->update_user_detail_info($data['userid'],$user_detail_info);
			}
			else
			{			
				// ADD User Detail information into database
				$this->users->add_user_detail_info($user_detail_info);
			}
			
			// redirect to Users listing page
			redirect(base_url().'users');
			exit();	
		}
		else
		{
			$this->load->model('lease_programe/lease_programe_model', 'loan');
				 
			// Get User data by $userid from database
			$user				  =	$this->users->get_single_user($userid);
			$this->_data["flist"] = $this->loan->get_all_custom_form();
			

			$this->_data['userid']				=	isset($user->userid) ? $user->userid : NULL;
			$this->_data['userlogin']			=	isset($user->userlogin) ? $user->userlogin : NULL;
			$this->_data['id_number']			=	isset($user->id_number) ? $user->id_number : NULL;
			$this->_data['userroleid']			=	isset($user->userroleid) ? $user->userroleid : NULL;
			$this->_data['email']				=	isset($user->email) ? $user->email : NULL;
			$this->_data['branchid']			=	isset($user->branchid) ? $user->branchid : NULL;
			$this->_data['fullname']			=	isset($user->fullname) ? $user->fullname : NULL;
			$this->_data['fathername']			=	isset($user->fathername) ? $user->fathername : NULL;
			$this->_data['gender']				=	isset($user->gender) ? $user->gender : NULL;
			$this->_data['maritialstatus']		=	isset($user->maritialstatus) ? $user->maritialstatus : NULL;
			$this->_data['maritialstatus_text']	=	isset($user->maritialstatus_text) ? $user->maritialstatus_text : NULL;
			$this->_data['profession']			=	isset($user->profession) ? $user->profession : NULL;
			$this->_data['profession_text']		=	isset($user->profession_text) ? $user->profession_text : NULL;
/*			$this->_data['dateofbirth']			=	isset($user->dateofbirth) ? $user->dateofbirth : NULL;*/
			$this->_data['nationality']			=	isset($user->nationality) ? $user->nationality : NULL;
			$this->_data['nationality_text']	=	isset($user->nationality_text) ? $user->nationality_text : NULL;
			$this->_data['is_profilepic']		=	isset($user->profilepic) ? $user->profilepic : NULL;
			$this->_data['is_resume']			=	isset($user->resume) ? $user->resume : NULL;
			$this->_data['numberofdependens']	=	isset($user->numberofdependens) ? $user->numberofdependens : NULL;
			$this->_data['userpassword']		=	isset($user->userpassword) ? '1FE4U' : NULL;
			$this->_data['date_of_birth']		=	isset($user->date_of_birth) ? date("Y-m-d",strtotime($user->date_of_birth)) : NULL;
			$this->_data['joining_date']		=	isset($user->joining_date) ? date("Y-m-d",strtotime($user->joining_date)) : NULL;
			$this->_data['level_id']			=	isset($user->level_id) ? $user->level_id : NULL;
			$this->_data['sub_profession']		=	isset($user->sub_profession) ? $user->sub_profession : NULL;
			$this->_data['manager_id']			=	isset($user->manager_id) ? $user->manager_id : NULL;
			$this->_data['idcardNumber']		=	isset($user->idcardNumber) ? $user->idcardNumber : NULL;
			 
			 $all_extra_things	=	$this->users->get_extra_things($this->_data['userid'],'EXTRA');
			 
			// Load Add new User Form 
			$this->load->view('add-user',$this->_data);
		}
	}
//-----------------------------------------------------------------------

	/*
	*
	*
	*/
	public function update_user_password()
	{
		$data	=	$this->input->post();
		
		if($data['new_pass'])
		{
			$user_auth_info['userpassword']	=	md5($data['new_pass']);
			
			// Update User Password into database
			$user_auth_info_id	=	$this->users->update_user_auth_info($data['userid'],$user_auth_info);
		}
		
		echo ' <strong>تم تحديث سجل</strong>';
	}
//-----------------------------------------------------------------------

	/*
	*
	* Branches Listing
	*
	*/
	public function branchs()
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['all_branches']	=	$this->users->get_all_branches();
		
		// Load Users Listing Page
		$this->load->view('branches-listing',$this->_data);
		
	}
	
//-----------------------------------------------------------------------

	/*
	*
	* Salaries Listing View
	*
	*/
	public function salaries($userid	=	NULL)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['userid']	=	$userid;
		
		// Load Users Salary Listing Page
		$this->load->view('salaries-listing',$this->_data);
		
	}
//-----------------------------------------------------------------------

	/*
	*
	* Salaries Listing Data
	*
	*/	
	public function salaries_listing($userid	=	NULL,$salaryid	=	NULL)
	{
		$all_salaries	=	$this->users->get_all_salaries($userid);
		
		$permissions	=	$this->haya_model->check_other_permission(array('129'));
		
		
		foreach($all_salaries as $salary) 
		{
			if($permissions[129]['d']	==	1)
			{
				$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$salary->salaryid.'" data-url="'.base_url().'users/delete_salary/'.$salary->salaryid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			if($permissions[129]['u']	==	1)
			{
				$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_salary/'.$userid.'/'.$salary->salaryid.'" id="'.$salary->salaryid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			}
			
			$arr[] = array(
				"DT_RowId"			=>	$salary->salaryid.'_durar_lm',
                "رقم الحساب" 		=>	$this->haya_model->get_accnumber_by_id($salary->bankaccoundid),
				"نوع الراتب" 		=>	$this->haya_model->get_name_from_list($salary->salary_type),
				"مبلغ" 				=>	$salary->amount,
				"شهر الراتب" 		=>	$salary->salarydate,
				"الإجراءات"			=>	$actions
				);

				unset($actions);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-----------------------------------------------------------------------

	/*
	*	Add / Update Salary
	*	@param $salaryid integer
	*/
	public function add_salary($userid	=	NULL,$salaryid	=	NULL)
	{
		$this->_data['userid']	=	$userid;

		if($this->input->post())
		{	
			// GET all values from POST
			$data		=	$this->input->post();
			$table_id	=	$data['data_table_id'];
				
			// UNSET the value from ARRAY
			unset($data['submit']);
			unset($data['data_table_id']);
			
			$certificate	==	NULL;
			if($_FILES["certificate"]["tmp_name"])
			{
				$certificate	=	$this->upload_file($this->input->post('userid'),'certificate','resources/users');
			}
			
			if($this->input->post('salaryid'))
			{
				$data	=	array(
							'userid'			=>	$this->input->post('userid'),
							'activity_user_id'	=>	$this->_login_userid,
							'bankaccoundid'		=>	$this->input->post('bankaccoundid'),
							'salary_type'		=>	$this->input->post('salary_type'),
							'salary_type_text'	=>	$this->input->post('salary_type_text'),
							'salarydate'		=>	$this->input->post('salarydate'),
							'amount'			=>	$this->input->post('amount'),
							'basic_salary'		=>	$this->input->post('basic_salary'),
							'level_id'			=>	$this->input->post('level_id'),
							'certificate'		=>	$certificate
				);
				
				if($certificate)
				{
					$data['certificate']		=	$certificate;
				}
				
				$this->users->update_level_into_users($this->input->post('userid'),array('level_id'	=>	$this->input->post('level_id')));
				
				$this->users->update_salary($this->input->post('salaryid'),$data);
				
				foreach($this->input->post('salarypaymenttype') as $sptkey => $sptvalue)
				{
					$arr = array(
						'salaryid'			=>	$this->input->post('salaryid'),
						'userid'			=>	$this->input->post('userid'),
						'salarypaymenttype'	=>	$sptvalue,
						'amount'			=>	$this->input->post('amount_'.$sptvalue),
						'activity_user_id'	=>	$this->_login_userid,
						'notes'				=>	$this->input->post('notes_'.$sptvalue));
						
						// ADD data into database
						$this->users->update_salary_detail($this->input->post('salaryid'),$sptvalue,$arr);
				}

			}
			else
			{
				$data	=	array(
							'userid'			=>	$this->input->post('userid'),
							'activity_user_id'	=>	$this->_login_userid,
							'bankaccoundid'		=>	$this->input->post('bankaccoundid'),
							'salary_type'		=>	$this->input->post('salary_type'),
							'salary_type_text'	=>	$this->input->post('salary_type_text'),
							'salarydate'		=>	$this->input->post('salarydate'),
							'amount'			=>	$this->input->post('amount'),
							'basic_salary'		=>	$this->input->post('basic_salary'),
							'level_id'			=>	$this->input->post('level_id'),
							'certificate'		=>	$certificate
				);
				
				
				$this->users->update_level_into_users($this->input->post('userid'),array('level_id'	=>	$this->input->post('level_id')));
				
				$salary_id	=	$this->users->add_salary($data);
				
				foreach($this->input->post('salarypaymenttype') as $sptkey => $sptvalue)
				{
					$arr = array(
						'salaryid'			=>	$salary_id,
						'userid'			=>	$this->input->post('userid'),
						'salarypaymenttype'	=>	$sptvalue,
						'amount'			=>	$this->input->post('amount_'.$sptvalue),
						'activity_user_id'	=>	$this->_login_userid,
						'notes'				=>	$this->input->post('notes_'.$sptvalue));
						
						// ADD data into database
						$this->users->add_salary_detail($arr);
				}

			}
			
			// Redirect to listing page
			echo $url	=	base_url().'users/add_user/'.$data['userid'].'/'.$table_id;
		}
		else
		{
			if($salaryid)
			{
				// Get Provinces
				$this->_data['salary_data']			=	$this->users->get_salary_data($salaryid);
			}
			else
			{
				
			}

			// Load Users Listing Page
			$this->load->view('add-salary',$this->_data);
		}

	}
//-----------------------------------------------------------------------

	/*
	*	Add / Update Salary
	*	@param $salaryid integer
	*/
	public function add_basic_salary($userid	=	NULL,$salaryid	=	NULL)
	{
		$this->_data['userid']	=	$userid;

		if($this->input->post())
		{	
			// GET all values from POST
			$data		=	$this->input->post();
			$table_id	=	$data['data_table_id'];
				
			// UNSET the value from ARRAY
			unset($data['submit']);
			unset($data['data_table_id']);
			
			if($this->input->post('salaryid'))
			{
				$data	=	array(
							'userid'			=>	$this->input->post('userid'),
							'activity_user_id'	=>	$this->_login_userid,
							'amount'			=>	$this->input->post('amount'),
							'notes'				=>	$this->input->post('notes')
				);
				
				$this->users->update_basic_salary($this->input->post('salaryid'),$data);
			}
			else
			{
				$data	=	array(
							'userid'			=>	$this->input->post('userid'),
							'activity_user_id'	=>	$this->_login_userid,
							'amount'			=>	$this->input->post('amount'),
							'notes'				=>	$this->input->post('notes'),
				);
				
				$salary_id	=	$this->users->add_basic_salary($data);
			}
			
			// Redirect to listing page
			echo $url	=	base_url().'users/add_user/'.$data['userid'].'/'.$table_id;
		}
		else
		{
			if($salaryid)
			{
				// Get Provinces
				$this->_data['salary_data']			=	$this->users->get_basic_salary_data($salaryid);
			}
			else
			{
				
			}

			// Load Users Listing Page
			$this->load->view('add-basic-salary',$this->_data);
		}

	}

//-----------------------------------------------------------------------

	/*
	*
	* All Documents View
	*
	*/
	public function documents($userid	=	NULL)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['userid']	=	$userid;
		
		// Load Users Salary Listing Page
		$this->load->view('documents-listing',$this->_data);
		
	}
//-----------------------------------------------------------------------

	/*
	*
	* All Documents Listing Data
	*
	*/	
	public function documents_listing($userid)
	{
		$all_documents	=	$this->users->get_all_documents($userid);
		
		$permissions	=	$this->haya_model->check_other_permission(array('133'));
		
		foreach($all_documents as $document) 
		{
			if($permissions[133]['d']	==	1)
			{
				$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$document->udocid.'" data-url="'.base_url().'users/delete_document/'.$document->udocid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			if($permissions[133]['u']	==	1)
			{
				$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_document/'.$userid.'/'.$document->udocid.'" id="'.$document->udocid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			}
			$arr[] = array(
				"DT_RowId"				=>	$document->udocid.'_durar_lm',
                "نوع الوثيقة" 			=>	$this->haya_model->get_name_from_list($document->documenttype),
				"بلد"					=>	$this->haya_model->get_name_from_list($document->issuecountry),
				"تاريخ الإصدار" 			=>	$document->issuedate,
				"تاريخ انتهاء الصلاحية" 	=>	$document->expirydate,
				"الإجراءات"				=>	$actions
				);

				unset($actions);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-----------------------------------------------------------------------

	/*
	*	Add / Update Salary
	*	@param $salaryid integer
	*/
	public function add_document($userid	=	NULL,$docid	=	NULL)
	{
		$this->_data['userid']	=	$userid;
		
		if($this->input->post())
		{
			//echo '<pre>';print_r($_FILES["document"]);exit();
			
			// Get all values from POST
			$data	=	$this->input->post();
			$table_id	=	$data['data_table_id'];
			
			// UNSET the value from ARRAY
			unset($data['submit']);
			unset($data['documenttype_text']);
			unset($data['issuecountry_text']);
			unset($data['data_table_id']);
			
			if($_FILES["document"]["tmp_name"])
			{
				$certificate		=	$this->upload_file($data['userid'],'document','resources/users');
				$data['document']	=	$certificate;
			}
			
			if($data['udocid'])
			{
				// Update data into database
				$this->users->update_document($data['udocid'],$data);
			}
			else
			{
				// ADD data into database
				$this->users->add_document($data);
			}
			
			// Redirect to listing page
			echo $url	=	base_url().'users/add_user/'.$data['userid'].'/'.$table_id;
		}
		else
		{
			// Get Provinces
			 
			$this->_data['document_detail']	=	$this->users->get_documents_detail($docid);
			
			//print_r($this->_data['document_detail']);

			$this->_data['udocid']			=	isset($this->_data['document_detail']->udocid) ? $this->_data['document_detail']->udocid : NULL;
			$this->_data['documenttype']	=	isset($this->_data['document_detail']->documenttype) ? $this->_data['document_detail']->documenttype : NULL;
			$this->_data['issuedate']		=	isset($this->_data['document_detail']->issuedate) ? $this->_data['document_detail']->issuedate : NULL;
			$this->_data['expirydate']		=	isset($this->_data['document_detail']->expirydate) ? $this->_data['document_detail']->expirydate : NULL;
			$this->_data['issuecountry']	=	isset($this->_data['document_detail']->issuecountry) ? $this->_data['document_detail']->issuecountry : NULL;
			
			// Load Users Listing Page
			$this->load->view('add-document',$this->_data);
		}
	}
//-----------------------------------------------------------------------

	/*
	*
	* All Documents View
	*
	*/
	public function experiences($userid	=	NULL)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['userid']	=	$userid;
		
		// Load Users Salary Listing Page
		$this->load->view('experiences-listing',$this->_data);
		
	}
//-----------------------------------------------------------------------

	/*
	*
	* All Documents Listing Data
	*
	*/	
	public function experiences_listing($userid)
	{
		$all_experiences	=	$this->users->get_all_experiences($userid);
		
		$permissions	=	$this->haya_model->check_other_permission(array('134'));
		
		foreach($all_experiences as $experience) 
		{
			if($permissions[134]['d']	==	1)
			{
				$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$experience->exe_id.'" data-url="'.base_url().'users/delete_experience/'.$experience->exe_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			if($permissions[134]['u']	==	1)
			{
				$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_experience/'.$userid.'/'.$experience->exe_id.'" id="'.$experience->exe_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			}
			
			$arr[] = array(
				"DT_RowId"				=>	$experience->udocid.'_durar_lm',
                "موقف / العنوان" 		=>	$experience->exe_title,
				"بلد"					=>	$this->haya_model->get_name_from_list($experience->country),
				"تاريخ البدء" 			=>	date('Y-m-d',strtotime($experience->start_date)),
				"تاريخ الانتهاء" 		=>	date('Y-m-d',strtotime($experience->end_date)),
				"التفاصيل" 				=>	$experience->detail,
				"الإجراءات"				=>	$actions
				);

				unset($actions);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
		
	}
//-----------------------------------------------------------------------

	/*
	*	Add / Update Salary
	*	@param $salaryid integer
	*/
	public function add_experience($userid	=	NULL,$exeid	=	NULL)
	{
		$this->_data['userid']	=	$userid;
		
		if($this->input->post())
		{
			// Get all values from POST
			$data	=	$this->input->post();
			$table_id	=	$data['data_table_id'];
			
			// UNSET the value from ARRAY
			unset($data['submit']);
			unset($data['country_text']);
			unset($data['data_table_id']);
			
			if($_FILES["certificate"]["tmp_name"])
			{
				$certificate			=	$this->upload_file($data['userid'],'certificate','resources/users');
				$data['certificate']	=	$certificate;
			}
			
			if($data['exe_id'])
			{
				// Update data into database
				$this->users->update_experience($data['exe_id'],$data);
			}
			else
			{
				// ADD data into database
				$this->users->add_experience($data);
			}
			
			// Redirect to listing page
			echo $url	=	base_url().'users/add_user/'.$data['userid'].'/'.$table_id;
		}
		else
		{
			// Get Provinces
			 
			$this->_data['experience_detail']	=	$this->users->get_experiences_detail($exeid);
			
			$this->_data['exe_id']			=	isset($this->_data['experience_detail']->exe_id) ? $this->_data['experience_detail']->exe_id : NULL;
			$this->_data['exe_title']		=	isset($this->_data['experience_detail']->exe_title) ? $this->_data['experience_detail']->exe_title : NULL;
			$this->_data['country']			=	isset($this->_data['experience_detail']->country) ? $this->_data['experience_detail']->country : NULL;
			$this->_data['start_date']		=	isset($this->_data['experience_detail']->start_date) ? date('Y-m-d',strtotime($this->_data['experience_detail']->start_date)) : NULL;
			$this->_data['end_date']		=	isset($this->_data['experience_detail']->end_date) ? date('Y-m-d',strtotime($this->_data['experience_detail']->end_date)) : NULL;
			$this->_data['detail']			=	isset($this->_data['experience_detail']->detail) ? $this->_data['experience_detail']->detail : NULL;
			
			// Load Users Listing Page
			$this->load->view('add-experience',$this->_data);
		}
	}
	
//-----------------------------------------------------------------------

	/*
	*
	* All Family Members Listing Data
	*
	*/
	public function family_members($userid	=	NULL)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['userid']	=	$userid;
		
		// Load Users Salary Listing Page
		$this->load->view('family-members-listing',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*
	* Communications Listing from AJAX
	*
	*/	
	public function family_members_listing($userid)
	{
		$all_family_members	=	$this->users->get_all_family_members($userid);
		
		$permissions	=	$this->haya_model->check_other_permission(array('132'));

		foreach($all_family_members as $member) 
		{
			if($permissions[132]['d']	==	1)
			{
				$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$member->ufamilyid.'" data-url="'.base_url().'users/delete_family_member/'.$member->ufamilyid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			if($permissions[132]['u']	==	1)
			{
				$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_family_member/'.$userid.'/'.$member->ufamilyid.'" id="'.$member->ufamilyid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			}
			$arr[] = array(
				"DT_RowId"		=>	$member->ufamilyid.'_durar_lm',
                "‫الاسم الكامل" 	=>	$member->fullname,
				"علاقة" 			=>	$this->haya_model->get_name_from_list($member->relation),
				"تاريخ الميلاد" 	=>	$member->dateofbirth,
				"الإجراءات"		=>	$actions
				);

				unset($actions);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-----------------------------------------------------------------------

	/*
	*	Add / Update Salary
	*	@param $salaryid integer
	*/
	public function add_family_member($userid	=	NULL,$fmid	=	NULL)
	{
		$this->_data['userid']	=	$userid;
		
		if($this->input->post())
		{
			// Get all values from POST
			$data		=	$this->input->post();
			$table_id	=	$data['data_table_id'];
			
			// UNSET the value from ARRAY
			unset($data['submit']);
			unset($data['relation_text']);
			unset($data['data_table_id']);
			
			if($_FILES["family_doc"]["tmp_name"])
			{
				$certificate		=	$this->upload_file($data['userid'],'family_doc','resources/users');
				$data['family_doc']	=	$certificate;
			}
			
			if($data['ufamilyid'])
			{
				// Update data into database
				$this->users->update_family_member($data['ufamilyid'],$data);
			}
			else
			{
				// ADD data into database
				$this->users->add_family_member($data);
			}
			
			// Redirect to listing page
			echo $url	=	base_url().'users/add_user/'.$data['userid'].'/'.$table_id;
		}
		else
		{
			// Get Provinces
			 
			$this->_data['family_detail']			=	$this->users->get_family_detail($fmid);

			$this->_data['ufamilyid']	=	isset($this->_data['family_detail']->ufamilyid) ? $this->_data['family_detail']->ufamilyid : NULL;
			$this->_data['fullname']	=	isset($this->_data['family_detail']->fullname) ? $this->_data['family_detail']->fullname : NULL;
			$this->_data['relation']	=	isset($this->_data['family_detail']->relation) ? $this->_data['family_detail']->relation : NULL;
			$this->_data['dateofbirth']			=	isset($this->_data['family_detail']->dateofbirth) ? $this->_data['family_detail']->dateofbirth : NULL;
			
			// Load Users Listing Page
			$this->load->view('add-family-member',$this->_data);
		}

	}
//-----------------------------------------------------------------------

	/*
	*
	* All Communications Listing
	*
	*/
	public function communications($userid	=	NULL)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['userid']	=	$userid;
		
		// Load Users Salary Listing Page
		$this->load->view('communications-listing',$this->_data);
		
	}
	
//-----------------------------------------------------------------------

	/*
	*
	* Communications Listing from AJAX
	*
	*/	
	public function communications_listing($userid)
	{
		$all_communications	=	$this->users->get_all_communications($userid);
		
		$permissions	=	$this->haya_model->check_other_permission(array('131'));
		
		foreach($all_communications as $communication) 
		{
			if($permissions[131]['d']	==	1)
			{
				$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$communication->communicationid.'" data-url="'.base_url().'users/delete_communication/'.$communication->communicationid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			if($permissions[131]['u']	==	1)
			{
				$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_communication/'.$userid.'/'.$communication->communicationid.'" id="'.$communication->communicationid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			}
			$arr[] = array(
				"DT_RowId"			=>	$communication->communicationid.'_durar_lm',
                "‫نوع الاتصال" 		=>	$this->haya_model->get_name_from_list($communication->communicationtype),
				"قيمة الاتصالات" 		=>	$communication->communicationvalue,
				"تاريخ" 			=>	$communication->addingdate,
				"الإجراءات"			=>	$actions
				);

				unset($actions);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}

//-----------------------------------------------------------------------

	/*
	*	Add / Update Salary
	*	@param $salaryid integer
	*/
	public function add_communication($userid	=	NULL,$cominid	=	NULL)
	{
		$this->_data['userid']	=	$userid;
		
		if($this->input->post())
		{
			// Get all values from POST
			$data	=	$this->input->post();
			$table_id	=	$data['data_table_id'];
			
			// UNSET the value from ARRAY
			unset($data['submit']);
			unset($data['communicationtype_text']);
			unset($data['data_table_id']);
			
			if($_FILES["communication_doc"]["tmp_name"])
			{
				$certificate				=	$this->upload_file($data['userid'],'communication_doc','resources/users');
				$data['communication_doc']	=	$certificate;
			}
			
			
			if($data['communicationid'])
			{
				// Update data into database
				$this->users->update_communication($data['communicationid'],$data);
			}
			else
			{
				// ADD data into database
				$this->users->add_communication($data);
			}
			
			// Redirect to listing page
			echo $url	=	base_url().'users/add_user/'.$data['userid'].'/'.$table_id;
		}
		else
		{
			// Get Provinces
			 
			$this->_data['communications_detail']			=	$this->users->get_communications_detail($cominid);

			$this->_data['communicationid']		=	isset($this->_data['communications_detail']->communicationid) ? $this->_data['communications_detail']->communicationid : NULL;
			$this->_data['communicationtype']	=	isset($this->_data['communications_detail']->communicationtype) ? $this->_data['communications_detail']->communicationtype : NULL;
			$this->_data['communicationvalue']	=	isset($this->_data['communications_detail']->communicationvalue) ? $this->_data['communications_detail']->communicationvalue : NULL;
			$this->_data['addingdate']			=	isset($this->_data['communications_detail']->addingdate) ? $this->_data['communications_detail']->addingdate : NULL;
			//$this->_data['accountfullname']	=	isset($this->_data['account_detail']->accountfullname) ? $this->_data['account_detail']->accountfullname : NULL;
			
			// Load Users Listing Page
			$this->load->view('add-communication',$this->_data);
		}

	}
//-----------------------------------------------------------------------

	/*
	*
	* All Banks Listing
	*
	*/
	public function bank_accounts($userid	=	NULL)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['userid']	=	$userid;
		
		// Load Users Salary Listing Page
		$this->load->view('bank-accounts-listing',$this->_data);
		
	}

//-----------------------------------------------------------------------

	/*
	*
	* All Banks Listing
	*
	*/	
	public function bank_accounts_listing($userid)
	{
		$all_bank_accounts	=	$this->users->get_all_bank_accounts($userid);
		
		$permissions	=	$this->haya_model->check_other_permission(array('130'));
		
		foreach($all_bank_accounts as $account) 
		{
			if($permissions[130]['d']	==	1)
			{
				$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$account->bankaccountid.'" data-url="'.base_url().'users/delete_bank_account/'.$account->bankaccountid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			if($permissions[130]['u']	==	1)
			{
				$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_bank_account/'.$userid.'/'.$account->bankaccountid.'" id="'.$account->bankaccountid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			}
			$arr[] = array(
				"DT_RowId"			=>	$account->bankaccountid.'_durar_lm',
                "‫اسم البنك" 		=>	$this->haya_model->get_name_from_list($account->bankid),
				"اسم الفرع" 		=>	$this->haya_model->get_name_from_list($account->branchid),
				"رقم الحساب" 		=>	$account->accountnumber,
				"اسم صاحب الحساب" 	=>	$account->accountfullname,
				"الإجراءات"			=>	$actions
				);

				unset($actions);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-----------------------------------------------------------------------

	/*
	*	Add / Update Salary
	*	@param $salaryid integer
	*/
	public function add_bank_account($userid	=	NULL,$accountid	=	NULL)
	{
		$this->_data['userid']	=	$userid;
		
		if($this->input->post())
		{
			// Get all values from POST
			$data		=	$this->input->post();
			$table_id	=	$data['data_table_id'];
			
			if($_FILES["bank_doc"]["tmp_name"])
			{
				$profilepic			=	$this->upload_file($this->_data['userid'],'bank_doc','resources/users');
				$data['bank_doc']	=	$profilepic;
			}
			
			// UNSET the value from ARRAY
			unset($data['submit']);
			unset($data['bankid_text']);
			unset($data['branchid_text']);
			unset($data['data_table_id']);
			
			if($data['bankaccountid'])
			{
				// Update data into database
				$this->users->update_bank_account($data['bankaccountid'],$data);
			}
			else
			{
				// ADD data into database
				$this->users->add_bank_account($data);
				
				
			}
			
			// Redirect to listing page
			echo $url	=	base_url().'users/add_user/'.$data['userid'].'/'.$table_id;
		}
		else
		{
			// Get Provinces
			 
			$this->_data['account_detail']		=	$this->users->get_account_detail($accountid);

			$this->_data['bankaccountid']		=	isset($this->_data['account_detail']->bankaccountid) ? $this->_data['account_detail']->bankaccountid : NULL;
			$this->_data['bankid']				=	isset($this->_data['account_detail']->bankid) ? $this->_data['account_detail']->bankid : NULL;
			$this->_data['branchid']			=	isset($this->_data['account_detail']->branchid) ? $this->_data['account_detail']->branchid : NULL;
			$this->_data['accountnumber']		=	isset($this->_data['account_detail']->accountnumber) ? $this->_data['account_detail']->accountnumber : NULL;
			$this->_data['accountfullname']		=	isset($this->_data['account_detail']->accountfullname) ? $this->_data['account_detail']->accountfullname : NULL;
			
			// Load Users Listing Page
			$this->load->view('add-bank-account',$this->_data);
		}

	}

//-----------------------------------------------------------------------

	/*
	*
	* Branches Listing
	*
	*/
	public function branches_listing()
	{
		$segment 	=	$this->uri->segment(1);
		$text		=	$this->lang->line($segment);
		$labels		=	$text['branchs']['listing'];
		
		$permissions	=	$this->haya_model->check_other_permission(array('112'));
		
		foreach($this->users->get_all_branches() as $branch) 
		{
			if($permissions[112]['d']	==	1)
			{
				$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$branch->branchid.'" data-url="'.base_url().'users/delete_branch/'.$branch->branchid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			if($permissions[112]['u']	==	1)
			{
				$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_branch/'.$branch->branchid.'" id="'.$branch->branchid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			}
			$arr[] = array(
				"DT_RowId"	=>	$branch->branchid.'_durar_lm',
                "الإسم" 		=>	$branch->branchname,
				"منطقة" 	=>	$this->haya_model->get_name_from_list($branch->province),
				"ولاية" 		=>	$this->haya_model->get_name_from_list($branch->wilaya),
				"الإجرائات" 	=>	$actions
				);

				unset($actions);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
	 
//-----------------------------------------------------------------------

	/*
	*	Add / Update Branch Data
	*	@param $branchid integer
	*/
	public function add_branch($branchid	=	NULL)
	{
		
		if($this->input->post())
		{
			// Get all values from POST
			$data	=	$this->input->post();
			
			// UNSET the value from ARRAY
			unset($data['submit']);
			unset($data['province_text']);
			unset($data['wilaya_text']);
			
			if($data['branchid'])
			{
				// Update data into database
				$this->users->update_branch($data['branchid'],$data);
			}
			else
			{
				//print_r($data);
				// ADD data into database
				$this->users->add_branch($data);
			}
			
			// Redirect to listing page
			redirect(base_url().'users/branchs');
			exit();
		}
		else
		{
			// Get Provinces
			$this->_data['provinces']		=	$this->users->get_provinces();
			 
			$this->_data['branch']			=	$this->users->get_single_branch($branchid);
			 
			$this->_data['branchid']		=	isset($this->_data['branch']->branchid) ? $this->_data['branch']->branchid : NULL;
			$this->_data['provinceid']		=	isset($this->_data['branch']->province) ? $this->_data['branch']->province : NULL;
			$this->_data['wiliayaid']		=	isset($this->_data['branch']->wilaya) ? $this->_data['branch']->wilaya : NULL;
			$this->_data['branchname']		=	isset($this->_data['branch']->branchname) ? $this->_data['branch']->branchname : NULL;
			$this->_data['branchaddress']	=	isset($this->_data['branch']->branchaddress) ? $this->_data['branch']->branchaddress : NULL;
			$this->_data['branchstatus']	=	isset($this->_data['branch']->branchstatus) ? $this->_data['branch']->branchstatus : NULL;
			 
			// Load Users Listing Page
			$this->load->view('add-branch',$this->_data);
		}

	}
	
//-----------------------------------------------------------------------

	/*
	* Add Attendence
	*
	*/
	public function add_leave_request($leaveid	=	NULL)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($data['start_date']	==	$data['end_date'])
			{
				$data['total_leaves']	=	1;
			}
			else
			{
				$total_leaves			=	(date('d',strtotime($data['end_date'])) - date('d',strtotime($data['start_date'])));
				$data['total_leaves']	=	($total_leaves	+	1);
			}
			
			// UNSET ARRAY KEY
			unset($data['reason_text']);
			
			if($data['leaveid'])
			{
				if($_FILES["leave_application"]["tmp_name"])
				{
					$leave_application	=	$this->upload_file($this->_login_userid,'leave_application','resources/users');
					
					$data['leave_application']	=	$leave_application;
				}
				else
				{
					$data['leave_application']	=	$data['leave_application_old'];
				}
				
				unset($data['leave_application_old']);
				
				// UPDATE the record
				$this->users->update_leave_request($data['leaveid'],$data);
			}
			else
			{
				if($_FILES["leave_application"]["tmp_name"])
				{
					$leave_application	=	$this->upload_file($this->_login_userid,'leave_application','resources/users');
					
					$data['leave_application']	=	$leave_application;
				}
				
				unset($data['leave_application_old']);
				
				// ADD record
				$this->users->add_leave_request($data);
			}

		}
		else
		{
			$this->_data['manager_id']	=	$this->_data['user_detail']['profile']->manager_id;
			
			// Get data for Leave Request by leaveid
			$request_record	=	$this->users->get_leave_req_record($leaveid);
			
			$this->_data['leaveid']				=	isset($request_record->leaveid) ? $request_record->leaveid : NULL;
			$this->_data['userid']				=	isset($request_record->userid) ? $request_record->userid : NULL;
			$this->_data['reason']				=	isset($request_record->reason) ? $request_record->reason : NULL;
			$this->_data['start_date']			=	isset($request_record->start_date) ? $request_record->start_date : NULL;
			$this->_data['end_date']			=	isset($request_record->end_date) ? $request_record->end_date : NULL;
			$this->_data['notes']				=	isset($request_record->notes) ? $request_record->notes : NULL;
			$this->_data['leave_application']	=	isset($request_record->leave_application) ? $request_record->leave_application : NULL;
			 
			 
			 
			// Load Users Listing Page
			$this->load->view('leave-request-form',$this->_data);
		}
	}
//-----------------------------------------------------------------------

	/*
	*
	* Leave Request Listing
	*
	*/
	public function leave_requests()
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['user_role']	=	$this->_data['user_detail']['profile']->userroleid;
		$this->_data['manager_id']	=	$this->_data['user_detail']['profile']->manager_id;
		
		//echo '<pre>'; print_r($this->_data['user_detail']);exit();

		// Load Leave Request Listing Page
		$this->load->view('leave-requests-listing',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*
	* Leave Request Listing
	*
	*/
	public function leave_requests_listing()
	{
		$data		=	$this->input->post();
		$this->_data['response']	=	$this->users->get_all_approved_leave_requests($data['startdate'],$data['enddate']);
		//print_r($response);
		//exit();
		
		$this->load->view('result-by-daterange',$this->_data);
		
		/*foreach($this->users->get_all_approved_leave_requests($data['startdate'],$data['enddate']) as $leave) 
		{
			if($leave->leave_application)
			{
				$actions	= '&nbsp;&nbsp;<a href="'.base_url().'users/download_file/'.$leave->userid.'/'.$leave->leave_application.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
			}
			
			$actions 	 .=  '	<a  onclick="alatadad(this);" data-url="'.base_url().'users/view_request/'.$leave->leaveid.'"  href="#"><i class="icon-eye-open"></i></a>';
			
			if($this->haya_model->check_other_permission(143,'u')==1)
			{
				$actions 	.=	'<a href="#addingDiag" onclick="alatadad(this);" data-url="'.base_url().'users/add_leave_request/'.$leave->leaveid.'" id="'.$leave->leaveid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			}
			
			elseif($leave->approved	==	'1')
			{
				$request	=	'<img src="'.base_url().'assets/images/approved.png" style="width: 24px;" alt="Approved" title="Approved"/>';
			}
			
			$arr[] = array(
				"DT_RowId"			=>	$leave->leaveid.'_durar_lm',
                "الأسم الكامل"		=>	$leave->fullname,
				"سبب" 				=>	$leave->reason,
				"من" 				=>	date('d-m-y',strtotime($leave->start_date)),
				"إلى" 				=>	date('d-m-y',strtotime($leave->end_date)),
				"إجازة يوم" 		=>	$leave->total_leaves,
				"ترك طلب الحالة"	=>	$request,
				"الإجرائات" 			=>	$actions
				);

				unset($actions);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);*/
	}
//-----------------------------------------------------------------------

	/*
	*
	* Holidays Level Listing
	*
	*/
	public function all_levels()
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		// Load Leave Request Listing Page
		$this->load->view('levels-listing',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*
	* Holidays Level Listing
	*
	*/
	public function ajax_all_levels()
	{
		$all_levels	=	$this->users->get_all_levels();
		
		foreach($all_levels as $level) 
		{
			$actions 	=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_level/'.$level->level_id.'" id="'.$level->level_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			
			$arr[] = array(
				"DT_RowId"		=>	$level->level_id.'_durar_lm',
                "الدرجة" 		=>	$level->level_name,
				"عدد الاجازات" 	=>	$level->level_holiday,
				"الإجرائات" 		=>	$actions
				);

				unset($actions);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-----------------------------------------------------------------------

	/*
	*	Add / Holidays Levels
	*	@param $level_id integer
	*/
	public function add_level($level_id	=	NULL)
	{
		if($this->input->post())
		{
			// Get all values from POST
			$data	=	$this->input->post();

			// UNSET the value from ARRAY
			unset($data['submit']);
			
			if($data['level_id'])
			{
				// Update data into database
				$this->users->update_level($data['level_id'],$data);
			}
			
			// Redirect to listing page
			echo $url	=	base_url().'users/all_levels/';
		}
		else
		{

			$level_detail		=	$this->users->get_level_by_id($level_id);

			$this->_data['level_id']		=	isset($level_detail->level_id) ? $level_detail->level_id : NULL;
			$this->_data['level_name']		=	isset($level_detail->level_name) ? $level_detail->level_name : NULL;
			$this->_data['level_holiday']	=	isset($level_detail->level_holiday) ? $level_detail->level_holiday : NULL;

			// Load Users Listing Page
			$this->load->view('add-level',$this->_data);
		}

	}
//-----------------------------------------------------------------------

	/*
	*	Get Current User Detail
	*/		
	function view_request($leaveid)
	{	
		$this->_data['user_data']	=	$this->users->leave_request_view($leaveid);
		$this->_data['user_role']	=	$this->_data['user_detail']['profile']->userroleid;
		
		$this->_data['user_id']		=	$this->_data['login_userid'];
		
		$this->load->view('user-leave-request',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*	Get Current User Detail
	*/		
	function request_response()
	{
		
		$leaveid		=	$this->input->post('leaveid');
		$request_value	=	$this->input->post('val');
		
		$data			=	array('approved'	=>	$request_value);
		
		/*$userid		=	$this->users->get_user_id($leaveid);
		
		$user_detail 	=	$this->haya_model->get_user_detail($userid);
		
		$this->send_email($to	=	NULL,$from	=	NULL,$subject	=	NULL,$message = NULL,$path	=	NULL); */
		
		//echo '<pre>'; print_r($data);exit();
		if($this->input->post('manager_id'))
		{
			$choose_reason		=	$this->input->post('choose_reason');
			$choose_reason		=	isset($choose_reason) ? $choose_reason : NULL;
			$manager_comment	=	$this->input->post('manager_comment');
			$manager_id			=	$this->input->post('manager_id');
			
			$manager_response	=	array(
										'choose_reason'		=>	$choose_reason,
										'manager_comment'	=>	$manager_comment,
										'leave_req_id'		=>	$leaveid,
										'approved_date'		=>	date('Y-m-d'),
										'manager_id'		=>	$manager_id);
										
			$manager_response_exist	=	$this->users->manager_response_exist($manager_id,$leaveid);
			
			if(!empty($manager_response_exist))
			{
				// Update Manager Response
				$this->users->update_manager_response($leaveid,$manager_response);
			}
			else
			{
				// Add Manager Response
				$this->users->add_manager_response($manager_response);
			}
			

		}
		$this->users->update_leave_request_response($leaveid,$data);
		
		$leave_detail	=	$this->users->get_leave_information($leaveid);
		
		$dateRange 		=	$this->dateRange($leave_detail->start_date, $leave_detail->end_date);
		
		if($leave_detail->approved	==	1)
		{
			foreach($dateRange	as $date)
			{
				$data_array	=	array(
				'userid'			=>	$leave_detail->userid,
				'absent_type'		=>	$leave_detail->reason,
				'attendence_status'	=>	'A',
				'reason'			=>	'1',
				'late'				=>	'0',
				'attendence_date'	=>	$date
				);
				
				$this->users->add_attendence($data_array);
			}
		}
		elseif($leave_detail->approved	==	2)
		{
			foreach($dateRange	as $date)
			{
				$this->users->delete_attendence($leave_detail->userid,$date);
			}
			
		}
	}
//-----------------------------------------------------------------------

	/*
	*	HR Response for Leave Request
	*	@param $salaryid integer
	*/
	function hr_response()
	{
			$hr_comment	=	$this->input->post('hr_comment');
			$hr_id		=	$this->input->post('hr_id');
			$leaveid	=	$this->input->post('leaveid');
			$status		=	$this->input->post('status');
			
			
			$hr_response	=	array(
										'hr_comment'	=>	$hr_comment,
										'leave_req_id'	=>	$leaveid,
										'hr_date'		=>	date('Y-m-d'),
										'status'		=>	$status,
										'hr_id'			=>	$hr_id);
			// Add HR Response
			$this->users->add_hr_response($leaveid,$hr_response);
	}
//-----------------------------------------------------------------------

	/*
	*	Add / Update Salary
	*	@param $salaryid integer
	*/
	public function add_extra_things($userid	=	NULL,$extra_id	=	NULL)
	{
		$this->_data['userid']	=	$userid;
		
		if($this->input->post())
		{
			// Get all values from POST
			$data	=	$this->input->post();
			$table_id	=	$data['data_table_id'];
			
			// UNSET the value from ARRAY
			unset($data['submit']);
			unset($data['data_table_id']);
			
			if($_FILES["certificate"]["tmp_name"])
			{
				$certificate			=	$this->upload_file($data['userid'],'certificate','resources/users');
				$data['certificate']	=	$certificate;
			}
			
			if($data['extra_id'])
			{
				// Update data into database
				$this->users->update_extra_things($data['extra_id'],$data);
			}
			else
			{
				// ADD data into database
				$this->users->add_extra_things($data);
			}
			
			// Redirect to listing page
			echo $url	=	base_url().'users/add_user/'.$data['userid'].'/'.$table_id;
		}
		else
		{ 
			$extra_things	=	$this->users->get_extra_things_by_id($extra_id);
			
			$this->_data['extra_id']		=	isset($extra_things->extra_id) ? $extra_things->extra_id : NULL;
			$this->_data['title']			=	isset($extra_things->title) ? $extra_things->title : NULL;
			$this->_data['amount']			=	isset($extra_things->amount) ? $extra_things->amount : NULL;
			$this->_data['submit_date']		=	isset($extra_things->submit_date) ? date('Y-m-d',strtotime($extra_things->submit_date)) : NULL;
			$this->_data['notes']			=	isset($extra_things->notes) ? $extra_things->notes : NULL;
			
			// Load Users Listing Page
			$this->load->view('add-extra-things',$this->_data);
		}
	}
//-----------------------------------------------------------------------

	/*
	*	Add / Update Salary
	*	@param $salaryid integer
	*/
	public function add_insurance_things($userid	=	NULL,$extra_id	=	NULL)
	{
		$this->_data['userid']	=	$userid;
		
		if($this->input->post())
		{
			// Get all values from POST
			$data	=	$this->input->post();
			$table_id	=	$data['data_table_id'];
			
			// UNSET the value from ARRAY
			unset($data['submit']);
			unset($data['data_table_id']);
			
			if($_FILES["certificate"]["tmp_name"])
			{
				$certificate			=	$this->upload_file($data['userid'],'certificate','resources/users');
				$data['certificate']	=	$certificate;
			}
			
			if($data['extra_id'])
			{
				// Update data into database
				$this->users->update_extra_things($data['extra_id'],$data);
			}
			else
			{
				// ADD data into database
				$this->users->add_extra_things($data);
			}
			
			// Redirect to listing page
			echo $url	=	base_url().'users/add_user/'.$data['userid'].'/'.$table_id;
		}
		else
		{ 
			$extra_things	=	$this->users->get_extra_things_by_id($extra_id);
			
			$this->_data['extra_id']		=	isset($extra_things->extra_id) ? $extra_things->extra_id : NULL;
			$this->_data['title']			=	isset($extra_things->title) ? $extra_things->title : NULL;
			$this->_data['amount']			=	isset($extra_things->amount) ? $extra_things->amount : NULL;
			$this->_data['submit_date']		=	isset($extra_things->submit_date) ? date('Y-m-d',strtotime($extra_things->submit_date)) : NULL;
			$this->_data['notes']			=	isset($extra_things->notes) ? $extra_things->notes : NULL;
			
			// Load Users Listing Page
			$this->load->view('add-insurance-things',$this->_data);
		}
	}
	
//-----------------------------------------------------------------------

	/*
	*	Add / Update Salary
	*	@param $salaryid integer
	*/
	public function add_deduction_things($userid	=	NULL,$extra_id	=	NULL)
	{
		$this->_data['userid']	=	$userid;
		
		if($this->input->post())
		{
			// Get all values from POST
			$data	=	$this->input->post();
			$table_id	=	$data['data_table_id'];
			
			// UNSET the value from ARRAY
			unset($data['submit']);
			unset($data['data_table_id']);
			
			if($_FILES["certificate"]["tmp_name"])
			{
				$certificate			=	$this->upload_file($data['userid'],'certificate','resources/users');
				$data['certificate']	=	$certificate;
			}
			
			if($data['extra_id'])
			{
				// Update data into database
				$this->users->update_extra_things($data['extra_id'],$data);
			}
			else
			{
				// ADD data into database
				$this->users->add_extra_things($data);
			}
			
			// Redirect to listing page
			echo $url	=	base_url().'users/add_user/'.$data['userid'].'/'.$table_id;
		}
		else
		{ 
			$extra_things	=	$this->users->get_extra_things_by_id($extra_id);
			
			$this->_data['extra_id']		=	isset($extra_things->extra_id) ? $extra_things->extra_id : NULL;
			$this->_data['title']			=	isset($extra_things->title) ? $extra_things->title : NULL;
			$this->_data['amount']			=	isset($extra_things->amount) ? $extra_things->amount : NULL;
			$this->_data['submit_date']		=	isset($extra_things->submit_date) ? date('Y-m-d',strtotime($extra_things->submit_date)) : NULL;
			$this->_data['notes']			=	isset($extra_things->notes) ? $extra_things->notes : NULL;
			
			// Load Users Listing Page
			$this->load->view('add-deduction',$this->_data);
		}
	}		
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/
	
	public function download_file($userid,$file_name)
	{
		$path	= 'resources/users/'.$userid.'/'.$file_name;

		// Download File
		downloadFile($path,$file_name);
	}	
//-------------------------------------------------------------------------------

	/*
	*
	* Sending Email Function
	* @param $to string
	* @param $from string
	* @param $subject string
	* @param $message string
	* @param $path string
	*
	*/
	public function send_email($to	=	NULL,$from	=	NULL,$subject	=	NULL,$message = NULL,$path	=	NULL) 
	{
		$config = array();
        $config['protocol'] 	= 'sendmail';
        $config['mailpath'] 	= '/usr/sbin/sendmail';
        $config['charset'] 	 	= 'iso-8859-1';
        $config['smtp_host'] 	= "localhost";
        $config['smtp_port'] 	= "25";
        $config['mailtype'] 	= 'html';
        $config['charset']  	= 'utf-8';
        $config['newline']  	= "\r\n";
        $config['wordwrap'] 	= TRUE;
		
		$this->load->library('email');
		
		$this->email->initialize($config);
	  	$this->email->from('no-reply@durat.com', 'No Reply' );
		$this->email->to($from);
		$this->email->subject($subject);
		$this->email->message($message);
		
		if($path)
		{
		 	$this->email->attach($path);
		}
		
		// Send Email 
		$this->email->send();
	}
//-----------------------------------------------------------------------

	/*
	*	File Uploading
	*	@param $userid integer
	*	@param $filefield integer Input File name
	*	@param $folder integer older name where Image Upload
	*	@param $width integer
	*	@param $height integer
	*/
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
				
		if (!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
//-----------------------------------------------------------------------
	/*
	*	Delete Branch
	*/	
	public function delete_salary($salaryid)
	{
		$this->users->delete_salary($salaryid);
		
		redirect(base_url().'users/branchs');
		exit();
	}
//-----------------------------------------------------------------------

	/*
	*	Delete Branch
	*/	
	public function delete_branch($branch_id)
	{
		$this->users->delete_branch($branch_id);
		
		redirect(base_url().'users/branchs');
		exit();
	}
//-----------------------------------------------------------------------

	/*
	*	Delete Branch
	*/	
	public function delete_bank_account($accountid)
	{
		$this->users->delete_bank_account($accountid);
		
		redirect(base_url().'users/bank_accounts');
		exit();
	}
//-----------------------------------------------------------------------

	/*
	*	Home Page
	*/	
	public function delete_user($userid)
	{
		$this->users->delete_user($userid);
		
		redirect(base_url().'users');
		exit();
	}
//-----------------------------------------------------------------------

	/*
	*	Delete User Leave Request
	*/	
	public function delete_leave_request($leaveid)
	{
		$this->users->delete_leave_request($leaveid);
		
		redirect(base_url().'users/leave_requests');
		exit();
	}
//-----------------------------------------------------------------------

	/*
	*	Delete Communication
	*/	
	public function delete_communication($communicationid)
	{
		$this->users->delete_communication($communicationid);
	}
//-----------------------------------------------------------------------

	/*
	*	Delete Family Member
	*/	
	public function delete_family_member($fmemeberid)
	{
		$this->users->delete_family_member($fmemeberid);
	}
//-----------------------------------------------------------------------

	/*
	*	Delete Family Member
	*/	
	public function delete_document($docid)
	{
		$this->users->delete_document($docid);
	}
//-----------------------------------------------------------------------

	/*
	*	Delete User Exeperience Detail
	*/	
	public function delete_experience($exeid)
	{
		$this->users->delete_exeid($exeid);
	}
//-----------------------------------------------------------------------

	/*
	*	Delete User Basic Salary Detail
	*/	
	public function delete_basic_salary($basicsalid)
	{
		$this->users->delete_basic_salary($basicsalid);
	}		
	
//-----------------------------------------------------------------------

	/*
	*	Date Range 
	*/		
	public function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d') 
	{
		$dates		=	array();
		$current	=	strtotime($first);
		$last 		=	strtotime($last);
	
		while($current <= $last) 
		{
			$dates[] = date($format, $current);
			$current = strtotime($step, $current);
		}
	
		return $dates;
	}
//-----------------------------------------------------------------------

	/*
	*	Date Range 
	*/		
	public function dateDifference($first, $last) 
	{
		$date1	=	date_create($first);
		$date2	=	date_create($last);
		
		
		$diff	=	date_diff($date1,$date2);
		
		if($first	==	$last)
		{
			return 1;
		}
		else
		{
			return $diff->format("%a");
		}
	}	

//-----------------------------------------------------------------------
/* End of file users.php */
/* Location: ./application/modules/users/users.php */
}
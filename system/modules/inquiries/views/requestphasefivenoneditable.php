<!doctype html>
<?PHP
	$ah_applicant = $_applicant_data['ah_applicant'];
	$ah_applicant_wife = $_applicant_data['ah_applicant_wife'];
	$ah_applicant_survayresult = $_applicant_data['ah_applicant_survayresult'];
	$ah_applicant_relation = $_applicant_data['ah_applicant_relation'];
	$ah_applicant_family = $_applicant_data['ah_applicant_family'];
	$ah_applicant_economic_situation = $_applicant_data['ah_applicant_economic_situation'];
	$ah_applicant_documents = $_applicant_data['ah_applicant_documents'];	
?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
  <form id="add_decission" name="add_decission" method="post" action="<?PHP echo base_url().'inquiries/add_decission' ?>" autocomplete="off">   
    <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $ah_applicant->applicantid;?>" />
    <input type="hidden" name="sarvayid" id="sarvayid" value="<?php echo $ah_applicant_survayresult->sarvayid;?>" />
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module, 'headxxx' => $heading)); ?>
      <div class="col-md-12">
        <div class="col-md-5 fox leftborder">
          <h4 class="panel-title customhr">بيانات طلب مساعدة</h4>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الاستمارة: </label>
            <strong><?PHP echo arabic_date($ah_applicant->applicantcode); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">التاريخ: </label>
            <strong><?PHP echo show_date($ah_applicant->registrationdate,5); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">نوع الطلب: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->charity_type_id); ?> </strong> </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الاسم الرباعي والقبيلة: </label>
            <strong><?PHP echo $ah_applicant->fullname; ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم جواز سفر: </label>
            <strong><?PHP echo arabic_date($ah_applicant->passport_number); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم البطاقة الشخصة: </label>
            <strong><?PHP echo arabic_date($ah_applicant->idcard_number); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">المحافظة \ المنطقة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->province); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">ولاية: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->wilaya); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">البلدة \ المحلة: </label>
            <strong><?PHP echo $ah_applicant->address; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">هاتف المنزل: </label>
            <strong><?PHP echo arabic_date($ah_applicant->hometelephone); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الهاتف :</label>
            <strong><?PHP echo arabic_date(implode('<br>',json_decode($ah_applicant->extratelephone,TRUE))); ?></strong></div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة الجتماعية:</label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->marital_status); ?></strong> </div>
          <?PHP foreach($ah_applicant_wife as $wife) { ?>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم الزوج\الزوجة: </label>
            <strong><?PHP echo $wife->relationname; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الجواز\البطاقة: </label>
            <strong><?PHP echo arabic_date($wife->relationdoc); ?></strong> </div>
          <?PHP } ?>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم الام: </label>
            <strong><?PHP echo $ah_applicant->mother_name; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الجواز\البطاقة: </label>
            <strong><?PHP echo arabic_date($ah_applicant->mother_id_card); ?></strong> </div>
          <!-----------------Documents--------------------------> 
          <!---------------------------------------------------->
          <div class="col-md-12 form-group">
            <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
              <h4 class="panel-title customhr">اترفق الوثائق والمستندات الازمة لطلب</h4>
              <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                <?PHP 
					$doccount = 0;					
					foreach($this->inq->allRequiredDocument($ah_applicant->charity_type_id) as $ctid) { $doccount++; 
						$doc = $ah_applicant_documents[$ctid->documentid];						
						$url = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$doc->document;
					?>
                <div class="panel panel-default" style="border-bottom: 1px solid #ddd;">
                  <div class="panel-heading" style="padding:7px 3px !important;" id="head<?PHP echo $ctid->documentid;?>">
                    <h4 class="panel-title" style="font-size:12px; font-weight:normal !important;">
                      <?PHP if($doc->appli_doc_id!='') { ?>
                      <span class="icons" id="removeicons<?PHP echo $doc->appli_doc_id; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> </span>
                      <?PHP } ?>
                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $ctid->documentid;?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                  </div>
                </div>
                <?PHP } ?>
              </div>
            </div>
          </div>
          <h4 class="panel-title customhr">افراد الأسرة: (بما فهيم العاملين و غيرالعاملين) </h4>
          <div class="col-md-12 form-group">
            <table class="table table-bordered table-striped dataTable">
              <thead>
                <tr role="row">
                  <th>الاسم</th>
                  <th style="width: 50px;">السن</th>
                  <th>القرابة</th>
                  <th>المهنة</th>
                  <th style="width: 70px;">الدخل الشهري</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?PHP 
			  
			  if($ah_applicant_relation)
			  {	
			  	foreach($ah_applicant_relation as $apr) { ?>
                <tr>
                  <td><?PHP echo $apr->relation_fullname; ?></td>
                  <td><?PHP echo arabic_date($apr->age); ?></td>
                  <td><?PHP echo $this->haya_model->get_name_from_list($apr->relationtype); ?></td>
                  <td><?PHP echo $this->haya_model->get_name_from_list($apr->professionid); ?></td>
                  <td><?PHP echo arabic_date($apr->monthly_income); ?></td>
                </tr>
                <?PHP }
			  }
			 ?>
              </tbody>
            </table>
          </div>
          <h4 class="panel-title customhr">االحالة الاقتصادية: </h4>
          <div class="col-md-6 form-group">
            <label class="text-warning">الدخل: </label>
            <strong><?PHP echo arabic_date($ah_applicant->salary); ?></strong> </div>
          <div class="col-md-6 form-group">
            <?PHP if($ah_applicant->source=='يوجد') { echo('يوجد: يوضح كالاتي'); } elseif($ah_applicant->source=='لايوجد') { echo('لايوجد: ويوضح مصدر المعيشة'); } ?>
          </div>
          <?PHP if($ah_applicant->source=='يوجد') { ?>
          <div class="col-md-6 form-group">
            <label class="text-warning">مصدره: </label>
            <strong><?PHP echo $ah_applicant->musadra; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">قيمتة: </label>
            <strong><?PHP echo $ah_applicant->qyama; ?></strong> </div>
          <?PHP } ?>
          <?PHP if($ah_applicant->source=='لايوجد') { ?>
          <div class="col-md-12 form-group">
            <label class="text-warning">مصدر المعيشة: </label>
            <strong><?PHP echo $ah_applicant->lamusadra; ?></strong> </div>
          <?PHP } ?>
          <div class="col-md-12 form-group">
            <label class="text-warning">وضف الحالة السكنية: </label>
            <?PHP echo $ah_applicant->current_situation; ?> </div>
          <div class="col-md-7 form-group">
            <label class="text-warning"><strong><?PHP echo $ah_applicant->ownershiptype; ?></strong> </label>
          </div>
          <div class="col-md-5 form-group"> <strong><?PHP echo arabic_date($ah_applicant->ownershiptype_amount); ?></strong> </div>
          <div class="col-md-12">
            <h4 class="panel-title customhr">مكونات المنزل:</h4>
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">نوع البناء:</label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->building_type); ?></strong> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">عدد الغرف:</label>
            <strong><?PHP echo arabic_date($ah_applicant->number_of_rooms); ?></strong> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">المرافق:</label>
            <strong><?PHP echo arabic_date($ah_applicant->utilities); ?></strong> </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">حالة الأثاث والموجودات:</label>
            <strong><?PHP echo $ah_applicant->furniture; ?><strong> </div>
          <div class="col-md-12">
            <h4 class="panel-title customhr">تفاصيل البنك:</h4>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم البنك:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant->bankid); ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الفرع:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant->branchid); ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الحساب:</label>
            <?PHP echo $ah_applicant->accountnumber; ?>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">نوع الحساب: جاري \ توفير: </label>
            <?PHP echo($ah_applicant->accounttype); ?>           
          </div>
          <?PHP if($ah_applicant->bankstatement) { ?>
          <div class="col-md-12 form-group">
            <label class="text-warning">يرفق صورة من كشف الحسابات (حديث الاصدار):</label>           
            <?PHP
          	$statmentURL = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->bankstatement;
			echo getFileResult($statmentURL,'يرفق صورة من كشف الحسابات (حديث الاصدار)',$statmentURL);
		  ?>
          </div>
          <?PHP } ?>
        </div>
        <div class="col-md-7 fox">       
          <h4 class="panel-title customhr">بعد إجراء البحث الاجتماعي والاطلاع على المستندات المؤيدة اتضح مايلي :-</h4>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة الصحية:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->health_condition); ?>
			
            
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة السكنية:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->housing_condition); ?>
            
            
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">عدد أفراد الأسرة:</label>
            <?PHP echo arabic_date($ah_applicant_survayresult->numberofpeople); ?>
            
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">ترتيب مقدم الطلب بالأسرة:</label>
            <?PHP echo arabic_date($ah_applicant_survayresult->positioninfamily); ?>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الحالة الاقتصادية:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->economic_condition); ?>            
            
          </div>
          <div class="col-md-12 form-group">
            <h4 class="panel-title customhr">نوع الحالة (<?PHP echo $ah_applicant_survayresult->casetype; ?>):-</h4>           
         </div>
          <div class="col-md-12 form-group casetype" id="zamanya" <?PHP if($ah_applicant_survayresult->casetype=='للحالات الضمانية') { ?> style="display:block !important;" <?PHP } ?>>
            <div class="col-md-6 form-group">
              <label class="text-warning">الحالة الضمانية باسم:</label>
              <?PHP echo $ah_applicant_survayresult->aps_name; ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الحاسب:</label>
              <?PHP echo $ah_applicant_survayresult->aps_account; ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الملف:</label>
              <?PHP echo $ah_applicant_survayresult->aps_filename; ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الفئة:</label>
              <?PHP echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->aps_category); ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">قيمة المعاش:</label>
              <?PHP echo arabic_date($ah_applicant_survayresult->aps_salaryamount); ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">تاريخ الربط:</label>
              <?PHP echo arabic_date(date('d/m/Y',strtotime($ah_applicant_survayresult->aps_date))); ?>
            </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">مصادر دخل أخرى:</label>
              <?PHP $json = json_decode($ah_applicant_survayresult->aps_another_income,TRUE);
			  		$inx = 0;
			  		for($mdi=1; $mdi<=4; $mdi++) 
					{ 
						if($json[$inx]) 
						{ 
							echo('<li>'.$json[$inx].'<li>');
							$inx++; 
						} 
					} 
				?>
            </div>
          </div>
          <div class="col-md-12 form-group casetype" id="ghairzamanya"  <?PHP if($ah_applicant_survayresult->casetype=='للحالات غير الضمانية') { ?> style="display:block !important;" <?PHP } ?>>
            <div class="col-md-12 form-group">
              <label class="text-warning">مصادر دخل أخرى:</label>
              <?PHP $inxp = 0;
			  		for($mdix=1; $mdix<=4; $mdix++) 
					{	if($json[$inxp])
						{
							echo('<li>'.$json[$inxp].'<li>');
							$inxp++; 
						}
					}?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">اجمالي الدخل الشهري للفرد:</label>
              <?PHP echo $ah_applicant_survayresult->aps_month; ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">اجمالي الدخل الشهري للاسرة:</label>
              <?PHP echo $ah_applicant_survayresult->aps_year; ?>
            </div>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الغرض من المساعدة:</label>
            <?PHP echo $ah_applicant_survayresult->whyyouwant; ?>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">ملخص الحالة:</label>
            <?PHP echo $ah_applicant_survayresult->summary; ?>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">رأي الباحث الاجتماعي و مبرراته:</label>
            <?PHP echo $ah_applicant_survayresult->review; ?>
          </div>
          <div class="col-md-12 form-group">
			<?PHP foreach(applicant_status() as $askey => $as) { ?>
                <label class="text-warning"><?PHP echo $as; ?></label>
                <input type="radio" name="application_status" data-id="x<?PHP echo $askey; ?>" id="application_status" class="apstatus" value="<?PHP echo $as; ?>">
            <?PHP } ?>
          </div>
          <div class="col-md-12 form-group" id="notess">
          	<textarea id="notes" name="notes"></textarea>
          </div>
          <div class="col-md-12 form-group">
          <?PHP echo $this->haya_model->module_button(0,$ah_applicant->applicantid,'save_decission',$ah_applicant->case_close); ?>
         
          </div>
        </div>
        
      </div>
    </div>
    </div>
  </form>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script src="<?PHP echo base_url(); ?>assets/js/tasgeel_js.js"></script>
</body>
</html>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="col-md-12">
            <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid">
              <div class="row table-header-row" style="  margin-top: 7px;
  margin-bottom: 7px;
  background-color: #FFF;
  padding: 7px;">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="col-md-1 text-warning">أفضلية:</div>
                    <?PHP foreach(appointment_status(6) as $priority => $pvalue) { 
										if($priority==1)
										{	$color = "class1";	}
										else if($priority==2)
										{	$color = "class2";	}
										else
										{	$color = "class3";	}
								?>
                    <div class="col-md-2"><i style="font-size: 13px;" class="icon-circle <?PHP echo $color; ?>"> <?PHP echo $pvalue; ?></i></div>
                    <?PHP } ?>
                  </div>
                  <div class="col-md-6">
                    <div class="col-md-1 text-warning">الحالات:</div>
                    <?PHP foreach(appointment_status(7) as $statuskey => $statusvalue) { 
							if($statuskey!='sms') { 
					?>
                    <div class="col-md-2"><i  style="font-size: 13px;" class="icon-flag <?PHP echo $statuskey; ?>"> <?PHP echo $statusvalue; ?></i></div>
                    <?PHP } } ?>
                  </div>
                </div>
              </div>
              <table class="table table-bordered table-striped dataTable" id="tableSortable" >
                <thead>
                  <tr role="row">
                    <th style="text-align:center;width: 52px;">الحالات</th>
                    <th style="text-align:center;">اسم</th>
                    <th style="text-align:center; width:80px;">تاريخ موعد</th>
                    <th style="text-align:center;">موضوع</th>
                    <th style="text-align:center; width:80px;">رقم الهاتف</th>
                    <th style="text-align:center;">المحافظة / الولاية</th>
                    <th style="text-align:center;">الموظفين</th>
                    <th style="text-align:center;">الإجراءات</th>
                  </tr>
                </thead>
                <tbody role="alert" aria-live="polite" aria-relevant="all">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'ajax/appointment_list/',
'columns_array'=>'
					{ "data": "الحالات" },
					{ "data": "اسم" },
					{ "data": "تاريخ موعد" },
					{ "data": "موضوع" },
					{ "data": "رقم الهاتف" },
					{ "data": "المحافظة / الولاية" },
					{ "data": "اسم فيستر" },
					{ "data": "الإجراءات"}')); ?>
</body>
</html>
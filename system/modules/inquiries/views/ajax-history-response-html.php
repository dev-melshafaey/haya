<div class="row" id="noah_form" style="display:none;">
  <form name="noahform" id="noahform" method="post" autocomplete="off">
    <script>
  	$(function(){
		$('.child').hide();
		$('.inquirytypeid').click(function(){			
			var val = $(this).val();
			var chd = '.child_'+val;
			if($(this).is(':checked'))
			{
				$(chd).slideDown('slow');
			}	
			else
			{
				$(chd).slideUp('slow');
			}
		});	
		
		$('.childxxxxx').click(function(){			
			var val = $(this).val();
			var chd = '.smallchild_'+val;
			if($(this).is(':checked'))
			{
				$(chd).slideDown('slow');
			}	
			else
			{
				$(chd).slideUp('slow');
			}
		});	
		
		
		
	});
  </script>
    <input type="hidden" id="tempid" value="<?PHP echo $tempid; ?>" name="tempid">
    <div class="col-md-12">
      <div class="panel panel-default panel-block">
        <div class="nav nav-tabs panel panel-default panel-block" style="padding: 11px 0px;">
          <div class="col-md-6">
            <div class="list-group-item" id="checkboxes-and-radios">
              <h4 class="section-title" style="padding-bottom: 2px; margin-bottom: 0px;">نوع الإستفسار</h4>
              <div class="form-group">
                <div class="multibox">
                  <?PHP inquiry_type_tree($main->tempid); ?>
                </div>
              </div>
            </div>
            <div class="col-md-6" style="margin-top:10px;">
              <div class="form-group">
                <button type="button" onClick="save_noah_form();" class="btn btn-success">حفظ</button>
                <button type="button"  onClick="hide_noah_form();" class="btn btn-error">إلغاء</button>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="list-group-item" id="checkboxes-and-radios">
              <h4 class="section-title" style="padding-bottom: 2px; margin-bottom: 0px;">تفاصيل الإستفسار</h4>
              <div class="form-group">
                <label for="text-area-auto-resize"></label>
                <div>
                  <textarea class="form-control auto-resize txt_textarea" name="inquiry_text" id="inquiry_text"><?PHP echo $main->inquiry_text; ?></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="list-group-item" id="checkboxes-and-radios">
              <h4 class="section-title" style="padding-bottom: 2px; margin-bottom: 0px;">ملاحظات الموظف</h4>
              <div class="form-group">
                <label for="text-area-auto-resize"></label>
                <div>
                  <textarea class="form-control auto-resize txt_textarea" data-handler="<?PHP echo $main->tempid; ?>" name="notestext" id="notestext"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<!---------------------------------------------------> 
<!---------------------------------------------------> 
<!--------------------------------------------------->

<div class="row" style="direction:rtl !important;">
  <div id="noah">
    <div class="list-group-item" id="checkboxes-and-radios">
      <div class="form-group">
        <button type="button" onClick="show_noah_form();" class="btn btn-success">إضافة الإستفسارات</button>
      </div>
    </div>
    <div id="historyPrint">
      <div class="col-md-12">
        <?PHP 
  	
  	foreach($history as $key => $value) { 
?>
        <table style="text-align:center;" class="table" id="tableSortable3" aria-describedby="tableSortable_info">
          <thead>
            <tr role="row">
              <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="2" style="text-align:center; background-color: #000; color: #FFF;"> <div class="table-responsive">
                 
                  <div class="col-md-6"><a href="#">الساعه</a><br>
                    <?PHP echo $value['system_time']?></div>
                  <div class="col-md-6"><a href="#">تاريخ</a><br>
                    <?PHP echo $value['system_date']?></div>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="gradeA odd" bgcolor="#f5f5f5">
              <td colspan="2" width="303" class=" sorting_1"  style="background-color:#FFF !important;"><strong style="font-size:19px !important;" class="text-warning"><?PHP echo $value['admin_name']?></strong></td>
            </tr>
            <?PHP if($value['notes']!='') { ?>
            <tr class="gradeA even">
              <td colspan="2" align="right" class=" sorting_1" style="background-color:#EFEFEF !important;"><h4>
                  <p>الملاحظات</p>
                </h4>
                <p><?PHP echo $value['notes']?></p></td>
            </tr>
            <?PHP } ?>
            <?PHP if($value['ntext']!='') { ?>
            <tr class="gradeA even">
              <td class=" sorting_1"  style="background-color:#FFF !important; border-bottom: 2px solid #d09c0d;"><strong>تفاصيل الإستفسار </strong></td>
              <td class=" sorting_1" align="right"  style="background-color:#FFF !important; border-bottom: 2px solid #d09c0d;"><p> <?PHP echo $value['ntext']; ?> </p></td>
            </tr>
            <?PHP } ?>
            <tr class="gradeA odd">
              <td class=" sorting_1"  style="background-color:#FFF !important; border-bottom: 2px solid #d09c0d;"><strong> نوع الإستفسار </strong></td>
              <td class=" sorting_1" align="right"  style="background-color:#FFF !important; border-bottom: 2px solid #d09c0d;"><p>
                  <?PHP foreach($value['inq'] as $inqkey => $inqdata) { ?>
                  <span class="text-success"><?PHP echo $inqdata['name']; ?></span><br />
                  <?PHP } ?>
                </p></td>
            </tr>
          </tbody>
        </table>
        <?PHP } ?>
      </div>
    </div>
  </div>
</div>
<div class="row" style="text-align:center !important;">
  <button type="button" id="" onClick="printthepage('historyPrint');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
</div>

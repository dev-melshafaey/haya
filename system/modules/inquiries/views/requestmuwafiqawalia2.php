<!doctype html>
<script type="text/javascript">
	///deleteimages
	function add_requestmuwafiq() {
	    $.ajax({
            url: config.BASE_URL + 'inquiries/add_request/',
            type: "POST",
            data: $("#request_form").serialize(),
            dataType: "html",
            beforeSend: function () {
                $('#loaderModel').modal({ width: 400 });
            },
            complete: function () {
                $('#loaderModel').modal('hide');
            },
            success: function (msg) {
               // show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
            }
        });
    
}
	function deleteFile(ind){
		//alert('delete');
		//del = $(obj).index();
		//ob = obj;
		//console.log(ob);
		//alert(del);	
			/*if($(ob).hasClass('doc8remove')){
				ind = $(ob).index();
				console.log(ind);	
			}*/
			image_name = $('.doc8'+ind).val();
			
			$.ajax({
							url: config.BASE_URL+'inquiries/delete_image/',
							type: "POST",
							data:{'image':image_name},
							dataType: "html",
							success: function(msg){
								if(msg){
									
									$(".doclink"+ind).remove();
									$('.doc8'+ind).remove();
									$('.doc8remove'+ind).remove();
																
								}
						  }
					});
			
			
			
			
			
	}
</script>
<?PHP
	$applicant = $applicant_data['applicants'];
	$project = $applicant_data['applicant_project'][0];
	$loan = $applicant_data['applicant_loans'][0];
	$evo  = $applicant_data['project_evolution'][0];
	$phones = $applicant_data['applicant_phones'];
	$comitte = $applicant_data['comitte_decision'][0];
	
	//$applicant->document_name_1;
	
	$applicant_document[1] = $applicant->document_name_1;
	$applicant_document[2] = $applicant->document_name_2;
	$applicant_document[3] = $applicant->document_name_3;
	$applicant_document[4] = $applicant->document_name_4;
	$applicant_document[5] = $applicant->document_name_5;
	$applicant_document[6] = $applicant->document_name_6;
	$applicant_document[7] = $applicant->document_name_7;
	$applicant_document[8] = $applicant->document_name_8;
				
		$uploaderArray = array(
	'icon-upload-alt pull-left'=>'بطاقة سجل القوى العاملة',
	'icon-user pull-left'=>'البطاقة الشخصية',
	'icon-certificate pull-left'=>'شهادة عدم محكومية',
	'icon-briefcase pull-left'=>'دراسة الجدوى الإقتصادية للمشروع',
	'icon-camera-retro pull-left'=>'صورة شمسية',
	'icon-wrench pull-left'=>'شهادات الخبرة / التدريب',
	'icon-umbrella pull-left'=>'بطاقة الضمان الاجتماعي'
	);
	
	//echo "<pre>";
///print_r($setting_data);
	//$setting_data-condition_text;
	
	$fullname = $applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name;
	foreach($phones as $p)

	{	$ar[] = arabic_date('986'.$p->applicant_phone);	}
		$applicantphone = implode('<br>',$ar);
		$percentage = $loan->loan_percentage*0.001;
  		$val_p = $evo->total_cost*$percentage;
		$instalment_points = json_decode($guarantee_data->instalment_points,true);

?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
  <form id="request_form" name="request_form" method="post" action="<?PHP echo base_url().'inquiries/update_muwafiq' ?>" autocomplete="off">
    <input type="hidden" name="form_step" id="form_step" value="5" />
    <input type="hidden" name="applicant_id" id="applicant_id" value="<?php echo $applicant->applicant_id;?>" />
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="list-group">
            <div class="list-group-item" id="input-fields">
              <div class="form-group">
                <h4 class="section-title preface-title text-warning"> <?PHP echo $fullname; ?> <span class="label label-default">رقم التسجيل <?php echo arabic_date(applicant_number($applicant_id)); ?></span></h4>
              </div>
              <div class="row">
                <div class="col-md-2">
                  <label class="text-warning">تاريخ</label>
                  <label><strong><?php echo show_date($applicatnt->applicant_regitered_date,1); ?></strong></label>
                </div>
                <div class="col-md-2">
                  <label class="text-warning">الموافق</label>
                  <label><strong><?php echo show_date(date(),1); ?></strong></label>
                </div>
                <div class="col-md-2">
                  <label class="text-warning">البرنامج المطلوب</label>
                  <label><strong><?php echo getLoanListType($loan->loan_limit); ?></strong></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                  <label class="text-warning">الاسم</label>
                  <label><strong><?PHP echo $fullname; ?></strong></label>
                </div>
                <div class="col-md-2">
                  <label class="text-warning">رقم الهاتف</label>
                  <label><strong><?PHP echo $applicantphone; ?></strong></label>
                </div>
                <div class="col-md-2">
                  <label class="text-warning">نشاط المشروع</label>
                  <label><strong><?php echo is_set($project->activity_project_text); ?></strong></label>
                </div>
                <div class="col-md-2">
                  <label class="text-warning">ص . ب</label>
                  <label><strong><?php echo is_set($applicant->zipcode); ?></strong></label>
                </div>
                <div class="col-md-2">
                  <label class="text-warning">ر . ب</label>
                  <label><strong><?php echo is_set($applicant->postalcode); ?></strong></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <label class="text-warning">العنوان</label>
                  <label><strong>أبراج الأصالة مكتب ٢١٦ الدور الثاني - الغبرة - مسقط</strong></label>
                </div>
              </div>
              <!--<div class="row uploader" id="drag0">
                <div class="col-md-8">
                  <label class="text-warning">تحميل</label>
                  <label>
                  <div class="browser">
                    <input style="border: 0px !important; color: #d09c0d;" type="file" name="files[]" multiple title='تحميل'>
                    <input type="hidden" name="attachment" id="attachment" />
                  </div>
                  </label>
                </div>
              </div>-->
              <!--<div class="row">
                <div class="panel-body demo-panel-files" id='demo-files0'> <span class="demo-note"></span> </div>
              </div>-->
            
            	<div class="col-lg-4">
    <div class="panel-group" id="demo-accordion" style="border-right: 1px solid #ddd;">
      <?PHP 
	  $applicant_doc_index = 1;
	  
	  foreach($uploaderArray as $tabkey => $tabvalue) 
	  { 
	  		$data_url = base_url().'upload_files/documents/'.$applicant_document[$applicant_doc_index];
			if($applicant_document[$applicant_doc_index]!='')
			{	$is_uploaded = 1;	}
			else
			{	$is_uploaded = 0;	}
						
	  ?>
      <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
        <div class="panel-heading" id="head<?PHP echo $applicant_doc_index;?>">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $applicant_doc_index; ?>"><?PHP echo $tabvalue; ?> <i class="<?PHP echo $tabkey; ?>"></i><?PHP if($applicant_document[$applicant_doc_index]!='') { echo('<i style="color:#00A603; float:left;" class="icon-ok-circle"></i>'); } ?></a> </h4>
        </div>
        <div id="demo-collapse<?PHP echo $applicant_doc_index; ?>" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
          <div class="panel-body multiple_uploader" data-index="<?PHP echo $applicant_doc_index; ?>" data-heading="<?PHP echo $tabvalue; ?>" id="drag<?php echo $applicant_doc_index; ?>">
            <div class="browser">
            </div>
            <div class="data<?PHP echo $applicant_doc_index; ?>">
            	<?PHP if($applicant_document[$applicant_doc_index]!='') 
				{	echo getFileResult($applicant_document[$applicant_doc_index],$tabvalue);	} ?>
            </div>
          </div>
        </div>
      </div>
      <?PHP 
	  	$applicant_doc_index++;
	  } 
	  
	  $data_url = base_url().'upload_files/documents/'.$applicant_document[8];
			if($applicant_document[8]!='')
			{	$is_uploaded = 1;	}
			else
			{	$is_uploaded = 0;	}

	  ?>
		      
      <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
        <div class="panel-heading" id="head8">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse8">موافقة أولية <i class="icon-umbrella pull-left"></i><?PHP if($applicant_document[8]!='') { echo('<i style="color:#00A603; float:left;" class="icon-ok-circle"></i>'); } ?></a> </h4>
        </div>
        <div id="demo-collapse8" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
          <div class="panel-body multiple_uploader2" data-index="8" data-heading="موافقة أولية" id="drag8">
            <div class="browser">
              <input type="hidden" data-id="8" id="document_uploaded_8" placeholder="موافقة أولية" class="dreq" value="<?PHP echo $is_uploaded; ?>">
              <input type="file" style="font-size: 11px;"  name="files[]" multiple="multiple" id="document_id8" title='موافقة أولية'>
            </div>
            <div class="data8">
            	<?PHP if(!empty($guarantee_attach)) {
						//echo "<pre>";
						//print_r($guarantee_attach);
						foreach($guarantee_attach as $i=>$g_attch){ 
						?>
                        html='<input type="hidden" name="document_name_8[<?php echo $i; ?>]" id="document_name_8" value="<?php echo $g_attch->gurarntee_image; ?>" class="doc8<?php echo $i; ?>">';
                        <a title="موافقة صورة " href="<?php echo base_url() ?>/upload_files/documents/<?php echo $g_attch->gurarntee_image; ?>" rel="gallery1" class="fancybox-button"><img style="width:60%; height:60%" src="<?php echo base_url(); ?>upload_files/documents/<?php echo $g_attch->gurarntee_image; ?>"></a>
                        <?php
						}
					} ?>
            </div>
          </div>
        </div>
      </div>
     
    </div>
  </div>  
  				
  				<?php
				
			//	echo "<pre>";
			//	print_r($loan);
				
				$percentage = $loan->applicant_percentage*0.001;
 				$val_p = $evo->total_cost*$percentage;
				?>	
  
  				<div class="col-md-8">
            <div class="form-group">
              <h4 class="section-title preface-title text-warning">1-استخدامات القرض</h4>
            </div>
            <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <tr class="gradeA even">
                  <td class="tdsmall" colspan="3" style="background-color:#a5c8e2; color:#FFF;"><strong>الاستثمار</strong></td>
                  <td class="tdsmall" colspan="3" style="background-color:#a5c8e2; color:#FFF;"><strong>التمويل</strong></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall" style="background-color:#a5c8e2; color:#FFF;"><strong>البند</strong></td>
                  <td class="tdsmall" colspan="2" style="background-color:#a5c8e2; color:#FFF;"><strong>التكلفة</strong></td>
                  <td class="tdsmall" style="background-color:#a5c8e2; color:#FFF;"><strong>البند</strong></td>
                  <td class="tdsmall" style="background-color:#a5c8e2; color:#FFF;"><strong>المبلغ(ر.ع)</strong></td>
                  <td class="tdsmall" style="background-color:#a5c8e2; color:#FFF;"><strong>لنسبة %</strong></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall" style="background-color:#a5c8e2; color:#FFF;"><strong>مصاريف ما قبل التشغيل</strong></td>
                  <td class="tdsmall" colspan="2"><input type="text" maxlength="10" size="15" placeholder="البند" id="evolution_pre_expenses" class="charges form-control xx NumberInput" value="<?PHP echo($evo->evolution_pre_expenses); ?>" name="evolution_pre_expenses" readonly></td>
                  <td class="tdsmall" style="background-color:#a5c8e2; color:#FFF;"><strong>مساهمة المقترض</strong></td>
                  <td class="tdsmall"><input maxlength="10" size="15" placeholder="مساهمة المقترض" id="contribute" class="charges form-control xx NumberInput " value="<?PHP echo $val_p; ?>" name="furniture_fixture" readonly type="text"></td>
                  <td class="tdsmall"><input maxlength="10" size="15" placeholder="مساهمة المقترض" id="contribute" class="charges form-control xx NumberInput" value="<?PHP echo($loan->applicant_percentage); ?>" name="furniture_fixture" readonly type="text"></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall" style="background-color:#a5c8e2; color:#FFF;"><strong>الآلات والمعدات</strong></td>
                  <td class="tdsmall" colspan="2"><input maxlength="10" size="15" placeholder="الآلات والمعدات" id="machinery_equipment" class="charges form-control xx NumberInput " value="<?PHP echo($evo->machinery_equipment); ?>" name="machinery_equipment" readonly type="text"></td>
                  <td class="tdsmall" rowspan="5" align="center" valign="middle" style="vertical-align:middle; background-color:#a5c8e2; color:#FFF;"><strong>قرض صندوق الرفد</strong></td>
                  <td colspan="2" rowspan="5" class="tdsmall" style="text-align: center;
vertical-align: middle;
font-size: 17px;
font-weight: bold;
letter-spacing: 1px;"><?PHP echo $evo->total_cost; ?></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall" style="background-color:#a5c8e2; color:#FFF;"><strong>اثاث وتركيبات</strong></td>
                  <td class="tdsmall" colspan="2"><input maxlength="10" size="15" placeholder="اثاث وتركيبات" id="furniture_fixture" class="charges form-control xx NumberInput " value="<?PHP echo($evo->furniture_fixture); ?>" name="furniture_fixture" readonly type="text"></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall" style="background-color:#a5c8e2; color:#FFF;"><strong>المركبات</strong></td>
                  <td class="tdsmall" colspan="2"><input maxlength="10" size="15" placeholder="المركبات" id="vehicles" class="charges form-control xx NumberInput" value="<?PHP echo($evo->vehicles); ?>" name="vehicles" readonly type="text"></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall" style="background-color:#a5c8e2; color:#FFF;"><strong>رأس المال العامل</strong></td>
                  <td class="tdsmall" colspan="2"><input maxlength="10" size="15" placeholder="رأس المال العامل" id="working_capital" class="charges form-control xx NumberInput" value="<?PHP echo($evo->working_capital); ?>" name="working_capital" readonly type="text"></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall" style="background-color:#a5c8e2; color:#FFF;"><strong>المبلغ المخصص للبائع (بالنسبة لشراء المشاريع)</strong></td>
                  <td class="tdsmall" colspan="2"><input maxlength="10" size="15" placeholder="رأس المال العامل" id="seller_amount" class="charges form-control xx NumberInput " value="<?PHP echo($evo->seller_amount);?>" name="seller_amount" readonly type="text"></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall">الإجمالي</td>
                  <td class="tdsmall" colspan="2"><input maxlength="10" size="15" placeholder="الاجمالي" id="total_cost" class="form-control xx NumberInput" value="<?php echo($evo->total_cost); ?>" name="total_cost" readonly type="text"></td>
                  <td class="tdsmall">الإجمالي</td>
                  <td class="tdsmall" colspan="2"><input maxlength="10" size="15" placeholder="الاجمالي" id="total_cost" class="form-control xx NumberInput" value="<?PHP echo $evo->total_cost+$val_p; ?>" name="total_cost" readonly type="text"></td>
                </tr>
              </tbody>
            </table>
            </div>
            </div>
            
            <br />
            <div class="row">
              <div class="col-md-8">
                <label class="text-warning"><strong>يسرني إفادتكم بالموافقة المبدئية على طلبكم المتعلق بالحصول على قرض من صندوق الرفد وفقا للآتي:</strong></label>
              </div>
            </div>
            
            <br />
            <br />
            <div class="form-group">
              <h4 class="section-title preface-title text-warning">2-سداد القرض</h4>
            </div>
            <div class="row">
              <div class="col-md-2">
                <label class="text-warning">مبلغ القرض</label>
                <label><strong><?PHP echo(arabic_date($evo->total_cost+$val_p)); ?> ر.ع</strong></label>
              </div>
              <div class="col-md-3">
                <label class="text-warning">نسبة الرسوم الإدارية و الفنية</label>
                <label><strong>(<?php echo arabic_date($loan->loan_percentage); ?> %) في السنة</strong></label>
              </div>
              <div class="col-md-2">
                <label class="text-warning">فترة السماح</label>
                <label><strong><?php echo arabic_date($loan->leave_installmment); ?></strong></label>
              </div>
              <div class="col-md-3">
                <label class="text-warning">مدة سداد القرض</label>
                <label><strong>(<?php echo arabic_date($loan->paid_instalment); ?>) سنوات</strong></label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-10">
                <label class="text-warning">آلية السداد</label>
                <label><strong> أصل القرض
                  •	عدد الأقساط (<?php echo arabic_date($loan->leave_installmment); ?>) قسطا ربع سنوي 
                  •	البدء في السداد بعد فترة (<?php echo arabic_date($loan->paid_instalment); ?>) شهرا من تاريخ توقيع الاتفاقية 
                  ب-الرسوم الإدارية و المالية
                  لا تحتسب أية رسوم على فترة السماح المحدودة</strong></label>
              </div>
            </div>
            <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <tr class="gradeA even">
                  <td rowspan="8" align="center" valign="middle" style="vertical-align:middle;" >الضــــــمانــــــــات المطـــــــــلوبـــــــــة</td>
                  <td>قبل التوقيع على الاتفاقية:</td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox" id="insurance_borrower" name="gurantee_val[0]" value="1" <?php if(isset($gurantee_val[0])) { ?> checked="checked" <?php } ?> onClick="gurantee_values(this)" class="checkOption" />
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>التأمين على المقترض</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox" id="vehicle_insurance" name="gurantee_val[1]" value="1" <?php if(isset($gurantee_val[1])) { ?> checked="checked" <?php } ?> onClick="gurantee_values(this)"  class="checkOption"/>
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>توفير تأمين شامل على المركبات</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox"  id="owner_ship" name="gurantee_val[2]" value="1" <?php if(isset($gurantee_val[2])) { ?> checked="checked" <?php } ?> onClick="gurantee_values(this)"  class="checkOption"/>
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>رهن ملكية المركبات</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox" id="commercial_register" name="gurantee_val[3]" <?php if(isset($gurantee_val[3])) { ?> checked="checked" <?php } ?> onClick="gurantee_values(this)"  value="1" class="checkOption"/>
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>رهن السجل التجاري لدى وزارة التجارة والصناعة</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox" id="project_assets" name="gurantee_val[4]" <?php if(isset($gurantee_val[4])) { ?> checked="checked" <?php } ?> onClick="gurantee_values(this)"  value="1" class="checkOption"/>
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>رهن أصول المشروع (إن وجد)</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox" id="munciple_license" name="gurantee_val[5]" <?php if(isset($gurantee_val[5])) { ?> checked="checked" <?php } ?> onClick="gurantee_values(this)"  value="1" class="checkOption"/>
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>توفير الترخيص البلدي و عقد الإيجار للمحل</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox" id="commitment_project_management" name="gurantee_val[6]" <?php if(isset($gurantee_val[0])) { ?> checked="checked" <?php } ?>  onClick="gurantee_values(this)" value="1" class="checkOption"/>
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>الالتزام بالتفرغ الكلي لإدارة المشروع</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td align="center" valign="middle" style="vertical-align:middle;" >الشروط اللازمة</td>
                  <td class="tdsmall"><textarea style="width: 1255px; height: 149px;" class="ssForm form-control" id="conditions" name="conditions"><?php if($guarantee_data->conditions !="") echo  $guarantee_data->conditions ; else echo  $setting_data->condition_text; ?></textarea></td>
                </tr>
                <tr class="gradeA even">
                  <td rowspan="10" align="center" valign="middle" style="vertical-align:middle;" >آليات الصرف</td>
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span> </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>يصرف مبلغ القرض بعد موافقة صندوق الرفد كالتالي</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span> </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>صرف مبلغ الآلات والمعدات للمورد مباشرة</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox" id="exchange_first_pay" name="instalment_points[0]" value="1" <?php if(isset($instalment_points[0])) { ?> checked="checked" <?php } ?> onClick="gurantee_values(this)"    class="checkOption"/>
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>دفعة واحدة</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox" id="exchange_second_pay" name="instalment_points[1]" value="1" <?php if(isset($instalment_points[1])) { ?> checked="checked" <?php } ?> onClick="gurantee_values(this)" class="checkOption"/>
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>دفعتين</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span> </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>صرف مبلغ رأس المال في حساب المورد مباشرة</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox" id="captial_amount_first_pay" name="instalment_points[2]" value="1" <?php if(isset($instalment_points[2])) { ?> checked="checked" <?php } ?> onClick="gurantee_values(this)" class="checkOption"/>
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>دفعة واحدة</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox" id="capital_amount_second_pay" name="instalment_points[3]" value="1" <?php if(isset($instalment_points[3])) { ?> checked="checked" <?php } ?> onClick="gurantee_values(this)" class="checkOption"/>
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>دفعتين</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span> </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>صرف جزء القرض المخصص لشراء المشروع مباشرة لصاحب المشروع (البائع)</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox" id="exchange_allocation_first_pay" name="instalment_points[4]" value="1" <?php if(isset($instalment_points[4])) { ?> checked="checked" <?php } ?> onClick="gurantee_values(this)" class="checkOption"/>
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>دفعة واحدة</label>
                      </div>
                    </div></td>
                </tr>
                <tr class="gradeA even">
                  <td class="tdsmall"><div class="row">
                      <div class="col-lg-1 pull-right">
                        <div class="checker"><span>
                          <input type="checkbox" id="exchange_allocation_second_pay" name="instalment_points[5]" value="1" <?php if(isset($instalment_points[5])) { ?> checked="checked" <?php } ?> onClick="gurantee_values(this)" class="checkOption" />
                          </span></div>
                      </div>
                      <div class="form-group col-md-7">
                        <label>دفعتين</label>
                      </div>
                    </div></td>
                </tr>
              </tbody>
            </table>
            <div class="row">
              <div class="col-md-12" style="padding: 19px 50px; font-size: 20px;">
                <label>يرجى منكم التكرم بمراجعة بنك التنمية العماني في محافظتكم لاستكمال باقي الاجراءات الخاصة بالقرض, مع العلم أن في حالة عدم إبرام اتفاقية القرض مع بنك التنمية العماني في مدة أقصاها ستة أشهر من تاريخ هذه الرسالة يعتبر القرض لاغيا. <br />
                  و تفضلوا بقبول فائق الاحترام,,, <br />
                  <br />
                  ونس بن محمد النصري <br />
                  <br />
                  مدير فرع صندوق الرفد بمحافظة مسقط <br />
                  <br />
                  وصل التسليم <br />
                  <br />
                  تاريخ التسليم: ( / / ) <br />
                  <br />
                  الموضوع :______________________________ <br />
                  <br />
                  الاسم والتوقيع:</label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12"  style="padding: 19px 50px;">
                <button type="button" id="save_data_form" name="save_data_form" class="btn btn-success btn-lg" onClick="add_requestmuwafiq();">حفظ</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </form>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>


</body>
</html>
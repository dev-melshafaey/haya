<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
    <?php $this->load->view('common/logo'); ?>
    <?php $this->load->view('common/usermenu'); ?>
    <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
    <?php $this->load->view('common/quicklunchbar'); ?>
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default panel-block panel-title-block">
                <div class="panel-heading">
                    <div>
                        <i class="icon-edit"></i>

                        <h1>
                            <span class="page-title">تسجيل المراجعين</span>
                            <small>                                يتم تسجيل بيانات كافة المعاملات من قبل المراجعين في هذه الصفحة ...
                            </small>
                        </h1>
                        <div class="pull-left add">
                            <?PHP if (check_permission($module, 'a') == 1) { ?>
                                <a href="#" class="add">
                                    <i class="icon-plus-sign"></i>
                                    <span>تسجيل مراجع جديد</span>
                                </a>
                            <?PHP } ?>
                            <button type="button" class="btn btn-success"><i class="icon-envelope-alt"></i> إرسال رسالة
                                نصية
                            </button>
                            <button type="button" class="btn btn-info"><i class="icon-money"></i> حاسبة القروض</button>
                            <button type="button" class="btn btn-info"><i class="icon-reorder"></i> قائمة الرسائل
                                المرسلة
                            </button>
                            <button type="button" class="btn btn-default"><i class="icon-print"></i> طباعة</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="panel panel-default panel-block">

                    <div id="data-table" class="panel-heading datatable-heading">
                        <h4 style="text-align:center" class="text-info"><i class="icon-edit"></i><br/>قائمة المراجعين
                        </h4>
                    </div>

                    <ul class="nav nav-tabs panel panel-default panel-block">
                        <?PHP
                        foreach ($blist as $aarzekul) {
                            ?>
                            <li class="<?PHP echo($aarzekul['class']); ?>"><a data-id="<?PHP echo($aarzekul['b_id']); ?>" data-count="<?PHP echo($aarzekul['b_count']); ?>" href="#" class="inquery_count" data-toggle="tab"> <?PHP echo($aarzekul['b_name']); ?> <span
                                        class="badge"><?PHP echo($aarzekul['b_count']); ?></span> </a></li>
                        <?PHP } ?>

                    </ul>
                    <div class="tab-content panel panel-default panel-block">

                        <div class="tab-pane list-group active" id="tabsdemo-1">
                            <div class="list-group-item">
                                <h4 align="center" class="section-title">عدد المعاملات = <span id="mamla_count"><?PHP echo($blist[0]['b_count']); ?></span> معاملة</h4>

                                <p>

                                <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid">
                                    <div class="row table-header-row">
                                    </div>
                                    <table class="table table-bordered table-striped dataTable" id="tableSortable"
                                           aria-describedby="tableSortable_info">
                                        <thead>
                                        <tr role="row">
                                            <th style="text-align:center;" class="sorting_asc" role="columnheader"
                                                tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1"
                                                aria-sort="ascending"
                                                aria-label="Rendering engine: activate to sort column descending">رقم
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0"
                                                aria-controls="tableSortable" rowspan="1" colspan="1"
                                                aria-label="Browser: activate to sort column ascending"
                                                style="width:15%; text-align:center;">الإسم
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0"
                                                aria-controls="tableSortable" rowspan="1" colspan="1"
                                                aria-label="CSS grade: activate to sort column ascending"
                                                style="width: 96px; text-align:center;">النوع
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0"
                                                aria-controls="tableSortable" rowspan="1"
                                                aria-label="Platform(s): activate to sort column ascending"
                                                style="width: 205px; text-align:center;">البطاقة الشخصية
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0"
                                                aria-controls="tableSortable" rowspan="1" colspan="1"
                                                aria-label="CSS grade: activate to sort column ascending"
                                                style="width: 96px; text-align:center;">الهاتف النقال
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0"
                                                aria-controls="tableSortable" rowspan="1" colspan="1"
                                                aria-label="Platform(s): activate to sort column ascending"
                                                style="width: 205px; text-align:center;">أخر الإستفسارات
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0"
                                                aria-controls="tableSortable" rowspan="1" colspan="2"
                                                aria-label="CSS grade: activate to sort column ascending"
                                                style="width: 96px; text-align:center;">تاريخ التسجيل
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0"
                                                aria-controls="tableSortable" rowspan="1" colspan="5"
                                                aria-label="CSS grade: activate to sort column ascending"
                                                style="width: 96px; text-align:center;">الإجرائات
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                                        <?php
                                        if (!empty($inquiries)):
                                            foreach ($inquiries as $i => $inq):
                                                $all_data = $this->inq->getLastDetail($inq->tempid);
                                                $user_detail = $this->inq->get_user_name($inq->tempid);
                                                $gender = $this->inq->get_gender($inq->tempid);
                                                $phone = $this->inq->get_phone_number($inq->tempid);
                                                $data = $this->inq->get_last_note($inq->tempid);
                                                if ($data->inquerytype) {
                                                    $type_string = rtrim($data->inquerytype, ',');
                                                    $types_array = explode(",", $type_string);
                                                    $array_size = sizeof($types_array);
                                                    if ($array_size > 0) {
                                                        $last_arry_key = end(array_keys($types_array));
                                                        $last_array = $types_array[$last_arry_key];
                                                        $type_id = explode("_", $last_array);
                                                        $type_name = $this->inq->get_type_name($type_id['0']);
                                                    }
                                                }
                                                ?>
                                                <tr class="gradeA even">
                                                    <td class=" sorting_1"><?php echo applicant_number($inq->tempid); ?></td>
                                                    <td class=" "><?php foreach ($all_data['main']->applicant as $names): ?><?php echo $names->first_name . ' ' . $names->middle_name . ' ' . $names->last_name . ' ' . $names->family_name; ?><?php endforeach; ?></td>
                                                    <td class=" "><?php foreach ($all_data['main']->applicant as $gender): ?><?php echo $gender->applicanttype; ?><?php endforeach; ?></td>
                                                    <td class=" "><?php foreach ($all_data['main']->applicant as $idcard): ?><?php echo $idcard->idcard; ?>
                                                            <br><?php endforeach; ?></td>
                                                    <td class=" "><?php foreach ($phone as $ph): ?><?php echo $ph->phonenumber; ?>
                                                            <br><?php endforeach; ?></td>
                                                    <td class=" "><?php echo $type_name; ?></td>
                                                    <td class=" "><?php echo date('h:m:s', strtotime($inq->applicantdate)); ?></td>
                                                    <td class="center "><span
                                                            class="label label-warning"><?php echo date('Y-m-d', strtotime($inq->applicantdate)); ?></span>
                                                    </td>
                                                    <td class="center "><a href="#"><i class="icon-eye-open"></i></a>
                                                    </td>
                                                    <td class=" "><a
                                                            href="<?php echo base_url() ?>inquiries/newinquery/<?php echo $inq->tempid; ?>"><i
                                                                style="color:#C90;" class="icon-edit-sign"></i></a></td>
                                                    <td class="center "><a
                                                            href="<?php echo base_url(); ?>inquiries/delete_auditor/<?php echo $inq->tempid; ?>"><i
                                                                style="color:#F00;" class="icon-remove-circle"></i></a>
                                                    </td>
                                                    <td class="center "><a href="#"><i style="color:#9C0;"
                                                                                       class="icon-comment-alt"></i></a>
                                                    </td>
                                                    <td class="center "><a href="#"><i
                                                                class="icon-comments-alt"></i></a></td>
                                                </tr>
                                                <?php
                                                unset($user_detail, $gender, $array_size, $last_arry_key, $last_array, $type_id, $type_name);
                                            endforeach;
                                        endif;
                                        ?>

                                        </tbody>
                                    </table>

                                    <div class="row table-footer-row">
                                        <div class="col-sm-12">
                                            <div class="pull-left">
                                                <div class="dataTables_info" id="tableSortable_info">Showing 1 to 10 of
                                                    57 entries
                                                </div>
                                            </div>
                                            <div class="pull-right">
                                                <div class="dataTables_paginate paging_bs_normal">
                                                    <ul class="pagination pagination-lg">
                                                        <li class="next disabled"><a href="#">«&nbsp;</a></li>
                                                        <li class="active"><a href="#">1</a></li>
                                                        <li><a href="#">2</a></li>
                                                        <li><a href="#">3</a></li>
                                                        <li><a href="#">4</a></li>
                                                        <li><a href="#">5</a></li>
                                                        <li class="previous"><a href="#">&nbsp;»</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>
                                </p>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        </div>
    </div>
</section>
<script>
    $(function(){
        alert("asdf");
        console.log(config.BASE_URL+"ajax/mamla_listing_filter");
        $('#tableSortable').dataTable( {
            "ajax": config.BASE_URL+"ajax/mamla_listing_filter",
            "columns": [
                { "data": "رقم" },
                { "data": "الإسم" },
                { "data": "النوع" },
                { "data": "البطاقة الشخصية" },
                { "data": "الهاتف النقال" },
                { "data": "أخر الإستفسارات" },
                { "data": "تاريخ التسجيل" },
                { "data": "الإجرائات" }
            ]
        } );
    } );
</script>
<?php $this->load->view('common/footer'); ?>

</body>
</html>

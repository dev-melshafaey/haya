<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <ul class="nav nav-tabs panel panel-default panel-block">
            <?PHP
                        foreach ($blist as $aarzekul) {
                            ?>
            <li id="block_<?PHP echo($aarzekul['b_id']); ?>" class="<?PHP echo($aarzekul['class']); ?> all_block"><a data-id="<?PHP echo($aarzekul['b_id']); ?>" data-count="<?PHP echo($aarzekul['b_count']); ?>" href="#" class="inquery_count" data-toggle="tab"> <?PHP echo($aarzekul['b_name']); ?> <span
                                        class="badge"><?PHP echo($aarzekul['b_count']); ?></span> </a></li>
            <?PHP } ?>
          </ul>
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <h4 align="center" class="section-title">عدد المعاملات = <span id="mamla_count"><?PHP echo($blist[0]['b_count']); ?></span> معاملة</h4>
                <p>
                <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"> </div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable"
                                           aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center; ">رقم </th>
                        <th class="sorting" style="width:15%; text-align:center;">الإسم </th>
                        <th class="sorting" style="width: 96px; text-align:center;">النوع </th>
                        <th class="sorting" style="width: 205px; text-align:center;">البطاقة الشخصية </th>
                        <th class="sorting" style="width: 96px; text-align:center;">الهاتف النقال </th>
                        <th class="sorting" style="width: 205px; text-align:center;">أخر الإستفسارات </th>
                        <th class="sorting" style="width: 96px; text-align:center;">تاريخ التسجيل </th>
                        <th class="sorting" style="width: 130px; text-align:center;">الإجرائات </th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
    $(function(){        
      var branchid = '<?PHP echo $branchid; ?>';
	  $('.all_block').removeClass('active');
	  $('#block_'+branchid).addClass('active');
	  
    } );
</script>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'ajax/mamla_listing_filter/'.$branchid,'columns_array'=>'{ "data": "رقم" },
                { "data": "الإسم" },
                { "data": "النوع" },
                { "data": "البطاقة الشخصية" },
                { "data": "الهاتف النقال" },
                { "data": "أخر الإستفسارات" },
                { "data": "تاريخ التسجيل" },
                { "data": "الإجرائات" }')); ?>
</div>
</body>
</html>
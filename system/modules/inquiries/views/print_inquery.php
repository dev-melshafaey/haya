<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body onLoad="window.print();">
<?php 
$inq_detail = $this->inq->getLastDetail($main_info->tempid);
$main_detail = $inq_detail['main'];
$applicant = $main_detail->applicant;
$user_detail	=	$this->inq->get_user_name($main_info->tempid);
$gender = $this->inq->get_gender($main_info->tempid);
$phone = $this->inq->get_phone_number($main_info->tempid);
$notes = $this->inq->get_notes($main_info->tempid);
$privince =	$this->inq->get_province_name($main_info->province);
$wilayats =	$this->inq->get_wilayats_name($main_info->walaya);
$user_name = $this->inq->get_user_name_of_added($main_info->userid);
$data =	$this->inq->get_last_note($main_info->tempid);
$user_name_last_added = $this->inq->get_user_name_of_added($data->userid);
if($data->inquerytype)
{
	$data->userid;
	$type_string = rtrim($data->inquerytype,',');
	$types_array = explode(",", $type_string);
	$array_size = sizeof($types_array);
	if($array_size > 0)
	{
		$last_arry_key = end(array_keys($types_array));
		$last_array = $types_array[$last_arry_key] ;
		$type_id = explode("_", $last_array);
	    $type_name = $this->inq->get_type_name($type_id['0']);
	}
}
$all_data =	$this->inq->getLastDetail($main_info->tempid);
$counter = '1';
$fllname = $applicant[0]->first_name.' '.$applicant[0]->middle_name.' '.$applicant[0]->last_name.' '.$applicant[0]->family_name;
?>
<div class="modal-body">
<p align="center"><img src="<?PHP echo base_url(); ?>images/proton-logo.png" width="96" height="86"><span id="single_user"> <br />
          إسم المراجع : <span id="mname"><?PHP echo $fllname; ?></span></span><br />
          رقم التسجيل : <span id="mraqam"><?PHP echo $main_detail->tempid_value; ?></span><br />
          طبيعة المراجعين : <span id="mrtype"><?PHP echo $main_detail->user_type; ?></span> </p>
<?php
foreach($all_data['main']->applicant as $list_data)
{
    $phone = $this->inq->get_phone_number($list_data->tempid);
    foreach($phone as $ph)
    {if($ph!='') { $phonenumber .=$ph->phonenumber.'<br>';}}
    ?>
    <table class="table" id="tableSortable" aria-describedby="tableSortable_info">
        <thead style="background-color:#f5f5f5;">
        <?PHP if($all_data['main']->user_type!='فردي') { ?>
        <tr>
            <th colspan="7" class="isam"><?PHP echo($list_data->first_name . ' ' . $list_data->middle_name . ' ' . $list_data->last_name . ' ' . $list_data->family_name); ?></th>
        </tr>
        <?PHP } ?>
        <tr role="row">
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">النوع</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="text-align:center;">رقم البطاقة الشخصية</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="text-align:center;">رقم الهاتف</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" aria-label="Platform(s): activate to sort column ascending" style=" text-align:center;">رقم سجل القوى العاملة</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="text-align:center;">تاريخ الميلاد</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="text-align:center;">الحالة الاجتماعية</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="text-align:center;">الوضع الحالي</th>
        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
            <td><?php echo is_set($list_data->applicanttype);?></td>
            <td><?php echo is_set($list_data->idcard);?></td>
            <td><?php echo $phonenumber;?></td>
            <td><?php echo is_set($list_data->cr_number);?></td>
            <td><?php echo is_set($list_data->datepicker);?></td>
            <td><?php echo is_set(getlistType('maritalstatus',$list_data->marital_status));?></td>
            <td><?php echo is_set(getlistType('job_status',$list_data->job_status));?></td>
        </tr>


        </tbody>

    </table>
    <table class="table" id="tableSortable" aria-describedby="tableSortable_info">
        <thead  style="background-color:#f5f5f5;">
        <tr role="row">
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">العنوان الشخصي</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" aria-label="Browser: activate to sort column ascending" style="text-align:center;">الولاية</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" aria-label="CSS grade: activate to sort column ascending" style="text-align:center;">            هل مسجل في التأمينات الإجتماعية؟</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" aria-label="Platform(s): activate to sort column ascending" style=" text-align:center;">رقم التسجيل</th>


        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
            <td><?PHP echo show_data('Region',$list_data->province); ?></td>
            <td><?PHP echo show_data('Walaya',$list_data->walaya); ?></td>
            <td><?php if(is_set($list_data->is_insurance)=='Y')  { echo 'نعم' ; } else { echo 'لا';} ;?></td>
            <td><?php echo is_set($list_data->insurance_number);?></td>

        </tr>    </tbody>
    </table>
    <table class="table" id="tableSortable" aria-describedby="tableSortable_info">
        <thead  style="background-color:#f5f5f5;">
        <tr role="row">
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">هل لديك مشروع؟</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" aria-label="Browser: activate to sort column ascending" style="text-align:center;">اسم المشروع</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" aria-label="CSS grade: activate to sort column ascending" style="text-align:center;">موقع المشروع</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" aria-label="Platform(s): activate to sort column ascending" style=" text-align:center;">نشاط المشروع</th>
        <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" aria-label="Platform(s): activate to sort column ascending" style=" text-align:center;">الاسم التجاري</th>
        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
            <td><?PHP if(is_set($list_data->confirmation)=='Y') { echo 'نعم' ; } else { echo 'لا';}  ?></td>
            <td><?PHP echo is_set($list_data->project_name); ?></td>
            <td><?php echo is_set($list_data->project_location);?></td>
            <td><?php echo is_set($list_data->project_activities);?></td>
            <td><?php echo is_set($list_data->project_cr_name);?></td>
        </tr>    </tbody>
    </table>
    <table class="table" id="tableSortable" aria-describedby="tableSortable_info">
        <thead  style="background-color:#f5f5f5;">
        <tr role="row">
            <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">هل سبق لك الحصول على قرض للمشروع؟</th>
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">بنك التنمية العماني</th>
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">صندوق شراكة</th>
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">بنك تجاري</th>
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending"> اخرى</th>
         </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
            <td><?PHP if(is_set($list_data->is_loan)=='Y') { echo 'نعم' ; } else { echo 'لا';}  ?></td>
            <td><?PHP if(is_set($list_data->is_bank_loan)=='1') { echo 'نعم' ; } else { echo 'لا';}  ?></td>
            <td><?PHP if(is_set($list_data->is_rafd_loan)=='1') { echo 'نعم' ; } else { echo 'لا';}  ?></td>
            <td><?PHP if(is_set($list_data->is_commercial_loan)=='1') { echo 'نعم' ; } else { echo 'لا';}  ?></td>
            <td><?PHP if(is_set($list_data->is_other_loan)=='1') { echo 'نعم' ; } else { echo 'لا';}  ?></td>

        </tr>    </tbody>
    </table>
<?PHP unset($phonenumber);  }

//Notes Loop
 ?>
    <table class="table" id="tableSortable" aria-describedby="tableSortable_info">
        <thead  style="background-color:#f5f5f5;">
        <tr role="row">
        <th style="text-align:right;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">نوع الاستفسار</th>
        <th style="text-align:right;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending"></th>
        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
        <?PHP foreach($notes as $noah) {   ?>
        <tr class="gradeA odd text-center">

            <td style="text-align:right;"><?PHP noa_ul_istasfar($noah->inquerytype); ?></td>
            <td style="text-align:right;">
                <strong>تفاصيل الإستفسار</strong>
                <br><?PHP echo $main_detail->inquiry_text; ?>
                <br></br><strong>ملاحظات الموظف</strong>
                </br><?PHP echo $noah->notestext; ?>
            </td>
        </tr>
        <?PHP } ?>
        </tbody>
    </table>
</div>
<?php $this->load->view('common/footer'); ?>
</body>
</html>
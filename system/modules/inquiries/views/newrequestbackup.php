<!doctype html>
<?PHP
	$main = $m;
	if($type != "inquiry")
	{
		$applicant = $main['applicants'];
		$applicant_qualification = $main['applicant_qualification'][0];
		$applicant_project = $main['applicant_project'];
		$applicant_professional_experience = $main['applicant_professional_experience'];
		$applicant_phones = $main['applicant_phones'];
		$applicant_partners = $main['applicant_partners'];
		$applicant_numbers = $main['applicant_numbers'];
		$applicant_loans = $main['applicant_loans'];
		$applicant_document = $main['applicant_document'];
		$applicant_businessrecord = $main['applicant_businessrecord'];	
		$step['s'] = 1;
		$step['temp'] = $applicant->applicant_id;
	}
	else
	{
		$applicant = $main['main']->applicant[0];
		$phones = $main['main']->phones;
		$tempId = $main['main']->tempid;
	}
	$steps = array('١. تسجيل ودراسة الطلبات‎','٢. بيانات المشروع','٣. القرض المطلوب','٤. مخاطر الإئتمان');
	$tab_heading = array('مشترك ١','مشترك ٢','مشترك ٣','مشترك ٤');
	$tasgeel_menu = array();
	
?>
<?PHP $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?PHP $this->load->view('common/bodyscript'); ?>
<?PHP $this->load->view('common/menu'); ?>
<?PHP if($main->tempid!='' && !empty($h)) { 
	$this->load->view('common/leftpanel',array('history'=>$h)); 
} ?>
<section class="wrapper scrollable">
<?PHP $this->load->view('common/logo'); ?>
<?PHP $this->load->view('common/usermenu'); ?>
<?PHP $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
<?PHP $this->load->view('common/quicklunchbar'); ?>
<div class="row">
  <div class="col-md-12">
    <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-tabs panel panel-default panel-block">
          <input id="searchcurrent" style="width: 30% !important; margin-top: 4px; margin-left: 6px; margin-bottom: 0px !important;" type="text" class="form-control label-default pull-left searchfieldforpan" style="" value="بحث رقم تسجيل" tabindex="1">
          <?PHP foreach($steps as $th_key => $th_value) {?>
          <li><img style="margen-top:5px;" src="<?php echo base_url();?>images/sep.png" width="20" height="44"></li>
          <li class="tabsdemo-<?PHP echo $th_key; ?> <?PHP if($th_key==0) { ?>active<?PHP } ?>"><a href="#steps-<?PHP echo $th_key; ?>"  data-toggle="tab"><?PHP echo $th_value; ?></a></li>
          <?PHP } ?>
        </ul>
      </div>
    </div>
    <div class="row"> 
      <!----------------->
      <form  id="requestform1" name="requestform1" autocomplete="off" method="post" action="<?PHP echo md5(date('Ymdhisf')); ?>" autocomplete="off">
      <input type="hidden" name="applicant_id" id="applicant_id" value="<?PHP echo $applicant->applicant_id; ?>" />
      <input type="hidden" name="partnerCount" id="partnerCount" value="0" />
      <input type="hidden" name="form_step" id="form_step" value="1" />
      <input type="hidden" name="iscomplete" id="iscomplete" value="0" />
      <div class="col-md-8" style="margin-top: 4px;">
      	<!----Radio Buttons---->
        <div class="panel-default panel-block">
          <div class="nav nav-tabs panel panel-default panel-block">
            <div class="col-md-2">
              <label class="text-warning font_size">فردي </label>
              <label><input onClick="setx_user_type(this)" type="radio" <?PHP  if($applicant->applicant_type=='فردي') { ?> checked="checked" <?PHP } else{ if($applicant->applicanttype=='فردي') { ?> checked="checked" <?PHP } }  ?> id="applicant_type" class="applicant_type" checked="checked" name="applicant_type" value="فردي" /></label>
            </div>
            <div class="col-md-2">
              <label class="text-warning font_size">مشترك </label>
              <label>
                <input onClick="setx_user_type(this)" type="radio" <?PHP  if($applicant->applicant_type=='مشترك') { ?> checked="checked" <?PHP }else{ if($applicant->applicanttype=='مشترك') { ?> checked="checked" <?PHP } } ?> id="applicant_type" class="applicant_type" name="applicant_type" value="مشترك" />
              </label>
            </div>
          </div>
          <div id="nimbus">
            <ul class="nav nav-tabs panel panel-default panel-block">
              <?PHP for($k=0; $k<=3; $k++) {?>
              <li><?PHP if($k!=0) { ?><img style="margen-top:5px;" src="<?php echo base_url();?>images/sep.png" width="20" height="44"><?PHP } ?></li>
              <li class="tabsdemo-<?PHP echo $k; ?> <?PHP if($k==0) { ?>active<?PHP } ?>"><a href="#tabsdemo-<?PHP echo $k; ?>"  data-toggle="tab"><?PHP echo $tab_heading[$k]; ?></a></li>
              <?PHP } ?>
            </ul>
          </div>
          
          <div class="tab-content panel panel-default panel-block">
            <?PHP 
					for($j=0; $j<=3; $j++) { 				
					$appli = $main->applicant[$j];
					$appid = $appli->applicantid;							
			?>
            <div class="tab-pane list-group <?PHP if($j==0) { ?>active<?PHP } ?>" id="tabsdemo-<?PHP echo $j; ?>">
              <div class="row setMargin">
                <div class="col-md-12">
                 <input name="applicant_id" id="applicant_id" value="" type="hidden">
                  <div class="form-group col-md-6">
                    <label class="text-warning">الاسم الأول</label>
                    <input name="applicant_first_name" value="<?PHP  if(isset($type) && $type != "inquiry") { echo $applicant->applicant_first_name; } else{ if(isset($type) && $type == "inquiry") { echo $applicant->first_name; } } ?>" placeholder="الاسم الأول" id="applicant_first_name" type="text" class="form-control req">
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">الاسم الثاني</label>
                    <input name="applicant_middle_name" value="<?PHP if(isset($type) && $type != "inquiry") { echo $applicant->applicant_middle_name; } else{ if(isset($type) && $type == "inquiry") { echo  $applicant->middle_name; } } ?>" placeholder="الاسم الثاني" id="applicant_middle_name" type="text" class="form-control req">
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">الاسم الثالث</label>
                    <input name="applicant_last_name" value="<?PHP  if(isset($type) && $type != "inquiry") { echo $applicant->applicant_last_name; } else{ if(isset($type) && $type == "inquiry") { echo  $applicant->last_name; } } ?>" placeholder="الاسم الثالث" id="applicant_last_name" type="text" class="form-control req">
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">القبيلة / العائلة</label>
                    <input name="applicant_sur_name" value="<?PHP  if(isset($type) && $type != "inquiry") { echo $applicant->applicant_sur_name; } else{ if(isset($type) && $type == "inquiry") { echo  $applicant->family_name; } }  ?>" placeholder="القبيلة / العائلة" id="applicant_sur_name" type="text" class="form-control req">
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">النوع</label>
                    <br>
                    <input type="radio" <?PHP if(isset($type) && $type != "inquiry") { if($applicant->applicant_gender=='ذكر') { ?> checked="checked" <?PHP } } else{ if(isset($type) && $type == "inquiry") { if($applicant->applicanttype=='ذكر') { ?> checked="checked" <?PHP } } }  ?> class="req" name="applicant_gender" value="ذكر" id="applicant_gender"/>
                    ذكر
                    &nbsp;&nbsp;&nbsp;&nbsp;
                   <input type="radio" <?PHP if(isset($type) && $type != "inquiry") { if($applicant->applicant_gender=='أنثى') { ?> checked="checked" <?PHP } } else{ if(isset($type) && $type == "inquiry") { if($applicant->applicanttype=='أنثى') { ?> checked="checked" <?PHP } } } ?> class="req" name="applicant_gender" value="أنثى" id="applicant_gender"/>
                    أنثى </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">رقم البطاقة الشخصية</label>
                    <input name="appliant_id_number"  value="<?PHP  if(isset($type) && $type != "inquiry") { echo $applicant->appliant_id_number; } else{ if(isset($type) && $type == "inquiry") { echo  $applicant->idcard; } }  ?>" id="appliant_id_number" placeholder="رقم البطاقة الشخصية" type="text" class="form-control NumberInput req idcard_autocomplete">
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">رقم بطاقة سجل القوى العاملة</label>
                    <input name="applicant_cr_number" id="applicant_cr_number" value="<?PHP if(isset($type) && $type != "inquiry") { echo $applicant->applicant_cr_number; } else { echo $main['main']->mr_number; }  ?>" placeholder="رقم سجل القوى العاملة" type="text" class="form-control NumberInput">
                  </div>
                  <div class="form-group col-md-4">
                    <label class="text-warning">تاريخ الميلاد</label>
                    <input name="applicant_date_birth" type="text"  value="<?PHP echo date('Y-m-d',strtotime($applicant->applicant_date_birth)); ?>" class="form-control age_datepicker" id="applicant_date_birth" placeholder="تاريخ الميلاد" size="15" maxlength="10">
                    
                  </div>
                  <div class="form-group col-md-2">
                    <label class="text-warning">العمر</label>
                    <input name="age" type="text" class="form-control smallfield" id="age" placeholder="العمر" size="5" maxlength="3" readonly>
                  </div>
                  <div class="form-group col-md-6" style="margin-bottom: 0px !important;">
                    <label class="text-warning">رقم الهاتف</label>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning"></label>
                  </div>
                  <?PHP 
				  		$px = $main->phones[$appli->applicantid];						
						for($p=0; $p<=3; $p++) 
						{	$phones = $px[$p];
					?>
                  <div class="form-group col-md-3">
                    <input name="phone_numbers[]" value="<?PHP echo $applicant_phones[0]->applicant_phone; ?>"  type="text" class="form-control NumberInput req p_number" id="phonenumber" placeholder="رقم الهاتف" maxlength="8">
                  </div>
                  <?PHP }  ?>
                  <div class="form-group col-md-6">
                    <label class="text-warning">الحالة الاجتماعية</label>
                    <?PHP multi_action_dropbox('','marital_status',$appli->applicantid,$main->tempid,$appli->marital_status,'اختر الحالة الاجتماعية','maritalstatus','',$appli->marital_status_text,'كم عدد الأطفال لديك','marital_status_text'); ?>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">الوضع الحالي</label>
                    <?PHP multi_action_dropbox('','job_status',$appli->applicantid,$main->tempid,$appli->job_status,'اختر الوضع الحالي','current_situation','',$appli->job_status_text,'الوضع الحالي','job_status_text'); ?>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">العنوان الشخصي</label>
                    <?PHP inq_reigons('','province',$appli->province,'',$main->tempid,$appli->applicantid); ?>
                  </div>
                 
                 
                </div>
              </div>
            </div>
            <?PHP unset($appli); } ?>
          </div>
          
        </div>
        <!----Radio Buttons---->        
      </div>
      <!------------------------>
      <div class="col-md-4"> part 2 </div>
      </form>
      <!-----------------> 
    </div>
  </div>
</div>
<?php $this->load->view('common/footer');?>
<script src="<?PHP echo base_url(); ?>js/muragain_js.js"></script>
</body>
</html>
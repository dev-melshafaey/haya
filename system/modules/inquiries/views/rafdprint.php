<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
	$applicant = $applicant_data['applicants'];
	$project = $applicant_data['applicant_project'][0];
	$loan = $applicant_data['applicant_loans'][0];
	$evo  = $applicant_data['project_evolution'][0];
	$phones = $applicant_data['applicant_phones'];
	$comitte = $applicant_data['comitte_decision'][0];
	$fullname = $applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name;
	$instalment_points = json_decode($guarantee_data->instalment_points,true);
	$gurantee_val = json_decode($guarantee_data->gurantee_val,true);
	$percentage = $loan->applicant_percentage*0.001;
	$val_p = $evo->total_cost*$percentage;
	foreach($phones as $p)
	{	$ar[] = arabic_date('986'.$p->applicant_phone);	}
		$applicantphone = implode('/',$ar);
?>
<?php $this->load->view('common/header'); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Print | <?PHP echo $fullname; ?></title>
</head>
<style>
body { background-color:#FFF !important; direction:rtl !important;}
th, td {   padding-right: 5px;
  color: #000;
  padding-bottom: 2px;
  padding-top: 2px;
  font-size: 13px;}
table { margin-top:10px; width:100% !important; }  
</style>
<!---->
<body onload="printthepage('printrafad');">
<div id="printrafad" style="margin: 12em 3em !important;">
  <div class="col-md-12">
      
    <table cellspacing="0" cellpadding="0" border="1">
        <tr>
        	<td colspan="4"><h3 style="text-align:center">موافقة أولية على تمويل المشروع</h3></td>
        </tr>
        <tr>
          <td>الاسم</td>
          <td><?php  echo $fullname ; ?></td>
          <td>رقم الهاتف</td>
          <td><?php echo $applicantphone; ?></td>
        </tr>
        <tr>
          <td>نشاط المشروع</td>
          <td><?php echo is_set($project->activity_project_text); ?></td>
          <td>ص . ب</td>
          <td><?php echo is_set($applicant->zipcode); ?></td>
        </tr>
        <tr>
          <td>العنوان</td>
          <td><?php  echo $applicant->way." ".$applicant->home." ".$applicant->village." ".$applicant->WILAYATNAME." ".$applicant->REIGONNAME;?></td>
          <td>ر . ب</td>
          <td><?php echo is_set($applicant->postalcode); ?></td>
        </tr>
        <?php
  if(!empty($parnter_data)){
  	foreach($parnter_data as $parnter){
		$fullnamep = $parnter->partner_first_name.' '.$parnter->parnter_middle_name.' '.$parnter->partner_last_name.' '.$applicant->applicant_sur_name;
		?>
        <tr>
          <td>الاسم</td>
          <td><?php  echo $fullnamep ; ?></td>
          <td>رقم الهاتف</td>
          <td><?php echo $parnter->mobile_number; ?></td>
        </tr>
        <tr>
          <td>نشاط المشروع</td>
          <td><?php echo is_set($project->activity_project_text); ?></td>
          <td>ص . ب</td>
          <td><?php echo is_set($applicant->zipcode); ?></td>
        </tr>
        <tr>
          <td>العنوان</td>
          <td><?php  echo $parnter->way." ".$parnter->home." ".$parnter->village." ".$parnter->walaya;?></td>
          <td>ر . ب</td>
          <td><?php echo is_set($applicant->postalcode); ?></td>
        </tr>
        <?php
	}
  }
  ?>
      </table>
      <table cellspacing="0" cellpadding="0"  border="1">
        <tbody>
        	<tr>
            	<td colspan="5">يسرني إفادتكم بالموافقة المبدئية على طلبكم المتعلق بالحصول على قرض من صندوق الرفد وفقا للآتي:</td>
            </tr>
            <tr>
            	<td colspan="5"><h4><?PHP echo arabic_date(1); ?> - استخدامات القرض :</h4></td>
            </tr>
            <tr>
            	<td><strong>الاستثمار</strong></td>
                <td></td>
                <td><strong>التمويل<strong></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
            <td>البند </td>
            <td>التكلفة </td>
            <td>البند </td>
            <td>المبلغ (ر.ع) </td>
            <td>لنسبة %</td>
          </tr>
            <tr>
            <td>مصاريف ما قبل التشغيل</td>
            <td><?php echo(arabic_date($evo->evolution_pre_expenses)); ?></td>
            <td>مساهمة المقترض</td>
            <td><?php arabic_date($val_p); ?></td>
            <td><?php echo(arabic_date($loan->applicant_percentage));  ?></td>
          </tr>
           <tr>
            <td>الآلات والمعدات</td>
            <td><?php echo(arabic_date($evo->machinery_equipment)); ?></td>
            <td colspan="2">قرض صندوق الرفد</td>
            <td><?php echo arabic_date($evo->total_cost); ?></td>
          </tr>
          <tr>
            <td>اثاث وتركيبات</td>
            <td><?PHP echo(arabic_date($evo->furniture_fixture)); ?></td>
            <td colspan="3">&nbsp;</td>
            
          </tr>
          <tr>
            <td>المركبات</td>
            <td><?PHP echo(arabic_date($evo->vehicles)); ?></td>
            <td colspan="3">&nbsp;</td>
            
          </tr>
          <tr>
            <td>رأس المال العامل</td>
            <td><?PHP echo(arabic_date($evo->working_capital)); ?></td>
            <td colspan="3">&nbsp;</td>
            
          </tr>
          <tr>
            <td>المبلغ المخصص للبائع (بالنسبة لشراء المشاريع)</td>
            <td><?PHP echo(arabic_date($evo->seller_amount));?></td>
            <td colspan="3">&nbsp;</td>
            
          </tr>
          <tr>
            <td>الاجمالي</td>
            <td><?php echo(arabic_date($evo->total_cost)); ?></td>
            <td colspan="2">الاجمالي</td>
            <td><?PHP echo arabic_date($evo->total_cost+$val_p); ?></td>
          </tr>
          <tr>
          	<td colspan="5"><h4><?PHP echo arabic_date(2); ?> - سداد القرض</h4></td>
          </tr>
          <tr>
            <td>مبلغ القرض </td>
            <td colspan="4" ><strong><?PHP echo(arabic_date($evo->total_cost+$val_p)); ?> ر.ع </strong></td>
          </tr>
          <tr>
            <td>نسبة الرسوم الإدارية و الفنية </td>
            <td colspan="4" ><strong>(<?php echo arabic_date($loan->loan_percentage); ?> %) في السنة</strong></td>
          </tr>
          <tr>
            <td>فترة السماح</td>
            <td colspan="4" ><strong><?php echo arabic_date($loan->leave_installmment); ?></strong></td>
          </tr>
          <tr>
            <td>مدة سداد القرض</td>
            <td colspan="4" ><strong>(<?php echo arabic_date($loan->total_no_years); ?>) سنوات</strong></td>
          </tr>
          <tr>
            <td>آلية السداد</td>
            <td colspan="4" ><strong>أ-أصــــــــــل القـــــــــــرض:</strong>
              عدد الأقساط (<?php echo arabic_date($loan->leave_installmment); ?>) قسطا ربع سنوي <br />
              البدء في السداد بعد فترة (<?php echo arabic_date($loan->paid_instalment); ?> )شهرا من تاريخ توقيع الاتفاقية <br />
              <strong>ب-الرسوم الإدارية و المالية:</strong>
              لا تحتسب أية رسوم على فترة السماح المحدودة </td>
          </tr>
          <tr>
            <td>الضــــــمانــــــــات المطـــــــــلوبـــــــــة </td>
            <td colspan="4" >قبل التوقيع على الاتفاقية: <br />
              <?php if(isset($gurantee_val[0])) { ?>
              <li>التأمين على المقترض</li>
              <?php } ?>
              <?php if(isset($gurantee_val[1])) { ?>
              <li>توفير تأمين شامل على المركبات</li>
              <?php
		}
		?>
              <?php if(isset($gurantee_val[2])) { ?>
              <li>رهن ملكية المركبات</li>
              <?php
		}
		?>
              <?php if(isset($gurantee_val[3])) { ?>
              <li>رهن السجل التجاري لدى وزارة التجارة والصناعة</li>
              <?php
		}
		?>
              <?php if(isset($gurantee_val[4])) { ?>
              <li>رهن أصول المشروع (إن وجد)</li>
              <?php
		}
		?>
              <?php if(isset($gurantee_val[5])) { ?>
              <li>توفير الترخيص البلدي و عقد الإيجار للمحل</li>
              <?php
		}
		?>
              <?php if(isset($gurantee_val[6])) { ?>
              <li>الالتزام بالتفرغ الكلي لإدارة المشروع </li>
              <?php
		}
		 ?></td>
          </tr>
          <tr>
            <td>الشــــــــــروط </td>
            <td colspan="4" ><?php if($guarantee_data->conditions !="") echo  $guarantee_data->conditions ; else echo  $setting_data->condition_text; ?></td>
          </tr>
          <tr>
            <td>آليات الصرف </td>
            <?php 
			if(isset($instalment_points[0]) || isset($instalment_points[1])) {
		?>
            <td colspan="4" >•	يصرف مبلغ القرض بعد موافقة صندوق الرفد كالتالي:<br />
              • صرف مبلغ الآلات والمعدات للمورد مباشرة: <br />
              <?php
			}
			else{
			?>
            <td colspan="4" ><?php
			}
			if(isset($instalment_points[0])) {
		?>
              <li>دفعة واحدة </li>
              <?php
			}
			
		?>
              <?php 
			if(isset($instalment_points[1])) {
		?>
              <li>دفعتين</li>
              <?php
			}
		?>
              <?php 
			if(isset($instalment_points[2]) || isset($instalment_points[3])) {
		?>
              •	صرف مبلغ رأس المال في حساب المورد مباشرة:<br />
              <?php
			}
			if(isset($instalment_points[2])) {
		?>
              <li>دفعة واحدة </li>
              <?php
			}
	   ?>
              <?php 
			if(isset($instalment_points[3])) {
		?>
              <li>دفعتين</li>
              <?php
			}
		?>
              <?php
			if(isset($instalment_points[4]) || isset($instalment_points[5])) {
		?>
              •	صرف جزء القرض المخصص لشراء المشروع مباشرة لصاحب المشروع (البائع) <br />
              <?php
			}
		?>
              <?php 
			if(isset($instalment_points[4])) {
		?>
              <li>دفعة واحدة </li>
              <?php
			}
		?>
              <?php 
			if(isset($instalment_points[5])) {
		?>
              <li>دفعتين</li>
              <?php
			}
	   ?></td>
          </tr>
          <tr>
          	<td colspan="5">يرجى منكم التكرم بمراجعة بنك التنمية العماني في محافظتكم لاستكمال باقي الاجراءات الخاصة بالقرض, مع العلم أن في حالة عدم إبرام اتفاقية القرض مع بنك التنمية العماني في مدة أقصاها ستة أشهر من تاريخ هذه الرسالة يعتبر القرض لاغيا.</td>
          </tr>
          <tr>
          	<td colspan="5"> و تفضلوا بقبول فائق الاحترام,,,</td>
          </tr>
          <tr>
          	<td colspan="5"><?PHP foreach($managers['managers'] as $managekey => $manager) { ?><li><?PHP echo $manager; ?></li><?PHP } ?></td>
          </tr>
          <tr>
          	<td colspan="5">مدير فرع صندوق الرفد بمحافظة <?PHP echo $managers['branch']; ?></td>
          </tr>
           <tr>
          	<td colspan="5">وصل التسليم</td>
          </tr>
                    <tr>
          	<td colspan="5">تاريخ التسليم: ( / / )</td>
          </tr>
                    <tr>
          	<td colspan="5">الموضوع :______________________________</td>
          </tr>
                    <tr>
          	<td colspan="5">الاسم والتوقيع:</td>
          </tr>
      </tbody>
      </table>
      
	</div>
</div>
<?php $this->load->view('common/footer'); ?>
</body>
</html>
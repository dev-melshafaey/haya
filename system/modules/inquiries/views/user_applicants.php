<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <p>
                <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"> </div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable"
                                           aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center;" class="sorting_asc" role="columnheader"
                                                tabindex="0" aria-controls="tableSortable"
                                                aria-sort="ascending"
                                                aria-label="">اسم</th>
                        <th class="sorting" role="columnheader" tabindex="0"
                                                aria-controls="tableSortable"
                                                aria-label=""
                                                style="text-align:center;">اسم المستخدم</th>
                        <th class="sorting" role="columnheader" tabindex="0"
                                                aria-controls="tableSortable"
                                                aria-label=""
                                                style="text-align:center;">البريد الإلكتروني</th>
                        <th class="sorting" role="columnheader" tabindex="0"
                                                aria-controls="tableSortable"
                                                aria-label=""
                                                style="text-align:center;">ارقم الهاتف</th>
                        <th class="sorting" role="columnheader" tabindex="0"
                                                aria-controls="tableSortable"
                                                aria-label=""
                                                style="text-align:center;">فرع</th>
                        <th class="sorting" role="columnheader" tabindex="0"
                                                aria-controls="tableSortable"
                                                aria-label=""
                                                style="text-align:center;">الإجراءات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                     
                    </tbody>
                  </table>
                </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
    $(function(){        
      var branchid = '<?PHP echo $branchid; ?>';
	 $('#tableSortable').dataTable( {
            "ajax": config.BASE_URL+"ajax/all_users_list/<?PHP echo $branchid; ?>",
			"oLanguage": {
				  "sSearch": "بحث: ",
				  "oPaginate": {
					  "sNext": "التالی",
						"sPrevious": "السابق"
					  }
				},
            "columns": [
                { "data": "اسم" },
                { "data": "اسم المستخدم" },
                { "data": "البريد الإلكتروني" },
                { "data": "رقم الهاتف" },
                { "data": "فرع" },
                { "data": "الإجراءات"}
            ]
        } );
    } );
</script>
<?php $this->load->view('common/footer'); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
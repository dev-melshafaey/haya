<!doctype html>
<?php $this->load->view('common/print_header'); ?>
<body class="dashboard-page">
<?php 
	$type_name = $this->inq->get_type_name($applicatn_info->applicant_marital_status);
	$privince =	$this->inq->get_province_name($applicatn_info->province);
	$wilayats =	$this->inq->get_wilayats_name($applicatn_info->walaya);
	$phone =	$this->inq->applicant_phone_number($applicatn_info->applicant_id);
	$tab_1 =	$this->inq->get_tab_data('applicant_qualification',$applicatn_info->applicant_id);
	$tab_2 =	$this->inq->get_tab_data('applicant_professional_experience',$applicatn_info->applicant_id);
	$tab_3 =	$this->inq->get_tab_data('applicant_businessrecord',$applicatn_info->applicant_id);
	$professional	=	$this->inq->getRequestInfo($applicatn_info->applicant_id);
	$full_name_for_single = $applicatn_info->applicant_first_name.' '.$applicatn_info->applicant_middle_name.' '.$applicatn_info->applicant_last_name.' '.$applicatn_info->applicant_sur_name;
	//Getting Info
	$project = $this->inq->get_applicant_project($applicatn_info->applicant_id);
	$laon = $this->inq->get_applicant_loan($applicatn_info->applicant_id);
?>
<div id="printable">
<div class="col-md-12">
  <div class="row" style="direction:rtl !important;">
    <table class="table">
    	<tr>
        	<th style="width:32px; text-align:center;"><img style="  margin-top: 7em; " src="<?PHP echo base_url(); ?>ajax/barcode/<?PHP echo applicant_number($applicatn_info->applicant_id); ?>" /></th>
            <th style="width:36px; text-align:center;"><p style="text-align: center;"><img src="<?PHP echo base_url(); ?>images/proton-logo.png" width="96" height="86"><br>
      <?PHP 
	  		$fullString = ' رقم التسجيل : '.arabic_date(applicant_number($applicatn_info->applicant_id));
			$fullString .= ', اسم : '.$full_name_for_single;
			$fullString .= ', رقم البطاقة الشخصية : '.arabic_date($applicatn_info->appliant_id_number);
	   ?>
      اسم مقدم الطلب : <span id="mname"><?PHP echo $full_name_for_single; ?></span><br />
      <?PHP //} ?>
      رقم التسجيل : <span id="mraqam"><?php echo applicant_number($applicatn_info->applicant_id);?></span><br />
      طبيعة المراجعين : <span id="mrtype">
      <?php  echo $applicatn_info->applicant_type;?>
      </span><br>
      رقم البطاقة الشخصية : <span id="mraqam"><?php echo is_set($applicatn_info->appliant_id_number);?></span></p></th>
            <th style="width:32px; text-align:center;"><?PHP qrcode_generator($fullString); ?></th>
        </tr>
    </table>
  
    <table class="table">
    
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">النوع</th>
          <th class="sorting bingomyth" style="text-align:center;">تاريخ الميلاد</th>
          <th class="sorting bingomyth" style=" text-align:center;">رقم الهاتف</th>
          <th class="sorting bingomyth" style="text-align:center;">الحالة الاجتماعية</th>
          <th class="sorting bingomyth" style="text-align:center;">الوضع الحالي</th>
          <th class="sorting bingomyth" style="text-align:center;">فئة الظمان الإجتماعي</th>
          <th class="sorting bingomyth" style="text-align:center;">فئة من ذوي الإعاقة</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($applicatn_info->applicant_gender);?></td>
          <td class="bingomytd"><?php echo date('Y-m-d',strtotime($applicatn_info->applicant_date_birth)); ?></td>
          <td class="bingomytd"><?php foreach($phone as $ph):?>
            <?php echo '<li class="data_list_grid">'.$ph->applicant_phone.'</li>';?>
            <?php endforeach;?></td>
          <td class="bingomytd"><?php echo is_set($this->inq->get_type_name($applicatn_info->applicant_marital_status));?></td>
          <td class="bingomytd"><?php echo is_set($this->inq->get_type_name($applicatn_info->applicant_job_staus)); ?></td>
          <td class="bingomytd"><?php if($applicatn_info->option1	==	'Y'):?>
            <?php echo $applicatn_info->option_txt; ?>
            <?php else:?>
            <?php echo 'لا'; ?>
            <?php endif;?></td>
          <td class="bingomytd"><?php if($applicatn_info->option2	==	'Y'):?>
            <?php echo 'نعم'; ?>
            <?php else:?>
            <?php echo 'لا'; ?>
            <?php endif;?></td>
        </tr>
      </tbody>
    </table>
    <br>
    <h4 class="section-title preface-title text-warning h4change">العنوان الشخصي</h4>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">محافظة</th>
          <th class="sorting bingomyth" style="text-align:center;">الولاية</th>
          <th class="sorting bingomyth" style=" text-align:center;">القرية</th>
          <th class="sorting bingomyth" style="text-align:center;">السكة</th>
          <th class="sorting bingomyth" style="text-align:center;">المنزل/المبني</th>
          <th class="sorting bingomyth" style="text-align:center;">الشقة</th>
          <th class="sorting bingomyth" style="text-align:center;">ص.ب / ر.ب</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($privince);?></td>
          <td class="bingomytd"><?php echo is_set($wilayats);?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->village); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->way); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->home); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->deparment); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->zipcode); ?> / <?php echo is_set($applicatn_info->postalcode); ?></td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">رقم سجل القوى العاملة</th>
          <th class="sorting bingomyth" style="text-align:center;">الهاتف الثابت</th>
          <th class="sorting bingomyth" style=" text-align:center;">الفاكس</th>
          <th class="sorting bingomyth" style="text-align:center;">البريد الإلكتروني</th>
          <th class="sorting bingomyth" style="text-align:center;">هاتف نقال أحد الأقارب</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($applicatn_info->applicant_cr_number); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->linephone); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->fax);?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->email);?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->refrence_number);?></td>
        </tr>
      </tbody>
    </table>
    <br>
    <h4 class="section-title preface-title text-warning h4change">المؤهلات</h4>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:right; color:#d09c0d;" class="sorting_asc bingomyth" colspan="4">1 / المستوى الدراسي</th>
        </tr>
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">المؤهل</th>
          <th class="sorting bingomyth" style="text-align:center;">التخصص</th>
          <th class="sorting bingomyth" style=" text-align:center;">الجهة</th>
          <th class="sorting bingomyth" style="text-align:center;">سنة التخرج</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($this->inq->get_type_name($applicatn_info->applicant_qualification)); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->applicant_specialization); ?></td>
          <td class="bingomytd"><?php echo is_set($this->inq->get_type_name($applicatn_info->applicant_institute)); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->applicant_institute_year); ?></td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:right; color:#d09c0d;" class="sorting_asc bingomyth" colspan="5">2 / التدريب المهني</th>
        </tr>
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">مركز التدريب</th>
          <th class="sorting bingomyth" style="text-align:center;">التخصص</th>
          <th class="sorting bingomyth" style=" text-align:center;">مدة التدريب (بالأشهر)</th>
          <th class="sorting bingomyth" style="text-align:center;">شهادة التدريب المهني المتحصل عليها</th>
          <th class="sorting bingomyth" style="text-align:center;">سنة الحصول على الشهادة</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($applicatn_info->applicant_institute); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->applicant_specializations); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->applicant_training_month); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->applicant_vtco); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->applicant_ytotc); ?></td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">دورات تدريبية ميدانية أخرى</th>
          <th class="sorting bingomyth" style="text-align:center;">دورات التدريب المتخصصة قبل إقامة المشروع</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($applicatn_info->applicant_other_trainning); ?></td>
          <td class="bingomytd"><?php echo is_set($applicatn_info->applicant_other_specializations); ?></td>
        </tr>
      </tbody>
    </table>
    <?PHP if(!empty($professional['applicant_partners'])) { ?>
    <h4 class="section-title preface-title text-warning h4change">مشترك</h4>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">اسم مقدم الطلب</th>
          <th class="sorting bingomyth" style="text-align:center;">النوع</th>
          <th class="sorting bingomyth" style=" text-align:center;">رقم البطاقة الشخصية</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <?PHP foreach($professional['applicant_partners'] as $partners) { ?>
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo $partners->partner_first_name.' '.$partners->partner_middle_name.' '.$partners->partner_last_name.' '.$partners->partner_sur_name;?></td>
          <td class="bingomytd"><?php echo is_set($partners->partner_gender);?></td>
          <td class="bingomytd"><?php echo is_set($partners->partner_id_number);?></td>
        </tr>
        <?PHP } ?>
      </tbody>
    </table>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">رقم الهاتف</th>
          <th class="sorting bingomyth" style="text-align:center;">االمرحلة</th>
          <th class="sorting bingomyth" style=" text-align:center;">رقم بطاقة سجل القوى العاملة</th>
          <th class="sorting bingomyth" style="text-align:center;">ارقم الهاتف</th>
          <th class="sorting bingomyth" style="text-align:center;">تاريخ الميلاد</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?PHP foreach($phone as $ph) { echo('<li class="data_list_grid">'.$ph->applicant_phone.'</li>'); } ?></td>
          <td class="bingomytd"><?php $step = get_lable('step-'.$applicatn_info->form_step); echo $step['ar'];?></td>
          <td class="bingomytd"><?php echo is_set($partners->applicant_cr_number); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->mobile_number); foreach($phone as $ph) { if(is_set($ph->phonenumber)!='----') {	echo('<li  class="data_list_grid">'.is_set($ph->phonenumber).'</li>'); } }?></td>
          <td class="bingomytd"><?php echo date('Y-m-d',strtotime(is_set($partners->partner_date_birth))); ?></td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">الحالة الاجتماعية</th>
          <th class="sorting bingomyth" style="text-align:center;">الوضع الحالي</th>
          <th class="sorting bingomyth" style=" text-align:center;">فئة الظمان الإجتماعي</th>
          <th class="sorting bingomyth" style="text-align:center;">فئة من ذوي الإعاقة</th>
          
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($this->inq->get_type_name($partners->partner_marital_status)); ?></td>
          <td class="bingomytd"><?php echo is_set($this->inq->get_type_name($partners->partner_job_staus)); ?></td>
          <td class="bingomytd"><?php if($partners->option1	== 'Y') { echo $partners->option_txt; } else { echo 'لا'; } ?></td>
          <td class="bingomytd"><?php if($partners->option2	== 'Y') { echo 'نعم'; } else { echo 'لا'; } ?></td>
          
        </tr>
      </tbody>
    </table>   
    <!---------------------------------------------------------------------->
    <!---------------------------------------------------------------------->
    <!---------------------------------------------------------------------->
    <h4 class="section-title preface-title text-warning h4change">العنوان الشخصي</h4>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">محافظة</th>
          <th class="sorting bingomyth" style="text-align:center;">الولاية</th>
          <th class="sorting bingomyth" style=" text-align:center;">القرية</th>
          <th class="sorting bingomyth" style="text-align:center;">السكة</th>
          <th class="sorting bingomyth" style="text-align:center;">المنزل/المبني</th>
          <th class="sorting bingomyth" style="text-align:center;">الشقة</th>
          <th class="sorting bingomyth" style="text-align:center;">ص.ب / ر.ب</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($privince);?></td>
          <td class="bingomytd"><?php echo is_set($wilayats);?></td>
          <td class="bingomytd"><?php echo is_set($partners->village); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->way); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->home); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->deparment); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->zipcode); ?> / <?php echo is_set($partners->postalcode); ?></td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">رقم سجل القوى العاملة</th>
          <th class="sorting bingomyth" style="text-align:center;">الهاتف الثابت</th>
          <th class="sorting bingomyth" style=" text-align:center;">الفاكس</th>
          <th class="sorting bingomyth" style="text-align:center;">البريد الإلكتروني</th>
          <th class="sorting bingomyth" style="text-align:center;">هاتف نقال أحد الأقارب</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($partners->applicant_cr_number); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->linephone); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->fax);?></td>
          <td class="bingomytd"><?php echo is_set($partners->email);?></td>
          <td class="bingomytd"><?php echo is_set($partners->refrence_number);?></td>
        </tr>
      </tbody>
    </table>
    <br>
    <h4 class="section-title preface-title text-warning h4change">المؤهلات</h4>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:right; color:#d09c0d;" class="sorting_asc bingomyth" colspan="4">1 / المستوى الدراسي</th>
        </tr>
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">المؤهل</th>
          <th class="sorting bingomyth" style="text-align:center;">التخصص</th>
          <th class="sorting bingomyth" style=" text-align:center;">الجهة</th>
          <th class="sorting bingomyth" style="text-align:center;">سنة التخرج</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($this->inq->get_type_name($partners->applicant_qualification)); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->applicant_specialization); ?></td>
          <td class="bingomytd"><?php echo is_set($this->inq->get_type_name($partners->applicant_institute)); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->applicant_institute_year); ?></td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:right; color:#d09c0d;" class="sorting_asc bingomyth" colspan="5">2 / التدريب المهني</th>
        </tr>
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">مركز التدريب</th>
          <th class="sorting bingomyth" style="text-align:center;">التخصص</th>
          <th class="sorting bingomyth" style=" text-align:center;">مدة التدريب (بالأشهر)</th>
          <th class="sorting bingomyth" style="text-align:center;">شهادة التدريب المهني المتحصل عليها</th>
          <th class="sorting bingomyth" style="text-align:center;">سنة الحصول على الشهادة</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($partners->applicant_institute); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->applicant_specializations); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->applicant_training_month); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->applicant_vtco); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->applicant_ytotc); ?></td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">دورات تدريبية ميدانية أخرى</th>
          <th class="sorting bingomyth" style="text-align:center;">دورات التدريب المتخصصة قبل إقامة المشروع</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($partners->applicant_other_trainning); ?></td>
          <td class="bingomytd"><?php echo is_set($partners->applicant_other_specializations); ?></td>
        </tr>
      </tbody>
    </table>
    <?PHP }	 ?>
     <h4 class="section-title preface-title text-warning h4change">الخبرة المهنية</h4>
     <?PHP if(!empty($professional['applicant_professional_experience'])) { $counter = 1;?>
     <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:right; color:#d09c0d;" class="sorting_asc bingomyth" colspan="5">الخبرة في نفس نشاط المشروع</th>
        </tr>
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">تاريخ بداية المشروع</th>
          <th class="sorting bingomyth" style="text-align:center;">اسم الجهة/المؤسسة/ المشروع الخاص</th>
          <th class="sorting bingomyth" style=" text-align:center;">نشاط الجهة/المؤسسة/ المشروع الخاص</th>
          <th class="sorting bingomyth" style="text-align:center;">المهنة المزاولة بالجهة/المؤسسة/ المشروع الخاص</th>
          <th class="sorting bingomyth" style="text-align:center;">عدد سنوات الخبرة</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
      <?PHP foreach($professional['applicant_professional_experience'] as $tab) { 
	  		if($counter <= '3') {?>
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($tab->option_one);?></td>
          <td class="bingomytd"><?php echo is_set($tab->option_two);?></td>
          <td class="bingomytd"><?php echo is_set($tab->option_three);?></td>
          <td class="bingomytd"><?php echo is_set($tab->option_four);?></td>
          <td class="bingomytd"><?php echo is_set($tab->option_five);?></td>
        </tr>
       <?PHP	}
	    $counter++; } ?> 
      </tbody>
    </table>
    <?PHP } ?>
    <?PHP if(!empty($professional['applicant_professional_experience'])) { $counter = 1;?>
     <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:right; color:#d09c0d;" class="sorting_asc bingomyth" colspan="5">الخبرة في أنشطة أخرى</th>
        </tr>
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth col-md-2">تاريخ بداية المشروع</th>
          <th class="sorting bingomyth col-md-2" style="text-align:center;">اسم الجهة/المؤسسة/ المشروع الخاص</th>
          <th class="sorting bingomyth col-md-2" style=" text-align:center;">نشاط الجهة/المؤسسة/ المشروع الخاص</th>
          <th class="sorting bingomyth col-md-2" style="text-align:center;">المهنة المزاولة بالجهة/المؤسسة/ المشروع الخاص</th>
          <th class="sorting bingomyth col-md-2" style="text-align:center;">عدد سنوات الخبرة</th>
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
      <?PHP foreach($professional['applicant_professional_experience'] as $tab) 
	  { 
	  	if($counter <= '3') 
		{
		?>
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($tab->activities_one);?></td>
          <td class="bingomytd"><?php echo is_set($tab->activities_two);?></td>
          <td class="bingomytd"><?php echo is_set($tab->activities_three);?></td>
          <td class="bingomytd"><?php echo is_set($tab->activities_four);?></td>
          <td class="bingomytd"><?php echo is_set($tab->activities_five);?></td>
        </tr>
        <?PHP 	
	  }
	   	$counter++;
		 
		} 
		?> 
      </tbody>
    </table>
    <?PHP } ?>
    
    <?PHP if(!empty($professional['applicant_businessrecord'])) { $counter = 1;?>
    <h4 class="section-title preface-title text-warning h4change">السجلات التجارية الأخرى</h4>
     <table class="table">
      <thead style="background-color:#f5f5f5;">
        <tr role="row">
          <th style="text-align:right; color:#d09c0d;" class="sorting_asc bingomyth" colspan="4">الخبرة في نفس نشاط المشروع</th>
        </tr>
        <tr role="row">
          <th style="text-align:center;" class="sorting_asc bingomyth">اسم السجل</th>
          <th class="sorting bingomyth" style="text-align:center;">رقم السجل</th>
          <th class="sorting bingomyth" style=" text-align:center;">عدد القوى العاملة الوطنية</th>
          <th class="sorting bingomyth" style="text-align:center;">عدد القوى العاملة الوافدة</th>
          
        </tr>
      </thead>
      <tbody role="alert" aria-live="polite" aria-relevant="all">
      <?PHP foreach($professional['applicant_businessrecord'] as $tab) { 
	  			if($counter <= '3') {?>
        <tr class="gradeA odd text-center">
          <td class="bingomytd"><?php echo is_set($tab->activity_name);?></td>
          <td class="bingomytd"><?php echo is_set($tab->activity_registration_no);?></td>
          <td class="bingomytd"><?php echo is_set($tab->activity_nationalmanpower);?></td>
          <td class="bingomytd"><?php echo is_set($tab->activity_laborforce);?></td>
          
        </tr>
       <?PHP 	}
	   		$counter++; } ?> 
      </tbody>
    </table>
    <?PHP } ?>
    <h4 class="section-title preface-title text-warning h4change" style="margin-top:5px;"> بيانات المشروع</h4>
    <?PHP $pro = $project[0]; ?>    
     <table class="table">
     
      <tbody role="alert" aria-live="polite" aria-relevant="all"> 
      <tr role="row">
          <td style="text-align:center;" class="sorting_asc bingomyth">نوع المشروع<br><strong><?PHP echo is_set($pro->list_name); ?></strong></td>
          <td class="sorting bingomyth" style="text-align:center;">القطاع الإقتصادي<br><strong><?PHP echo is_set($pro->alist); ?></strong></td>
          <td class="sorting bingomyth" style=" text-align:center;">نشاط المشروع<br><strong><?PHP echo is_set($pro->activity_project_text); ?></strong></td>          
        </tr>    
        <tr class="gradeA odd text-center">
          <td class="bingomytd">نبذة عن المشروع<br><strong><?PHP echo is_set($pro->about_project); ?></strong></td>
          <td class="bingomytd">رقم السجل التجاري<br><strong><?PHP echo is_set($pro->project_registration_number); ?></strong></td>
          <td class="bingomytd">الاسم التجاري للمشروع<br><strong><?PHP echo is_set($pro->commercial_name); ?></strong></td>          
        </tr>
        <tr class="gradeA odd text-center">
          <td class="bingomytd" colspan="3"><h4 class="section-title preface-title text-warning h4change">عنوان المشروع (المقترح)</h4></td>
                   
        </tr>
      <tr class="gradeA odd text-center">
          <td class="bingomytd">المحافظة<br><strong><?PHP echo is_set($pro->REIGONNAME); ?></strong></td>
          <td class="bingomytd">الولاية<br><strong><?PHP echo is_set($pro->WILAYATNAME); ?></strong></td>
          <td class="bingomytd">القرية<br><strong><?PHP echo is_set($pro->project_village); ?></strong></td>          
        </tr>
        <tr class="gradeA odd text-center">
          <td class="bingomytd">الهاتف<br><strong><?PHP echo is_set($pro->project_linephone); ?></strong></td>
          <td class="bingomytd">الفاكس<br><strong><?PHP echo is_set($pro->project_faxnumber); ?></strong></td>
          <td class="bingomytd">البريد الالكتروني<br><strong><?PHP echo is_set($pro->project_email); ?></strong></td>          
        </tr>
        <?PHP if($pro->list_name!='جديد') { ?>
        <tr class="gradeA odd text-center">
          <td class="bingomytd" colspan="3"><h4 class="section-title preface-title text-warning h4change">بالنسبة للمشاريع القائمة</h4></td>                   
        </tr>
        <tr class="gradeA odd text-center">
          <td class="bingomytd">تاريخ التأسيس الأول للمشروع<br><strong><?PHP echo is_set($pro->foundation_date); ?></strong></td>
          <td class="bingomytd">القيمة التقديرية للتأسيسات والتجهيزات الموجودة حاليا بالمشروع<br><strong><?PHP echo is_set($pro->estimated_value); ?></strong></td>
          <td class="bingomytd">طبيعة محل المشروع<br><strong><?PHP echo is_set($pro->nature_projectx); ?></strong></td>          
        </tr>
        <tr class="gradeA odd text-center">
          <td class="bingomytd">طبيعة موقع المشروع<br><strong><?PHP echo is_set($pro->nature_project_sitex); ?></strong></td>
          <td class="bingomytd">التعمين<br><strong><?PHP echo is_set($pro->project_employmentx); ?></strong></td>
          <td class="bingomytd">في حالة تواجد صعوبات أذكر أهمها<br><strong><?PHP echo is_set($pro->project_difficulties); ?></strong></td>          
        </tr>
        <tr class="gradeA odd text-center">
          <td class="bingomytd" colspan="3"><h4 class="section-title preface-title text-warning h4change">عدد القوى العاملة الوطنية بالمشروع</h4></td>                   
        </tr>
        <tr class="gradeA odd text-center">
          <td class="bingomytd" colspan="3">عدد القوى العاملة الوطنية بالمشروع : ذكر(<?PHP echo is_set($pro->national_male_employes); ?>)&nbsp;&nbsp;&nbsp;أنثى(<?PHP echo is_set($pro->national_female_employes); ?>)&nbsp;&nbsp;&nbsp;مجموع(<?PHP echo is_set($pro->national_female_employes+$pro->national_male_employes); ?>)</td>          
        </tr>
        <tr class="gradeA odd text-center">
          <td class="bingomytd" colspan="3">عدد القوى العاملة الوافدة إن وجد : ذكر(<?PHP echo is_set($pro->foreign_male_employes); ?>)&nbsp;&nbsp;&nbsp;أنثى(<?PHP echo is_set($pro->foreign_female_employes); ?>)&nbsp;&nbsp;&nbsp;مجموع(<?PHP echo is_set($pro->foreign_female_employes+$pro->foreign_male_employes); ?>)</td>          
        </tr>
        <?PHP } ?>
      </tbody>
    </table>
  
  	<?PHP if($laon) { 
			$loan = $laon[0];
			
	?>
    <h4 class="section-title preface-title text-warning h4change" style="margin-top:5px;">القرض المطلوب</h4>
     
     <table class="table">
     
      <tbody role="alert" aria-live="polite" aria-relevant="all"> 
      <tr role="row">
          <td style="text-align:center;" class="sorting_asc bingomyth">نوع المنتج<br><strong><?PHP echo is_set($loan->one); ?></strong></td>
          <td class="sorting bingomyth" style="text-align:center;">البرنامج المطلوب‎<br><strong><?PHP echo is_set($loan->two); ?></strong></td>
          <td class="sorting bingomyth" style=" text-align:center;">نسبة الرسوم<br><strong><?PHP echo is_set($loan->loan_percentage); ?>%</strong></td>
          <td class="sorting bingomyth" style=" text-align:center;">مبلغ القرض<br><strong><?PHP echo is_set($loan->loan_amount); ?></strong></td>          
        </tr>    
         
      </tbody>
    </table>
    <?PHP } ?>
    <!--------------->
    <br>
    
  </div>
</div>  
</div>
<div class="row" style="text-align:center !important;">
    <form name="print_inquery" method="post" action="<?PHP echo base_url(); ?>inquiries/get_applicant_data/<?PHP echo $applicatn_info->applicant_id; ?>" target="printing_iframe">
        <input type="hidden" name="applicant_id" id="applicant_id" value="<?PHP echo $applicatn_info->applicant_id; ?>">
        <button type="button" id="" onClick="printthepage('printable');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
      </form>
    </div>
</body>
</html>
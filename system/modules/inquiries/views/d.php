<?php
$applicant = $applicant_data['applicants'];
$applicant_document[1] = $applicant->document_name_1;
	$applicant_document[2] = $applicant->document_name_2;
	$applicant_document[3] = $applicant->document_name_3;
	$applicant_document[4] = $applicant->document_name_4;
	$applicant_document[5] = $applicant->document_name_5;
	$applicant_document[6] = $applicant->document_name_6;
	$applicant_document[7] = $applicant->document_name_7;
	$applicant_document[8] = $applicant->document_name_8;
				
		$uploaderArray = array(
	'icon-upload-alt pull-left'=>'بطاقة سجل القوى العاملة',
	'icon-user pull-left'=>'البطاقة الشخصية',
	'icon-certificate pull-left'=>'شهادة عدم محكومية',
	'icon-briefcase pull-left'=>'دراسة الجدوى الإقتصادية للمشروع',
	'icon-camera-retro pull-left'=>'صورة شمسية',
	'icon-wrench pull-left'=>'شهادات الخبرة / التدريب',
	'icon-umbrella pull-left'=>'بطاقة الضمان الاجتماعي'
	);
	
?>
<div class="col-lg-12">
    <div class="panel-group" id="demo-accordion" style="border-right: 1px solid #ddd;">
      <?PHP 
	  $applicant_doc_index = 1;
	  
	  foreach($uploaderArray as $tabkey => $tabvalue) 
	  { 
	  		$data_url = base_url().'upload_files/documents/'.$applicant_document[$applicant_doc_index];
			if($applicant_document[$applicant_doc_index]!='')
			{	$is_uploaded = 1;	}
			else
			{	$is_uploaded = 0;	}
						
	  ?>
      <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
        <div class="panel-heading" id="head<?PHP echo $applicant_doc_index;?>">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $applicant_doc_index; ?>"><?PHP echo $tabvalue; ?> <i class="<?PHP echo $tabkey; ?>"></i><?PHP if($applicant_document[$applicant_doc_index]!='') { echo('<i style="color:#00A603; float:left;" class="icon-ok-circle"></i>'); } ?></a> </h4>
        </div>
        <div id="demo-collapse<?PHP echo $applicant_doc_index; ?>" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
          <div class="panel-body multiple_uploader" data-index="<?PHP echo $applicant_doc_index; ?>" data-heading="<?PHP echo $tabvalue; ?>" id="drag<?php echo $applicant_doc_index; ?>">
            <div class="browser">
            </div>
            <div class="data<?PHP echo $applicant_doc_index; ?>">
            	<?PHP if($applicant_document[$applicant_doc_index]!='') 
				{	echo getFileResult($applicant_document[$applicant_doc_index],$tabvalue);	} ?>
            </div>
          </div>
        </div>
      </div>
      <?PHP 
	  	$applicant_doc_index++;
	  } 
	  
	  $data_url = base_url().'upload_files/documents/'.$applicant_document[8];
			if(!empty($guarantee_attach))
			{	$is_uploaded = 1;	}
			else
			{	$is_uploaded = 0;	}
			
	  ?>
		      
      <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
        <div class="panel-heading" id="head8">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse8">موافقة أولية <i class="icon-umbrella pull-left"></i><?PHP if(!empty($guarantee_attach)) { echo('<i style="color:#00A603; float:left;" class="icon-ok-circle"></i>'); } ?></a> </h4>
        </div>
        <div id="demo-collapse8" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
          <div class="panel-body multiple_uploader2" data-index="8" data-heading="موافقة أولية" id="drag8">
            <div class="browser">
              <input type="hidden" data-id="8" id="document_uploaded_8" placeholder="موافقة أولية" class="dreq" value="<?PHP echo $is_uploaded; ?>">
            </div>
            <div class="data8">
            	<?PHP if(!empty($guarantee_attach)) {
						//echo "<pre>";
						//print_r($guarantee_attach);
						foreach($guarantee_attach as $i=>$g_attch){ 
						?>
                        <input type="hidden" name="document_name_8[<?php echo $i; ?>]" id="document_name_8" value="<?php echo $g_attch->gurarntee_image; ?>" class="doc8<?php echo $i; ?>">
                        <a  title="موافقة صورة " href="<?php echo base_url() ?>/upload_files/documents/<?php echo $g_attch->gurarntee_image; ?>" rel="gallery1" class="fancybox-button doclink<?php echo $i; ?>"><img style="width:60%; height:60%" src="<?php echo base_url(); ?>upload_files/documents/<?php echo $g_attch->gurarntee_image; ?>"></a>
                       <i class="icon-remove-sign doc8remove<?php echo $i; ?>" style="color:#CC0000;cursor:pointer"  onclick="deleteFile('<?php echo $i; ?>')"></i>
                        <?php
						}
					} ?>
            </div>
          </div>
        </div>
      </div>
      
      
      <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
        <div class="panel-heading" id="head9">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse9">إعادة جدولة<i class="icon-umbrella pull-left"></i><?PHP if(!empty($guarantee_attach)) { echo('<i style="color:#00A603; float:left;" class="icon-ok-circle"></i>'); } ?></a> </h4>
        </div>
        <div id="demo-collapse9" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
          <div class="panel-body multiple_uploader2" data-index="98" data-heading="إعادة جدولة" id="drag9">
            <div class="browser">
              <input type="hidden" data-id="8" id="document_uploaded_8" placeholder="إعادة جدولة" class="dreq" value="<?PHP echo $is_uploaded; ?>">
            </div>
            <div class="data8">
            	<?PHP if(!empty($schedular_attach)) {
						//echo "<pre>";
						//print_r($schedular_attach);
						//exit;
						foreach($schedular_attach as $i=>$g_attch){ 
						?>
                        <input type="hidden" name="document_name_8[<?php echo $i; ?>]" id="document_name_8" value="<?php echo $g_attch->rescehdule_document; ?>" class="doc8<?php echo $i; ?>">
                        <a  title="إعادة جدولة" href="<?php echo base_url() ?>/upload_files/documents/<?php echo $g_attch->rescehdule_document; ?>" rel="gallery1" class="fancybox-button doclink<?php echo $i; ?>"><img style="width:60%; height:60%" src="<?php echo base_url(); ?>upload_files/documents/<?php echo $g_attch->rescehdule_document; ?>"></a>
                       <i class="icon-remove-sign doc8remove<?php echo $i; ?>" style="color:#CC0000;cursor:pointer"  onclick="deleteFile('<?php echo $i; ?>')"></i>
                        <?php
						}
					} ?>
            </div>
          </div>
        </div>
      </div>
     
    </div>
  </div>
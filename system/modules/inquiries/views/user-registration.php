<?php $login_user	=	$this->session->userdata('userid');?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <?php //if(isset($single_user->id) AND $single_user->id	==	$login_user):?> <!--readonly--> <?php //endif;?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <form action="<?php echo current_url();?>" method="POST" id="validate_form_new" name="validate_form_new" autocomplete="off">
          <div class="col-md-12">
            <div class="panel panel-default panel-block">
              <div class="list-group">
                <div class="list-group-item" id="input-fields">                  
                  <div class="form-group col-md-6">
                    <label for="basic-input">الاسم الأول</label>
                    <input type="text" class="form-control req" name="firstname" id="firstname" placeholder="الاسم الأو" value="<?php echo (isset($single_user->firstname) ? $single_user->firstname : NULL);?>" />
                  </div>
                  <div class="form-group col-md-6">
                    <label for="basic-input">اسم العائلة</label>
                    <input type="text" class="form-control req" name="lastname" id="lastname" placeholder="اسم العائلة" value="<?php echo (isset($single_user->lastname) ? $single_user->lastname : NULL);?>" />
                  </div>
                  <div class="form-group col-md-6">
                    <label for="basic-input">اسم المستخدم</label>
                    <?PHP if($single_user->id=='') { ?>
                  <input type="text" class="form-control req" name="user_name" id="user_name" placeholder="اسم المستخدم" value="<?php echo (isset($single_user->user_name) ? $single_user->user_name : NULL);?>"/>
                <?PHP } else { ?>
					<input type="hidden" class="form-control req" name="user_name" id="user_name" placeholder="اسم المستخدم" value="<?php echo (isset($single_user->user_name) ? $single_user->user_name : NULL);?>"/>
					<strong><?php echo (isset($single_user->user_name) ? $single_user->user_name : NULL);?></strong>
				<?PHP } ?>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="basic-input">كلمة السر</label>
                    <input type="password" class="form-control <?PHP if($single_user->id=='') { ?>req<?PHP } ?>" name="password" id="password" placeholder="******" value=""/>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="basic-input">تأكيد كلمة المرور</label>
                    <input type="password" class="form-control  <?PHP if($single_user->id=='') { ?>req<?PHP } ?>" name="confirm_password" id="confirm_password" placeholder="******" value=""/>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="basic-input">البريد الإلكتروني</label>
                    <input type="text" class="form-control req " name="email" id="email" placeholder="البريد الإلكتروني" value="<?php echo (isset($single_user->email) ? $single_user->email : NULL);?>" />
                  </div>
                  <div class="form-group col-md-6">
                    <label for="basic-input">رقم الهاتف</label>
                    <input type="text" class="form-control  NumberInput req" maxlength="8" name="number" id="number" placeholder="رقم الهاتف" value="<?php echo (isset($single_user->number) ? $single_user->number : NULL);?>"/>
                  </div>
                  <div class="form-group  col-md-6">
                    <label for="basic-input">عن مستخدم</label>
                    <input type="text" class="form-control " name="about_user" id="about_user" placeholder="عن مستخدم" value="<?php echo (isset($single_user->about_user) ? $single_user->about_user : NULL);?>"/>
                  </div>
                  <div class="form-group col-md-6" <?php if(isset($single_user->id) AND $single_user->id	==	$login_user):?> style="display:block;" <?php endif;?>>
                    <label for="basic-input">فرع</label>
                    <?php get_branches('branch_id',$single_user->branch_id);?>
                  </div>
                  <div class="form-group col-md-6" <?php if(isset($single_user->id) AND $single_user->id	==	$login_user):?> style="display:block;" <?php endif;?>>
                    <label for="basic-input">‫القسم</label>
                    <?php parent_dropbox('user_parent_role',$single_user->user_parent_role);?>
                  </div>
                  <div class="form-group col-md-6" id="get_parent_roles" <?php if(isset($single_user->id) AND $single_user->id	==	$login_user):?> style="display:block;" <?php endif;?>>
                    <label for="basic-input">الوظيفة</label>
                    <?php child_dropbox_role('user_role_id',$single_user->user_role_id,'req',$single_user->user_parent_role);?>
                  </div>
                  <div class="form-group col-md-6">
                  	<label for="basic-input">صلاحية الزيارة الميدانية</label>
                    <input type="checkbox" <?PHP if($single_user->zyarapermission==1) { ?> checked <?PHP } ?> name="zyarapermission" id="zyarapermission" value="1">
                  </div>
                  <div class="form-group">
                    <?php if($single_user->id):?>
                	<input type="hidden" name="id" value="<?php echo $single_user->id;?>" />
                    <input type="hidden" name="old_password" value="<?php echo $single_user->password;?>" />
                    <?php endif;?>
                    <input type="button" id="save_data_form_new" class="btn btn-success btn-lrg" name="save_data_form_new"  value="حفظ" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<script>
    $(function(){        
      var branchid = '<?PHP echo $branchid; ?>';
	
    } );
</script>
<?php $this->load->view('common/footer'); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
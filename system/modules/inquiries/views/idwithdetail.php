<?PHP
	$ah_applicant = $_applicant_data['ah_applicant'];
	$ah_applicant_wife = $_applicant_data['ah_applicant_wife'];
	$ah_applicant_survayresult = $_applicant_data['ah_applicant_survayresult'];
	$ah_applicant_relation = $_applicant_data['ah_applicant_relation'];
	$ah_applicant_family = $_applicant_data['ah_applicant_family'];
	$ah_applicant_economic_situation = $_applicant_data['ah_applicant_economic_situation'];
	$ah_applicant_documents = $_applicant_data['ah_applicant_documents'];	
?>

<div class="row">
  <div class="col-md-12 fox leftborder">
    <h4 class="panel-title customhr">بيانات طلب مساعدة</h4>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الاستمارة: </label>
      <strong><?PHP echo arabic_date($ah_applicant->applicantcode); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">التاريخ: </label>
      <strong><?PHP echo show_date($ah_applicant->registrationdate,5); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">نوع الطلب: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->charity_type_id); ?> </strong> </div>
    <div class="col-md-12 form-group">
      <label class="text-warning">الاسم الرباعي والقبيلة: </label>
      <strong><?PHP echo $ah_applicant->fullname; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم جواز سفر: </label>
      <strong><?PHP echo arabic_date($ah_applicant->passport_number); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم البطاقة الشخصة: </label>
      <strong><?PHP echo arabic_date($ah_applicant->idcard_number); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">المحافظة \ المنطقة: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->province); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">ولاية: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->wilaya); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">البلدة \ المحلة: </label>
      <strong><?PHP echo $ah_applicant->address; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">هاتف المنزل: </label>
      <strong><?PHP echo arabic_date($ah_applicant->hometelephone); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الهاتف :</label>
      <strong><?PHP echo arabic_date(implode('<br>',json_decode($ah_applicant->extratelephone,TRUE))); ?></strong></div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الحالة الجتماعية:</label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->marital_status); ?></strong> </div>
    <?PHP foreach($ah_applicant_wife as $wife) { ?>
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم الزوج\الزوجة: </label>
      <strong><?PHP echo $wife->relationname; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الجواز\البطاقة: </label>
      <strong><?PHP echo arabic_date($wife->relationdoc); ?></strong> </div>
    <?PHP } ?>
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم الام: </label>
      <strong><?PHP echo $ah_applicant->mother_name; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الجواز\البطاقة: </label>
      <strong><?PHP echo arabic_date($ah_applicant->mother_id_card); ?></strong> </div>
    <!-----------------Documents--------------------------> 
    <!---------------------------------------------------->
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr">اترفق الوثائق والمستندات الازمة لطلب</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
          <?PHP 
					$doccount = 0;					
					foreach($this->inq->allRequiredDocument($ah_applicant->charity_type_id) as $ctid) { $doccount++; 
						$doc = $ah_applicant_documents[$ctid->documentid];						
						$url = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$doc->document;
					?>
          <div class="panel panel-default" style="border-bottom: 1px solid #ddd;">
            <div class="panel-heading" style="padding:7px 3px !important;" id="head<?PHP echo $ctid->documentid;?>">
              <h4 class="panel-title" style="font-size:12px; font-weight:normal !important;">
                <?PHP if($doc->appli_doc_id!='') { ?>
                <span class="icons" id="removeicons<?PHP echo $doc->appli_doc_id; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> </span>
                <?PHP } ?>
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $ctid->documentid;?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
            </div>
          </div>
          <?PHP } ?>
        </div>
      </div>
    </div>
    <h4 class="panel-title customhr">افراد الأسرة: (بما فهيم العاملين و غيرالعاملين) </h4>
    <div class="col-md-12 form-group">
      <table class="table table-bordered table-striped dataTable">
        <thead>
          <tr role="row">
            <th>الاسم</th>
            <th style="width: 50px;">السن</th>
            <th>القرابة</th>
            <th>المهنة</th>
            <th style="width: 70px;">الدخل الشهري</th>
          </tr>
        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
          <?PHP 
			  
			  if($ah_applicant_relation)
			  {	
			  	foreach($ah_applicant_relation as $apr) { ?>
          <tr>
            <td><?PHP echo $apr->relation_fullname; ?></td>
            <td><?PHP echo arabic_date($apr->age); ?></td>
            <td><?PHP echo $this->haya_model->get_name_from_list($apr->relationtype); ?></td>
            <td><?PHP echo $this->haya_model->get_name_from_list($apr->professionid); ?></td>
            <td><?PHP echo arabic_date($apr->monthly_income); ?></td>
          </tr>
          <?PHP }
			  }
			 ?>
        </tbody>
      </table>
    </div>
    <h4 class="panel-title customhr">االحالة الاقتصادية: </h4>
    <div class="col-md-6 form-group">
      <label class="text-warning">الدخل: </label>
      <strong><?PHP echo arabic_date($ah_applicant->salary); ?></strong> </div>
    <div class="col-md-6 form-group">
      <?PHP if($ah_applicant->source=='يوجد') { echo('يوجد: يوضح كالاتي'); } elseif($ah_applicant->source=='لايوجد') { echo('لايوجد: ويوضح مصدر المعيشة'); } ?>
    </div>
    <?PHP if($ah_applicant->source=='يوجد') { ?>
    <div class="col-md-6 form-group">
      <label class="text-warning">مصدره: </label>
      <strong><?PHP echo $ah_applicant->musadra; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">قيمتة: </label>
      <strong><?PHP echo $ah_applicant->qyama; ?></strong> </div>
    <?PHP } ?>
    <?PHP if($ah_applicant->source=='لايوجد') { ?>
    <div class="col-md-12 form-group">
      <label class="text-warning">مصدر المعيشة: </label>
      <strong><?PHP echo $ah_applicant->lamusadra; ?></strong> </div>
    <?PHP } ?>
    <div class="col-md-12 form-group">
      <label class="text-warning">وضف الحالة السكنية: </label>
      <?PHP echo $ah_applicant->current_situation; ?> </div>
    <div class="col-md-7 form-group">
      <label class="text-warning"><strong><?PHP echo $ah_applicant->ownershiptype; ?></strong> </label>
    </div>
    <div class="col-md-5 form-group"> <strong><?PHP echo arabic_date($ah_applicant->ownershiptype_amount); ?></strong> </div>
    <div class="col-md-12">
      <h4 class="panel-title customhr">مكونات المنزل:</h4>
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">نوع البناء:</label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->building_type); ?></strong> </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">عدد الغرف:</label>
      <strong><?PHP echo arabic_date($ah_applicant->number_of_rooms); ?></strong> </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">المرافق:</label>
      <strong><?PHP echo arabic_date($ah_applicant->utilities); ?></strong> </div>
    <div class="col-md-12 form-group">
      <label class="text-warning">حالة الأثاث والموجودات:</label>
      <strong><?PHP echo $ah_applicant->furniture; ?><strong> </div>
    <div class="col-md-12">
      <h4 class="panel-title customhr">تفاصيل البنك:</h4>
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم البنك:</label>
      <?PHP echo $this->haya_model->get_name_from_list($ah_applicant->bankid); ?> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الفرع:</label>
      <?PHP echo $this->haya_model->get_name_from_list($ah_applicant->branchid); ?> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الحساب:</label>
      <?PHP echo $ah_applicant->accountnumber; ?> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">نوع الحساب: جاري \ توفير: </label>
      <?PHP echo($ah_applicant->accounttype); ?> </div>
    <div class="col-md-12 form-group">
      <label class="text-warning">يرفق صورة من كشف الحسابات (حديث الاصدار):</label>
      <?PHP
          	$statmentURL = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->bankstatement;
			echo getFileResult($statmentURL,'يرفق صورة من كشف الحسابات (حديث الاصدار)',$statmentURL);
		  ?>
    </div>
  </div>
</div>

<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12" >
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12"  style="background-color:#FFF; padding: 20px;">
        <form action="<?php echo current_url();?>" method="POST" id="people_form" name="people_form" autocomplete="off">
          <div class="form-group col-md-5">
            <label for="basic-input">الاسم</label>
            <input type="text" class="form-control req" name="firstname" id="firstname" placeholder="الاسم الأو" value="<?php echo($user->firstname);?>"/>
          </div>
          <div class="form-group col-md-3" style="display:none;">
            <label for="basic-input">اسم العائلة</label>
            <input type="text" class="form-control req" name="lastname" id="lastname" placeholder="اسم العائلة" value="XXX"/>
          </div>
          <div class="form-group col-md-5">
            <label for="basic-input">البريد الإلكتروني</label>
            <input type="text" class="form-control req" name="email" id="email" placeholder="البريد الإلكتروني" value="<?php echo($user->email);?>"/>
          </div>
          <div class="form-group col-md-5">
            <label for="basic-input">رقم الهاتف</label>
            <input type="text" class="form-control req NumberInput" maxlength="8" name="number" id="number" placeholder="رقم الهاتف" value="<?php echo($user->number);?>"/>
          </div>
          <div class="form-group col-md-5">
            <label for="basic-input">فرع</label>
            <?php get_branches('branch_id',$user->branch_id);?>
          </div>
          <div class="form-group col-md-5">
            <label for="basic-input">زياده</label>
            <input type="checkbox" name="iseligible" value="1" <?PHP if($user->iseligible=='1') { ?> checked <?PHP } ?> id="iseligible">
          </div>
          <div class="form-group col-md-11">
            <input type="hidden" name="id" value="<?PHP echo $user->id; ?>" />
            <input type="hidden" name="user_parent_role" value="19" />
            <input type="button" id="save_system_people" class="btn btn-success btn-lrg" name="save_system_people"  value="حفظ" />
          </div>
        </form>
        <br clear="all">
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script src="<?PHP echo base_url(); ?>js/system.js"></script>
</body>
</html>
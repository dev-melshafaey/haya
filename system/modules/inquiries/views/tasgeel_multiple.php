<?PHP
	$mustarik = '_'.$evo;
	$mqm = $this->inq->getPartnerInfo($aid,$evo);
	$p = $mqm['partners'];
	$p->parnter_id;	
	$ape = $mqm['ape'];
	$apb = $mqm['apb'];	
	
	$index = 1;
	$ind  = $mustarik.''.$index;
	$hiddenInd  =$mustarik.''.$index;
	$hiddenIndex = 1;
	$uploaderArray = array(
	'icon-upload-alt pull-left'=>'بطاقة سجل القوى العاملة',
	'icon-user pull-left'=>'البطاقة الشخصية',
	'icon-certificate pull-left'=>'شهادة عدم محكومية',
	'icon-briefcase pull-left'=>'دراسة الجدوى الإقتصادية للمشروع',
	'icon-camera-retro pull-left'=>'صورة شمسية',
	'icon-wrench pull-left'=>'شهادات الخبرة / التدريب',
	'icon-umbrella pull-left'=>'بطاقة الضمان الاجتماعي');
?>

<div class="row">
  <input type="hidden" name="partner_id<?PHP echo $mustarik; ?>" id="partner_id<?PHP echo $mustarik; ?>"  value="<?php $p->parnter_id; ?>"/>
  <div class="col-md-8" style="  margin-top:10px !important;">
    <div class="col-md-4 form-group">
      <label class="text-warning">الاسم الأول :</label>
      <input name="partner_first_name<?PHP echo $mustarik; ?>" value="<?PHP  if(isset($type) && $type != "inquiry") { echo $p->partner_first_name; } else{ if(isset($type) && $type == "inquiry") { echo $p->first_name; } } ?>" placeholder="الاسم الأول" id="partner_first_name<?PHP echo $mustarik; ?>" type="text" class="form-control">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">الاسم الثاني :</label>
      <input name="partner_middle_name<?PHP echo $mustarik; ?>" value="<?PHP if(isset($type) && $type != "inquiry") { echo $p->partner_middle_name; } else{ if(isset($type) && $type == "inquiry") { echo  $p->middle_name; } } ?>" placeholder="الاسم الثاني" id="partner_middle_name<?PHP echo $mustarik; ?>" type="text" class="form-control">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">الاسم الثالث :</label>
      <input name="partner_last_name<?PHP echo $mustarik; ?>" value="<?PHP  if(isset($type) && $type != "inquiry") { echo $p->partner_last_name; } else{ if(isset($type) && $type == "inquiry") { echo  $p->last_name; } } ?>" placeholder="الاسم الثالث" id="partner_last_name<?PHP echo $mustarik; ?>" type="text" class="form-control">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">القبيلة / العائلة :</label>
      <input name="partner_sur_name<?PHP echo $mustarik; ?>" value="<?PHP  if(isset($type) && $type != "inquiry") { echo $p->partner_sur_name; } else{ if(isset($type) && $type == "inquiry") { echo  $p->family_name; } }  ?>" placeholder="القبيلة / العائلة" id="partner_sur_name<?PHP echo $mustarik; ?>" type="text" class="form-control">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">النوع :</label>
      <input type="radio" <?PHP if(isset($type) && $type != "inquiry") { if($p->partner_gender=='ذكر') { ?> checked="checked" <?PHP } } else{ if(isset($type) && $type == "inquiry") { if($p->applicanttype=='ذكر') { ?> checked="checked" <?PHP } } }  ?>  name="partner_gender<?PHP echo $mustarik; ?>" value="ذكر" id="partner_gender<?PHP echo $mustarik; ?>"/>
      ذكر
      <input type="radio" <?PHP if(isset($type) && $type != "inquiry") { if($p->partner_gender=='أنثى') { ?> checked="checked" <?PHP } } else{ if(isset($type) && $type == "inquiry") { if($p->applicanttype=='أنثى') { ?> checked="checked" <?PHP } } } ?>  name="partner_gender<?PHP echo $mustarik; ?>" value="أنثى" id="partner_gender<?PHP echo $mustarik; ?>"/>
      أنثى </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">رقم البطاقة الشخصية :</label>
      <input name="partner_id_number<?PHP echo $mustarik; ?>"  value="<?PHP  if(isset($type) && $type != "inquiry") { echo $p->partner_id_number; } else{ if(isset($type) && $type == "inquiry") { echo  $p->partner_id_number; } }  ?>" id="partner_id_number<?PHP echo $mustarik; ?>" placeholder="رقم البطاقة الشخصية" type="text" class="form-control NumberInput appliant_id_number">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">رقم سجل القوى العاملة :</label>
      <input name="partner_cr_number<?PHP echo $mustarik; ?>" id="partner_cr_number<?PHP echo $mustarik; ?>" value="<?PHP if(isset($type) && $type != "inquiry") { echo $p->partner_cr_number; } else { echo $main['main']->mr_number; }  ?>" placeholder="رقم سجل القوى العاملة" type="text" class="form-control NumberInput">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">تاريخ الميلاد :</label>
      <input data-durar="age<?PHP echo $mustarik; ?>" name="partner_date_birth<?PHP echo $mustarik; ?>" data-bingo="<?PHP echo $mustarik; ?>" type="text"  value="<?PHP echo date('Y-m-d',strtotime($p->partner_date_birth)); ?>" class="form-control age_datepicker" id="partner_date_birth<?PHP echo $mustarik; ?>" placeholder="تاريخ الميلاد" size="15" maxlength="10">
    </div>
    <div class="col-md-1 form-group">
      <label class="text-warning">&nbsp;</label>
      <input name="age<?PHP echo $mustarik; ?>" type="text" value="<?PHP if($p->partner_date_birth) { echo calcualteAge($p->partner_date_birth); } ?>" class="form-control NumberInput" id="age<?PHP echo $mustarik; ?>" placeholder="العمر" readonly>
    </div>
    <div class="col-md-11 form-group" style="  width: 100% !important;">
      <label class="text-warning">رقم الهاتف :</label>
      <br>
      <?PHP for($p=0; $p<=3; $p++) { 
				echo(' <div class="form-group col-md-3" style="float:left !important; padding-right: 2px !important;   padding-left: 2px !important;">');
				if(isset($type) && $type != "inquiry") { ?>
      <input name="phone_numbers<?PHP echo $mustarik; ?>[]" on value="<?PHP echo $partner_phones[0]->partner_phone; ?>"  type="text" class="form-control applicantphone checkphonenumber phoneautocomplete NumberInput" id="phonenumber<?PHP echo $mustarik; ?>" placeholder="رقم الهاتف" maxlength="8">
      <?PHP	} else { ?>
      <input name="phone_numbers<?PHP echo $mustarik; ?>[]" value="<?PHP echo $phones[$p->applicantid][0]->phonenumber; ?>"  type="text" class="form-control applicantphone checkphonenumber phoneautocomplete NumberInput" id="phonenumber<?PHP echo $mustarik; ?>" placeholder="رقم الهاتف" maxlength="8">
      <?PHP	} echo('</div>');
				} ?>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الحالة الاجتماعية :</label>
      <?PHP hd_dropbox('partner_marital_status'.$mustarik,$p->partner_marital_status,'اختر الحالة الاجتماعية','maritalstatus','form-control',$p->partner_marital_status_text,'كم عدد الأطفال لديك','partner_marital_status_text'); ?>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الوضع الحالي :</label>
      <?PHP hd_dropbox('partner_job_staus'.$mustarik,$p->partner_job_staus,'اختر الوضع الحالي','current_situation','form-control',$p->partner_job_status_text,'','partner_job_status_text'); ?>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">فئة الضمان الإجتماعي :</label>
      <br>
      <input data-open="option_txt_id<?PHP echo $mustarik; ?>" id="option1<?PHP echo $mustarik; ?>" onClick="openOptions(this);" type="radio" <?PHP if($p->option1=='Y') { ?>checked="checked"<?PHP } ?> name="option1<?PHP echo $mustarik; ?>" value="Y" />
      نعم
      <input data-open="option_txt_id<?PHP echo $mustarik; ?>" id="option1<?PHP echo $mustarik; ?>" onClick="openOptions(this);" <?PHP if($p->option1=='N') { ?>checked="checked"<?PHP } ?> type="radio" name="option1<?PHP echo $mustarik; ?>" value="N" />
      لا
      <div class="row" id="option_txt_id<?PHP echo $mustarik; ?>" style=" <?PHP if($p->option1!='Y') { ?>display:none;<?PHP } ?>">
        <div class="col-md-11">
          <input type="text" class="form-control " value="<?PHP echo $p->option_txt; ?>" name="option_txt<?PHP echo $mustarik; ?>" id="option_txt<?PHP echo $mustarik; ?>" placeholder="رقم بطاقة الضمان الاجتماعي">
        </div>
      </div>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">فئة الاحتياجات الخاصة:</label>
      <br>
      <input  data-open="disable<?PHP echo $mustarik; ?>" id="option2<?PHP echo $mustarik; ?>" <?PHP if($p->option2=='Y') { ?>checked="checked"<?PHP } ?> type="radio" name="option2<?PHP echo $mustarik; ?>" value="Y"  onClick="openOptions(this);" />
      نعم
      <input  data-open="disable<?PHP echo $mustarik; ?>" id="option2<?PHP echo $mustarik; ?>" <?PHP if($p->option2=='N') { ?>checked="checked"<?PHP } ?> type="radio" name="option2<?PHP echo $mustarik; ?>" value="N"  onClick="openOptions(this);"/>
      لا
      <div class="row" style=" <?PHP if($p->option2!='Y') { ?>display:none;<?PHP } ?>" id="disable<?PHP echo $mustarik; ?>">
        <div class="col-md-11">
          <?PHP hd_dropbox('disable_type'.$mustarik,$p->disable_type,'اختر نوع الإعاقة','disable_type','form-control',$p->partner_disable_type_text,'','partner_disable_type_text'.$mustarik);  ?>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="panel-group" id="demo-accordion" style="border-right: 1px solid #ddd;">
      <?PHP foreach($uploaderArray as $tabkey => $tabvalue) { 
	  		$objid = $evo.$index;  ?>
      <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
        <div class="panel-heading" id="head<?PHP echo $objid;?>">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $objid; ?>"><?PHP echo $tabvalue; ?> <i class="<?PHP echo $tabkey; ?>"></i></a> </h4>
        </div>
        <div id="demo-collapse<?PHP echo $objid; ?>" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
          <div class="panel-body multiple_uploader" data-index="<?PHP echo $objid; ?>" data-heading="<?PHP echo $tabvalue; ?>" id="drag<?php echo $objid; ?>">
            <div class="browser">
              <input type="hidden" name="document_name_<?PHP echo $objid; ?>" id="document_name_<?PHP echo $objid; ?>" value="<?PHP if($applicant_document[$applicant_doc_index]!='') { echo($applicant_document[$applicant_doc_index]); } ?>">
              <input type="hidden" data-id="<?PHP echo $objid; ?>" id="document_uploaded_<?PHP echo $objid;?>" placeholder="<?PHP echo $tabvalue; ?>" value="0">
              <input type="file" style="font-size: 11px;" name="document_id<?php echo $objid; ?>" id="document_id<?php echo $objid; ?>" title='<?PHP echo $tabvalue; ?>'>
            </div>
            <div class="data<?PHP echo $objid; ?>">
                
            	<?PHP if($applicant_document[$objid]!='') 
				{	echo getFileResult($applicant_document[$objid],$tabvalue);
					echo('<i class="delete-icon icon-remove-sign doc8remove0" style="color:#CC0000;cursor:pointer" onclick="deleteDoc(this,\''.$applicant_document[$objid].'\');"></i>'); 	} 
				
				?>
            </div>
          </div>
        </div>
      </div>
      <?PHP 
	  	$index++;
	    $ind++;
	  } ?>
      <?PHP 
for($a=1;$a<=8;$a++)
{	echo('<input type="hidden" name="document_name'.$hiddenInd.$evo.'" id="document_name'.$hiddenInd.$evo.'">');
	$hiddenInd++;	}
?>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12" style="margin-top:10px !important;">
    <h4 class="section-title preface-title text-warning" style="padding-right: 15px !important;">العنوان الشخصي</h4>
    <div class="col-md-3 form-group">
      <label class="text-warning">المحافظة:</label>
      <?PHP inq_reigons('','province'.$mustarik,$p->province,'',$evo,$appli->applicantid); ?>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الولاية:</label>
      <?PHP inq_wilayats('walaya'.$mustarik,$p->walaya,$p->province,'',$main->tempid,$appli->applicantid); ?>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">القرية:</label>
      <input type="text" value="<?PHP echo $p->village; ?>" class="form-control" name="village<?PHP echo $mustarik; ?>" placeholder="القرية">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">السكة:</label>
      <input type="text" value="<?PHP echo $p->way; ?>" class="form-control" name="way<?PHP echo $mustarik; ?>" placeholder="السكة">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">المنزل/المبني:</label>
      <input type="text" value="<?PHP echo $p->home; ?>" class="form-control" name="home<?PHP echo $mustarik; ?>" placeholder="المنزل/المبني">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الشقة:</label>
      <input type="text" value="<?PHP echo $p->deparment; ?>" class="form-control" name="deparment<?PHP echo $mustarik; ?>" placeholder="الشقة">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">ص.ب:</label>
      <input type="text" value="<?PHP echo $p->zipcode; ?>" class="form-control NumberInput" name="zipcode<?PHP echo $mustarik; ?>" placeholder="">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">ر.ب:</label>
      <input type="text" value="<?PHP echo $p->postalcode; ?>" class="form-control NumberInput" name="postalcode<?PHP echo $mustarik; ?>" placeholder="ر.ب">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الهاتف النقال:</label>
      <input type="text" value="<?PHP echo $p->mobile_number; ?>" class="form-control NumberInput" name="mobile_number<?PHP echo $mustarik; ?>" placeholder="الهاتف النقال">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الهاتف الثابت:</label>
      <input type="text" value="<?PHP echo $p->linephone; ?>" class="form-control NumberInput" name="linephone<?PHP echo $mustarik; ?>" placeholder="الهاتف الثابت">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الفاكس:</label>
      <input type="text" value="<?PHP echo $p->fax; ?>" class="form-control NumberInput" name="fax<?PHP echo $mustarik; ?>" placeholder="الفاكس">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">البريد الإلكتروني:</label>
      <input type="text" value="<?PHP echo $p->email; ?>" class="form-control" name="email<?PHP echo $mustarik; ?>" placeholder="البريد الإلكتروني">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">هاتف نقال أحد الأقارب:</label>
      <input type="text" value="<?PHP echo $p->refrence_number; ?>" class="form-control NumberInput" name="refrence_number<?PHP echo $mustarik; ?>" placeholder="هاتف نقال أحد الأقارب">
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12" style="margin-top:10px !important;">
    <div class="panel-group" id="demo-accordion">
      <div class="panel panel-default" style="border-bottom: 1px solid #ddd;">
        <div class="panel-heading">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseOne<?PHP echo $mustarik; ?>"><i class="icon-chevron-sign-left "></i> المؤهلات </a> </h4>
        </div>
        <div id="demo-collapseOne<?PHP echo $mustarik; ?>" class="panel-collapse collapse" style="height: auto;">
          <div class="panel-body">
            <h4 class="myheading" style="padding-right: 15px !important; margin-top: 15px;">١ / المستوى الدراسي</h4>
            <div class="col-md-3 form-group">
              <label class="text-warning">المؤهل:</label>
              <?PHP hd_dropbox('partner_qualification'.$mustarik,$p->partner_qualification,'اختر المؤهل','qualification','form-control',$p->partner_qualification_text,'','partner_qualification_text'.$mustarik); ?>
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">التخصص:</label>
              <input name="partner_specialization<?PHP echo $mustarik; ?>" type="text"  value="<?PHP echo $p->partner_specialization; ?>" class="form-control " id="partner_specialization<?PHP echo $mustarik; ?>" placeholder="التخصص">
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">الجهة:</label>
              <?PHP hd_dropbox('partner_institute'.$mustarik,$p->partner_institute,'اختر الجهة','institute','form-control',$p->partner_institute_text,'الجهة','partner_institute_text'.$mustarik); ?>
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">المؤهل:</label>
              <input name="partner_institute_year<?PHP echo $mustarik; ?>" id="partner_institute_year<?PHP echo $mustarik; ?>" value="<?PHP echo $p->partner_institute_year; ?>" placeholder="سنة التخرج" type="text" class="form-control NumberInput">
            </div>
            <h4 class="myheading" style="padding-right: 15px !important;">٢ / التدريب المهني</h4>
            <div class="col-md-3 form-group">
              <label class="text-warning">مركز التدريب:</label>
              <input name="partner_trainningcenter<?PHP echo $mustarik; ?>" type="text"  value="<?PHP echo $p->partner_trainningcenter; ?>" class="form-control " id="partner_trainningcenter<?PHP echo $mustarik; ?>" placeholder="مركز التدريب">
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">التخصص:</label>
              <input name="partner_specializations<?PHP echo $mustarik; ?>" type="text"  value="<?PHP echo $p->partner_specializations; ?>" class="form-control " id="partner_specializations<?PHP echo $mustarik; ?>" placeholder="التخصص">
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">مدة التدريب (بالأشهر):</label>
              <input name="partner_training_month<?PHP echo $mustarik; ?>" type="text"  value="<?PHP echo $p->partner_training_month; ?>" class="form-control NumberInput" id="partner_training_month<?PHP echo $mustarik; ?>" placeholder="مدة التدريب (بالأشهر)">
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">شهادة التدريب المهني المتحصل عليها:</label>
              <input name="partner_vtco<?PHP echo $mustarik; ?>" type="text"  value="<?PHP echo $p->partner_vtco; ?>" class="form-control " id="partner_vtco<?PHP echo $mustarik; ?>" placeholder="شهادة التدريب المهني المتحصل عليها">
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">سنة الحصول على الشهادة:</label>
              <input name="partner_ytotc<?PHP echo $mustarik; ?>" type="text"  value="<?PHP echo $p->partner_ytotc; ?>" class="form-control " id="partner_ytotc<?PHP echo $mustarik; ?>" placeholder="سنة الحصول على الشهادة">
            </div>
            <div class="col-md-9 form-group">
              <label class="text-warning">دورات تدريبية ميدانية أخرى (اختصاص التدريب, جهة التدريب, مدة التدريب (بالأشهر):</label>
              <textarea style="resize:none !important;" name="partner_other_trainning<?PHP echo $mustarik; ?>" id="partner_other_trainning<?PHP echo $mustarik; ?>" class="form-control" placeholder="دورات تدريبية ميدانية أخرى (اختصاص التدريب, جهة التدريب, مدة التدريب (بالأشهر) )"><?PHP echo $p->partner_other_trainning; ?></textarea>
            </div>
            <div class="col-md-12 form-group" style="position:static !important;">
              <label class="text-warning">دورات التدريب المتخصصة قبل إقامة المشروع: (تنمية المبادرة-إدارة المؤسسات-مجالات فنية أخرى):</label>
              <textarea style="resize:none !important;" name="partner_other_specializations<?PHP echo $mustarik; ?>" id="partner_other_specializations<?PHP echo $mustarik; ?>" class="form-control" placeholder="دورات التدريب المتخصصة قبل إقامة المشروع: (تنمية المبادرة-إدارة المؤسسات-مجالات فنية أخرى)"><?PHP echo $p->partner_other_specializations; ?></textarea>
            </div>
            <br clear="all">
          </div>
        </div>
      </div>
      <div class="panel panel-default"  style="border-bottom: 1px solid #ddd;">
        <div class="panel-heading">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseTwo<?PHP echo $mustarik; ?>"><i class="icon-chevron-sign-left"></i> الخبرة المهنية </a> </h4>
        </div>
        <div id="demo-collapseTwo<?PHP echo $mustarik; ?>" class="panel-collapse collapse" style="height: 0px;">
          <div class="panel-body">
            <h4 class="myheading" style="padding-right: 15px !important; margin-top: 15px;">الخبرة في نفس نشاط المشروع</h4>
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr>
                <th class="ex_th">تاريخ بداية المشروع</th>
                <th class="ex_th">اسم الجهة/المؤسسة/ المشروع الخاص</th>
                <th class="ex_th">نشاط الجهة/المؤسسة/ المشروع الخاص</th>
                <th class="ex_th">المهنة المزاولة بالجهة/المؤسسة/ المشروع الخاص</th>
                <th class="ex_th">عدد سنوات الخبرة</th>
              </tr>
              <?php for($i=0; $i<=2; $i++) { 
								   $xp = $partner_professional_experience[$i];
								   //New Code 
								  ?>
              <input name="experienceid<?PHP echo $mustarik; ?>[]" type="hidden"  value="<?PHP echo $xp->experienceid; ?>" class="form-control xx dateinput" id="experienceid<?PHP echo $i.$mustarik; ?>" placeholder="">
              <tr>
                <td><input name="option_one<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $xp->option_one; ?>" class="form-control xx dateinput" id="option_one<?PHP echo $i.$mustarik; ?>" placeholder="تاريخ"></td>
                <td><input name="option_two<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $xp->option_two; ?>" class="form-control xx" id="option_two<?PHP echo $mustarik; ?>" placeholder="اسم الجهة"></td>
                <td><input name="option_three<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $xp->option_three; ?>" class="form-control xx " id="option_three<?PHP echo $mustarik; ?>" placeholder="نشاط الجهة"></td>
                <td><input name="option_four<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $xp->option_four; ?>" class="form-control xx" id="option_four<?PHP echo $mustarik; ?>" placeholder="المهنة المزاولة بالجهة"></td>
                <td><input name="option_five<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $xp->option_five; ?>" class="form-control xx" id="option_five<?PHP echo $mustarik; ?>" placeholder="عدد سنوات الخبرة"></td>
              </tr>
              <?PHP } ?>
            </table>
            <h4 class="myheading" style="padding-right: 15px !important; margin-top: 15px;">الخبرة في أنشطة أخرى</h4>
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr>
                <th class="ex_th">تاريخ بداية المشروع</th>
                <th class="ex_th">اسم الجهة/المؤسسة/ المشروع الخاص</th>
                <th class="ex_th">نشاط الجهة/المؤسسة/ المشروع الخاص</th>
                <th class="ex_th">المهنة المزاولة بالجهة/المؤسسة/ المشروع الخاص</th>
                <th class="ex_th">عدد سنوات الخبرة</th>
              </tr>
              <?php for($j=0; $j<=2; $j++) 
						  { 
						  	$pq = $applicant_professional_experience[$j];
					?>
              <tr>
                <td><input name="activities_one<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $pq->activities_one; ?>" class="form-control xx  dateinput" id="activities_one<?php echo $j; ?>" placeholder="تاريخ"></td>
                <td><input name="activities_two<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $pq->activities_two; ?>" class="form-control xx" id="activities_two" placeholder="اسم الجهة"></td>
                <td><input name="activities_three<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $pq->activities_three; ?>" class="form-control xx" id="activities_three" placeholder="نشاط الجهة"></td>
                <td><input name="activities_four<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $pq->activities_four; ?>" class="form-control xx" id="activities_four" placeholder="المهنة المزاولة بالجهة"></td>
                <td><input name="activities_five<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $pq->activities_five; ?>" class="form-control xx" id="activities_five" placeholder="عدد سنوات الخبرة"></td>
              </tr>
              <?PHP } ?>
            </table>
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed " data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseThree<?PHP echo $mustarik; ?>"><i class="icon-chevron-sign-left"></i> السجلات التجارية الأخرى </a> </h4>
        </div>
        <div id="demo-collapseThree<?PHP echo $mustarik; ?>" class="panel-collapse collapse" style="height: 0px;">
          <div class="panel-body">
            <input value="مالك" type="checkbox" name="partner_activity<?PHP echo $mustarik; ?>" />
            مالك
            <input value="شريك" type="checkbox" name="partner_activity<?PHP echo $mustarik; ?>" />
            شريك
            <input value="مفوض بالتوقيع" type="checkbox" name="partner_activity<?PHP echo $mustarik; ?>" />
            مفوض بالتوقيع
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr>
                <th class="ex_th">اسم السجل</th>
                <th class="ex_th">رقم السجل</th>
                <th class="ex_th">عدد القوى العاملة الوطنية</th>
                <th class="ex_th">عدد القوى العاملة الوافدة</th>
              </tr>
              <?php 
			for($i=0; $i<=2; $i++)
			{	$act = $apb[$i];	?>
              <tr>
                <input name="bid<?PHP echo $mustarik; ?>[]" type="hidden"  value="<?PHP echo $act->bid; ?>" class="form-control " id="bid<?PHP echo $mustarik; ?>" placeholder="اسم الجهة">
                <td><input name="activity_name<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $act->activity_name; ?>" class="form-control " id="activity_name<?PHP echo $mustarik; ?>" placeholder="اسم الجهة"></td>
                <td><input name="activity_registration_no<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $act->activity_registration_no; ?>" class="form-control " id="activity_registration_no<?PHP echo $mustarik; ?>" placeholder="نشاط الجهة"></td>
                <td><input name="activity_nationalmanpower<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $act->activity_nationalmanpower; ?>" class="form-control " id="activity_nationalmanpower<?PHP echo $mustarik; ?>" placeholder="المهنة المزاولة بالجهة"></td>
                <td><input name="activity_laborforce<?PHP echo $mustarik; ?>[]" type="text"  value="<?PHP echo $act->activity_laborforce; ?>" class="form-control " id="activity_laborforce<?PHP echo $mustarik; ?>" placeholder="عدد سنوات الخبرة"></td>
              </tr>
              <?PHP } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
    
    <!----------------------------------------------------------------------------------> 
    
  </div>
</div>

<!doctype html>
<?PHP
	$ah_applicant = $_applicant_data['ah_applicant'];
	$ah_applicant_wife = $_applicant_data['ah_applicant_wife'];
	$ah_applicant_survayresult = $_applicant_data['ah_applicant_survayresult'];
	$ah_applicant_relation = $_applicant_data['ah_applicant_relation'];
	$ah_applicant_family = $_applicant_data['ah_applicant_family'];
	$ah_applicant_economic_situation = $_applicant_data['ah_applicant_economic_situation'];
	$ah_applicant_documents = $_applicant_data['ah_applicant_documents'];	
?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
  <form id="add_socilservayresult" name="add_socilservayresult" method="post" action="<?PHP echo base_url().'inquiries/add_socilservayresult' ?>" autocomplete="off">
    <input type="hidden" name="step" id="step" value="2" />
    <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $ah_applicant->applicantid;?>" />
    <input type="hidden" name="sarvayid" id="sarvayid" value="<?php echo $ah_applicant_survayresult->sarvayid;?>" />
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module, 'headxxx' => $heading)); ?>
      <div class="col-md-12">
        <div class="col-md-5 fox leftborder">
          <h4 class="panel-title customhr">بيانات طلب مساعدة</h4>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الاستمارة: </label>
            <strong><?PHP echo arabic_date($ah_applicant->applicantcode); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">التاريخ: </label>
            <strong><?PHP echo show_date($ah_applicant->registrationdate,5); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">نوع الطلب: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->charity_type_id); ?> </strong> </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الاسم الرباعي والقبيلة: </label>
            <strong><?PHP echo $ah_applicant->fullname; ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم جواز سفر: </label>
            <strong><?PHP echo arabic_date($ah_applicant->passport_number); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم البطاقة الشخصة: </label>
            <strong><?PHP echo arabic_date($ah_applicant->idcard_number); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">المحافظة \ المنطقة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->province); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">ولاية: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->wilaya); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">البلدة \ المحلة: </label>
            <strong><?PHP echo $ah_applicant->address; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">هاتف المنزل: </label>
            <strong><?PHP echo arabic_date($ah_applicant->hometelephone); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الهاتف :</label>
            <strong><?PHP echo arabic_date(implode('<br>',json_decode($ah_applicant->extratelephone,TRUE))); ?></strong></div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة الجتماعية:</label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->marital_status); ?></strong> </div>
          <?PHP foreach($ah_applicant_wife as $wife) { ?>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم الزوج\الزوجة: </label>
            <strong><?PHP echo $wife->relationname; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الجواز\البطاقة: </label>
            <strong><?PHP echo arabic_date($wife->relationdoc); ?></strong> </div>
          <?PHP } ?>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم الام: </label>
            <strong><?PHP echo $ah_applicant->mother_name; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الجواز\البطاقة: </label>
            <strong><?PHP echo arabic_date($ah_applicant->mother_id_card); ?></strong> </div>
          <!-----------------Documents--------------------------> 
          <!---------------------------------------------------->
          <div class="col-md-12 form-group">
            <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
              <h4 class="panel-title customhr">اترفق الوثائق والمستندات الازمة لطلب</h4>
              <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                <?PHP 
					$doccount = 0;					
					foreach($this->inq->allRequiredDocument($ah_applicant->charity_type_id) as $ctid) { $doccount++; 
						$doc = $ah_applicant_documents[$ctid->documentid];						
						$url = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$doc->document;
					?>
                <div class="panel panel-default" style="border-bottom: 1px solid #ddd;">
                  <div class="panel-heading" style="padding:7px 3px !important;" id="head<?PHP echo $ctid->documentid;?>">
                    <h4 class="panel-title" style="font-size:12px; font-weight:normal !important;">
                      <?PHP if($doc->appli_doc_id!='') { ?>
                      <span class="icons" id="removeicons<?PHP echo $doc->appli_doc_id; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> </span>
                      <?PHP } ?>
                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $ctid->documentid;?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                  </div>
                </div>
                <?PHP } ?>
              </div>
            </div>
          </div>
          <h4 class="panel-title customhr">افراد الأسرة: (بما فهيم العاملين و غيرالعاملين) </h4>
          <div class="col-md-12 form-group">
            <table class="table table-bordered table-striped dataTable">
              <thead>
                <tr role="row">
                  <th>الاسم</th>
                  <th style="width: 50px;">السن</th>
                  <th>القرابة</th>
                  <th>المهنة</th>
                  <th style="width: 70px;">الدخل الشهري</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?PHP 
			  
			  if($ah_applicant_relation)
			  {	
			  	foreach($ah_applicant_relation as $apr) { ?>
                <tr>
                  <td><?PHP echo $apr->relation_fullname; ?></td>
                  <td><?PHP echo arabic_date($apr->age); ?></td>
                  <td><?PHP echo $this->haya_model->get_name_from_list($apr->relationtype); ?></td>
                  <td><?PHP echo $this->haya_model->get_name_from_list($apr->professionid); ?></td>
                  <td><?PHP echo arabic_date($apr->monthly_income); ?></td>
                </tr>
                <?PHP }
			  }
			 ?>
              </tbody>
            </table>
          </div>
          <h4 class="panel-title customhr">االحالة الاقتصادية: </h4>
          <div class="col-md-6 form-group">
            <label class="text-warning">الدخل: </label>
            <strong><?PHP echo arabic_date($ah_applicant->salary); ?></strong> </div>
          <div class="col-md-6 form-group">
            <?PHP if($ah_applicant->source=='يوجد') { echo('يوجد: يوضح كالاتي'); } elseif($ah_applicant->source=='لايوجد') { echo('لايوجد: ويوضح مصدر المعيشة'); } ?>
          </div>
          <?PHP if($ah_applicant->source=='يوجد') { ?>
          <div class="col-md-6 form-group">
            <label class="text-warning">مصدره: </label>
            <strong><?PHP echo $ah_applicant->musadra; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">قيمتة: </label>
            <strong><?PHP echo $ah_applicant->qyama; ?></strong> </div>
          <?PHP } ?>
          <?PHP if($ah_applicant->source=='لايوجد') { ?>
          <div class="col-md-12 form-group">
            <label class="text-warning">مصدر المعيشة: </label>
            <strong><?PHP echo $ah_applicant->lamusadra; ?></strong> </div>
          <?PHP } ?>
          <div class="col-md-12 form-group">
            <label class="text-warning">وضف الحالة السكنية: </label>
            <?PHP echo $ah_applicant->current_situation; ?> </div>
          <div class="col-md-7 form-group">
            <label class="text-warning"><strong><?PHP echo $ah_applicant->ownershiptype; ?></strong> </label>
          </div>
          <div class="col-md-5 form-group"> <strong><?PHP echo arabic_date($ah_applicant->ownershiptype_amount); ?></strong> </div>
          <div class="col-md-12">
            <h4 class="panel-title customhr">مكونات المنزل:</h4>
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">نوع البناء:</label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->building_type); ?></strong> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">عدد الغرف:</label>
            <strong><?PHP echo arabic_date($ah_applicant->number_of_rooms); ?></strong> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">المرافق:</label>
            <strong><?PHP echo arabic_date($ah_applicant->utilities); ?></strong> </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">حالة الأثاث والموجودات:</label>
            <strong><?PHP echo $ah_applicant->furniture; ?><strong> </div>
          <div class="col-md-12">
            <h4 class="panel-title customhr">تفاصيل البنك:</h4>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم البنك:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant->bankid); ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الفرع:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant->branchid); ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الحساب:</label>
            <?PHP echo $ah_applicant->accountnumber; ?>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">نوع الحساب: جاري \ توفير: </label>
            <?PHP echo($ah_applicant->accounttype); ?>           
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">يرفق صورة من كشف الحسابات (حديث الاصدار):</label>           
            <?PHP
          	$statmentURL = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->bankstatement;
			echo getFileResult($statmentURL,'يرفق صورة من كشف الحسابات (حديث الاصدار)',$statmentURL);
		  ?>
          </div>  
        </div>
        <div class="col-md-7 fox">       
          <h4 class="panel-title customhr">بعد إجراء البحث الاجتماعي والاطلاع على المستندات المؤيدة اتضح مايلي :-</h4>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة الصحية:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('health_condition','health_condition',$ah_applicant_survayresult->health_condition,0,'req'); ?>
            
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة السكنية:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('housing_condition','housing_condition',$ah_applicant_survayresult->housing_condition,0,'req'); ?>
            
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">عدد أفراد الأسرة:</label>
            <?PHP number_drop_box('numberofpeople',$ah_applicant_survayresult->numberofpeople,'عدد أفراد الأسرة'); ?>
            
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">ترتيب مقدم الطلب بالأسرة:</label>
            <?PHP number_drop_box('positioninfamily',$ah_applicant_survayresult->positioninfamily,'ترتيب مقدم الطلب بالأسرة'); ?>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الحالة الاقتصادية:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('economic_condition','economic_condition',$ah_applicant_survayresult->economic_condition,0,'req'); ?>
            
          </div>
          <div class="col-md-12 form-group">
            <h4 class="panel-title customhr">نوع الحالة :-</h4>
            <label class="text-warning">للحالات الضمانية:</label>
            &nbsp;&nbsp;&nbsp;
            <input type="radio" name="casetype" data-id="zamanya" <?PHP if($ah_applicant_survayresult->casetype=='للحالات الضمانية') { ?> checked <?PHP } ?> class="casetypeinput" value="للحالات الضمانية">
            <label class="text-warning">للحالات غير الضمانية:</label>
            &nbsp;&nbsp;&nbsp;
            <input type="radio" name="casetype" data-id="ghairzamanya" <?PHP if($ah_applicant_survayresult->casetype=='للحالات غير الضمانية') { ?> checked <?PHP } ?> class="casetypeinput" value="للحالات غير الضمانية">
          </div>
          <div class="col-md-12 form-group casetype" id="zamanya" <?PHP if($ah_applicant_survayresult->casetype=='للحالات الضمانية') { ?> style="display:block !important;" <?PHP } ?>>
            <div class="col-md-12 form-group">
              <label class="text-warning">الحالة الضمانية باسم:</label>
              <input name="aps_name" value="<?PHP echo $ah_applicant_survayresult->aps_name; ?>" placeholder="الحالة الاقتصادية" id="aps_name" type="text" class="form-control">
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الحاسب:</label>
              <input name="aps_account" value="<?PHP echo $ah_applicant_survayresult->aps_account; ?>" placeholder="الحالة الاقتصادية" id="aps_account" type="text" class="form-control NumberInput">
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الملف:</label>
              <input name="aps_filename" value="<?PHP echo $ah_applicant_survayresult->aps_filename; ?>" placeholder="الحالة الاقتصادية" id="aps_filename" type="text" class="form-control  NumberInput">
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الفئة:</label>
              <?PHP echo $this->haya_model->create_dropbox_list('aps_category','casetype',$ah_applicant_survayresult->aps_category,0,''); ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">قيمة المعاش:</label>
              <input name="aps_salaryamount" value="<?PHP echo $ah_applicant_survayresult->aps_salaryamount; ?>" placeholder="الحالة الاقتصادية" id="aps_salaryamount" type="text" class="form-control  NumberInput">
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">تاريخ الربط:</label>
              <input name="aps_date" value="<?PHP echo $ah_applicant_survayresult->aps_date; ?>" placeholder="الحالة الاقتصادية" id="aps_date" type="text" class="form-control">
            </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">مصادر دخل أخرى:</label>
              <?PHP $json = json_decode($ah_applicant_survayresult->aps_another_income,TRUE);
			  		$inx = 0;
			  		for($mdi=1; $mdi<=4; $mdi++) { ?>    
                         
              <input style="margin-bottom:2px;" name="aps_another_income[]" value="<?PHP echo $json[$inx]; ?>" placeholder="مصادر دخل أخرى <?PHP echo arabic_date($mdi); ?>." id="aps_another_income" type="text" class="form-control NumberInput">
             
			  <?PHP $inx++; } ?>
            </div>
          </div>
          <div class="col-md-12 form-group casetype" id="ghairzamanya"  <?PHP if($ah_applicant_survayresult->casetype=='للحالات غير الضمانية') { ?> style="display:block !important;" <?PHP } ?>>
            <div class="col-md-12 form-group">
              <label class="text-warning">مصادر دخل أخرى:</label>
              <?PHP 
			  		$inxp = 0;
			  		for($mdi=1; $mdi<=4; $mdi++) { 
						
					?>
              <input style="margin-bottom:2px; width:50% !important;" name="aps_another_income[]" value="<?PHP echo $json[$inxp]; ?>" placeholder="مصادر دخل أخرى <?PHP echo arabic_date($mdi); ?>." id="aps_another_income" type="text" class="form-control  NumberInput">
              <?PHP $inxp++; } ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">اجمالي الدخل الشهري للفرد:</label>
              <input name="aps_month" value="<?PHP echo $ah_applicant_survayresult->aps_month; ?>" placeholder="اجمالي الدخل الشهري للفرد" id="aps_month" type="text" class="form-control  NumberInput">
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">اجمالي الدخل الشهري للاسرة:</label>
              <input name="aps_year" value="<?PHP echo $ah_applicant_survayresult->aps_year; ?>" placeholder="اجمالي الدخل الشهري للاسرة" id="aps_year" type="text" class="form-control NumberInput">
            </div>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الغرض من المساعدة:</label>
            <input name="whyyouwant" value="<?PHP echo $ah_applicant_survayresult->whyyouwant; ?>" placeholder="الغرض من المساعدة" id="whyyouwant" type="text" class="form-control">
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">ملخص الحالة:</label>
            <textarea name="summary" style="height:150px; resize:none;" id="summary" placeholder="ملخص الحالة"  class="form-control req"><?PHP echo $ah_applicant_survayresult->summary; ?></textarea>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">رأي الباحث الاجتماعي و مبرراته:</label>
            <textarea name="review" id="review" style="height:150px; resize:none;" placeholder="رأي الباحث الاجتماعي و مبرراته"  class="form-control req"><?PHP echo $ah_applicant_survayresult->review; ?></textarea>
          </div>
          <div class="col-md-12 form-group">
          <?PHP echo $this->haya_model->module_button(140,$ah_applicant->applicantid,'save_socilservayresult',$ah_applicant->case_close); ?>
         
          </div>
        </div>
        
      </div>
    </div>
    </div>
  </form>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script src="<?PHP echo base_url(); ?>assets/js/tasgeel_js.js"></script>
</body>
</html>
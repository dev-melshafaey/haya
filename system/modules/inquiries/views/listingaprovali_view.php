<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <ul class="nav nav-tabs panel panel-default panel-block">
           
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <?PHP $this->load->view("common/globalfilter",array('type'=>'mawafiq')); ?>
                <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"> </div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable"
                                           aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center;">رقم </th>
                        <th>الإسم </th>
                        <th style="width:35px; text-align:center;">النوع</th>
                        <th style="width:90px; text-align:center;">صيغة المشروع</th>
                        <th style="width:110px; text-align:center;">البطاقة الشخصية</th>
                        <th style="width: 205px; text-align:center;">المرحلة</th>
                        <th style="width: 120px; text-align:center;">الإجرائات </th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      
                    </tbody>
                  </table>
                </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'ajax/listingaproval_view/approved/queries/'.$branchid,'columns_array'=>' { "data": "رقم" },
                { "data": "الإسم" },
                { "data": "النوع" },
                { "data": "صيغة المشروعة" },
                { "data": "البطاقة الشخصية" },
                { "data": "المرحلة" },
                { "data": "الإجرائات" }')); ?>


<!-- /.modal-dialog -->

</div>
</body>
</html>
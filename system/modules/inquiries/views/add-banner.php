<?php $login_user	=	$this->session->userdata('userid');?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <form action="<?php echo current_url();?>" method="POST" id="add_banner" name="add_banner" autocomplete="off">
          <div class="col-md-12">
            <div class="panel panel-default panel-block">
              <div class="list-group">
                <div class="list-group-item" id="input-fields">                  
                  <div class="form-group col-md-6">
                    <label for="basic-input">اسم الصورة</label>
                    <input type="text" class="form-control req" name="image_title" id="image_title" placeholder="الاسم الأو" value="<?php echo (!empty($banner_detail)?$banner_detail->image_title : NULL);?>"/>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="basic-input">ترتيب</label>
                    <input type="text" class="form-control" name="image_order" id="image_order" placeholder="ترتيب" value="<?php echo (!empty($banner_detail)?$banner_detail->image_order : NULL);?>"/>
                  </div>
                  <div class="form-group col-md-3 uploader-banner" id="bannerupload">
                    <label for="basic-input">تحميل</label>
                    <div class="browser">
                    <input style="border: 0px !important; color: #d09c0d;" type="file" name="files" title='تحميل'>
                    <input type="hidden" name="attachmentbanner" id="attachmentbanner" />
                    <input type="hidden" name="imageid" id="imageid" value="<?php echo (!empty($banner_detail)	?	$banner_detail->imageid : NULL);?>"/>
                    <input type="hidden" name="db_banner_image" id="db_banner_image" value="<?php echo (!empty($banner_detail)	?	$banner_detail->banner_image : NULL);?>"/>
                    <input type="hidden" name="userid" id="userid" value="<?php echo $login_user;?>"/>
                     <input type="hidden" name="branch_id" id="branch_id" value="<?php //echo get_branch_id($login_user);?>"/>
                  </div>
                  </div>
                  
                  <br clear="all" />
                   <div class="form-group col-md-12">
                   <div id="show-image">
               <?php if(!empty($banner_detail) AND $banner_detail->banner_image	!= ""): ?>
               <i class="icon-remove-sign doc8remove0" style="color:#CC0000;cursor:pointer" onclick="deleteFile('<?php echo $banner_detail->banner_image;?>');"></i>
               	<img src="<?php echo base_url();?>upload_files/banners/<?php echo $banner_detail->banner_image; ?>" width="400"/>
               <?php endif;?>
                   </div>
                   </div>
               <br clear="all" />
               <?php if(!empty($banner_detail) AND $banner_detail->banner_image	!= ""): ?>
               	<div class="col-md-12" style="  margin-top: 4px; margin-bottom: 10px;  margin-right: 13px; ">
                <input type="hidden" name="banner_image" id="banner_image" value="<?php echo (!empty($banner_detail)?$banner_detail->banner_image : NULL);?>"/>
                  <button type="button" id="update_banner" class="btn btn-lg btn-success">حفظ</button>
                </div>
               <?php endif;?>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<script>
    $(function(){        
      var branchid = '<?PHP echo $branchid; ?>';
	
    } );
function deleteFile(image)
{
	var r = confirm("Are you sure You want to delete");
	if (r == true)
	{		
		$.ajax({
			url: config.BASE_URL+'inquiries/delete_banner_ajax/',
			type: "POST",
			data:{'image':image},
			dataType: "html",
			success: function(msg){
			console.log(msg);
			if(msg)
			{
				$("#show-image").hide();							
			}
			}
		});		
	}		
}
</script>
<?php $this->load->view('common/footer'); ?>
<!-- /.modal-dialog -->
</div>
</body>
</html>
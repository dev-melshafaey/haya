<?PHP
$main = $m;
$appli = 	$main->applicant;
?>

<div class="row setMargin">
  <div class="col-md-12">
    <input type="hidden" name="applicantid[<?php echo $index; ?>]" id="applicantid<?php echo $in ?>" value="<?php echo $appli->applicantid; ?>" />
    <div class="form-group col-md-6">
      <label class="text-warning">الاسم الأول</label>
      <input name="first_name[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" value="<?PHP echo $appli->first_name; ?>" placeholder="الاسم الأول" id="first_name" type="text" class="req tempapplicant form-control">
    </div>
    <div class="form-group col-md-6">
      <label class="text-warning">الاسم الثاني</label>
      <input name="middle_name[<?php echo $index; ?>]"  data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" value="<?PHP echo $appli->middle_name; ?>" placeholder="الاسم الثاني" id="middle_name" type="text" class="tempapplicant form-control">
    </div>
    <div class="form-group col-md-6">
      <label class="text-warning">الاسم الثالث</label>
      <input name="last_name[<?php echo $index; ?>]"  data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" value="<?PHP echo $appli->last_name; ?>" placeholder="الاسم الثالث" id="last_name" type="text" class="tempapplicant form-control">
    </div>
    <div class="form-group col-md-6">
      <label class="text-warning">القبيلة / العائلة</label>
      <input name="sur_name[<?php echo $index; ?>]"  data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" value="<?PHP echo $appli->family_name; ?>" placeholder="القبيلة / العائلة" id="family_name" type="text" class="tempapplicant form-control">
    </div>
    <div class="form-group col-md-6">
      <label class="text-warning">النوع</label>
      <br>
      <input type="radio" name="applicanttype[<?php echo $index; ?>]" <?PHP if($appli->applicanttype=='ذكر') { ?>checked="checked"<?PHP } ?>   data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" value="ذكر" data-title="personal"  required />
      ذكر
      &nbsp;&nbsp;&nbsp;&nbsp;
      <input type="radio" name="applicanttype[<?php echo $index; ?>]" <?PHP if($appli->applicanttype=='أنثى') { ?>checked="checked"<?PHP } ?>   data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" value="أنثى" data-title="partner" />
      أنثى </div>
    <div class="form-group col-md-6">
      <label class="text-warning">رقم البطاقة الشخصية</label>
      <input name="idcard[<?php echo $index; ?>]"  value="<?PHP echo $appli->idcard; ?>" id="idcard" placeholder="رقم البطاقة الشخصية" type="text" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" class="tempapplicant form-control NumberInput autocomplete">
    </div>
    <div class="form-group col-md-6">
      <label class="text-warning">رقم سجل القوى العاملة</label>
      <input name="cr_number[<?php echo $index; ?>]"  value="<?PHP echo $appli->cr_number; ?>" id="cr_number" placeholder="رقم سجل القوى العاملة" type="text" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" class="tempapplicant form-control NumberInput autocomplete ">
    </div>
    <div class="form-group col-md-4">
      <label class="text-warning">تاريخ الميلاد</label>
      <input name="datepicker[<?php echo $index; ?>]" type="text"  value="<?PHP echo $appli->datepicker; ?>" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" class="tempapplicant form-control age_datepicker p_age"  id="datepicker_<?php echo $index; ?>" placeholder="تاريخ الميلاد" size="15" maxlength="10">
    </div>
    <div class="form-group col-md-2">
      <label class="text-warning">العمر</label>
      <input name="age[<?php echo $index; ?>]" type="text" class="form-control" id="age_datepicker_<?php echo $index; ?>" value="<?PHP echo $agenumber ?>"  placeholder="العمر" size="5" maxlength="8" readonly>
    </div>
    <div class="form-group col-md-6" style="margin-bottom: 0px !important;">
      <label class="text-warning">رقم الهاتف</label>
    </div>
    <div class="form-group col-md-6">
      <label class="text-warning"></label>
    </div>
    <?PHP for($p=0; $p<=3; $p++) { ?>
    <div class="form-group col-md-3">
      <input data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>_<?PHP echo $phones->phoneid; ?>" name="phone_numbers[]" value="<?PHP echo $phones->phonenumber; ?>"  type="text" class="tempapplicant <?PHP if($p==0) { ?>req<?PHP } ?> form-control applicantphone" id="phonenumber" placeholder="رقم الهاتف" maxlength="8">
    </div>
    <?PHP }  ?>
    <div class="form-group col-md-6">
      <label class="text-warning">الحالة الاجتماعية</label>
      <?PHP multiporpose_dropbox_partner($index,'marital_status',$appli->applicantid,$main->tempid,$appli->marital_status,'اختر الحالة الاجتماعية','maritalstatus','',$appli->marital_status_text,'كم عدد الأطفال لديك','marital_status_text'); ?>
    </div>
    <div class="form-group col-md-6">
      <label class="text-warning">الوضع الحالي</label>
      <?PHP multiporpose_dropbox_partner($index,'job_status',$appli->applicantid,$main->tempid,$appli->job_status,'اختر الوضع الحالي','current_situation','',$appli->job_status_text,'الوضع الحالي','job_status_text'); ?>
    </div>
    <div class="form-group col-md-6">
      <label class="text-warning">العنوان الشخصي</label>
      <?PHP reigons_partner_inq($index,'province',$appli->province,'req',$main->tempid,$appli->applicantid); ?>
    </div>
    <div class="form-group col-md-6">
      <label class="text-warning">الولاية</label>
      <?PHP election_wilayats_partner_inq($index,'walaya',$appli->walaya,$appli->province,'req',$main->tempid,$appli->applicantid); ?>
    </div>
    <div class="form-group col-md-6">
      <label class="text-warning">رقم بطاقة سجل القوى العاملة</label>
      <input name="mr_number[<?php echo $index; ?>]" id="mr_number" value="<?PHP echo $appli->mr_number; ?>" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" placeholder="رقم بطاقة سجل القوى العاملة" type="text" class="tempapplicant form-control NumberInput">
    </div>
    <div class="form-group col-md-6">
      <div class="form-group col-md-8">
        <label class="text-warning">هل مسجل في التأمينات الإجتماعية؟:</label>
        <input id="is_insurance" onclick="pass_id(this,'insinfo','<?php echo $index; ?>')" type="radio" <?PHP if($appli->is_insurance=='Y') { ?>checked="checked"<?PHP } ?> name="is_insurance[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" class="ins" value="Y" />
        نعم
        <input id="is_insurance"  onclick="pass_id(this,'insinfo','<?php echo $index; ?>')"  <?PHP if($appli->is_insurance=='N') { ?>checked="checked"<?PHP } ?> type="radio" class="ins" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" name="is_insurance[<?php echo $index; ?>]" value="N" />
        لا </div>
      <div class="form-group col-md-4 arrange_field" id="insinfo<?php echo $index; ?>" <?PHP if($appli->is_insurance=='Y') { ?>style="display:block !Important;"<?PHP } else{?> style="display:none !Important;"<?php } ?>>
        <label class="text-warning">رقم التسجيل</label>
        <input name="insurance_number[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $main->tempid; ?>" value="<?PHP echo $appli->insurance_number; ?>" id="insurance_number" placeholder="رقم التسجيل" type="text" class="tempapplicant form-control">
      </div>
    </div>
    <div class="form-group col-md-11">
      <label class="text-warning">هل لديك مشروع؟</label>
      <input id="confirmation"  onclick="pass_id(this,'extrainfo','<?php echo $index; ?>')" type="radio" <?PHP if($appli->confirmation=='Y') { ?>checked="checked"<?PHP } ?> name="confirm[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" class="conf" value="Y" />
      نعم
      <input id="confirmation" onclick="pass_id(this,'extrainfo','<?php echo $index; ?>')" <?PHP if($appli->confirmation=='N') { ?>checked="checked"<?PHP } ?> type="radio" class="conf" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" name="confirm[<?php echo $index; ?>]" value="N" />
      لا </div>
    <?PHP
    	$display = 'none';
		if($appli->confirmation == 'Y')	{	$display = 'block';	}
	?>
    <div class="form-group col-md-12" id="extrainfo<?php echo $index; ?>" style="display:<?php echo $display; ?>">
      <div class="form-group col-md-6">
        <label class="text-warning">اسم المشروع</label>
        <input name="project_name[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" value="<?PHP echo $appli->project_name; ?>" id="project_name" placeholder="اسم المشروع" type="text" class="tempapplicant form-control">
      </div>
      <div class="form-group col-md-6">
        <label class="text-warning">موقع المشروع</label>
        <input name="project_location[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" value="<?PHP echo $appli->project_location; ?>" id="project_location" placeholder="المكان" type="text" class="tempapplicant form-control">
      </div>
      <div class="form-group col-md-6">
        <label class="text-warning">نشاط المشروع</label>
        <input name="project_activities[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" value="<?PHP echo $appli->project_activities; ?>" id="project_activities" placeholder="نشاط المشروع" type="text" class="tempapplicant form-control">
      </div>
      <div class="form-group col-md-6">
        <label class="text-warning">الاسم التجاري</label>
        <input name="project_cr_name[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" value="<?PHP echo $appli->project_cr_name; ?>" id="project_cr_name" placeholder="الاسم التجاري" type="text" class="tempapplicant form-control">
      </div>
      <div class="form-group col-md-6" id="extrainfo_q">
        <label class="text-warning">هل سبق لك الحصول على قرض للمشروع؟</label>
        <input id="is_loan" onclick="pass_id(this,'question_details','<?php echo $index; ?>')" type="radio" <?PHP if($appli->is_loan=='Y') { ?>checked="checked"<?PHP } ?> name="is_loan[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" class="confirmation_q " value="Y" />
        نعم
        <input id="is_loan" onclick="pass_id(this,'question_details','<?php echo $index; ?>')" <?PHP if($appli->is_loan=='N') { ?>checked="checked"<?PHP } ?> type="radio" class="confirmation_q  " data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" name="is_loan[<?php echo $index; ?>]" value="N" />
        لا </div>
      <div class="form-group question_details col-md-6" id="question_details<?php echo $index; ?>" <?PHP if($main->is_loan=='Y') { ?>style="display:block;"<?PHP } else { ?>style="display:none;"<?PHP } ?>>
        <li class="data_list_grid">
          <input id="is_bank_loan"  type="checkbox" <?PHP if($main->is_bank_loan=='1') { ?>checked="checked"<?PHP } ?> name="is_bank_loan[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" class="" value="1" />
          بنك التنمية العماني </li>
        <li class="data_list_grid">
          <input id="is_rafd_loan"  type="checkbox" <?PHP if($main->is_rafd_loan=='1') { ?>checked="checked"<?PHP } ?> name="is_rafd_loan[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $main->tempid; ?>" class="" value="1" />
          صندوق شراكة
          </liv>
        <li class="data_list_grid">
          <input id="is_commercial_loan"  type="checkbox" <?PHP if($main->is_commercial_loan=='1') { ?>checked="checked"<?PHP } ?> name="is_commercial_loan[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" class="" value="1" />
          بنك تجاري </li>
        <li class="data_list_grid">
          <input id="is_other_loan<?php echo $index; ?>"  onclick="pass_id(this,'other_value','<?php echo $index; ?>')"  type="checkbox" <?PHP if($main->is_other_loan =='1') { ?>checked="checked"<?PHP } ?> name="is_other_loan[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" class="" value="1" />
          اخرى </li>
        <li class="data_list_grid">
          <?PHP if($main->is_other_loan =='1') { 
				 	 $dislay = "Block";
				 }
				 else{
					$dislay = "None";
				 }
				 
				 ?>
          <input id="other_value<?php echo $index; ?>" name="other_value[<?php echo $index; ?>]" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $appli->applicantid; ?>" value="<?PHP echo $main->project_location; ?>"  placeholder="اخرى" type="text" class="tempapplicant form-control" style="display:<?php echo $dislay; ?>">
        </li>
      </div>
    </div>
  </div>
</div>
<!----------------------------------------> 
<!----------------------------------------> 


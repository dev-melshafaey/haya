<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inquiries_model extends CI_Model {
	
	/*
	* Properties
	*/
	private $_table_users;	
	private $_table_donate;	
	private $_table_applied_donation;
	private $_table_sms_management;
	//----------------------------------------------------------------------    
	/*
	* Constructor
	*/	
	function __construct()
    {
        parent::__construct();		
		$this->load->helper("file");		
		//Load Table Names from Config
		$this->_table_users 			=  $this->config->item('table_users');
		$this->_table_donate 			=  $this->config->item('table_donate');
        $this->_table_user_profile 		=  $this->config->item('table_user_profile');
		$this->_table_applied_donation 	=  $this->config->item('table_applied_donation');
		$this->_table_sms_management 	=  $this->config->item('table_sms_management');
		$this->_login_userid			=	$this->session->userdata('userid');
    }
	
	function allRequiredDocument($charity_type_id_value)
	{
		$this->db->select('documentid,documenttype,isrequired');
        $this->db->from('ah_document_required');
        $this->db->where('charity_type_id',$charity_type_id_value);
		$this->db->where('documentstatus',1);
        $this->db->order_by("documentorder", "ASC");
        $query = $this->db->get();
		return $query->result();
		
	}
	
	function submitfor()
	{
		$form_data = $this->input->post();
		
		$applicantid = $form_data['applicantid'];
		$logged_in_user_detail = $this->haya_model->get_user_detail($this->_login_userid);
		$ah_applicant['userid'] = $this->_login_userid;
		$ah_applicant['branchid'] = $logged_in_user_detail['profile']->branchid;
		$ah_applicant['charity_type_id'] = $form_data['charity_type_id'];
		$ah_applicant['province'] = $form_data['province'];
		$ah_applicant['wilaya'] = $form_data['wilaya'];
		$ah_applicant['address'] = $form_data['address'];
		$ah_applicant['fullname'] = $form_data['fullname'];		
		if($applicantid=='')
		{	$ah_applicant['passport_number'] = $form_data['passport_number'];
			$ah_applicant['idcard_number'] = $form_data['idcard_number']; 	}
			
		$ah_applicant['hometelephone'] = $form_data['hometelephone'];
		$ah_applicant['extratelephone'] = json_encode($form_data['extratelephone']);
		$ah_applicant['marital_status'] = $form_data['marital_status'];
		$ah_applicant['income'] = $form_data['income'];
		$ah_applicant['mother_name'] = $form_data['mother_name'];
		if($applicantid=='') { $ah_applicant['mother_id_card'] = $form_data['mother_id_card'];	}
		$ah_applicant['salary'] = $form_data['salary'];
		$ah_applicant['source'] = $form_data['source'];
		$ah_applicant['musadra'] = $form_data['musadra'];
		$ah_applicant['qyama'] = $form_data['qyama'];
		$ah_applicant['lamusadra'] = $form_data['lamusadra'];
		$ah_applicant['current_situation'] = $form_data['current_situation'];
		$ah_applicant['ownershiptype'] = $form_data['ownershiptype'];
		$ah_applicant['ownershiptype_amount'] = $form_data['ownershiptype_amount'];
		$ah_applicant['building_type'] = $form_data['building_type'];
		$ah_applicant['number_of_rooms'] = $form_data['number_of_rooms'];
		$ah_applicant['utilities'] = $form_data['utilities'];
		$ah_applicant['furniture'] = $form_data['furniture'];
		$ah_applicant['bankid'] = $form_data['bankid'];
		$ah_applicant['bankbranchid'] = $form_data['bankbranchid'];
		$ah_applicant['accountnumber'] = $form_data['accountnumber'];
		$ah_applicant['accounttype'] = $form_data['accounttype'];
		if($applicantid!='')
		{	$this->db->where('applicantid',$applicantid);
			$this->db->update('ah_applicant',json_encode($ah_applicant),$this->_login_userid,$ah_applicant); 	}
		else
		{	$this->db->insert('ah_applicant',$ah_applicant,json_encode($ah_applicant),$this->_login_userid);	
			$applicantid = $this->db->insert_id(); 	
			$this->haya_model->save_steps($applicantid,1);
			}
			
			$applicantcode = $applicantid.$logged_in_user_detail['profile']->branchid.$this->_login_userid.$form_data['charity_type_id'];
		//Uploading Path
			$fullPath = './resources/applicants/'.$applicantcode.'/';
		//Deleting Data
			$this->db->query("DELETE FROM ah_applicant_wife WHERE applicantid='".$applicantid."'");
			$this->db->query("DELETE FROM ah_applicant_relation WHERE applicantid='".$applicantid."'");
			$this->db->query("UPDATE ah_applicant SET `applicantcode`='".$applicantcode."' WHERE applicantid='".$applicantid."'");
		/////////////////
		foreach($form_data['relationname'] as $relationkey => $relationvalue)
		{
			if($relationvalue!='')
			{
				$ah_applicant_wife['applicantid'] =  $applicantid;
				$ah_applicant_wife['relationname'] = $relationvalue;
				$ah_applicant_wife['relationdoc'] = $form_data['relationdoc'][$relationkey];
				$this->db->insert('ah_applicant_wife',$ah_applicant_wife,json_encode($ah_applicant_wife),$this->_login_userid);
			}
		}
		
		foreach($form_data['relation_fullname'] as $rfkey => $rfvalue)
		{
			if($rfvalue!='')
			{
				$ah_applicant_relation = array('applicantid'=>$applicantid,
					'relationtype'=>$form_data['relationtype'][$rfkey],
					'relationtype_text'=>$form_data['relationtype_text'][$rfkey],
					'relation_fullname'=>$rfvalue,
					'age'=>$form_data['age'][$rfkey],
					'professionid'=>$form_data['professionid'][$rfkey],
					'professionid_text'=>$form_data['professionid_text'][$rfkey],
					'monthly_income'=>$form_data['monthly_income'][$rfkey],
					'sort_order'=>$rfkey);
					$this->db->insert('ah_applicant_relation',$ah_applicant_relation,json_encode($ah_applicant_relation),$this->_login_userid);
			}
		}
		
		
		if($_FILES['bankstatement']['name']!='')
		{
			$ah_applicant_bankstatement['bankstatement'] = $this->haya_model->upload_file('bankstatement',$fullPath);
			$this->db->where('applicantid',$applicantid);
			$this->db->update('ah_applicant',json_encode($ah_applicant_bankstatement),$this->_login_userid,$ah_applicant_bankstatement);
		}
		
		foreach($this->inq->allRequiredDocument($form_data['charity_type_id']) as $ctid) 
		{
			if($_FILES['doclist'.$ctid->documentid]['name']!='')
			{
				$this->db->query("DELETE FROM ah_applicant_documents WHERE applicantid='".$applicantid."' AND documentid='".$ctid->documentid."'");
				$ah_applicant_documents['applicantid'] = $applicantid;
				$ah_applicant_documents['userid'] = $this->_login_userid;
				$ah_applicant_documents['documentid'] = $ctid->documentid;
				$ah_applicant_documents['document'] = $this->haya_model->upload_file('doclist'.$ctid->documentid,$fullPath);
				$this->db->insert('ah_applicant_documents',$ah_applicant_documents,json_encode($ah_applicant_documents),$this->_login_userid);				
			}
			
		}
		
		$ah_applicant_notes['applicantid'] = $applicantid;
		$ah_applicant_notes['userid'] = $this->_login_userid;
		$ah_applicant_notes['branchid'] = $logged_in_user_detail['profile']->branchid;
		$ah_applicant_notes['notes'] = $this->input->post("notes");
		$ah_applicant_notes['notestype'] = 'from_receipt';
		$this->db->insert('ah_applicant_notes',$ah_applicant_notes,json_encode($ah_applicant_notes),$this->_login_userid);
		redirect(base_url().'inquiries/transactions');
	}
	//----------------------------------------------------------------------
	/*
	* Insert User Record
	* @param array $data
	* return True
	*/
	function insert_user_detail($data)
	{
		$this->db->insert($this->_table_users,$data);
		
		return TRUE;
	}
	/***********************************************/
	
    //29/12/2014-------------------------------------START
    public function users_role()
    {
        $this->db->select('id,user_name,firstname,lastname,user_role_id');
        $this->db->from('admin_users');
        $this->db->where('status',1);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();

        $dropdown = '<select name="userid" id="userid" class="form-control">';
        $dropdown .= '<option value="">حدد المستخدم</option>';

        foreach($query->result() as $row)
        {
            $dropdown .= '<option value="'.$row->id.'" ';
            if($value==$row->id)
            {
                $dropdown .= 'selected="selected"';
            }
            $dropdown .= '>'.$row->firstname.' '.$row->lastname.' ('.$row->user_name.')</option>';
        }
        $dropdown .= '</select>';
        echo($dropdown);
    }
    //29/12/2014-------------------------------------END
    //10/01/2015-------------------------------------START
    function user_perm_list()
    {
        $this->db->select('id,user_name,firstname,lastname,user_role_id');
        $this->db->from('admin_users');
        $this->db->where('status',1);
        $this->db->order_by("firstname", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
	

    function parent_module()
    {
        $this->db->select('moduleid,module_name,module_icon');
        $this->db->from('mh_modules');
        $this->db->where('module_parent',0);
        $this->db->where('module_status','A');
        $this->db->order_by("module_order", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function childe_module($parentid)
    {
        $this->db->select('moduleid,module_name,module_icon');
        $this->db->from('mh_modules');
        $this->db->where('module_parent',$parentid);
        //$this->db->where('module_status','A');
        $this->db->order_by("module_order", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
    //10/01/2015-------------------------------------END
	function getAprovalList($applicant_id){
		$sql = "SELECT 
  SUM(
    sealed_company + commercial_papers + municipal_contractrent + membership_certificate +company_general_authority+open_account+check_book+registration_zip
  ) AS total 
FROM
  `check_list` AS cl 
WHERE cl.`applicant_id` = '".$applicant_id."' ";
		$q = $this->db->query($sql);
		return $r =  $q->result();	
	}
	

	function deleteTempDelete()
	{
		//Login Query
		$userid = $this->session->userdata('userid');
		$this->db->where('userid',$userid);
		$query = $this->db->get('applicant_temp_document');		
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $temp_data)
			{
				$path = APPPATH.'../upload_files/'.$userid.'/'.$temp_data->documentname;
				if(file_exists($path))
				{
					unlink($path); //Delete temporary uploaded files
				}
				$this->db->where("documentid",$temp_data->documentid);
				$this->db->delete('applicant_temp_document');
			}
		}
	}
	
	
	function checkApplicaitonProject($id){
		$this->db->where('applicant_id',$id);
		$query = $this->db->get('applicant_project');
		
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

//----------------------------------------------------------------------
	/*
	* Insert User Record
	* @param array $data
	* return True
	*/
	function insert_donation_product($data)
	{
		$this->db->insert($this->_table_donate,$data);
		
		return TRUE;
	}
//----------------------------------------------------------------------
    /*
    * insert user_profile method
    */
    function insert_user_profile($data)
    {
        $this->db->insert($this->_table_user_profile,$data);

        return TRUE;
    }
//----------------------------------------------------------------------
	/*
	* User login
	* @param string $user_email
	* @param string $password
	* return Login User Data
	*/
	function login_user($user_email,$password)
	{
		//Login Query
		$this->db->where('email',$user_email);
		$this->db->where('password',md5($password));
		$query = $this->db->get($this->_table_users);
		
		//Check if Result is Greater Than Zero
		
		if($query->num_rows() > 0)
		{
			$this->session->set_userdata('user_id',$query->row()->user_id);
			return $query->row();
		}
	}
	
	function loan_data($id)
	{
		//Login Query
		$this->db->where('loan_caculate_id',$id);
		$query = $this->db->get('loan_calculate');
		
		//Check if Result is Greater Than Zero
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	
	function get_childLoan_data($id)
	{
		//Login Query
		$this->db->where('parent_id',$id);
		$query = $this->db->get('loan_calculate');
		
		//Check if Result is Greater Than Zero
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	
	function get_app_ids($user_id)
	{
		$query = $this->db->query(
		"SELECT
		GROUP_CONCAT(applicantid) AS applicants
		FROM applicant_process_log WHERE userid='$user_id' AND stepsid = '1'");
		if($query->num_rows() > 0)
		{
		return $query->row();
		}
	}
	function get_all_applicatnts_data($data)
	{
		//Login Query
		$this->db->where_in('applicant_id',$data);
		$query = $this->db->get('applicants');
		 
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
		return $query->result();
		}
	}
	
		
	
//----------------------------------------------------------------------
	
	function getChild($parentId){
			
			$this->db->where('parent_id',$parentId);
		$query = $this->db->get('loan_calculate');
		
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//------------------------------------------------------------------------

    /**
     * 
     * Insert User Data for Registration
     * @param array $data
     * return integer
     */
	function update_user($userid,$data)
	{
		$update['firstname'] = $data['firstname'];
		$update['lastname'] = $data['lastname'];
		$update['password'] = ($data['password']);	
		$update['email'] = $data['email'];
		$update['mobile_number'] = $data['number'];
		$update['number'] = $data['number'];
		$update['about_user'] = $data['about_user'];
		$update['branch_id'] = $data['branch_id'];
		$update['user_role_id'] = $data['user_role_id'];
		$update['user_parent_role'] = $data['user_parent_role'];
		$update['zyarapermission'] = $data['zyarapermission'];
		$this->db->where('id',$userid);
		$this->db->update('admin_users', $update);		
		return TRUE;
	}
		
	public function getLastDetail($tempid='',$format='array')
	{
		$this->session->unset_userdata('inq_id');		
		$this->db->select('*');
		$this->db->from('main');		
		$this->db->where('tempid',$tempid);
		$this->db->order_by("applicantdate", "DESC"); 
		$this->db->limit(1);
		$tempMain = $this->db->get();
		if($tempMain->num_rows() > 0)
		{
			foreach($tempMain->result() as $data)
			{				
				$arr['main'] = $data;
				$tempid = $data->tempid;							
				$tempMain->free_result(); //Clean Result Set
				//Getting Applicant Data
				$this->db->select('*');
				$this->db->from('main_applicant');		
				$this->db->where('tempid',$tempid);
				$this->db->order_by("applicantid", "ASC");
				$tempApplicant = $this->db->get();
				if($tempApplicant->num_rows() > 0)
				{
					foreach($tempApplicant->result() as $app)
					{
                        $arr['main']->applicant[] = $app;
						//Getting Phone Data
						$this->db->select('*');
						$this->db->from('main_phone');		
						$this->db->where('tempid',$tempid);
						$this->db->where('applicantid',$app->applicantid);
						$this->db->order_by("phoneid", "ASC");
						$tempPhone = $this->db->get();
						if($tempPhone->num_rows() > 0)
						{
							foreach($tempPhone->result() as $phones)
							{
								$arr['main']->phones[$app->applicantid][] = $phones;						
							}
							$tempPhone->free_result(); //Clean Result Set
						}					
					}
					$tempApplicant->free_result(); //Clean Result Set
				}				
				//Getting Inquiry Type Data
				$this->db->select('*');
				$this->db->from('main_inquirytype');		
				$this->db->where('tempid',$data->tempid);
				$this->db->order_by("inqid", "ASC");
				$tempIType = $this->db->get();
				if($tempIType->num_rows() > 0)
				{
					foreach($tempIType->result() as $itype)
					{
						$arr['main']->inquery[] = $itype;
					}
					$tempIType->free_result(); //Clean Result Set
				}				
				//Getting Notes Data
				$this->db->select('*');
				$this->db->from('main_notes');		
				$this->db->where('tempid',$data->tempid);
				$this->db->order_by("notesid", "ASC");
				$tempNotes = $this->db->get();
				if($tempNotes->num_rows() > 0)
				{
					foreach($tempNotes->result() as $notes)
					{
						$arr['main']->notes[] = $notes;
						//$arr['notes'][] = $notes;
					}
					$tempNotes->free_result(); //Clean Result Set
				}				
								
			}
		}
		if($format=='json')
		{
			echo json_encode($arr);
		}
		else
		{
		return $arr;
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_data_for_store($limit,$start){
		//Login Query
		$this->db->where('permission','1');
		$this->db->limit($limit,$start);	
		$query = $this->db->get($this->_table_donate);
		
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	function getsave_document(){
		//Login Query
		$userid = $this->session->userdata('userid');
		$query = $this->db->query("SELECT * FROM applicant_temp_document WHERE userid='".$userid."'");		
		if($query->num_rows() > 0)
		{
			foreach($query->result_array() as $ddx)
			{
				$ar[] = $ddx;
			}
			return $ar;
		}
	}	

//----------------------------------------------------------------------
	/*
	* Get All Records From Giving Table
	*/
	public function get_rows($table_name)
	{
		$count_all = $this->db->get($table_name);
		if($count_all->num_rows() > 0)
		{
			return $count_all->num_rows();
		}
	}
//----------------------------------------------------------------------
	/*
	* Insert Applied Product Record into Database
	* Return True 
	*/
	public function applied_donation($data)
	{
		$this->db->insert($this->_table_applied_donation,$data);
		return TRUE;
	}
//----------------------------------------------------------------------
	/*
	* Insert Applied Product Record into Database
	* Return True 
	*/
	public function add_user_registration($data)
	{
		$this->db->insert('admin_users',$data);
		return TRUE;
	}

//----------------------------------------------------------------------
	/*
	* Get Applied Product Record into Database
	* Return True 
	*/
	public function get_applied_detail($user_id,$donate_id)
	{
		$this->db->where('user_id',$user_id);
		$this->db->where('donation_id',$donate_id);

		$query = $this->db->get($this->_table_applied_donation);
		
		if ($query->num_rows() > 0)
		{
			return $query->row()->donation_id;
		}
	}
//----------------------------------------------------------------------
	/*
	* Search Applied Products if 
	* Return True 
	*/
	public function donation_id_exists()
	{
		$this->db->where('user_id',$this->session->userdata('user_id'));
		$this->db->select('donation_id');
		$query = $this->db->get($this->_table_applied_donation);
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	public function update_db($table,$data,$condition){
		return $this->db->update($table,$data,$condition);
	}

    //29/12/2014-------------------------------------START

	//29/12/2014-------------------------------------END

/*
	* Search Applied Products if 
	* Return True 
	*/
	public function getInquiriesSms($type)
	{
		$this->db->where('sms_module','inquiry');
		$this->db->where('type',$type);
		$this->db->where('status',1);
		$this->db->select('*');
		$this->db->order_by('sms_order','ASC');
		$query = $this->db->get($this->_table_sms_management);
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	public function getSms($module_id,$type)
	{
		$this->db->where('sms_module_id',$module_id);
		$this->db->where('type',$type);
		
		if($module_id  == 1)
		{
			$this->db->join('register_auditors',"auditor_id=sms_receiver_id");
		}
		
		$this->db->order_by("sms_id", "DESC"); 
		$this->db->select('*');
		$query = $this->db->get('sms_history');
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
		
	}
	function get_sms_history($module_id,$userid)
	{
		if($module_id	==	'1')
		{
			$this->db->select('sms_history.*,admin_users.firstname,admin_users.lastname,main_applicant.first_name,main_applicant.middle_name,main_applicant.last_name,main_applicant.family_name');
		}
		else
		{
			$this->db->select('sms_history.*,admin_users.firstname,admin_users.lastname,applicants.applicant_first_name,applicants.applicant_middle_name,applicants.applicant_last_name,applicants.applicant_sur_name');
		}
		
		$this->db->from('sms_history');
		
		if($module_id	==	'1')
		{
			$this->db->join('main_applicant',"main_applicant.tempid=sms_history.sms_receiver_id");
		}
		else
		{
			$this->db->join('applicants',"applicants.applicant_id=sms_history.sms_receiver_id");
		}
		
		$this->db->join('admin_users',"admin_users.id=sms_history.sms_sender_id");
		$this->db->where('sms_module_id',$module_id);
		$this->db->where('sms_receiver_id',$userid);
		$query = $this->db->get();
		
		//echo $this->db->last_query();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	/*
	* get info of applicant comitte_decision
	* @param int $applicant_id
	* Created by M.Ahmed
	*/
	function getInquiresOfRequestChangePhaseFive($applicant_id)
	{
		// select info
		$this->db->where('applicant_id',$applicant_id);
		$query = $this->db->get('comitte_decision');
		
		//Check if Result is Greater Than Zero
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	
	
	public function new_inquery()
	{	
		$userid = $this->session->userdata('userid');
		$getQuery = $this->db->query("SELECT tempid FROM main WHERE tempid_value=0 AND userid=".$userid." LIMIT 1;");
		if($getQuery->num_rows() > 0)
		{
			foreach($getQuery->result() as $trex)
			{
				return $this->getLastDetail($trex->tempid);
			}
		}
		else
		{
			$this->db->trans_start();
			$this->db->insert('main',array('userid'=>$userid,'branchid'=>get_branch_id()));			
			$tempid = $this->db->insert_id();
			for($j=0; $j<=3; $j++)
			{
				$this->db->insert('main_applicant',array('tempid'=>$tempid,'applicanttype'=>'ذكر'));
				$applicantid = $this->db->insert_id();
				$this->db->insert('main_phone',array('tempid'=>$tempid,'applicantid'=>$applicantid));
			}
			$this->db->trans_complete();
			return $this->getLastDetail($tempid);
		}	
	}
	

	function sms_delete($sms_id)
	{
		$this->db->where("sms_id",$sms_id);
		$this->db->delete('sms_history');
		
		return true; 
	}

	
	function checkData($table,$returnKey,$whereKey,$value){
		$this->db->select('*');
		$this->db->from($table);		
		$this->db->where($whereKey,$value);
		$checkOut = $this->db->get(); 
		$row = $checkOut->num_rows();
		$temp_main_data	=	(array)$checkOut->row();
		if($row>0){
		
			return 	$temp_main_data[''.$returnKey.''];	;
		}
		else{
			false;
		}
        
	}
	function addUpdate($table,$data){
		
		//echo $C->db->on_duplicate('finance_project',$insertData);
		echo $table;
		echo "<pre>";
		print_r($data);
		//return $this->db->on_duplicate($table,$data);	
	}
	function get_all_users()
	{
		$query = $this->db->query(
		"SELECT
		admin_users.id
		,admin_users.user_name
		,admin_users.firstname
		,admin_users.lastname
		,admin_users.email
		,admin_users.number
		,branches.branch_name,
		COUNT(ap.`applicantid`) AS total
		FROM admin_users
		INNER JOIN branches
		ON (admin_users.branch_id = branches.branch_id)
		LEFT JOIN `applicant_process_log` AS ap 
		ON ap.`userid` = admin_users.`id` AND ap.`stepsid` = '1'
		Where admin_users.`branch_id` !=''
		GROUP BY admin_users.`id`
		ORDER BY total DESC");
		if($query->num_rows() > 0)
		{
		return $query->result();
		}
	}
	
	function get_all_banks()
	{
		$query = $this->db->query(
		"SELECT 
		  admin_users.id,
		  admin_users.user_name,
		  admin_users.firstname,
		  admin_users.lastname,
		  admin_users.email,
		  admin_users.number,
		  bb.branch_name   
		FROM
		  `admin_users` 
		  INNER JOIN `bank_branches` AS bb 
			ON bb.`branch_id` = admin_users.`bank_branch_id` ");
		if($query->num_rows() > 0)
		{
		return $query->result();
		}
	}
	
	public function add_data_into_main($tempid	=	'')
	{

		
		$userid = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->from('temp_main');		
		$this->db->where('tempid',$tempid);
		$checkOut = $this->db->get(); 
		$checkOut->num_rows();
		
		
		$this->db->select('*');
		$this->db->from('main');		
		$this->db->where('tempid',$tempid);
		$checkOut2 = $this->db->get(); 
		$checkOut2->num_rows();
		
		if($tempid)
		$temp_main_data	=	(array)$checkOut->row();
		//echo $tempid;
		//exit();
		
		$this->db->insert('main',$temp_main_data);			
		$tempid = $this->db->insert_id();
		
		
		$applicantQuery = $this->db->query("SELECT * FROM temp_main_applicant WHERE tempid='".$tempid."'");
		
		
		foreach($applicantQuery->result() as $applicant)
		{
			if($applicant->first_name !="" && $applicant->idcard !=""){
			$temp_main_applicant = array('tempid'=>$applicant->tempid,
			'first_name'=>$applicant->first_name,
			'middle_name'=>$applicant->middle_name,
			'last_name'=>$applicant->last_name,
			'family_name'=>$applicant->family_name,
			'applicanttype'=>$applicant->applicanttype,
			'idcard'=>$applicant->idcard);
			$this->db->insert('main_applicant',$temp_main_applicant);
			$applicantid = $this->db->insert_id();
			}
						$phoneQuery = $this->db->query("SELECT * FROM temp_main_phone WHERE tempid='".$tempid."' AND applicantid='".$applicant->applicantid."'");
						foreach($phoneQuery->result() as $phoneres)
						{
							$temp_main_phone = array('tempid'=>$tempid,'applicantid'=>$applicantid,'phonenumber'=>$phoneres->phonenumber);
							$this->db->insert('main_phone',$temp_main_phone);
						}
						/*$this->db->select('*');
						$this->db->from('temp_main_phone');		
						$this->db->where('tempid',$tempid);
						$temp_main_phone = $this->db->get();
						$temp_main_phone	=	(array)$temp_main_phone->row();
						$temp_main_phone['applicantid']	=	$id;*/
						
						
		}
		
/*		$this->db->select('*');
		$this->db->from('temp_main_applicant');		
		$this->db->where('tempid',$tempid);
		$temp_main_applicant = $this->db->get();
		$temp_main_applicant	=	(array)$temp_main_applicant->row();

		$this->db->insert('main_applicant',$temp_main_applicant);			
		$id = $this->db->insert_id();*/
		
		$this->db->select('*');
		$this->db->from('temp_main_inquirytype');		
		$this->db->where('tempid',$tempid);
		$temp_main_inquirytype = $this->db->get();
		$temp_main_inquirytype	=	(array)$temp_main_inquirytype->row();
		
		$this->db->insert('main_inquirytype',$temp_main_inquirytype);			
		
		//$tempid = $this->db->insert_id();
		$this->db->delete('temp_main_notes', array('tempid' =>$tempid,'userid'=>'0'));
		///
		$this->db->select('*');
		$this->db->from('temp_main_notes');		
		$this->db->where('tempid',$tempid);
		$temp_main_notes = $this->db->get();
		$temp_main_notes	=	(array)$temp_main_notes->row();
		
		$this->db->insert('main_notes',$temp_main_notes);			
		//$tempid = $this->db->insert_id();
		
		
		$this->session->unset_userdata('inq_id');
		$this->db->delete('temp_main', array('userid' => $userid));
			$mobileNumbers = get_mobilenumbers($tempid);
			
			$this->db->where('tempid',$tempid);
			$this->db->update('main',array('tempid_value'=>applicant_number($tempid)));
			$userNumber = applicant_number($tempid);
			send_sms(1,$mobileNumbers,$userid,$userNumber);
	}
	
	public function reset_inquery()
	{
		$this->session->unset_userdata('inq_id');
		$userid = $this->session->userdata('userid');
		$this->db->trans_start();
		$this->db->query("DELETE FROM main WHERE idcard='' AND userid='".$userid."'");
		$this->db->insert('main',array('userid'=>$userid,'branchid'=>get_branch_id()));			
		$tempid = $this->db->insert_id();
		$this->session->set_userdata('inq_id',$tempid);	
		for($a==0; $a<4; $a++)
		{	$this->db->insert('main_applicant',array('tempid'=>$tempid,'applicanttype'=>'ذكر'));
			$applicantid = $this->db->insert_id();
			$this->db->insert('main_phone',array('tempid'=>$tempid,'applicantid'=>$applicantid));
		}
		$this->db->trans_complete();
		redirect(base_url().'inquiries/newinquery');			
	}
	
	
	
	
	function getInquiries(){
		$this->db->select('*');
		$this->db->from('register_auditors');
		//$this->db->join('register_auditors',"auditor_id=sms_receiver_id");
		$this->db->order_by("auditor_id", "DESC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result();
		} 
	}
	
	public function getNotes($applicant_id)
	{
		$this->db->select('apn.notesdate, apn.notestype, apn.notes, ah_userprofile.fullname, ah_branchs.branchname');
		$this->db->from('ah_applicant_notes AS apn');
		$this->db->join('ah_userprofile',"apn.userid=ah_userprofile.userid");
		$this->db->join('ah_branchs',"ah_branchs.branchid=apn.branchid");
		$this->db->where("apn.applicantid",$applicant_id);
		$this->db->order_by("apn.notesdate","desc");
		$notes = $this->db->get();
		return $notes->result();
	}

    function muragain_branch_count()
    {
        $mbc = $this->db->query("SELECT branches.`branch_name`,COUNT(admin_users.`id`) AS cnt,branches.`branch_id` FROM `branches`
          INNER JOIN `admin_users` ON (`branches`.`branch_id` = `admin_users`.`branch_id`)
          INNER JOIN `main`  ON (`admin_users`.`id` = `main`.`userid`)
		  INNER JOIN `main_applicant` ON (`main`.`tempid` = `main_applicant`.`tempid`) WHERE `main_applicant`.`idcard`!='' AND `main_applicant`.`idcard`!='0'		  
		   GROUP BY branches.`branch_id` ORDER BY cnt DESC LIMIT 0,10;");
        $xz = 0;
        $mindex = 0;
        $arr[0]['b_name'] = 'عرض الكل ';
        $arr[0]['b_id'] = '0';
        $arr[0]['class'] = 'active';
        foreach($mbc->result() as $qdata)
        {
            $mindex++;
            $arr[$mindex] = array('b_name'=>$qdata->branch_name,'b_count'=>$qdata->cnt,'b_id'=>$qdata->branch_id,'class'=>'');
            $xz += $qdata->cnt;
        }
        $arr[0]['b_count'] = $xz;
        return $arr;
    }
	
	
	

	function getmaindata($branchid='')
		{
			$query = "SELECT `main`.`tempid`,`main`.`applicantdate` FROM `main` 
			INNER JOIN `admin_users` ON (`main`.`userid` = `admin_users`.`id`)
			INNER JOIN `main_applicant` ON (`main`.`tempid` = `main_applicant`.`tempid`) WHERE `main_applicant`.`idcard`!='' AND `main_applicant`.`idcard`!='0' AND ";
			
			if($branchid!='' && $branchid!=0)
			{
				$query .= " `admin_users`.`branch_id`='".$branchid."' AND ";
			}
				$query .= " `admin_users`.`branch_id` > '0' GROUP BY `main`.`tempid` ORDER BY `main`.`applicantdate` DESC ";	
				$qx = $this->db->query($query);
				if ($qx->num_rows() > 0)
				{
					return $qx->result();
				} 
	}
	
	public function getLastInquery($tempid='')
	{
		$userid = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->from('main');		
		$this->db->where('userid',$userid);
		$this->db->order_by("applicantdate", "DESC"); 
		$this->db->limit(1);
		$tempMain = $this->db->get();
		if($tempMain->num_rows() > 0)
		{
			foreach($tempMain->result() as $data)
			{				
				$arr['main'] = $data;
				$tempid = $data->tempid;							
				$tempMain->free_result(); //Clean Result Set
				//Getting Applicant Data
				$this->db->select('*');
				$this->db->from('main_applicant');		
				$this->db->where('tempid',$tempid);
				$this->db->order_by("applicantid", "ASC");
				$tempApplicant = $this->db->get();
				if($tempApplicant->num_rows() > 0)
				{
					foreach($tempApplicant->result() as $app)
					{
						$arr['main']->applicant[] = $app;
						//Getting Phone Data
						$this->db->select('*');
						$this->db->from('main_phone');		
						$this->db->where('tempid',$tempid);
						$this->db->where('applicantid',$app->applicantid);
						$this->db->order_by("phoneid", "ASC");
						$tempPhone = $this->db->get();
						if($tempPhone->num_rows() > 0)
						{
							foreach($tempPhone->result() as $phones)
							{
								$arr['main']->phones[$app->applicantid][] = $phones;						
							}
							$tempPhone->free_result(); //Clean Result Set
						}					
					}
					$tempApplicant->free_result(); //Clean Result Set
				}				
				//Getting Inquiry Type Data
				$this->db->select('*');
				$this->db->from('main_inquirytype');		
				$this->db->where('tempid',$data->tempid);
				$this->db->order_by("inqid", "ASC");
				$tempIType = $this->db->get();
				if($tempIType->num_rows() > 0)
				{
					foreach($tempIType->result() as $itype)
					{
						$arr['main']->inquery[] = $itype;
					}
					$tempIType->free_result(); //Clean Result Set
				}				
				//Getting Notes Data
				$this->db->select('*');
				$this->db->from('main_notes');		
				$this->db->where('tempid',$data->tempid);
				$this->db->order_by("notesid", "ASC");
				$tempNotes = $this->db->get();
				if($tempNotes->num_rows() > 0)
				{
					foreach($tempNotes->result() as $notes)
					{
						$arr['main']->notes[] = $notes;
						//$arr['notes'][] = $notes;
					}
					$tempNotes->free_result(); //Clean Result Set
				}				
								
			}
		}
		return $arr;
	}
	

	
	public function check_record($table,$id){
		$this->db->select('*');
		$this->db->from($table);			
		$this->db->where('applicant_id',$id);
		$tempMain = $this->db->get();	
		if($tempMain->num_rows() > 0)
		{
			return $tempMain->result();
		}
		else{
			return false;
		}	
	}
	
	
	function getSeniorPeople($applicantid){
		$sql = $this->db->query("SELECT peopleid,notes,notesdate FROM `applicant_touqeeyat` WHERE applicantid = '".$applicantid."'");
		foreach($sql->result() as $gsp)
		{
			$arr[$gsp->peopleid]['peopleid'] = $gsp->peopleid;
			$arr[$gsp->peopleid]['notes'] = $gsp->notes;
			$arr[$gsp->peopleid]['notesdate'] = $gsp->notesdate;
		}
		return $arr;
		
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	
	
	function get_all_applicatnts($branchid='')
	{
				
		$sql = "SELECT a.*, cd.`commitee_decision_type`, br.`is_reject`,br.`loan_id` FROM `applicants` AS a 
		LEFT JOIN `comitte_decision` AS cd ON cd.`applicant_id` = a.`applicant_id` 
		LEFT JOIN `bank_response` AS br ON br.`applicant_id` = a.`applicant_id` 
		LEFT JOIN `admin_users` AS au ON au.`id` = a.`addedby`  ";
		if($branchid!='')
		{	$sql .= " WHERE au.`branch_id`='".$branchid."' ";	};
			$sql .= " ORDER BY a.applicant_id DESC ";
							
		$query = $this->db->query($sql);	
						
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	
	function get_all_reject_bank()
	{
		$sql = "SELECT *
				FROM
			  `applicants` AS a
			  INNER JOIN `bank_response` ON `bank_response`.`applicant_id` = a.`applicant_id`
			  WHERE `bank_response`.`is_reject` = '1'";
							
		$query = $this->db->query($sql);					
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		//Login Query	
		/*$query = $this->db->get('applicants');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}*/
	}
	
	function get_mukhair()
	{
				/*$query = "SELECT 
		  a.* 
		FROM
		  applicants AS a 
		  LEFT JOIN `study_analysis_demand` AS sa 
			ON sa.`applicant_id` = a.`applicant_id` 
		WHERE sa.`credit_risk` = 'نعم' 
		  AND sa.`is_musanif` = 'classified' ";*/
		  $query = "SELECT applicants.* FROM `applicant_loans` 
		  INNER JOIN `applicants` ON (`applicant_loans`.`applicant_id` = `applicants`.`applicant_id`) 
		  LEFT JOIN `study_analysis_demand` ON (`study_analysis_demand`.`applicant_id` = `applicants`.`applicant_id`) 
		  WHERE 
		  (study_analysis_demand.`credit_risk` = 'نعم' AND 
		  study_analysis_demand.`is_musanif` = 'classified') OR
		  `study_analysis_demand`.`credit_risk` IS NULL
		  
		  ORDER BY `applicants`.applicant_regitered_date DESC;";
		  $result = $this->db->query($query);
		return $r =  $result->result();

	}
	function getStudy_analyze($id){
		$sql = "SELECT * FROM `study_analysis_demand` AS sa WHERE sa.`applicant_id` = '".$id."'";
			$result = $this->db->query($sql);
			return $r =  $result->row();
	}
	
	
	function getAprovalStepData(){
				/*$this->db->select('*');
				$this->db->from('applicants AS a');
				$this->db->join('comitte_decision AS cd',"a.applicant_id=cd.applicant_id");		
				$this->db->where('form_step',5);
				$this->db->where('committee_decision_is_aproved','approval');
				$this->db->order_by("a.applicant_id", "DESC");
				$tempNotes = $this->db->get();
				if($tempNotes->num_rows() > 0)
				{
					return $tempNotes->result();
				}*/
						$sql= "SELECT 
		  * 
		FROM
		  `applicants` AS a 
		  INNER JOIN `check_list` AS cl 
			ON cl.`applicant_id` = a.`applicant_id` 
		WHERE cl.`sealed_company` = '1' 
		  AND cl.`registration_zip` = '1' 
		  AND cl.`municipal_contractrent` = '1'  
		  AND cl.`open_account` = '1' 
		  AND cl.`membership_certificate` = '1'
		  AND cl.`company_general_authority` = '1'
		  AND cl.`commercial_papers` = '1'
		  AND cl.`check_book` = '1'";
		  $q = $this->db->query($sql);
		return $r =  $q->result();

	}
	
	function getSmsHistory($id){
			
				$this->db->select('sh.*');
				$this->db->from('sms_history as sh');
				$this->db->join('admin_users AS au',"au.id=sh.id");
				$this->db->where('sh.sms_receiver_id',$id);
				$this->db->order_by("sh.sms_id", "DESC");
				$tempNotes = $this->db->get();
				if($tempNotes->num_rows() > 0)
				{
					return $tempNotes->result();
				}

	}
	

//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_single_applicatnt($id)
	{
		//Login Query
		$this->db->where("applicant_id",$id);
		$query = $this->db->get('applicants');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	
	///
	function get_applicant_evolution($id)
	{
		//Login Query
		$this->db->where("applicant_id",$id);
		$query = $this->db->get('project_evolution');		
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	
	///getEvolutionAmount
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_single_auditor($id)
	{
		//Login Query
		$this->db->where("auditor_id",$id);
		$query = $this->db->get('register_auditors');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_main_info($id)
	{
		//Login Query
		$this->db->where("tempid",$id);
		$query = $this->db->get('main');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
		//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_phone_number($id)
	{
		//Login Query
		$this->db->where("tempid",$id);
		$query = $this->db->get('main_phone');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

			//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_notes($id)
	{
		//Login Query
		$query = $this->db->query("SELECT
						CONCAT(`admin_users`.`firstname`,' ',`admin_users`.`lastname`) AS adminname
						, `admin_users`.`user_name`
						, `main_notes`.`tempid`
						, `main_notes`.`userid`
						, `main_notes`.`notesdate`
						, `main_notes`.`notestext`
						, `main_notes`.`inquiry_text`
						, `main_notes`.`notesip`
						, `main_notes`.`inquerytype`
						, `main_notes`.`notesid`
					FROM
						`admin_users`
						INNER JOIN `main_notes` ON (`admin_users`.`id` = `main_notes`.`userid`)
						WHERE `main_notes`.`tempid`='".$id."' ORDER BY `main_notes`.`notesdate` DESC;");
		
		
		
		//$this->db->where("tempid",$id);
		//$query = $this->db->get('main_notes');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_last_note($id)
	{
		//Login Query
		$this->db->where("tempid",$id);
		$this->db->order_by("notesdate","DESC");
		$this->db->limit("1");
		$query = $this->db->get('main_notes');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_type_name($typeid)
	{
		//Login Query
		$this->db->where("list_id",$typeid);

		$query = $this->db->get('list_management');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->list_name;
		}
	}
	//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_gender($typeid)
	{
		//Login Query
		$this->db->where("tempid",$typeid);

		$query = $this->db->get('main_applicant');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->applicanttype;
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_user_name_of_added($id)
	{
		//Login Query
		$this->db->select("firstname,lastname");
		
		$this->db->where("id",$id);

		$query = $this->db->get('admin_users');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
		//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_user_name($id)
	{
		//Login Query
		$this->db->where("tempid",$id);

		$query = $this->db->get('main_applicant');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_province_name($provinceid)
	{
		$this->db->where('ID',$provinceid);

		$query = $this->db->get('election_reigons');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->REIGONNAME;
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_wilayats_name($wilayatsid)
	{
		$this->db->where('WILAYATID',$wilayatsid);

		$query = $this->db->get('election_wilayats');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->WILAYATNAME;
		}
	}
//----------------------------------------------------------------------

	function get_tab_data($table_name,$app_id)
	{
		$this->db->where("applicant_id",$app_id); 
		$query = $this->db->get($table_name);
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}	
//----------------------------------------------------------------------

	function get_branch_name($branchid)
	{
		$this->db->where("branch_id",$branchid); 
		$query = $this->db->get('branches');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->branch_name;
		}
	}
//----------------------------------------------------------------------

	function get_user_role($role_id)
	{
		$this->db->select('role_name');
		$this->db->where("role_id",$role_id); 
		$this->db->where('role_parent_id','0');
		
		$query = $this->db->get('user_roles');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->role_name;
		}
	}
//----------------------------------------------------------------------

	function get_user_child_role($parent_role)
	{
		//$this->db->select('role_name');
		$this->db->where("role_id",$parent_role); 
		
		$query = $this->db->get('user_roles');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->role_name;
		}
	}
//----------------------------------------------------------------------

	function marajeen_phones()
	{
		$query = $this->db->get('main_phone');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	function tasjeel_phones()
	{
		$query = $this->db->get('applicant_phones');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
//----------------------------------------------------------------------
	/*
	* Insert Banner Record
	* @param array $data
	* return True
	*/
	function add_banner($data)
	{
		//print_r($data);
		$this->db->insert('system_images',$data);
		
		
		
		return TRUE;
	}
	
	
//----------------------------------------------------------
	function get_all_banners(){
		
		$query = $this->db->get('system_images');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}	
	}
	function delete_banner($banner_id)
	{	
		$this->db->where("imageid",$banner_id);
		$this->db->delete('system_images');
		
		return true; 	
	}

	function delete_banner_ajax($image)
	{
		$this->db->where('banner_image',$image);
		$query 	= 	$this->db->get('system_images');
		$img_id	=	$query->row()->imageid;

		if($query->num_rows() > 0)
		{
			$this->db->where("imageid",$img_id);
			$this->db->delete('system_images');

		}
		
		if(file_exists(rootpath.'upload_files/banners/'.$image))
		{
			unlink(rootpath.'upload_files/banners/'.$image);	
		}
		
		return true; 	
	}
	
	function delete_image($image,$type ='')
	{
		
		if($type !=""){
				if($type == 'commetti'){
					

					$this->db->where('image_name',$image);
				$query = $this->db->get('comitte_decision_image');
				$guarantee_img_id	=	 $query->row()->id;
				
				if($query->num_rows() > 0){
				$this->db->where("id",$guarantee_img_id);
				$this->db->delete('comitte_decision_image');
				
				}
					
				}
		}
		else{
				$this->db->where('gurarntee_image',$image);
				$query = $this->db->get('guanttee_attachment');
				$guarantee_img_id	=	 $query->row()->guarantee_img_id;
				if($query->num_rows() > 0){
				$this->db->where("guarantee_img_id",$guarantee_img_id);
				$this->db->delete('guanttee_attachment');
				
				}
		
		}
		//					exit;
		//echo"<pre>";	
		//print_r($query->row());
		//exit;
		
		
		if(file_exists(rootpath.'upload_files/documents/'.$image)){
			unlink(rootpath.'upload_files/documents/'.$image);	
		}
		
		
		return true; 	
	}
//--------------------------------------------------------------------------	
	/*
	*
	* Delete Document File From Database asa well from Folder
	*
	*/
	function delete_document($image)
	{
		$this->db->where('documentname',$image);
		$query = $this->db->get('applicant_document');
		$documentid	=	 $query->row()->documentid;

		if($query->num_rows() > 0)
		{
			$this->db->where("documentid",$documentid);
			$this->db->delete('applicant_document');

		}
		
		if(file_exists(rootpath.'upload_files/documents/'.$image))
		{
			unlink(rootpath.'upload_files/documents/'.$image);	
		}
		
		return true; 	
	}
//--------------------------------------------------------------------------	
	public function update_status($image_id,$data)
	{
		$this->db->where('imageid',$image_id);
		$this->db->update('system_images', $data);
		
		return TRUE;
	}
//-------------------------------------------------------------
	public function get_banner_detail($image_id)
	{
		$this->db->where('imageid',$image_id);
		
		$query = $this->db->get('system_images');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}	
	}
//-------------------------------------------------------------
	public function update_banner($image_id,$data)
	{
		$this->db->where('imageid',$image_id);
		$this->db->update('system_images', $data);
		
		return TRUE;
	}	
//-------------------------------------------------------------
	
}

?>
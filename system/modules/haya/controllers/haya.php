<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Haya extends CI_Controller {

	/*
	 * Properties
	 */
	private $_data	=	array();

//-----------------------------------------------------------------------

	/*
	 * Constructor
	 */
	function __construct()
	{
		 parent::__construct();	
	
		// Loade Admin Model
		$this->load->model('haya_model','haya');
		
	}
	
//-----------------------------------------------------------------------

	/*
	*	Home Page
	*/
	public function get_sub_category()
	{
		$provinceid	=	$this->input->post('provinceid');

		// Get Willaya from province ID
		 echo ($this->haya->get_sub_category($provinceid));
	}
	
//------------------------------------------------------------------------

  /**
   * Change language
   * @param $name string
   */
	 public function changeLanguage($name) 
	 {
		if ($name == 'english')
		{
				$this->session->set_userdata('lang', 'english');
				$this->session->set_userdata('lang_code', 'en');
		}
		else
		{
				$this->session->set_userdata('lang', 'arabic');
				$this->session->set_userdata('lang_code', 'ar');
		}
		
		echo '1';
	 }
//-----------------------------------------------------------------------
/* End of file haya.php */
/* Location: ./application/modules/haya/haya.php */
}

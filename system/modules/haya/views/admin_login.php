<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">

<!-- Viewport Metatag -->
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>admin_template/bootstrap/css/bootstrap.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>admin_template/css/fonts/ptsans/stylesheet.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>admin_template/css/fonts/icomoon/style.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>admin_template/css/login.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>admin_template/css/mws-theme.css" media="screen">
 <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
 <script>
  var siteurl = "<?php echo base_url(); ?>";
  </script>

<script src="<?php echo base_url(); ?>admin_template/js/custom.js"></script>


<title>MWS Admin - Login Page</title>

</head>

<body>
<?php $user_name	=	$this->session->userdata('admin_user_name');?>
<?php 
if($user_name)
{
	redirect(base_url().'admin/dashboard');
	exit();
}
?>
    <div id="mws-login-wrapper">
        <div id="mws-login">
            <h1>Login</h1>
            <div class="mws-login-lock"><i class="icon-lock"></i></div>
            <div id="mws-login-form">
                <form class="mws-form" method="post" id="login_form">
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="text" name="username" class="mws-login-username required" placeholder="username" id="login_text">
                        </div>
                    </div>
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="password" name="password" class="mws-login-password required" placeholder="password" id="pass_text">
                        </div>
                    </div>
                  	 <div id="resp"></div>
                    <div class="mws-form-row">
                        <input type="submit" value="Login" class="btn btn-success mws-login-button">
                    </div>
                    
                </form>
            </div>
        </div>
    </div>

    <!-- JavaScript Plugins -->
    <script src="<?php echo base_url(); ?>admin_template/js/libs/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url(); ?>admin_template/js/libs/jquery.placeholder.min.js"></script>
    <script src="<?php echo base_url(); ?>admin_template/custom-plugins/fileinput.js"></script>
    
    <!-- jQuery-UI Dependent Scripts -->
    <script src="<?php echo base_url(); ?>admin_template/jui/js/jquery-ui-effects.min.js"></script>

    <!-- Plugin Scripts -->
    <script src="<?php echo base_url(); ?>admin_template/plugins/validate/jquery.validate-min.js"></script>

    <!-- Login Script -->
    <script src="<?php echo base_url(); ?>admin_template/js/core/login.js"></script>
	
</body>
</html>

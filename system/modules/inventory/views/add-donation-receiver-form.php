<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block" style="padding: 10px 10px;">
          <form method="POST" id="add-donation-receiver-form" name="add-donation-form" enctype="multipart/form-data">
            <div class="form-group col-md-4">
              <label class="text-warning">اسم المستلم:</label>
              <input type="text" class="form-control req" value="" placeholder="اسم المانحة" name="donorid" id="donorid" />
            </div>
            <div class="form-group col-md-4">
              <label class="text-warning">الفئة:</label>
              <?PHP echo $this->haya_model->create_dropbox_list('list_category','category',$item->list_category,0,'req'); ?> </div>
            <div class="form-group col-md-4">
              <label class="text-warning">الفئة الفرعية:</label>
              <?PHP echo $this->haya_model->create_dropbox_list('list_subcategory','subcategory',$item->list_subcategory,$item->list_category,'req'); ?> </div>
            <div class="form-group col-md-4">
              <label class="text-warning">العناصر</label>
              	<select id="itemid" name="itemid" class="form-control">
                <option value="">واختيار عناصر</option>
              </select>
            </div>
            <div class="form-group col-md-4">
              <label class="text-warning">السعر:</label>
              <input type="text" class="form-control req" value="" placeholder="السعر" name="price" id="price" />
            </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">كمية</label>
              <input type="text" class="form-control required" name="quantity"  id="quantity"  placeholder="كمية" value=""/>
            </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">تاريخ</label>
              <input type="text" class="datepicker form-control required" name="submited_date"  id="submited_date"  placeholder="تاريخ" value=""/>
            </div>
            <div class="form-group col-md-4">
              <label class="text-warning">إيصال:</label>
              <input type="file" accept="image/*" id="donor_receipt" name="donor_receipt">
              <?PHP if($item->itemphoto!='') { ?>
              <a class="fancybox-button" rel="gallery1" href="<?PHP echo base_url(); ?>resources/items/<?PHP echo $item->itemphoto; ?>"><i class="icon-eye-open"></i></a>
              <?PHP } ?>
            </div>
            <br clear="all">
            <div class="form-group col-md-6">
              <label class="text-warning">وصف:</label>
              <textarea name="itemdescription" placeholder="وصف" class="form-control req" style="resize:none; height:300px;" id="itemdescription"><?PHP echo $item->itemdescription; ?></textarea>
            </div>
            <div class="form-group col-md-6">
              <label class="text-warning">ملاحظات:</label>
              <textarea name="itemdescription" placeholder="ملاحظات" class="form-control req" style="resize:none; height:300px;" id="itemdescription"><?PHP echo $item->itemdescription; ?></textarea>
            </div>
            <div class="form-group col-md-6">
              <button type="button" id="save_donation" name="save_donation" class="btn btn-success">حفظ</button>
            </div>
          </form>
          <br clear="all">
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		/*yearRange: "-80:+0",*/
		yearRange: new Date().getFullYear() + ':' + new Date().getFullYear(),
		dateFormat:'yy-mm-dd',
		});
		
});

function select_item() 
{
	var cat_id		=	$("#list_category").val();
	var sub_cat_id	=	$("#list_subcategory").val();
	
	$.ajax({
			url: '<?php echo base_url();?>inventory/get_items',
			type: "POST",
			data:'cat_id='+cat_id+'&sub_cat_id='+sub_cat_id,
			dataType: "html",
			success: function(msg)
			{
				$("#itemid").html(msg);
			}
		});
}
</script>
</div>
</body>
</html>
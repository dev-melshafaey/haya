<div id="user-profile">
  <div class="row" dir="rtl">
    <div class="col-md-12 fox leftborder">
      <h4 class="panel-title customhr">Donation Detail Page</h4>
      <div class="col-md-6 form-group">
        <label class="text-warning">اسم المانحة : </label>
        <strong><?php echo $donation_detail->user_name; ?> </strong> </div>
      <div class="col-md-6 form-group">
        <label class="text-warning">رقم البطاقة : </label>
        <strong><?php echo $donation_detail->card_number; ?></strong> </div>
      <br clear="all" />
      <?php $donation_items	=	$this->inventory->get_all_items_by_item($donation_detail->donation_id);?>
      <?php if(!empty($donation_items)):?>
      <div class="col-md-12 form-group">
        <table class="table table-bordered table-striped dataTable" id="item-atable" >
          <thead>
            <tr role="row">
              <th style="text-align:center;">الفئة</th>
              <th style="text-align:center;">الفئة الفرعية</th>
              <th style="text-align:center;">العناصر</th>
              <th style="text-align:center;">السعر</th>
              <th style="text-align:center;">كمية</th>
            </tr>
          </thead>
          <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php foreach($donation_items as $item): ?>
            <?php $total_price	+=	$item->price;?>
            <?php $total_items	+=	$item->total_items;?>
            <tr role="row">
              <td style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($item->list_category);?></td>
              <td style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($item->list_subcategory);?></td>
              <td style="text-align:center;"><?php echo $this->inventory->get_item_name($item->itemid);?></td>
              <td style="text-align:center;"><?php echo $item->price;?></td>
              <td style="text-align:center;"><?php echo $item->total_items;?></td>
            </tr>
            <?php endforeach; ?>
            <tr>
              <td colspan="3" style="text-align:center;"><strong>الإجمالي الكلي</strong></td>
              <td  style="text-align:center;"><strong><?php echo $total_price;?></strong></td>
              <td style="text-align:center;"><strong><?php echo $total_items;?></strong></td>
            </tr>
          </tbody>
        </table>
      </div>
      <?php endif;?>
    </div>
  </div>
</div>
</div>
</div>
<div class="row" style="text-align: center;">
  <button type="button" id="" onClick="printthepage('user-profile');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
</div>

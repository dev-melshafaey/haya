<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventory_model extends CI_Model {
	
	/*
	* Properties
	*/
	private $_table_users;	
	private $_table_donate;	
	private $_table_applied_donation;
	private $_table_sms;
//----------------------------------------------------------------------
    
	/*
	* Constructor
	*/
	
	function __construct()
    {
        parent::__construct();
		
		$this->load->helper("file");
		
		//Load Table Names from Config
		$this->_table_users 			=  $this->config->item('table_users');
		$this->_table_donate 			=  $this->config->item('table_donate');
        $this->_table_user_profile 		=  $this->config->item('table_user_profile');
		$this->_table_applied_donation 	=  $this->config->item('table_applied_donation');
		$this->_table_sms 				=  $this->config->item('sms_management');
		$this->_login_userid			=	$this->session->userdata('userid');
    }
	
	function saveitem()
	{
		$itemid = $this->input->post('itemid');
		$addedby = $this->_login_userid;
		$list_category = $this->input->post('list_category');
		$list_subcategory = $this->input->post('list_subcategory');
		$itemname = $this->input->post('itemname');
		$itemdescription = $this->input->post('itemdescription');
		$itemphoto = $this->haya_model->upload_file('itemphoto',realpath('resources/items'));
		$itemqty = $this->input->post('itemqty');
		$itemstatus = $this->input->post('itemstatus');
		$oldfile = $this->input->post('oldfile');
		if($oldfile!='' && $itemname=='')
		{	$itemphoto = $oldfile;	}
				
		$ah_inventory = array('addedby'=>$addedby,'list_category'=>$list_category,'list_subcategory'=>$list_subcategory,'itemname'=>$itemname,'itemdescription'=>$itemdescription,'itemphoto'=>$itemphoto,'itemstatus'=>1);
		if($itemid!='')
		{
			$this->db->where('itemid',$itemid);
			$this->db->update('ah_inventory',json_encode($ah_inventory),$this->_login_userid,$ah_inventory);
		}
		else
		{
			$this->db->insert('ah_inventory',$ah_inventory,json_encode($ah_inventory),$this->_login_userid);
		}
	}
	
	function getitem($itemid)
	{
		$this->db->where('itemid',$itemid);
        $query = $this->db->get('ah_inventory');
		return $query->row();
	}
	
//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function add_donation($data)
	 {
		 $this->db->insert('ah_donations',$data,json_encode($data),$this->_login_userid);
		 
		 return TRUE;
	 }
//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function update_donation($donation_id,$data)
	 {
	 	$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('donation_id', $donation_id);

		$this->db->update('ah_donations',$json_data,$this->_login_userid,$data);

		return TRUE;
	 }	 
	 

	 
//----------------------------------------------------------------------

	/*
	*
	* Get all Donors
	*/
	 public function get_all_donors($user_type)
	 {
		 $this->db->where('user_type',$user_type);
		$this->db->where('delete_record','0');
		$this->db->order_by('donation_id','DESC');
		
        $query = $this->db->get('ah_donations');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	 }
//----------------------------------------------------------------------

	/*
	*
	* Get Donation User Detail By donation_id
	* @param $donation_id	integer
	*/
	 public function get_donation_user_info($donation_id)
	 {
		$this->db->where('donation_id',$donation_id);
		 
        $query = $this->db->get('ah_donations');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	 }
//----------------------------------------------------------------------

	/*
	* Delete Donation Information
	*/

	function delete_donation($donation_id)
	{
		$json_data	=	json_encode(array('record'=>'delete','donation_id'	=>	$donation_id));
		
		$data	=	array('delete_record'=>'1');
		
		$this->db->where('donation_id', $donation_id);

		$this->db->update('ah_donations',$json_data,$this->session->userdata('userid'),$data);

		return TRUE;
	}
//----------------------------------------------------------------------

	/*
	* Delete Donation Information
	*/

	function delete_d_item($d_itemid)
	{
		$this->db->where('d_itemid', $d_itemid);
		$this->db->delete('ah_inventory_qty');
		
		$this->db->where('d_itemid', $d_itemid);
		$this->db->delete('ah_donation_items');
		
		return TRUE;
	}
	
//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function add_donation_items($data)
	 {
		$this->db->insert('ah_donation_items',$data,json_encode($data),$this->_login_userid);
		 
		$insert_id = $this->db->insert_id();

   		return  $insert_id;
	 }
//----------------------------------------------------------------------

	/*
	*
	* 
	* 
	*/
	 public function get_donation_items($donation_id)
	 {
		$this->db->where('donation_id',$donation_id);
        $query = $this->db->get('ah_donation_items');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	 }
	//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function add_item_quantity($data)
	 {
		 
		 $this->db->insert('ah_inventory_qty',$data,json_encode($data),$this->_login_userid);

   		return  TRUE;
	 } 
	
//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function get_all_detail_by_id($donation_id)
	 {
		$this->db->select('donation_id,card_number,user_name,delete_record,submitted_date');
		$this->db->where('donation_id',$donation_id);
		
		$this->db->limit(1);
		
		$query = $this->db->get('ah_donations');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	 }
//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function get_all_items_by_item($donation_id)
	 {
		$this->db->select('d_itemid,itemid,donation_id,list_category,list_subcategory,price,total_items,description,notes');
		$this->db->where('donation_id',$donation_id);
		
		$query = $this->db->get('ah_donation_items');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	 }
//----------------------------------------------------------------------

	/*
	*
	* 
	*/	 
	function get_item_name($itemid)
	{
		$this->db->select('itemname');
		$this->db->where('itemid',$itemid);
		
        $query = $this->db->get('ah_inventory');
		
		return $query->row()->itemname;
	}
	 
//----------------------------------------------------------------------	 
}
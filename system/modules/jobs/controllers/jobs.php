<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobs extends CI_Controller 
{
//-------------------------------------------------------------------------------	
	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;
	
//-------------------------------------------------------------------------------

	/*
	* Costructor
	*/

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('jobs_model','job');
		
		// Loade Admin Model		
		$this->_data['module']			=	$this->haya_model->get_module();
			
		// SET Login USER ID
		$this->_login_userid			=	$this->session->userdata('userid');
		$this->_data['login_userid']	=	$this->_login_userid;	
		
		$this->_data['user_detail'] 	= $this->haya_model->get_user_detail($this->_login_userid);	
	}
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
    public function index()
    {
        check_permission($this->_data['module'],'v');
		redirect(base_url().'jobs/all_jobs');
    }
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
	public function all_jobs()
	{
		$this->load->view('job_list',$this->_data);
	}	
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
	public function addjob($jobid)
	{
		if($this->input->post()!='')
		{
			$this->job->savejob();
			redirect(base_url().'jobs/all_jobs');
		}
		
		$this->_data['job'] = $this->job->getjob($jobid);
		$this->load->view('add_job',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
	public function delete_job($jobid)
	{
		$json_data = json_encode(array('isdeleted'=>'1','jobsid'=>$jobid));	
		$data =	array('isdeleted'=>'1');		
		$this->db->where('jobsid',$jobid);
		$this->db->update('ah_jobs',$json_data,$this->session->userdata('userid'),$data);		
		return TRUE;
	}
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
	public function getJobDetails($jobid)
	{
		$this->_data['job'] = $this->job->getjob($jobid);
		$this->load->view('viewjob',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/		
	public function ajax_alljobs()
	{
			$this->db->select('jobsid,jobtitle,expirydate,jobstatus,location,salaryStart,salaryEnd');
			$this->db->from('ah_jobs');
			$this->db->where('isdeleted','0');
			$this->db->order_by('expirydate','DESC');
			$query = $this->db->get();
			
			$permissions	=	$this->haya_model->check_other_permission(array('142'));
						
			foreach($query->result() as $lc)
			{
				$action  =  '	<a  onclick="alatadad(this);" data-url="'.base_url().'jobs/getJobDetails/'.$lc->jobsid.'"  href="#"><i class="icon-eye-open"></i></a>';
				if($permissions[142]['u']	==	1) 
				{	$action .= '<a class="iconspace" href="'.base_url().'jobs/addjob/'.$lc->jobsid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
				
				if($permissions[142]['d']	==	1) 
				{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->jobsid.'" data-url="'.base_url().'jobs/delete_job/'.$lc->jobsid.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
				
				$arr[] = array(
					"DT_RowId"=>$lc->jobsid.'_durar_lm',
					"رقم" =>arabic_date($lc->jobsid),
					"عنوان وظيفة" =>$lc->jobtitle,
					"راتب" =>arabic_date($lc->salaryStart).' - '.arabic_date($lc->salaryEnd),
					"تاريخ انتهاء" =>arabic_date($lc->expirydate),
					"موقع" =>$lc->location,              
					"الإجرائات" =>$action);
					unset($action);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
	}
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
	public function candidatelist()
	{
		 $this->haya_model->check_permission($this->_data['module'],'v');
		 $this->load->view('candidatelist',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*	Approved Candidates List
	*/	
	public function approved_candidatelist()
	{
		 $this->haya_model->check_permission($this->_data['module'],'v');
		 $this->load->view('approved-candidatelist',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*	Reject Candidates List
	*/	
	public function reject_candidatelist()
	{
		 $this->haya_model->check_permission($this->_data['module'],'v');
		 $this->load->view('reject-candidatelist',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
	public function all_candidate_list()
	{
		$text		=	$this->lang->line('users');
		$labels		=	$text['users']['listing'];		
		$all_users	=	$this->job->get_all_candidate();
		
		//print_r($all_users);
		 
	
		if(!empty($all_users))
		{
			foreach($all_users as $users)
			{
				
				if($users->status	==	'' || $users->status	==	'0')
				{
					//print_r($users);
					//echo 'status IN = '.$users->status;
					$actions  = '<a  onclick="alatadad(this);" data-url="'.base_url().'users/getUsersDetails/'.$users->userid.'"  href="#"><i class="icon-eye-open"></i></a>';
					$actions .= '&nbsp;&nbsp;<a href="'.base_url().'jobs/download_file/'.$users->userid.'/'.$users->resume.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
					$actions .= '&nbsp;&nbsp;<a href="#" onClick="alatadad(this);" data-url="'.base_url().'jobs/add_schedule/'.$users->userid.'/'.$users->jobsid.'" id="'.$users->userid.'"><i style="color:#FF7400 !important;" class="icon-calendar"></i></a>';
					$actions .= '&nbsp;&nbsp;<a href="#" onClick="alatadad(this);" data-url="'.base_url().'jobs/add_interview_response/'.$users->userid.'/'.$users->jobsid.'" id="'.$users->userid.'"><i style="color:#FF7400 !important;" class="icon-certificate"></i></a>';
					$arr[] = array(
					"DT_RowId"		=>	$users->userid,
					"الأسم الكامل" 		=>	$users->fullname,
					"خيار التطبيق لى" 	=>	$users->jobtitle,
					"الحالة الإجتماعية"		=>	$users->list_name,
					"لبريد الإلكتروني"		=>	$users->email,
					"الجنسية" 			=>	$users->gender,
					"الإجرائات" 		=>	$actions);
					
					unset($actions);
				}
			}

			$ex['data'] = $arr;
			
			echo json_encode($ex);			
		}
	}
	
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
	public function ajax_reject_candidatelist()
	{
		$text		=	$this->lang->line('users');
		$labels		=	$text['users']['listing'];		
		$all_users	=	$this->job->get_all_reject_candidate();
	
		if(!empty($all_users))
		{
			foreach($all_users as $users)
			{
				
				$actions  = '<a  onclick="alatadad(this);" data-url="'.base_url().'jobs/interniew_remarks_view/'.$users->interview_responseid.'"  href="#"><i class="icon-eye-open"></i></a>';
				$actions .= '&nbsp;&nbsp;<a href="'.base_url().'jobs/download_file/'.$users->userid.'/'.$users->resume.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';

				$status	=	'<img src="'.base_url().'assets/images/not-approved.png" style="width: 24px;" alt="Reject" title="Reject"/>';
				
				$arr[] = array(
				"DT_RowId"			=>	$users->userid,
				"الأسم الكامل" 		=>	$users->fullname,
				"خيار التطبيق لى" 	=>	$users->jobtitle,
				"الحالة الإجتماعية"	=>	$users->list_name,
				"لبريد الإلكتروني"	=>	$users->email,
               	"الجنسية" 			=>	$users->gender,
				"الحالة" 			=>	$status,
                "الإجرائات" 			=>	$actions);
				
				unset($actions);
			}

			$ex['data'] = $arr;
			
			echo json_encode($ex);			
		}
	}
	//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
	public function ajax_approved_candidatelist()
	{
		$text		=	$this->lang->line('users');
		$labels		=	$text['users']['listing'];		
		$all_users	=	$this->job->get_all_approved_candidate();
	
		if(!empty($all_users))
		{
			foreach($all_users as $users)
			{
				$actions  = '<a  onclick="alatadad(this);" data-url="'.base_url().'jobs/interniew_remarks_view/'.$users->interview_responseid.'"  href="#"><i class="icon-eye-open"></i></a>';
				$actions .= '&nbsp;&nbsp;<a href="'.base_url().'jobs/download_file/'.$users->userid.'/'.$users->resume.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';

				$status	=	'<img src="'.base_url().'assets/images/not-approved.png" style="width: 24px;" alt="Reject" title="Reject"/>';
				
				
				$arr[] = array(
				"DT_RowId"			=>	$users->userid,
				"الأسم الكامل" 		=>	$users->fullname,
				"خيار التطبيق لى" 	=>	$users->jobtitle,
				"الحالة الإجتماعية"	=>	$users->list_name,
				"لبريد الإلكتروني"	=>	$users->email,
               	"الجنسية" 			=>	$users->gender,
				"الحالة" 			=>	$status,
                "الإجرائات" 			=>	$actions);
				
				unset($actions);
			}

			$ex['data'] = $arr;
			
			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------

	/*
	*	Interview Remarks View
	*	@param $salaryid integer
	*/
	public function interniew_remarks_view($interview_responseid)
	{
		$this->_data['detail']	=	$this->job->interview_remarks($interview_responseid);
		
		
			
		// Load Remarks Interview
		$this->load->view('interview-response',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/
	public function add_schedule($userid	=	NULL,$jobid	=	NULL)
	{
		$this->_data['userid']	=	$userid;
		$this->_data['jobid']	=	$jobid;

		if($this->input->post())
		{	
			// GET all values from POST
			$data		=	$this->input->post();
				
			// UNSET the value from ARRAY
			unset($data['submit']);
			
			$data	=	array(
							'userid'			=>	$this->input->post('userid'),
							'jobid'				=>	$this->input->post('jobid'),
							'activity_user_id'	=>	$this->_login_userid,
							'schedule_date'		=>	$this->input->post('schedule_date'),
							'detail'			=>	$this->input->post('detail')
				);
			
			// Add Schedule
			$this->job->add_schedule($data);
		}
		else
		{
			// Get all schedules by $userid
			$this->_data['user_schedules']	=	$this->job->all_schedule($userid);
			
			// Load Users Listing Page
			$this->load->view('add-schedule',$this->_data);
		}
	}
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/
	public function add_interview_response($userid	=	NULL,$jobid	=	NULL)
	{
		$this->_data['userid']	=	$userid;
		$this->_data['jobid']	=	$jobid;

		if($this->input->post())
		{	
			// GET all values from POST
			$data		=	$this->input->post();
				
			// UNSET the value from ARRAY
			unset($data['submit']);
			
			$data	=	array(
							'userid'			=>	$this->input->post('userid'),
							'jobid'				=>	$this->input->post('jobid'),
							'activity_user_id'	=>	$this->_login_userid,
							'being_late'		=>	$this->input->post('being_late'),
							'appearance'		=>	$this->input->post('appearance'),
							'impression'		=>	$this->input->post('impression'),
							'interruptions'		=>	$this->input->post('interruptions'),
							'being_tired'		=>	$this->input->post('being_tired'),
							'appearance'		=>	$this->input->post('appearance'),
							'impression'		=>	$this->input->post('impression'),
							'interruptions'		=>	$this->input->post('interruptions'),
							'manners'			=>	$this->input->post('manners'),
							'status'			=>	$this->input->post('status'),
							'email_msg'			=>	$this->input->post('email_msg')
				);
			
			// Add Schedule
			$this->job->add_response($data);
			
			// Email to User
			//send_email($to	=	NULL,$from	=	NULL,$subject	=	NULL,$message = NULL,$path	=	NULL);
		}
		else
		{
			// Get all schedules by $userid
			$this->_data['interview_responses']	=	$this->job->all_interview_responses($userid);
			
			// Load Users Listing Page
			$this->load->view('add-interview-response',$this->_data);
		}
	}
//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/
	
	public function download_file($userid,$file_name)
	{
		$path	= 'resources/users/'.$userid.'/'.$file_name;
		
		//echo FCPATH   ;
		//exit();
		
		// Download File
		downloadFile($path,$file_name);
	}	
	

//-----------------------------------------------------------------------
}
?>
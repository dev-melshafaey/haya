<?php $user_detail	=	$this->haya_model->get_user_detail($userid);?>
<h3><?php echo $user_detail['profile']->fullname;?></h3><h2 align="center" style="margin-top: -35px;">ملاحظات على مقابلة</h2>
<div class="row">
  <div class="row col-md-12">
  
    <form action="" method="POST" id="add_remarks" name="add_remarks" enctype="multipart/form-data" autocomplete="off">
      <div class="col-md-4 form-group">
        <label class="text-warning">يجري في وقت متأخر</label>
        <input type="text" name="being_late" id="being_late" class="form-control req" placeholder="يجري في وقت متأخر"/>
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">مظهر</label>
        <input type="text" name="appearance" id="appearance" class="form-control req" placeholder="مظهر"/>
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">الانطباع</label>
        <input type="text" name="impression" id="impression" class="form-control req" placeholder="الانطباع‎"/>
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">أخلاق</label>
        <input type="text" name="manners" id="manners" class="form-control req" placeholder="أخلاق‎"/>
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">انقطاع</label>
        <input type="text" name="interruptions" id="interruptions" class="form-control req" placeholder="انقطاع‎"/>
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">التعب</label>
        <input type="text" name="being_tired" id="being_tired" class="form-control req" placeholder="التعب"/>
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">مركز مقابلة</label>
        <select name="status" id="status" class="form-control req">
        	<option value="">مختار</option>
            <option value="1">وافق</option>
            <option value="2">رفض</option>
        </select>
      </div>
      <div class="col-md-8 form-group">
        <label class="text-warning">الرسالة</label>
        <textarea name="email_msg" id="email_msg" placeholder="الرسالة" style="margin-top: 0px; margin-bottom: 0px; height: 125px;" class="form-control req"></textarea>
      </div>
      <input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>"/>
      <input type="hidden" name="jobid" id="jobid" value="<?php echo $jobid;?>"/>
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group  col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="add_remark();" value="حفظ" />
    </div>
  </div>
</div>
<br clear="all" />
<div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
  <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
    <thead>
      <tr role="row">
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">تاريخ</th>
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">ملاحظات</th>
        <!--<th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>-->
      </tr>
    </thead>
    <tbody role="alert" aria-live="polite" aria-relevant="all">
      <?php if(!empty($interview_responses)):?>
		  <?php foreach($interview_responses as $response):?>
			  <?php
                //$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$schedule->scheduleid.'" data-url="'.base_url().'users/delete_salary/'.$salary->salaryid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
                
				$status	=	NULL;
				if($response->status	==	'0')
				{
					$status	=	'تحت المعالجة'; 
				}
				elseif($leave->approved	==	'1')
				{
					$status	=	'<img src="'.base_url().'assets/images/approved.png" style="width: 24px;" alt="Approved" title="Approved"/>';
				}
				else
				{
					$status	=	'<img src="'.base_url().'assets/images/not-approved.png" style="width: 24px;" alt="Reject" title="Reject"/>';
				}
			  ?>
              <tr role="row" id="<?php echo $response->interview_responseid.'_durar_lm';?>">
                <td style="text-align:center;" ><?php echo date('d-m-Y',strtotime($response->submited_date));?></td>
                <td style="text-align:center;" ><?php echo $response->email_msg;?></td>
                <!--<td style="text-align:center;" ><?php echo $actions;?></td>-->
              </tr>
          <?php unset($actions); endforeach;?>
      <?php endif;?>
    </tbody>
  </table>
</div>
<script>
function create_data_table(mingo)
{
	if($('.newbasicTable').length > 0)
	{
		 $('.newbasicTable thead th').each( function (index, value) {				
				 var title = $.trim($(this).html());
				 var attr_id = $('.newbasicTable thead th').eq( $(this).index() ).attr('id');
				 if(title!='الإجراءات' && title!='' && title!='متوسط صافي الريح' && title!='متوسط الايرادات' && title!='الشهرية' && title!='السنوية')
				 {				 
				 	$(this).html(title+'<input type="search" class="form-control '+attr_id+' search_filter" placeholder="'+title+'" />' );
				 }
		});
		
		 var basic_table = $('.newbasicTable').DataTable({
			 "ordering": false,
			 "oLanguage": {
				 "sSearch": "",
				 "oPaginate": { "sNext": "التالی", "sPrevious": "السابق" }
				}
			});
		
		basic_table.columns().eq(0).each(function (colIdx) 
		{
			$('input', basic_table.column(colIdx).header()).on('keyup change', function() 
			{
				basic_table.column(colIdx).search(this.value).draw();
			});
		});	
		
		$('.search_filter').keyup(function(){
			$('.newbasicTable td').removeHighlight().highlight($(this).val());
		});	
		
		basic_table.on( 'draw', function () {
    		tiptop();
		} );
	}
}
$(function(){
	
	create_data_table(0);
	
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});
</script>
<div class="col-md-12" style="background-color:#FFF !important; padding:10px 0px;">
      <div class="form-group col-md-6">
        <label for="basic-input"><strong>عنوان وظيفة:</strong></label>
        <?PHP echo $job->jobtitle; ?>
      </div>
      <div class="form-group col-md-3">
        <label for="basic-input"><strong>الراتب ابتداء من:</strong></label>
        <?PHP echo arabic_date($job->salaryStart); ?>
      </div>
      <div class="form-group col-md-3">
        <label for="basic-input"><strong>نهاية الراتب:</strong></label>
        <?PHP echo arabic_date($job->salaryEnd); ?>
      </div>
      <div class="form-group col-md-4">
        <label for="basic-input"><strong>موقع:</strong></label>
        <?PHP echo $job->location; ?>
      </div>
      <div class="form-group col-md-4">
        <label for="basic-input"><strong>تاريخ انتهاء:</strong></label>
        <?PHP echo arabic_date($job->expirydate); ?>
      </div>
      <div class="form-group col-md-4">
        <label for="basic-input"><strong>نهاية الراتب:</strong></label>
        <?PHP echo status_dropbox('jobstatus',$job->jobstatus,3); ?>
      </div>
      <div class="form-group col-md-12">
        <label for="basic-input"><strong>تفاصيل الوظيفة:</strong></label><br>
        <?PHP echo nl2br($job->jobdescription); ?>
      </div>
    
    
</div>

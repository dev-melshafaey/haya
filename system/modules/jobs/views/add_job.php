<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block" style="padding: 10px 10px;">       
          <form method="POST" id="form_add_job" name="form_add_job">
            <input type="hidden" name="jobsid" id="jobsid" value="<?PHP echo $job->jobsid; ?>">
           
            <div class="form-group col-md-6">
              <label for="basic-input"><strong>عنوان وظيفة:</strong></label>
              <input type="text" class="form-control req" value="<?PHP echo $job->jobtitle; ?>" placeholder="عنوان وظيفة" name="jobtitle" id="jobtitle" />
            </div>
            <div class="form-group col-md-3">
              <label for="basic-input"><strong>الراتب ابتداء من:</strong></label>
              <input type="text" class="form-control req NumberInput" value="<?PHP echo $job->salaryStart; ?>" placeholder="الراتب ابتداء من" name="salaryStart" id="salaryStart" />
            </div>
            <div class="form-group col-md-3">
              <label for="basic-input"><strong>نهاية الراتب:</strong></label>
              <input type="text" class="form-control req  NumberInput" value="<?PHP echo $job->salaryEnd; ?>" placeholder="نهاية الراتب" name="salaryEnd" id="salaryEnd" />
            </div>
            <div class="form-group col-md-4">
              <label for="basic-input"><strong>موقع:</strong></label>
              <input type="text" class="form-control req" value="<?PHP echo $job->location; ?>" placeholder="موقع" name="location" id="location" />
            </div>
            <div class="form-group col-md-4">
              <label for="basic-input"><strong>تاريخ انتهاء:</strong></label>
              <input type="text" class="form-control req" value="<?PHP echo $job->expirydate; ?>" placeholder="تاريخ انتهاء" name="expirydate" id="expirydate" />
            </div>
            <div class="form-group col-md-4">
              <label for="basic-input"><strong>نهاية الراتب:</strong></label>
              <?PHP status_dropbox('jobstatus',$job->jobstatus); ?>
            </div>
            <div class="form-group col-md-12">
              <label for="basic-input"><strong>تفاصيل الوظيفة:</strong></label>
              <textarea style="height:200px;"  class="form-control req" id="jobdescription" name="jobdescription"><?PHP echo $job->jobdescription; ?></textarea>
            </div>
          
           
            <div class="form-group col-md-6">
              <button type="button" id="save_job" name="save_job" class="btn btn-success">حفظ</button>
            </div>
          </form>
          <br clear="all">
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
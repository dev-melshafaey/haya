<table class="table table-bordered table-striped dataTable">
  <thead>
    <tr>
      <th style="text-align:center !important;">رقم</th>
      <th>عناصر التقييم</th>
      <th>العدد ( 0-5)</th>
      <th>ملاحظات</th>
    </tr>
  </thead>
  <tbody>
    <?PHP   	
  foreach($list as $listkey => $listvalue) { 
  	$dividvalue = $list['divid'];
	$message = $list['message'];
  	if($listkey!='message' && $listkey!='divid') {   
  ?>
    <tr>
      <td style="text-align:center !important;"><?PHP echo arabic_date($listkey); ?></td>
      <td><?PHP echo $listvalue; ?></td>
      <td><input placeholder="<?PHP echo $listvalue; ?>"  type="text"  name="a<?PHP echo $listkey; ?>" class="NumberInput req ratings form-control" /></td>
      <td><input placeholder="ملاحظات"  type="text"  name="b<?PHP echo $listkey; ?>" class="form-control" /></td>
    </tr>
    <?PHP } } 
  
  	$multiplayer = 100;
  ?>
    <tr>
      <td colspan="4"><table width="700" border="0" style="border:0px !important;">
          <tbody>
            <tr>
              <td rowspan="2" class="middletext noborder" style="  width: 85px;">المجموع =</td>
              <td  class="middletext noborder amountcolor" style="border-bottom:1px solid #666 !important; width: 120px; "><input  type="text" placeholder="المجموع" class="NumberInput req form-control sum" name="totalrating" id="totalrating" readonly/></td>
              <td rowspan="2" class="middletext noborder" style="width: 70px;">x <span class="amountcolor"><?PHP echo $multiplayer; ?></span> =</td>
              <td rowspan="2" class="middletext noborder amountcolor" style="width:100px;" id="taqem_html">--------</td>
              <td rowspan="2" class="middletext noborder" style="width: 30px;">%</td>
              <td rowspan="2" class="middletext noborder"></td>
            </tr>
            <tr>
              <td class="middletext noborder amountcolor" ><?PHP echo $dividvalue; ?></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td colspan="2"><strong>مستوى تقييم العنصر:</strong>
        <li>(0 – 1) ضعيف</li>
        <li>(2-3) جيد</li>
        <li>(4-5) ممتاز</li></td>
      <td colspan="2"><strong>مستوى التقييم العام:</strong>
        <li>(أقل من 50% ) ضعيف</li>
        <li>(50%-69%) جيد</li>
        <li>(70%-89%) جيد جداً</li>
        <li>(90% فأكثر) ممتاز</li></td>
    </tr>
    <tr>
    	<td colspan="4"><?PHP echo $message; ?></td>
    </tr>
  </tbody>
</table>
<script>
$(function(){
		var a_number = ["٠","١","٢","٣","٤","٥","٦","٧","٨","٩"];
		var e_number = ["0","1","2","3","4","5","6","7","8","9"];

	function returnstatus(nmx)
	{
		if(nmx >= 0 && nmx <= 50)
		{
			return '<span class="btn btn-info">(أقل من 50% ) ضعيف</span>';
		}
		else if(nmx > 50 && nmx <= 69)
		{
			return '<span class="btn btn-default">(50%-69%) جيد</span>';
		}	
		else if(nmx > 69 && nmx <= 89)
		{
			return '<span class="btn btn-warning">(70%-89%) جيد جداً</span>';
		}				
		else if(nmx > 89)
		{
			return '<span class="btn btn-success">(90% فأكثر) ممتاز</span>';
		}
	}
	
	$(".ratings").keyup(function(){
			total_charges = 0;			
			$(".ratings").each(function( i, l )
			{
				if($(".ratings").eq(i).val() !="")
				{
					if($(".ratings").eq(i).val()<=5)
					{
						val = $(".ratings").eq(i).val();
						total_charges = parseInt(total_charges) + parseInt(val);
					}
					else
					{	$(".ratings").eq(i).val('');	}
				}
			});
			
			$("#totalrating").val(total_charges);
			divider = '<?PHP echo $dividvalue; ?>';
			
			var multiplayer = 100;
			taq = total_charges/divider*multiplayer;
			taqeem = taq.toFixed(1);
			
			$("#taqem_html").html(returnstatus(taq));
		});	
});
</script>
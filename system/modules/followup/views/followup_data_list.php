<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <p>
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <?php $permissions	=	$this->haya_model->check_other_permission(array('74'));?>
                  <?php if($permissions[74]['a']	==	1){?>
                  <div class="row table-header-row"> <a class="btn btn-success" style="float:right;" href="<?php echo base_url();?>followup/requestfollowup/<?php echo $applicant_id;?>" data-icon="<?php echo base_url();?>images/menu/team_icon.png" data-heading="إضافة" data-color="#4096EE">إضافة</a> </div>
                  <?PHP } ?>
                  <table class="table table-bordered table-striped dataTable" id="basicTable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center; width:80px;">عدد زيارة</th>
                        <th style="text-align:center; width:156px;">تاريخ الزيارة</th>
                        <th style="text-align:center;">طريقة إسناد الأعداد</th>
                        <th style="text-align:center; width: 200px;">اسم فيستر</th>
                        <th colspan="2" style="text-align:center;">متوسط الايرادات</th>
                        <th colspan="2" style="text-align:center;">متوسط صافي الريح </th>
                        <th style="text-align:center;">الإجراءات</th>
                      </tr>
                      <tr role="row">
                        <th colspan="4" style="text-align:center; width: 75px;"></th>
                        <th style="text-align:center; color:#d08926;">الشهرية</th>
                        <th style="text-align:center; color:#d08926;">السنوية</th>
                        <th style="text-align:center; color:#d08926;">الشهرية</th>
                        <th style="text-align:center; color:#d08926;">السنوية</th>
                        <th style="text-align:center; width: 94px;"></th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <?PHP foreach($this->follow->followup_data_list_table_new($applicant_id) as $fol) { ?>
                      <tr role="row">
                        <td style="text-align:center;"><?PHP echo $fol['visit']; ?></td>
                        <td style="text-align:center;"><?PHP echo $fol['visit_month']; ?></td>
                        <td style="text-align:center;"><?PHP echo $fol['visit_award']; ?></td>
                        <td style="text-align:center;"><?PHP echo $fol['visit_person']; ?></td>
                        <td style="text-align:center; color:#d08926;"><?PHP echo arabic_date($fol['visit_total']); ?></td>
                        <td style="text-align:center; color:#d08926;"><?PHP echo arabic_date($fol['visit_grand_total']); ?></td>
                        <td style="text-align:center; color:#d08926;"><?PHP echo arabic_date($fol['a_total']); ?></td>
                        <td style="text-align:center; color:#d08926;"><?PHP echo arabic_date($fol['b_total']); ?></td>
                        <td style="text-align:center;"><?PHP echo $fol['visit_action']; ?></td>
                      </tr>
                      <?PHP } ?>
                    </tbody>
                  </table>
                </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<!-- /.modal-dialog -->
</div>
</body>
</html>
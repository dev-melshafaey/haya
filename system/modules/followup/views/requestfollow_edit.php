<!doctype html>
<?PHP
	$applicant = $applicant_data['applicants'];
	$project = $applicant_data['applicant_project'][0];	
	$professional = $applicant_data['applicant_professional_experience'];
	$loan = $applicant_data['applicant_loans'][0];
	$evo  = $applicant_data['project_evolution'][0];
	$phones = $applicant_data['applicant_phones'];
	$comitte = $applicant_data['comitte_decision'][0];
	
	$fullname = $applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name;
	foreach($phones as $p)
	{	$ar[] = '986'.$p->applicant_phone;	}
	$applicantphone = implode('<br>',$ar);
		
	$steps = get_lable('step-'.$applicant->form_step);
	$youm = array('day'=>'يوم','week'=>'أسبوع','month'=>'شهر','year'=>'عام');
	
	$evaluate = $evaluate_data[0];	
?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php 
	  
	  $this->load->view('common/panel_block', array('module' => $module)); ?>
      <form id="validate_form" name="validate_form" method="post" action="<?PHP echo base_url().'followup/update_follow_up' ?>" autocomplete="off">
        <input type="hidden" name="form_step" id="form_step" value="5" />
        <input type="hidden" name="applicant_id" id="applicant_id" value="<?php echo $id;?>" />
        <div class="col-md-12">
          <div class="panel panel-default panel-block">
            <div class="list-group">
              <div class="list-group-item" id="input-fields">
                <div class="form-group">
                  <h4 class="section-title preface-title text-warning"> <?PHP echo $fullname; ?> <span class="label label-default">رقم التسجيل <?php echo arabic_date(applicant_number($id)); ?></span></h4>
                </div>
                 <div class="form-group">
                  <div class="col-md-2">
                            <label class="text-warning">عن طريق الزيارة الميدانية:</label>
                            <label><input type="radio" <?PHP if ($evaluate->zyara_type=='foot') { ?> checked <?PHP } ?> id="zyara_type" name="zyara_type" value="foot"></label>
                          </div>
                          <div class="col-md-2">
                            <label class="text-warning">عن طريق الهاتف:</label>
                            <label><input type="radio" <?PHP if ($evaluate->zyara_type=='phone') { ?> checked <?PHP } ?> id="zyara_type" name="zyara_type" value="phone"></label>
                          </div>
                          <br clear="all">
                </div>
                <!--------Starting Tabs--------->
                <div class="row">
                  <div class="col-md-12">
                    <ul class="nav nav-tabs panel panel-default panel-block">
                      <li class="active"><a href="#tabsdemo-2" data-toggle="tab">الاستمار والمردود المالي</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-3" data-toggle="tab">أوجه الدعم المقدمة من الجهات الاخرى</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-4" data-toggle="tab">بطاقة تقييم مشروع</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-5" data-toggle="tab">تاريخ التقييم</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-6" data-toggle="tab">رأي المتابـــع</a></li>
                    </ul>
                    <div class="tab-content panel panel-default panel-block" style="background-color: #fff; padding: 6px 8px;">
                      <div class="tab-pane list-group active" id="tabsdemo-2"> 
                        <!------------------------------------->
                        <?php						
                        if(!empty($financial)) {
                            foreach($financial as  $i=>$finance) { ?>
                        <div class="row" id="first">
                        <input type="hidden" id="returns_id" name="returns_id" value="<?php echo $finance->returns_id; ?>">
                          <div class="col-md-2 fcounter<?php echo $i; ?>">
                            <label class="text-warning">قيمة المشروع الحالية(رع):</label>
                            <label>
                            <input type="text" name="present_value_project[]" id="present_value_project[]" value="<?php echo $finance->present_value_project; ?>" class="NumberInput form-control" />
                           
                            </label>
                          </div>
                          <div class="col-md-2 fcounter<?php echo $i; ?>">
                            <label class="text-warning">متوسط الايرادات الشهرية(رع):</label>
                            <label>
                            <input type="text" name="average_monthly_revenue[]" id="average_monthly_revenue[]" value="<?php echo $finance->average_monthly_revenue; ?>" class="NumberInput  form-control"/>
                            
                            </label>
                          </div>
                          <div class="col-md-2 fcounter<?php echo $i; ?>">
                            <label class="text-warning">السنوية الايرادات متوسط(رع):</label>
                            <label>
                            <input type="text" name="average_anual_revenue[]" id="average_anual_revenue[]" value="<?php echo $finance->average_anual_revenue; ?>" class="NumberInput  form-control"/>
                           
                            </label>
                          </div>
                          <div class="col-md-3 fcounter<?php echo $i; ?>">
                            <label class="text-warning">الشهري الريح صافي متوسط(رع):</label>
                            <label>
                            <input type="text" name="net_average_monthly_revenue[]" id="net_average_monthly_revenue[]" value="<?php echo $finance->average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            
                            </label>
                          </div>
                          <div class="col-md-3 fcounter<?php echo $i; ?>">
                            <label class="text-warning">السنوي الريح صافي متوسط(رع):</label>
                            <label>
                            <input type="text" name="net_average_anual_revenue[]" id="net_average_anual_revenue[]"  value="<?php echo $finance->net_average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            
                            </label>
                          </div>                          
                        </div>
                        <div class="row setMargin">
                        <div class="col-md-3 fcounter<?php echo $i; ?>">
                          	<label class="text-warning">تاريخ: </label>
                            <label><?php echo show_date($finance->created,6);  ?></label>
                          </div>
                        </div>
                        <?PHP if($finance->user_name!='') { ?>
                        <div class="row setMargin">
                        <div class="col-md-3 fcounter<?php echo $i; ?>">
                          	<label class="text-warning">وأضاف المستعمل من قبل: </label>
                            <label><?php echo $finance->user_name;  ?></label>
                          </div>
                        </div>
                        <?PHP } } 
                        } ?>
                        <!-------------------------------------> 
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-3">
                        <?PHP 
					  if(!empty($support))	{ 
					  	foreach($support as $i=>$sup)	{	
							$checked1 = "";
							$checked2 = "";
							if($sup->support_training == '1')
							{
								$checked1 ="checked";
								$display = "block";			}
							else
							{	$checked2 ="checked";
								$display = "none";			}
				 ?>
                        <div class="row" id="second"> 
                          <div class="col-md-10">
                            <label class="text-warning">الدعم التدريبي:</label>
                            <label>نعم
                              <input type="radio" name="support_training[]" id="support_training<?php echo $i; ?>" value="1" <?php echo $checked1;  ?>  class="supporting_classs"/>
                              &nbsp;&nbsp; لا
                              <input type="radio" name="support_training[]" id="support_training<?php echo $i; ?>" value="0" <?php echo $checked2;  ?> class="supporting_classs"/>
                            </label>
                          </div>
                          <div class="col-md-11 yes_class_support support_training<?php echo $i; ?>"  style="display:<?php echo $display; ?>;">
                            <div class="col-md-2">
                              <input class="form-control" placeholder="مجال التدريب لصاحب المنشأة" type="text"  name="training_owner_facility[]" id="training_owner_facility"  value="<?php echo $sup->training_owner_facility;  ?>"/>
                            </div>
                            <div class="col-md-2">
                              <input class="form-control" placeholder="جهة التدريب" type="text"  name="training[]" id="training"  value="<?php echo $sup->training;  ?>"/>
                            </div>
                            <div class="col-md-2">
                              <input id="duration[]" placeholder="المدة" name="duration[]" class="form-control" value="<?php echo $sup->duration; ?>" />
                            </div>
                            <div class="col-md-2">
                              <select id="duration[]" name="duration[]"  class="form-control">
                                <?PHP foreach($youm as $y) { ?>
                                <option value="<?PHP echo $y; ?>" <?PHP if($sup->duration == $y) { ?>selected="selected" <?PHP } ?>><?PHP echo $y; ?></option>
                                <?PHP } ?>
                              </select>
                            </div>
                            <div class="col-md-2 boxx">
                            <?PHP
                            		if($sup->before_incoporation == '1')
									{
										$bi_checked_1 ="checked";
										$display = "block";			}
									else
									{	$bi_checked_2 ="checked";
										$display = "none";			}
							?>
                              <label class="text-warning">قبل التأسيس:</label>
                              نعم
                              <input type="radio" <?PHP echo $bi_checked_1; ?> name="before_incoporation[]" id="before_incoporation" value="1" class="supporting_classs"/>
                              لا
                              <input type="radio"  <?PHP echo $bi_checked_2; ?> name="before_incoporation[]" id="before_incoporation" value="0" class="supporting_classs" />
                            </div>
                            <div class="col-md-2">
                              <select id="durationtype[]" name="durationtype[]"  class="form-control">
                                <option value="">بعد التأسيس</option>
                                <?PHP foreach($youm as $y) { ?>
                                <option value="<?PHP echo $y; ?>" <?PHP if($sup->durationtype == $y) { ?>selected="selected" <?PHP } ?>><?PHP echo $y; ?></option>
                                <?PHP } ?>
                              </select>
                            </div>
                          </div>
                          <!----------------------------------> 
                          <!---------------------------------->
                          <?PHP
						$fchecked1 = "";
						$fchecked2 = "";
						if($sup->funding_support == '1')
						{
							$fchecked1 ="checked";
							$funding_support = "block";
						}
						else
						{
							$fchecked2 ="checked";
							$funding_support = "none";
						}
					?>
                          <div class="col-md-11">
                            <label class="text-warning">الدعم التمويلي:</label>
                            <label>نعم
                              <input type="radio" name="funding_support[]" id="funding_support<?php echo $i; ?>" value="1" class="supporting_classs" <?php echo $fchecked1; ?>/>
                              &nbsp;&nbsp; لا
                              <input type="radio" name="funding_support[]" id="funding_support<?php echo $i; ?>" value="0" class="supporting_classs" <?php echo $fchecked2; ?>/>
                            </label>
                          </div>
                          <div class="col-md-11 yes_class_funds funding_support<?php echo $i; ?>" style="display:<?php echo $funding_support; ?>;">
                            <div class="col-md-2">
                              <input type="text" placeholder="مبلغ الدعم (رع)"  name="amount_support[]" id="amount_support" class="NumberInput form-control" value="<?php echo $sup->amount_support;  ?>"/>
                            </div>
                            <div class="col-md-2">
                              <input type="text" placeholder="جهة الدعم (رع)"  name="support_point[]" id="support_point" class="NumberInput form-control" value="<?php echo $sup->support_point;  ?>"/>
                            </div>
                            <div class="col-md-2">
                              <select name="support_type[]" id="<?php echo $i; ?>" class="others form-control">
                                <option value="">نوع الدعم</option>
                                <?PHP $aax = array('قرض'=>'قرض','منحة'=>'منحة','others'=>'اخرى يتم ذكرها');
							foreach($aax as $paax => $paaxvalue)
							{
								$html .= '<option value="'.$paaxvalue.'" ';
								if($paax==$sup->support_type)
								{
									$html .= ' selected="selected" ';
								}
								$html .= '>'.$paaxvalue.'</option>';
							}
								echo $html;				
						?>
                              </select>
                            </div>
                            <?php
							if($sup->support_type == 'اخرى يتم ذكرها')
							{	$yes_class_funds= "block"; 	}
							else
							{	$yes_class_funds= "none";	}
						?>
                            <div class="col-md-2 yes_class_funds" id="others<?php echo $i; ?>" style="display:<?php echo $yes_class_funds; ?>;">
                              <input  type="text" class="form-control" placeholder="أخرى (يتم ذكرها)" name="mention_others[]" id="mention_others" value="<?php echo $sup->mention_others;  ?>"/>
                            </div>
                          </div>
                          <!----------------------------------> 
                          <!---------------------------------->
                          <?php
							if($sup->face_others_support == '1')
							{	$face_others_support_display = "block";
								$face_others_support_checked = 'checked';	}
							else
							{	$face_others_support_display = "none";
								$face_others_support_checked = '';	}
						?>
                          <div class="col-md-11">
                            <label class="text-warning">وجه دعم أخرى: </label>
                            <label>نعم
                              <input type="radio"  <?PHP echo $face_others_support_checked; ?> name="face_others_support[]" id="face_others_support<?php echo $i; ?>" value="1" class="supporting_classs"/>
                              &nbsp;&nbsp; لا
                              <input type="radio" <?PHP echo $face_others_support_checked; ?> name="face_others_support[]" id="face_others_support<?php echo $i; ?>" value="0" class="supporting_classs"/>
                            </label>
                          </div>
                          <div class="col-md-11 yes_class_others face_others_support0"  style="display:<?PHP echo $face_others_support_display; ?>;">
                            <div class="col-md-2">
                              <input type="text" placeholder="اذكرها"  name="face_others_support_text[]" id="face_others_support_text" class="NumberInput form-control" value="<?php echo $sup->face_others_support_text;  ?>"/>
                            </div>
                          </div>
                        </div>
                        <br>
                        <?PHP 		}
						}   if(!empty($annoted1) || !empty($annoted2))	{	?>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">المشروع</h4>
                          </div>
                        </div>
                        <?PHP 	$anot = $annoted1[0];
								$anot2 = $annoted2[0];
																					 
								if($anot->anoted_type == '1')
								{	$p_status = "checked";
									$display = 'block'; }
								else
								{	$p_status = "";
									$dislpay = 'none';	}
									
									if($anot->anoted_value =='نشاط متميز جدا')
									{	$active1 = "checked";
										$display ="none";	}
									elseif($anot->anoted_value =='نشاط  متميز')
									{	$active2 = "checked";
										$display ="none";	}
									elseif($anot->anoted_value =='نشاط عادي')
									{	$active3 = "checked";
										$display ="none";	}
									elseif($anot->anoted_value =='يواجه صعوبات')
									{	$active4 = "checked";
										$display ="block";	}
									////////////////////////////////////////
									////////////////////////////////////////
									if($anot2->anoted_type == '1')
									{	$p_status2 = "checked";
										$display2 = 'block'; }
									else
									{	$p_status2 = "";
										$dislpay2 = 'none';	}
										
										if($anot2->anoted_value =='نهائي')
										{	$active11 = "checked";
											$display2 ="block";	}
										elseif($anot2->anoted_value =='ظرفي')
										{	$active12 = "checked";
											$display2 ="block";	}
									
						?>
                        <div class="row" id="second">
                        <input type="hidden" id="anot_id1" name="anot_id1" value="<?php echo $anot->anoted_id; ?>">
                          <div class="col-md-11">
                            <label class="text-warning"></label>
                            <label>مفتوح
                              <input type="checkbox" <?php echo $p_status; ?> name="parwa_open" id="parwa_open" value="1" onclick = "check_status(this)"/>
                              &nbsp;&nbsp; مغلق
                              <input type="checkbox" <?php echo $p_status2; ?> name="close_project" id="close_project" value="2" onclick = "check_status(this)"/>
                            </label>
                          </div>
                          <div class="col-md-11  parwa_open" style="display:<?PHP echo $display; ?>;">
                            <div class="col-md-2">
                              <label class="text-warning">نشاط متميز جدا</label>
                              <input type="radio" class="activty_type" <?php echo $active1; ?> name="activty_type" id="activty_type1" value="نشاط متميز جدا"  onclick="check_activity(this)" />
                            </div>
                            <div class="col-md-2">
                              <label class="text-warning">نشاط  متميز</label>
                              <input type="radio" class="activty_type" <?php echo $active2; ?> name="activty_type" id="activty_type2" value="نشاط  متميز" onClick="check_activity(this)"/>
                            </div>
                            <div class="col-md-2">
                              <label class="text-warning">نشاط عادي</label>
                              <input type="radio" class="activty_type" <?php echo $active3; ?> name="activty_type" id="activty_type3" value="نشاط عادي" onClick="check_activity(this)"/>
                            </div>
                            <div class="col-md-2">
                              <label class="text-warning">يواجه صعوبات</label>
                              <input type="radio" class="activty_type" <?php echo $active4; ?> name="activty_type" id="activty_type4" value="يواجه صعوبات" onClick="check_activity(this)"/>
                            </div>
                          </div>
                          <div class="col-md-11 difficulties_details" style="display:<?php echo $display; ?>;">
                            <div class="col-md-10">
                              <label class="text-warning">اذكرالصعوبات</label>
                              <br>
                              <textarea id="difficulties" name="difficulties" class="form-control txtarea"><?php echo $anot->anoted_details; ?></textarea>
                            </div>
                          </div>
                          <div class="col-md-10  close_project" style="display:<?PHP echo $display2; ?>;">
                            <div class="col-md-2">
                              <label class="text-warning">نهائي</label>
                              <input type="radio" <?PHP echo $active11; ?> name="project_status" id="project_status" class="p_status" value="نهائي" onClick="check_status2(this);" />
                            </div>
                            <div class="col-md-2">
                              <label class="text-warning">ظرفي</label>
                              <input type="radio" <?PHP echo $active12; ?> name="project_status" id="project_status2" class="p_status" value="ظرفي"  onclick="check_status2(this);" />
                            </div>
                          </div>
                          <div class="col-md-10  reason" style='display:<?PHP echo $display2; ?>;'>
                            <div class="col-md-10">
                              <label class="text-warning"> الأسباب (ان وجدت)</label>
                              <br>
                              <textarea id="reason_text" name="reason_text" class="form-control txtarea"><?php echo $anot2->anoted_details; ?></textarea>
                            </div>
                          </div>
                          <!----------------------------------> 
                          <!----------------------------------> 
                          
                        </div>
                        <br>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">مقترحات صاحب المشروع لتخطي الصعوبات او تطوير الموسسة:</h4>
                          </div>
                          <div class="col-md-10">
                          <input type="hidden" id="project_detail_id" name="project_detail_id" value="<?php echo $prop->project_detail_id; ?>">	
                            <textarea id="ckeditor" name="project_propsel" class="txtarea"><?php echo $prop->project_propsel; ?></textarea>
                          </div>
                        </div>
                        <!-------------------------------------> 
                        <?PHP  } ?>
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-4">
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">بطاقة تقييم مشروع:</h4>
                          </div>
                          <div class="col-md-12">
                         
                          	<div class="col-md-2" style="color: #d08926;">بطاقة تقييم مشروع:</div>
                            <div class="col-md-3">
                             <?PHP if($applicant->mashrow_taqseem!='')
							 		{	echo '<strong>'.$applicant->mashrow_taqseem.'</strong><input type="hidden" name="mashrow_taqseem" value="'.$applicant->mashrow_taqseem.'">';  ?>
                                    	<script>
											$(function(){
												mashrow_taqseem('<?PHP echo $applicant->mashrow_taqseem; ?>');
											});
										</script>
                                    <?PHP
									}
									else
									{
						  ?>
                            <select place-holder="بطاقة تقييم مشروع" id="mashrow_taqseem" name="mashrow_taqseem" class="form-control req">
                                        	<option value="0">بطاقة تقييم مشروع</option>
                                            <?PHP foreach(bataka_taqseem() as $key => $value) { ?>
                                            	<option value="<?PHP echo $key; ?>"><?PHP echo $key; ?></option>
                                            <?PHP } ?>
                                        </select>
                            <?PHP  } ?>            
                                        </div>
                                        <br clear="all">
                          </div>
                          <div class="col-md-12" id="inamatlist">
                            
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-5">
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">تاريخ التقييم:</h4>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-3">
                              <label class="text-warning">تاريخ التقييم:</label>
                              <label>
                                <input type="text" class="form-control" name="month" id="month" value="<?php echo date('m/d/Y'); ?>">
                              </label>
                            </div>
                          </div>
                        </div>
                        <?PHP foreach($monthly_financial as $monthly) { ?>
                        <input type="hidden" id="month_financial" name="month_financial" value="<?php echo $monthly->month_financial; ?>">
                        <div class="row">
                          <div class="col-md-12">
                            <table  class="table table-bordered table-striped dataTable">
                              <tr>
                                <td colspan='2' class="td_pad">المصروفات (ر.ع)</td>
                                <td colspan='2' class="td_pad">الإيرادات (ر.ع)</td>
                              </tr>
                              <tr>
                                <td class="td_pad">مشتريات</td>
                                <td><input  type="text" placeholder="مشتريات"  name="purchase" class="NumberInput req expence form-control" id="purchase" value="<?php echo $monthly->purchase;  ?>"/></td>
                                <td  rowspan='3' class="td_pad">إيراد</td>
                                <td  class="td_pad"><input  type="text" placeholder="إيراد"  name="manpower_project" class="NumberInput req income form-control" id="manpower_project" style="width: 100%;height:100%" value="<?php echo $monthly->manpower_project;  ?>"/>
                                </tr>
                              <tr>
                                <td class="td_pad">راتب صاحب المشروع (بما في ذلك التأمينات الاجتماعية)</td>
                                <td class="td_pad"><input  type="text" placeholder="راتب صاحب المشروع (بما في ذلك التأمينات الاجتماعية)"  name="salary_project" class="NumberInput req expence form-control" id="salary_project" value="<?php echo $monthly->salary_project;  ?>"/></td>
                              </tr>
                              <tr>
                                <td class="td_pad">رواتب القوى العاملة بالمشروع (بما في ذلك التأمينات الاجتماعية)</td>
                                <td class="td_pad"><input  type="text" placeholder="راتب صاحب المشروع (بما في ذلك التأمينات الاجتماعية)"  name="manpower_project" class="NumberInput req expence form-control" id="manpower_project" value="<?php echo $monthly->manpower_project;  ?>"/></td>
                              </tr>
                              <tr>
                                <td class="td_pad">إيجار</td>
                                <td class="td_pad"><input  type="text" placeholder="إيجار"  name="rent" class="NumberInput req expence form-control" id="rent" value="<?php echo $monthly->rent;  ?>"/></td>
                              </tr>
                              <tr>
                                <td class="td_pad">إنترنت</td>
                                <td class="td_pad"><input  type="text" placeholder="إنترنت"  name="expence" class="NumberInput req expence form-control" id="expence" value="<?php echo $monthly->expence;  ?>"/></td>
                              </tr>
                               
                              <tr>
                                <td class="td_pad">كهرباء</td>
                                <td class="td_pad"><input  type="text" placeholder="كهرباء"  name="wire_expence" class="NumberInput req expence form-control" id="wire_expence" value="<?php echo $monthly->wire_expence;  ?>"/></td>
                              <td rowspan='2' >إيرادات أخرى متصلة بالمشروع </td>
                             	<td rowspan='3'><input  type="text" placeholder="إيرادات أخرى متصلة بالمشروع "  name="other_income" class="NumberInput req income form-control" id="other_income" style="width: 100%;height:100%" value="<?php  echo $monthly->other_income;  ?>"/></td>
                          
                              </tr>
                              <tr>
                                <td class="td_pad">ماء</td>
                                <td class="td_pad"><input  type="text" placeholder="ماء"  name="water_expence" class="NumberInput req expence form-control" id="water_expence" value="<?php echo $monthly->water_expence;  ?>"/></td>
                              </tr>
                              <tr>
                                <td class="td_pad">هاتف</td>
                                <td class="td_pad"><input  type="text" placeholder="هاتف"  name="number_expence" class="NumberInput req expence form-control" id="number_expence" value="<?php echo $monthly->number_expence;  ?>"/></td>
                              </tr>
                              <tr>
                                <td class="td_pad">فاكس</td>
                                <td class="td_pad"><input  type="text" placeholder="فاكس"  name="fax_expence" class="NumberInput req expence form-control" id="fax_expence" value="<?php echo $monthly->fax_expence;  ?>"/></td>
                              </tr>
                              <tr>
                                <td class="td_pad">خدمات مختلفة</td>
                                <td class="td_pad"><input  type="text" placeholder="خدمات مختلفة"  name="diffrent_services" class="NumberInput req expence form-control" id="diffrent_services" value="<?php echo $monthly->diffrent_services;  ?>"/></td>
                              </tr>
                              <tr>
                                <td class="td_pad">مصروفات أخرى متصلة بالمشروع</td>
                                <td class="td_pad"><input  type="text" placeholder="مصروفات أخرى متصلة بالمشروع"  name="other_expence" class="NumberInput req expence form-control" id="other_expence" value="<?php echo $monthly->other_expence;  ?>"/></td>
                              </tr>
                              <tr>
                                <td class="td_pad">الإجمالي</td>
                                <td class="td_pad"><input  type="text" placeholder="الإجمالي"  name="total_expence" class="NumberInput req form-control" id="total_expence" value="<?php echo $monthly->total_expence;  ?>"/></td>
                                <td class="td_pad" colspan='2'><input  type="text" placeholder="الإجمالي"  name="total_income" class="NumberInput req  form-control" id="total_income" value="<?php echo $total_income = $monthly->other_income+$monthly->manpower_project;  ?>"/></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                        <?PHP } 
							  $obs = $observer['0'];
						?>
                        <div class="row">
                          <div class="col-md-12"> صافي الدخل الشهري للمشروع   = <span id="tt2"></span>- <span id="tt"></span> = <span id="tt3"></span> ريال عماني </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="col-md-3">صافي الأرباح الشهرية   = </div>
                            <div class="col-md-3"><span>
                              <input type="text" class="form-control" id="tt5" />
                              </span> - </div>
                            <div class="col-md-3"><span id="tt4"></span> =<span id="tt6"> ريال عماني </span></div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-6">
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">رأي المتابـــع:</h4>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                            <input type="hidden" id="observer_id" name="observer_id" value="<?php echo $obs->project_detail_id; ?>">
                              <textarea id="ckeditor2" name="observe_view" style="width: 100%; height: 300px;"><?php echo $obs->project_details; ?></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">                    
                    <button type="button" id="save_data_form" class="btn btn-success">حفظ</button>
                  </div>
                </div>
                <!-------------------------------------> 
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
$(function() {
	first_counter = '<?php echo count($financial); ?>';
	second_counter = '<?php echo count($support); ?>';
});
</script> 
<script src="<?PHP echo base_url(); ?>js/request_followup.js"></script>
</div>
</body>
</html>
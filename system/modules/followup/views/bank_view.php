<!doctype html>
<?PHP
	$applicant = $applicant_data['applicants'];
	$project = $applicant_data['applicant_project'][0];	
	$professional = $applicant_data['applicant_professional_experience'];
	$loan = $applicant_data['applicant_loans'][0];
	$evo  = $applicant_data['project_evolution'][0];
	$phones = $applicant_data['applicant_phones'];
	$comitte = $applicant_data['comitte_decision'][0];
	$fullname = $applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name;
	foreach($phones as $p)
	{	$ar[] = '986'.$p->applicant_phone;	}
		$applicantphone = implode('<br>',$ar);
?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
  <form id="validate_form_data" name="validate_form_data" method="post" action="<?PHP echo base_url().'inquiries/comittie_decision' ?>" autocomplete="off">
    <input type="hidden" name="form_step" id="form_step" value="5" />
    <input type="hidden" name="applicant_id" id="applicant_id" value="<?php echo $applicant->applicant_id;?>" />
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="list-group">
            <div class="list-group-item" id="input-fields">
              <div class="form-group">
                <h4 class="section-title preface-title text-warning">بيانات القرض</h4>
                <h2 style="float:left;"><a href="#1" onClick="showglobaldata(this);" data-url="<?php echo base_url(); ?>inquiries/requestdocument_data/<?php echo $applicant->applicant_id; ?>" id="<?php echo $applicant->applicant_id; ?>"><i style="color:#0099FF;" class="icon-file-alt"></i></a></h2>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label class="text-warning"> رقم القرض:</label>
                  <label><strong><?php echo $loan_data->ILOM_SEQUENCE; ?></strong></label>
                </div>
                <div class="col-md-3">
                  <label class="text-warning">تاريخ الاتفاقية:</label>
                  <label><strong>
                    <?php  echo $loan_data->ILOM_VAL_TRS_DATE;?>
                    </strong></label>
                </div>
                <div class="col-md-3">
                  <label class="text-warning">قيمة القرض:</label>
                  <label><strong>
                    <?php  echo $loan_data->ILOM_AMOUNT;?>
                    ريال عماني</strong></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label class="text-warning">المبالغ المصروفة:</label>
                  <label><strong><?php echo $loan_data->ILOM_WITHDRAWN_AMOUNT; ?> ريال عماني </strong></label>
                </div>
                <div class="col-md-3">
                  <label class="text-warning">المبلغ المتبقي للصرف:</label>
                  <label><strong><?php echo $loan_data->REMAIN_DISB; ?> ريال عماني</strong></label>
                </div>
                <div class="col-md-3">
                  <label class="text-warning">تاريخ انتهاء الصرف:</label>
                  <label><strong><?php echo $loan_data->ILOM_DRAWDOWN_LMT_DATE; ?></strong></label>
                </div>
              </div>
            </div>
            <?PHP if(isset($bank_gurantee) && !empty($bank_gurantee))	{
					$bank = $bank_gurantee['0'];  ?>
            <div class="list-group-item" id="input-fields">
              <div class="form-group">
                <h4 class="section-title preface-title text-warning">الضمانة البنكية (LG)</h4>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label class="text-warning"> اسم المورد:</label>
                  <label><strong><?php echo $bank->SUPPLIER_NAME; ?></strong></label>
                </div>
                <div class="col-md-3">
                  <label class="text-warning">تاريخ بدء الضمانة:</label>
                  <label><strong> <?php echo $bank->LG_DATE_FROM; ?> </strong></label>
                </div>
                <div class="col-md-3">
                  <label class="text-warning">تاريخ انتهاء الضمانة:</label>
                  <label><strong> <?php echo $bank->LG_DATE_TO; ?></strong></label>
                </div>
              </div>
            </div>
            <?PHP } if(isset($bank_dist) && !empty($bank_dist)) { ?>
            <div class="list-group-item" id="input-fields">
              <div class="form-group">
                <h4 class="section-title preface-title text-warning">صرف الدفعات</h4>
              </div>
              <?PHP foreach($bank_dist as $dis) { 
			  	//echo "<pre>";
				//print_r($dis);
				//echo $dist['DISB_DATE'];
				//echo $dis->DISB_AMT."sdasd";
			  ?>
              <div class="row">
                <div class="col-md-3">
                  <label class="text-warning"> تاريخ الصرف :</label>
                  <label><strong><?php echo $dis->DISB_DATE; ?></strong></label>
                </div>
                <div class="col-md-3">
                  <label class="text-warning"> المبلغ المصروف:</label>
                  <label><strong><?php echo $dis->DISB_AMT; ?> ريال عماني</strong></label>
                </div>
              </div>
              <?PHP } ?>
            </div>
            <?PHP } if($repayment_schduale !="" && !empty($repayment_schduale)) {
				foreach($repayment_schduale as $repayment){
				 ?>
            <div class="list-group-item" id="input-fields">
              <div class="form-group">
                <h4 class="section-title preface-title text-warning">جدول السداد</h4>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label class="text-warning">أقساط فترة السماح:</label>
                  <label><strong><?php echo $repayment->ILOM_GRACE_PERD_PERIOD ?></strong></label>
                </div>
                <div class="col-md-3">
                  <label class="text-warning">أشهر فترة السماح:</label>
                  <label><strong><?php echo $repayment->GRACE_MONTHS ?></strong></label>
                </div>
                <div class="col-md-3">
                  <label class="text-warning">أقساط السداد:</label>
                  <label><strong><?php echo $repayment->ILOM_NB_INSTALL; ?></strong></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label class="text-warning">فترة السداد:</label>
                  <label><strong><?php echo $repayment->PAYMENT_YEARS; ?></strong></label>
                </div>
                <div class="col-md-3">
                  <label class="text-warning">أاجمالي المسدد:</label>
                  <label><strong><?php echo $repayment->TOT_PAID; ?> ريال عماني</strong></label>
                </div>
                <div class="col-md-3">
                  <label class="text-warning">اجمالي المتبقي:</label>
                  <label><strong><?php echo $repayment->TOT_OUTSTD; ?> ريال عماني</strong></label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-9">
                  <label class="text-warning">المبالغ المتأخرة:</label>
                  <label><strong><?php echo $repayment->TOT_PD; ?> ريال عماني</strong></label>
                </div>
              </div>
            </div>
            <?PHP 
				}
			} if(!empty($banks_payment_exchange)) { ?>
            <div class="list-group-item" id="input-fields">
              <div class="row">
                <div class="col-md-9">
                  <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                     <tr>
                        <th>التسلسل‬</th>
                        <th>‫مبلغ القائم‬</th>
                        <th>تاريخ الاستحقاق</th>
                        <th>‫‫مبلغ القسط‬</th>
                        <th>‫الرسوم</th>
                        <th>المبلغ المسدد‬</th>
                        <th>حالة القسط‬</th>
                      </tr>
                    </thead>
                    <tbody>                      
                      <?PHP foreach($banks_payment_exchange as $payment ) { ?>
                      <tr>
                        <td><?php echo $payment->ILOM_SEQUENCE; ?></td>
                        <td><?php echo $payment->ILOD_CPT_AMNT; ?></td>
                        <td><?php echo $payment->ILOD_BILL_MTR;?></td>
                        <td><?php echo $payment->ILOD_BILL_MNT;?></td>
                        <td><?php echo $payment->ILOD_BILL_INT;?></td>
                        <td><?php echo $payment->ILOD_PAID_MNT;?>
                        <td><?php echo $payment->ILOD_STATUS;?></td>
                      </tr>
                      <?PHP } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <?PHP } if(check_view_permission(64)==1) { ?>
            <form id="validate_form_data" >
            <div class="list-group-item" id="input-fields">
              <div class="form-group">
                <h4 class="section-title preface-title text-warning">إعادة جدولة</h4>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label class="text-warning">عدد الأقساط:</label><br>
                  <label><input class="form-control" name="installment_value" id="installment_value" type="text" value="4"></label>
                </div>
                <div id="demo-collapse8" class="panel-collapse" style="height: 0px;  text-align:center;">
          <div class="panel-body multiple_uploader2" data-index="8" data-heading="المستندات" id="drag8">
            <div class="browser">
              <input type="hidden" data-id="8" id="document_uploaded_8" placeholder="المستندات" class="dreq" value="<?PHP //echo $is_uploaded; ?>">
              <input type="file" style="font-size: 11px;"  name="files[]" multiple id="document_id8" title='المستندات'>
            </div>
            <div class="col-md-3">
                <div class="data8">
                    <?PHP 
						 ?>
                </div>
            </div>
            
          </div>
           <div class="row"><?php if(!empty($loan_rescehuling)) {
							//echo "<pre>";
							//print_r($loan_rescehuling);
							foreach($loan_rescehuling as $loan){
								?>
                                <div class="col-md-3">
                              <label class="text-warning">عدد الأقساط:</label>
                                <label><strong><?php echo $loan['approved_amount']; ?></strong></label>
                            </div>
                                <br/>
                                <div class="col-md-3">
                            	<?php
									if(!empty($loan_rescehuling_document)) {
                         	   //echo "<pre>";
                            //print_r($guarantee_attach);
                            foreach($loan_rescehuling_document as $i=>$g_attch){
								if($loan['id'] == $g_attch['reschedual_id']){ 
                            ?>
                            <input type="hidden" name="document_name_8[<?php echo $i; ?>]" id="document_name_8" value="<?php echo $g_attch['rescehdule_document']; ?>" class="doc8<?php echo $i; ?>">
                            <a  title=" صورة " href="<?php echo base_url() ?>/upload_files/documents/<?php echo $g_attch['rescehdule_document']; ?>" rel="gallery1" class="fancybox-button doclink<?php echo $i; ?>"><img style="width:60%; height:60%" src="<?php echo base_url(); ?>upload_files/documents/<?php echo $g_attch['rescehdule_document']; ?>"></a>
                           <i class="icon-remove-sign doc8remove<?php echo $i; ?>" style="color:#CC0000;cursor:pointer"  onclick="deleteFile('<?php echo $i; ?>')"></i>
                            <?php
								}
                            }
                        } 
								?>
                            </div>
                                <?php
							}
						}
				?></div></div>
           <div class="col-md-8">
          <div style="padding: 4px; float:right;" class="panel panel-default panel-block">
            <button class="btn btn-lg btn-success" id="save_data_form_rescehduale" type="button">حفظ</button>
          </div>
        </div>
        </div>
                <!--<div class="col-md-3">
                  <label class="text-warning">المستندات:</label>
                  <label><input style="font-size: 11px; letter-spacing: 1px; text-transform: capitalize !important;"  class="form-control" type="file"></label>
                </div>-->                
              </div>
            </div>
            </form>
            <?PHP } ?>          
            
          </div>
        </div>
      </div>
    </div>
    </div>
  </form>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
$(function(){
	$('#example').DataTable();	
});
</script>
<script type="text/javascript">
$(document).ready(function(){
		$('#save_data_form_rescehduale').click(function()
		{
			 check_my_session();
			 //alert('asdas');
			$('#validate_form_data .req').removeClass('parsley-error');
			var form_action = $('#validate_form_data').attr('action');
			var ht = '<ul>';
			$('#validate_form_data .req').each(function(index, element)
			{
				if($(this).val()=='')
				{
					$(this).addClass('parsley-error');
					ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
				}
			});
			var redline = $('.parsley-error').length;
			ht += '</ul>';
			if(redline <= 0)
			{
				
				var dadx = $('#validate_form_data').serialize();				
				var taurusData = $.ajax({
					url:  config.BASE_URL+'followup/add_recheduale_data',
					type:"POST",
					data:dadx,
					beforeSend: function(){	$('#loaderModel').modal(); },
					success: function(msg)
					{	
						//alert('sad');
						//return;
						show_notification('وقد أضيف إلى الاستعلام عن في النظام');	
							$('#loaderModel').modal('hide');
						
						}
				});
			}
			else
			{	show_notification_error_end(ht);	}
		});

	
	});
</script>
<script type="text/javascript">
function deleteFile(ind){
			var r = confirm("هل أنت متأكد أنك تريد حذف؟");
if (r == true) {
    		image_name = $('.doc8'+ind).val();
			type = 'commetti';
			$.ajax({
				url: config.BASE_URL+'inquiries/delete_image/',
				type: "POST",
				data:{'image':image_name,'type':type},
				dataType: "html",
				success: function(msg){
				console.log(msg);
				if(msg)
				{
					
					$(".doclink"+ind).remove();
					$('.doc8'+ind).remove();
					$('.doc8remove'+ind).remove();
												
				}
				}
			});		
}
			
			
	}

</script>
</body>
</html>
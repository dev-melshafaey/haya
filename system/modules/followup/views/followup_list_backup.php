<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <?PHP //$this->load->view("common/globalfilter",array('type'=>'followup')); ?>
                <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"></div>
                  <table class="table table-bordered table-striped dataTable" id="motabiaTable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center; width: 75px;">رقم</th>
                        <th style="text-align:center;">اسم</th>
                        <th style="text-align:center; width: 105px;">البطاقة الشخصية</th>
                        <th style="text-align:center; width: 100px;">رقم الهاتف</th>
                        <th style="text-align:center;">طريقة إسناد الأعداد</th>        
                        <th style="text-align:center;">متوسط الايرادات</th>
                        <th style="text-align:center;">متوسط صافي الريح </th>                                                                                                                       
                        <th style="text-align:center; width: 94px;">الإجراءات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'followup/follow_list_new/'.$branchid,'columns_array'=>'{ "data": "رقم" },
					{ "data": "اسم" },
					{ "data": "البطاقة الشخصية" },
					{ "data": "رقم الهاتف" },
					{ "data": "طريقة إسناد الأعداد" },					
					{ "data": "متوسط الايرادات" },
					{ "data": "الريح صافي متوسط" },			
					{ "data": "الإجراءات"}')); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
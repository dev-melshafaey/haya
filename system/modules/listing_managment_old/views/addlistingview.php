<div class="row col-md-12">
  <form action="" method="POST" id="save_data_form1" name="save_data_form1">
    <input type="hidden" name="parent_id" id="parent_id" />
    <input type="hidden" name="list_type" id="inquiry_type" />
    <input type="hidden" id="list_id" name="list_id" value="<?php echo $list_id;?>" />
    <div class="form-group col-md-6">
      <label for="basic-input">قائمة الاسم</label>
      <input type="text" class="form-control req" name="list_name" id="list_name" placeholder="اسم القائمة" value="<?php echo (isset($single_list->list_name) ? $single_list->list_name : NULL);?>"/>
    </div>
    <?php if($segment == ''):?>
    <div class="form-group col-md-6">
      <label for="basic-input"> اكتب قائمة</label>
      <select name="list_type" id="list_type" class="form-control">
        <?php foreach($this->listing->get_all_list_type() as $listdata):?>
        <?php $typename = list_types($listdata->list_type);?>
        <option value="<?php echo $listdata->list_type;?>" <?php if($single_list->list_type	==	$listdata->list_type):?> selected="selected" <?php endif;?>><?php echo $typename['ar']; ?></option>
        <?php endforeach;?>
      </select>
    </div>
    <?php else:?>
    <div class="form-group col-md-6">
      <label for="basic-input">وضع</label>
      <select name="list_type" id="list_type" class="form-control">
        <?php if($segment == 'maritalstatus'):?>
        <option value="maritalstatus" <?php if($single_list->list_type	==	'maritalstatus'):?> selected="selected" <?php endif;?>>الحالة الزوجية</option>
        <?php endif;?>
        <?php if($segment == 'current_situation'):?>
        <option value="current_situation" <?php if($single_list->list_type	==	'current_situation'):?> selected="selected" <?php endif;?>>الوضع الحالي</option>
        <?php endif;?>
        <?php if($segment == 'inquiry_type'):?>
        <option value="inquiry_type" <?php if($single_list->list_type	==	'inquiry_type'):?> selected="selected" <?php endif;?>>الاستفسار عن قرض</option>
        <?php endif;?>
      </select>
    </div>
    <?php endif;?>
    <div class="form-group col-md-6">
      <label for="basic-input">الوضع</label>
      <select name="list_status" id="list_status" class="form-control">
        <option value="1" <?php if($single_list->list_status	==	'1'):?> selected="selected" <?php endif;?>>نشط</option>
        <option value="0" <?php if($single_list->list_status	==	'0'):?> selected="selected" <?php endif;?>>غير نشط</option>
      </select>
    </div>
  </form>
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" onclick="submit_form();" class="btn btn-success btn-lrg" name="save_data_form_new"  value="حفظ" />
  </div>
</div>

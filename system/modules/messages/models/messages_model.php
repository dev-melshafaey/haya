<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Messages_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
	
	public function listofusers()
	{
		$this->db->select("ah_userprofile.fullname,ah_userprofile.userid,system_messages.messagedate");
		$this->db->from("system_messages");
		$this->db->join("ah_userprofile","system_messages.senderid = ah_userprofile.userid");
		//$this->db->where("system_messages.senderid", $this->session->userdata('userid'));
		$this->db->where("system_messages.recevierid", $this->session->userdata('userid'));
		$this->db->group_by("ah_userprofile.userid");
		$this->db->order_by("system_messages.messagedate","desc");
		$q = $this->db->get();
		
		return $q->result();
	}
	
	public function alluser($name='')
	{
		$userid = $this->session->userdata('userid');
		$nameArray = split(' ',$name);
		$nameQuery = "SELECT userid, fullname FROM ah_userprofile WHERE userid!='".$userid."' AND ";
		if($nameArray[0]!='')
		{	$nameQuery .= " fullname LIKE '%".$nameArray[0]."%' AND "; 	}
		
		if($nameArray[1]!='')
		{	$nameQuery .= " fullname LIKE '%".$nameArray[1]."%' AND "; 	}
			$nameQuery .= " userid > 0 ORDER BY fullname ASC;";
			$q = $this->db->query($nameQuery);
			return $q->result();
	}
	
	public function loadChat($messageid,$senderid,$recevierid)
	{
		$chat = 0;
		if($messageid!='')
		{	$this->db->query("UPDATE `system_messages` SET `isviewed`='1', `viewdate`='".date('Y-m-d h:i:s')."' WHERE (`senderid`='".$senderid."' AND `recevierid`='".$recevierid."') OR (`recevierid`='".$senderid."' AND `senderid`='".$recevierid."')");	
/*			$firstQuery = $this->db->query("SELECT
								CONCAT(`sender_man`.`firstname`,' ',`sender_man`.`lastname`) AS sendername
								, `system_messages`.`messid`
								, `system_messages`.`replyid`
								, `system_messages`.`isviewed`
								, `system_messages`.`messagedate`
								, `system_messages`.`viewdate`
								, `system_messages`.`message`
								, CONCAT(`recevier_man`.`firstname`,' ',`recevier_man`.`lastname`) AS receviername
							FROM
								`system_messages`
								INNER JOIN `admin_users` AS `sender_man` ON (`system_messages`.`senderid` = `sender_man`.`id`)
								INNER JOIN `admin_users` AS `recevier_man` ON (`system_messages`.`recevierid` = `recevier_man`.`id`)
								WHERE `system_messages`.`messid`='".$messageid."';");
			foreach($firstQuery->result() as $fdata)
			{
			$chatArray[$chat] = array(
						'sendername'=>$fdata->sendername,
						'messid'=>$fdata->messid,
						'replyid'=>$fdata->replyid,
						'isviewed'=>$fdata->isviewed,
						'messagedate'=>$fdata->messagedate,
						'viewdate'=>$fdata->viewdate,
						'message'=>$fdata->message,
						'receviername'=>$fdata->receviername);
		}*/
		}
		$secondQuery = $this->db->query("SELECT
								CONCAT(`sender_man`.`fullname`) AS sendername
								, `system_messages`.`messid`
								, `system_messages`.`replyid`
								, `system_messages`.`isviewed`
								, `system_messages`.`messagedate`
								, `system_messages`.`viewdate`
								, `system_messages`.`message`
								, CONCAT(`recevier_man`.`fullname`) AS receviername
							FROM
								`system_messages`
								INNER JOIN `ah_userprofile` AS `sender_man` ON (`system_messages`.`senderid` = `sender_man`.`userid`)
								INNER JOIN `ah_userprofile` AS `recevier_man` ON (`system_messages`.`recevierid` = `recevier_man`.`userid`)
								WHERE (`system_messages`.`senderid`='".$senderid."' AND `system_messages`.`recevierid`='".$recevierid."') OR (`system_messages`.`recevierid`='".$senderid."' AND `system_messages`.`senderid`='".$recevierid."')
								ORDER BY `system_messages`.`messagedate` ASC;");
		foreach($secondQuery->result() as $sdata)
		{
			$chat++;
			$chatArray[$chat] = array(
						'sendername'=>$sdata->sendername,
						'messid'=>$sdata->messid,
						'replyid'=>$sdata->replyid,
						'isviewed'=>$sdata->isviewed,
						'messagedate'=>$sdata->messagedate,
						'viewdate'=>$sdata->viewdate,
						'message'=>$sdata->message,
						'receviername'=>$sdata->receviername);
		}
		
		
		
		return $chatArray;
	}
}

?>
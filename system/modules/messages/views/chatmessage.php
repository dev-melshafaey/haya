<div class="row">
  <form name="frmmessagelist" id="frmmessagelist" method="post" autocomplete="off">
    <input type="hidden" id="recevierid" name="recevierid" value="<?PHP echo $recevierid; ?>">
    <input type="hidden" id="senderid" name="senderid" value="<?PHP echo $senderid; ?>">
    <input type="hidden" id="messid" name="messid" value="<?PHP echo $messid; ?>">
    <div class="col-md-12">
      <div class="normaldiv" id="normaldiv">
        <?PHP 
	  $i=0;
	  foreach($messages as $mes) { ?>
        <div class="col-md-12 <?PHP if ($i % 2 == 0) {?>sender<?PHP } else { ?>recevier<?PHP } ?>">
          <label class="text-warning"><strong><?PHP echo $mes['sendername']; ?></strong> <span> : <?PHP echo arabic_date($mes['messagedate']); ?></span></label>
          <br>
          <?PHP echo $mes['message']; ?> </div>
        <?PHP 
	  $i++;
	  } ?>
      </div>
      <div class="col-md-12 sending">
        <input type="text" class="form-control req"  placeholder="رسالة" name="message" id="txtmessage">
      </div>
    </div>
  </form>
</div>
<script>
$(function(){
	$('#txtmessage').keypress(function (e) 
	{	
		var key = e.which;
		if(key == 13)
		{
			e.preventDefault();
			$('#frmmessagelist .req').removeClass('parsley-error');
			$('#frmmessagelist .req').each(function (index, element) {
				if ($.trim($(this).val()) == '') {
					$(this).addClass('parsley-error');
				}
			});
			var len = $('#frmmessagelist .parsley-error').length;
			if(len<=0)
			{
				var message_data = 	$('#frmmessagelist').serialize();		
				var request = $.ajax({
				  url: config.BASE_URL+'messages/add_new_message',
				  type: "POST",
				  data: message_data,
				  dataType: "html",
				  beforeSend: function()	{	$('#txtmessage').val(' '); },
				  success: function(msg)
				  {
					  $('#normaldiv').html(msg);	
				  }
				});
			}	
		}
	});	
});
</script>
<?php	$nameofCompany = $company->arabic_name.' | '.$company->english_name;?>
<div class="row col-md-12">
  <form class="mws-form" method="post" action="<?php echo current_url();?>" id="add_comp_doc" name="add_comp_doc" enctype="multipart/form-data">
    <input type="hidden" name="companyid" id="companyid" value="<?PHP echo $companyid; ?>">
    <input type="hidden" name="userid" id="userid" value="<?php echo $login_userid;?>"  />
    <input type="hidden" name="companyid" id="companyid" value="<?php echo $companyid;?>"  />
    <div class="col-md-6 form-group">
      <label class="text-warning">عنوان الوثيقة :</label>
      <input type="text" class="form-control req" name="documenttitle" id="documenttitle" value="<?php echo $company->documenttitle;?>">
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">وثيقة : </label>
      <input type="file" name="document" id="document" class="form-control req">
    </div>
    <div class="col-md-4 form-group">
      <label for="basic-input"></label>
      <input type="button" value="حفظ" name="submit" class="btn btn-success mws-login-button" onclick="submit_company_doc();">
    </div>
  </form>
</div>
<div class="row col-md-12">
  <table class="table table-bordered table-striped newbasicTable" aria-describedby="tableSortable_info">
    <thead>
      <tr role="row">
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">عنوان الوثيقة</th>
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
      </tr>
    </thead>
    <tbody role="alert" aria-live="polite" aria-relevant="all">
      <?php if(!empty($documentlist)):?>
      <?php foreach($documentlist as $list):?>
      <?php $actions   =	' <a class="fancybox-button" rel="gallery1" href="'.base_url().'resources/users/'.$list->userid.'/'.$list->document.'"><i class="icon-eye-open" data-hasqtip="9" aria-describedby="qtip-9"></i></a>';?>
      <?php $actions	.=	' <a onclick="button_delete(\''.trim($list->documentid).'\');" href="#_" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';?>
      <tr role="row" id="<?php echo $list->documentid.'_durar_lm';?>">
        <td style="text-align:center;"><?php echo $list->documenttitle;?></td>
        <td style="text-align:center;"><?php echo $actions;?></td>
      </tr>
      <?php unset($actions); endforeach;?>
      <?php endif;?>
    </tbody>
  </table>
</div>
<script>
$(function(){
	$('#t_heading').html('<?PHP echo $nameofCompany; ?>');
});

function button_delete(documentid)
{
	$.ajax({
	  url: config.BASE_URL + 'company/delete_company_doc/'+documentid,
	  type: "POST",
	  data: "",
	  dataType: "json",
	  success: function(msg)
	  {
		  $("#"+documentid+"_durar_lm").remove();
	  }
	});
}
	
function submit_company_doc()
{
	$('#add_comp_doc .req').removeClass('parsley-error');
    $('#add_comp_doc .req').each(function (index, element) {
        if ($.trim($(this).val()) == '') {
            $(this).addClass('parsley-error');
        }
    });
    var len = $('#add_comp_doc .parsley-error').length;

	if(len<=0)
	{

		
		var str_data = 	$('#add_comp_doc').serialize();
		var fd = new FormData(document.getElementById("add_comp_doc"));
		
		var request = $.ajax({
		  url: config.BASE_URL+'company/company_documents',
		  type: "POST",
		  data: fd,
		  enctype: 'multipart/form-data',
		  dataType: "html",
		  processData: false,  // tell jQuery not to process the data
          contentType: false ,  // tell jQuery not to set contentType
		  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(newrow)
		  {  
			 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
			 //$('#newbasicTable').dataTable().fnDestroy();
			 
			 $('.newbasicTable').append(newrow);
			 $('#ajax_action').hide();
			 
			 $("#documenttitle").val('');
			// create_data_table(1);
		  } 
		});
	}
	
/*	function saveBudgetForm()
	{
		$('#add_budget .req-code').removeClass('parsley-error');
	
		 var ht = '<ul>';
			$('#add_budget .req-code').each(function(index, element) {
				if($(this).val()=='')
				{
					$(this).addClass('parsley-error');
					ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
				}
			});
		  
		  var redline = $('#add_budget .parsley-error').length;
		  ht += '</ul>';
		  
		  if(redline <= 0)
		  {
			  var total_amount	=	$("#budget_amount").val();
			  var dept_count	=	0;
			  
			  $('.departments').each(function(index, element) {
				  
				if($(this).val()!='')
				{
					dept_count += Number($(this).val());
				}
			  });
			  
			  if(total_amount > dept_count || total_amount == dept_count)
			  {
				  var	sms_code	=	$("#sms_code").val();
				  var	email_code	=	$("#email_code").val();
	
				$.ajax({
				  url: config.BASE_URL + 'company/match_codes',
				  type: "POST",
				  data: "sms_code="+sms_code+"&email_code="+email_code,
				  dataType: "json",
				  success: function(msg)
				  {
					  mmm = msg;
					  if(msg.SMS	==	'1' && msg.EMAIL	==	'1')
					  {
						  document.getElementById("add_budget").submit();
					  }
					  else
					  {
						  $("#error-code").html('codes are Not matched.');
						  //alert('fasdfasdfasdfasd');
					  }
				  }
				});
				  
				 
			  }
			  else
			  {
				  show_notification_error_end('Your Budget Amount is Less than your departments Budget.');
			  }
				
		  }
		  else
		  {	
			show_notification_error_end(ht);	
		  }
	}*/
}
</script>
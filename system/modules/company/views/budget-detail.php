<div id="printable">
<div class="row" style="background-color:#FFF !important;direction:rtl !important;">
<div class="col-md-12 form-group">
  <div class="tab-pane list-group active">
    <div class="col-md-12 form-group">
      <div class="col-md-6 form-group">
        <label class="text-warning">سنة :</label>
        <?php echo date('Y',strtotime($budget_year)); ?> </div>
      <div class="col-md-6 form-group">
        <label class="text-warning">مجموع الميزانية :</label>
        <?php echo $budget_amount; ?> </div>
      <br clear="all" />
      <table class="table table-bordered table-striped dataTable">
        <tbody>
        <thead>
            <th><h4>قسم الميزانية</h4></th>
            <th><h4>ميزانية</h4></th>
            <th><h4>مكلفة</h4></th>
        </thead>
      <?php $departments	=	$this->company->get_dept_budget($budgetid);?>
      <?php $add_budget		=	0;?>
      
      <?php foreach($departments as $dept):?>
      <?php $add_budget +=	$dept->dept_budget;?>
          <tr>
            <td><?php echo $this->haya_model->get_name_from_list($dept->department)?></td>
            <td><?php echo $dept->dept_budget; ?></td>
            <td>00.00</td>
          </tr>
      <?php endforeach;?>
         <tr>
            <td><label class="text-warning">مجموع</label></td>
            <td><strong><?php echo number_format($add_budget,2); ?></strong></td>
            <td></td>
          </tr>
          <tr>
            <td><label class="text-warning">المتبقية</label></td>
            <td>
            <?php $remaining	=	($budget_amount-$add_budget);?>
            <strong><?php echo number_format($remaining,2); ?></strong>
            </td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
<div class="row" style="text-align:center !important;">
  <button type="button" id="" onClick="printthepage('printable');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
</div>
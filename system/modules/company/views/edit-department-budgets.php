<div class="row col-md-12">
  <form method="post" action="<?php echo current_url();?>" enctype="multipart/form-data" id="add_budget" name="add_budget">
      <input type="hidden" name="addedby" id="addedby" value="<?php echo $login_userid;?>"/>
      <input type="hidden" name="budgetid" id="budgetid" value="<?php echo $budgetid;?>"/>
      <input type="hidden" name="budget_year" id="budget_year" value="<?PHP echo $budget_year; ?>" />
	<div class="col-md-12 form-group" style="background-color: #FFF;
    padding-bottom: 3px;
    border: 1px solid #CCC;
    margin: 15px 16px;
    width: 97%;">
		 <h4 >سنة : <?PHP echo arabic_date($budget_year); ?></h4>
   </div>
    <div id="hide-form">
   <div class="col-md-6 form-group">
      <label class="text-warning">مجموع الميزانية :</label>
      <input type="text" class="form-control req NumberInput"  placeholder="مجموع الميزانية" name="budget_amount" id="budget_amount" value="<?php echo $budget_amount;?>">
    </div>
   <div class="col-md-6 form-group">
		<label class="text-warning">المتبقية :</label>
		
	<span id="dept-count" class="form-control" style="font-size: 16px; font-weight: bold; letter-spacing: 1px;">0</span>
   </div>
   <div class="col-md-12 form-group" style="background-color: #FFF;
    padding-bottom: 3px;
    border: 1px solid #CCC;
    margin: 15px 16px;
    width: 97%;">
		 <h4 >قسم الميزانية</h4>
   </div>
   
   
   <?php $departments	=	$this->haya_model->get_dropbox_list_value('departments');?>
   <?php foreach($departments as $dpt):?>
   <?php $explode	=	explode('_',$d_budget[$dpt->list_id]);?>

   <div class="col-md-4 form-group">
      <label class="text-warning"><?php echo $dpt->list_name?> :</label>
      <input type="text" class="form-control NumberInput departments req"  placeholder="<?php echo $dpt->list_name;?>" name="department_budget[<?php echo $explode[0];?>]" id="department_budget" value="<?php echo $explode[1];?>">
    </div>
   <?php endforeach;?>
    
    <br clear="all">
    <div class="col-md-12 form-group">
      <input type="button" value="حفظ العرض" onClick="show_codes();" name="match_codes" id="match_codes" class="btn btn-success mws-login-button">
    </div>
    </div>
   <br clear="all">
   <div id="codes" style="display:none;">
   <div class="col-md-6 form-group">
      <label class="text-warning">كود الرسائل القصيرة :</label>
      <input type="text" class="form-control req-code"  placeholder="كود الرسائل القصيرة" name="sms_code" id="sms_code" />
    </div>
       <div class="col-md-6 form-group">
      <label class="text-warning">كود البريد الإلكتروني :</label>
      <input type="text" class="form-control req-code"  placeholder="كود البريد الإلكتروني" name="email_code" id="email_code" />
    </div>
   
        <br clear="all">
    <div class="col-md-12 form-group newboxxxx">
      <input type="button" value="مباراة" onClick="saveBudgetForm();" name="submit_budget" id="submit_budget" class="btn btn-success mws-login-button">
    </div>
    <br clear="all">
    <div class="col-md-12 form-group" id="error-code"></div>
     </div>
  </form>
</div>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-0:+1",
		dateFormat:'yy-mm-dd',
		});
	});
</script>
<script>
function show_codes()
{
	$('#add_budget .req').removeClass('parsley-error');

	 var ht = '<ul>';
		$('#add_budget .req').each(function(index, element) {
			if($(this).val()=='')
			{
				$(this).addClass('parsley-error');
				ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
			}
		});
	  var redline = $('#add_budget .parsley-error').length;
	  ht += '</ul>';
	  if(redline <= 0)
	  {
		  var total_amount	=	$("#budget_amount").val();
		  var dept_count	=	0;
		  
		  $('.departments').each(function(index, element) {
		  	if($(this).val()!='')
			{
				dept_count += Number($(this).val());
			}
		  });
		  
		  if(total_amount > dept_count || total_amount == dept_count)
		  {
			  $("#hide-form").show();
			  
			  var budgetid		=	$('#budgetid').val();
			  
			$.ajax({
					  url: config.BASE_URL + 'company/add_codes',
					  type: "POST",
					  data: "budgetid="+budgetid,
					  dataType: "html",
					  success: function(msg)
					  {
						 $("#codes").show();
						 $("#hide-form").hide();
					  }
					});
		  }
		  else
		  {
			  show_notification_error_end('Your Budget Amount is Less than your departments Budget.');
		  }
			
	  }
	  else
	  {	
	  	show_notification_error_end(ht);	
	  }
}

function saveBudgetForm()
{
	$('#add_budget .req-code').removeClass('parsley-error');

	 var ht = '<ul>';
		$('#add_budget .req-code').each(function(index, element) {
			if($(this).val()=='')
			{
				$(this).addClass('parsley-error');
				ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
			}
		});
	  
	  var redline = $('#add_budget .parsley-error').length;
	  ht += '</ul>';
	  
	  if(redline <= 0)
	  {
		  var total_amount	=	$("#budget_amount").val();
		  var dept_count	=	0;
		  
		  $('.departments').each(function(index, element) {
			  
		  	if($(this).val()!='')
			{
				dept_count += Number($(this).val());
			}
		  });
		  
		  if(total_amount > dept_count || total_amount == dept_count)
		  {
			  var	sms_code	=	$("#sms_code").val();
			  var	email_code	=	$("#email_code").val();

			$.ajax({
			  url: config.BASE_URL + 'company/match_codes',
			  type: "POST",
			  data: "sms_code="+sms_code+"&email_code="+email_code,
			  dataType: "json",
			  success: function(msg)
			  {
				  mmm = msg;
				  if(msg.SMS	==	'1' && msg.EMAIL	==	'1')
				  {
					  document.getElementById("add_budget").submit();
				  }
				  else
				  {
					  $("#error-code").html('codes are Not matched.');
					  //alert('fasdfasdfasdfasd');
				  }
			  }
			});
			  
			 
		  }
		  else
		  {
			  show_notification_error_end('Your Budget Amount is Less than your departments Budget.');
		  }
			
	  }
	  else
	  {	
	  	show_notification_error_end(ht);	
	  }
}
$( ".departments" ).keyup(function() {
	
  var total_amount	=	$("#budget_amount").val();
  var dept_count	=	0;
  
  $('.departments').each(function(index, element) {
	if($(this).val()!='')
	{
		dept_count += Number($(this).val());
		
		var remaining	=	(total_amount-dept_count);
		$("#dept-count").html(remaining);
	}
  });
  
  if(total_amount < dept_count)
  {
	  show_notification_error_end('Your Budget Amount is Less than your departments Budget.');
  }
});
</script>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="nav nav-tabs panel panel-default panel-block">
          <div class="tab-pane list-group active">
           <?php if($this->haya_model->check_other_permission(157,'a')==1):?>
                 <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-right: 30px;margin-top: 10px;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url();?>company/add_budget" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
           <?php endif;?>
           <h1 align="center" style="color:#F00;"><?php echo $this->session->flashdata('msg');?></h1>
            <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th class="sorting">سنة</th>
                  <th class="sorting">مجموع الميزانية</th>
                  <th class="sorting">المتبقية</th>
                  <th class="sorting">الإجرائات</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'company/departmentbudgetlist/','columns_array'=>'
                {"data": "سنة"},
				{"data": "مجموع الميزانية"},
				{"data": "المتبقية"},
                {"data": "الإجرائات" }')); ?>
<!-- /.modal-dialog -->
</div>
</body>
</html>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12 form-group">
      <form name="frm_company_register" id="frm_company_register"  method="post" autocomplete="off">
      <input type="hidden" id="companyid" name="companyid" value="<?PHP echo $company->companyid; ?>">
        <div class="nav nav-tabs panel panel-default panel-block">
          <div class="tab-pane list-group active">
          	<div class="col-md-8 form-group" style="margin-top: 10px; border-left: 1px solid #EFEFEF;">
          		<div class="col-md-4 form-group">
                	<label class="text-warning">نوع الشركة :</label>
                    <?PHP echo $this->haya_model->create_dropbox_list('company_type_listmanagement','company_type',$company->company_type_listmanagement,0,'req'); ?>
                </div>
                <div class="col-md-4 form-group">
                	<label class="text-warning">شركة المنشأ :</label>
                    <?PHP $this->haya_model->enum_dropbox('companyorigin',$company->companyorigin,'ah_company','companyorigin','شركة المنشأ'); ?>
                </div>
                 <div class="col-md-4 form-group">
                	<label class="text-warning">البلد :</label>
                    <?php echo $this->haya_model->create_dropbox_list('country_listmanagement','issuecountry',$company->country_listmanagement,0,''); ?>
                </div>
                <div class="col-md-6 form-group">
                	<label class="text-warning">رقم السجل التجاري :</label>
                    <?PHP if($company->companyid!='') { ?>
                    <br>
                    <input type="hidden" id="cr_number" name="cr_number" value="<?PHP echo($company->cr_number); ?>">
                    <strong><?PHP echo(arabic_date($company->cr_number)); ?></strong>
                    <?PHP } else { ?>
                    <input name="cr_number" value="<?PHP echo($company->cr_number); ?>" placeholder="رقم السجل التجاري" id="cr_number" type="text" class="form-control req NumberInput">
                    <?PHP } ?>
                </div>
                <div class="col-md-6 form-group">
                	<label class="text-warning">تاريخ الانتهاء :</label>
                    <input name="cr_expiry" value="<?PHP echo($company->cr_expiry); ?>" placeholder="تاريخ الانتهاء" id="cr_expiry" type="text" class="form-control req">
                </div>
                 <div class="col-md-12 form-group">
                	<label class="text-warning">الإسم باللغة الإنجليزية :</label>
                    <input name="english_name" value="<?PHP echo($company->english_name); ?>" placeholder="الإسم باللغة الإنجليزية" id="english_name" type="text" class="form-control req">
                </div>
                 <div class="col-md-12 form-group">
                	<label class="text-warning">الإسم باللغة العربية :</label>
                    <input name="arabic_name" value="<?PHP echo($company->arabic_name); ?>" placeholder="الإسم باللغة العربية" id="arabic_name" type="text" class="form-control req">
                </div>
                <div class="col-md-6 form-group">
                	<label class="text-warning">المحافظة :</label>
                    <?PHP echo $this->haya_model->create_dropbox_list('province','regions',$company->province,0,'req'); ?>
                </div>
                <div class="col-md-6 form-group">
                	<label class="text-warning">الولاية :</label>
                    <?PHP echo $this->haya_model->create_dropbox_list('wilaya','wilaya',$company->wilaya,$company->province,'req'); ?>
                </div>
                <div class="col-md-12 form-group">
                	<label class="text-warning">العنوان بالتفصيل :</label>
                    <textarea id="fulladdress" name="fulladdress" placeholder="العنوان بالتفصيل" class="form-control req"><?PHP echo $company->fulladdress; ?></textarea>
                </div>                
                <div class="col-md-12 form-group">
                	<label class="text-warning">الحالة :</label>
                    <?PHP company_status('companystatus',$company->companystatus); ?>
                </div>
                <div class="col-md-12 form-group">
                	<button type="button" id="save_company_information" class="btn btn-success">اضف شركة</button>
                </div>
          	</div>
            <div class="col-md-4 form-group" style="margin-top: 10px;">
            	<div class=col-md-12 form-group">
				  <label class="text-warning">قسم الشركة :</label>
				  <div class="col-md-12" style="overflow-y: scroll; overflow-x: hidden; height: 150px;">
					<table class="table table-bordered table-striped dataTable">
					  <tbody>
						  <?php
							  $supplier_types	=	$this->haya_model->get_dropbox_list_value('supplier');?>
						  <?php foreach($supplier_types as $type):?>
						<tr>
						  <td style="text-align: center !important;"><input type="checkbox" <?PHP if($own_supplier_types[$type->list_id]==$type->list_id) { ?> checked <?PHP } ?>  name="supplier_type_id[]" class="checkoption" value="<?php echo $type->list_id;?>"/></td>
						  <td><?php echo $type->list_name;?></td>
						</tr>
						<?php endforeach;?>
					  </tbody>
					</table>
				  </div>
				</div>
				<div class="col-md-12 form-group" style="margin-top:25px;">
                	<label class="text-warning">الشخص الذي يمكن الاتصال به :</label>
                    <input name="contact_person" value="<?PHP echo($company->contact_person); ?>" placeholder="الشخص الذي يمكن الاتصال به" id="contact_person" type="text" class="form-control req">
                </div>
                <div class="col-md-12 form-group">
                	<label class="text-warning">البريد الإلكتروني :</label>
                    <input name="email_address" value="<?PHP echo($company->email_address); ?>" placeholder="البريد الإلكتروني" id="email_address" type="text" class="form-control req">
                </div>
                <div class="col-md-12 form-group">
                	<label class="text-warning">نوع البطاقة :</label>
                    <?PHP card_type_id('contact_person_id_type',$company->contact_person_id_type); ?>
                </div>
                <div class="col-md-12 form-group">
                	<label class="text-warning">رقم البطاقة :</label>
                    <input name="contact_person_id" value="<?PHP echo($company->contact_person_id); ?>" placeholder="رقم البطاقة" id="contact_person_id" type="text" class="form-control req NumberInput">
                </div>
            	<div class="col-md-12 form-group">
                	<label class="text-warning">بطاقة انتهاء الصلاحية :</label>
                    <input name="contact_person_expiry" value="<?PHP echo($company->contact_person_expiry); ?>" placeholder="بطاقة انتهاء الصلاحية" id="contact_person_expiry" type="text" class="form-control req">
                </div>
                <div class="col-md-12 form-group">
               	  <label class="text-warning">الهاتف النقال :</label>
                    <input name="mobile_number" type="text" class="form-control req NumberInput" id="mobile_number" placeholder="الهاتف النقال" value="<?PHP echo($company->mobile_number); ?>" maxlength="8">
                </div>
                <div class="col-md-6 form-group">
               	  <label class="text-warning">رقم الهاتف :</label>
                    <input name="phone_number" type="text" class="form-control req NumberInput" id="phone_number" placeholder="رقم الهاتف" value="<?PHP echo($company->phone_number); ?>" maxlength="8">
                </div>
                <div class="col-md-6 form-group">
                	<label class="text-warning">رقم الهاتف :</label>
                    <input name="phone_number_two" value="<?PHP echo($company->phone_number_two); ?>" placeholder="رقم الهاتف" id="phone_number_two" type="text" class="form-control req NumberInput">
                </div>
                <div class="col-md-6 form-group">
               	  <label class="text-warning">ص.ب :</label>
                    <input name="postboxnumber" type="text" class="form-control req NumberInput" id="postboxnumber" placeholder="ص.ب" value="<?PHP echo($company->postboxnumber); ?>" maxlength="8">
                </div>
                <div class="col-md-6 form-group">
               	  <label class="text-warning">ر.ب :</label>
                    <input name="postcode" type="text" class="form-control req NumberInput" id="postcode" placeholder="ر.ب" value="<?PHP echo($company->postcode); ?>" maxlength="8">
                </div>
            </div>
          </div>
        </div>
      </form>  
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
	$(function(){
		$('#companyorigin').change(function(){
			if($(this).val()=='شركة غير محلية')
			{	$('#country_listmanagement').addClass('req'); }
			else
			{	$('#country_listmanagement').removeClass('req'); }
		});	
	});
</script>
</div>
</body>
</html>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Company_model extends CI_Model {

	function __construct()
    {
        parent::__construct();
		$this->_login_userid = $this->session->userdata('userid');
    }
	
	function company_detail($id=0,$crnumber=0)
	{
		if($id!=0 || $crnumber!=0)
		{
			$this->db->from('ah_company');
			if($id!='' && $id!=0)
			{
				$this->db->where('companyid',$id);
			}
			if($crnumber!='' && $crnumber!=0)
			{
				$this->db->where('cr_number',$crnumber);
			}
				$query = $this->db->get();
				return $query->row(); 
		}
	}
	
	public function get_company_info($companyid)
	{
		$this->db->select('ah_company.companyid, ah_company.english_name, ah_company.arabic_name, ah_company_document.documentdate, ah_company_document.documentid, ah_company_document.documenttitle, ah_company_document.document');
		$this->db->from('ah_company');
		$this->db->join('ah_company_document',"ah_company_document.companyid = ah_company.companyid",'left');		
		$this->db->where('ah_company.companyid',$companyid);
		$this->db->where('ah_company.delete_record','0');
		$query = $this->db->get();
		
		return $query->row();
	}
	
	public function get_company_documentlist($companyid)
	{
		$this->db->select('*');
		$this->db->from('ah_company_document');		
		$this->db->where('companyid',$companyid);
		$this->db->where('delete_record','0');
		$query = $this->db->get();
		
		return $query->result();
	}	
	
	function getCompanyName($like,$searchcolumn)
	{
		$this->db->select('companyid,english_name,arabic_name,cr_number');
		$this->db->from('ah_company');
		$this->db->like($searchcolumn,$like);
		$query = $this->db->get();
		return $query->result();
	}
	
//-------------------------------------------------------------------
	/*
	 * Get Documents Detail BY Docid
	 * Return OBJECT
	 */
		
	function get_all_customers()
	{
		$this->db->select('customer_id,userid,customer_name,company_name,customer_title,address,postal_code,province,wilaya,delete_record');
		$query = $this->db->get('ah_customers');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Documents Detail BY Docid
	 * Return OBJECT
	 */
		
	function get_single_customer($customer_id)
	{
		$this->db->where('customer_id',$customer_id);		
		$query = $this->db->get('ah_customers');		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}	
//----------------------------------------------------------------------

	/*
	* Get all Data 
	* return OBJECT
	*/

	function update_customer($customer_id,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('customer_id',$customer_id);
		$this->db->update('ah_customers',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
	
//-------------------------------------------------------------------
	/*
	* Add List 
	* @param $data ARRAY
	* return TRUE
	*/

	function add_customer($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_customers',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Delete Branch
	 * @param $branchid integer
	 * Return TRUE
	 */
		
	function delete_customer($customer_id)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','customer_id'	=>	$customer_id));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('customer_id',$customer_id);
		$this->db->update('ah_customers',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	* Add List Document
	* @param $data ARRAY
	* return TRUE
	*/

	function add_document($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_company_document',$data,$json_data,$this->session->userdata('userid'));
		
		//print_r($this->db->last_query());exit();
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	 * Delete Company Document
	 * @param $branchid integer
	 * Return TRUE
	 */
		
	function delete_company_doc($documentid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','documentid'	=>	$documentid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('documentid',$documentid);

		$this->db->update('ah_company_document',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function allCategory($parent_id=0)
	{
		$this->db->select('list_id,list_name');
		$this->db->where('list_type','item');
		$this->db->where('list_parent_id',0);
		$query = $this->db->get('ah_listmanagement');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	function allSubCategory($parent_id=0)
	{
		$this->db->select('list_id,list_name');
		$this->db->where('list_type','item');
		$this->db->where('list_parent_id',$parent_id);
		$query = $this->db->get('ah_listmanagement');
		$html = '';
		
		foreach($query->result() as $as)
		{
			$html .= '<option value="'.$as->list_id.'">'.$as->list_name.'</option>';	
		}
		echo $html;
	}
//----------------------------------------------------------------------

	/*
	* GET All Quotation Records
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_all_quotations()
	{
		$this->db->select('quoteid,addedby,branch_id,dept_id,quote_title,quote_detail,quote_attachment,quote_status,quote_data,budget_amount,expiry_date,delete_record');
		$this->db->where('delete_record','0');
		
		$query = $this->db->get('ah_quote');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* GET Single Quotation Record
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_quotation_by_id($quoteid)
	{
		$this->db->select('quoteid,addedby,branch_id,dept_id,quote_title,quote_detail,quote_attachment,quote_status,quote_data,expiry_date,budget_amount,delete_record');
		$this->db->where('delete_record','0');
		$this->db->where('quoteid',$quoteid);
		
		$query = $this->db->get('ah_quote');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------

	/*
	* UPDATE Quotation
	* return OBJECT
	*/

	function update_quote($quoteid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('quoteid',$quoteid);
		$this->db->update('ah_quote',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Add Quotation 
	* @param $data ARRAY
	* return TRUE
	*/

	function add_quote($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_quote',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Delete Quotation
	 * @param $quoteid integer
	 * Return TRUE
	 */
		
	function delete_quotation($quoteid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','quoteid'	=>	$quoteid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('quoteid',$quoteid);

		$this->db->update('ah_quote',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	* Add Company Quotation 
	* @param $data ARRAY
	* return last Inserted Record ID
	*/

	function add_company_quotation($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_quote_company',$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
	
//-------------------------------------------------------------------
	/*
	* Add Quotation Item 
	* @param $data ARRAY
	* return TRUE
	*/

	function add_quotation_item($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_quote_items',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Search Quotations
	* @param $string string
	* return JSON
	*/
	function get_quotation_search($string)
	{
		$this->db->select('quoteid,quote_title,budget_amount');
		$this->db->like('quote_title', $string);
		$this->db->or_like('quoteid', $string);
		//$this->db->where('expiry_date < ',"NOW()");
		
		$query = $this->db->get('ah_quote');
		
		if($query->num_rows > 0)
		{
		  foreach ($query->result_array() as $row)
		  {
			$new_row['lable']	=	$row['quote_title'];
			$new_row['value']	=	$row['quote_title'];
			$new_row['id']		=	$row['quoteid'];
			$new_row['amount']	=	$row['budget_amount'];

			$row_set[] = $new_row; //build an array
		  }
		  
		  echo json_encode($row_set); //format the array into json data
		}
  	}

//-------------------------------------------------------------------
	/*
	* Search Company Quotations
	* @param $string string
	* return JSON
	*/	
	public function search_company($string)
	{
		$this->db->select('companyid,english_name,arabic_name,cr_number');
		$this->db->like('english_name', $string);
		$this->db->or_like('arabic_name', $string);
		$this->db->or_like('companyid', $string);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get('ah_company');
		
		if($query->num_rows > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$new_row['lable']	=	$row['english_name'].'|'.$row['arabic_name'].'('.$row['cr_number'].')';
				$new_row['value']	=	$row['english_name'].'|'.$row['arabic_name'].'('.$row['cr_number'].')';
				$new_row['id']		=	$row['companyid'];
				
				$row_set[] = $new_row; //build an array
			 }
			  
			 echo json_encode($row_set); //format the array into json data
		}
	}
//----------------------------------------------------------------------

	/*
	* GET Single Quotation Record
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_quotation_amount($quoteid)
	{
		$this->db->select('budget_amount');
		$this->db->where('quoteid',$quoteid);
		
		$query = $this->db->get('ah_quote');
		
		if($query->num_rows() > 0)
		{
			echo $query->row()->budget_amount;
		}
	}
//----------------------------------------------------------------------

	/*
	* GET All Quotation Records
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_company_quotations($quoteid)
	{
		$coloumn	=	NULL;
		
		if($quoteid)
		{
			$string	=	'WHERE ah_quote.quoteid='.$quoteid.'';
			$coloumn	=	', COUNT(ah_quote_company.companyid) AS tot_comp';
		}
		
		/*$query = $this->db->query("
		SELECT
			`ah_company`.`cr_number`
			, `ah_company`.`english_name`
			, `ah_company`.`arabic_name`
			, `ah_company`.`companyid`
			, `ah_quote_company`.`quote_title`
			, `ah_quote_1`.`addedby`
			, `ah_quote_1`.`quote_title`
			, `ah_quote_1`.`budget_amount`
			, `ah_quote_1`.`quoteid`
			, `ah_quote_company`.`comp_quoteid`
		FROM
			`alhaya`.`ah_quote`, 
			`alhaya`.`ah_company`
		INNER JOIN `alhaya`.`ah_quote_company` 
			ON (`ah_company`.`companyid` = `ah_quote_company`.`companyid`)
		INNER JOIN `alhaya`.`ah_quote` AS `ah_quote_1` 
			ON (`ah_quote_1`.`quoteid` = `ah_quote_company`.`quoteid`)  GROUP BY `ah_company`.`companyid`;");*/
			
			
	$query = $this->db->query("SELECT
		`ah_quote_company`.`comp_quoteid`
	   ,`ah_quote`.`quoteid`
		,`ah_quote`.`quote_title`
		, `ah_quote`.`budget_amount`
		, `ah_quote`.`expiry_date`
		, COUNT(`ah_quote_company`.`companyid`) AS tot_comp
	FROM
		`alhaya`.`ah_quote`
		INNER JOIN `alhaya`.`ah_quote_company` 
			ON (`ah_quote`.`quoteid` = `ah_quote_company`.`quoteid`) ".$string." GROUP BY `ah_quote`.`quoteid`;");
		
		
		//print_r($this->db->last_query());	
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* GET All Quotation Records
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_companies_by_quotation($quoteid)
	{
		$query = $this->db->query("
			SELECT
				`ah_company`.`cr_number`
				, `ah_company`.`english_name`
				, `ah_company`.`arabic_name`
				, `ah_company`.`companyid`
				, `ah_quote_company`.`quote_data`
				, `ah_quote_company`.`quote_title`
				, `ah_quote_1`.`addedby`
				, `ah_quote_1`.`quote_title`
				, `ah_quote_1`.`budget_amount`
				, `ah_quote_1`.`quoteid`
				, `ah_quote_company`.`comp_quoteid`
			FROM
				`alhaya`.`ah_quote`, 
				`alhaya`.`ah_company`
			INNER JOIN `alhaya`.`ah_quote_company` 
				ON (`ah_company`.`companyid` = `ah_quote_company`.`companyid`)
			INNER JOIN `alhaya`.`ah_quote` AS `ah_quote_1` 
				ON (`ah_quote_1`.`quoteid` = `ah_quote_company`.`quoteid`) WHERE ah_quote_company.quoteid=".$quoteid." GROUP BY `ah_company`.`companyid`;");
				
			
			//print_r($this->db->last_query());	
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
	}
		
//----------------------------------------------------------------------

	/*
	* GET All Company Quotation and Item Detail Records
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_company_quote_info($quoteid)
	{
		$query_a = $this->db->query("
			SELECT
				`ah_company`.`cr_number`
				, `ah_company`.`english_name`
				, `ah_company`.`arabic_name`
				, `ah_company`.`companyid`
				, `ah_quote_company`.`quote_data`
				, `ah_quote_company`.`quote_title`
				, `ah_quote_1`.`addedby`
				, `ah_quote_1`.`quote_title`
				, `ah_quote_1`.`budget_amount`
				, `ah_quote_1`.`quoteid`
				, `ah_quote_company`.`comp_quoteid`
			FROM
				`alhaya`.`ah_quote`, 
				`alhaya`.`ah_company`
			INNER JOIN `alhaya`.`ah_quote_company` 
				ON (`ah_company`.`companyid` = `ah_quote_company`.`companyid`)
			INNER JOIN `alhaya`.`ah_quote` AS `ah_quote_1` 
				ON (`ah_quote_1`.`quoteid` = `ah_quote_company`.`quoteid`) WHERE ah_quote_company.quoteid=".$quoteid." GROUP BY `ah_company`.`companyid`;");
				
			
			if($query_a->num_rows() > 0)
			{
				 $data['company_data']	=	$query_a->row();
			}
			
		$query_b = $this->db->query("SELECT * FROM ah_quote_items WHERE quoteid=".$query_a->row()->comp_quoteid.";");
			
		if($query_b->num_rows() > 0)
		{
			 $data['items']	= $query_b->result();
		}
		
		return $data;
	}
//----------------------------------------------------------------------

	/*
	* GET All Budget
	* return OBJECT
	*/	
	function all_budgets()
	{
		$this->db->select('budgetid,budget_amount,budget_year,delete_record');

		$query = $this->db->get('total_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	* Add Total Budget
	* @param $data ARRAY
	* return TRUE
	*/

	function add_budget($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('total_budget',$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	* Add Department Budget
	* @param $data ARRAY
	* return TRUE
	*/

	function add_dept_budget($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('departments_budget',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* GET Single Budget BY ID
	* return OBJECT
	*/	
	function get_single_budget($budgetid)
	{
		$this->db->select('budgetid,budget_amount,budget_year,delete_record');
		$this->db->where('budgetid',$budgetid);

		$query = $this->db->get('total_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------

	/*
	* GET Department Budget by Yearly budget ID
	* return OBJECT
	*/	
	function get_d_budget_by_tb_id($budgetid)
	{
		$this->db->select('deptid,department,dept_budget');
		$this->db->where('budgetid',$budgetid);

		$query = $this->db->get('departments_budget');
		
		$deartment_values	=	array();
		
		if($query->num_rows() > 0)
		{
			$result	=	$query->result();

			foreach($result	as	$value)
			{
				$deartment_values[$value->department]	=	$value->deptid.'_'.$value->dept_budget;
			}

			return $deartment_values;
		}
	}
//----------------------------------------------------------------------	
	function get_budget_maxyear()
	{
		$query = $this->db->query('SELECT MAX(budget_year)+1 AS mybug FROM total_budget');
		return $query->row()->mybug;
	}
//----------------------------------------------------------------------

	/*
	* GET Department Budget BY ID
	* return OBJECT
	*/	
	function get_dept_budget($budgetid)
	{
		$this->db->select('deptid,budgetid,department,dept_budget');
		$this->db->where('budgetid',$budgetid);

		$query = $this->db->get('departments_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* GET Department Budget BY ID
	* return OBJECT
	*/	
	function dept_budget_sum($budgetid)
	{
		$query = $this->db->query("SELECT SUM(dept_budget) AS total FROM `departments_budget` WHERE budgetid='".$budgetid."'");
		
		if($query->num_rows() > 0)
		{
			return $query->row()->total;
		}
	}
	
//----------------------------------------------------------------------

	/*
	* GET Single Quotation Record
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_companies_email($dept_id)
	{
		$this->db->select('companyid');
		$this->db->where('departmentid',$dept_id);
		
		$this->db->group_by("companyid"); 
		
		$query = $this->db->get('ah_company_category');
		
		if($query->num_rows() > 0)
		{
			$result	=	 $query->result();

			foreach($result as $res)
			{
				if($this->get_email($res->companyid))
				{
					$data[]	=	$this->get_email($res->companyid);
				}
				
			}
			
			return $data;
		}
	}
	
//----------------------------------------------------------------------

	/*
	* GET Single Quotation Record
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_email($companyid)
	{
		$this->db->select('companyid,branchid,email_address,arabic_name,contact_person');
		$this->db->where('companyid',$companyid);
			
		$query = $this->db->get('ah_company');
		
		if($query->num_rows() > 0)
		{
			return $result	=	 $query->row();
		}
	}
//----------------------------------------------------------------------

	/*
	* GET Single Budget BY ID
	* return OBJECT
	*/	
	function check_yearly_budget($year)
	{
		//$year	=	date("Y");
		
		$this->db->where('YEAR(budget_year)',$year);

		$query = $this->db->get('total_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	* Edit Total Budget
	* @param $data ARRAY
	* return TRUE
	*/

	function edit_yearly_budget($budgetid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('budgetid',$budgetid);
		$this->db->update('total_budget',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Edit Department Budget
	* @param $data ARRAY
	* return TRUE
	*/

	function edit_dept_budget($deptid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('deptid',$deptid);
		$this->db->update('departments_budget',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Update SMS/EMAIL Codes
	* @param $budgetid integer
	* @param $data ARRAY
	* return TRUE
	*/

	function update_codes($budgetid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('budgetid',$budgetid);
		$this->db->update('total_budget',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* Check If SMS CODE EXIST
	* return OBJECT
	*/	
	function sms_code_exist($sms_code)
	{
		$this->db->where('sms_code',$sms_code);

		$query = $this->db->get('total_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->sms_code;
		}
	}
//----------------------------------------------------------------------

	/*
	* Check If EMAIL CODE EXIST
	* return OBJECT
	*/	
	function email_code_exist($email_code)
	{
		$this->db->where('email_code',$email_code);

		$query = $this->db->get('total_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->email_code;
		}
	}
//-------------------------------------------------------------------
}
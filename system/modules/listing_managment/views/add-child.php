<div class="row col-md-12">
  <form action="" method="POST" id="add_parent" name="add_parent" autocomplete="off">
    <input type="hidden" id="parent_id" name="parent_id" value="<?php echo $parent_id; ?>"/>
    <input type="hidden" id="list_type" name="list_type" value="<?php echo $type; ?>"/>
    <input type="hidden" id="list_id" name="list_id" value="<?php echo isset($child_data->list_id) ? $child_data->list_id : NULL;?>"/>
    <div class="form-group col-md-6">
      <label for="basic-input">إسم</label>
      <input type="text" class="form-control req" name="add_sub"  id="add_sub"  placeholder="إسم" value="<?php echo isset($child_data->list_name) ? $child_data->list_name : NULL;?>"/>
    </div>
    <div class="form-group col-md-3">
      <label for="basic-input">ترتيب&lrm;‎</label>
      <input type="text" name="list_order" id="list_order" class="form-control" value="<?php echo isset($child_data->list_order) ? $child_data->list_order : NULL;?>"/>
    </div>
  </form>
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" onClick="save_qaima_child_data();" class="btn btn-success btn-lrg" name="save_data_form_new"  value="حفظ" />
  </div>
</div>
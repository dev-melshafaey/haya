<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>

<?php $segment = $this->uri->segment(3); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <p>
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <?PHP if($permissions[$module['moduleid']]['a']	==	1) { ?><div class="row table-header-row"> <a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url();?>listing_managment/add_required_document" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div><?PHP } ?>
                  <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
				  <?PHP foreach($doclist as $doc) {  ?>
                  
                  <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                    <div class="panel-heading" id="head<?PHP echo $applicant_doc_index;?>">
                      <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $doc->list_id; ?>"><?PHP echo $doc->list_name; ?> (<?PHP echo $this->haya_model->dataCount('ah_document_required','charity_type_id',$doc->list_id,'COUNT'); ?>)</a> </h4>
                    </div>
                    <div id="demo-collapse<?PHP echo $doc->list_id; ?>" class="panel-collapse collapse">
                      <div class="panel-body" style="text-align:right;">                       
                        <?PHP foreach($this->listing->get_all_document_list($doc->list_id) as $lmm) { ?>
                        	<li class="doc_list"><?PHP echo $lmm->documenttype; ?>                            
                            <div><a class="mytooltip" data-title="تعديل" href="#addingDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>listing_managment/add_required_document/<?PHP echo $lmm->documentid; ?>"><i class="icon-pencil"></i></a> <?PHP getStatus($lmm->documentstatus); ?></div>
                            </li>
                        <?PHP } ?>
						
                      </div>
                    </div>
                  </div>
                  <?PHP } ?>
                  </div> 
                  
                </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php $this->load->view('common/footer'); ?>
<!-- /.modal-dialog -->
</div>
</body>
</html>
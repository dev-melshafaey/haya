<?php $labels	=	$this->config->item('list_types');?>
<div class="row col-md-12">
  <form action="<?php echo current_url();?>" method="POST" id="validate_form" name="validate_form">
  <input type="hidden" id="list_id" name="list_id" value="<?php echo $list_id;?>" />
  <div class="form-group col-md-6">
    <label for="basic-input">قائمة الاسم</label>
    <input type="text" class="form-control req" name="list_name" id="list_name" placeholder="قائمة الاسم" value="<?php echo (isset($single_list->list_name) ? $single_list->list_name : NULL);?>"/>
  </div>
  <div class="form-group col-md-3">
    <label for="basic-input"> اكتب قائمة</label>
    <select name="list_type" id="list_type" class="form-control">
          <?php foreach($list_types as $listdata):?>
                <option value="<?php echo $listdata->list_type;?>" <?php if($single_list->list_type	==	$listdata->list_type):?> selected="selected" <?php endif;?>><?php echo $labels[$listdata->list_type]['en'];?></option>
			<?php endforeach;?>

      </select>
  </div>
  <div class="form-group col-md-3">
    <label for="basic-input">وضع</label>
    <select name="list_status" id="list_status">

                    <option value="1" <?php if($single_list->list_status	==	'1'):?> selected="selected" <?php endif;?>>نشط</option>

                    <option value="0" <?php if($single_list->list_status	==	'0'):?> selected="selected" <?php endif;?>>دي نشط</option>

                  </select>
  </div>
  <div class="form-group col-md-6">
    <label for="basic-input">نسبة الرسوم</label>
    <input type="text" class="form-control req NumberInput" value="<?php echo $d->loan_percentage; ?>" name="loan_percentage"  id="loan_percentage"  placeholder="نسبة الرسوم"/>
  </div>
  <div class="form-group col-md-3">
    <label for="basic-input">فترة السماح‎</label>
    <?PHP youmdropbox('loan_start_timeperiod',$d->loan_start_timeperiod); ?>
    </div>
  <div class="form-group col-md-3">
    <label for="basic-input">مدة السداد (سنة)</label>
    <?PHP youmdropbox('loan_expire_timeperiod',$d->loan_expire_timeperiod); ?>
    
  </div>
  <div class="form-group col-md-6">
    <label for="basic-input">المساهمة الشخصية</label>
    <input type="text" class="form-control req NumberInput" value="<?php echo $d->loan_applicant_percentage; ?>" name="loan_applicant_percentage"  id="loan_applicant_percentage"  placeholder="المساهمة الشخصية"/>
  </div>
  
  </form>
</div>
<div class="row col-md-12">
<div class="form-group  col-md-12">
      <input type="button" onClick="save_barnamij_data();" class="btn btn-success btn-lrg" name="save_data_form_new"  value="حفظ" />
  </div>
</div>
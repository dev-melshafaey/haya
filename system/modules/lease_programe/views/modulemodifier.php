<?PHP
	$d = $mi[0];	
 ?>

<div class="row col-md-12">
  <form action="" autocomplete="off" method="post" id="update_module" name="update_module" autocomplete="off">
  <input type="hidden" id="moduleid" name="moduleid" value="<?php echo $d->moduleid; ?>"/>
  <input type="hidden" id="module_icon" name="module_icon" value="<?PHP echo $d->module_icon; ?>" />
  <div class="form-group col-md-6">
    <label for="basic-input">اسم وحدة</label>
    <input type="text" class="form-control req" value="<?php echo $d->module_name; ?>" name="module_name"  id="module_name"  placeholder="اسم وحدة"/>
  </div>
  <div class="form-group col-md-6">
    <label for="basic-input">وحدة التفاصيل</label>
    <input type="text" class="form-control req" value="<?php echo $d->module_text; ?>" name="module_text"  id="module_text"  placeholder="وحدة التفاصيل"/>
  </div>
  <div class="form-group col-md-6">
    <label for="basic-input">وحدة الأم</label>
    <?PHP $this->haya_model->module_dropbox('module_parent',$d->module_parent); ?>
  </div>
  <div class="form-group col-md-6">
    <label for="basic-input">عرض على القائمة</label>
    <?PHP show_on_menu('show_on_menu',$d->show_on_menu); ?>
  </div>
  <div class="form-group col-md-3">
    <label for="basic-input">وحدة ترتيب‎</label>
    <?PHP number_drop_box('module_order',$d->module_order); ?>
  </div>
  <div class="form-group col-md-3">
    <label for="basic-input">الظهور في القائمة اليمين</label>
    <?PHP show_on_menu('show_in_right',$d->show_in_right); ?>
  </div>
  <div class="form-group col-md-2">
    <label for="basic-input">حالة وحدة</label>
    <select name="module_status" id="module_status" class="form-control">
      <option value="A" <?php if($d->module_status	==	'A'):?> selected="selected" <?php endif;?>>نشط</option>
      <option value="D" <?php if($d->module_status	==	'D'):?> selected="selected" <?php endif;?>>دي نشط</option>
    </select>
  </div>
  <div class="form-group col-md-2">
    <label for="basic-input">لون الخلفية</label>
    <input type="color" class="form-control" name="module_background" id="module_background" value="<?PHP echo $d->module_background; ?>">
  </div>
  <div class="form-group col-md-2">
    <label for="basic-input">لون الأمامية</label>
   <input type="color" class="form-control" name="module_foreground" id="module_foreground" value="<?PHP echo $d->module_foreground; ?>">
  </div>
  <div class="form-group col-md-12" style="float: left;">
    <label for="basic-input">وحدة أيقونة</label>
    <div class="list-group">
      <div class="list-group-item" id="web-application-icons">
        <div class="form-group" style="max-height: 200px; overflow-y: scroll; overflow-x: hidden;">
          <section class="icons-demo">
            <div class="the-icons row">
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-adjust"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-anchor"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-archive"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-asterisk"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-ban-circle"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-bar-chart"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-barcode"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-beaker"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-beer"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-bell"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-bell-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-bolt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-book"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-bookmark"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-bookmark-empty"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-briefcase"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-bug"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-building"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-bullhorn"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-bullseye"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-calendar"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-calendar-empty"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-camera"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-camera-retro"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-certificate"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-check"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-check-empty"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-check-minus"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-check-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-circle"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-circle-blank"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-cloud"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-cloud-download"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-cloud-upload"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-code"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-code-fork"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-coffee"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-cog"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-cogs"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-collapse"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-collapse-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-collapse-top"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-comment"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-comment-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-comments"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-comments-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-compass"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-credit-card"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-crop"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-dashboard"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-desktop"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-download"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-download-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-edit"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-edit-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-ellipsis-horizontal"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-ellipsis-vertical"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-envelope"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-envelope-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-eraser"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-exchange"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-exclamation"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-exclamation-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-expand"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-expand-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-external-link"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-external-link-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-eye-close"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-eye-open"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-facetime-video"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-female"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-fighter-jet"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-film"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-filter"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-fire"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-fire-extinguisher"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-flag"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-flag-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-flag-checkered"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-folder-close"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-folder-close-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-folder-open"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-folder-open-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-food"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-gamepad"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-gear"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-gears"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-gift"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-glass"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-globe"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-group"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-hdd"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-headphones"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-heart"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-heart-empty"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-home"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-inbox"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-info"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-info-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-key"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-keyboard"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-laptop"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-leaf"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-legal"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-lemon"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-level-down"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-level-up"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-lightbulb"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-lock"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-magic"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-magnet"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-mail-forward"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-mail-reply"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-mail-reply-all"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-male"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-map-marker"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-meh"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-microphone"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-microphone-off"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-minus"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-minus-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-minus-sign-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-mobile-phone"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-money"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-moon"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-move"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-music"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-off"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-ok"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-ok-circle"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-ok-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-pencil"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-phone"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-phone-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-picture"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-plane"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-plus"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-plus-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-plus-sign-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-power-off"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-print"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-pushpin"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-puzzle-piece"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-qrcode"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-question"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-question-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-quote-left"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-quote-right"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-random"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-refresh"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-remove"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-remove-circle"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-remove-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-reorder"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-reply"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-reply-all"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-resize-horizontal"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-resize-vertical"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-retweet"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-road"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-rocket"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-rss"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-rss-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-screenshot"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-search"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-share"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-share-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-share-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-shield"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-shopping-cart"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-sign-blank"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-signal"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-signin"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-signout"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-sitemap"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-smile"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-sort"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-sort-by-alphabet"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-sort-by-alphabet-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-sort-by-attributes"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-sort-by-attributes-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-sort-by-order"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-sort-by-order-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-sort-down"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-sort-up"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-spinner"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-star"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-star-empty"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-star-half"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-star-half-empty"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-star-half-full"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-subscript"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-suitcase"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-sun"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-superscript"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-tablet"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-tag"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-tags"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-tasks"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-terminal"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-thumbs-down"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-thumbs-down-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-thumbs-up"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-thumbs-up-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-ticket"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-time"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-tint"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-trash"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-trophy"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-truck"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-umbrella"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-unchecked"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-unlock"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-unlock-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-upload"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-upload-alt"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-user"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-volume-down"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-volume-off"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-volume-up"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-warning-sign"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-wrench"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-zoom-in"></i></a></div>
              <div class="col-md-1"><a href="javascript:;"><i onClick="seticon(this)"  class="myicon icon-zoom-out"></i></a></div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
 <div class="row col-md-12" style="float: left;">
  <div class="form-group  col-md-12">
    <input type="button" onClick="update_module_data();" class="btn btn-success btn-lrg" name="save_data_form_new"  value="حفظ" />
  </div>
</div>
  </form>
</div>

</div>
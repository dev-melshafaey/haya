<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<?php $segment = $this->uri->segment(3); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <?PHP foreach($this->loan->get_all_module() as $lease) { 
				$sublist = $this->loan->get_all_sub_module($lease->moduleid);	
		?>
        <div class="col-md-3">
          <section class="row">
            <div class="panel-heading text-overflow-hidden">
              <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
                <thead>
                  <tr role="row">
                    <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending"> <div class="table-responsive">
                        <div class="col-md-12"><a onClick="showglobaldata(this);" href="#globalDiag" data-url="<?PHP echo base_url(); ?>lease_programe/modulemodifier/<?PHP echo $lease->moduleid; ?>" style="font-size: 20px;  color:#029625 !important;" data-id="<?PHP echo $lease->moduleid; ?>"> <i class="<?PHP echo  $lease->module_icon; ?>"></i> <?PHP echo $lease->module_name; ?> </a><br>
                        <a href="#" class="sub_menu_activater" style="font-size: 11px;" data-id="<?PHP echo $lease->moduleid; ?>">وحدات فرعية (<?PHP echo $this->loan->get_all_module_count('mh_modules','module_parent',$lease->moduleid); ?>)</a></div>
                      </div>
                    </th>
                  </tr>
                  <tr role="row" class="sub_menu_list" id="sub_menu_<?PHP echo $lease->moduleid; ?>">
                  	<th id="sub_menu_data_load_<?PHP echo $lease->moduleid; ?>"><?PHP echo $lease->moduleid; ?></th>
                  </tr>
                </thead>
                <tbody role="alert" aria-live="polite" aria-relevant="all">                 
                  <?PHP 
				  if(isset($sublist))
				{
				 	foreach($sublist as $subm) { ?>
                  <tr class="gradeA even">
                    <td class=" sorting_1 bigbash">
                    <h5 style="font-size:15px !important;">
					<div class="col-md-8" style="text-align:right !important;"><?PHP echo $subm['moduletitle']; ?> </div>
                    <div class="col-md-3"><button type="button" data-url="<?PHP echo $subm['controllerlink'] ?>" onClick="gototype(this);"  class="btn btn-sm <?PHP if($subm['count']<=0) { ?>btn-danger<?PHP } else { ?>btn-success<?PHP } ?> border_radius"><?PHP echo $subm['count']; ?></button></div></h5></td>
                  </tr>
                  <?PHP } }?>
                </tbody>
              </table>
            </div>
          </section>
        </div>
        <?PHP  }  ?>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
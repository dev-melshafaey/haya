<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<?php $segment = $this->uri->segment(3); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12" style="background-color:#FFF !important; margin-top: 2px !important; padding-top:16px !important;">
		<table class="table table-bordered table-striped basicTable" id="tableSortable"
                                           aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th>اسم استمارة</th>
                        <th>وحدة الأم</th>
                        <th>وحدة أيقونة</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?PHP foreach($flist as $Illuminati) { ?>
					<tr role="row">
                        <td><?PHP echo $Illuminati->module_name; ?></td>
                        <td><?PHP echo $this->haya_model->ModuleName($Illuminati->module_parent); ?></td>
                        <td><a onclick="alatadad(this);" data-icon="<?PHP echo($Illuminati->module_icon); ?>" data-heading="<?PHP echo($Illuminati->module_name); ?>" href="#" data-url="<?PHP echo base_url().$Illuminati->module_controller; ?>"><i class="<?PHP echo $Illuminati->module_icon; ?>"></i></a></td>
                      </tr>
					<?PHP } ?>
					</tbody>
                  </table>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</body>
</html>
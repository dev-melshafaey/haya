<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<?php $segment = $this->uri->segment(3); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12" style="background-color:#FFF !important; margin-top: 2px !important; padding-top:16px !important;">
		  
		  <form name="frmbuild" action="<?PHP echo base_url(); ?>lease_programe/formsave" id="frmbuild" method="post" autocomplete="off">			  
			  
			  <div class="col-md-1 form_bold">اسم استمارة:</div>
			  <div class="col-md-3"><input type="text" placeholder="اسم استمارة" class="form-control req" name="form_name" id="form_name"></div>
			  <div class="col-md-1 form_bold">التفاصيل:</div>
			  <div class="col-md-3"><input type="text" placeholder="التفاصيل" class="form-control req" name="form_description" id="form_description"></div>
			  
			  <div class="col-md-1 form_bold">وحدة الأم:</div>
			  <div class="col-md-3"><?PHP $this->haya_model->module_dropbox('form_module',$form_module); ?></div>
			   <br style="clear: both;"><br style="clear: both;">
			  <div class="col-md-1 form_bold">وحدة أيقونة:</div>
			  <div class="col-md-3"><input type="text" placeholder="وحدة أيقونة" name="form_icon" id="form_icon" class="form-control req"></div>
			 
			  <div class="col-md-1 form_bold">الحالة:</div>
			  <div class="col-md-3">
				<select name="form_status" id="form_status" class="form-control">
				  <option value="A" <?php if($d->module_status	==	'A'):?> selected="selected" <?php endif;?>>نشط</option>
				  <option value="D" <?php if($d->module_status	==	'D'):?> selected="selected" <?php endif;?>>دي نشط</option>
				</select>				
			  </div>
			  <div class="col-md-1 form_bold">ترتيب:</div>
			  <div class="col-md-3">
				<?PHP number_drop_box('form_order',$d->form_order); ?>				
			  </div>
			  
			  <div class="col-md-1 form_margin"></div>
			  <div class="col-md-11 form_margin" id="addmyfield"><button class="btn btn-success" type="button" name="form_button" id="form_button">إضافة حقل جديد</button></div>
		  
			  
			  <div class="col-md-12" id="savebar" style="text-align: right; display: none;"><button class="btn btn-success" type="button" name="form_save" id="form_save"><i></i> حفظ</button></div>
		  </form>
		  <br style="clear: both;">
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</body>
</html>
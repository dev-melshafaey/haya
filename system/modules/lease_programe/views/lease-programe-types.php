<?php $this->load->view('common/meta');?>
<script>
function closePopup(){
		$("#overlay").hide();
		$("#dialog").hide();
}
$(document).ready(function(){
 $(".other").click(function(){
				var id = $(this).attr('id');
				var ele = $("#"+id).find(':checkbox');
				if ($("#"+id).prop('checked')) 
				{
				  ///ele.prop('checked', false);
				  var entry	=	'1';
				  $("#show"+id).show().delay(2000).fadeOut();
				} 
				else 
				{
				  var entry	=	'0';
				   $("#hide"+id).show().delay(2000).fadeOut();
				  //ele.prop('checked', true);
				}
		
				var request = $.ajax({
					  url: config.BASE_URL+'lease_programe/other',
					  type: "POST",
					  data: { id : id , entry : entry},
					  dataType: "html",
					  success: function(msg)
					  {

					  }
					});  
			});
		
			});
			
			function viewNew(id,parent){
				//alert(id+'id');
				//alert(parent+'parent');
				if(id != '')
				{
					$("#btn-name").val('تحديث');
										
					$("#overlay").show();
					$("#dialog").show();

					var request = $.ajax({
						  url: config.BASE_URL+'lease_programe/get_category_data',
						  type: "POST",
						  data: "id="+id,
						  dataType: "json",
						  cache: false,
						  success: function(data)
						  {
							  console.log(data);
							  
							 // console.log(msg);
							 $("#loan_category_name").val(data.cat_name);
							 
							 $("#loan_category_id").val(id);
							 $("#parent_id").val(parent);
							 
						  }

						});
				}
				else
				{
					$("#loan_category_id").val('');
					$("#btn-name").val('إضافة');
					$("#parent_id").val(parent);
					$("#overlay").show();
					$("#dialog").show();
				}

			}
			
	function addnew(){
				
				
				var loan_category_name	= $("#loan_category_name").val();
				var loan_category_id	= $("#loan_category_id").val();
				var parent_id	= $("#parent_id").val();

					var request = $.ajax({
					  url: config.BASE_URL+'lease_programe/add_category',
					  type: "POST",
					  data: { loan_category_name : loan_category_name, loan_category_id : loan_category_id,parent_id:parent_id},
					  dataType: "html",
					  success: function(msg)
					  {
							$("#overlay").hide();
							$("#dialog").hide();
							
							window.location.href	=	config.CURRENT_URL;
					  }
					});
			}
</script>
 <style type="text/css">
.addnew{
	background:#f5bf05;	
}
.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 101;
   display:none;
}
.web_dialog
{
   position: fixed;
   width: 380px;
   height: 140px;
   top: 50%;
   left: 50%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #ffffff;
   border: 2px solid #0000;
   padding: 0px;
   z-index: 102;
   font-family: Verdana;
   font-size: 10pt;
   display:none;
}
.web_dialog_title
{
   border-bottom: solid 2px #0000;
   background-color: #CCC;
   padding: 4px;
   color: White;
   font-weight:bold;
   text-align:right;
}
.web_dialog_title a
{
   color: White;
   text-decoration: none;
}
.align_right
{
   text-align: Left;
}
.msg{
	text-align: center;
	padding-top: 16%;
}

</style>
<div class="body">
  <div id="overlay" class="web_dialog_overlay"></div> 
<div id="dialog" class="web_dialog">
 <input id="loan_category_id" name="loan_category_id" type="hidden" class="txt_field">
 <input id="parent_id" name="parent_id" type="hidden" class="txt_field">
   <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
      <tr>
         <td class="web_dialog_title">إضافة جديدة</td>
         <td class="web_dialog_title align_right">
            <a href="javascript:void(0)" id="btnClose" onClick="closePopup()">X</a>
         </td>
         
      </tr>
    </table>
   <div class="form_raw"  style="margin-top: 29px;">
      <div class="user_txt" style="width:auto; padding-left:26px;">إسم</div>
	    <div class="user_field">
        <input id="loan_category_name" name="loan_category_name" type="text" class="txt_field">
        </div>
    </div>
    <div class="user_field"><div class="add_team_btn"><input type="button" id="btn-name" value="إضافة" class="transperant_btn" onclick=" addnew()" /> </div></div>    
</div>
<?php $this->load->view('common/banner');?>
<div class="body_contant">
  <?php $this->load->view('common/floatingmenu');?>
  <?PHP parentMenu(); ?>
  <div class="main_contant"> 

        <?php $success	=	$this->session->flashdata('success');?>
        <?php if(!empty($success)):?>
            <div class="right_nav_raw">
            <div class="nav_icon"><img src="<?php echo base_url();?>images/body/right.png" width="60" height="60"></div>
            <?php echo $success;?>
            </div>
        <?php endif;?>
    <div class="data_raw">
      <div class="main_box">
        <div class="data_box_title"> 
          <div class="data_title">‫أسماء السلطات التعليمية المحلية</div>
          <div class="page_controls">
          	<div class="page_control" style="float:left;"><a id="addnew" class="addnew" href="javascript:void(0)" onclick="viewNew('0','0')">إضافة</a></div>
          </div>
        </div>
        <div class="data">
          <div class="main_data">
          <?php if(!empty($all_types)):?>
          <?php foreach($all_types as $types):?>
                <div class="main_tab" id="bingo<?php echo $types->loan_category_id;?>">
                  <div class="gray_main_right_icon"></div>
                  <div class="tab_txt" style="width:auto !important margin-right:50px;"><?php echo $types->loan_category_name;?></div>

                  <div class="tab_cotrols">
                  	<a id="addnew" class="addnew" href="javascript:void(0)" onclick="viewNew('<?php echo $parent_id;?>','<?php echo $types->loan_category_id;?>')" style="margin-left:20px;">إضافة</a>
                    <a href="#_" onclick="viewNew('<?php echo $types->loan_category_id;?>')">
                    <div class="tab_control">
                    <img src="<?php echo base_url();?>images/body/contant/edit.png" width="16" height="16">
                    تعديل</div>
                    </a>
                    <a class="delete-btn" id="<?php echo $types->loan_category_id;?>" data-url="<?php echo base_url();?>lease_programe/delete_type/<?php echo $types->loan_category_id;?>" href="#_">
                    <div class="tab_control_last">
                    <img src="<?php echo base_url();?>images/body/contant/delete.png" width="16" height="16"> حذف</div>
                    </a>
                  </div>
                </div>
            <?php endforeach;?>
            <?php else:?>
            	 <div class="data_title" style="margin-right:10px;color:#F00">‫ليس هناك اسم لياس</div>
            <?php endif;?>
           
          </div>
        </div>
        <div id="dialog-confirm" title="تحذير" style="display:none;">
          <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>أنه سيتم حذف بشكل دائم ولا يمكن استردادها. هل أنت متأكد؟</p>
        </div>

      </div>
    </div>
  </div>
</div>
<?php $this->load->view('common/footer');?>

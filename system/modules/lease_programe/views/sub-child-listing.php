<?php $this->load->view('common/meta');?>
<style>
.dialog-height
{
	height:162px !important;
}
.tab_control
{
	border-left: none !important;
}
</style>
<script>

function parent_form(id)
{
	$('#add_parent')[0].reset();
	if(id	!=	'')
	{
	var request = $.ajax({
		  url: config.BASE_URL+'lease_programe/get_data',
		  type: "POST",
		  data: "id="+id,
		  dataType: "json",
		  cache: false,
		  success: function(data)
		  {
			  console.log(data);
			 // console.log(msg);
			 $("#loan_caculate_id").val(data.loan_caculate_id);
			 $("#loan_category_name").val(data.loan_category_name);

		  }

		});
		
	}
	else
	{
		$("#loan_caculate_id").val('');
	}
	
	$('.ui-dialog-buttonpane').slideDown('slow');

		$( "#dialog-parent" ).dialog({
			  resizable: true,
			  height:500,
			  width:900,
			  hide: 'bounce',
        	  show: 'bounce',			  
			  modal: true,
			  buttons: {
				"إرسال": function() {
				
					var str_data = 	$('#add_parent').serialize();
					
					var request = $.ajax({
					  url: config.BASE_URL+'lease_programe/add_child',
					  type: "POST",
					  data: str_data,
					  dataType: "html",
					  beforeSend: function(){	$('.ui-dialog-buttonpane').slideUp('slow');	},
					  success: function(msg)
					  {
						console.log(msg);
							//$("#overlay").hide();
							//$("#dialog").hide();
							
							window.location.href	=	config.CURRENT_URL;
					  }
					});
							
				},

				"حذف": function() {
				  $( this ).dialog( "close" );
				}
			  }
			});
}
function child_form(id)
{
	$('#add_child')[0].reset();
	if(id	!=	'')
	{
	var request = $.ajax({
		  url: config.BASE_URL+'lease_programe/get_data',
		  type: "POST",
		  data: "id="+id,
		  dataType: "json",
		  cache: false,
		  success: function(data)
		  {
			  console.log(data);
			 // console.log(msg);
			 //$("#loan_caculate_id").val(data.loan_caculate_id);
			 //$("#loan_category_name").val(data.loan_category_name);

		  }

		});
		
	}
	else
	{
		$("#loan_caculate_id").val('');
	}
	
	$('.ui-dialog-buttonpane').slideDown('slow');

		$( "#dialog-child" ).dialog({
			  resizable: true,
			  height:500,
			  width:900,
			  hide: 'bounce',
        	  show: 'bounce',			  
			  modal: true,
			  buttons: {
				"إرسال": function() {
		
				var str_data = 	$('#add_child').serialize();	
				
					var request = $.ajax({
					  url: config.BASE_URL+'lease_programe/add_child',
					  type: "POST",
					  data: str_data,
					  dataType: "html",
					  beforeSend: function(){	$('.ui-dialog-buttonpane').slideUp('slow');	},
					  success: function(msg)
					  {
						console.log(msg);
							//$("#overlay").hide();
							//$("#dialog").hide();
							
						//	window.location.href	=	config.CURRENT_URL;
					  }
					});
							
				},

				"حذف": function() {
				  $( this ).dialog( "close" );
				}
			  }
			});
}
</script>
<div class="body">
<?php $this->load->view('common/banner');?>
<div class="body_contant">
  <?php $this->load->view('common/floatingmenu');?>
  <?PHP parentMenu(); ?>
  <div class="main_contant"> 
    <?php $success	=	$this->session->flashdata('success');?>
    <?php if(!empty($success)):?>
    <div class="right_nav_raw">
      <div class="nav_icon"><img src="<?php echo base_url();?>images/body/right.png" width="60" height="60"></div>
      <?php echo $success;?> </div>
    <?php endif;?>
    <div class="data_raw">
      <div class="main_box">
        <div class="data_box_title"> 
          <!--<div class="data_box_title_icon"><img src="images/menu/question_s.png" width="22" height="20" /></div>-->
          <div class="data_title"><a href="#_" class="addnew" onclick="parent_form('');" ><img src="<?php echo base_url();?>images/listicon/001_01.png" width="25" height="25"></a></div>
          <div class="page_controls">
            <div class="page_control"><a href="#" onclick="javascript:history.go(-1);return false;"><img src="<?php echo base_url();?>images/body/contant/back.png" width="28" height="26" border="0" /></a></div>
          </div>
        </div>
        <div class="data">
          <div class="main_data">
            <?php if(!empty($all_loans)): ?>
            <?php foreach($all_loans as $loan):?>
            <?php $category_name	=	$this->loan->get_loan_types($loan->loan_category_id);?>
			<?php $count	=	$this->loan->child_count($loan->loan_caculate_id);?>
            <div class="main_tab" id="bingo<?php echo $loan->loan_caculate_id;?>">
              <div class="gray_main_right_icon"></div>
              
              <div class="team2_tab_txt" style="width:90px !important ;"><?PHP echo $loan->loan_category_name; ?></div>
              <div class="team2_tab_txt" style="width:186px !important;">من <?PHP echo $loan->loan_start_amount; ?> إلى <?PHP echo $loan->loan_end_amount; ?></div>

              <div class="team2_tab_txt" style="width:143px !important;"><?PHP echo (isset($loan->loan_percentage) ? $loan->loan_percentage."%" : NULL); ?></div>
              
              <div class="team2_tab_txt" style="width:143px !important;">من <?PHP echo $loan->loan_starting_day; ?> <?php if($loan->loan_start_timeperiod	==	"day"): ?>
                    <?php echo 'يوم';?>
                  <?php elseif($loan->loan_start_timeperiod	==	"month"):?>
                    <?php echo 'شهر';?>
                  <?php elseif($loan->loan_start_timeperiod	==	"year"):?>
                    <?php echo 'عام';?>
                  <?php endif;?>
              </div>
              <div class="team2_tab_txt" style="width:143px !important;"><?PHP echo $loan->loan_expire_day; ?> إلى 
				  <?php if($loan->loan_expire_timeperiod	==	"day"): ?>
                    <?php echo 'يوم';?>
                  <?php elseif($loan->loan_expire_timeperiod	==	"month"):?>
                    <?php echo 'شهر';?>
                  <?php else:?>
                    <?php echo 'عام';?>
                  <?php endif;?>
              </div>
              
              <div class="tab_cotrols">			  
				<!-- <a href="#_">
                <div class="tab_control"> 
					<a href="javascript:void(0)" onclick="child_form('<?php  echo $loan->loan_caculate_id;?>')"><img src="<?php echo base_url();?>images/listicon/001_01.png" width="20" height="20"></a>
				</div>
                </a> -->

				<a class="delete-btn" id="<?php echo $loan->loan_caculate_id;?>" href="#_" data-url="<?php echo base_url();?>lease_programe/delete/<?php echo $loan->loan_caculate_id;?>">
                <div class="tab_control_last"> <img src="<?php echo base_url();?>images/body/contant/delete.png" width="16" height="16"></div>
                </a>
				<?php if($count > 0):?>
				<a href="#_">
                <div class="tab_control"> 
					<a href="<?php echo base_url();?>inquiries/sub_child_listing/<?PHP echo $list->list_id; ?>">
					  عرض الكل (<?php echo $count;?>)
					</a>
					</div>
                </a>
				<?php endif;?>
				</div>
            </div>
            <?php endforeach;?>
            <?php endif;?>
          </div>
        </div>
        <div id="dialog-confirm" title="حذف" style="display:none;">
		
          <p><span class="ui-icon ui-icon-alert" style="float:right; margin:0 7px 20px 0;"></span>أنه سيتم حذف بشكل دائم ولا يمكن استردادها. هل أنت متأكد؟</p>
        </div>
		<div id="dialog-parent" class="child-height" title="إضافة" style="margin-left: -7px;display:none;">
		  <div class="data_raw">
			<form action="" method="POST" id="add_parent" name="add_parent" autocomplete="off">
			  <input type="hidden" name="parent_id" value="<?php  echo $parent_id;?>"/>
			  <div class="form_raw">
                <div class="form_txt">إسم</div>
                <div class="form_field">
                  <input type="text" class="txt_field req" name="loan_category_name"  id="loan_category_name"  placeholder="إسم"/>
                </div>
              </div>
              <div class="form_raw">
                <div class="form_txt">من</div>
                <div class="form_field">
                  <input type="text" class="txt_field req NumberInput" name="loan_start_amount"  id="loan_start_amount"  placeholder="من"/>
                </div>
                <div class="form_field"></div>
                <div class="form_txt" style="margin-left: -45px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;إلى</div>
                <div class="form_field">
                  <input type="text" class="txt_field req NumberInput" name="loan_end_amount"  id="loan_end_amount"  placeholder="إلى"/>
                </div>
              </div>
              
              <div class="form_raw">
                <div class="form_txt">نسبة مئوية</div>
                <div class="form_field">
                  <input type="text" class="txt_field req NumberInput" name="loan_percentage"  id="loan_percentage"  placeholder="نسبة مئوية"/>
                </div>
              </div>
              <div class="form_raw">
                <div class="form_txt">فترة السماح‎</div>
                <div class="form_field">
                  <input style="width:50px !important;" type="text" class="txt_field req NumberInput" name="loan_starting_day"  id="loan_starting_day" placeholder="فترة السماح‎"/>
                </div>

                <div class="form_field_selected">
                 <select name="loan_start_timeperiod" id="loan_start_timeperiod" >
                 	<option value="day">يوم</option>
                    <option value="month">شهر</option>
                    <option value="year">عام</option>
                 </select>
                </div>
              </div>
              <div class="form_raw">
                <div class="form_txt">حتى</div>
                <div class="form_field">
                  <input style="width:50px !important;" type="text" class="txt_field req NumberInput" name="loan_expire_day"  id="loan_expire_day"  placeholder="فرع كود"/>
                </div>

                <div class="form_field_selected">
                 <select name="loan_expire_timeperiod" id="loan_expire_timeperiod" >
                 	<option value="day">يوم</option>
                    <option value="month">شهر</option>
                    <option value="year">عام</option>
                 </select>
                </div>
              </div>
            </form>
			</form>
		  </div>
		</div>
		<div id="dialog-child" class="child-height" title="إضافة" style="margin-left: -7px;display:none;">
		  <div class="data_raw">
			<form action="" method="post" id="add_child" name="add_child" autocomplete="off">
			<input type="hidden" name="parent_id" value="<?php  echo $loan->loan_caculate_id;?>"/>
			  <div class="form_raw">
                <div class="form_txt">إسم</div>
                <div class="form_field">
                  <input type="text" class="txt_field req" name="loan_category_name"  id="loan_category_name"  placeholder="إسم"/>
                </div>
              </div>
              <div class="form_raw">
                <div class="form_txt">من</div>
                <div class="form_field">
                  <input type="text" class="txt_field req NumberInput" name="loan_start_amount"  id="loan_start_amount"  placeholder="من"/>
                </div>
                <div class="form_field"></div>
                <div class="form_txt" style="margin-left: -45px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;إلى</div>
                <div class="form_field">
                  <input type="text" class="txt_field req NumberInput" name="loan_end_amount"  id="loan_end_amount"  placeholder="إلى"/>
                </div>
              </div>
              
              <div class="form_raw">
                <div class="form_txt">نسبة مئوية</div>
                <div class="form_field">
                  <input type="text" class="txt_field req NumberInput" name="loan_percentage"  id="loan_percentage"  placeholder="نسبة مئوية"/>
                </div>
              </div>
              <div class="form_raw">
                <div class="form_txt">فترة السماح‎</div>
                <div class="form_field">
                  <input style="width:50px !important;" type="text" class="txt_field req NumberInput" name="loan_starting_day"  id="loan_starting_day" placeholder="فترة السماح‎"/>
                </div>

                <div class="form_field_selected">
                 <select name="loan_start_timeperiod" id="loan_start_timeperiod" >
                 	<option value="day">يوم</option>
                    <option value="month">شهر</option>
                    <option value="year">عام</option>
                 </select>
                </div>
              </div>
              <div class="form_raw">
                <div class="form_txt">حتى</div>
                <div class="form_field">
                  <input style="width:50px !important;" type="text" class="txt_field req NumberInput" name="loan_expire_day"  id="loan_expire_day"  placeholder="فرع كود"/>
                </div>

                <div class="form_field_selected">
                 <select name="loan_expire_timeperiod" id="loan_expire_timeperiod" >
                 	<option value="day">يوم</option>
                    <option value="month">شهر</option>
                    <option value="year">عام</option>
                 </select>
                </div>
              </div>
            </form>
		  </div>
		</div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('common/footer');?>

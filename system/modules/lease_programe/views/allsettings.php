<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <form name="frmsetting" id="frmsetting" method="post" autocomplete="off">
            <div class="col-md-12 form-group">
              <div class="col-md-12" style="padding:25px 0px !important;">
                <?PHP foreach($list as $li) { ?>
                <div class="col-md-6 form-group">
                  <label class="text-warning"><?PHP echo $li->settingtitle; ?> : </label>
                  <input  autocomplete="off" value="<?PHP echo $li->settingvalue; ?>" type="text" placeholder="<?PHP echo $li->settingtitle; ?>" class="form-control" name="s_<?PHP echo $li->settingsid; ?>"  id="s_<?PHP echo $li->settingsid; ?>"/>
                </div>
                <?PHP } ?>
              </div>
              <div class="col-md-12" style="padding:25px 0px !important;">
                <input type="submit" class="form-control text-warning" value="حفظ">
              </div>
              <br clear="all" />
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
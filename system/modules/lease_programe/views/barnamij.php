<?PHP
	$t = $type;
	$d = $data;
	if($t=='child') { ?>
<div class="row col-md-12">
  <form action="" autocomplete="off" method="post" id="add_child" name="add_child" autocomplete="off">
  <input type="hidden" id="parent_id" name="parent_id" value="<?php echo $d->parent_id; ?>"/>
  <input type="hidden" id="loan_caculate_id" name="loan_caculate_id" value="<?php echo $d->loan_caculate_id; ?>"/>
  <div class="form-group col-md-6">
    <label for="basic-input">إسم</label>
    <input type="text" class="form-control req" value="<?php echo $d->loan_category_name; ?>" name="loan_category_name"  id="loan_category_name"  placeholder="إسم"/>
  </div>
  <div class="form-group col-md-3">
    <label for="basic-input">من</label>
    <input type="text" class="form-control req NumberInput" value="<?php echo $d->loan_start_amount; ?>" name="loan_start_amount"  id="loan_start_amount"  placeholder="من"/>
  </div>
  <div class="form-group col-md-3">
    <label for="basic-input">إلى</label>
    <input type="text" class="form-control req NumberInput" value="<?php echo $d->loan_end_amount; ?>" name="loan_end_amount"  id="loan_end_amount"  placeholder="إلى"/>
  </div>
  <div class="form-group col-md-6">
    <label for="basic-input">نسبة الرسوم</label>
    <input type="text" class="form-control req NumberInput" value="<?php echo $d->loan_percentage; ?>" name="loan_percentage"  id="loan_percentage"  placeholder="نسبة الرسوم"/>
  </div>
  
  <div class="form-group col-md-3">
    <label for="basic-input">فترة السماح</label>
    <input  type="text" class="form-control req NumberInput" name="loan_starting_day"  id="loan_starting_day" placeholder="فترة السماح‎" value="<?php echo $d->loan_starting_day; ?>"/>
    <?PHP //youmdropbox('loan_expire_timeperiod',$d->loan_expire_timeperiod); ?>
    
  </div>
  <div class="form-group col-md-3">
    <label for="basic-input">مدة السداد (سنة)</label>
<input type="text" class="form-control req NumberInput" name="loan_expire_day"  id="loan_expire_day"  placeholder="مدة السداد (سنة)" value="<?php echo $d->loan_expire_day; ?>"/>
    <?PHP //youmdropbox('loan_start_timeperiod',$d->loan_start_timeperiod); ?>
    </div>
  <div class="form-group col-md-6">
    <label for="basic-input">المساهمة الشخصية</label>
    <input type="text" class="form-control req NumberInput" value="<?php echo $d->loan_applicant_percentage; ?>" name="loan_applicant_percentage"  id="loan_applicant_percentage"  placeholder="المساهمة الشخصية"/>
  </div>
  
  </form>
</div>
<div class="row col-md-12">
<div class="form-group  col-md-12">
      <input type="button" onClick="save_barnamij_data();" class="btn btn-success btn-lrg" name="save_data_form_new"  value="حفظ" />
  </div>
</div>
<?PHP } else { ?>
<div class="row col-md-12">
  <form action="" method="POST" id="add_parent" name="add_parent" autocomplete="off">
  <input type="hidden" id="loan_caculate_id" name="loan_caculate_id" value="<?php echo $d->loan_caculate_id; ?>"/>
  <div class="form-group col-md-6">
    <label for="basic-input">إسم</label>
    <input type="text" class="form-control req" value="<?php echo $d->loan_category_name; ?>" name="loan_category_name"  id="loan_category_name"  placeholder="إسم"/>
  </div>  
  </form>
</div>
<div class="row col-md-12">
<div class="form-group  col-md-12">
      <input type="button" onClick="save_barnamij_parent_data();" class="btn btn-success btn-lrg" name="save_data_form_new"  value="حفظ" />
  </div>
</div>

<?PHP } ?>
<script>
$(function(){
restrict_number();
});
</script>
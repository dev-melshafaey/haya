<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lease_programe extends CI_Controller 
{
//-------------------------------------------------------------------------------	
	/*
	* Properties
	*/
	private $_data = array();
	private $_user_info	=	array();
//-------------------------------------------------------------------------------

	/*
	* Costructor
	*/
	
	public function __construct()
	{
		parent::__construct();		
		//Load Models
        $this->_data['module'] = $this->haya_model->get_module();		
		//SET Login USER ID
		$this->_login_userid			=	$this->session->userdata('userid');
		$this->_data['login_userid']	=	$this->_login_userid;	
		$this->_data['user_detail'] 	= 	$this->haya_model->get_user_detail($this->_login_userid);		
        $this->_data['user_info']	=	$this->haya_model->get_user_detail($this->_login_userid);        
		$this->load->model('lease_programe_model', 'loan');
	}
//-------------------------------------------------------------------------------	
	public function modulemodifier($moduleid)
	{
		$this->db->select('*');
		$this->db->from('mh_modules');
		$this->db->where('moduleid',$moduleid);
		$this->db->limit(1); 
		$query = $this->db->get();
		$this->_data['mi'] = $query->result();
		$this->load->view('modulemodifier', $this->_data);
	}
	
	public function savesorting()
	{
		$sorting = explode('-',$this->input->post('my'));
		foreach($sorting as $skey => $svalue)
		{
			if($svalue!='')
			{
				$this->db->query("UPDATE mh_modules SET module_order='".$skey."' WHERE moduleid='".$svalue."'");
			}
		}
	}
//-------------------------------------------------------------------------------	
	public function update_module()
	{
		$moduleid = $this->input->post('moduleid');
		$data = $this->input->post();
		unset($data['moduleid']);
		$this->db->where('moduleid', $moduleid);
		$this->db->update('mh_modules',json_encode($data),$this->_login_userid,$data);
		echo('1');

	}
//-------------------------------------------------------------------------------	
	public function get_submenu_list($menuid)
	{
		$this->db->select('moduleid,module_name');
		$this->db->from('mh_modules');
		$this->db->where('module_parent',$menuid);		
		$query = $this->db->get();
		$html = '';
		foreach($query->result() as $m)
		{
			$html .= '<li><a style="font-size:11px !important;" onClick="showglobaldata(this);" href="#globalDiag" data-url="'.base_url().'lease_programe/modulemodifier/'.$m->moduleid.'" data-id="'.$m->moduleid.'">'.$m->module_name.'</a></li>';
		}
		echo $html;
	}
	
//-------------------------------------------------------------------------------

	/*
	*
	* Main Page
	*/
	public function index()
	{
		 $this->haya_model->check_permission($this->_data['module'],'v');
		$this->load->view('listmanagement_landing', $this->_data);
	}
//-------------------------------------------------------------------------------
	public function add_child_barnamij($parentid,$childid=0)
	{
		if($childid!='' && $childid!=0)
		{
			$this->_data['data'] = $this->loan->get_data($childid);
		}
		else
		{
			$this->_data['data']->parent_id = $parentid;
		}
		
		$this->_data['type'] = 'child';
		$this->load->view('barnamij',$this->_data);
	}
//-------------------------------------------------------------------------------	
	public function add_data_for_barnamij($parentid)
	{
		if($parentid!='' && $parentid!=0)
		{
			$this->_data['data'] = $this->loan->get_data($parentid);
		}
		
		$this->_data['type'] = 'parent';
		$this->load->view('barnamij',$this->_data);
	}
//-------------------------------------------------------------------------------	
	/*
	*
	* Add List Detail
	*/
	public function add($loanid	= NULL)
	{
		if($loanid)
		{
			$this->_data['single_loan']	=	$this->loan->get_single_loan_calculate($loanid);	

		}
		
		if($this->input->post())
		{
			
			$data		=	$this->input->post();

			// UNSET ARRAY key
			unset($data['save_data_form']);
			
			if($this->input->post('loan_caculate_id'))
			{

				$this->loan->update_loan_calculation($this->input->post('loan_caculate_id'),$data);
				
				$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				redirect(base_url()."lease_programe/listing");
				exit();
			}
			else
			{
				$this->loan->add_loan_calculate($data);
				
				$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				redirect(base_url()."lease_programe/listing");
				exit();
			}
		}
		else
		{
			if($loanid)
			{
				$this->_data['loan_caculate_id']	=	$loanid;
			}
			else
			{
				$this->_data['loan_caculate_id']	=	'';
			}
			
			$this->load->view('add', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Add List Detail
	*/
	public function add_child()
	{
		if($this->input->post())
		{
			$data		=	$this->input->post();
			
			if($this->input->post('loan_caculate_id'))
			{
				$this->loan->update_loan_calculation($this->input->post('loan_caculate_id'),$data);
				
				$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				
			}
			elseif($this->input->post('loan_id'))
			{
				$loan_caculate_id = $this->input->post('loan_id');
				unset($data['loan_id']);
				$this->loan->update_loan_calculation($loan_caculate_id,$data);
				
				$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				
			}
			else
			{
				$this->loan->add_loan_calculate($data);
				$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');

			}
		}
	}
	
//-------------------------------------------------------------------------------

	/*
	*
	* Parent Listing Page
	*/
	public function listing()
	{
		$this->_data['all_loans']	=	$this->loan->get_all_loan_calculate();

		$this->load->view('lease-programe-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Child Listing Page
	*/
	public function child_listing($parent_id	=	NULL)
	{
		$this->_data['parent_id']	=	$parent_id;		
		$this->_data['parent_name']	=	$this->loan->get_parent_name($parent_id);		
		$this->_data['all_loans']	=	$this->loan->child_listing($parent_id);
		$this->load->view('child-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Sub Child Listing Page
	*/
	public function sub_child_listing($parent_id	=	NULL)
	{
		$this->_data['parent_id']	=	$parent_id;
		$this->_data['all_loans']	=	$this->loan->child_listing($parent_id);

		$this->load->view('sub-child-listing', $this->_data);
	}
	
//-------------------------------------------------------------------------------	
	public function get_data()
	{
		$id	=	$this->input->post('id');
		
		$data	=	$this->loan->get_data($id);
		
		echo  $data	=	json_encode($data);
	}
//--------------------------------------------------------------------------------

	public function add_parent()
	{
		$id	=	$this->input->post("id");
		$name	=	$this->input->post("name");
		
		$data	=	array("loan_category_name"=>$name);
		
		if($id)
		{
			$this->loan->update_parent($id,$data);
		}
		else
		{
			$this->loan->add_parent($data);
		}
	}
	
	public function add_parent_noor()
	{
		$loan_caculate_id	=	$this->input->post("loan_caculate_id");
		$loan_category_name	=	$this->input->post("loan_category_name");
		
		$data	=	array("loan_category_name"=>$loan_category_name);
		
		if($loan_caculate_id)
		{
			$this->loan->update_parent($loan_caculate_id,$data);
		}
		else
		{
			$this->loan->add_parent($data);
		}
	}
//--------------------------------------------------------------------------------

	/*
	*
	*
	*/
	public function add_category($catid	= NULL)
	{
		if($catid)
		{
			$this->_data['single_category']	=	$this->loan->get_single_loan_calculate($catid);	

		}
		if($this->input->post())
		{
			$data		=	$this->input->post();

			// UNSET ARRAY key
			unset($data['save_data_form']);
			
			if($this->input->post('loan_category_id'))
			{

				$this->loan->update_category($this->input->post('loan_category_id'),$data);
				
				$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				redirect(base_url()."lease_programe/add_category");
				exit();
			}
			else
			{

				$this->loan->add_category($data);
				
				$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				redirect(base_url()."lease_programe/add_category");
				exit();
			}
		}
		else
		{
			if($catid)
			{
				$this->_data['loan_category_id']	=	$catid;
			}
			else
			{
				$this->_data['loan_category_id']	=	'';
			}
			
			$this->_data['all_types']	=	$this->loan->get_all_loan_types();
			$this->load->view('lease-programe-types', $this->_data);
		}
		
	}
//-------------------------------------------------------------------------------

	public function get_category_data()
	{
		$category_id	=	 $this->input->post('id');
		
		$get_category_name	=	$this->loan->category_name($category_id);
		
		echo  $data	=	json_encode(array('cat_name'	=>	$get_category_name));
		
	}
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	public function delete($loan_id)
	{
		$this->loan->delete($loan_id);
		
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
		redirect(base_url().'lease_programe/listing');
		exit();

	}
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	public function delete_type($cat_id)
	{
		$this->loan->delete_category($cat_id);
		
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
		redirect(base_url().'lease_programe/listing');
		exit();
	}
//-------------------------------------------------------------------------------

	/*
	* Logout
	* Destroy All Sessions
	*/
	
	public function logout()
	{
		// Destroy all sessions
		$this->session->sess_destroy();
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('userinfo');
		redirect(base_url());
		exit();
	}
//-------------------------------------------------------------------------------

	/*
	* 
	* 
	*/    
    public function allsettings()
    {
        $lxx = $this->loan->get_all_setting_list();
		
        $this->_data['list'] = $lxx;
		
        if($this->input->post())
        {
            foreach($lxx as $lxp)
            {
                $value = $this->input->post('s_'.$lxp->settingsid);
				
                $this->db->query("UPDATE ah_settings SET settingvalue='".$value."' WHERE settingsid='".$lxp->settingsid."'");
            }
			
			redirect(current_url());
			exit();
        }
		else
		{
			$this->load->view('allsettings',$this->_data);
		}
        
    }
	
	public function formbuilder()
	{
		$this->load->view("formbuilder",$this->_data);
	}
	
	public function icons()
	{
		$this->load->view("icons");
	}
	
	public function get_input($value)
	{
		$html = '<option value="">اختر وحدة</option>';		
		foreach($this->haya_model->get_listmanagment_types() as $gltkey => $gvalue)
		{
			$html .= '<option value="'.$gvalue.'" ';
			if($value==$gvalue)
			{
				$html .= 'selected="selected"';
			}
			$lab = $this->config->item('list_types');
			$html .= '>'.$lab[$gvalue]['ar'].'</option>';
		}
		echo $html;
	}
	
	public function form()
	{
		
		$this->_data['key'] = get_key();
			$this->load->view("form",$this->_data);
	}
	
	public function save_my_form()
	{
		$postData = $this->input->post();
		$field_type = sizeof($this->input->post("field_type"));
		for($i=0; $i<$field_type; $i++)
		{
			$ar[] = array(
					'ft'=>$postData['field_type'][$i],
					'fn'=>$postData['field_name'][$i],
					'fo'=>$postData['field_order'][$i],
					'fr'=>$postData['field_required'][$i],
					'fs'=>$postData['field_status'][$i],
					'fv'=>$postData['field_value'][$i]);
		}
		$myform['data'] = $ar;
		$custom_form = json_encode($myform);
		$mh_modules["module_parent"] = $postData['form_module'];
		$mh_modules["show_on_menu"] = '1';
		$mh_modules["module_order"] = $postData['form_order'];
		$mh_modules["module_icon"] = $postData['form_icon'];
		$mh_modules["module_name"] = $postData['form_name'];		
		$mh_modules["module_text"] = $postData['form_description'];
		$mh_modules["module_status"] = $postData['form_status'];
		$mh_modules["module_type"] = 'popup';
		$mh_modules["custom_form"] = $custom_form;		
		$this->db->insert('mh_modules',$mh_modules,json_encode($mh_modules),$this->_login_userid);
			$moduleid = $this->db->insert_id();
			$mm["module_controller"] = 'lease_programe/customform/'.$moduleid;
			$this->db->where('moduleid',$moduleid);
			$this->db->update('mh_modules',json_encode($mm),$this->_login_userid,$mm);
		//print_r($ar);
	}
	
	public function formlist()
	{
		$this->_data["flist"] = $this->loan->get_all_custom_form();
		$this->load->view("formlist", $this->_data);
	}
	
	public function customform($formid)
	{
		$this->_data["flist"] = $this->loan->get_all_custom_form($formid);
		$this->load->view("form_view", $this->_data);
	}
//-------------------------------------------------------------------------------
}
<div class="panel panel-default panel-block panel-title-block">
  <div class="panel-heading">
    <div> <i class="<?PHP echo $module['module_icon']; ?>"></i>
      <h1><?PHP echo($module['module_name']); ?>       		
      <small><?PHP echo $module['module_text']; ?></small> </h1>
      <div class="pull-left add" id="sortable">
        <?PHP echo $this->haya_model->childMenu($module['module_parent']); ?>
      </div>
    </div>
  </div>
</div>
<script>
  $(function() {
    $( "#sortable" ).sortable({
        update: function( event, ui ) {
            var mydata = '';
            $('.ui-state-default').each(function(index){
                mydata += $(this).attr('data-value')+'-';  
            });
            var taurusData = $.ajax({
				  url: config.BASE_URL+'lease_programe/savesorting',
				  type:"POST",
				  data:{my:mydata},								
				  success: function(msg)
				  {}});
          }
      });
    $( "#sortable" ).disableSelection();
  });
  </script>
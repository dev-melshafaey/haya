
<section class="sidebar retracted"> 
  <script>
                if ($.cookie('protonSidebar') == 'retracted') {
                    $('.sidebar').removeClass('extended').addClass('retracted');
                    $('.wrapper').removeClass('retracted').addClass('extended');
					
                }
                if ($.cookie('protonSidebar') == 'extended') {
                    $('.wrapper').removeClass('extended').addClass('retracted');
                    $('.sidebar').removeClass('retracted').addClass('extended');
					
                }
            </script>
  <div class="panel panel-default"  style="height:600px !important; overflow-x: scroll; overflow-y: scroll;">
    <?PHP 
	
					foreach($history as $key => $value) { 
				
					?>
    <table style="text-align:center;" class="table" id="tableSortable3" aria-describedby="tableSortable_info">
      <thead>
        <tr role="row">
          <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="2" style="text-align:center; background-color: #000; color: #FFF;"> <div class="table-responsive">
              <div class="col-md-4"><a href="#">أخر عملية</a><br>
                <?PHP show_date($value['system_date'],7); ?>
              </div>
              <div class="col-md-4"><a href="#">الساعه</a><br>
                <?PHP echo $value['system_time']?></div>
              <div class="col-md-4"><a href="#">تاريخ</a><br>
                <?PHP echo $value['system_date']?></div>
            </div>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr class="gradeA odd" bgcolor="#f5f5f5">
          <td colspan="2" width="303" class=" sorting_1"  style="background-color:#FFF !important;"><strong style="font-size:19px !important;" class="text-warning"><?PHP echo $value['admin_name']?></strong></td>
        </tr>
        <?PHP if($value['notes']!='') { ?>
        <tr class="gradeA even">
          <td colspan="2" align="right" class=" sorting_1" style="background-color:#EFEFEF !important;"><h4>
              <p>الملاحظات</p>
            </h4>
            <p><?PHP echo $value['notes']?></p></td>
        </tr>
        <?PHP } ?>
         <?PHP if($value['ntext']!='') { ?>
        <tr class="gradeA even">
          <td class=" sorting_1"  style="background-color:#FFF !important; border-bottom: 2px solid #d09c0d;"><strong>تفاصيل الإستفسار </strong></td>
          <td class=" sorting_1" align="right"  style="background-color:#FFF !important; border-bottom: 2px solid #d09c0d;"><p> <?PHP echo $value['ntext']; ?> </p></td>
        </tr>
        <?PHP } ?>
        <tr class="gradeA odd">
          <td class=" sorting_1"  style="background-color:#FFF !important; border-bottom: 2px solid #d09c0d;"><strong> نوع الإستفسار </strong></td>
          <td class=" sorting_1" align="right"  style="background-color:#FFF !important; border-bottom: 2px solid #d09c0d;"><p>
              <?PHP foreach($value['inq'] as $inqkey => $inqdata) { ?>
              <span class="text-success"><?PHP echo $inqdata['name']; ?></span><br />
              <?PHP } ?>
            </p></td>
        </tr>
       
      </tbody>
    </table>
    <?PHP } ?>
  </div>
  <div class="sidebar-handle"> <i class="icon-ellipsis-horizontal"></i> <i class="icon-ellipsis-vertical"></i> </div>
</section>

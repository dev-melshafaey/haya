<?php if($userid):?>
<?php $next_func	=	$this->uri->segment(2);?>
<div class="col-md-12">
  <ul class="nav nav-tabs panel panel-default panel-block">
    <li class="tabsdemo-1 <?php if($next_func	==	'add_user'):?>active <?php endif;?>"><a href="<?php echo base_url();?>users/add_user/<?php echo $userid;?>">تحرير المستخدم</a></li>
    <li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
    <li class="tabsdemo-1 <?php if($next_func	==	'bank_accounts'):?>active <?php endif;?>"><a href="<?php echo base_url();?>users/bank_accounts/<?php echo $userid;?>">إضافة حساب البنك</a></li>
    <li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
    <li class="tabsdemo-2 <?php if($next_func	==	'communications'):?>active <?php endif;?>"><a href="<?php echo base_url();?>users/communications/<?php echo $userid;?>">إضافة الاتصالات</a></li>
    <li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
    <li class="tabsdemo-3 <?php if($next_func	==	'family_members'):?>active <?php endif;?>"><a href="<?php echo base_url();?>users/family_members/<?php echo $userid;?>"  >إضافة أفراد الأسرة</a></li>
    <li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
    <li class="tabsdemo-4 <?php if($next_func	==	'documents'):?>active <?php endif;?>"><a href="<?php echo base_url();?>users/documents/<?php echo $userid;?>" >إضافة الوثيقة</a></li>
    <li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
    <li class="tabsdemo-5 <?php if($next_func	==	'salaries'):?>active <?php endif;?>"><a href="<?php echo base_url();?>users/salaries/<?php echo $userid;?>" >إضافة الراتب</a></li>
    <li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
    <li class="tabsdemo-6 <?php if($next_func	==	'experiences'):?>active <?php endif;?>"><a href="<?php echo base_url();?>users/experiences/<?php echo $userid;?>">إضافة الخبرة</a></li>
  </ul>
</div>
<?php endif;?>

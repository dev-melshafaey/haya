<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $this->config->item('project_name');?></title>
<link rel="icon" type="image/png" href="../images/favicon.png" />
<!--<link rel='stylesheet' id='google-fonts-css' href='https://fonts.googleapis.com/css?family=Open+Sans%3A400%2C600%2C700%2C300&#038;ver=1' type='text/css' media='screen' />
-->
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/amaran.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.datetimepicker.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/tipped.css?v=2.1.5" media="screen" />
<!--<link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet">  --> 
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.dataTables.min.css">
<script src="<?php echo base_url();?>js/jquery-1.10.2.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.js"></script>
<script src="<?php echo base_url();?>js/jquery-ui.js"></script>
<script src="<?php echo base_url();?>js/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url();?>js/jquery.filter_input.js"></script>
<script src="<?php echo base_url();?>js/jquery.feedback_me.js"></script>
<script src="<?php echo base_url();?>js/jquery.amaran.min.js"></script>
<script src="<?php echo base_url();?>js/hd_script.js"></script>
<script src="<?php echo base_url();?>js/highcharts-custom.js"></script>
<script src="<?PHP echo base_url();?>js/highcharts-3d.js"></script>
<script src="<?PHP echo base_url();?>js/exporting.js"></script>
<script src="<?PHP echo base_url();?>js/raphael-min.js"></script>
<script src="<?PHP echo base_url();?>js/raphael-font.js"></script>
<script src="<?PHP echo base_url();?>js/breadcrumb.js"></script>
<script src="<?PHP echo base_url();?>js/shortcut.js"></script>
<script src="<?PHP echo base_url();?>js/jquery.tipsy.js"></script>
<script src="<?PHP echo base_url();?>js/jquery.datetimepicker.js"></script>
<script src="<?PHP echo base_url();?>js/jquery.fancybox.js?v=2.1.5"></script>
<script src="<?PHP echo base_url();?>js/html5.js"></script>
<script src="<?PHP echo base_url();?>js/jquery.dataTables.js"></script>
<script src="<?PHP echo base_url();?>js/jquery.highlight-4.js"></script>
<script src="<?PHP echo base_url();?>js/tipped.js"></script>
<script>
var config	=	
{
	BASE_URL		: '<?php echo base_url();?>',
	AJAX_URL		: '<?php echo base_url();?>ajax/',
	CURRENT_URL		: '<?php echo current_url();?>',
	MODULE_ID		: '<?php echo $module['moduleid'];?>',
	ORDER_ID		: '<?php echo get_order_id();?>',
	MODULE_NAME		: '<?php echo $module['module_name'];?>',
	CONTROLLER_NAME	: '<?php echo $this->uri->segment('1');?>',
	METHOD_NAME		: '<?php echo $this->uri->segment('2');?>'
}

function addclass()
{
	$(".menu_items li").parent().find('li').removeClass("close");
}
function ddx(msg)
{
	$('#msgx').html(' ');
	$('#msgx').html(msg);
	$( "#dialog-message" ).dialog({
	  modal: true,
	  buttons: {
		"موافق": function() {
		  $( this ).dialog( "close" );
		}
	  }
	});
}

</script>
<script type="text/javascript">
	
	function startTime() 
	{
		var digits = new Array("٠","١","٢","٣","٤","٥","٦","٧","٨","٩");
		var today=new Date();
		var h=today.getHours();
		var m=today.getMinutes();
		var s=today.getSeconds();
		m = checkTime(m);
		s = checkTime(s);
		document.getElementById('digitalclock').innerHTML = h+":"+m+":"+s;
		var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
  if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}
	/**
	* Initiates Graph Functions
	**/
	function graphit($graph_id,$lines,$bar_margins,$bar_speed,$animate){
		
		$v = new Object(); // create graph object
		$v.graphid = $graph_id; // id of graph container, example "graph1" or "myGraph"
		$v.values = new Array(); // array of values
		$v.heights = new Array(); // array of bar heights
		$v.colors = new Array(); // colors for bars
		$v.lines = $lines; // number of lines - keep this 10 unless you want to write a bunch more code
		$v.bm = $bar_margins; // margins between the bars
		$v.mx = 0; // highest number, or rounded up number
		$v.gw = $('#'+$v.graphid+' .graph').width(); // graph width	
		$v.gh = $('#'+$v.graphid+' .graph').height(); // graph height
		$v.speed = $bar_speed; // speed for bar animation in milliseconds
		$v.animate = $animate; // determines if animation on bars are run, set to FALSE if multiple charts
		
		getValues(); // load the values & colors for bars into $v object	
		graphLines(); // makes the lines for the chart
		graphBars(); // make the bars
		if($v.animate)
			animateBars(0); // animate and show the bars
	}
	
	/**
	* Makes the HTML for the lines on the chart, and places them into the page.
	**/
	function graphLines(){
		$r = ($v.mx < 100)?10:100; // determine to round up to 10 or 100
		$v.mx = roundUp($v.mx,$r); // round up to get the max number for lines on chart
		$d = $v.mx / $v.lines; // determines the increment for the chart line numbers	
		
		// Loop through and create the html for the divs that will make up the lines & numbers
		$html = ""; $i = $v.mx;			
		if($i>0 && $d>0){
			while($i >= 0){
				$html += graphLinesHelper($i, $v.mx);
				$i = $i - $d;
			}
		}
		$('#'+$v.graphid+' .graph').html($html); // Put the lines into the html		
		$margin = $v.gh / $v.lines; // Determine the margin size for line spacing
		$('#'+$v.graphid+' .line').css("margin-bottom",$margin + "px");	// Add the margins to the lines			
	}
	
	/**
	* Creates the html for the graph lines and numbers
	**/
	function graphLinesHelper($num, $maxNum){
		$fix = ($i == $maxNum || $i == 0)? "fix ":""; // adds class .fix, which removes the "border" for top and bottom lines
		return "<div class='"+$fix+"line'><span>" + $num + "</span></div>";
	}
	
	/**
	* A Simple Round Up Function
	**/
	function roundUp($n,$r){
		return (($n%$r) > 0)?$n-($n%$r) + $r:$n;
	}
	
	/**
	* Gets the values & colors from the HTML <labels> and saves them into $v ohject
	**/
	function getValues(){			
		$lbls = $('#'+$v.graphid+' .values span'); // assigns the span DOM object to be looped through
		// loop through
		for($i=0;$i <= $lbls.length-1; $i++){
			$vals = parseFloat($lbls.eq($i).text());
			$v.colors.push($lbls.eq($i).css('background-color'));
			$v.mx = ($vals > $v.mx)?$vals:$v.mx;
			$v.values.push($vals);
		}
	}
	
	/**
	* Creates the HTML for the Bars, adds colors, widths, and margins for proper spacing. 
	* Then Puts it on the page.
	**/
	function graphBars(){			
		$xbars  = $v.values.length; // number of bars	
		$barW	= ($v.gw-($xbars * ($v.bm))) / $xbars; 
		$mL 	= ($('#'+$v.graphid+' .line span').width()) + ($v.bm/2);			
		$html="";
		for($i=1;$i<=$xbars;$i++){
			$v.heights.push(($v.gh / $v.mx) * $v.values[$i-1]);
			$ht = ($v.animate == true)?0:$v.heights[$i-1];
			$html += "<div class='bar' id='"+$v.graphid+"_bar_"+($i-1)+"' style='height: "+$ht+"px; margin-top: -"+($ht+1)+"px; ";
			$html += "background-color: "+$v.colors[$i-1]+"; margin-left: "+$mL+"px'>&nbsp;</div>";
			$mL = $mL + $barW + $v.bm;
		}
		$($html).insertAfter('#'+$v.graphid+' .graph');
		$('#'+$v.graphid+' .bar').css("width", $barW + "px");			
	}
	
	/**
	* Animates the Bars to the correct heights.
	**/
	function animateBars($i){
		if($i == $v.values.length){ return; }
		$('#'+$v.graphid+'_bar_'+$i).animate({
			marginTop: "-" + ($v.heights[$i] + 1) + "px",
			height: ($v.heights[$i]) + "px"
		},$v.speed,"swing", function(){animateBars($i+1); });
	}

	function add_requestmuwafiq(msg)
	{
		$.amaran({
			content:{
				bgcolor:'#8e44ad',
				color:'#fff',
				message:msg
			   },
			theme:'colorful',
			position:'bottom center',
			closeButton:false,
			cssanimationIn: 'rubberBand',
			cssanimationOut: 'bounceOutUp'
		 
		});
	}

</script>

<!-- end of chart-->

<script type="text/javascript">
	$(document).ready(function(){	
	
		
								   
		$graph_id    = 'testgraph'; // id of graph container	
		$lines 		 = 5; // number of lines - keep this 10 unless you want to write a bunch more code
		$bar_margins = 20; // margins between the bars
		$bar_speed 	 = 500; // speed for bar animation in milliseconds		
		$animate 	 = true; // set to false if multiple charts on one page
		graphit($graph_id,$lines,$bar_margins,$bar_speed, $animate);

 		
	});
</script>
<script type="text/javascript">
	
	$(document).ready(function () {
		 startTime();
		 $('#addmore_partner').hide();
		 $('.needtip').tipsy({fade: true,html: true, gravity: 'w'});
		 shortcut.add("Ctrl+F1",function() { location.href = config.BASE_URL+'inquiries/resetinq'; });
		 shortcut.add("Ctrl+F2",function() { location.href = config.BASE_URL+'inquiries/newrequest'; });
		 $('.fancybox').fancybox({
			maxWidth	: 1024,
			maxHeight	: 150,
			fitToView	: false,
			width		: '100%',
			height		: '80%',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'fade',
    		closeEffect	: 'fade'
			 
			 });
			 
			 $("#prin").click(function(){
			  	//alert('pp');
				//window.print();
				//return false;
				window.top.location  = config.BASE_URL+'inquiries/transactionsprint/';
			  })
			
		 $('.timeline').click(function(){
				var aad = $(this).attr('id');
				$('.hidetimeline').hide();
					var applicant_info = $.ajax({
						  url: config.AJAX_URL+'timeline',
						  type: "POST",
						  data:{pid:aad},
						  dataType: "html",
						  success: function(msg){
							  $('#timeline'+aad).show();
							  $('#time_data_'+aad).html(msg);
							 }
						});
		 });
		 
		 //Saving Applicant Info
		///////////////////////////////////		 
		 $('.tempapplicant').blur(function(){
			  var reviews = $.trim($('#review').val());
			 var value = $.trim($(this).val());
			 var column = $.trim($(this).attr('id'));
			 var data_attribute = $(this).attr('data-handler').split('_');
			 if(value!='')
			 {	var applicant_info = $.ajax({
					  url: config.AJAX_URL+'saveTempInfo',
					  type: "POST",
					  data:{value:value,tempid:data_attribute[0],applicantid:data_attribute[1],type:'Applicant',column:column,reviews:reviews},
					  dataType: "html",
					  success: function(msg){}
					});
			 }
			 
		 });
		 
		 $('.extraInput').change(function(){
						    
				var dataid = $('#'+$(this).attr('id')+' option:selected').attr('dataid').split(':');
				var parent = $(this).parent();
				var name_obj = dataid[1]+'_'+dataid[2];
				var vali = dataid[0];
				var nali = dataid[1];
				var tali = dataid[2];
				console.log(name_obj);
				return false;				
				if(vali=='1')
				{	document.getElementsByName(name_obj).style.display = 'block';	}
				else
				{	document.getElementsByName(name_obj).style.display = 'none';}
			
			
		});
		 
		 $('.checkother').change(function(){
				var dataid = $('#'+$(this).attr('id')+' option:selected').attr('dataid').split(':');
				var vali = dataid[0];
				var nali = dataid[1];
				
				if(vali=='1')
				{
					$('#'+nali).fadeIn('slow');
				}
				else
				{
					$('#'+nali).fadeOut('slow');
				}
		 });
		 

		 
		 $('.tempmain').blur(function(){
			  var reviews = $.trim($('#review').val());
			 var value = $.trim($(this).val());
			 var column = $.trim($(this).attr('id'));
			 var data_attribute = $(this).attr('data-handler').split('_');			 
			// alert(val);
			 if(value!='')
			 {	var applicant_info = $.ajax({
					  url: config.AJAX_URL+'saveTempInfo',
					  type: "POST",
					  data:{value:value,tempid:data_attribute[0],applicantid:data_attribute[1],type:'Main',column:column,reviews:reviews},
					  dataType: "html",
					  success: function(msg){
						  
						 }
					});
			 }			 
		 });
		  $('.temptypemain').click(function(){
			  var reviews = $.trim($('#review').val());
			 var value = $.trim($(this).val());
			 var column = $.trim($(this).attr('id'));
			 var data_attribute = $(this).attr('data-handler').split('_');			 
			 //alert(val);
			 if(value!='')
			 {	var applicant_info = $.ajax({
					  url: config.AJAX_URL+'saveTempInfo',
					  type: "POST",
					  data:{value:value,tempid:data_attribute[0],applicantid:data_attribute[1],type:'Main',column:column,reviews:reviews},
					  dataType: "html",
					  success: function(msg){
						  
						  	
						 }
					});
			 }			 
		 });
		 //addPartners();
		 
		 $('.saveoptions').blur(function(){
			  var reviews = $.trim($('#review').val());
			 var value = $.trim($(this).val());
			 var column = $.trim($(this).attr('id'));
			 var data_attribute = $(this).attr('data-handler').split('_');			 
			 if(value!='')
			 {	var applicant_info = $.ajax({
					  url: config.AJAX_URL+'saveTempInfo',
					  type: "POST",
					  data:{value:value,tempid:data_attribute[0],applicantid:data_attribute[1],type:'Main',column:column,reviews:reviews},
					  dataType: "html",
					  success: function(msg){}
					});
			 }			 
		 });
		 
		 $('.inquirytypeid').click(function(){
			 var cntlen = 0;
			 var reviews = $.trim($('#review').val());
			 if(reviews!='')
			 {
				 $('.inquirytypeid').each(function(index, element) {
					if ( $(this).is(':checked') ) 
					{
						cntlen+=1;
					}
				});
				$('#mulit_count').html(cntlen);
				if(cntlen > 0)
				{
					$('.multiboxsave').slideDown('slow');
				}
				else
				{
					$('.multiboxsave').slideUp('slow');
				}
			 }
		 });
		 
		 $('.extraInput ').change(function(){
			//console.log($(this).data());
			//console.log($(this).find(':selected').data())
			drpId = $(this).attr('id');
			 console.log($(this).find('option').data());
			 //alert($(this).val());
			 v = $(this).val();
			 other =$("#other"+v).val();
			 if($("#other"+v).length){
				 $("#"+drpId+'_text').show();
			 }
			 else{
			 	$("#"+drpId+'_text').hide();
			 }
		});
		 
		 
		 $('#notestext').blur(function(){
			 var reviews = $.trim($('#review').val());
			 var value = $.trim($(this).val());
			 var column = $.trim($(this).attr('id'));
			 var data_attribute = $(this).attr('data-handler');			 
			 if(value!='ملاحظات' && reviews=='1')
			 {	
			 	var mytxt = '';
			 	$('.inquirytypeid').each(function(index, element) {
                    if($(this).prop('checked'))
					{						
						var propData = $('#inqirydate'+$(this).val()).val();						
						mytxt += $(this).val()+'_'+propData+',';					
					}
                });
			 	var applicant_info = $.ajax({
					  url: config.AJAX_URL+'saveTempInfo',
					  type: "POST",
					  data:{value:value,tempid:data_attribute,type:'Notes',column:column,reviews:reviews,txt:mytxt},
					  dataType: "html",
					  beforeSend: function(){
						  
						  },					  
					  success: function(msg){
						  if($.trim(msg)==1)
						  {
						  		var coming = $.ajax({
								  url: config.AJAX_URL+'history/'+data_attribute,
								  type: "POST",
								  data:{value:1},
								  dataType: "html",
								  success: function(msg){ 
								   if(reviews!='')
								   { 
									$('#feedback_content').html(msg);
									$('#feedback_content').show();
									$('#feedback_trigger').show();
									$('#feedback_trigger').click();
								   }
									$('#notestext').val(' ');
								  }
								});
						  }
						}
					});
			 }			 
		 });

		 	$('.tempinqury').click(function(){
			 var reviews = $.trim($('#review').val());
			 var value = $.trim($(this).val());
			 var column = $.trim($(this).attr('id'));
			 var data_attribute = $(this).attr('data-handler').split('_');		
			 if($(this).prop('checked'))
			 {
				 $('.mydatepicker'+value).show();
			 }
			 else
			 {
				 $('.mydatepicker'+value).hide();
			 }
			 /*if(value!='')
			 {	var applicant_info = $.ajax({
					  url: config.AJAX_URL+'saveTempInfo',
					  type: "POST",
					  data:{value:value,tempid:data_attribute[0],applicantid:data_attribute[1],type:'Inquiry',column:column,reviews:reviews},
					  dataType: "html",
					  success: function(msg){}
					});
			 }		*/	 
		 });
		 	
			$('.addnewphone').click(function(){
				//alert('asd');
				 var vox = $(this).attr('data-on').split('_');
				 var reviews = $.trim($('#review').val());
				 var newphones = $.ajax({
					 url: config.AJAX_URL+'new_phone',
				  	type: "POST",
				  data: { tempid:vox[0],applicantid:vox[1],reviews:reviews},
				  dataType: "html",
				  success: function(msg){
					
					$('#hatfi'+vox[1]).last().after(msg)
					
				  }
				});
				
				});
				
				$('#addnewmusanif').click(function(){
				 var newphones = $.ajax({
						  url: config.AJAX_URL+'new_musanif',
						  type: "POST",
						  data: {},
						  dataType: "html",
						  success: function(msg){
							
							//$('#hatfi'+vox[1]).last().after(msg)
							
						  }
						});
				
				});
		     //Auto Complete/////////////////////////////////
			$( ".autocomplete" ).autocomplete({
			  source:config.AJAX_URL+'getIDCardNumber',
			  minLength:3,
			  select: function( event, ui ) {	
			  	location.href = config.CURRENT_URL+'/'+ui.item.id			
									
			  }
			});
			$( ".applicantphone" ).autocomplete({
			  source:config.AJAX_URL+'getApplicantPhone',
			  minLength:3,
			  select: function( event, ui )
			  {	
			  	location.href = config.BASE_URL+'inquiries/newinquery/'+ui.item.id			
									
			  }
			});
			
			
			
			$( ".search_field" ).autocomplete({
			  source:config.AJAX_URL+'getListofApplicant',
			  minLength:3,
			  select: function( event, ui )
			  {	
			  	location.href = config.BASE_URL+'inquiries/newinquery/'+ui.item.id			
									
			  }
			});
			/////////////////////////////////////////////////
		 fm_options = {
        position: "left-bottom",
        name_required: true,
        message_placeholder: "Go ahead, type your feedback here...",
        message_required: true,
        show_asterisk_for_required: true,
        feedback_url: "send_feedback_clean",
        custom_params: {
            csrf: "my_secret_token",
            user_id: "john_doe",
            feedback_type: "clean"
        }
    };
    //init feedback_me plugin
    fm.init(fm_options);
		 //Form Validation
		 /*$('.req').blur(function(){
			if($.trim($(this).val())=='')
			{
				$(this).addClass('redline');
				$(this).removeClass('greenline');
				var place_holder = ' طلب '+$(this).attr('placeholder');
				var objid = $(this).attr('id');
				$('#'+objid).focus(); 
				ddx(place_holder);
				
			}
			else
			{
				$(this).removeClass('redline');
				$(this).addClass('greenline');
			}
		 });*/
		 
		
		 
		 $('.delete-btn').click(function(e){
			 e.preventDefault();

			 var url_redirect	=	$(this).attr("data-url");
			 var did = $(this).attr('id');
			 
			

			 $( "#dialog-confirm" ).dialog({
			  resizable: false,
			  height:200,
			  buttons: {
				"حذف": function() {
					 $('#bingo'+did).hide();
					var request = $.ajax({
					  url: url_redirect,
					  dataType: "html",
					  success: function(msg){
						$('#bingo'+did).hide();
					  }
					});
				  $( this ).dialog( "close" );
				},
				"إلغاء": function() {
				  $( this ).dialog( "close" );
				}
			  }
			});
				
				
			});
	
		$('.parent_role_class').change(function(){
			$('#get_parent_roles').html('');
		var vals = $(this).val();
		$.ajax({
			url: config.AJAX_URL+'getParentsRoles',
			type: "POST",
			data: { parent_role_id : vals },
			dataType: "html",
			success: function(msg){
				  $('#get_parent_roles').html(msg);
			}
		});

	})
	
	$('.checklist').click(function(e){
			// alert('click');
			var id = $(this).attr('id');
			 
			 $(".show-content").html('');
			 $(".show-content").load(config.BASE_URL+'inquiries/get_check_list/'+id);
			 e.preventDefault();
			 
			 $( "#set-dialog-message-3" ).dialog({
					draggable: false,
					show: "fade", 
					hide: "explode",
					height:500,
					width:860,
					modal: true,
					buttons: {
					Ok: function() 
					{
						$(".show-content").val('');
						$( this ).dialog( "close" );
					}
					  }
					});
	});
	$('.parent').change(function(){
			//$('#get_parent_roles').html('');
		parent_id = $('.parent').val();
		//var vals = $(this).val();
		$.ajax({
			url: config.AJAX_URL+'getChildData',
			type: "POST",
			data: { parent_role_id : parent_id },
			dataType: "html",
			success: function(msg){
				  $('#get_child_roles').html(msg);
				  $("#child_parent").show();
			}
		});
	});
	
	$('.detail').click(function(e){
			 
			var id = $(this).attr('id');
			 
			 $(".show-content").html('');
			 $(".show-content").load(config.BASE_URL+'inquiries/get_applicant_data/'+id);
			 e.preventDefault();
			 
			 $( "#set-dialog-message-2" ).dialog({
					draggable: false,
					show: "fade", 
					hide: "explode",
					height:500,
					width:600,
					modal: true,
					buttons: {
					Ok: function() 
					{
						$(".show-content").val('');
						$( this ).dialog( "close" );
					}
					  }
					});
	});
	
	
	$('.history').click(function(){
				var id = $(this).attr('id');
			 
			 $(".show-content").html('');
			 $(".show-content").load(config.BASE_URL+'inquiries/getHistory/'+id);
			 
			 $( "#set-dialog-message" ).dialog({
					draggable: false,
					show: "fade", 
					hide: "explode",
					height:500,
					width:600,
					modal: true,
					buttons: {
					Ok: function() 
					{
						$(".show-content").val('');
						$( this ).dialog( "close" );
					}
					  }
					});
		
		});
	//
	$('.detail-view-inquiry').click(function(e){
		 e.preventDefault();
			 var id = $(this).attr('id');
			 $(".show-content").html('');
			e.preventDefault();
			 
			 	var request = $.ajax({
					  url: config.BASE_URL+'inquiries/get_auditor_data/'+id,
					  type: "POST",
					  data: { id : id },
					  dataType: "html",
					  success: function(msg){
						  $(".show-content").html(msg);
						  
						  			$( "#set-dialog-message" ).dialog({
									resizable: false,
									height:500,
									width:600,
									modal: true,
									buttons: {
									'حسنا': function() {
										$(".show-content").html('');
										$( this ).dialog( "close" );
										},
									'طباعة تفاصيل': function() {											
											printDiv();
											$( this ).dialog( "close" );
										}	
									  }
									});
					  }
					});
			  
	});	
	
	function printDiv() {    
           var divContents = $("#printable").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css" /><title></title>');
            printWindow.document.write('</head><body style="direction:rtl !important;; text-align:right;">');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
	}
		$(".ins").change(function(){
				if($(this).val() == "Y")
				{
					$('#insinfo').slideDown('slow');
				//	$('#insurance_number').addClass('req');
				}
				else
				{
					$('#insinfo').slideUp('slow');
				//	$('#insurance_number').removeClass('req');
				}					
			});
	$('.multiboxsave').click(function(){
				var reviews = $.trim($('#review').val());
				var value = $.trim($('#notestext').val());
				var column = $.trim($('#notestext').attr('id'));
				var data_attribute = $('#notestext').attr('data-handler');				 		
					var mytxt = '';
					$('.inquirytypeid').each(function(index, element) 
					{
						if($(this).prop('checked'))
						{
							var propData = $('#inqirydate'+$(this).val()).val();
							mytxt += $(this).val()+'_'+propData+',';
						}
					});
					var applicant_info = $.ajax({
						  url: config.AJAX_URL+'saveTempInfo',
						  type: "POST",
						  data:{value:value,tempid:$('#tempid').val(),type:'Notes',column:column,reviews:reviews,txt:mytxt},
						  dataType: "html",					  
						  success: function(msg){
							  if($.trim(msg)==1)
						  		{
						  		var coming = $.ajax({
								  url: config.AJAX_URL+'history/'+data_attribute,
								  type: "POST",
								  data:{value:1},
								  dataType: "html",
								  success: function(msg){ 
								   if(reviews!='')
								   { 
									$('#feedback_content').html(msg);
									$('#feedback_content').show();
									$('#feedback_trigger').show();
									$('#feedback_trigger').click();
								   }
									$('#notestext').val(' ');
								  }
								});
						  }
							  }
						});	
			
		});	
	$('#save_data_inquery').click(function(){
			//Checking Error of The Form
			 $('.req').removeClass('redline');
			 var ht = '<ul>';
			 	$('.req').each(function(index, element) {
                    if($(this).val()=='')
					{
						$(this).addClass('redline');
						ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
					}
                });
			  var redline = $('.redline').length;
			  ht += '</ul>';
			  //IF no error found in form it will execute the first part			 
			  if(redline <= 0)
			  {	 
			  	  //Saving Mulazhat///////////////
				  ////////////////////////////////
					var reviews = $.trim($('#review').val());
					var value = $.trim($('#notestext').val());
					var column = $.trim($('#notestext').attr('id'));
					var data_attribute = $('#notestext').attr('data-handler');				 		
						var mytxt = '';
						$('.inquirytypeid').each(function(index, element) 
						{
							if($(this).prop('checked'))
							{
								var propData = $('#inqirydate'+$(this).val()).val();
								mytxt += $(this).val()+'_'+propData+',';
							}
						});
						if(value!='' && value!='ملاحظات')
						{	var applicant_info = $.ajax({
							  url: config.AJAX_URL+'saveTempInfo',
							  type: "POST",
							  data:{value:value,tempid:$('#tempid').val(),type:'Notes',column:column,reviews:reviews,txt:mytxt},
							  dataType: "html",					  
							  success: function(msg){
								  	//saveMurageen();
								  }
							});	}
						saveMurageen();
					}
					else
					{
						ddx(ht);
					}
				});
	function saveMurageen()
	{
			var tempid = $('#tempid').val();
			var request = $.ajax({
			  url: config.BASE_URL+'inquiries/add_data_into_main/'+tempid,					  
			  dataType: "html",
			  beforeSend: function(){
				 $( "#dialog-confirm_dd" ).dialog({
									resizable: false,
									height:500,
									width:600,
									modal: true,
									buttons: {
									Ok: function() {
										$(".show-content").html('');
									$( this ).dialog( "close" );
										}
									  }
									}); 
				  },
			  complete: function(){$( "#dialog-confirm_dd" ).dialog( "close" );},					  
			  success: function(msg)
			  {
				  
				  updateMurajeenData();
				 /* $.amaran({
					  content:{
						  bgcolor:'#8e44ad',
						  color:'#fff',
						  message:'وقد أضيف إلى الاستعلام عن في النظام'},
						  theme:'colorful',
						  position:'bottom center',
						  closeButton:false,
						  cssanimationIn: 'rubberBand',
						  cssanimationOut: 'bounceOutUp',
						  beforeStart: function()
						  {	location.href = config.CURRENT_URL;	}
						});*/
					}
			});
		}
		
		/*$('#save_data_form_step2').click(function(){
			location.href = 'http://www.hotmail.com';
			console.log('ewrwer');
			
			return;
			 $('.req').removeClass('redline');
			 var ht = '<ul>';
			 	$('.req').each(function(index, element) {
					
					
                    if($(this).val()=='')
					{
						$(this).addClass('redline');
						ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
					}
                });
			  var redline = $('.redline').length;
			  ht += '</ul>';
			 	 
			  if(redline <= 0)
			  {	
			  	$("#validate_form_step2").submit();
			  }
			  else
			  {
				   ddx(ht);
			  }
		});*/
		
		//Adding Key Event in The Doc
		
		//////////////////////////////////////////////////////
		$('#save_data_form').click(function(){
			 $('.req').removeClass('redline');
			 var ht = '<ul>';
			 	$('.req').each(function(index, element) {
					
					
                    if($(this).val()=='')
					{
						$(this).addClass('redline');
						ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
					}
                });
			  var redline = $('.redline').length;
			  ht += '</ul>';
			 	 
			  if(redline <= 0)
			  {	
			  	$("#validate_form").submit();
			  }
			  else
			  {
				   ddx(ht);
			  }
		});
		$('#save_data_form_new').click(function(){
			 $('.req').removeClass('redline');
			 var ht = '<ul>';
			 	$('.req').each(function(index, element) {
					
					
                    if($(this).val()=='')
					{
						$(this).addClass('redline');
						ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
					}
                });
			  var redline = $('.redline').length;
			  ht += '</ul>';
			 	 
			  if(redline <= 0)
			  {	
			  	var pass			=	$("#password").val();
				var confirm_pass	=	$("#confirm_password").val();
				
				
				if(pass	!= confirm_pass)
				{
					$("#password").addClass('redline');
					$("#confirm_password").addClass('redline');
					
					ddx("و كلمات السر لا تتطابق مطابقة");
					
				}
				else
				{
					$("#validate_form_new").submit();
				}
			  }
			  else
			  {
				   ddx(ht);
			  }
		});
		 
		$('.TextInput').filter_input({regex:'[a-zA-Z\u0600-\u06FF]'}); 
		$('.NumberInput').filter_input({regex:'[0-9]'});  
		
		 var d = new Date();
		 var year = d.getFullYear();
		 var maxyear = d.getFullYear()-200;
		 d.setFullYear(maxyear);
		 d.setFullYear(year);
		//$('#BirthDate').datepicker({ changeYear: true, changeMonth: true, yearRange: '1920:' + year + '', defaultDate: d});
$( "#start_date" ).datepicker({
			showAnim:'slide',
			changeMonth: true,
			changeYear: true,
			dateFormat:'yy-mm-dd',
			onSelect: function(selected,evnt) {}
		  });
		  
		  
		  	$("#getData").click(function(e) {
			//alert('ajax');
			//calc_form
            var request = $.ajax({
					  url: config.BASE_URL+'inquiries/get_calc_data/',
					  type: "POST",
					  data: $("#calc_form").serialize(),
					  dataType: "html",
					  beforeSend: function() {		},
					  complete: function(){  },
					  success: function(msg){
								$("#response").html(msg);
						  }
					});
        });
		
		
		  $( ".age_datepicker" ).datepicker({
			 showAnim:'slide',
			 changeMonth: true,
			  changeYear: true,
			  dateFormat:'yy-mm-dd',
			  yearRange: '1920:' + year + '', defaultDate: d,
			  onSelect: function(selected,evnt) {
					 	birthday = selected
  						birthday = new Date(birthday);
  						age = new Number((new Date().getTime() - birthday.getTime()) / 31536000000).toFixed(0);
						var namex = '#age_'+$(this).attr('name');
						var namexxx = 'age_'+$(this).attr('name');
						
						//alert($(this).attr('id'));
						cl_status = $(this).hasClass('p_age');
						if(cl_status){
							ind_st = $(this).index();
							//alert(age);
							ind_st = ind_st+1;
							//alert(ind_st);
								if(age < 18)
								{
									
									$(this).val(' ');
									ddx('غير مطابق لشروط العمر‎ شروط العمر ما بين ١٨ - ٥٥');
									$('#age_datepicker_'+ind_st).val(' ');
								}
								else if(age > 55)
								{
									$(this).val(' ');
									ddx("غير مطابق لشروط العمر‎ شروط العمر ما بين ١٨ - ٥٥");
									$('#age_datepicker_'+ind_st).val(' ');
								}
								else
								{
									$(this).val(selected);
									
									$('#age_datepicker_'+ind_st).val(age-1);
								}
								
							}
						else{
						
						if(age < 18)
						{
							
							$(this).val(' ');
							ddx('غير مطابق لشروط العمر‎ شروط العمر ما بين ١٨ - ٥٥');
							$(namex).val(' ');
						}
						else if(age > 55)
						{
							$(this).val(' ');
							ddx("غير مطابق لشروط العمر‎ شروط العمر ما بين ١٨ - ٥٥");
							$(namex).val(' ');
						}
						else
						{
							$(this).val(selected);
							
							$(namex).val(age-1);
						}
						
						}
				}
		  });
		  
		 		  
		  
		  $('.inquiry_type').click(function(){
			  var id = '#datepicker'+$(this).val();
			  if($(this).is(':checked'))
			  {
				  $(id).fadeIn('slow');
			  }
			  else
			  {
				  $(id).fadeOut('slow');
			  }
			 });
		  
		 var dx = new Date();
		 var start_year = dx.getFullYear();
		 var end_year = dx.getFullYear()+55;
		 dx.setFullYear(start_year);
		 dx.setFullYear(end_year);
		  
		  
		  $( "#loan_date" ).datepicker({
			 showAnim:'slide',
			 changeMonth: true,
			  changeYear: true,
			  dateFormat:'yy-mm-dd',
			  yearRange: start_year+':' + end_year + '', defaultDate: start_year
		  });
		  
		  $( "#foundation_date" ).datepicker({
			 showAnim:'slide',
			 changeMonth: true,
			  changeYear: true,
			  dateFormat:'yy-mm-dd',
			  yearRange: start_year+':' + end_year + '', defaultDate: start_year
		  });
		  
		  		  $( ".db" ).datepicker({
			 showAnim:'slide',
			 changeMonth: true,
			  changeYear: true,
			  dateFormat:'yy-mm-dd',
			  yearRange: start_year+':' + end_year + '', defaultDate: start_year
		  });
		  
		  
		  $('.dpicker').hide();
		  $( ".dpicker" ).datepicker({
			 showAnim:'slide',
			 changeMonth: true,
			  changeYear: true,
			  dateFormat:'yy-mm-dd',
			  yearRange: start_year+':' + end_year + '', defaultDate: start_year
		  });
		  
		  
		  
		  $('#loan_reason1').change(function(){
			 	if($(this).val()!='')
				{
					$('.ddx').show();
				}
				else
				{
					$('.ddx').hide();
				}
			 });
		  
		  $(".user_type").change(function(){
				if($(this).val() == "مشترك")
				{					
					$("#addmore_partner").show();
					$('#personal2').hide();
				}
				else
				{
					$('#personal2').show();
					$("#addmore_partner").hide();
				}					
			});
			
			$('#extrainfo').hide();
			$(".conf").change(function(){
				if($(this).val() == "Y")
				{
					$('#extrainfo').slideDown('slow');
					$('#extrainfo2').slideDown('slow');
					//
					$("#extrainfo_q").slideDown('slow');
					//$('#project_name, #project_location').addClass('req');
				}
				else
				{
					$("#extrainfo_q").slideUp('slow');
					$('#extrainfo').slideUp('slow');
					$('#extrainfo2').slideUp('slow');
					$('#project_name, #project_location').removeClass('req');
				}					
			});	
			
			$(".confirmation_q").change(function(){
				//alert($(this).val());
				if($(this).val() == "Y")
				{
					$('#question_details').slideDown('slow');
				}
				else
				{
					$("#question_details").slideUp('slow');
				}					
			});	
			//confirmation_q
			$('#province').change(function(){
				var province = $(this).val();
				var request = $.ajax({
					  url: config.AJAX_URL+'getWilayats',
					  type: "POST",
					  data: { province : province },
					  dataType: "html",
					  success: function(msg){
						  var walayalen = $('#walaya').length;
						  if(walayalen > 0)
						  {
							  $('#walaya').html(msg);
						  }
						  else
						  {
							  $('#wilayats').html(msg);
						  }
					  }
					});
				
			});
		/*$('#user_role_id').change(function(){
				var role_id = $(this).val();
				var request = $.ajax({
					  url: config.AJAX_URL+'getChilds',
					  type: "POST",
					  data: { role_id : role_id },
					  dataType: "html",
					  success: function(msg){
						$('#user_parent_role').html(msg);
					  }
					});
				
			});*/
			
			var cxt = 1;
			$('#addmore_partner').click(function(){
				var request = $.ajax({
					  url: config.AJAX_URL+'addNewPartner',
					  type: "POST",
					  data: { tempid : $('#tempid').val() },
					  dataType: "html",
					  success: function(msg){
						location.href = config.CURRENT_URL;
						//$('.bigbangtheory').after(msg);
					  }
					});
			
			
			
				
				//var personal = '<div class="personal" style="background-color:#EFEFEF;" id="pp'+cxt+'"><input style="float: left; font-size: 20px;" type="button" onclick="removeRow(\'pp'+cxt+'\')" id="remove" value="حذف" />'+$('#personalbingo').html()+'</div>';
				//$('#personalbingo').last().after(personal); // adding new tr after last tr of table
				//cxt++;				
			});
			
			$('#calc').click(function(e){
		//alert('asd');
			$( "#dialog-message2" ).dialog({
									resizable: false,
									height:600,
									width:714,
									modal: true,
									buttons: {
									Ok: function() {
										$(".show-content").html('');
									//$( this ).dialog( "close" );
										}
									  }
									});
			

			
			});
			
			
	});
	
	
function updateMurajeenData(){
			var request = $.ajax({
					  url: config.BASE_URL+'inquiries/updateMurajeen/',
					  type: "POST",
					  data: $("#form2").serialize(),
					  dataType: "html",
					  beforeSend: function() {		},
					  complete: function(){  },
					  success: function(msg){
												  $.amaran({
								  content:{
									  bgcolor:'#8e44ad',
									  color:'#fff',
									  message:'وقد أضيف إلى الاستعلام عن في النظام'},
									  theme:'colorful',
									  position:'bottom center',
									  closeButton:false,
									  cssanimationIn: 'rubberBand',
									  cssanimationOut: 'bounceOutUp',
									  beforeStart: function()
									  {  location.href = config.CURRENT_URL;
										}
									});
						  }
					});
		}	
		
		function getParnterWilaya(ind){

				console.log(ind);
					//p= $(obj).hasClass('partnr');
					//alert(p);
					//pat = $(obj +"partnr").index();
					//alert(ind);
					$('.partnr').eq(ind).val();
				var province = $('.partnr').eq(ind).val();
				var request = $.ajax({
					  url: config.AJAX_URL+'getWilayats',
					  type: "POST",
					  data: { province : province },
					  dataType: "html",
					  success: function(msg){
						 // var walayalen = $('#walaya').length;
						//  if(walayalen > 0)
						 // {
							 // $('#walaya').html(msg);
							$('.wilaya').eq(ind).html(msg);
						  //}
						  //else
						  //{
							//  $('.wilaya').eq(ind).html(msg);
						  //}
					  }
					});
				
			
}
function calculate(){
		paidInstalment = $("#paid_instalment").val();
				leaveInstalment = $("#leave_installmment").val();
				typeInstallment = $("#type_installment").val();
				amount = $('#amount').val();
				if(paidInstalment !=""){
					if(amount !=""){
						instalmentAmount = amount/paidInstalment;
						$("#instalment_amount").val(instalmentAmount);
					}
				}
				
				if(typeInstallment != "" && paidInstalment!=""){
					if(typeInstallment == '12'){
						//alert('if');
						totalnoInstalment  = parseInt(paidInstalment)+parseInt(leaveInstalment);
						totalYeras = totalnoInstalment/typeInstallment;
					}
					else{
						//alert('else');
						totalnoInstalment  = parseInt(paidInstalment)+parseInt(leaveInstalment);
						totalYeras = parseInt(totalnoInstalment)*parseInt(typeInstallment)/12;
						
					}
					
					$("#total_no_years").val(totalYeras);
				}
					
	}
	
	function load_child_category(id){
	var request = $.ajax({
					  url: config.AJAX_URL+'loadChild',
					  type: "POST",
					  data: { parentId :id,child_id :child_id},
					  dataType: "html",
					  success: function(msg){
						//location.href = config.CURRENT_URL;
						//$('.bigbangtheory').after(msg);
						$("#child_c").html(msg);
						$("#child_c").show();
						$(".child").show();
						
					  
					  }
					});
}

	</script>
<!--end of hover -->
<!-- menu -->

<!-- Include JQuery Vertical Tabs plugin -->
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-jvert-tabs-1.1.4.js"></script>
<?php
//echo mainMenu;

//$this->session->userdata('menuIndex');
if($this->session->userdata('menuIndex') !=""){
	$classParent = $this->session->userdata('menuIndex');
}
else{
	$classParent = 0;
}
//echo $classParent;
?>
<script type="text/javascript">
classParent = '<?php echo $classParent?>';
function selectMenu(){
	//
	sMenu = $("#selectedMenu").val();
		
	//alert(sMenu);
	//$("#vtabs1").jVertTabs({
	//			selected: sMenu
	//		});
}

$(document).ready(function(){
			config.ORDER_ID = window.location.hash.substr(1);
			//alert(type);
			//config.ORDER_ID =4;
			
			$("#vtabs1").jVertTabs({
				selected:config.ORDER_ID
			});
			
			setTimeout('selectMenu()',10);
		});

function removeRow(v)
{
	var request = $.ajax({
		  url: config.AJAX_URL+'removePartner',
		  type: "POST",
		  data: { applicant : v},
		  dataType: "html",
		  success: function(msg){
			$('#personalbingo'+v).remove();
		  }
		});
}

function removePhone(v)
{
	var request = $.ajax({
		  url: config.AJAX_URL+'removePhone',
		  type: "POST",
		  data: { phoneid : v},
		  dataType: "html",
		  success: function(msg){
			$('#hatfi'+v).remove();
		  }
		});
}
function checkPhoneLen(v)
{
	var phoneNumberLength = $(v).val().length;
	var phoneNumber = $(v).val();
	if(phoneNumberLength < 8)
	{
		$(v).addClass('redline').removeClass('greenline');
		ddx('يجب أن يكون رقم الهاتف المكون من 8 أرقام');
	}
	else
	{
		if(phoneNumber!='')
		{
			var reviews = $.trim($('#review').val());
			var value = $.trim($(v).val());
			var column = $.trim($(v).attr('id'));
			var data_attribute = $(v).attr('data-handler').split('_');
			if(value!='')
			 {	var applicant_info = $.ajax({
					  url: config.AJAX_URL+'saveTempInfo',
					  type: "POST",
					  data:{value:value,tempid:data_attribute[0],applicantid:data_attribute[1],phoneid:data_attribute[2],type:'Phone',column:column,reviews:reviews},
					  dataType: "html",
					  success: function(msg){ ; }
					});
			 }
			
			$(v).addClass('greenline').removeClass('redline');
		}
	}
		 
}
</script>
<style>
[data-notifications] {
 position: relative;
}
[data-notifications]:after {
	content: attr(data-notifications);
	position: absolute;
	background: red;
	border-radius: 50%;
	display: inline-block;
	padding: 0.3em;
	color: #f2f2f2;
	left: 16px;
	top: -15px;
}
.addnewphone{
	background:url(../../../images/addnewphone.png)
	
}
</style>
<!-- end of menu-->
<link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css" />
<?php //echo '<pre>'; print_r($module['Module_name']);?>
</head>

<body>
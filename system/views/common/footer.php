<!--<footer class="ex_footer" style="text-align:center; margin-top:4px;">
  <div class="copy_right"> <a href="http://www.durar-it.com" target="_blank">تصميم و برمجة شركة <img src="http://www.durar-it.com/images/footer_durar_ico.png" width="15" height="15" border="0"> درر للحلول الذكية ش.م.م </a></div>
  </div>
</footer>-->          
<script src="<?PHP echo base_url(); ?>assets/js/9e25e8e2.bootstrap.min.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/dist/js/bootstrap-select.js"></script>

<!-- Proton base scripts: --> 
<script src="<?PHP echo base_url(); ?>assets/js/3fa227ae.proton.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/jquery-ui.js"></script> 
<!-- Page-specific scripts: -->
<?PHP if($this->uri->segment(2)=='dashboard') {  ?>
<script src="<?PHP echo base_url(); ?>assets/js/proton/73f27b75.dashboard.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/proton/217183f0.dashdemo.js"></script>
<?PHP } ?>
<script src="<?PHP echo base_url(); ?>assets/js/proton/6c42db75.sidebar.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/proton/7d8c8d18.forms.js"></script> 
<!-- jsTree --> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/jquery.jstree.js"></script> 
<!-- Bootstrap Tags Input --> 
<!-- http://timschlechter.github.io/bootstrap-tagsinput/examples/ --> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/bootstrap-tagsinput.min.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/proton/8b870695.ui-components-general.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/jquery.raty.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/farbtastic.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/jquery.maskedinput.min.js"></script>
<script src="<?PHP echo base_url(); ?>assets/js/proton/jquery.autotab.min.js"></script>

<!-- Raphael, used for graphs --> 
<!-- http://raphaeljs.com/ --> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/raphael-min.js"></script> 

<!-- Morris graphs --> 
<!-- https://github.com/oesmith/morris.js --> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/morris.min.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/jquery.pnotify.min.js"></script> 
<!-- Select2 For Bootstrap3 --> 
<!-- https://github.com/fk/select2-bootstrap-css --> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/select2.min.js"></script> 

<!-- Number formating for dashboard demo --> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/numeral.min.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/bootstrap-datetimepicker.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/charCount.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/jquery.textareaCounter.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/summernote.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/vendor/jquery.highlight-4.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/demo.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/dmuploader.js"></script> 
<script src="<?PHP echo base_url(); ?>assets/js/jQuery.print.js"></script>

<script src="<?PHP echo base_url(); ?>assets/js/jquery.qtip.custom/jquery.qtip.js"></script>


<script src="<?PHP echo base_url(); ?>assets/highchart-js/highcharts.js"></script>
<script src="<?PHP echo base_url(); ?>assets/highchart-js/highcharts-3d.js"></script>
<script src="<?PHP echo base_url(); ?>assets/highchart-js/modules/exporting.js"></script>
<script src="<?PHP echo base_url(); ?>assets/js/jquery.searchabledropdown-1.0.8.src.js"></script>


<input type="hidden" id="datatable_ajax_url_go" value="<?PHP echo $ajax_url; ?>">
<input type="hidden" id="datatable_ajax_columns_go" value="<?PHP echo $columns_array; ?>">
<input type="hidden" id="searchcurrent_inp">
<script src="<?PHP echo base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
<script>
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

	
function create_data_table(mingo)
{
	if ($('#serverSideTable').length > 0)
	{
		$('#serverSideTable thead th').each( function (index, value) {				
			var title = $(this).html();
			var attr_id = $('#serverSideTable thead th').eq( $(this).index() ).attr('id');
			if(title!='الإجراءات' && title!='الإجرائات' && title!='ع')
			{				 
			   $(this).html(title+'<input type="search" class="form-control '+attr_id+' search_filter" placeholder="'+title+'" />' );
			}
		});
		
		var ajax_url_for_data = '<?PHP echo $ajax_url; ?>';		
		var serverSideTable = $('#serverSideTable').DataTable( {
				"ajax": ajax_url_for_data,
				"processing": true,
				"serverSide": true,
				"lengthMenu": [2, 200, 300, 400, 500,1000],
        		"pageLength":2,
				"ordering": false,
				"oLanguage": {
					  "sSearch": "",
					  "oPaginate": {
						  "sNext": "التالی",
							"sPrevious": "السابق"
						  }
					}
			} );
			
		serverSideTable.columns().eq( 0 ).each( function ( colIdx ) 
		{
			$( 'input', serverSideTable.column( colIdx ).header() ).on( 'keyup change', function () 
			{	serverSideTable.column( colIdx ).search( this.value ).draw();	});
		});	
		
		$('.search_filter').keyup(function(){
			$('#serverSideTable td').removeHighlight().highlight($(this).val());
		});
		
		var front_end_banner = $('.front-end-banner').length;
		if(front_end_banner > 0)
		{	$(".front-end-banner").fancybox({prevEffect : 'fade', nextEffect : 'fade', closeBtn : true}); }
		
		serverSideTable.on( 'draw', function () {
    		tiptop();
		} );	
		
	}
	
	
	if($('#tableSortable').length > 0)
	{
		if(mingo==0)
		{
			$('#tableSortable thead th').each( function (index, value) {				
				 var title = $(this).html();
				 var attr_id = $('#tableSortable thead th').eq( $(this).index() ).attr('id');
				 if(title!='الإجراءات'  && title!='الإجرائات' && title!='ع')
				 {				 
				 	$(this).html(title+'<input type="search" class="form-control '+attr_id+' search_filter" placeholder="'+title+'" />' );
				 }
			});
		}
		var ajax_url_for_data = '<?PHP echo $ajax_url; ?>';		
		var datatable_handler = $('#tableSortable').DataTable( {
				"ajax": ajax_url_for_data,
				"lengthMenu": [100, 200, 300, 400, 500,1000],
        		"pageLength":100,
				"ordering": false,
				"oLanguage": {
					  "sSearch": "",
					  "oPaginate": {
						  "sNext": "التالی",
							"sPrevious": "السابق"
						  }
					},
				"columns": [ <?PHP echo $columns_array; ?> ]
			} );
			
		datatable_handler.columns().eq( 0 ).each( function ( colIdx ) 
		{
			$( 'input', datatable_handler.column( colIdx ).header() ).on( 'keyup change', function () 
			{
				datatable_handler.column( colIdx ).search( this.value ).draw();
			});
		});	
		
		$('.search_filter').keyup(function(){
			$('#tableSortable td').removeHighlight().highlight($(this).val());
		});
		
		var front_end_banner = $('.front-end-banner').length;
		if(front_end_banner > 0)
		{	$(".front-end-banner").fancybox({prevEffect : 'fade', nextEffect : 'fade', closeBtn : true}); }
		
		datatable_handler.on( 'draw', function () {
    		tiptop();
		} );
			
	}
	
	if($('#basicTable').length > 0)
	{
		 $('#basicTable thead th').each( function (index, value) {				
				 var title = $.trim($(this).html());
				 var attr_id = $('#basicTable thead th').eq( $(this).index() ).attr('id');
				 if(title!='الإجراءات' && title!='الإجرائات' && title!='' && title!='متوسط صافي الريح' && title!='متوسط الايرادات' && title!='الشهرية' && title!='السنوية')
				 {				 
				 	$(this).html(title+'<input type="search" class="form-control '+attr_id+' search_filter" placeholder="'+title+'" />' );
				 }
		});
		
		 var basic_table = $('#basicTable').DataTable({
			 "ordering": false,
			 "oLanguage": {
				 "sSearch": "",
				 "oPaginate": { "sNext": "التالی", "sPrevious": "السابق" }
				}
			});
		
		basic_table.columns().eq(0).each(function (colIdx) 
		{
			$('input', basic_table.column(colIdx).header()).on('keyup change', function() 
			{
				basic_table.column(colIdx).search(this.value).draw();
			});
		});	
		
		$('.search_filter').keyup(function(){
			$('#basicTable td').removeHighlight().highlight($(this).val());
		});	
		
		basic_table.on( 'draw', function () {
    		tiptop();
		} );
	}
	
	
	if($('.newbasicTable').length > 0)
	{
		 $('.newbasicTable thead th').each( function (index, value) {				
				 var title = $.trim($(this).html());
				 var attr_id = $('.newbasicTable thead th').eq( $(this).index() ).attr('id');
				 if(title!='الإجراءات' && title!='الإجرائات' && title!='' && title!='متوسط صافي الريح' && title!='متوسط الايرادات' && title!='الشهرية' && title!='السنوية')
				 {				 
				 	$(this).html(title+'<input type="search" class="form-control '+attr_id+' search_filter" placeholder="'+title+'" />' );
				 }
		});
		
		 var new_basic_table = $('.newbasicTable').DataTable({
			 "ordering": false,
			 "oLanguage": {
				 "sSearch": "",
				 "oPaginate": { "sNext": "التالی", "sPrevious": "السابق" }
				}
			});
		
		new_basic_table.columns().eq(0).each(function (colIdx) 
		{
			$('input', new_basic_table.column(colIdx).header()).on('keyup change', function() 
			{
				new_basic_table.column(colIdx).search(this.value).draw();
			});
		});	
		
		$('.search_filter').keyup(function(){
			$('.newbasicTable td').removeHighlight().highlight($(this).val());
		});	
		
		new_basic_table.on( 'draw', function () {
    		tiptop();
		} );
	}
}

function tiptop()
{
	var myposition = {
			my: 'bottom center',  // Position my top left...
			at: 'top center', // at the bottom right of...
		};
	$('.icon-edit-sign, .icon-pencil').qtip({
				content: 'تعديل',
				position: myposition });
	$('.icon-remove-circle').qtip({
				content: 'حذف',
				position: myposition });
	$('.icon-remove-sign').qtip({
				content: 'حذف',
				position: myposition });			
	$('.icon-search, .icon-eye-open').qtip({
				content: 'مشاهدة',
				position: myposition});
	$('.icon-comment-alt').qtip({
				content: 'إرسال رسالة',
				position: myposition });	
	$('.icon-check').qtip({
				content: 'كشف بالمستندات المطلوبة بعد الموافقة',
				position: myposition });
	$('.icon-comments-alt').qtip({
				content: 'تاريخ الرسالة',
				position: myposition});		
	$('.icon-time').qtip({
				content: 'الخط الزمنى',
				position: myposition});	
	$('.icon-plus').qtip({
				content: 'اضافة الزياراة',
				position: myposition});	
	$('.icon-plus-sign-alt').qtip({
				content: 'إضافة',
				position: myposition});
	$('.icon-eye-close').qtip({
				content: 'تخطئ',
				position: myposition});
	$('.icon-download-alt').qtip({
				content: 'تحميل السيرة الذاتية',
				position: myposition});	
	$('.icon-download-alt').qtip({
				content: 'تحميل السيرة الذاتية',
				position: myposition});	
	$('.icon-calendar').qtip({
				content: 'جدول مقابلة',
				position: myposition});	
	$('.icon-certificate').qtip({
				content: 'نتيجة المقابلة',
				position: myposition});																												
}

$(function()
  {
	
	if (config.MODULE_ID==config.MODULE_PARENT) {
		
		$('#Aireen'+config.MODULE_ID).addClass('navActive');
	}
	create_data_table(0);
	var  yx = {
			my: 'bottom center',  // Position my top left...
			at: 'top center', // at the bottom right of...
		};
	 $('.mymainmenu').qtip({position: yx});
	 
	 var  zz = {
			my: 'top center',  // Position my top left...
			at: 'bottom center', // at the bottom right of...
		};
	 $('.child_menu').qtip({position: zz});
	
	
    $('#userid').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
});
</script>
<iframe name="printing_iframe" id="printing_iframe" width="1" height="1" frameborder="0" style="width:1px; height:1px; border:0px;"></iframe>
<div id="loaderModel" tabindex="-1" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body mody-body">
        <center>
          <img src="<?PHP echo base_url(); ?>assets/images/logo.png" alt="El Rafd"><br>
          <h4 class="section-title preface-title text-warning">يرجى الانتظار</h4>
        </center>
      </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>
<!----------------------------------->
<div id="deleteDiag" tabindex="-1" role="dialog" class="modal fade">
  <div class="modal-dialog"  style="width:350px !important;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" style=" color:#CC0000;"><i style="float: right; margin-left: 8px;" class="icon-remove-circle"></i> حذف</h4>
      </div>
      <div class="modal-body mody-body">
        <form name="frmx_delete" id="frmx_delete" method="post">
          <p>أنه سيتم حذف بشكل دائم ولا يمكن استردادها. هل أنت متأكد؟</p>
          <input type="hidden" name="delete_id" id="delete_id">
          <input type="hidden" name="action_url" id="action_url">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">إلغاء</button>
        <button type="button" onClick="delete_data_system();" class="btn btn-lg btn-primary">حذف</button>
      </div>
    </div>
  </div>
</div>
<!----------------------------------->
<div id="globalDiag" tabindex="-1" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title"><i style="float: right; margin-left: 8px;" class="icon-search"></i><span id="ghead">مشاهدة</span></h4>
      </div>
      <div class="modal-body mody-body" id="global_data"> </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>
<!----------------------------------->
<div id="MessagesDiag" tabindex="-1" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title"><i style="float: right; margin-left: 8px;" class="icon-comment-alt"></i>رسالة جديدة <span id="mtitle"></span></h4>
      </div>
      <div class="modal-body mody-body" id="message_data"> </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>
<!----------------------------------->
<div id="addingDiag" tabindex="-1" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="mtcolor"><i id="icon" style="float: right; margin-left: 8px;" class=""></i><span id="t_heading"></span></h4>
      </div>
      <div class="modal-body mody-body">
        <div  id="adding_glo_data"></div>
      </div>
      <div class="modal-footer">
        <div id="ajax_action" style="display:none;"><img src="<?PHP echo base_url(); ?>assets/images/hourglass.gif"></div>
      </div>
    </div>
  </div>
</div>
<!----------------------------------->
<div id="appointmentDiag" tabindex="-1" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title"><i style="float: right; margin-left: 8px;" class="icon-calendar-empty"></i><span>الموعد</span></h4>
      </div>
      <div class="modal-body mody-body">
        <form name="frm_appointment_new" id="frm_appointment_new" method="post" action="<?PHP base_url(); ?>ajax/addappointment" autocomplete="off">
          <input type="hidden" name="appoid" id="appoid" value="">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group col-md-4">
                <label class="text-warning">الاسم :</label>
                <input name="name" placeholder="الاسم" id="name" type="text" class="req form-control">
              </div>
              <div class="form-group col-md-4">
                <label class="text-warning">الموضوع :</label>
                <input name="subject" placeholder="موضوع" id="subject" type="text" class="req form-control">
              </div>
              <div class="form-group col-md-4">
                <label class="text-warning">رقم الهاتف :</label>
                <input name="phonenumber" placeholder="رقم الهاتف" maxlength="8" id="phonenumber" type="text" class="req form-control NumberInput">
              </div>
              <div class="form-group col-md-4">
                <label class="text-warning">البريد الإلكتروني :</label>
                <input name="email" placeholder="البريد الإلكتروني" id="email" type="text" class="form-control">
              </div>
              <div class="col-md-4 form-group">
                <label class="text-warning">المحافظة \ المنطقة:</label>
                <?PHP echo $this->haya_model->create_dropbox_list('province','regions',$ah_applicant->province,0,'req'); ?> </div>
              <div class="col-md-4 form-group">
                <label class="text-warning">ولاية:</label>
                <?PHP echo $this->haya_model->create_dropbox_list('wilaya','wilaya',$ah_applicant->wilaya,$ah_applicant->province,'req'); ?> </div>
              <div class="col-md-8 form-group">
              <div class="form-group col-md-12">
                <label class="text-warning">تفاصيل الموعد :</label>
                <textarea name="appodetail" id="appodetail" placeholder="تفاصيل الموعد" class="req form-control"></textarea>
              </div>
<!--              <div class="form-group col-md-4">
                <label class="text-warning">أفضلية :</label>
                <?PHP //appointment_status(1,'appointpriority'); ?>
              </div>
              <div class="form-group col-md-4">
                <label class="text-warning">الموظفين :</label>
                <?PHP //appointment_status(5,'appointmentuser'); ?>
              </div>-->
              <div class="form-group col-md-12">
                <button type="button" id="save_appointment_new" class="btn btn-success"><i class="icon-save"></i> حفظ </button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div id="appointmentloader" style="display:none;"><img src="<?PHP echo base_url(); ?>assets/images/hourglass.gif"></div>
      </div>
    </div>
  </div>
</div>

<!------------------------------------>

<!-- ------------------------------------------------------- -->
<div id="leaverequestDialog" tabindex="-1" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title"><i style="float: right; margin-left: 8px;" class="icon-calendar-empty"></i><span>الموعد</span></h4>
      </div>
      <div class="modal-body mody-body">
        <form name="frm_appointment" id="frm_appointment" method="post" action="<?PHP base_url(); ?>ajax/addappointment" autocomplete="off">
          <input type="hidden" name="appoid" id="appoid" value="">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group col-md-4">
                <label class="text-warning">الاسم :</label>
                <input name="name" placeholder="الاسم" id="name" type="text" class="req form-control">
              </div>
              <div class="form-group col-md-4">
                <label class="text-warning">الموضوع :</label>
                <input name="subject" placeholder="موضوع" id="subject" type="text" class="req form-control">
              </div>
              <div class="form-group col-md-4">
                <label class="text-warning">رقم الهاتف :</label>
                <input name="phonenumber" placeholder="رقم الهاتف" maxlength="8" id="phonenumber" type="text" class="req form-control NumberInput">
              </div>
              <div class="form-group col-md-4">
                <label class="text-warning">البريد الإلكتروني :</label>
                <input name="email" placeholder="البريد الإلكتروني" id="email" type="text" class="form-control">
              </div>
              <div class="col-md-4 form-group">
                <label class="text-warning">المحافظة \ المنطقة:</label>
                <?PHP echo $this->haya_model->create_dropbox_list('province','regions',$ah_applicant->province,0,'req'); ?> </div>
              <div class="col-md-4 form-group">
                <label class="text-warning">ولاية:</label>
                <?PHP echo $this->haya_model->create_dropbox_list('wilaya','wilaya',$ah_applicant->wilaya,$ah_applicant->province,'req'); ?> </div>
              <div class="col-md-8 form-group">
              <div class="form-group col-md-12">
                <label class="text-warning">تفاصيل الموعد :</label>
                <textarea name="appodetail" id="appodetail" placeholder="تفاصيل الموعد" class="req form-control"></textarea>
              </div>
<!--              <div class="form-group col-md-4">
                <label class="text-warning">أفضلية :</label>
                <?PHP //appointment_status(1,'appointpriority'); ?>
              </div>
              <div class="form-group col-md-4">
                <label class="text-warning">الموظفين :</label>
                <?PHP //appointment_status(5,'appointmentuser'); ?>
              </div>-->
              <div class="form-group col-md-12">
                <button type="button" id="save_appointment" class="btn btn-success"><i class="icon-save"></i> حفظ </button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div id="appointmentloader" style="display:none;"><img src="<?PHP echo base_url(); ?>assets/images/hourglass.gif"></div>
      </div>
    </div>
  </div>
</div>
<!-- ------------------------------------------------------- -->
<div id="quickLaunchModal" tabindex="-1" role="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body mody-body" id="printable_data">
        <div> 
          <!-- <p align="center"><img src="<?PHP echo base_url(); ?>images/proton-logo.png" width="96" height="86"><span id="single_user"> <br />
            إسم المراجع : <span id="mname"></span></span><br />
            رقم التسجيل : <span id="mraqam"></span><br />
            طبيعة المراجعين : <span id="mrtype"></span> </p>-->
          <div id="landata"></div>
        </div>
      </div>
      <div class="modal-footer"> 
        <!--<form name="print_inquery" method="post" action="<?PHP echo base_url(); ?>inquiries/inqprint/" target="printing_iframe">
          <input type="hidden" name="inquery_id" id="inquery_id">
          <button type="submit" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
        </form>--> 
      </div>
    </div>
  </div>
</div>
<!------------------------------------->
<div id="smsModel" tabindex="-1" role="dialog" class="modal fade">
  <?php $type	=	$this->uri->segment(2);?>
  <?php 
if($type	== 'transactions')
{
	$type_name	=	'transactions';
}
if($type	== '')
{
	$type_name	=	'inquiry';
}
?>
  <form action="" method="POST" id="send_confirm_sms" name="send_confirm_sms" autocomplete="off">
    <input type="hidden" name="parent_id" id="parent_id" />
    <input type="hidden" name="pk_id" id="pk_id" value="" />
    <input type="hidden" name="list_id" id="list_id" value="<?php echo $list_id;?>" />
    <input type="hidden" name="tempid" id="tempid"/>
    <input type="hidden" name="sms_receiver_id" id="sms_receiver_id"/>
    <input type="hidden" name="sms_receiver_number" id="sms_receiver_number"/>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body mody-body">
       
          <div class="form-group">
            <label class="checkbox-inline">
            <div class="checker"><span class="checked">
              <div class="checker"><span>
                <input type="radio" id="sms_time" class="sms_time" value="1" checked="checked" name="sms_time">
                </span></div>
              </span>الأن</div>
            </label>
            &nbsp;
            <label class="checkbox-inline">
            <div class="checker"><span class="checked">
              <div class="checker"><span>
                <input type="radio" id="sms_time" class="sms_time" value="0" name="sms_time">
                </span></div>
              </span>لاحقا</div>
            </label>
          </div>
          <div class="list-group-item">
            <div class="form-group" style="display:none;" id="show_time">
              <label for="text-area-character-counter">حدد التاريخ والوقت :</label>
              <input type="text" placeholder="حدد التاريخ والوقت"  class="txt_field" name="date_time" id="date_time" />
              <div> </div>
            </div>
            <div class="form-group">
              <label for="text-area-character-counter">نص الرسالة</label>
              <div>
                <textarea id="sms_msg" name="expiry_msg" rows="2" class="form-control auto-resize" style="overflow-y: hidden; height: 152px; resize:off;"></textarea>
                <span class="character-counter"></span> </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="type" id="type" value="<?php echo $type_name;?>" />
          <button type="button" id="send_sms_to_user" class="btn btn-success"><i class="icon-envelope-alt"></i> إرسال </button>
          <button type="button" id="sms_processing" style="display:none;" class="btn btn-success"><img src="<?PHP echo base_url(); ?>assets/images/hourglass.gif"></button>
        </div>
      </div>
      <!-- /.modal-content --> 
    </div>
  </form>
</div>
<div id="bankModel" tabindex="-1" role="dialog" class="modal fade">
  <form action="" method="POST" id="send_approve_bank" name="send_approve_bank" autocomplete="off">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <input type="hidden"  class="form-control" name="app_id" id="app_id" />
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body mody-body">
        
          <div class="list-group-item">
            <div class="form-group" style="display:none;" id="show_time">
              <label for="text-area-character-counter">حدد التاريخ والوقت :</label>
              <input type="text" placeholder="حدد التاريخ والوقت"  class="form-control" name="list_accept" id="list_accept" />
              <div> </div>
            </div>
            <div class="form-group" id="reject_reason" style="display:none;">
              <label for="text-area-character-counter">نص الرسالة</label>
              <div>
                <textarea id="list_reason" name="list_reason" rows="2" class="form-control auto-resize" style="overflow-y: hidden; height: 152px; resize:off;"></textarea>
                <span class="character-counter"></span> </div>
            </div>
            <div class="form-group" id="accept_reason" style="display:none;">
              <label for="text-area-character-counter">رقم قرض</label>
              <div>
                <input type="text" placeholder="رقم"  class="form-control" name="list_accept" id="list_accept"  style="width: 30%;"/>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" id="send_to_user" class="btn btn-success" onclick="addreason()"> حفظ </button>
            <button type="button" id="sms_processing" style="display:none;" class="btn btn-success"><img src="<?PHP echo base_url(); ?>assets/images/ldx.gif"></button>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </form>
</div>
<div id="reject_popup" tabindex="-1" role="dialog" class="modal fade">
  <form action="" method="POST" id="send_rej_popup" name="send_rej_popup" autocomplete="off">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <input type="hidden"  class="form-control" name="app_id" id="app_id" />
          <input type="hidden"  class="form-control" name="is_approved" id="is_approved" />
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body mody-body">
         
          <div class="list-group-item">
            <div class="form-group" id="reject_reason_new" style="display:none;">
              <label for="text-area-character-counter">رفض السبب</label>
              <div>
              	<div id="reject_reason_div"></div>
                <span class="character-counter"></span> </div>
            </div>
                      </div>
          <div class="modal-footer">
            <button type="button" id="sms_processing" style="display:none;" class="btn btn-success"><img src="<?PHP echo base_url(); ?>assets/images/ldx.gif"></button>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </form>
</div>

<div id="bankModelNew" tabindex="-1" role="dialog" class="modal fade">
  <form action="" method="POST" id="send_scheduale" name="send_scheduale" autocomplete="off">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <input type="hidden"  class="form-control" name="app_id" id="app_id" />
          <input type="hidden"  class="form-control" name="is_approved" id="is_approved" />
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body mody-body">
        
          <div class="list-group-item">
            <div class="form-group" id="reject_reason_new" style="display:none;">
              <label for="text-area-character-counter">رفض السبب</label>
              <div>
                <textarea id="reject_reason" name="reject_reason" rows="2" class="form-control auto-resize" style="overflow-y: hidden; height: 152px; resize:off;"></textarea>
                <span class="character-counter"></span> </div>
            </div>
            <div class="form-group" id="accept_amount" style="display:none;">
              <label for="text-area-character-counter">مبلغ الأقساط</label>
              <div>
                <input type="text" placeholder="مبلغ الأقساط"  class="form-control" name="approved_amount" id="approved_amount"  style="width: 30%;"/>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" id="send_to_user" class="btn btn-success" onclick="addapprove()"> حفظ </button>
            <button type="button" id="sms_processing" style="display:none;" class="btn btn-success"><img src="<?PHP echo base_url(); ?>assets/images/ldx.gif"></button>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </form>
</div>
<div id="bankResponse" tabindex="-1" role="dialog" class="modal fade">
  <form action="" method="POST" id="send_approve_bank" name="send_approve_bank" autocomplete="off">
    <div class="modal-dialog" style="width: 37%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body mody-body">
          <p align="center" id="reason_reply"></p>
          <div class="modal-footer"> </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </form>
</div>
<div id="sessionhandler" tabindex="-1" role="dialog" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" style="width: 37%;">
    <div class="modal-content">
      <div class="modal-header" style="text-align:center; color:#FF0509 !important; font-size:17px !important;">انتهت صلاحية الوقت المحدود الدجاء تسجيل الدخول</div>
      <form action="<?php echo base_url() ?>admin/loginexp" name="frm_login_users" id="frm_login_users" method="post" autocomplete="off">
        <input type="hidden" id="useraction" name="useraction" value="EXP">
        <div class="modal-body mody-body">
          <div>
          <div class="col-md-12" style="direction:rtl;">
          <label class="text-warning">إسم المستخدم</label>
          <input type="text" class="form-control" name="username" id="username" autocomplete="off" placeholder="إسم المستخدم">
          <br clear="all">
        </div>
        <div class="col-md-12" style="direction:rtl;">
          <label class="text-warning">كلمة المرور</label>
          <input type="password" class="form-control" autocomplete="off" name="userpassword" id="userpassword" placeholder="كلمة المرور">
          <br clear="all">
        </div>
          </div>
          <div class="modal-footer"><input type="button" onClick="loginme();" class="btn btn-lg btn-success" value="تسجيل دخول"  /></div>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="sms_sender" tabindex="-1" role="dialog" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" style="width: 37%;">
    <div class="modal-content">
      <div class="modal-header" style="text-align:right; color:#FF0509 !important; font-size:17px !important;">ارسال الرسائل القصيرة</div>
      <div class="modal-body mody-body">
      		<div class="row">
            	<div class="col-md-12">
                        <div class="col-md-5" style="direction:rtl;">
                            <label class="text-warning">إجمالي الهاتف وجدت:</label>
                            <label class="text-warning" id="totalnumbers"></label>         
                    </div>
                    <div class="col-md-5" style="direction:rtl;">
                            <label class="text-warning">أرسلت:</label>
                            <label class="text-warning" id="totalsend">0</label>         
                    </div>
                    <div class="col-md-8" style="direction:rtl;" id="numbers_send">
                    	
                    </div>
                  </div>
            </div>
    	</div>
    </div>
  </div>
</div>
<div id="divToPrint2"></div>
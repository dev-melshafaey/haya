<?php
  $permissions	=	$this->haya_model->check_other_permission(array('86','141'));
  $profile_image  = base_url().'resources/users/'.$user_detail['profile']->userid.'/'.$user_detail['profile']->profilepic;
?>
<nav class="main-menu">
  <ul>
    <li style="display:none;"><a href="#"><img style="width:54px !important; height: 54px !important; margin-right: 3px !important; border: 1px solid #FFF; border-radius: 52px;" src="<?PHP echo $profile_image; ?>"></a></li>
    <li> <a href="<?php echo base_url(); ?>dashboard"> <i class="icon-home nav-icon"></i> <span class="nav-text"></span> </a> </li>
  <?php if($permissions[86]['a']==1) { ?>
    <li id="after_li"> <a href="#" onClick="openAppointment();" data-toggle="modal"> <i class="icon-calendar-empty nav-icon"></i> <span class="nav-text"></span> </a> </li>
    <?php } ?>
    <?php if($permissions[86]['v']==1) { ?>
    <li> <a href="<?PHP echo base_url(); ?>inquiries/allappointments"> <i class="icon-th-list nav-icon"></i> <span class="nav-text"></span> </a> </li>
    <?php } ?>
    <?php if($permissions[141]['a']==1):?>
    <li> <a href="#_" onclick="alatadad(this);" data-url="<?php echo base_url();?>users/add_leave_request"><i class="icon-male nav-icon"></i><span class="nav-text"></span></a> </li>
    <?php endif;?>
    <?PHP $this->haya_model->rightMenu(); ?>   
    <li> <a href="<?php echo base_url(); ?>admin/logout"> <i style="color: #F00 !important;" class="icon-off nav-icon"></i> <span class="nav-text"></span> </a> </li>
  </ul>
  
</nav>



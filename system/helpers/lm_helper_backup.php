<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Email Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/email_helper.html
 */

// ------------------------------------------------------------------------

/**
 * Validate email address
 *
 * @access	public
 * @return	bool
 */
 if ( ! function_exists('check_permisions'))
{
	function check_permisions($id){
		$array  = array(4,7,9,13,15,17,20,21);
		//SELECT GROUP_CONCAT(ur.`role_id`) AS totalmuazif FROM `user_roles` AS ur WHERE ur.role_name = 'موظف' 
		$CI =& get_instance();
		$CI->db->select('GROUP_CONCAT(`role_id`) AS totalmuazif');
		$CI->db->from('user_roles');
		$CI->db->where('role_name','موظف'); 
		$query = $CI->db->get();
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			if(!empty($row)){
				//print_r($row);
				$totalMuazif = $row->totalmuazif;
				$muazifArr = explode(',',$totalMuazif);
				//print_r($muazifArr);
				if(in_array($id,$muazifArr)){
					//echo "easd";
					return false;
				}

				else{
					return true;
				}
			}
			else{
				return true;
			}
		}
		else{
			return true;
		}
	}
}
 
 
if ( ! function_exists('get_lable'))
{
function get_lable($value)
{
$lables = array('thanks' => array('ar' => 'تذكير للانتهاء'), 
'expire' => array('ar' => 'رسالة ناجحة'), 
'murajeen' => array('ar' => 'المراجعين'), 
'step-1' => array('ar' => 'تسجيل ودراسة الطلبات'), 
'step-2' => array('ar' => 'بيانات المشروع'), 
'step-3' => array('ar' => 'القرض المطلوب'), 
'step-4' => array('ar' => 'دراسه وتحليل الطلب'), 
'step-5' => array('ar' => 'قرار اللجنة'), 
'step-6' => array('ar' => 'موافقة أولية'), 
'step-7' => array('ar' => 'الزيارة'), 
'step-8' => array('ar' => 'تأجيل '), 
'step-9' => array('ar' => 'الصرف'),
 'step-10' => array('ar' => 'المتابعة'), 'step-11' => array('ar' => 'الرفض'), 'step-12' => array('ar' => ''), 'step-13' => array('ar' => ''), 'step-14' => array('ar' => ''), 'step-15' => array('ar' => '')
 
);
return $lables[$value];
}
}

 
if ( ! function_exists('loan_sub_category'))
{
	function loan_sub_category($name,$parent,$id)
	{
		//echo $id;
		//exit;
		if($parent){
			$parent_class = "child";
		}
		else{
			$parent_class = "parent";
		}
		$CI =& get_instance();		
		$CI->db->select('loan_caculate_id,loan_category_id,loan_category_name');
		$CI->db->from('loan_calculate');	
		$CI->db->where('parent_id',$parent);
		$CI->db->order_by("loan_category_name", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select name="'.$name.'" id="'.$name.'" class="req '.$parent_class.'" placeholder="البرنامج المطلوب‎" onchange="check_data(this.value)" >';
		$dropdown .= '<option value="">البرنامج المطلوب‎</option>';

		foreach($query->result() as $row)
		{
			
					$dropdown .= '<option value="'.$row->loan_caculate_id.'" ';
					if($id !=""){
					if($id==$row->loan_caculate_id){
						$dropdown .= 'selected="selected"';
						}
					}
					$dropdown .= '>'.$row->loan_category_name.'</option>';
			
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
} 

if ( ! function_exists('getCountsById'))
{
 function getCountsById($table,$id){
	 		
			$CI =& get_instance();
			$CI->db->select('COUNT(*)  AS total');
			$CI->db->where('applicant_id',$id);
			$query = $CI->db->get($table);
		if($query->num_rows() > 0){
			return $query->row();

		}
		else{
			return false;
		}
	}
}
 
if ( ! function_exists('parentMenu'))
{
	function parentMenu()
	{
		//$userinfo = $this->session->userdata('userinfo');<br />
	$CI =& get_instance();
		 $userinfo = $CI->session->userdata('userinfo');
		//echo "<pre>";
		//print_r($userinfo);
		$userinfo->user_role_id;
		$CI =& get_instance();
		$CI->db->select('moduleid,module_name,module_icon,module_controller,module_parent');
		$CI->db->from('mh_modules');
		$CI->db->where('module_parent',0); 
		$CI->db->where('module_status','A');
		$CI->db->order_by("module_order", "ASC");
		$query = $CI->db->get();
		if($query->num_rows() > 0)
		{
			$html = '<nav class="quick-launch-bar">';
			$html .= '<ul>';
			foreach($query->result() as $i=>$parentMenu)
			{
				if(check_view_permission($parentMenu->moduleid)==1)
                {
                    $html .= '<li><a href="'.base_url().$parentMenu->module_controller.'"><i class="'.$parentMenu->module_icon.'"></i><span>'.$parentMenu->module_name.'</span></a></li>';
                }
			}
			$query->free_result();
			$html .= '</ul>';
			$html .= '</nav>';
			echo $html;
		}
	}
}

if ( ! function_exists('childMenu'))
{
	function childMenu($parentid,$name,$order)
	{
		
		//data-notifications="'.arabic_date(10).'"
		$CI =& get_instance();		
		$CI->db->select('moduleid,module_name,module_icon,module_controller');
		$CI->db->from('mh_modules');
		$CI->db->where('module_parent',$parentid); 
		$CI->db->where('module_status','A');
		$CI->db->order_by("module_order", "ASC");
		$childQuery = $CI->db->get();		
		if($childQuery->num_rows() > 0)
		{	
			
			//<span class="noti" data-count='2'></span>
			$span = '<span class="noti" data-count="2"></span>';
			$url3 = $CI->uri->segment(3);
			if($url3){
				$cchild = $CI->uri->segment(1).'/'.$CI->uri->segment(2).'/'.$CI->uri->segment(3);
			}else{
				$cchild = $CI->uri->segment(1).'/'.$CI->uri->segment(2);
			}
				
			$xhtml .= '<div id="#vtabs-content-'.$order.'">';
			$xhtml .= '<div class="submenu_contant submenu">';
			
			$xhtml .= '<div class="submenu_title">'.$name.'</div>';
			foreach($childQuery->result() as $childMenu)
			{
                if(check_view_permission($childMenu->moduleid)==1)
                {
				//echo $childMenu->module_controller;
				$text = explode('#',$childMenu->module_controller);
				if(is_array($text))
				$menu_change_name=$text[0];
				else
				$menu_change_name = $childMenu->module_controller;
				
				if($cchild == trim($menu_change_name))
				{
					$childMenu->module_controller;
				 	$chd = 'submenuactive';
					//echo $cchild.' 333333333   '.$childMenu->module_controller.'<br><br>';
				}
				else{
					$chd = '';
					//echo'test '.$cchild.' 333333333   '.$childMenu->module_controller.'<br><br>';
				}

				$xhtml .= '<div class="submenu_txt "><a  class="'.$chd.'" href="'.base_url().$childMenu->module_controller.'">'.$childMenu->module_name.' </a></div>';
                }
			}
			$xhtml .= '</div>';
			$xhtml .= '</div>';
			
		}
		return $xhtml;
	}
}
if ( ! function_exists('error_hander'))
{
	function error_hander($eid)
	{
		
		echo $eid;
		exit;
		
		if($eid!='')
		{
			$ErrorCSS = array(
					'S'=>'<div class="right_nav_raw">
      <div class="nav_icon"><img width="60" height="60" src="'.base_url().'/images/body/right.png"></div>
      قد تمت العملية بالشكل الصحيح </div>',
					'E'=>'<div class="wrong_nav_raw">
      <div class="nav_icon"><img width="60" height="60" src="'.base_url().'/images/body/wrong.png"></div>
      لم تتم العملية يرجي  التأكد من الأخطاء </div>',
					'W'=>'<div class="alert alert-warning alert-dismissable"><strong>WARNING!</strong> [HEARTDISK]</div>',
					'N'=>'<div class="alert alert-info alert-dismissable"><strong>HEADS UP!</strong> [HEARTDISK]</div>');
			//$CI =& get_instance();
			//$q = $CI->db->query("SELECT error_title,error_type FROM mh_errors WHERE errorid='".$eid."' LIMIT 0,1");
			
			/*foreach($q->result() as $error)
			{	echo str_replace('[HEARTDISK]',$error->error_title,$ErrorCSS[$error->error_type]);	}*/
			
			echo "<pre>";
			print_r($ErrorCSS);
			// echo   $ErrorCSS[$eid];			
		}
	}
}

/*
module id assign in sms management table
number means user phone numbers,
uid means record id,
messsage  message for concinate
*/
/// 

function calcualteAge($dob){
	$from = new DateTime(date('Y-m-d',strtotime($dob)));
	$to   = new DateTime('today');
	return  $from->diff($to)->y;	
}

if ( ! function_exists('send_sms'))
{
	function send_sms($moduleid,$numbers,$uid,$message)
	{
		
		$CI =& get_instance();
		$CI->db->select('sms_value');
		$CI->db->from('sms_management');
		$CI->db->where('sms_type',$moduleid);  
		$query = $CI->db->get();
		$result = $query->row();		
		  $mess = urlencode(str_replace('[CODE]',$message,$result->sms_value));
		  send_step_sms($mess,$Recipients,'');
		  
		//   $send = "http://sms4world.net/smspro/sendsms.php?user=DURAR&password=123456&numbers=".$numbers."&sender=Durar&message=".$mess."&lang=ar";
		 // $return_status = file_get_contents($send);
			// $return = strstr($return_status,'1');	
			//$return = 1;	
			/*if($return){
				$CI =& get_instance();
				if(is_array($numbers)){
					//echo "if";
					foreach($numbers as $phone){
						
						$myData['sms_receiver_id'] = $uid;
						$myData['sms_receiver_number'] = $phone;
						$myData['sms_sender'] = 'Durar';
						$myData['type'] ='sms';
						$myData['sms_module_id'] = $moduleid;					
						$data[] = $myData; 
					}
				}
				else{
					//echo "else";
						$myData['sms_receiver_id'] = $uid;
						$myData['sms_receiver_number'] = $phone;
						$myData['sms_sender'] = 'Durar';
						$myData['type'] ='sms';
						$myData['sms_module_id'] = $moduleid;
						$data[] = $myData;
				}
				
				//print_r($data);
				//$query = $CI->db->insert('sms_history',$data);
				 $CI->db->insert_batch('sms_history',$data);
				//exit;
				return true;
			}
			else{
				return false;
			}*/
			//print_r($return_status);
	}
	
}

// send sms code for 
if ( ! function_exists('send_sms_code'))
{

	function send_sms_code($receiverid,$numbers)
	{
		
		 $code = generateCode();
		 $CI =& get_instance();
		 $message = $code."يرجى إدخال هذا الرمز للتحقق من هويتك";
		 $mess = urlencode($message);
		 $send = "http://sms4world.net/smspro/sendsms.php?user=DURAR&password=123456&numbers=".$numbers."&sender=Durar&message=".$mess."&lang=ar";
		 $return_status = file_get_contents($send);
		 $return = strstr($return_status,'1');
		 
		 $data['sms_code'] = $code;
		 $data['sms_receiver_number'] = $numbers;
		 $data['sms_receiver_id'] = $receiverid;
		 $CI->db->insert('sms_code_varification',$data);	
	}
}

if ( ! function_exists('getLoanListType'))
{
	function getLoanListType($id)
	{
		//echo $req;
		$CI =& get_instance();		
		$CI->db->select('loan_category_name');
		$CI->db->from('loan_category');		
		//$CI->db->where('list_id',$list_id);
		$CI->db->where('loan_category_id',$id);
		$query = $CI->db->get();
		$res = $query->row();
		//print_r($res);
		return $res->loan_category_name;
	}
}

if ( ! function_exists('getlistType'))
{
function getlistType($type,$id){
				$CI =& get_instance();		
				$CI->db->select('list_name');
				$CI->db->from('list_management');	
				$CI->db->where('list_type',$type);
				$CI->db->where('list_id',$id);
				$query = $CI->db->get();
				$return = $query->row();
				return $return->list_name;
	}
}
if ( ! function_exists('show_data'))
{

	function show_data($case,$id)
	{
		$CI =& get_instance();
		switch($case)
		{
			case 'calc';
					$CI =& get_instance();		
				$CI->db->select('loan_category_name');
				$CI->db->from('loan_calculate');	
				$CI->db->where('loan_category_id',$id);
				$query = $CI->db->get();
				$return = $query->row();
				return $return->loan_category_name;
				break;
			case 'Region';
					$CI =& get_instance();		
				$CI->db->select('REIGONID,REIGONNAME');
				$CI->db->from('election_reigons');	
				$CI->db->where('ID',$id);
				$query = $CI->db->get();
				$return = $query->row();
				return $return->REIGONNAME;
				break;
			case 'Walaya';
				$CI->db->select('WILAYATNAME');
				$CI->db->from('election_wilayats');
				$CI->db->where('WILAYATID',$id); 
				$query = $CI->db->get();
				foreach($query->result() as $xx)
				{
					return $xx->WILAYATNAME;
				}
			break;
			case 'LoanLimit';
				$CI->db->select('loan_category_name');
				$CI->db->from('loan_category');
				$CI->db->where('loan_category_id',$id); 
				$query = $CI->db->get();
				foreach($query->result() as $xx)
				{
					return $xx->loan_category_name;
				}
			break;
		}
	}
}

if ( ! function_exists('send_custom_sms'))
{
	function send_custom_sms($message,$numbers)
	{
	 $code = generateCode();
		 $CI =& get_instance();
		// $message = $code."يرجى إدخال هذا الرمز للتحقق من هويتك";
		 $mess = urlencode($message);
		 $send = "http://sms4world.net/smspro/sendsms.php?user=DURAR&password=123456&numbers=".$numbers."&sender=Durar&message=".$mess."&lang=ar";
		 $return_status = file_get_contents($send);
		return  $return = strstr($return_status,'1');
		 

	}
	//functio	
}

if ( ! function_exists('send_general_sms'))
{
	function send_general_sms($message,$Recipients)
	{
	 	$code = generateCode();
		
		
		//echo "<pre>";
		//print_r($arr);
		
		$CI =& get_instance();
		ini_set("soap.wsdl_cache", "0");
		ini_set("soap.wsdl_cache_enabled", "0");
		
		
		header('Content-Type: text/plain');
			if (!class_exists('SoapClient'))
		{
		
				die ("You haven't installed the PHP-Soap module.");
		
		}
		

	//$userID="E.Alraffd";
	//$Password="Oman12!@";
	//$Message="السلام عليكم تجربة للبرنامج الرسائل النصية على برنامج (رافد )شكرا لكم";
	//$Message="شيشيشسيشسيشس";
	//$Language=2;
	$Recipients= explode(',',$Recipients);
	//$Recipients=array(0=> '96897890223',1 => '96895755443',2 => '96899622755',3=> '96899888987');
	
//exit;

ini_set('max_execution_time',300 );
        try {
            $options = array(
                'soap_version'=>SOAP_1_2,
                'exceptions'=>true,
                'trace'=>1,
                'cache_wsdl'=>WSDL_CACHE_NONE
            );
            $client = new SoapClient('http://ismartsms.net/iBulkSMS/webservice/IBulkSMS.asmx?WSDL', $options);
            $results = $client->PushMessage(
                    array(
		'UserID' => sms_id,
		'Password' =>sms_password,
		//	'Message' => '.',
	  	'Message' => $message,
		'Language' =>64,
		'ScheddateTime' => date('Y-m-d'),
		'Recipients' => $Recipients,
		'RecipientType' => 1
                       )
                    );
					
				//echo "<pre>";	
				//print_r($results);	
        } catch (Exception $e) {
            echo "<h2>Exception Error!</h2>";
            echo $e->getMessage();
        }

	}
	//functio	
}

if ( ! function_exists('send_step_sms'))
{
	function send_step_sms($message,$Recipients,$moduleid)
	{
	 	//$code = generateCode();
		
		
		//echo "<pre>";
		//print_r($arr);
		if($moduleid !=""){
			$CI =& get_instance();
			$CI->db->select('sms_value');
			$CI->db->from('sms_management');
			$CI->db->where('sms_type',$moduleid);  
			$query = $CI->db->get();
			$result = $query->row();
			$message = $result->sms_value;
		}
		
		$CI =& get_instance();
		ini_set("soap.wsdl_cache", "0");
		ini_set("soap.wsdl_cache_enabled", "0");
		
		
		header('Content-Type: text/plain');
			if (!class_exists('SoapClient'))
		{
		
				die ("You haven't installed the PHP-Soap module.");
		
		}
		

	//$userID="E.Alraffd";
	//$Password="Oman12!@";
	//$Message="السلام عليكم تجربة للبرنامج الرسائل النصية على برنامج (رافد )شكرا لكم";
	//$Message="شيشيشسيشسيشس";
	//$Language=2;
	$Recipients= explode(',',$Recipients);
	//$Recipients=array(0=> '96897890223',1 => '96895755443',2 => '96899622755',3=> '96899888987');
	
//exit;

ini_set('max_execution_time',300 );
        try {
            $options = array(
                'soap_version'=>SOAP_1_2,
                'exceptions'=>true,
                'trace'=>1,
                'cache_wsdl'=>WSDL_CACHE_NONE
            );
            $client = new SoapClient('http://ismartsms.net/iBulkSMS/webservice/IBulkSMS.asmx?WSDL', $options);
            $results = $client->PushMessage(
                    array(
		'UserID' => sms_id,
		'Password' =>sms_password,
		//	'Message' => '.',
	  	'Message' => $message,
		'Language' =>64,
		'ScheddateTime' => date('Y-m-d'),
		'Recipients' => $Recipients,
		'RecipientType' => 1
                       )
                    );
					
				//echo "<pre>";	
				//print_r($results);	
        } catch (Exception $e) {
            echo "<h2>Exception Error!</h2>";
            echo $e->getMessage();
        }

	}
	//functio	
}


if ( ! function_exists('rQuote'))
{
	function rQuote($txt)
	{
		return str_replace('"',"",$txt);
	}
	//functio	
}

if ( ! function_exists('ageCalculator'))
{
	function ageCalculator($date)
	{
		$from = new DateTime($date);
		$to   = new DateTime('today');
		return $from->diff($to)->y;
	}
	//functio	
}

// send sms code for 
if ( ! function_exists('send_sms_steps'))
{

	function send_sms_steps($stepNo,$numbers)
	{
		if($stepNo ==1){
			$msg= " عزيزنا العميل: تم تسجيل بياناتك الشخصية بنجاح";
		}
		elseif($stepNo ==2){
			$msg= "عزيزنا العميل: تم تسجيل بيانات المشروع بنجاح";
		}
		elseif($stepNo ==3){
			$msg= "عزيزنا العميل: تم تسجيل بيانات القرض المطلوب بنجاح";
		}
		elseif($stepNo ==4){
			$msg= "عزيزنا العميل: تم تحويل معاملتك الى دراسة وتحليل الطلب";
		}
		elseif($stepNo ==5){
			$msg= "عزيزنا العميل: تم تحويل معاملتك الى دراسة وتحليل الطلب 5";
		}
		elseif($stepNo ==6){
			$msg= "عزيزنا العميل: تم تحويل معاملتك الى دراسة وتحليل الطلب 6";
		}
		elseif($stepNo ==7){
			$msg= "عزيزنا العميل: تم تحويل معاملتك الى دراسة وتحليل الطلب 7";
		}
		
		
		$num = explode(',',$numbers);
		//print_r($num);
		 foreach($num as $number){
			if(strlen($number)==8)
			{
				$numberNew[] = '968'.$number;
			}
			else
			{
				$numberNew[] = $number;
			}
		 }
		 $numbers = implode(',',$numberNew);
		 $code = generateCode();
		 $CI =& get_instance();
		 $mess = urlencode($msg);
		 $send = "http://sms4world.net/smspro/sendsms.php?user=DURAR&password=123456&numbers=".$numbers."&sender=Durar&message=".$mess."&lang=ar";
		 $return_status = file_get_contents($send);
		 return $return = strstr($return_status,'1');
		 
	}
}


// send sms code for 
if ( ! function_exists('send_sms_message'))
{

	function send_sms_message($msg,$numbers)
	{
		
		 $code = generateCode();
		 $CI =& get_instance();
		 $message = $msg;
		 $numbers;
		 $num = explode(',',$numbers);
		 foreach($num as $number){
			$numberNew[] = '968'+$number;
		 }
		 echo $numbers = implode(',',$numberNew);
		 $mess = urlencode($message);
		 $send = "http://sms4world.net/smspro/sendsms.php?user=DURAR&password=123456&numbers=".$numbers."&sender=Durar&message=".$mess."&lang=ar";
		 $return_status = file_get_contents($send);
		 $return = strstr($return_status,'1');
		 	
	}
}

///verif code  for sms
if ( ! function_exists('varify_code'))
{

	function varify_code($code)
	{
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('sms_code_varification');
		$CI->db->where('sms_code',$code);
			$query = $CI->db->get();
		if($query->num_rows() > 0)
		{
			return 1;
		}
		else{
			return 0;
		}
		//SELECT * FROM sms_code_varification  scvWHERE scv.`sms_code` = '1212'  
		
	}
}

if ( ! function_exists('generateCode'))
{
	function generateCode(){
				$length = 5;
					$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
					$string = '';
	 
						for ($p = 0; $p<=$length; $p++) {
							$string .= $characters[mt_rand(0, strlen($characters))];
						}
					  return $string;
							
	}
}

if ( ! function_exists('userinfo'))
{
	function userinfo()
	{
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('admin_users');
			$CI->db->where('id',$CI->session->userdata('userid'));
			$query = $CI->db->get();
		if($query->num_rows() > 0)
		{
			$ppx = $query->result_array();
			$result = $ppx[0];			
			if($result['status']=='0')
			{	redirect_to_login(4);	}
			else
			{
				return $result;			
			}
		}
		else
		{
			redirect_to_login(3);
		}	
	}
}
// ------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('redirect_to_login'))
{
	function redirect_to_login($errorcode)
	{
		$CI =& get_instance();		
		$CI->session->sess_destroy();
		redirect(base_url().'admin?e='.$errorcode);
		
	}
}
if ( ! function_exists('project_evolution'))
{
	function project_evolution($applicant_id)
	{
		$CI =& get_instance();		
		$q = $CI->db->query("SELECT * FROM project_evolution WHERE applicant_id='".$applicant_id."'");
		foreach($q->result() as $data)
		{
			return $data;
		}
		
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('people'))
{
	function people()
	{
		 return array(
		 'الرئيس التنفيذي لصندوق الرفد طارق بن سليمان الفارسي',
		 	'ابراهيم بن  سالم العلوي',
			'ابراهيم بن  سالم العلوي',
			'يونس بن محمد النصري',
			'محمد بن خميس الحنشي',
			'اميرة بنت احمد الزدجالية',
			'عبود بن عامر العمري',
			'حسن بن علي الرئيسي',
			'ايمان بنت درويش الفارسية',
			'حميدة بنت سلوم الشكيري'
		 );
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('calcPay'))
{
	/*$principal = 684000; //Mortgage Amount 
	$interest_rate = 2.89; //Interest Rate %
	$down = $principal *0.10; //10% down payment
	$years = 25;
	$months = 0;
	$compound = 2; //compound is always set to 2
	$frequency = 12; //Number of months (Monthly (12), Semi-Monthly (24), Bi-Weekly(26) and Weekly(52) 
*/
	function calcPay($MORTGAGE, $AMORTYEARS, $AMORTMONTHS, $INRATE, $COMPOUND, $FREQ, $DOWN,$PERCENT='')
	{
		$MORTGAGE = $MORTGAGE - $DOWN;
		$compound = $COMPOUND/12;
		$monTime = ($AMORTYEARS * 12) + (1 * $AMORTMONTHS);
		$RATE = ($INRATE*1.0)/100;
		$yrRate = $RATE/$COMPOUND;
		$rdefine = pow((1.0 + $yrRate),$compound)-1.0;
		if($PERCENT=='A')
		{
			$percent = $MORTGAGE*$INRATE/100;
			return arabic_date(number_format($percent,0)).'&nbsp;&nbsp;ريال عماني';
		}
		$PAYMENT = ($MORTGAGE*$rdefine * (pow((1.0 + $rdefine),$monTime))) / ((pow((1.0 + $rdefine),$monTime)) - 1.0);
		if($FREQ==12){
			return number_format($PAYMENT,0);}
		if($FREQ==26){
			return $PAYMENT/2.0;}
		if($FREQ==52){
			return $PAYMENT/4.0;}
		if($FREQ==24){
			$compound2 = $COMPOUND/$FREQ;
			$monTime2 = ($AMORTYEARS * $FREQ) + ($AMORTMONTHS * 2);
			$rdefine2 = pow((1.0 + $yrRate),$compound2)-1.0;
			$PAYMENT2 = ($MORTGAGE*$rdefine2 * (pow((1.0 + $rdefine2),$monTime2)))/  ((pow((1.0 + $rdefine2),$monTime2)) - 1.0);
			return $PAYMENT2;
			}
		}
}

//////////////////////////////////////////////////////////////////////
if ( ! function_exists('get_module'))
{
	function get_module($firstURL='',$secondURL='')
	{
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('mh_modules');
        if($firstURL==''){
        $firstURL = $CI->uri->segment(1); }
        if($secondURL==''){
        $secondURL = $CI->uri->segment(2);}
        if($firstURL!='' && $secondURL=='')
        {
			$CI->db->like('module_controller',$firstURL);
        }
        else if($secondURL!='')
        {
            //listing_managment/add#7
            $fullURL = $firstURL.'/'.$secondURL;
            $CI->db->like('module_controller',$fullURL);
        }
            $CI->db->where('module_status','A');
			$query = $CI->db->get();
			$ppx = $query->result_array();
			$query->free_result();

			return $ppx[0];
	}
}

if ( ! function_exists('get_order_id'))
{
	function get_order_id()
	{		
		/*$CI =& get_instance();
		$Child = $CI->uri->segment(1).'/'.$CI->uri->segment(2);
		$q = $CI->db->query("SELECT a.module_name, a.module_order, a.module_show_content FROM mh_modules AS a, mh_modules AS b WHERE a.`moduleid`=b.`module_parent` AND a.`module_controller`='".$Child."' GROUP BY a.`module_order`");
		
		foreach($q->result() as $xxxxxx)
		{
			return $xxxxxx->module_show_content;
		}*/
	}
}

if ( ! function_exists('arabic_date'))
{
	function arabic_date($date)
	{
		header('Content-Type: text/html; charset=utf-8');
		$standard = array("0","1","2","3","4","5","6","7","8","9");
		$eastern_arabic_symbols = array("٠","١","٢","٣","٤","٥","٦","٧","٨","٩");
		$arabic_date = str_replace($standard , $eastern_arabic_symbols ,$date);	
		return $arabic_date;
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('noticeboard'))
{
	function noticeboard($tempid)
	{
		$CI =& get_instance();
		$html = '<div class="noticeboard">';
		$CI->db->select('list_name,list_id');
		$CI->db->from('list_management');
			$CI->db->where('list_type','rules');
			$CI->db->where('list_status','1');
			$query = $CI->db->get();
			$html .= '<div class="qhead">المستندات المطلوبة</div>';
			$xp = 0;
			foreach($query->result() as $parent)
			{	//uploader_document				
				$html .= '<div class="qrule opendocument" id="'.$parent->list_id.'">'.$parent->list_name;
				$html .= '<form autocomplete="off" method="post" action="'.base_url().'inquiries/upload_file" target="uploader_document" name="frm'.$parent->list_id.'" id="frm'.$parent->list_id.'"  enctype="multipart/form-data" >';
				$html .= '<input type="hidden" name="document_id" id="document_id" value="'.$parent->list_id.'" />';
				$html .= '<input name="frmdocument" style="display:none; width: 89px; float: left;" data-value="'.$parent->list_id.'" class="fileupload" type="file" data-attribute="'.$parent->list_id.'" id="file'.$parent->list_id.'">';
				$html .= '</form>';
				$html .= '</div>';
				$xp++;
			}
		$html .= '</div>';
		echo $html;
	}
}

if ( ! function_exists('showFileUpload'))
{
	function showFileUpload($tempid)
	{
			$CI =& get_instance();
				$html .= '<div class="form_raw unmusanif"  id="type_value2" style="display:none;">';
				$html .= '<div class="user_txt"></div>';
				$html .='<div class="user_field">';
				$html .= '<form autocomplete="off" method="post" action="'.base_url().'inquiries/upload_file" target="uploader_document" name="frm'.$tempid.'" id="frm'.$tempid.'"  enctype="multipart/form-data" >';
				$html .= '<input type="hidden" name="document_id" id="document_id" value="'.$tempid.'" />';
				$html .= '<input name="frmdocument" style="width: 89px; float: left;" data-value="" class="fileupload" type="file" data-attribute="" id="file">';
				$html .= '</form>';
				$html .= '</div>';
				$html .= '</div>';
				echo $html;
	}
}
///////////////////////////////////////////////////////////////////////

/*$html .= '<div class="qrule opendocument" id="'.$parent->list_id.'">'.$parent->list_name;
				$html .= '<form autocomplete="off" method="post" action="'.base_url().'inquiries/upload_file" target="uploader_document" name="frm'.$parent->list_id.'" id="frm'.$parent->list_id.'"  enctype="multipart/form-data" >';
				$html .= '<input type="hidden" name="document_id" id="document_id" value="'.$parent->list_id.'" />';
				$html .= '<input name="frmdocument" style="display:none; width: 89px; float: left;" data-value="'.$parent->list_id.'" class="fileupload" type="file" data-attribute="'.$parent->list_id.'" id="file'.$parent->list_id.'">';
				$html .= '</form>';
				$html .= '</div>';
	*/


///////////////////////////////////////////////////////////////////////
if ( ! function_exists('upload_file'))
{
	function upload_file($filefield,$folderpath,$thumb=false,$w=0,$h=0)
	{
			$CI =& get_instance();
			if (!is_dir($folderpath))
			{	mkdir($folderpath, 0777, true);	}
			
			$config['upload_path'] = $folderpath;
			$config['allowed_types'] = '*';
			$config['max_size']	= '5000';
			$config['encrypt_name'] = TRUE;
			$CI->load->library('upload', $config);
			if (!$CI->upload->do_upload($filefield))
			{
				$error = array('error' => $CI->upload->display_errors());
				return $error	=	'';
			}
			else
			{
				$image_data = $CI->upload->data();
				
				return $image_data['file_name'];
			}
		}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('inquiry_type_tree'))
{
	function inquiry_type_tree($tempid)
	{
		$CI =& get_instance();
		
		$tree = '';
		$others = '';
		$q = $CI->db->query("SELECT list_id,list_name  FROM list_management WHERE list_status='1' AND `list_parent_id`='0' AND list_type='inquiry_type' ORDER BY list_order ASC");
		foreach($q->result() as $parent)
		{
			if($parent->list_id == '227'){
				
				$others .= '<div class="parent row" data-id="'.$parent->list_id.'" id="'.$parent->list_id.'">
				<div class="col-xs-8"><input id="inquirytypeid" data-handler="'.$tempid.'_'.$tempid.'" type="checkbox" name="inquiry_type[]" class="inquirytypeid tempinqury" value="'.$parent->list_id.'" /> '.$parent->list_name.' </div>
				<div class="col-xs-4" style="display:none !important;"><input data-handler="'.$tempid.'_'.$tempid.'" name="datepicker" value="'.date('Y-m-d h:i:s').'" type="text" class="form-control dpicker mydatepicker'.$parent->list_id.' tempinqury" data-id="inqirydate" id="inqirydate'.$parent->list_id.'"></div>
				</div>';
				$qchild = $CI->db->query("SELECT list_id,list_name FROM list_management WHERE list_status='1' AND `list_parent_id`='".$parent->list_id."' ORDER BY list_order ASC");
				foreach($qchild->result() as $child)
				{
					$others .= '<div class="globe row child child_'.$parent->list_id.'" data-id="'.$child->list_id.'" id="child_'.$parent->list_id.'">
					<div class="col-xs-8"><input id="inquirytypeid" data-handler="'.$tempid.'_'.$tempid.'" type="checkbox" name="inquiry_type[]" class="childxxxxx  inquirytypeid tempinqury" value="'.$child->list_id.'" /> '.$child->list_name.' </div>
					<div class="col-xs-4" style="display:none !important;"><input data-handler="'.$tempid.'_'.$tempid.'" name="datepicker" value="'.date('Y-m-d h:i:s').'" type="text" class="form-control dpicker mydatepicker'.$child->list_id.' tempinqury" data-id="inqirydate" id="inqirydate'.$child->list_id.'"></div>
					</div>';
					$subchild = $CI->db->query("SELECT list_id,list_name FROM list_management WHERE list_status='1' AND `list_parent_id`='".$child->list_id."' ORDER BY list_order ASC");
					foreach($subchild->result() as $subchild)
					{
						$others .= '<div class="globe row smallchild smallchild_'.$child->list_id.'" data-id="'.$subchild->list_id.'" id="smallchild_'.$child->list_id.'">
						<div class="col-xs-8"><input id="inquirytypeid" data-handler="'.$tempid.'_'.$tempid.'" type="checkbox" name="inquiry_type[]" class="inquirytypeid tempinqury" value="'.$subchild->list_id.'" /> '.$subchild->list_name.' </div>
						<div class="col-xs-4" style="display:none !important;"><input data-handler="'.$tempid.'_'.$tempid.'" name="datepicker" value="'.date('Y-m-d h:i:s').'"  type="text" class="form-control dpicker mydatepicker'.$subchild->list_id.' tempinqury" data-id="inqirydate" id="inqirydate'.$subchild->list_id.'"></div>
						</div>';
					}
				}

			}
			else{
				$tree .= '<div class="parent row" data-id="'.$parent->list_id.'" id="'.$parent->list_id.'">
				<div class="col-xs-8"><input id="inquirytypeid" data-handler="'.$tempid.'_'.$tempid.'" type="checkbox" name="inquiry_type[]" class="inquirytypeid tempinqury" value="'.$parent->list_id.'" /> '.$parent->list_name.' </div>
				<div class="col-xs-4" style="display:none !important;"><input data-handler="'.$tempid.'_'.$tempid.'" name="datepicker" value="'.date('Y-m-d h:i:s').'" type="text" class="form-control dpicker mydatepicker'.$parent->list_id.' tempinqury" data-id="inqirydate" id="inqirydate'.$parent->list_id.'"></div>
				</div>';
				$qchild = $CI->db->query("SELECT list_id,list_name FROM list_management WHERE list_status='1' AND `list_parent_id`='".$parent->list_id."' ORDER BY list_order ASC");
				foreach($qchild->result() as $child)
				{
					$tree .= '<div class="globe row child child_'.$parent->list_id.'" data-id="'.$child->list_id.'" id="child_'.$parent->list_id.'">
					<div class="col-xs-8"><input id="inquirytypeid" data-handler="'.$tempid.'_'.$tempid.'" type="checkbox" name="inquiry_type[]" class="childxxxxx  inquirytypeid tempinqury" value="'.$child->list_id.'" /> '.$child->list_name.' </div>
					<div class="col-xs-4" style="display:none !important;"><input data-handler="'.$tempid.'_'.$tempid.'" name="datepicker" value="'.date('Y-m-d h:i:s').'" type="text" class="form-control dpicker mydatepicker'.$child->list_id.' tempinqury" data-id="inqirydate" id="inqirydate'.$child->list_id.'"></div>
					</div>';
					$subchild = $CI->db->query("SELECT list_id,list_name FROM list_management WHERE list_status='1' AND `list_parent_id`='".$child->list_id."' ORDER BY list_order ASC");
					foreach($subchild->result() as $subchild)
					{
						$tree .= '<div class="globe row smallchild smallchild_'.$child->list_id.'" data-id="'.$subchild->list_id.'" id="smallchild_'.$child->list_id.'">
						<div class="col-xs-8"><input id="inquirytypeid" data-handler="'.$tempid.'_'.$tempid.'" type="checkbox" name="inquiry_type[]" class="inquirytypeid tempinqury" value="'.$subchild->list_id.'" /> '.$subchild->list_name.' </div>
						<div class="col-xs-4" style="display:none !important;"><input data-handler="'.$tempid.'_'.$tempid.'" name="datepicker" value="'.date('Y-m-d h:i:s').'" type="text" class="form-control dpicker mydatepicker'.$subchild->list_id.' tempinqury" data-id="inqirydate" id="inqirydate'.$subchild->list_id.'"></div>
						</div>';
					}
				}
			}
		}
		$tree.=$others;
		$tree .= '';
		echo $tree;
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('get_mobilenumbers'))
{
	function get_mobilenumbers($tempid)
	{
		$CI =& get_instance();
		$q = $CI->db->query("SELECT GROUP_CONCAT(CONCAT('968',phonenumber)) AS pnumber FROM main_phone WHERE tempid='".$tempid."';");
		foreach($q->result() as $data)
		{
			return $data->pnumber;
		}
	}
}

if ( ! function_exists('get_applicant_number'))
{
	function get_applicant_number($applicantid)
	{
		$CI =& get_instance();
		$q = $CI->db->query("SELECT GROUP_CONCAT(CONCAT('968',applicant_phone)) AS pnumber FROM applicant_phones WHERE applicant_id='".$applicantid."';");
		foreach($q->result() as $data)
		{
			return $data->pnumber;
		}
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('stepsLogs'))
{
	function stepsLogs($array)
	{
		$CI =& get_instance();
		$data = array(
			'applicantid'=>$array['aid'],
			'userid'=>$CI->session->userdata('userid'),
			'stepsid'=>$array['sid'],
			'processip'=>$_SERVER['REMOTE_ADDR'],
			'processdevice'=>$_SERVER['HTTP_USER_AGENT']);
			$CI->db->insert('applicant_process_log',$data);
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('applicant_activity'))
{
	function applicant_activity()
	{
		$app_activity = array('مالك','شريك','مفوض بالتوقيع');
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('exdrobpx'))
{
	function exdrobpx($name,$value,$optiontitle,$list_type,$req,$tempid=0)
	{
		//echo $req;
		$CI =& get_instance();		
		$CI->db->select('list_id,list_name,list_type,other');
		$CI->db->from('list_management');		
		//$CI->db->where('list_id',$list_id);
		$CI->db->where('list_type',$list_type);
		$CI->db->where('list_status','1');		
		$CI->db->order_by("list_order", "ASC"); 
		$query = $CI->db->get();
		if($list_type=='loan_reason1')
		{
			$mb = 'multiple="multiple"';
		}
		$dropdown = '<select data-handler="'.$tempid.'_'.$tempid.'"  name="'.$name.'" id="'.$name.'" '.$mb.' class="'.$req.' checkother tempmain" placeholder="'.$optiontitle.'">';
		$dropdown .= '<option value="">'.$optiontitle.'</option>';
		
		foreach($query->result() as $row)
		{
			$ls = list_types($row->list_type);
			$dropdown .= '<option dataid="'.$row->other.':'.$ls['fname'].'" value="'.$row->list_id.'" ';
			if($value==$row->list_id)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->list_name.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}


//////////////////////////////////////////////////////////////////////
if ( ! function_exists('reigons_partner'))
{
	function reigons_partner($name,$value)
	{
		$CI =& get_instance();		
		$CI->db->select('REIGONID,REIGONNAME');
		$CI->db->from('election_reigons');	
		$CI->db->order_by("REIGONNAME", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select name="'.$name.'" id="'.$name.'" class="provinceExtra" placeholder="ختر المحافظة">';
		$dropdown .= '<option value="">اختر المحافظة</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->REIGONID.'" ';
			if($value==$row->REIGONID)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->REIGONNAME.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}

if ( ! function_exists('wilayats_partner'))
{
	function wilayats_partner($name,$value,$provinceid=0)
	{
		$CI =& get_instance();		
		$CI->db->select('WILAYATID,WILAYATNAME');
		$CI->db->from('election_wilayats');	
		if($provinceid!='0')
		{
			$CI->db->where('REIGONID',$provinceid);
		}
		$CI->db->order_by("WILAYATNAME", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select name="'.$name.'" id="'.$name.'" placeholder="اختر الولاية">';
		$dropdown .= '<option value="">اختر الولاية</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->WILAYATID.'" ';
			if($value==$row->WILAYATID)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->WILAYATNAME.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}

//////////////////////////////////////////////////////////////////////
if ( ! function_exists('hd_dropbox'))
{
	function hd_dropbox($name,$value,$optiontitle,$list_type,$required='',$othervalue='',$otherplaceholder='',$othername='')
	{
		
		$CI =& get_instance();		
		$CI->db->select('list_id,list_name,list_type,other');
		$CI->db->from('list_management');		
		$CI->db->where('list_type',$list_type);
		$CI->db->where('list_status','1');		
		$CI->db->order_by("list_order", "ASC"); 
		$query = $CI->db->get();
		$dropdown = '<div class="form_field_selected"><select name="'.$name.'" id="'.$name.'" class="'.$required.' checkExtra " placeholder="'.$optiontitle.'">';
		$dropdown .= '<option dataid="0:'.$othername.'" value="">'.$optiontitle.'</option>';		
		foreach($query->result() as $row)
		{	//$ls = list_types($row->list_type);			
			$dropdown .= '<option dataid="'.$row->other.':'.$othername.'" value="'.$row->list_id.'" ';
			if($value==$row->list_id)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->list_name.'</option>';
		}
		if($othervalue!='')
		{
			$disp = ' style="display:block !important;"';
		}
		$dropdown .= '</select>
		</div><div class="extra_field"><input '.$disp.' name="'.$othername.'" id="'.$othername.'" value="'.$othervalue.'" placeholder="'.$otherplaceholder.'" type="text" class="txt_field othervalue"></div>';
		echo($dropdown);
		unset($disp);
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('hd_dropbox2'))
{
	function hd_dropbox2($name,$value,$optiontitle,$list_type,$required='',$othervalue='',$otherplaceholder='',$othername='')
	{
		
		$CI =& get_instance();		
		$CI->db->select('list_id,list_name,list_type,other');
		$CI->db->from('list_management');		
		$CI->db->where('list_type',$list_type);
		$CI->db->where('list_status','1');		
		$CI->db->order_by("list_order", "ASC"); 
		$query = $CI->db->get();
		$dropdown = '<div class="form_field_selected"><select name="'.$name.'" id="'.$name.'" class="'.$required.' checkExtra " placeholder="'.$optiontitle.'">';
		$dropdown .= '<option dataid="0:'.$othername.'" value="">'.$optiontitle.'</option>';		
		foreach($query->result() as $row)
		{	//$ls = list_types($row->list_type);			
			$dropdown .= '<option dataid="'.$row->other.':'.$othername.'" value="'.$row->list_id.'" ';
			if($value==$row->list_id)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->list_name.'</option>';
		}
		$dropdown .= '</select>
		</div><div class="extra_field"><input name="'.$othername.'" id="'.$othername.'" value="'.$othervalue.'" placeholder="'.$otherplaceholder.'" type="text" class="txt_field othervalue"></div>';
		echo($dropdown);
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('multiporpose_dropbox'))
{
	function multiporpose_dropbox($name,$applicantid,$tempid,$value,$optiontitle,$list_type,$required='',$othervalue='',$otherplaceholder='',$othername='')
	{
		
		$CI =& get_instance();		
		$CI->db->select('list_id,list_name,list_type,other');
		$CI->db->from('list_management');		
		$CI->db->where('list_type',$list_type);
		$CI->db->where('list_status','1');		
		$CI->db->order_by("list_order", "ASC"); 
		$query = $CI->db->get();
		$other_status = 0;
		$dropdown = '<select name="'.$name.'_'.$applicantid.'" id="'.$name.'" data-handler="'.$tempid.'_'.$applicantid.'" class="'.$required.' tempapplicant extraInput form-control" placeholder="'.$optiontitle.'">';
		$dropdown .= '<option dataid="0:'.$othername.':'.$applicantid.'" value="">'.$optiontitle.'</option>';		
		$q = $query->result();
		foreach($q as $row)
		{	//$ls = list_types($row->list_type);			
			$dropdown .= '<option dataid="'.$row->other.':'.$othername.':'.$applicantid.':'.$row->other.'" value="'.$row->list_id.'" ';
			if($value==$row->list_id)
			{
				if($row->other == '1'){
					$other_status = 1;	
				}
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->list_name.'</option>';
		}
		$dropdown .= '</select>';
		
		foreach($q as $row)
		{	//$ls = list_types($row->list_type);
			if($row->other){			
				$dropdown .= '<input name="other'.$row->list_id.'" id="other'.$row->list_id.'" value="'.$othervalue.'"  data-handler="'.$tempid.'_'.$applicantid.'" placeholder="'.$otherplaceholder.'" type="hidden" class="form-control tempapplicant othervalue">';
			}
		}
		if($other_status){
			$display = 'block';
		}
		else{
			$display = 'none';
		}
		//echo $display;	
		$dropdown .='<div class="extra_field_x"><input name="'.$othername.'_'.$applicantid.'" id="'.$othername.'" value="'.$othervalue.'"  data-handler="'.$tempid.'_'.$applicantid.'" placeholder="'.$otherplaceholder.'" value="1" type="text" class="form-control tempapplicant othervalue" style="display:'.$display.'"></div>';
		echo($dropdown);
	}
}

if ( ! function_exists('multiporpose_dropbox_partner'))
{
	function multiporpose_dropbox_partner($index,$name,$applicantid,$tempid,$value,$optiontitle,$list_type,$required='',$othervalue='',$otherplaceholder='',$othername='')
	{
		
		$CI =& get_instance();		
		$CI->db->select('list_id,list_name,list_type,other');
		$CI->db->from('list_management');		
		$CI->db->where('list_type',$list_type);
		$CI->db->where('list_status','1');		
		$CI->db->order_by("list_order", "ASC"); 
		$query = $CI->db->get();
		$other_status = 0;
		$dropdown = '<div class="form_field_selected"><select name="'.$name.'['.$index.']" id="'.$name.'" data-handler="'.$tempid.'_'.$applicantid.'" class="'.$required.' extraInput " placeholder="'.$optiontitle.'">';
		$dropdown .= '<option dataid="0:'.$othername.':'.$applicantid.'" value="">'.$optiontitle.'</option>';		
		$q = $query->result();
		foreach($q as $row)
		{	//$ls = list_types($row->list_type);			
			$dropdown .= '<option dataid="'.$row->other.':'.$othername.':'.$applicantid.':'.$row->other.'" value="'.$row->list_id.'" ';
			if($value==$row->list_id)
			{
				if($row->other == '1'){
					$other_status = 1;	
				}
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->list_name.'</option>';
		}
		$dropdown .= '</select>';
		
		foreach($q as $row)
		{	//$ls = list_types($row->list_type);
			if($row->other){			
				$dropdown .= '<input name="other'.$row->list_id.'" id="other'.$row->list_id.'" value="'.$othervalue.'"  data-handler="'.$tempid.'_'.$applicantid.'" placeholder="'.$otherplaceholder.'" type="hidden" class="txt_field tempapplicant othervalue">';
			}
		}
		if($other_status){
			$display = 'block';
		}
		else{
			$display = 'none';
		}
		//echo $display;	
		$dropdown .='</div><div class="extra_field_x"><input name="'.$othername.'_'.$applicantid.'" id="'.$othername.'" value="'.$othervalue.'"  data-handler="'.$tempid.'_'.$applicantid.'" placeholder="'.$otherplaceholder.'" value="1" type="text" class="txt_field tempapplicant othervalue" style="display:'.$display.'"></div>';
		echo($dropdown);
	}
}

//////////////////////////////////////////////////////////////////////
if ( ! function_exists('show_date'))
{
	function show_date($date,$case)
	{
		if($date=='')
		{	$date = date('Y-m-d');	}
		
		$months = array('1'=>"ينــاير",'2'=>"فبرايــر",'3'=>"مــارس",'4'=>"ابريــل",'5'=>"مــايو",'6'=>"يــونيه",'7'=>"يوليــو",'8'=>"أغسطـــس",'9'=>"سبتمبــر",'10'=>"أكتوبــر",'11'=>"نوفمبــر",'12'=>"ديسمبــر");
		$days = array("الأحـد", "الإثنين", "الثــلاثاء", "الإربعــاء", "الخميــس", "الجمعــة", "السبــت");
		switch($case)
		{
			case '1';
				$day = arabic_date(date('d',strtotime($date)));
				$month = $months[date('n',strtotime($date))];
				$year =  arabic_date(date('Y',strtotime($date)));
				echo $day.' '.$month.' '.$year;
			break;
            case '2';
                $day = arabic_date(date('d',strtotime($date)));
                $month = $months[date('n',strtotime($date))];
                $year =  arabic_date(date('Y',strtotime($date)));
                $dayx = date('w');
                echo $days[$dayx].' '.$day.' '.$month.' '.$year;
                break;
			
			case '3';
				$day = arabic_date(date('d',strtotime($date)));
				$month = $months[date('n',strtotime($date))];
				$year =  arabic_date(date('Y',strtotime($date)));
				echo $day.' '.$month.' '.$year.' '.arabic_date(date('h:i:s',strtotime($date)));
			break;
			
			case '2';
				$day = date('w');
				echo $days[$day];
			break;
		}
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('showtable_names'))
{
	function showtable_names($tablename)
	{
		$table_name = str_replace('`','',trim($tablename));
		$amb = array('temp_main'=>'',
					'applicants'=>'مقدم الطلب',
					'applicant_phones'=>'مقدم الطلب الهاتف',
					'applicant_process_log'=>'سجل عملية مقدم الطلب',
					'applicant_qualification'=>'مؤهلات مقدم الطلب',
					'applicant_businessrecord'=>'طالب الشركة سجل',
					'applicant_professional_experience'=>'الخبرة المهنية الطالبة',
					'applicant_partners'=>'شركاء مقدم الطلب',
					'applicant_partner_experience'=>'تجربة شريك مقدم الطلب',
					'applicant_partner_businessrecord'=>'سجل الأعمال شريك مقدم الطلب',
					'applicant_partner_phones'=>'الهاتف شريك مقدم الطلب',
					'temp_main'=>'بيانات مؤقتة',
					'temp_main_applicant'=>'بيانات مؤقتة',
					'temp_main_phone'=>'بيانات مؤقتة',
					'temp_main_inquirytype'=>'بيانات مؤقتة',
					'temp_main_notes'=>'بيانات مؤقتة',
					'branches'=>'فرع',);
		if($amb[$table_name]!='')
		{
			return $amb[$table_name];
		}
		else
		{
			return $table_name;
		}
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('loan_category'))
{
	function loan_category($name,$value,$parent =0)
	{
		if($parent){
			$parent_class = "child";
		}
		else{
			$parent_class = "parent";
		}
		$CI =& get_instance();		
		$CI->db->select('loan_caculate_id,loan_category_id,loan_category_name');
		$CI->db->from('loan_calculate');	
		$CI->db->where('parent_id',$parent);
		$CI->db->order_by("loan_category_name", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select name="'.$name.'" id="'.$name.'" class="req '.$parent_class.'" placeholder="البرنامج المطلوب‎" onchange="load_child_category(this.value)">';
		$dropdown .= '<option value="">البرنامج المطلوب‎</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->loan_caculate_id.'" ';
			if($value==$row->loan_caculate_id)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->loan_category_name.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}
if ( ! function_exists('get_branches'))
{
	function get_branches($name,$value)
	{
		$CI =& get_instance();		
		$CI->db->select('branch_id,branch_name');
		$CI->db->from('branches');	
		$CI->db->order_by("branch_name", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select name="'.$name.'" id="'.$name.'" class="req form-control" placeholder="اختيار فرع">';
		$dropdown .= '<option value="">اختيار فرع</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->branch_id.'" ';
			if($value==$row->branch_id)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->branch_name.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}



if ( ! function_exists('get_bank_branches'))
{
	function get_bank_branches($name,$value)
	{
		$CI =& get_instance();		
		$CI->db->select('branch_id,branch_name');
		$CI->db->from('bank_branches');	
		$CI->db->order_by("branch_id", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select name="'.$name.'" id="'.$name.'" class="req" placeholder="اختيار فرع">';
		$dropdown .= '<option value="">اختيار فرع</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->branch_id.'" ';
			if($value==$row->branch_id)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->branch_name.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}//////////////////////////////////////////////////////////////////////
if ( ! function_exists('reigons'))
{
	function reigons($name,$value,$tempid=0,$req='req')
	{
		$CI =& get_instance();		
		$CI->db->select('REIGONID,REIGONNAME');
		$CI->db->from('election_reigons');	
		$CI->db->order_by("REIGONNAME", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select data-handler="'.$tempid.'_'.$tempid.'"  name="'.$name.'" id="'.$name.'" class="'.$req.' tempmain" placeholder="ختر المحافظة">';
		$dropdown .= '<option value="">اختر المحافظة</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->REIGONID.'" ';
			if($value==$row->REIGONID)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->REIGONNAME.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}

if ( ! function_exists('election_wilayats'))
{
	function election_wilayats($name,$value,$provinceid=0,$tempid=0,$req='req')
	{
		$CI =& get_instance();		
		$CI->db->select('WILAYATID,WILAYATNAME');
		$CI->db->from('election_wilayats');	
		if($provinceid!='0')
		{
			$CI->db->where('REIGONID',$provinceid);
		}
		$CI->db->order_by("WILAYATNAME", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select data-handler="'.$tempid.'_'.$tempid.'"  name="'.$name.'" id="'.$name.'" class="'.$req.' tempmain" placeholder="اختر الولاية">';
		$dropdown .= '<option value="">اختر الولاية</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->WILAYATID.'" ';
			if($value==$row->WILAYATID)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->WILAYATNAME.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('reigons_applicant'))
{
	function reigons_applicant($name,$value,$tempid=0,$applicantid=0)
	{
		$CI =& get_instance();		
		$CI->db->select('REIGONID,REIGONNAME');
		$CI->db->from('election_reigons');	
		$CI->db->order_by("REIGONNAME", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select data-handler="'.$tempid.'_'.$applicantid.'"  name="'.$name.'_'.$applicantid.'" id="'.$name.'" class="req tempapplicant" placeholder="ختر المحافظة">';
		$dropdown .= '<option value="">اختر المحافظة</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->REIGONID.'" ';
			if($value==$row->REIGONID)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->REIGONNAME.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}

if ( ! function_exists('reigons_partner_applicant'))
{
	function reigons_partner_applicant($ind,$name,$value,$req,$tempid=0,$applicantid=0)
	{
		//echo $value;
		//exit;
		$CI =& get_instance();		
		$CI->db->select('REIGONID,REIGONNAME');
		$CI->db->from('election_reigons');	
		$CI->db->order_by("REIGONNAME", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select data-handler="'.$tempid.'_'.$applicantid.'"  name="'.$name.'_'.$applicantid.'" id="'.$name.'" class="'.$req.' tempapplicant partnr form-control" placeholder="ختر المحافظة" onchange="getParnterWilaya('.$ind.')">';
		$dropdown .= '<option value="">اختر المحافظة</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->REIGONID.'" ';
			if($value==$row->REIGONID)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->REIGONNAME.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}

if ( ! function_exists('reigons_partner_inq'))
{
	function reigons_partner_inq($ind,$name,$value,$req,$tempid=0,$applicantid=0)
	{
		//echo $value;
		//exit;
		$CI =& get_instance();		
		$CI->db->select('REIGONID,REIGONNAME');
		$CI->db->from('election_reigons');	
		$CI->db->order_by("REIGONNAME", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select data-handler="'.$tempid.'_'.$applicantid.'"  name="'.$name.'['.$ind.']" id="'.$name.'" class="'.$req.' partnr" placeholder="ختر المحافظة" onchange="getParnterWilaya('.$ind.')">';
		$dropdown .= '<option value="">اختر المحافظة</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->REIGONID.'" ';
			if($value==$row->REIGONID)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->REIGONNAME.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}

if ( ! function_exists('election_wilayats_applicant'))
{
	function election_wilayats_applicant($name,$value,$provinceid=0,$req='',$tempid=0,$applicantid=0)
	{
		$CI =& get_instance();		
		$CI->db->select('WILAYATID,WILAYATNAME');
		$CI->db->from('election_wilayats');	
		if($provinceid!='0')
		{
			$CI->db->where('REIGONID',$provinceid);
		}
		$CI->db->order_by("WILAYATNAME", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select data-handler="'.$tempid.'_'.$applicantid.'"  name="'.$name.'_'.$applicantid.'" id="'.$name.'" class="req tempapplicant" placeholder="اختر الولاية">';
		$dropdown .= '<option value="">اختر الولاية</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->WILAYATID.'" ';
			if($value==$row->WILAYATID)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->WILAYATNAME.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}

if ( ! function_exists('election_wilayats_partner_applicant'))
{
	function election_wilayats_partner_applicant($name,$value,$provinceid=0,$req,$tempid=0,$applicantid=0)
	{
		$CI =& get_instance();		
		$CI->db->select('WILAYATID,WILAYATNAME');
		$CI->db->from('election_wilayats');	
		if($provinceid!='0')
		{
			$CI->db->where('REIGONID',$provinceid);
		}
		$CI->db->order_by("WILAYATNAME", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select data-handler="'.$tempid.'_'.$applicantid.'"  name="'.$name.'_'.$applicantid.'" id="'.$name.'" class="'.$req.' tempapplicant wilaya form-control" placeholder="اختر الولاية">';
		$dropdown .= '<option value="">اختر الولاية</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->WILAYATID.'" ';
			if($value==$row->WILAYATID)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->WILAYATNAME.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}

if ( ! function_exists('election_wilayats_partner_inq'))
{
	function election_wilayats_partner_inq($ind,$name,$value,$provinceid=0,$req,$tempid=0,$applicantid=0)
	{
		$CI =& get_instance();		
		$CI->db->select('WILAYATID,WILAYATNAME');
		$CI->db->from('election_wilayats');	
		if($provinceid!='0')
		{
			$CI->db->where('REIGONID',$provinceid);
		}
		$CI->db->order_by("WILAYATNAME", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select data-handler="'.$tempid.'_'.$applicantid.'"  name="'.$name.'['.$ind.']" id="'.$name.'" class="'.$req.' wilaya" placeholder="اختر الولاية">';
		$dropdown .= '<option value="">اختر الولاية</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->WILAYATID.'" ';
			if($value==$row->WILAYATID)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->WILAYATNAME.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('child_dropbox_role'))
{
	function child_dropbox_role($name,$value,$req='req',$parent_role='0')
	{
		$CI =& get_instance();		
		$CI->db->select('role_id,role_name');
		$CI->db->from('user_roles');	
		$CI->db->where('role_parent_id',$parent_role);
		$CI->db->order_by("role_name", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select  name="'.$name.'" id="'.$name.'" class="'.$req.' tempmain form-control" placeholder="اختر الوظيفة" >';
		$dropdown .= '<option value="">اختر الوظيفة</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->role_id.'" ';
			if($value==$row->role_id)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->role_name.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('parent_dropbox'))
{
	function parent_dropbox($name,$value,$req='req')
	{
		$CI =& get_instance();		
		$CI->db->select('role_id,role_name');
		$CI->db->from('user_roles');	
		$CI->db->where('role_parent_id','0');
		$CI->db->order_by("role_name", "ASC"); 
		$query = $CI->db->get();
		
		$dropdown = '<select  name="'.$name.'" id="'.$name.'" class="'.$req.' tempmain parent_role_class form-control" placeholder="اختر القسم" >';
		$dropdown .= '<option value="">اختر القسم</option>';

		foreach($query->result() as $row)
		{
			$dropdown .= '<option value="'.$row->role_id.'" ';
			if($value==$row->role_id)
			{
				$dropdown .= 'selected="selected"';
			}
			$dropdown .= '>'.$row->role_name.'</option>';
		}
		$dropdown .= '</select>';
		echo($dropdown);
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('show_date'))
{
	function show_date($date,$case)
	{
		if($date=='')
		{	$date = date('Y-m-d');	}
		
		$months = array('1'=>"ينــاير",'2'=>"فبرايــر",'3'=>"مــارس",'4'=>"ابريــل",'5'=>"مــايو",'6'=>"يــونيه",'7'=>"يوليــو",'8'=>"أغسطـــس",'9'=>"سبتمبــر",'10'=>"أكتوبــر",'11'=>"نوفمبــر",'12'=>"ديسمبــر");
		$days = array("الأحـد", "الإثنين", "الثــلاثاء", "الإربعــاء", "الخميــس", "الجمعــة", "السبــت");
		switch($case)
		{
			case '1';
				$day = arabic_date(date('d',strtotime($date)));
				$month = $months[date('n',strtotime($date))];
				$year =  arabic_date(date('Y',strtotime($date)));
				echo $day.' '.$month.' '.$year;
			break;
			
			case '2';
				$day = date('w');
				echo $days[$day];
			break;
		}
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('applicant_number'))
{
	function applicant_number($number)
	{
		return sprintf("%07s", $number);
	}
}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('list_types'))
{
	function list_types($value)
	{
		$listTYpe = array(
			'disable_type'=>array('en'=>'Disable Type','ar'=>'نوع الإعاقة','status'=>'disable','fname'=>'disable_text'),
			'maritalstatus'=>array('en'=>'Marital Status','ar'=>'الحالة الزوجية','status'=>'marital','fname'=>'marital_text'),
			'current_situation'=>array('en'=>'Current Situation','ar'=>'الوضع الحالي','status'=>'situation','fname'=>'job_text'),
			'inquiry_type'=>array('en'=>'Inquery Type','ar'=>'الاستفسار عن قرض','status'=>'inquiry_type','fname'=>''),
			'nature_project_site'=>array('en'=>'Nature Of Project','ar'=>'‫طبيعة موقع المشروع‬‎','status'=>'nature_project_site','fname'=>''),
			'nature_project'=>array('en'=>'Nature Project','ar'=>'‫‫طبيعة محل المشروع‬‎','status'=>'nature_project','fname'=>''),
			'business_type'=>array('en'=>'Business Type','ar'=>'‫‫‫القطاع الإقتصادي‬‎','status'=>'business_type','fname'=>''),
			'activity_project'=>array('en'=>'Activity Project','ar'=>'‫نشاط المشروع','status'=>'activity_project','fname'=>''),
			'project_employment'=>array('en'=>'Employment Project','ar'=>'‫‫التعمين‬‎','status'=>'project_employment','fname'=>''),
			'rules'=>array('en'=>'Rules','ar'=>'قواعد','status'=>'rules','fname'=>''),
			'project_type'=>array('en'=>'Rules','ar'=>'‫نوع المشروع‬‎','status'=>'project_type','fname'=>''),
			'qualification'=>array('en'=>'Rules','ar'=>'‫المؤهل‬‎','status'=>'qualification','fname'=>''),
			'institute'=>array('en'=>'Institute','ar'=>'الجهة','status'=>'institute','fname'=>''),
			'conversion'=>array('en'=>'Conversion','ar'=>'تحويل','status'=>'conversion','fname'=>'‫تحويل'),
			'postponed'=>array('en'=>'Postponed','ar'=>'‫تأجيل','status'=>'postponed','fname'=>'‫تأجيل‬‎'),
			'rejected'=>array('en'=>'Rejected','ar'=>'‫الرفض','status'=>'rejected','fname'=>'>‫الرفض‬‎')
			
			);
		return $listTYpe[$value];		
	}
}
//////////////////////////////////////////////////////
if ( ! function_exists('get_step_id'))
{
	function get_step_id($step)
	{
			$arMenu = array(
			'1'=>'newrequest',
			'2'=>'requestphasetwo',
			'3'=>'requestphasethree',
			'4'=>'requestphasefour');
			return $arMenu[$step];
	}
}
//////////////////////////////////////////////////////
if ( ! function_exists('get_heading'))
{
    function get_heading($step)
    {
        $arMenu = array(
            'h1'=>'قائمة معاملات ‫مشروطة',
            'h2'=>'قائمة قرار اللجنة',
            'h3'=>'قائمة‫ تأجيل ',
            'h4'=>'قائمة‫ ‫‫الرفض ',
            'h5'=>'قائمةالموافقة ');
        return $arMenu[$step];
    }
}

//////////////////////////////////////////////////////
if ( ! function_exists('is_set'))
{
    function is_set($value)
    {
        return isset($value) ? $value : '----';
    }
}
////////////////////////
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('role_dropbox'))
{
    function role_dropbox($name,$type)
    {
        $CI =& get_instance();
        if($type=='parent')
        {
            $CI->db->select('role_id,role_name');
            $CI->db->from('user_roles');
            $CI->db->where('role_parent_id','0');
            $CI->db->order_by("role_name", "ASC");
            $query = $CI->db->get();
            $dropdown = '<select  name="'.$name.'" id="'.$name.'" class="rolex" placeholder="اختر القسم" >';
            $dropdown .= '<option value="">اختر القسم</option>';
            foreach($query->result() as $row)
            {
                $dropdown .= '<option value="'.$row->role_id.'" ';
                if($value==$row->role_id)
                {
                    $dropdown .= 'selected="selected"';
                }
                $dropdown .= '>'.$row->role_name.'</option>';
            }
            $dropdown .= '</select>';
            echo($dropdown);
        }
        else if($type=='child')
        {
            $CI->db->select('role_id,role_name');
            $CI->db->from('user_roles');
            $CI->db->where('role_parent_id',$parent_role);
            $CI->db->order_by("role_name", "ASC");
            $query = $CI->db->get();

            $dropdown = '<select  name="'.$name.'" id="'.$name.'" class="rolex" placeholder="اختر الوظيفة" >';
            $dropdown .= '<option value="">اختر الوظيفة</option>';

            foreach($query->result() as $row)
            {
                $dropdown .= '<option value="'.$row->role_id.'" ';
                if($value==$row->role_id)
                {
                    $dropdown .= 'selected="selected"';
                }
                $dropdown .= '>'.$row->role_name.'</option>';
            }
            $dropdown .= '</select>';
            echo($dropdown);
        }
    }
}

if ( ! function_exists('check_permission'))
{
    function check_permission($moduledata,$permtype='v')
    {
        $CI =& get_instance();
        $moduleid = $moduledata['moduleid'];

        $userid = $CI->session->userdata('userid');
        $CI->db->select('permission');
        $CI->db->from('system_permission');
        $CI->db->where('userid',$userid);
        $query = $CI->db->get();
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $sp)
            {
                $perm = json_decode($sp->permission,TRUE);
                $newperm = $perm[$moduleid];
                if($newperm['v']==0 && $permtype=='v')
                {	redirect('dashboard');
                }
                else
                {	return $newperm[$permtype];	}	//It will return 0 or 1
            }
        }
        else
        {
            redirect('dashboard');
        }
    }
}
if ( ! function_exists('check_view_permission'))
{
    function check_view_permission($moduleid)
    {
        $CI =& get_instance();
        $userid = $CI->session->userdata('userid');
        $CI->db->select('permission');
        $CI->db->from('system_permission');
        $CI->db->where('userid',$userid);
        $query = $CI->db->get();
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $sp)
            {
                $perm = json_decode($sp->permission,TRUE);
                $newperm = $perm[$moduleid];
                return $newperm['v'];//It will return 0 or 1
            }
        }
        else
        {
           return 0;
        }
    }
}

if ( ! function_exists('check_other_permission'))
{
    function check_other_permission($moduleid,$type)
    {
        $CI =& get_instance();
        $userid = $CI->session->userdata('userid');
        $CI->db->select('permission');
        $CI->db->from('system_permission');
        $CI->db->where('userid',$userid);
        $query = $CI->db->get();
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $sp)
            {
                $perm = json_decode($sp->permission,TRUE);
                $newperm = $perm[$moduleid];
                return $newperm[$type];//It will return 0 or 1
            }
        }
        else
        {
           return 0;
        }
    }
}

if ( ! function_exists('hijiridate'))
{
    function hijiridate()
    {
        date_default_timezone_set('UTC');
        $time = time();
        require_once APPPATH."/third_party/Arabic.php";
        $Ar = new I18N_Arabic('Date');
        $fix = $Ar->dateCorrection ($time);

        return $Ar->date('l dS F Y h:i:s A',  $time, $fix);

        /*$CI =& get_instance();
        date_default_timezone_set('UTC');
        $time = time();
        $CI->load->library('arabic');
        $Ar = new I18N_Arabic('Date');
        $fix = $Ar->dateCorrection ($time);
        return $Ar->date('l dS F Y ',  $time, $fix);*/
    }
}

if ( ! function_exists('wilayat_count'))
{
    function wilayat_count()
    {
        $CI =& get_instance();
        $query = $CI->db->qery("SELECT
                COUNT(`main`.`tempid_value`) AS tv,
                election_wilayats.`WILAYATNAME`,
                election_wilayats.`WILAYATID`
            FROM
                `main`
                LEFT JOIN `election_wilayats`
                    ON `main`.`walaya` = `election_wilayats`.`WILAYATID`
                 WHERE election_wilayats.`WILAYATNAME`!=''
                    GROUP BY election_wilayats.`WILAYATID`
                ORDER BY tv DESC
                LIMIT 0,10;");

    }
}

if ( ! function_exists('noa_ul_istasfar'))
{
    function noa_ul_istasfar($inquerytype)
    {
        $CI =& get_instance();
        $inqtype = explode(',',$inquerytype);
        $html = '';
        foreach($inqtype as $data)
        {
            $dataexp = explode('_',$data);
            $qx = $CI->db->query("SELECT `list_name` FROM list_management WHERE `list_id`='".$dataexp[0]."' ORDER BY `list_name` ASC LIMIT 1;");
            foreach($qx->result_array() as $dx)
            {
                $html .= '<strong>'.$dx['list_name'].'</strong> ('.arabic_date($dataexp[1]).')<br>';
            }
        }
        echo $html;
    }
}



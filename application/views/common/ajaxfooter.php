<script>
$(function(){ 

$('#ajaxTable thead th').each( function (index, value) {				
				 var title = $.trim($(this).html());
				 var attr_id = $('#ajaxTable thead th').eq( $(this).index() ).attr('id');
				 if(title!='الإجراءات' && title!='' && title!='متوسط صافي الريح' && title!='متوسط الايرادات' && title!='الشهرية' && title!='السنوية')
				 {				 
				 	$(this).html(title+'<input type="search" class="form-control '+attr_id+' search_filter" placeholder="'+title+'" />' );
				 }
		});
		
var ajax_table = $('#ajaxTable').DataTable({
			 "ordering": false,
			
			 "oLanguage": {
				 "sSearch": "",
				 "oPaginate": { "sNext": "التالی", "sPrevious": "السابق" },
				 "sProcessing": "<img src='"+config.BASE_URL+"hourglass.gif'>"
				}
			});
		
ajax_table.columns().eq(0).each(function (colIdx) 
{
	$('input', ajax_table.column(colIdx).header()).on('keyup change', function() 
	{
		ajax_table.column(colIdx).search(this.value).draw();
	});
});	
		
$('.search_filter').keyup(function(){
	$('#ajaxTable td').removeHighlight().highlight($(this).val());
});	
 
	
});
</script>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 lt-ie10"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?PHP echo $module['module_name']; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!-- Fonts CSS: -->
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>css/d6220a84.bootstrap.css">
	<link rel="stylesheet" href="<?PHP echo base_url(); ?>css/custom.css">
    
    
    <!-- Page-specific Plugin CSS: -->
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>css/vendor/jquery.pnotify.default.css">
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>css/vendor/select2/select2.css">


    <!-- Proton CSS: -->
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>css/1b2c4b33.proton.css">
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>css/vendor/animate.css">

    <link rel="stylesheet" href="<?PHP echo base_url(); ?>css/9a41946e.font-awesome.css" type="text/css" />
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>css/4d9a7458.font-titillium.css" type="text/css" />
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>css/helvatice.css" type="text/css" />

    <!-- Data Table Plugins -->
    <link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>js/databale/media/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>js/databale/examples/resources/syntax/shCore.css">
    
    <style>
	@media print{
.print{
display:none;
}
.printlabel{
display:none;
}

}
	</style>

</head>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 lt-ie10"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->

    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <!---Software Engineer (Noor Khan) AND (Muhammad Muzaffar)---->
    <title>الهيئة العمانية للأعمال الخيرية</title>
    <meta name="description" content="Durar Smart Solutions L.L.C">
    <meta name="keywords" content="Durar Smart Solutions L.L.C">
    <meta name="author" content="Durar Smart Solutions L.L.C">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/favicon-16x16.png">
    <link rel="manifest" href="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?PHP echo base_url(); ?>assets/images/fbc48c09580f04db8813cfec65cea892.ico/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Fonts CSS: -->
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/d6220a84.bootstrap.css">
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/dist/css/bootstrap-select.css">
    
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/custom.css">

    <!-- Page-specific Plugin CSS: -->
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/vendor/jquery.pnotify.default.css">
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/vendor/select2/select2.css">
    <link href="<?PHP echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">

    <!-- Proton CSS: -->
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/1b2c4b33.proton.css">
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/animate.css">
    
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/js/jquery.qtip.custom/jquery.qtip.css">


    <!-- adds CSS media query support to IE8   -->
    <!--[if lt IE 9]>
    <script src="<?PHP echo base_url(); ?>assets/js/html5shiv.js"></script>
    <script src="<?PHP echo base_url(); ?>assets/js/vendor/respond.min.js"></script>
    <![endif]-->
    
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>/assets/css/dmuploader/uploader.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>/assets/css/dmuploader/demo.css" rel="stylesheet" />

    <!-- Fonts CSS: -->
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/9a41946e.font-awesome.css" type="text/css" />
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/4d9a7458.font-titillium.css" type="text/css" />
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/helvatice.css" type="text/css" />
    <!--<link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/component.css" type="text/css" />-->

    <!-- Common Scripts: -->
    <script src="<?PHP echo base_url(); ?>assets/js/jquery/external/jquery/jquery.js"></script>
    <script src="<?PHP echo base_url(); ?>assets/js/jquery/jquery-ui.js"></script>
    
        <!--Live JQUERY-->
	<script src="<?PHP echo base_url(); ?>assets/js/jquery.livequery.js"></script>
    
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/js/jquery/jquery-ui.css" type="text/css" />
    
    <script src="<?PHP echo base_url(); ?>assets/js/vendor/modernizr.js"></script>
    <script src="<?PHP echo base_url(); ?>assets/js/vendor/jquery.cookie.js"></script>

    <!-- Data Table Plugins -->
    <link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>assets/js/databale/media/css/jquery.dataTables.css">
    <script type="text/javascript" language="javascript" src="<?PHP echo base_url(); ?>assets/js/databale/media/js/jquery.dataTables.js"></script>
    <!--<script type="text/javascript" language="javascript" src="<?PHP echo base_url(); ?>assets/js/databale/media/js/daterangefilter.js"></script>-->
    
    <!------------------------------------------->
    <!-------Fancy Box Script-------------------->
    
    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="<?PHP echo base_url(); ?>assets/js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?PHP echo base_url(); ?>assets/js/fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>assets/js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>assets/js/fancybox/jquery.fancybox-buttons.css?v=1.0.5" />
    <script type="text/javascript" src="<?PHP echo base_url(); ?>assets/js/fancybox/jquery.fancybox-buttons.js?v=1.0.5"></script>

    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="<?PHP echo base_url(); ?>assets/js/fancybox/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="<?PHP echo base_url(); ?>assets/js/fancybox/jquery.fancybox-thumbs.js?v=1.0.7"></script>

    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="<?PHP echo base_url(); ?>assets/js/fancybox/jquery.fancybox-media.js?v=1.0.6"></script>
    <!------------------------------------------->
    <link href='<?PHP echo base_url(); ?>assets/js/calender/fullcalendar.css' rel='stylesheet' />
    <link href='<?PHP echo base_url(); ?>assets/js/calender/fullcalendar.print.css' rel='stylesheet' media='print' />
    <script src='<?PHP echo base_url(); ?>assets/js/calender/lib/moment.min.js'></script>
    <script src='<?PHP echo base_url(); ?>assets/js/calender/fullcalendar.min.js'></script>
    <script src='<?PHP echo base_url(); ?>assets/js/calender/lang/ar-sa.js'></script>
    

    
    <!--Own Script-->
    <script src="<?PHP echo base_url(); ?>assets/js/hd_script.js"></script>
    <script src="<?PHP echo base_url(); ?>assets/js/lm_scripts.js"></script>
    <script src="<?PHP echo base_url(); ?>assets/js/animo.js"></script>
    
    <!--Script Initiliazer-->
    <script>
	
        //Configuration
        var config	=
        {
            BASE_URL		: '<?php echo base_url();?>',
            AJAX_URL		: '<?php echo base_url();?>ajax/',
            CURRENT_URL		: '<?php echo current_url();?>',
			C_URL			: '<?php echo base_url();?><?php echo $this->uri->segment('1');?>/<?php echo $this->uri->segment('2');?>',
            MODULE_ID		: '<?php echo $module['moduleid'];?>',
			MODULE_PARENT	: '<?php echo $module['module_parent'];?>',           
            MODULE_NAME		: '<?php echo $module['module_name'];?>',
            CONTROLLER_NAME	: '<?php echo $this->uri->segment('1');?>',
            METHOD_NAME		: '<?php echo $this->uri->segment('2');?>',
			USER_ID			: '<?PHP echo $this->session->userdata('userid'); ?>'
        }
	    
    </script>
    <?PHP //the_boss(); ?>
    </head>
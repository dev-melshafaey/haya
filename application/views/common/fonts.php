<style>
@font-face {
	font-family: 'Titillium Web';
	src: url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-extralight-webfont.eot');
	src: url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-extralight-webfontd41d.eot?#iefix') format('embedded-opentype'),  url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-extralight-webfont.svg#titillium_webthin') format('svg'),  url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-extralight-webfont.woff') format('woff'),  url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-extralight-webfont.ttf') format('truetype');
	font-weight: 300;
	font-style: normal;
}
@font-face {
	font-family: 'Titillium Web';
	src: url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-regular-webfont.eot');
	src: url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-regular-webfontd41d.eot?#iefix') format('embedded-opentype'),  url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-regular-webfont.svg#titillium_webregular') format('svg'),  url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-regular-webfont.woff') format('woff'),  url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-regular-webfont.ttf') format('truetype');
	font-weight: 400;
	font-style: normal;
}
@font-face {
	font-family: 'Titillium Web';
	src: url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-bold-webfont.eot');
	src: url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-bold-webfontd41d.eot?#iefix') format('embedded-opentype'),  url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-bold-webfont.svg#titillium_webbold') format('svg'),  url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-bold-webfont.woff') format('woff'),  url('<?PHP echo base_url(); ?>assets/fonts/titillium/titilliumweb-bold-webfont.ttf') format('truetype');
	font-weight: 700;
	font-style: normal;
}

body {
	font-family:'Titillium Web' !important; }
</style>

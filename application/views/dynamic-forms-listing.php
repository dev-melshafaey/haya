<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
    <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
    <div class="col-md-12">
    <form action="" method="POST" id="user_form" name="user_form" enctype="multipart/form-data" >
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="list-group">
            <?php if(!empty($flist)):?>
            <?php
                    $cu 			=	$flist->custom_form;	
                    $custom_form 	=	json_decode($cu, true);		
            ?>
            <div class="tab-pane list-group active" id="tabsdemo-m-<?php echo $flist->moduleid;?>">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?PHP echo base_url()."lease_programe/customform/".$formid."/".$userid; ?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <?php foreach($custom_form['data'] as $cfkey => $cfvalue) { ?>
                      <th style="text-align:center;"><?php echo $cfvalue['fn']; ?>‎</th>
                      <?php }?>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $formValue		=	$this->haya_model->getCustomeFormValue($flist->moduleid,$userid);?>
                    <?php if($formValue):?>
                    <?php foreach($formValue as $fv) { ?>
                    <tr role="row" id="<?php echo $fv->mmdid;?>_durar_lm">
                    
                      <?php $json_decode	=	json_decode($fv->moduledata);?>
                      <?php $total_record	=	 count($custom_form['data']);?>

                      <?php $i	=	1;?>
                      <?php foreach($custom_form['data'] as $cfkey => $cfvalue) { ?>
                      <?php $key_value	=	$cfvalue['fn'];?>
                      <?php if($json_decode->$key_value){?>
                      <td style="text-align:center;"><?php if($cfvalue['ft']	==	'select'):?>
                        <?php echo $this->haya_model->get_name_from_list($json_decode->$key_value);?>
                        <?php elseif($cfvalue['ft']	==	'radio'):?>
                        <?php echo $this->haya_model->get_name_from_list($json_decode->$key_value);?>
                        <?php elseif($cfvalue['ft']	==	'checkbox'):?>
                        <?php $get_check_box_value	=	$this->haya_model->query_in($json_decode->$key_value);?>
                        <?php if(!empty($get_check_box_value)):?>
							<?php foreach($get_check_box_value	as	$bval):?>
                            	<li><?php echo $bval->list_name;?></li>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php else:?>
                        <?php echo $json_decode->$key_value;?>
                        <?php if($i	==	$total_record):?>
                        	<?php echo '<div style="float:left;">';?>
							<?php echo '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$fv->mmdid.'" data-url="'.base_url().'lease_programe/delete_dynamic_form_data/'.$fv->mmdid.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';?>
                            <?php echo '<a href="#globalDiag" onClick="alatadad(this);" data-url="'.base_url().'lease_programe/customform/'.$formid.'/'.$userid.'/'.$fv->mmdid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';?>
							<?php '</div>';?>
                        <?php endif;?>
                        <?php endif;?></td>
                        <?php }?>
                      <?php $i++; }?>
                    </tr>
                    <?php }?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <?php //}?>
            <?php endif;?>
          </div>
          <br clear="all" />
        </div>
      </div>
      </div>
      </div>
    </form>
  </div>
  </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
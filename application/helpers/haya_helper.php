<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------
	if ( ! function_exists('test_method'))
	{
		function test_method($var = '')
		{
			//$var	=	$ci->session->userdata('name');
			return $var;
		}   
	}
// ------------------------------------------------------------------------
	if ( ! function_exists('check_null_oco'))
	{
		function check_null_oco($var = '')
		{
			if(trim($var)!='' || trim($var)!=0)
			{
				return $var;
			}
			else
			{
				return '<span class="redhorn">لا يوجد معلومات</span>';
			}
		}   
	}	
// ------------------------------------------------------------------------

	if ( ! function_exists('arabic_date'))
	{
		function arabic_date($date)
		{
			header('Content-Type: text/html; charset=utf-8');
			$standard = array("0","1","2","3","4","5","6","7","8","9");
			$eastern_arabic_symbols = array("٠","١","٢","٣","٤","٥","٦","٧","٨","٩");
			$arabic_date = str_replace($standard , $eastern_arabic_symbols ,$date);	
			//return $arabic_date;
			return $date;
		}
	}
	
		if ( ! function_exists('a_date'))
	{
		function a_date($date)
		{
			header('Content-Type: text/html; charset=utf-8');
			$standard = array("0","1","2","3","4","5","6","7","8","9");
			$eastern_arabic_symbols = array("٠","١","٢","٣","٤","٥","٦","٧","٨","٩");
			$arabic_date = str_replace($standard , $eastern_arabic_symbols ,$date);	
			//return $arabic_date;
			return $arabic_date;
		}
	}	

// ------------------------------------------------------------------------

	if ( ! function_exists('save_button'))
	{
		function save_button($buttonid,$status)
		{
			if($status==0)
			{
				return '<button type="button" id="'.$buttonid.'" class="btn btn-success"><i class="icon-hdd"></i> حفظ</button>';
			}
		}
	}

// ------------------------------------------------------------------------

	if ( ! function_exists('the_boss'))
	{
		function the_boss()
		{
			if($_SERVER['REMOTE_ADDR']=='192.168.1.32' || $_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='localhost') 
			{ 
				echo('<pre>');
				print_r($_SERVER);
				echo('</pre>');
				exit();
			}
		}
	}
// ------------------------------------------------------------------------
	if ( ! function_exists('is_logged_in'))
	{
		function is_logged_in($logged)
		{	
		   if ($logged)
		   {
				redirect(base_url().'dashboard');
				exit();
		   }
		   else
		   {
				redirect(base_url().'admin/login');
				exit();
		   }
		}
	}
// ------------------------------------------------------------------------
	if ( ! function_exists('charity_edit_url'))
	{
		function charity_edit_url($id)
		{	
		   switch($id)
		   {
			   case 78;
			   	return base_url().'inquiries/housingform/';
			   break;
			   
			   case 79;
			   	return base_url().'inquiries/cashform/';
			   break;
			   
			   case 80;
			   	return base_url().'inquiries/medicalform/';
			   break;
			   
			   case 81;
			   	return base_url().'inquiries/educationform/';
			   break;			   			   			   
		   }
		}
	}
// ------------------------------------------------------------------------
	if ( ! function_exists('sms_lagend'))
	{
		function sms_lagend() 
		{
			return array('APPCODE','DATE','STATUS','wilayat');
		}
	}
// ------------------------------------------------------------------------
	if ( ! function_exists('kafala_duration'))
	{
		function kafala_duration($name, $value) 
		{
			//$arr = array('monthly'=>'الشهرية','yearly'=>'سنوية'); Khamees told me we have only two option  yearly and open
			
			$arr = array('yearly'=>'سنوية','monthly'=>'مفتوح');
			
			$dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req">';
			$dropdown .= '<option value="">مدة الكفالة</option>';				   
			foreach($arr as $row => $data)
			{
				$dropdown .= '<option value="'.$row.'" ';
				if($value==$row)
				{
					$dropdown .= 'selected="selected"';
				}
				 $dropdown .= '>'.$data.'</option>';
			}
			$dropdown .= '</select>';
			echo($dropdown);
		}
	}	
// ------------------------------------------------------------------------
	/**
	 * Calculate Age
	 * @access	public
	 * @return	number (age)
	 */
	if ( ! function_exists('calcualteAge'))
	{
		function calcualteAge($dob)
		{
			$from = new DateTime(date('Y-m-d',strtotime($dob)));
			$to   = new DateTime('today');
			return  $from->diff($to)->y;	
		}
	}
// ------------------------------------------------------------------------
	/**
	 * Send Sms
	 *
	 * @access	public
	 * @Param Message, $Recipients
	 * @return	NULL
	 */
	if ( ! function_exists('send_general_sms'))
	{
		function send_general_sms($message,$Recipients)
		{
			try 
			{
				$options	= array(
								'soap_version'	=>	SOAP_1_2,
								'exceptions'	=>	true,
								'trace'			=>	1,
								'cache_wsdl'	=>	WSDL_CACHE_NONE,
								"exception" 	=> 1);
				$client		= new SoapClient('http://ismartsms.net/iBulkSMS/webservice/IBulkSMS.asmx?WSDL');
				$config		= array(
								'UserID'		=> 	SMS_ID,
								'Password'		=>	SMS_PASSWORD,
								'Message'		=> 	$message,
								'Language'		=> 	64,
								'ScheddateTime'	=> 	date('Y-m-d'),
								'Recipients' 	=> 	$Recipients,
								'RecipientType' => 	1);
				
				$results = $client->PushMessage($config);
				print_r($results);	
			}
			catch (Exception $e) 
			{
				echo "<h2>Exception Error!</h2>";
				echo $e->getMessage();
			}
		}
	}
// ------------------------------------------------------------------------
	/**
	 * Creating SMS Log
	 *
	 * @access	public
	 * @Param Message, $Recipients
	 * @return	NULL
	 */
	
	if ( ! function_exists('sms_log'))
	{
		function sms_log($text)
		{		
			$smsFile = fopen(FCPATH."smslog.txt", "a") or die("Unable to open file!");	
			fwrite($smsFile, $text);
			fclose($smsFile);		 	
		}
	}

// ------------------------------------------------------------------------


//////////////////////////////////////////////////////////////////////
	if ( ! function_exists('show_date'))
	{
		function show_date($date,$case)
		{
			if($date=='')
			{	$date = date('Y-m-d');	}
			
			$months = array('1'=>"ينــاير",'2'=>"فبرايــر",'3'=>"مــارس",'4'=>"ابريــل",'5'=>"مــايو",'6'=>"يــونيه",'7'=>"يوليــو",'8'=>"أغسطـــس",'9'=>"سبتمبــر",'10'=>"أكتوبــر",'11'=>"نوفمبــر",'12'=>"ديسمبــر");
			$days	= array("الأحـد", "الإثنين", "الثــلاثاء", "الإربعــاء", "الخميــس", "الجمعــة", "السبــت");
			switch($case)
			{
				case '1';
					$day 	=	a_date(date('d',strtotime($date)));
					$month 	=	$months[date('n',strtotime($date))];
					$year 	= 	a_date(date('Y',strtotime($date)));
					echo $day.' '.$month.' '.$year;
				break;				
				case '2';
					$day	=	a_date(date('d',strtotime($date)));
					$month	=	$months[date('n',strtotime($date))];
					$year 	= 	a_date(date('Y',strtotime($date)));
					$dayx	=	date('w');
					echo $days[$dayx].' '.$day.' '.$month.' '.$year;
				break;
				case '3';
					$day	=	a_date(date('d',strtotime($date)));
					$month	=	$months[date('n',strtotime($date))];
					$year	=	a_date(date('Y',strtotime($date)));
					echo $day.' '.$month.' '.$year.' '.a_date(date('h:i:s',strtotime($date)));
				break;				
				case '4';
					return '('.calcualteAge($date).') '.date('Y-m-d',strtotime($date));
				break;				
				case '5';
					$day	=	a_date(date('d',strtotime($date)));
					$month	=	$months[date('n',strtotime($date))];
					$year	= 	a_date(date('Y',strtotime($date)));
					$dayx	=	date('w');
					return $days[$dayx].' '.$day.' '.$month.' '.$year;
					break;
				case '6';
					$day	=	a_date(date('d',strtotime($date)));
					$month	=	$months[date('n',strtotime($date))];
					$year	= 	a_date(date('Y',strtotime($date)));
					$dayx	=	date('w');
					$time	=	a_date(date('h:i:s',strtotime($date)));
					return $days[$dayx].' '.$day.' '.$month.' '.$year.',  '.$time;
					break;
				case '7';
					$day = date('w',strtotime($date));
					echo $days[$day];
				break;
				case '8';
					return $months[$date];
				break;
				case '9';
					return date('Y-m-d',strtotime($date));
				break;
			}
		}
	}

//////////////////////////////////////////////////////////////////////
	if ( ! function_exists('applicant_number'))
	{
		function applicant_number($number)
		{
			return sprintf("%07s", $number);
		}
	}

//////////////////////////////////////////////////////
	if ( ! function_exists('is_set'))
	{
		function is_set($value)
		{
			if($value!='')
			{
				return $value;
			}
			else
			{
				return '----';
			}
		}
	}

	if ( ! function_exists('check_all'))
	{
		function check_all($value)
		{
			
			return a_date(is_set($value));
		}
	}

	if ( ! function_exists('checkzero'))
	{
		function checkzero($value)
		{
			if($value!='')
			{
				return number_format($value,0);
			}
			else
			{
				return number_format('0',0);
			}
		}
	}
////////////////////////

	if ( ! function_exists('show_on_menu'))
	{
		function show_on_menu($name,$value)
		{
			$youm = array('1'=>'نعم','0'=>'لا');
			$dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req">';			   
					foreach($youm as $row => $data)
					{
						$dropdown .= '<option value="'.$row.'" ';
						if($value==$row)
						{
							$dropdown .= 'selected="selected"';
						}
						$dropdown .= '>'.$data.'</option>';
					}
					$dropdown .= '</select>';
					echo($dropdown);
			
		   
		}
	}

	if ( ! function_exists('status_dropbox'))
	{
		function status_dropbox($name,$value,$ar=0)
		{
			$youm = array('1'=>'نشط','0'=>'نشط غير');
			if($ar==0)
			{
			$dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req">';			   
					foreach($youm as $row => $data)
					{
						$dropdown .= '<option value="'.$row.'" ';
						if($value==$row)
						{
							$dropdown .= 'selected="selected"';
						}
						$dropdown .= '>'.$data.'</option>';
					}
					$dropdown .= '</select>';
					echo($dropdown);
			}
			else
			{
				return $youm[$value];
			}
		   
		}
	}


	if ( ! function_exists('number_drop_box'))
	{
		function number_drop_box($name,$value,$title='ترتيب',$start=1,$end=30, $placeholder='')
		{       
			$dropdown = '<select  name="'.$name.'" id="'.$name.'" placeholder="'.$placeholder.'" class="form-control">';			   
					$dropdown .= '<option value="">'.$title.'</option>';
					for($i=$start; $i<=$end; $i++)
					{
						$dropdown .= '<option value="'.$i.'" ';
						if($value==$i)
						{
							$dropdown .= 'selected="selected"';
						}
						$dropdown .= '>'.arabic_date($i).'</option>';
					}
					$dropdown .= '</select>';
					echo($dropdown);
			
		   
		}
	}
	
	
	if ( ! function_exists('year_drop_box'))
	{
		function year_drop_box($name,$value)
		{   
			$startYear = date('Y', strtotime($Date. ' -80 year'));
			$endYear = date('Y');    
			$dropdown = '<select  name="'.$name.'" id="'.$name.'" data-id="'.$value.'" onChange="populateAge(this);" placeholder="'.$placeholder.'" class="form-control">';			   
					$dropdown .= '<option value="">السنة</option>';
					for($i=$startYear; $i<=$endYear; $i++)
					{
						$dropdown .= '<option value="'.$i.'" ';
						if($value==$i)
						{
							$dropdown .= 'selected="selected"';
						}
						$dropdown .= '>'.arabic_date($i).'</option>';
					}
					$dropdown .= '</select>';
					return $dropdown;
			
		   
		}
	}
//////////////////////////////////////////////////////////////////////
	if ( ! function_exists('number_drop_box_arabic'))
	{
		function number_drop_box_arabic($name,$value,$type=0)
		{    
			$array = array('0'=>'صفر', 	'1'=>'واحد', 	'2'=>'اثنان', '3'=>'ثلاثة', '4'=>'أربعة', '5'=>'خمسة', '6'=>'ستة', '7'=>'سبعة', '8'=>'ثمانية', '9'=>'تسعة', '10'=>'عشرة');   
			if($type!=0)
			{
				return $array[$value];
			}
			else
			{
				$dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req">';			
				foreach($array as $akey => $avalue)
				{
					$dropdown .= '<option value="'.$akey.'" ';
					if($value==$akey)
					{
						$dropdown .= 'selected="selected"';
					}
					$dropdown .= '>'.$avalue.'</option>';
				}
				$dropdown .= '</select>';
				echo($dropdown);
			}	
		   
		}
	}
//////////////////////////////////////////////////////////////////////
	if ( ! function_exists('card_type_id'))
	{
		function card_type_id($name,$value,$type=0)
		{    
			$array = array('Resident ID'=>'المقيم بطاقة الهوية','National ID'=>'بطاقة الهوية الوطنية');   
			if($type!=0)
			{
				return $array[$value];
			}
			else
			{
				$dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req">';			
				foreach($array as $akey => $avalue)
				{
					$dropdown .= '<option value="'.$akey.'" ';
					
					if($value==$akey)
					{
						$dropdown .= 'selected="selected"';
					}
					
					$dropdown .= '>'.$avalue.'</option>';
				}
				
				$dropdown .= '</select>';
				echo($dropdown);
			}
		}
	}
	///////////////////////////////////////////////////////////////////////
	if ( ! function_exists('checkArraySize'))
	{
		function checkArraySize($array)
		{
			if(sizeof($array) > 0)
			{	
				$ex['data'] = $array;
				echo json_encode($ex);
			}
			else
			{
				$ex['data'] = array();
				echo json_encode($ex);
			}
		}
	}
	//////////////////////////////////////////////////////////////////////
	if ( ! function_exists('company_status'))
	{
		function company_status($name,$value,$type=0)
		{    
			$array = array('0'=>'مفعل','1'=>'غير مفعل','2'=>'مغلق','3'=>'القائمة السوداة');   
			if($type!=0)
			{
				return $array[$value];
			}
			else
			{
				$dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req">';
				$dropdown .= '<option value="">الحالة</option>';		
				foreach($array as $akey => $avalue)
				{
					$dropdown .= '<option value="'.$akey.'" ';
					if($value==$akey)
					{
						$dropdown .= 'selected="selected"';
					}
					$dropdown .= '>'.$avalue.'</option>';
				}
				$dropdown .= '</select>';
				echo($dropdown);
			}	
		   
		}
	}

//////////////////////////////////////////////////////////////////////
	if ( ! function_exists('hijiridate'))
	{
		function hijiridate()
		{
			date_default_timezone_set('UTC');
			$time = time();	
			
			require_once(APPPATH."third_party/Arabic.php");
			
			$Ar 		=	new I18N_Arabic('Date');
			$fix 		=	$Ar->dateCorrection($time);
			$HijriDate	=	arabic_date($Ar->date('l dS F Y',  $time, $fix));
			$Ar->setMode(3); 
			
			$ArabicDate = arabic_date($Ar->date('l dS F Y', $time));
			return '<li class="data_list_grid">'.$HijriDate.'</li><li class="data_list_grid">'.$ArabicDate.'</li>';
		}
	}
	
	
	if ( ! function_exists('add_button'))
	{
		function add_button($url,$heading,$pm)
		{				
		   if($pm	==	'1')
		   {
				return '<a class="btn btn-success" style="float:right;" href="'.base_url().$url.'">'.$heading.'</a>';
		   }
		}
	}
	
	if ( ! function_exists('badge_color'))
	{
		function badge_color($count)
		{				
		   if($count <=	0)
		   {
			   return 'gray';
		   }
		   else
		   {
			   return 'green';
		   }
		}
	}
	
	if ( ! function_exists('is_required'))
	{
		function is_required($is_required)
		{
			if($is_required	==	0)
			{
				return 'req';
			}
		}
	}
	
	if (!function_exists('getFilePath'))
	{
		function getFilePath($file)
		{
			return pathinfo($file, PATHINFO_EXTENSION);
		}
	}
	
	
	if (!function_exists('getFileResult'))
	{
		function getFileResult($file,$title,$url)
		{
			$doclist = array('doc','docx','xls','ppt');
			$pdflist = array('pdf');
			$imglist = array('jpg','png','gif','jpeg','tif','bmp');
			$fileext = getFilePath($file);
			
			if($fileext)
			{			
				if (in_array($fileext, $doclist)) 
				{			
					return '<i class="icon-search fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" href="'.base_url().'ajax/opendoc/'.$url.'" title="'.$title.'"></i>';
				}
				else if(in_array($fileext, $pdflist)) 
				{
					
					return '<i class="icon-search fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" href="'.base_url().$url.'" title="'.$title.'"></i>';
				}
				else
				{
					return '<i class="icon-search fancybox-button" rel="gallery1" style="color:#00A403; cursor:pointer;" href="'.base_url().$url.'" title="'.$title.'"></i>';	
				}
			}
		}
	}
	
	if (!function_exists('filePreview'))
	{
		function filePreview($file,$title)
		{
			$doclist = array('doc','docx','xls','ppt');
			$pdflist = array('pdf');
			$imglist = array('jpg','png','gif','jpeg','tif','bmp');
			
			$file_extension = getFilePath($file);
			
			if($file_extension)
			{			
				if (in_array($file_extension, $doclist)) 
				{			
					$mb = explode("alhayav2",$file);					
					parseWord(realpath(dirname()).$mb[1]);
				}
				else if(in_array($file_extension, $pdflist)) 
				{
					
					return '<i class="icon-search fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" href="'.$url.'" title="'.$title.'"></i>';
				}
				else
				{
					return '<img style="width:940px !important;" src="'.$file.'">';	
				}
			}
		}
	}
	
	if (!function_exists('parseWord'))
	{
	function parseWord($userDoc) 
	{
		$fileHandle = fopen($userDoc, "r");
		$line = @fread($fileHandle, filesize($userDoc));   
		$lines = explode(chr(0x0D),$line);
		$outtext = "";
		foreach($lines as $thisline)
		  {
			$pos = strpos($thisline, chr(0x00));
			if (($pos !== FALSE)||(strlen($thisline)==0))
			  {
			  } else {
				$outtext .= $thisline." ";
			  }
		  }
		 $outtext = preg_replace("/[^a-zA-Z0-9\s\,\.\-\n\r\t@\/\_\(\)]/","",$outtext);
		return $outtext;
	}
	} 
	
	if (!function_exists('downloadFile'))
	{
		function downloadFile($path,$doc_name)
		{
			$url	=	FCPATH.$path;
			$data	=	file_get_contents($url); // Read the file's contents
			$name	=	$doc_name;
			
			echo force_download($name, $data);
		}
	}
	
	if (!function_exists('getStatus'))
	{
		
		function getStatus($status)
		{
			switch($status)
			{
				case '1';
					echo '<i class="icon-ok mytooltip" data-title="نشط" style="color:#029625 !important;"></i>';
				break;
				
				case '0';
					echo '<i class="icon-off mytooltip" data-title="غير نشط" style="color:#FF0000 !important;"></i>';
				break;
			}
		}
	}
	
	//---------------------------------------------------------------------
	if (!function_exists('applicant_status'))
	{	
		function applicant_status($status=NULL, $pos=0)
		{
			if($pos==0)
			{
				return array('موافق','إعتذار','إحالة','إعادة نظر');
			}
			else if($pos==1)
			{
				$posArray = array('A'=>'موافق','R'=>'رفض','P'=>'إحالة / أستثناء');
				return $posArray[$status];
			}
			else if($pos==2)
			{
				$posArray = array('R'=>'لقد تم رفض طلبك فى تايخ','A'=>'لقد تم موافق طلبك فى تاريخ','P'=>'طلبك مازال قيد المراجعة');
				return $posArray[$status];
			}
			else if($pos==3)
			{
				$posArray = array('A'=>'موافق','R'=>'رفض','P'=>'إحالة / أستثناء');
				return $posArray;
			}
		}
	}
	//---------------------------------------------------------------------
	if (!function_exists('textChange'))
	{	
		function textChange($gender,$index,$form)
		{
			
			if($form==1)
			{
				$newArray = array(
					'ذكر'=>array('الفاضل / ','يحمل','ويعمل','ويتقاضي','راتبه','المذكور','انه','عمله','خدماتة','مستحقاته','تعينه','ومازال','له','طلبه'),
					'أنثى'=>array('الفاضلة / ','تحمل','وتعمل','وتتقاضى','راتبها','المذكوره','انها','عملها','خدماتها','مستحقاتها','تعينها','زالت','لها','طلبها')
							  );
				return $newArray[$gender][$index];
				//return 'gender = '.$gender.' index = '.$index.' form = '.$form;
			}
		}
	}
	
	if (!function_exists('input_types'))
	{	
		function input_types($name,$value,$key)
		{
			$textInput = array(
							   'text'=>'مربع نص',
							   'textarea'=>'مربع نص طويل',
							   'number'=>'رقم',
							   'checkbox'=>'مربع اختيار متعدد',
							   'radio'=>'مربع اختيار فردي',
							   'select'=>'قائمة خيارات',
							   'date'=>'تاريخ',
							   'tel'=>'رقم هاتف');
			$dropdown = '<select placeholder="نوع الحقل"  name="'.$name.'[]" id="'.$name.'" data-id="'.$key.'" onChange="myoptions(this);" class="form-control inputtype req">';
				$dropdown .= '<option value="">نوع الحقل</option>';		
				foreach($textInput as $akey => $avalue)
				{
					$dropdown .= '<option value="'.$akey.'" ';
					if($value==$akey)
					{
						$dropdown .= 'selected="selected"';
					}
					$dropdown .= '>'.$avalue.'</option>';
				}
				$dropdown .= '</select>';
				echo($dropdown);
		}
	}

//-------------------------------------------------------------------------------

	/*
	*
	* Sending Email Function
	* @param $to string
	* @param $from string
	* @param $subject string
	* @param $message string
	* @param $path string
	*
	*/
	if (!function_exists('send_email'))
	{	
		 function send_email($to	=	NULL,$from	=	NULL,$subject	=	NULL,$message = NULL,$path	=	NULL) 
		{
			$config = array();
			$config['protocol'] 	= 'sendmail';
			$config['mailpath'] 	= '/usr/sbin/sendmail';
			$config['charset'] 	 	= 'iso-8859-1';
			$config['smtp_host'] 	= "localhost";
			$config['smtp_port'] 	= "25";
			$config['mailtype'] 	= 'html';
			$config['charset']  	= 'utf-8';
			$config['newline']  	= "\r\n";
			$config['wordwrap'] 	= TRUE;
			
			$this->load->library('email');
			
			$this->email->initialize($config);
			$this->email->from($from, 'No Reply' );
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($message);
			
			if($path)
			{
				$this->email->attach($path);
			}
			
			$this->email->send();
		}
	}

//---------------------------------------------------------------------

	if (!function_exists('appointment_status'))
	{
		function appointment_status($case,$name,$value)
		{
			$appointpriority	=	array('1'=>'مهم',	'2'=>'غير مهم','3'=>'طبيعي');
			$appointstatus		=	array('approved'=>'اعتماد','pending'=>'معلق','rejected'=>'رفض','done'=>'تم');
			
			switch($case)
			{
				case '1';
					$dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req">';
					$dropdown .= '<option value="">أفضلية</option>';
						   
					foreach($appointpriority as $row => $data)
					{
						$dropdown .= '<option value="'.$row.'" ';
						if($value==$row)
						{
							$dropdown .= 'selected="selected"';
						}
						 $dropdown .= '>'.$data.'</option>';
					}
					$dropdown .= '</select>';
					echo($dropdown);
				break;
				
				case '2';
					$dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req">';
					$dropdown .= '<option value="">الحالات</option>';		   
					foreach($appointstatus as $row => $data)
					{
						$dropdown .= '<option value="'.$row.'" ';
						if($value==$row)
						{
							$dropdown .= 'selected="selected"';
						}
						$dropdown .= '>'.$data.'</option>';
					}
					$dropdown .= '</select>';
					echo($dropdown);
				break;
				
				case '3';
					return $appointpriority[$value];
				break;
				
				case '4';
					return $appointstatus[$value];
				break;
			}
			
			return $commitee_decision_type[$type];
		}

	if (!function_exists('get_key'))
	{
		
		function get_key()
		{
			return (rand(1000,9000)+date("Ymdhis")/2)*rand(5,5);
		}
	}
//////////////////////////////////////////////////////////////////////
if ( ! function_exists('budget_tree'))
{
	
	function budget_tree($tempid='2')
	{
		$CI =& get_instance();
		
		$tree		=	'';
		$others		=	'';
		
		/*$q = $CI->db->query("SELECT list_id,list_name  FROM list_management WHERE list_status='1' AND `list_parent_id`='0' AND list_type='inquiry_type' ORDER BY list_order ASC");*/
		
		$q = $CI->db->query("SELECT bd_id,bd_year FROM `ah_budget_main` WHERE delete_record='0';");
		
		foreach($q->result() as $parent)
		{
			if($parent->bd_id == '3'){
				
				$others .= '<div class="parent row" data-id="'.$parent->bd_id.'" id="'.$parent->bd_id.'">
				<div class="col-xs-8"><input id="inquirytypeid" data-handler="'.$tempid.'_'.$tempid.'" type="checkbox" name="inquiry_type[]" class="inquirytypeid tempinqury" value="'.$parent->bd_id.'" /> '.$parent->bd_year.' </div>
				<div class="col-xs-4" style="display:none !important;"><input data-handler="'.$tempid.'_'.$tempid.'" name="datepicker" value="'.date('Y-m-d h:i:s').'" type="text" class="form-control dpicker mydatepicker'.$parent->bd_id.' tempinqury" data-id="inqirydate" id="inqirydate'.$parent->bd_id.'"></div>
				</div>';
				$qchild = $CI->db->query("SELECT bd_id,bds_title FROM `ah_budget_sub` WHERE delete_record='0' AND bd_id='".$parent->bd_id."' AND bds_categoryId='0';");
				foreach($qchild->result() as $child)
				{
					$others .= '<div class="globe row child child_'.$parent->list_id.'" data-id="'.$child->bd_id.'" id="child_'.$parent->bd_id.'">
					<div class="col-xs-8"><input id="inquirytypeid" data-handler="'.$tempid.'_'.$tempid.'" type="checkbox" name="inquiry_type[]" class="childxxxxx  inquirytypeid tempinqury" value="'.$child->bd_id.'" /> '.$child->bds_title.' </div>
					<div class="col-xs-4" style="display:none !important;"><input data-handler="'.$tempid.'_'.$tempid.'" name="datepicker" value="'.date('Y-m-d h:i:s').'" type="text" class="form-control dpicker mydatepicker'.$child->bd_id.' tempinqury" data-id="inqirydate" id="inqirydate'.$child->bd_id.'"></div>
					</div>';

					$subchild = $CI->db->query("SELECT bd_id,bds_title FROM `ah_budget_sub` WHERE delete_record='0' AND bds_categoryId='".$child->bd_id."';");
					foreach($subchild->result() as $subchild)
					{
						$others .= '<div class="globe row smallchild smallchild_'.$child->bd_id.'" data-id="'.$subchild->bd_id.'" id="smallchild_'.$child->bd_id.'">
						<div class="col-xs-8"><input id="inquirytypeid" data-handler="'.$tempid.'_'.$tempid.'" type="checkbox" name="inquiry_type[]" class="inquirytypeid tempinqury" value="'.$subchild->bd_id.'" /> '.$subchild->bds_title.' </div>
						<div class="col-xs-4" style="display:none !important;"><input data-handler="'.$tempid.'_'.$tempid.'" name="datepicker" value="'.date('Y-m-d h:i:s').'"  type="text" class="form-control dpicker mydatepicker'.$subchild->list_id.' tempinqury" data-id="inqirydate" id="inqirydate'.$subchild->bd_id.'"></div>
						</div>';
					}
				}

			}
			else{
				$tree .= '<div class="parent row" data-id="'.$parent->bd_id.'" id="'.$parent->bd_id.'">
				<div class="col-xs-8"><input id="inquirytypeid" data-handler="'.$tempid.'_'.$tempid.'" type="checkbox" name="inquiry_type[]" class="inquirytypeid tempinqury" value="'.$parent->list_id.'" /> '.$parent->bd_year.' </div>
				<div class="col-xs-4" style="display:none !important;"><input data-handler="'.$tempid.'_'.$tempid.'" name="datepicker" value="'.date('Y-m-d h:i:s').'" type="text" class="form-control dpicker mydatepicker'.$parent->bd_id.' tempinqury" data-id="inqirydate" id="inqirydate'.$parent->bd_id.'"></div>
				</div>';
				$qchild = $CI->db->query("SELECT bd_id,bds_title FROM `ah_budget_sub` WHERE delete_record='0' AND bd_id='".$parent->bd_id."' AND bds_categoryId='0';");
				foreach($qchild->result() as $child)
				{
					$tree .= '<div class="globe row child child_'.$parent->bd_id.'" data-id="'.$child->bd_id.'" id="child_'.$parent->bd_id.'">
					<div class="col-xs-8"><input id="inquirytypeid" data-handler="'.$tempid.'_'.$tempid.'" type="checkbox" name="inquiry_type[]" class="childxxxxx  inquirytypeid tempinqury" value="'.$child->bd_id.'" /> '.$child->bds_title.' </div>
					<div class="col-xs-4" style="display:none !important;"><input data-handler="'.$tempid.'_'.$tempid.'" name="datepicker" value="'.date('Y-m-d h:i:s').'" type="text" class="form-control dpicker mydatepicker'.$child->bd_id.' tempinqury" data-id="inqirydate" id="inqirydate'.$child->bd_id.'"></div>
					</div>';
					$subchild = $CI->db->query("SELECT bd_id,bds_title FROM `ah_budget_sub` WHERE delete_record='0' AND bds_categoryId='".$child->bd_id."';");
										$subchild = $CI->db->query("SELECT bd_id,bds_title FROM `ah_budget_sub` WHERE delete_record='0' AND  bds_categoryId='".$child->bd_id."';");
					foreach($subchild->result() as $subchild)
					{
						$tree .= '<div class="globe row smallchild smallchild_'.$child->bd_id.'" data-id="'.$subchild->bd_id.'" id="smallchild_'.$child->bd_id.'">
						<div class="col-xs-8"><input id="inquirytypeid" data-handler="'.$tempid.'_'.$tempid.'" type="checkbox" name="inquiry_type[]" class="inquirytypeid tempinqury" value="'.$subchild->bd_id.'" /> '.$subchild->bds_title.' </div>
						<div class="col-xs-4" style="display:none !important;"><input data-handler="'.$tempid.'_'.$tempid.'" name="datepicker" value="'.date('Y-m-d h:i:s').'" type="text" class="form-control dpicker mydatepicker'.$subchild->bd_id.' tempinqury" data-id="inqirydate" id="inqirydate'.$subchild->bd_id.'"></div>
						</div>';
					}
				}
			}
		}
		///$tree.=$others;
		$tree .= '';
		echo $tree;
	}
}		
//---------------------------------------------------------------------
}
/* End of file url_helper.php */
/* Location: ./appliction/helpers/haya_helper.php */
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Miscellaneous Configuration
|--------------------------------------------------------------------------
|
| Define miscellaneous configuration here
|
*/

$config['project_name'] 	=	"Al-Hayya";
$config['upload'] 			=	"resources/";
$config['monthly_amount'] 	=	"50000";
$config['holiday_iztreya'] 	=	"10";
//$config['return_arabic_words'] 	= new NumberFormatter("ar", NumberFormatter::SPELLOUT);

/*
|--------------------------------------------------------------------------
| Database lIST TYPES
|--------------------------------------------------------------------------
|
| Define all LIST TYPES with there ENGLISH/ARABIC names
|
*/

$config['list_types']	=	array(
							'charity_type'		=>	array(
													'ar'	=>	'المساعدات',	
													'en'	=>	'Charity Type'),
							'donation_type'		=>	array(
													'ar'	=>	'الإغاثة',	
													'en'	=>	'Donation Type'),
							'marital_status'	=>	array(
													'ar'	=>	'الحالة الاجتماعية',	
													'en'	=>	'Marital Status'),
							'regions'			=>	array(
													'ar'	=>	'المحافظة',	
													'en'	=>	'Region'),
							'work_place'		=>	array(
													'ar'	=>	'مكان العمل',	
													'en'	=>	'Work Place'),
							'muhafiza'			=>	array(
													'ar'	=>	'المحافظة',	
													'en'	=>	'Region'),
							'wilaya'			=>	array(
													'ar'	=>	'الولاية',	
													'en'	=>	'Wilaya'),
							'user_relation'		=>	array(
													'ar'	=>	'قرابة',	
													'en'	=>	'User Relation'),
							'salary_type'		=>	array(
													'ar'	=>	'نوع الراتب',	
													'en'	=>	'Salary Type'),
							'document_type'		=>	array(
													'ar'	=>	'نوع الوثيقة',	
													'en'	=>	'Document Type'),
							'user_role'			=>	array(
													'ar'	=>	'دور المستخدم',	
													'en'	=>	'User Role'),
							'nationality'		=>	array(
													'ar'	=>	'الجنسية',	
													'en'	=>	'Nationality'),
							'designation'		=>	array(
													'ar'	=>	'تعيين',	
													'en'	=>	'Designation'),
							'school_type'		=>	array(
													'ar'	=>	'نوع المدرسة',	
													'en'	=>	'Type of school'),
							'foster_agencies'		=>	array(
													'ar'	=>	'الجهات الكافلة',	
													'en'	=>	'Foster Agencies'),
							'bank'				=>	array(
													'ar'	=>	'بنك',	
													'en'	=>	'Bank'),
							'bank_branch'		=>	array(
													'ar'	=>	'فرع بنك',	
													'en'	=>	'Bank Branch'),
							'communicationtype'	=>	array(
													'ar'	=>	'نوع الاتصال',	
													'en'	=>	'Communication Type'),
							'salarypaymenttype'	=>	array(
													'ar'	=>	'الراتب نوع الدفع',	
													'en'	=>	'Salary Payment Type'),
							'profession'		=>	array(
													'ar'	=>	'مهنة',	
													'en'	=>	'Profession'),
							'buildingtype'		=>	array(
													'ar'	=>	'نوع المبنى',	
													'en'	=>	'Building Type'),
							'category'			=>	array(
													'ar'	=>	'فئة',
													'en'	=>	'Category'),
							'place_name'			=>	array(
													'ar'	=>	'الجهة',
													'en'	=>	'Place Name'),
							'subcategory'		=>	array(
													'ar'	=>	'الفئة الفرعية',	
													'en'	=>	'Sub Category'),
							'issuecountry'		=>	array(
													'ar'	=>	'الدولة',
													'en'	=>	'Country Issue'),
							'gender'			=>	array(
													'ar'	=>	'جنس',	
													'en'	=>	'Gender'),
							'city'				=>	array(
													'ar'	=>	'المدينة',	
													'en'	=>	'city'),
							'casetype'			=>	array(
													'ar'	=>	'نوع الحالة',	
													'en'	=>	'Case Type'),
							'housing_condition'	=>	array(
													'ar'	=>	'الحالة السكنية',	
													'en'	=>	'House Condition'),
							'health_condition'	=>	array(
													'ar'	=>	'الحالة الصحية',	
													'en'	=>	'Health Condition'),
							'economic_condition'	=>	array(
													'ar'	=>	'الحالة الاقتصادية',	
													'en'	=>	'Economic Condition'),												
							'health_condition'	=>	array(
													'ar'	=>	'الحالة الصحية',	
													'en'	=>	'Health Condition'),
							'leave_reasons'		=>	array(
													'ar'	=>	'أسباب',	
													'en'	=>	'Leave Resons'),
							'company_type'		=>	array(
													'ar'	=>	'نوع التسجيل',	
													'en'	=>	'Registraion'),
							'payment_type'		=>	array(
													'ar'	=>	'طريقة الدفع',	
													'en'	=>	'Payment Type'),
							'sponser_type'		=>	array(
													'ar'	=>	'نوع الكفالة',	
													'en'	=>	'Sponser Type'),
							'town'				=>	array(
													'ar'	=>	'المنطقة',	
													'en'	=>	'Registraion'),
							'item'				=>	array(
													'ar'	=>	'العنصر',	
													'en'	=>	'Item')	,
							'departments'		=>	array(
													'ar'	=>	'الإدارات',	
													'en'	=>	'Departments'),
                            'supplier'			=>	array(
													'ar'	=>	'مورد',
													'en'	=>	'Supplier'),
							'request_decision'	=>	array(
													'ar'	=>	'اسباب عدم الحضور',	
													'en'	=>	'Request Decision'),
							'outside_projects_types'	=>	array(
													'ar'	=>	'المشاريع الخارجية',	
													'en'	=>	'Out Side Projects'),
							'item_number'	=>	array(
													'ar'	=>	'رقم المادة',	
													'en'	=>	'Item Numbers'),
							'maintenance_type'	=>	array(
													'ar'	=>	'نوع الصيانة',
													'en'	=>	'Maintainance Type'),
							'aps_category'	=>	array(
													'ar'	=>	'الفئة',
													'en'	=>	'Aps Category'),
							'section_types'	=>	array(
													'ar'	=>	'أنواع القسم',
													'en'	=>	'Section Types'),
							'yateem_boxes'	=>	array(
													'ar'	=>	'القسم الأيتام',
													'en'	=>	'Yateem Landing Page'),
							'beneficiary'	=>	array(
													'ar'	=>	'المستفيد ',
													'en'	=>	'Beneficiary'),
							'graduation_year'	=>	array(
													'ar'	=>	'سنة التخرج',
													'en'	=>	'Graduation Year'),
							'terms_list'		=>	array(
													'ar'	=>	'قائمة البنود',
													'en'	=>	'Terms of contract'),
							'type_of_material'		=>	array(
													'ar'	=>	'نوع المواد',
													'en'	=>	'Material Type'),
							'finish_kafeel_reason'	=>	array(
													'ar'	=>	'أسباب الإنتهاء (اليتيم)',
													'en'	=>	'Reason for Finish')																																				
							);
													
/*
|--------------------------------------------------------------------------
| Define leave types ARRAY
|--------------------------------------------------------------------------
|
| Define all Leave types
|
*/							
$config['absent_types']	=	array(
							'sick'				=>	'مريض',
							'trip'				=>	'رحلة',
							'otherreason'		=>	'السبب الآخر',
							'annual_holiday'	=>	'',
							'public_holiday'	=>	'',
							'weekly_holiday'	=>	''
							);
/*
|--------------------------------------------------------------------------
| Define Steps ARRAY
|--------------------------------------------------------------------------
|
| Define all STEPS name
|
*/
$config['steps']	=	array(
							'1'	=>	'الباحث الإجتماعي',
							'2'	=>	'اعتمادات الباحث الإجتماعي',
							'3'	=>	'الأقسام',
							'4'	=>	'اللجنة',
							'5'	=>	'Decession',
							'6'	=>	'الصرف',
							'7'	=>	'تم الصرف'
							);
							
/*
|--------------------------------------------------------------------------
| Define Steps ARRAY For Orphan
|--------------------------------------------------------------------------
|
| Define all STEPS name
|
*/
$config['orphan_step']	=	array(
							'1'			=>	'الباحث الإجتماعي',
							'2'			=>	'الايتام',
							'3'			=>	'كفالة الايتام',
							'cancel'	=>	'تحديث البيانات --> الايتامألغى',
							'reject'	=>	'الايتام --> الاعتذارات',
							'approve'	=>	'كفالة الايتام --> قائمة الاستبدال',
							'defult'	=>	'الايتام --> جميع المعاملات'
							);
$config['orphan_section']	=	array(
							'1'	=>	'الباحث الإجتماعي',
							'2'	=>	'الايتام',
							'3'	=>	'كفالة الايتام'
							);						
/*
|--------------------------------------------------------------------------
| Define Steps ARRAY For Sposor
|--------------------------------------------------------------------------
|
| Define all STEPS name
|
*/
$config['sponsor_step']	=	array(
							'0'			=>	'الكفلاء',
							'1'			=>	'كفالة الايتام',
							'cancel'	=>	'تحديث البيانات --> إلغاء الراعي',
							'reject'	=>	'الكفلاء --> الاعتذارات',
							'approve'	=>	'كفالة الايتام'
							);

/*
|--------------------------------------------------------------------------
| Database Tables
|--------------------------------------------------------------------------
|
| Define all database tables name here
|
*/

//AL-Hayya database table names
$config['table_users']					=	'ah_users';
$config['table_users_documents'] 		=	'ah_users_documents';
$config['table_listmanagement'] 		=	'ah_listmanagement';
$config['table_modules'] 				=	'ah_modules';
$config['table_userprofile'] 			=	'ah_userprofile';
$config['table_users_bankaccount'] 		=	'ah_users_bankaccount';
$config['table_users_communications'] 	=	'ah_users_communications';
$config['table_users_family'] 			=	'ah_users_family';
$config['table_users_salary'] 			=	'ah_users_salary';
$config['table_users_salary_detail'] 	=	'ah_users_salary_detail';
$config['table_branchs'] 				=	'ah_branchs';
$config['table_ahul'] 					=	'ah_user_activity_log';
$config['table_experiences'] 			=	'ah_experiences';
$config['table_attendence'] 			=	'ah_attendence';
$config['table_leave_request'] 			=	'ah_leave_request';
$config['table_sms_management'] 		=	'sms_management';
$config['table_users_basic_salary'] 	=	'ah_users_basic_salary';
$config['table_schedule'] 				=	'ah_schedule';
$config['table_interview_response'] 	=	'interview_response';

/*
|--------------------------------------------------------------------------
End of file project.php */
/* Location: ./application/config/project.php */
<?php $permissions = $this->haya_model->check_other_permission(array($module['moduleid'])); ?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default panel-block panel-title-block">
        <div class="panel-heading">
          <h4> </h4>
        </div>
      </div>
      <?php $msg = $this->session->flashdata('msg'); ?>
      <?php if ($msg): ?>
      <div class="col-md-12">
        <div style="padding: 22px 20px !important; background:#c1dfc9;">
          <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg; ?></h4>
        </div>
      </div>
      <?php endif; ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"> <a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url(); ?>budget/add_bank/" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th>الاسم</th>
                        <th>الفرع</th>
                        <th>رقم الحساب</th>
                        <th>الرصيد الإفتتاحي</th>
                        <th>القيمة المتبقية</th>
                        <th>الفاكس</th>
                        <th>الإيميل</th>
                        <th>السويفت كود</th>
                        <th>التاريخ</th>
                        <th>الإجراءات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer', array('ajax_url' => base_url() . 'budget/get_all_banks/' , 'columns_array' => '
{ "data": "bank_name" },
{ "data": "branch" },{ "data": "account_no" },
{ "data": "ammount" },{ "data": "القيمة المتبقية" },
{ "data": "fax" },{ "data": "email" },
{ "data": "swift_code" },{ "data": "created_date" },
{ "data": "الإجراءات" }')); ?>
</div>
</body>
</html></script>
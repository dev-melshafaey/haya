<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller
{

//-------------------------------------------------------------------------------	

	/*
	* Properties
	*/

	private $_data 		=	array();
	private $_user_info	=	array();

//-------------------------------------------------------------------------------

	/*
	* Costructor
	*/

	public function __construct()
	{

		parent::__construct();	
		$this->_data['user_info']	=	$this->haya_model->get_user_detail();
		$this->load->model('ajax_model', 'ajax');
		$this->load->model('users/users_model','users');
	}
	
//-------------------------------------------------------------------------------	
	public function getlistforrow_afrad()
	{
		$index = $this->input->post('myindex');
		$html = '<tr id="afrad_'.$index.'">';
		$html .='<td><input name="relation_fullname[]" value="" placeholder="الاسم" id="relation_fullname" type="text" class="form-control"></td>';
		$html .='<td><input name="age[]" type="text" class="form-control NumberInput" id="age" placeholder="السن" value="" maxlength="2"></td>';
		$html .='<td>'.$this->haya_model->create_dropbox_list('relationtype','user_relation',0,0,'req',0,$index).'</td>';
		$html .='<td>'.$this->haya_model->create_dropbox_list('professionid','profession',$item->professionid,0,'req',0,$index).'</td>';
		$html .='<td><input name="monthly_income[]" value="" placeholder="الدخل الشهري" id="monthly_income" type="text" class="form-control NumberInput"></td>';
		$html .='<td class="center" style="vertical-align: middle;"><i style="color:#CC0000; cursor:pointer;" onClick="delete_afrad(this);" data-index="afrad_'.$index.'" class="icon-remove-sign"></i></td>';
		$html .='</tr>';
		echo $html;
	}
	
//-------------------------------------------------------------------------------	
	public function add_males_field()
	{
		$index = $this->input->post('myindex');
		$html = '<tr id="males_'.$index.'">';
		$html .='<td><input name="males[]" value="" placeholder="" id="males" type="text" class="form-control"></td>';
		$html .='<td class="center" style="vertical-align: middle;"><i style="color:#CC0000; cursor:pointer;" onClick="delete_males(this);" data-index="males_'.$index.'" class="icon-remove-sign"></i></td>';
		$html .='</tr>';
		echo $html;
	}
//-------------------------------------------------------------------------------	
	public function add_females_field()
	{
		$index = $this->input->post('myindex');
		$html = '<tr id="females_'.$index.'">';
		$html .='<td><input name="females[]" value="" placeholder="" id="females" type="text" class="form-control"></td>';
		$html .='<td class="center" style="vertical-align: middle;"><i style="color:#CC0000; cursor:pointer;" onClick="delete_females(this);" data-index="females_'.$index.'" class="icon-remove-sign"></i></td>';
		$html .='</tr>';
		echo $html;
	}
//-------------------------------------------------------------------------------	
	public function add_experience_fields()
	{
		$index = $this->input->post('myindex');
		$html = '<tr id="experience_'.$index.'">';
        $html .='<td><input name="office_name[]" value="" placeholder="اسم المكتب" id="office_name" type="text" class="form-control req"></td>';
        $html .='<td><input name="start_data[]" value="" placeholder="تاريخ البدء" id="start_'.$index.'" type="text" class="datepicker form-control req"></td>';
        $html .='<td><input name="end_date[]" value="" placeholder="تاريخ الانتهاء" id="end_'.$index.'" type="text" class="datepicker form-control req"></td>';
        $html .='<td class="center" style="vertical-align: middle;"><i style="color:#CC0000; cursor:pointer;" onClick="delete_females(this);" data-index="experience_'.$index.'" class="icon-remove-sign"></i></td>';
		$html .='</tr>';
		echo $html;
	}
	
//-------------------------------------------------------------------------------
	public function appointment_list()
    {
        $q = $this->db->query("
			SELECT 
				`ah_userprofile`.`fullname`
				, `appointment`.`appoid`
				, `appointment`.`name`
				, `appointment`.`phonenumber`
				, `appointment`.`appointmentdate`
				, `appointment`.`appointpriority`
				, `appointment`.`appointstatus`
				, `appointment`.`subject`
				, `appointment`.`province` AS REIGONNAME
				, `appointment`.`wilaya` AS WILAYATNAME
			FROM
				`appointment` 
				INNER JOIN `ah_userprofile` ON (`appointment`.`addedby` = `ah_userprofile`.`userid`)
				INNER JOIN `ah_listmanagement` ON (`appointment`.`province` = `ah_listmanagement`.`list_id`)
			WHERE (`appointment`.`appointmentuser`='".$this->session->userdata('userid')."' OR `appointment`.`addedby`='".$this->session->userdata('userid')."') ORDER BY `appointment`.`appointmentdate` ASC;");
		
        foreach($q->result() as $inq)
        {    
				if($inq->appointpriority==1)
				{	$color = "class1";	}
				else if($inq->appointpriority==2)
				{	$color = "class2";	}
				else
				{	$color = "class3";	}		
				
				
			$arr[] = array(
				"DT_RowId"			=>	$inq->appoid.'_durar_lm',				
                "الحالات"			=>	'<i class="icon-flag '.$inq->appointstatus.'"></i>',
				"اسم"				=>	$inq->name,
				"تاريخ موعد"		=>	date('Y-m-d',strtotime($inq->appointmentdate)),
                "موضوع"				=>	$inq->subject,
                "رقم الهاتف"		=>	$inq->phonenumber,
                "المحافظة / الولاية"	=>	$inq->REIGONNAME.' / '.$inq->WILAYATNAME,
                "اسم فيستر"			=>	$inq->fullname,
                "الإجراءات"			=>	'<i class="icon-circle '.$color.'"></i> <a onClick="viewappointment('.$inq->appoid.');" href="#1" data-toggle="modal"><i class="icon-edit"></i></a>');
				
				unset($type_name,$temp_name,$color);
				unset($idcardnumber);
       }
	   
        $ex['data'] = $arr;
        echo json_encode($ex);
    }
	
	public function appointment_phone_number($phonenumber)
    {
       
		$q = $this->db->query("SELECT 
			`ah_userprofile`.`fullname`
			, `appointment`.`appoid`						
			, `appointment`.`appointmentdate`
			, `appointment`.`appointpriority`
			, `appointment`.`appointstatus`
			, `appointment`.`subject`
			, `appointment`.`appodetail`
			, `appointment`.`appointmentnotes`							
		FROM
			`appointment` 
			INNER JOIN `ah_userprofile` ON (`appointment`.`addedby` = `ah_userprofile`.`userid`)						
							WHERE `appointment`.phonenumber='".$phonenumber."' ORDER BY `appointment`.`appointmentdate` ASC;");
		
        foreach($q->result() as $inq)
        {    
				if($inq->appointpriority==1)
				{	$color = "class1";	}
				else if($inq->appointpriority==2)
				{	$color = "class2";	}
				else
				{	$color = "class3";	}		
				
				
			$arr[] = array(
				"DT_RowId"		=>	$inq->appoid.'_durar_lm',				
                "status"		=>	'<i class="icon-flag '.$inq->appointstatus.'"></i>&nbsp;<i class="icon-circle '.$color.'"></i>',				
				"appointmentdate"		=>	date('Y-m-d',strtotime($inq->appointmentdate)),              
                "appodetail"		=>	$inq->appodetail,
                "appointmentnotes"			=>	$inq->appointmentnotes
               );
				
				unset($type_name,$temp_name,$color);
				unset($idcardnumber);
       }
	   return $arr;
    }	
//-------------------------------------------------------------------------------
	
	public function opendoc($file)
	{		
		$file = base_url().'upload_files/documents/'.$file;		
		header('Content-disposition: inline');
		header('Content-type: application/msword'); // not sure if this is the correct MIME type
		readfile($file);
		//echo '<iframe src="http://docs.google.com/viewer?url='.urlencode($file).'&embedded=true" width="600" height="780" style="border: none;"></iframe>';
		//$filename = $file; /* Note: Always use .pdf at the end. */		
		//header("Content-type: application/pdf");
		//header("Content-Disposition: inline; filename=".$filename);
		//@readfile($file);
		
		
		
/*		header('Content-type: application/pdf');
		header('Content-Disposition: inline; filename="' . $filename . '"');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . filesize($file));
		header('Accept-Ranges: bytes');
		
		@readfile($file);*/
		
		/*echo APPPATH.'libraries/fpdi/fpdf.php';
		echo $filepath;
		//require_once(APPPATH.'libraries/fpdi/fpdf.php');
		//require_once(APPPATH.'libraries/fpdi/fpdi.php');	
		$pdf		= new FPDI();	
		$pageCount	= $pdf->setSourceFile($filepath);
		$tplIdx		= $pdf->importPage(1, '/MediaBox');		
		$pdf->addPage();
		$pdf->useTemplate($tplIdx, 10, 10, 90);		
		$pdf->Output();*/
	}
	
	
		function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
		
			
		if (!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
	
	function delete_bro_sis_id(){
			$postData = $this->input->post();
		//	echo "<pre>";
			//print_r($postData);
			$sis_id = $postData['sis_id'];
			$yateem_info['delete_record'] = 1;
			
			$json_data =	json_encode(array('data'=>$yateem_info));
			$this->db->where('ah_y_br_sis_id',$sis_id);
			$this->db->update('ah_yateem_brothers_sisters',$json_data,$this->session->userdata('userid'),$yateem_info);

	}
	function add_yateem_data(){
		$postData = $this->input->post();
					echo "<pre>";
			print_r($postData);
			exit;		
	
	if(!empty($postData)){

				$userid = $this->session->userdata('userid');
				 $branch_id =   $this->haya_model->get_branch_id($userid);
				 
				$yateem_info['orphan_name'] = $postData['orphans_name'];
			
				$yateem_info['orphan_nationalty'] = $postData['nationality'];
		
				$yateem_info['date_birth'] = date('Y-m-d',strtotime($postData['date_birth']));
		
				$yateem_info['country_id'] = $postData['country_id'];
				
				$yateem_info['city_id'] = $postData['city_id'];
				
				$yateem_info['po_adress'] = $postData['p_o_box'];
				$yateem_info['pc_adress'] = $postData['p_c_address'];
				
				$yateem_info['orphan_arrangement_brothers'] = $postData['arrangment'];
						
				$yateem_info['total_family_memebers'] = $postData['family_members'];
				
				$yateem_info['phone_number'] = $postData['phone_number'];
		
				$yateem_info['home_number'] = $postData['home_number'];
			
				$yateem_info['userid'] = $this->session->userdata('userid');
					$yateem_info['user_branch_id'] = $branch_id;
				
		
				//echo "<pre>";
				//print_r($yateem_info);
				
			//	$this->db->where('orphan_id',2);
			//	$this->db->update('ah_yateem_info',$yateem_info);
			//	exit;
				
				$yateem_id  = $this->check_id('orphans_id','ah_yateem_info','orphan_id',$postData,$yateem_info,'');
				
				$userid = $this->session->userdata('userid');
				$branch_id =   $this->haya_model->get_branch_id($userid);
				
				
				$alldocs = $this->haya_model->allRequiredDocument(205);
				if(!empty($alldocs)){
					foreach($alldocs as $doc){
						$docname = $doc->documentid;
						
						if($_FILES["".$docname.""]["tmp_name"]){
							$docData[''.$document_name.''] 	=	$this->upload_file($yateem_id,$docname,'resources/yateem');
							$docData['orphan_id'] = $yateem_id;
							$docData['userid'] = $this->session->userdata('userid');
							$docData['user_branch_id'] = $branch_id;
							$yateem_id  = $this->check_id('orphans_id','ah_yateem_info','orphan_id',$postData,$yateem_info,'');
							$this->check_docfile($docname,$yateem_id,$table,$docData);
							
					}
							
				
				}
				
				
				exit;
				if($_FILES["orphan_picture"]["tmp_name"]){
				$profileData['orphan_picture'] 	=	$this->upload_file($yateem_id,'orphan_picture','resources/yateem');
				$json_data =	json_encode(array('data'	=>$profileData));
				$this->db->where('orphan_id',$yateem_id);
				$this->db->update('ah_yateem_info',$json_data,$this->session->userdata('userid'),$profileData);
				//exit;			
			}
	
				//
				$brothers = array();
				if(!empty($postData['males'])){
				
				$f_ids = $postData['male_id'];
				
				//$m_names = $postData['males'];
				
				//count($f_names);
				
				
						foreach($postData['males'] as $i_m=>$male){
							
							$brother['br_sis_name'] = $male;
							$brother['yateem_id'] = $yateem_id;
							$brother['br_sis_type'] = 'brother';
							$brother['userid'] = $this->session->userdata('userid');
							$brother['user_branch_id'] =$branch_id;					
							
							if(isset($postData['male_id'][$i_m])){
									
									
							$json_data	=	json_encode(array('data'	=>$brother));
							
							$this->db->where('ah_y_br_sis_id',$postData['male_id'][$i_m]);
					
							$this->db->update('ah_yateem_brothers_sisters',$json_data,$this->session->userdata('userid'),$brother);
							
							}
							else{
									$brothers[] = $brother; 			
						
							}
						}
					if(!empty($brothers)){	
							$this->db->insert_batch('ah_yateem_brothers_sisters',$brothers); 
						}
				}
				$sisters = array();
				//echo "<pre>";
				//print_r($postData['female_id']);
				//exit;
				if(!empty($postData['females'])){
						foreach($postData['females'] as $f_i=>$female){
							$sister['br_sis_name'] = $female;
							$sister['yateem_id'] = $yateem_id;
							$sister['br_sis_type'] = 'sister';
							$sister['userid'] = $this->session->userdata('userid');
							$sister['user_branch_id'] = $branch_id;					
								if(isset($postData['female_id'][$i_m])){
									$json_data	=	json_encode(array('data'	=>$sister));								
									$this->db->where('ah_y_br_sis_id',$postData['female_id'][$f_i]);					
									$this->db->update('ah_yateem_brothers_sisters',$json_data,$this->session->userdata('userid'),$sister);
							}
							else{
									$sisters[] = $sister; 
							}
											
						}
					if(!empty($sisters)){	
					$this->db->insert_batch('ah_yateem_brothers_sisters',$sisters);
					}
				}
			
							if(!empty($postData['parent_name'])){
				$total = count($postData['parent_name']);
				for($a=0;$a<=$total;$a++){
					
					if(isset($postData['parent_name'][$a])){
							$parent_info['parent_name']= $postData['parent_name'][$a];
					}
					
					if(isset($postData['parent_relationship'][$a])){
							$parent_info['parent_relationship']= $postData['parent_relationship'][$a];
					}
					
					if(isset($postData['town'][$a])){
							$parent_info['town']= $postData['town'][$a];
					}
					
					if(isset($postData['phone_number'][$a])){
							$parent_info['phone_number']= $postData['phone_number'][$a];
					}
					
					if(isset($postData['parent_country_code'][$a])){
							$parent_info['parent_country_code']= $postData['parent_country_code'][$a];
					}
					
					if(isset($postData['parent_region_id'][$a])){
							$parent_info['parent_region_id']= $postData['parent_region_id'][$a];
					}
					
					if(isset($postData['parent_marital_status'][$a])){
							$parent_info['parent_marital_status']= $postData['parent_marital_status'][$a];
					}
					
					if(isset($postData['parent_address'][$a])){
							$parent_info['parent_address']= $postData['parent_address'][$a];
					}
					if(isset($postData['parent_country_code'][$a])){
							$parent_info['parent_country_code']= $postData['parent_country_code'][$a];
					}
					
					if(isset($postData['id_no'][$a])){
							$parent_info['id_no']= $postData['id_no'][$a];
					}
					
					$parent_info['userid'] = $this->session->userdata('userid');
					$parent_info['user_branch_id'] =$branch_id;
					$parent_info['ah_yateem_id'] = $yateem_id;
			
					$parent_id  = $this->check_id('parent_id','ah_yateem_parents','ah_yateem_parent_id',$postData,$parent_info,$a);
			
					//$this->db->insert('ah_yateem_parents',$parent_info);
					
					
					//town
				
				}
				
						$bank['name_bank'] = $postData['bankid'];
						$bank['branch_id'] = $postData['branchid'];
						$bank['account_number'] = $postData['account_number'];
						$bank['account_title'] = $postData['accountant_name'];
						$bank['user_branch_id'] = $branch_id;				
						$bank['userid'] = $this->session->userdata('userid');
						$bank['yateem_id'] = $yateem_id;
						
						//$this->db->insert('ah_yateem_banksinfo',$bank);
						$parent_id  = $this->check_id('ah_yateem_bank_id','ah_yateem_banksinfo','ah_yateem_bank_id',$postData,$bank,'');


						$otherinfo['date_father_death'] = date('Y-m-d',strtotime($postData['date_father_death']));
						$otherinfo['reason_father_death'] = $postData['reason_father_death'];
						$otherinfo['family_members'] = $postData['family_members'];
						$otherinfo['brothers'] = $postData['brothers'];
						$otherinfo['sisters'] = $postData['sisters'];
						$otherinfo['living_condition_family'] = $postData['living_condition_family'];
						$otherinfo['housing_condition'] = $postData['housing_condition'];
						$otherinfo['economic_condition'] = $postData['economic_condition'];
						$otherinfo['monthly_income'] = $postData['monthly_income'];
						$otherinfo['source_income'] = $postData['source_income'];
						$otherinfo['number_brothers'] = $postData['number_brothers'];
						$otherinfo['source_income'] = $postData['source_income'];
						$otherinfo['user_branch_id'] = $branch_id;				
						$otherinfo['userid'] = $this->session->userdata('userid');
						$otherinfo['yateem_id'] = $yateem_id;
						//$this->db->insert('ah_yateem_other_data',$otherinfo);
						$other_id  = $this->check_id('ah_yateem_other_id','ah_yateem_other_data','ah_yateem_other_id',$postData,$otherinfo,'');

						
						
						$eduinfo['school_name'] = $postData['school_name'];
						$eduinfo['branch_id'] = $postData['branchid'];
						$eduinfo['last_result'] = $postData['last_result'];
						$eduinfo['class'] = $postData['class'];
						$eduinfo['order'] = $postData['order'];
						$eduinfo['health_status'] = $postData['health_status'];
						$eduinfo['user_branch_id'] = $branch_id;			
						$eduinfo['userid'] = $this->session->userdata('userid');
						$eduinfo['yateem_id'] = $yateem_id;
						//$this->db->insert('ah_yateem_education',$eduinfo);
						
						$other_id  = $this->check_id('yateem_edu_id','ah_yateem_education','yateem_edu_id',$postData,$eduinfo,'');
						
						$candidateData['candidate_name'] = $postData['candidate_name'];
						$candidateData['candidate_city_id'] = $postData['candidate_city_id'];
						$candidateData['candidate_address'] = $postData['candidate_address'];
						$candidateData['candidate_phone_number'] = $postData['candidate_phone_number'];
						$candidateData['candidate_pobox'] = $postData['candidate_pobox'];
						$candidateData['candidate_pincode'] = $postData['candidate_pincode'];
						$candidateData['candidate_town'] = $postData['candidate_town'];
						$candidateData['candidate_approved'] = $postData['candidate_approved'];
						$candidateData['candidate_fax'] = $postData['candidate_fax'];
						$candidateData['candidate_pobox'] = $postData['candidate_pobox'];
						$candidateData['candidate_town'] = $postData['candidate_town'];
						$candidateData['candidate_pincode'] = $postData['candidate_pincode'];
						$candidateData['user_branch_id'] = $branch_id;				
						$candidateData['userid'] = $this->session->userdata('userid');
						$candidateData['yateem_id'] = $yateem_id;
						//$this->db->insert('ah_yateem_candidate',$candidateData);
						$other_id  = $this->check_id('yateem_cand_id','ah_yateem_candidate','yateem_cand_id',$postData,$candidateData,'');
						

				//echo $yateem_id;                                      	
				redirect(base_url().'yateem/yateem_list/');
			exit();	
			}
			
			
			//echo "<pre>";
			

			//$yateem_parents[]
		}
		else{
			echo false;	
		}
	}
//-------------------------------------------------------------------------------

	
	function check_docfile($doc_type_id,$parent_id,$tableData){
	
	
	//echo "<pre>";
		//echo $key;
	//	print_r($table);
	//	exit;
		$json_data	=	json_encode(array('data'=>$tableData));
		$whereValue = "orphan_id='$parent_id' AND doc_type_id='$doc_type_id' ";
		$this->db->select('*');
		$this->db->from('ah_yateem_documents');
		$this->db->where($whereValue);
		$query = $this->db->get();
		if($query->num_rows() > 0){

				$this->db->where($whereValue);
				return $this->db->update('ah_yateem_documents',$json_data,$this->session->userdata('userid'),$tableData);
				///return $postData[$id];
				
		}
		else{
			
			$this->db->insert('ah_yateem_documents',$tableData);
			return $yateem_id = $this->db->insert_id();
		
		}
		
		
	
	}
	function check_id($id,$table,$key,$postData,$tableData,$ind){
	
	
	//echo "<pre>";
		//echo $key;
	//	print_r($table);
	//	exit;
		if(isset($postData[$id]) && $postData[$id]!="" ){
			///	echo "if";

				$json_data	=	json_encode(array('data'=>$tableData));
				//ind
				if(is_array($postData[$id])){
				//	echo "if";
				$whereValue = $postData[$id][$ind];
				}
				else{
					//echo "else";
					$whereValue = $postData[$id];
				}
				$this->db->where($key,$whereValue);
				$this->db->update($table,$json_data,$this->session->userdata('userid'),$tableData);
				return $postData[$id];
			
		}
		else
		{
			$this->db->insert($table,$tableData);
			return $yateem_id = $this->db->insert_id();
			
		}
	
	}
//----------------------------------------------------------------
	/*
	*
	*
	*/
	public function addappointment()
	{
		if($this->input->post('name')!='')
		{ 
			$appoid = $this->input->post('appoid');
			
			if($this->input->post('appointmentdate')!='')
			{	
				$appointmentdate = $this->input->post('appointmentdate'); 
			}
			else
			{	
				$appointmentdate = date('Y-m-d h:i:s'); 
			}
			
			$sent = $this->input->post('sendmessage');
			$appointment = array(
				'addedby'			=>	$this->session->userdata('userid'),
				'branchid'			=>	$this->haya_model->get_branch_id($this->session->userdata('userid')),
				'subject'		    =>	$this->input->post('subject'),
				'appointmentnotes'	=>	$this->input->post('appointmentnotes'),
				'appointmentuser'	=>	$this->input->post('appointmentuser'),
				'province'			=>	$this->input->post('province'),
				'wilaya'			=>	$this->input->post('wilaya'),
				'name'				=>	$this->input->post('name'),
				'phonenumber'		=>	$this->input->post('phonenumber'),
				'email'				=>	$this->input->post('email'),
				'appodetail'		=>	$this->input->post('appodetail'),
				'appointmentdate'	=>	$this->input->post('appointmentdate'),
				'venue'				=>	$this->input->post('venue'),
				'appointpriority'	=>	$this->input->post('appointpriority'),
				'appointstatus'		=>	$this->input->post('appointstatus'));
				

				if($appoid!='')
				{
					$json_data	=	json_encode(array('data'	=>	$appointment));
					
					$this->db->where('appoid', $appoid);
					$this->db->update('appointment', $json_data,$this->session->userdata('userid'),$appointment);
					if($this->input->post('appointstatus')=='sms' && $this->input->post('venue')!='')
					{
						$message = arabic_date($this->input->post('venue'));
					}
					else
					{
						$message = ' عزيزي تم '.appointment_status(4,'',$this->input->post('appointstatus')).' موعدك في '.arabic_date(date('Y-m-d',strtotime($this->input->post('appointmentdate'))));
					}
					if($sent==1)
					{	
						send_general_sms($message,'968'.$this->input->post('phonenumber'));
					}
				}
				else
				{
					$json_data	=	json_encode(array('data'	=>	$appointment));
					
					$this->db->insert('appointment',$appointment,$json_data,$this->session->userdata('userid'));
				}				
		}
			$this->_data['app'] = $this->ajax->get_appointment();
			
		    $this->load->view("appappointment",$this->_data);	
	}
//-------------------------------------------------------------------------------	
	/*
	*
	* load bank data list for bank users
	*

	*/

	function bank_user_view()
	{
		$this->load->model('admin/admin_model');
		
		$id 			= $this->session->userdata('userid');
		$bankid 		= $this->admin_model->getBankAdmin($id);
		$bank_data 		= $this->admin_model->getWilayas($bankid);
			
		$wilaya 		= $bank_data->wilaya;
		
		$all_applicatns	= $this->admin_model->getBankApplicants($wilaya);
		
		if(!empty($all_applicatns))
		{
			foreach($all_applicatns as $applicant)
			{			
				//STEP NUMBER
				$applicant_name = $applicant->applicant_first_name." ".$applicant->applicant_middle_name." ".$applicant->applicant_last_name;
				$steps	  = get_lable('step-'.$applicant->form_step);			
				$actions .=' <a  onclick="open_dialog_approve(this);" href="#bankModel" data-type="1" dataappname="'.$applicant_name.'" data-id="'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'" data-toggle="modal"><i style="color:#4096EE;" class="icon-thumbs-up-alt"></i></a> ';
				$actions .=' <a onclick="open_dialog_approve(this);" href="#bankModel" data-type="2" dataappname="'.$applicant_name.'" data-id="'.$applicant->applicant_id.'" dataappname="'.$applicant_name.'" id="'.$applicant->applicant_id.'" data-toggle="modal"><i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></a> ';
				$actions .=' <a href="#1" onClick="showglobaldata(this);" data-url="'.base_url().'inquiries/requestdocument_data/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#0099FF;" class="icon-file-alt"></i></a> ';
				$actions .=' <a href="#1" onClick="showglobaldata(this);" data-url="'.base_url().'inquiries/get_applicant_data/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#0099FF;" class="icon-search"></i></a> ';
				$actions .=' <a href="'.base_url().'followup/bankfollowup/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#8EA928;" class="icon-usd"></i></a> ';
                
              	$name = $applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name;
				 $arr[] = array(
				 "DT_RowId"			=>	$applicant->applicant_id.'_durar_lm',
                "رقم"				=>	applicant_number($applicant->applicant_id),
                "الاسم"				=>	$name,
				"صيغة المشروع"		=>	$applicant->applicant_type,
                "النوع"				=>	$applicant->applicant_gender,
                "الرقم المدني"	=>	$applicant->appliant_id_number,               
                "المرحلة"			=>	$steps['ar'],
				"الإجراءات"			=>	$actions);
				
				unset($actions);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}

	}

//-------------------------------------------------------------------------------
	
	public function getevents()
	{
		$events = $this->db->query("
		SELECT 
		`admin_users`.`firstname`,
		`admin_users`.`lastname`,
		`election_reigons`.`REIGONNAME`,
		`election_wilayats`.`WILAYATNAME`,
		`appointment`.`name`, 
		`appointment`.`appoid`, 
		`appointment`.`appointmentdate` , 
		`appointment`.`appointstatus` , 
		`appointment`.`appointpriority`
		FROM 
		`admin_users` 
		INNER JOIN 
		`appointment` ON (`admin_users`.`id` = `appointment`.`addedby`)
		INNER JOIN 
		`election_reigons` ON (`election_reigons`.`ID` = `appointment`.`provianceid`)
		INNER JOIN 
		`election_wilayats` ON (`election_wilayats`.`WILAYATID` = `appointment`.`wilayaid`) 
		ORDER BY 
		`appointment`.`appointmentdate` ASC, 
		`appointment`.`appointpriority` ASC;
		");
			
		foreach($events->result() as $evalue)
		{
			if($evalue->appointpriority==1)
			{
				$color = "#e77755;";
			}
			else if($evalue->appointpriority==2)
			{
				$color = "#f4c84f;";
			}
			else
			{
				$color = "#aece4e;";
			}
			
			$arr[] = array(
				'title'	=>	$evalue->name,
				'start'	=>	date('Y-m-d',strtotime($evalue->appointmentdate)),
				'id'	=>	$evalue->appoid,
				'color'	=>	$color
			);
			
			unset($color);
		}
		
		echo json_encode($arr);
	}
	
//-------------------------------------------------------------------------------
	
	public function updateappointmet($id)
	{
		$app = $this->ajax->appointment($id);
		$this->_data['app'] = $app;
		
		$this->_data['appointments'] = $this->appointment_phone_number($app[0]->phonenumber);
		$this->_data['allusers'] = $this->ajax->allUsers();

		$this->load->view("appointment_update",$this->_data);	
	}

//-------------------------------------------------------------------------------
	
	public function qaima_mamla_listing_filter($branchid='')
    {
        $this->load->model('inquiries/inquiries_model', 'inq_ajax');
		
        foreach($this->inq_ajax->get_all_applicatnts($branchid) as $applicatnt)
        {
			$professional	=	$this->inq_ajax->getRequestInfo($applicatnt->applicant_id);			
			$phone 			=	$this->inq_ajax->applicant_phone_number($applicatnt->applicant_id);			
			$steps 			=	get_lable('step-'.$applicatnt->form_step);
			$type_name 		=	$steps['ar'];
			$decision_type	=	commitee_decision_type($applicatnt->commitee_decision_type);			
			$app_name		=	$applicatnt->applicant_first_name. ' ' . $applicatnt->applicant_middle_name .' ' . $applicatnt->applicant_last_name . ' ' . $applicatnt->applicant_sur_name;
			$name 			=	rander_html($professional['applicant_partners'],'name',$applicatnt->applicant_first_name . ' ' . $applicatnt->applicant_middle_name . ' ' . $applicatnt->applicant_last_name . ' ' . $applicatnt->applicant_sur_name);
			$gender 		=	rander_html($professional['applicant_partners'],'gender',$applicatnt->applicant_gender);
			$idcardnumber	=	rander_html($all_data['main']->applicant,'idcard');
			$phonenumber 	=	rander_html($phone,'phone');
			$al_marhala = $type_name;
			if($applicatnt->commitee_decision_type!='')
			{	$al_marhala .=	'<i style="color:#9C0; margin:0px 2px;" class="icon-double-angle-left"></i> '.trim($decision_type); }
            
			$action = '<a class="iconspace" data-toggle="modal" dataid="'.$applicatnt->applicant_id.'" datatype="'.$type_name.'" datatempvalue="'.$tempid_value.'" dataname="'.$app_name.'" onclick="open_dialog_tasjeel(this)" href="#quickLaunchModal"><i class="icon-eye-open"></i></a>';
			
			if(check_other_permission(17,'u')==1) 
			{	$action .= '<a class="iconspace" href="'.base_url().'inquiries/newrequest/'.$applicatnt->applicant_id.'/'.$applicatnt->form_step.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
			
			if(check_other_permission(17,'d')==1) 
			{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$applicatnt->applicant_id.'" data-url="'.base_url().'inquiries/delete_applicant/'.$applicatnt->applicant_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
			
			if(check_other_permission(104,'v')==1) 
			{	$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$applicatnt->applicant_id.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';	}
			
			if(check_other_permission(105,'v')==1) 
			{	$action .= '<a class="iconspace" data-toggle="modal" href="#View" data-tile="الخط الزمنى" onClick="showglobaldata(this);" id="'.$tempdata->id.'" data-url="'.base_url().'ajax/timeline/'.$applicatnt->applicant_id.'/'.$applicatnt->form_step.'"><i style="color:#9C0;" class="icon-time"></i></a>';	}
			
			if(check_other_permission(108,'v')==1) 
			{	$action .= '<a class="iconspace" href="'.base_url().'inquiries/get_sms_history/2/'.$applicatnt->applicant_id.'"><i class="icon-comments-alt"></i></a>';	}
			
			
			$arr[] = array(
				"DT_RowId"		=>	$applicatnt->applicant_id.'_durar_lm',
                "رقم" 			=>	applicant_number($applicatnt->applicant_id),
                "الإسم" 			=>	$name,
				"نوع المنتج" 		=>	getLoanTitle($applicatnt->applicant_id),
				"صيغة المشروع"		=>	$applicatnt->applicant_type,
                "النوع" 			=>	$gender,
                "الرقم المدني" 	=>	$applicatnt->appliant_id_number,
                "الهاتف النقال" 		=>	$phonenumber,
                "المرحلة" 			=>	$al_marhala,
                "تاريخ التسجيل" 		=>	date('Y-m-d', strtotime($applicatnt->applicant_regitered_date)),
                "الإجرائات" 		=>	$action);

				unset($gender,$action,$phone,$professional,$steps,$type_name,$decision_type,$phonenumber,$idcardnumber,$gender,$name,$al_marhala,$decision_type);
       }
	   
        $ex['data'] = $arr;
        echo json_encode($ex);
    }

//-------------------------------------------------------------------------------
	
	public function banner_images()
    {
        $this->load->model('inquiries/inquiries_model', 'inq_ajax');
		
        foreach($this->inq_ajax->get_all_banners() as $banner)
        {
			if($banner->isactive	==	0)
			{
				$status	=	'غير نشط';
			}
			else
			{
				$status	=	'نشط';
			}
			
			$banner_image	=	'<img src="'.base_url().'upload_files/banners/'.$banner->banner_image.'" width="450" height="100"/>';
            $arr[] = array(
				"DT_RowId"		=>	$banner->imageid.'_durar_lm',
                "اسم الصورة" 	=>	$banner->image_title,
				"راية صورة"		=>	$banner_image,
                "الإجرائات" 		=>	'
				 <a href="'.base_url().'inquiries/addbanners/'.$banner->imageid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>
				<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$banner->imageid.'" data-url="'.base_url().'inquiries/delete_banner/'.$banner->imageid.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>
<a href="'.base_url().'inquiries/update_status/'.$banner->isactive.'/'.$banner->imageid.'">'.$status.'</a>');
       }
	   
        $ex['data'] = $arr;
        echo json_encode($ex);
    }

//-------------------------------------------------------------------------------
	
	public function mukhatib_listing_filter()
    {
        $this->load->model('inquiries/inquiries_model', 'inq_ajax');
		
        foreach($this->inq_ajax->get_mukhair() as $applicatnt)
        {
			$professional	=	$this->inq_ajax->getRequestInfo($applicatnt->applicant_id);
			
			$phone 			= $this->inq_ajax->applicant_phone_number($applicatnt->applicant_id);
			
            $name 			=	'';
            $app_name 		=	'';
            $idcardnumber 	=	'';
            $phonenumber 	=	'';
			$gender			=	'';
			$type_name		=	'';
			$decision_type	=	'';
			
			if($applicatnt->form_step	==	'1'): $type_name	=	 'تسجيل الطلبات'; endif;

            if($applicatnt->form_step	==	'2'): $type_name	= 'بيانات المشروع'; endif;

            if($applicatnt->form_step	==	'3'): $type_name	= 'القرض المطلوب'; endif;

            if($applicatnt->form_step	==	'4'): $type_name	= 'دراسه وتحليل الطلب'; endif;

            if($applicatnt->form_step	==	'5')
			{ 
				$type_name	= 'قرار اللجنة &nbsp;';  
			}

			if($applicatnt->commitee_decision_type		==	'postponed')
			{ 
				$decision_type	= "تأجيل ";
			}
			elseif($applicatnt->commitee_decision_type	==	'rejected')
			{ 
				$decision_type	= "الرفض ";
			}
			elseif($applicatnt->commitee_decision_type	==	'approved')
			{ 
				$decision_type	= "موافقة ";
			}
			
			
			$name	.=	'<li class="data_list_grid"><a href="'.base_url().'inquiries/newrequest/'.$applicatnt->applicant_id.'/'.$applicatnt->form_step.'">'.$applicatnt->applicant_first_name . ' ' . $applicatnt->applicant_middle_name . ' ' . $applicatnt->applicant_last_name . ' ' . $applicatnt->applicant_sur_name.'</a></li>';
			$app_name	=	$applicatnt->applicant_first_name. ' ' . $applicatnt->applicant_middle_name .' ' . $applicatnt->applicant_last_name . ' ' . $applicatnt->applicant_sur_name;
            foreach ($professional['applicant_partners'] as $names)
            {   
				if(!empty($names)) 
				{
						$name .= '<li class="data_list_grid"><a href="'.base_url().'inquiries/viewzyaratlist/'.$applicatnt->applicant_id.'">'.$names->applicant_first_name . ' ' . $names->applicant_middle_name . ' ' . $names->applicant_last_name . ' ' . $names->applicant_sur_name.'</a></li>';
				}
			}

			$gender	.= '<li class="data_list_grid">'.$applicatnt->applicant_gender.'</li>';
			
            foreach ($professional['applicant_partners'] as $gender)
            {  
				if($gender!='') 
				{ 
					$gender .='<li class="data_list_grid">'.$gender->partner_gender.'</li>'; 
				}
			}

            foreach($phone as $ph)
            {
				if($ph!='') 
				{ 
					$phonenumber .='<li class="data_list_grid">'.$ph->applicant_phone.'</li>';
				}
			}

            $arr[] = array(
				"DT_RowId"			=>	$applicatnt->applicant_id.'_durar_lm',
                "رقم"				=>	applicant_number($applicatnt->applicant_id),
                "الإسم" 				=>	$name,
				"صيغة المشروع"		=>	$applicatnt->applicant_type,
                "النوع" 			=>	$gender,
                "الرقم المدني" 	=>	$applicatnt->appliant_id_number,
                "الهاتف النقال" 	=>	$phonenumber,
                "المرحلة" 			=>	$type_name.' '.$decision_type,
                /*"تاريخ التسجيل" =>date('h:m:s', strtotime($inq->applicantdate)).'<span class="label label-warning">'.date("Y-m-d", strtotime($inq->applicantdate)).'</span>',*/
                "الإجرائات" 			=>'<a data-toggle="modal" dataid="'.$applicatnt->applicant_id.'" datatype="'.$type_name.'" datatempvalue="'.$tempid_value.'" dataname="'.$app_name.'" onclick="open_dialog_tasjeel(this)" href="#quickLaunchModal"><i class="icon-eye-open"></i></a>
                <a href="'.base_url().'inquiries/newrequest/'.$applicatnt->applicant_id.'/'.$applicatnt->form_step.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>
				<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$applicatnt->applicant_id.'" data-url="'.base_url().'inquiries/delete_applicant/'.$applicatnt->applicant_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>
                ');

				unset($gender);
       }
	   
        $ex['data'] = $arr;
        echo json_encode($ex);
    }

//-------------------------------------------------------------------------------
	
	public function altalabaat_listing_filter()
    {
        $this->load->model('inquiries/inquiries_model', 'inq_ajax');
		
        foreach($this->inq_ajax->getIsComplete() as $applicatnt)
        {
			$professional	=	$this->inq_ajax->getRequestInfo($applicatnt->applicant_id);
			
			$phone 			=	$this->inq_ajax->applicant_phone_number($applicatnt->applicant_id);
			

            $name 			= '';
            $app_name 		= '';
            $idcardnumber 	= '';
            $phonenumber 	= '';
			$gender			=	'';
			$type_name		=	'';
			$decision_type	=	'';
			
			if($applicatnt->form_step	==	'1'): $type_name	=	 'تسجيل الطلبات'; endif;

            if($applicatnt->form_step	==	'2'): $type_name	= 'بيانات المشروع'; endif;

            if($applicatnt->form_step	==	'3'): $type_name	= 'القرض المطلوب'; endif;

            if($applicatnt->form_step	==	'4'): $type_name	= 'دراسه وتحليل الطلب'; endif;

            if($applicatnt->form_step	==	'5')
			{ 
				$decision_type	= 'قرار اللجنة &nbsp;';  
			}

			if($applicatnt->commitee_decision_type		==	'postponed')
			{ 
				$decision_type	= "تأجيل ";
			}
			elseif($applicatnt->commitee_decision_type	==	'rejected')
			{ 
				$decision_type	= "الرفض ";
			}
			elseif($applicatnt->commitee_decision_type	==	'approved')
			{ 
				$decision_type	= "موافقة ";
			}
			
			
			$name	.=	'<li class="data_list_grid"><a href="'.base_url().'inquiries/viewzyaratlist/'.$applicatnt->applicant_id.'">'.$applicatnt->applicant_first_name . ' ' . $applicatnt->applicant_middle_name . ' ' . $applicatnt->applicant_last_name . ' ' . $applicatnt->applicant_sur_name.'</a></li>';
			
			$app_name	=	$applicatnt->applicant_first_name. ' ' . $applicatnt->applicant_middle_name .' ' . $applicatnt->applicant_last_name . ' ' . $applicatnt->applicant_sur_name;
			
            foreach ($professional['applicant_partners'] as $names)
            {   
				if(!empty($names)) 
				{
						$name .= '<li class="data_list_grid"><a href="'.base_url().'inquiries/viewzyaratlist/'.$applicatnt->applicant_id.'">'.$names->applicant_first_name . ' ' . $names->applicant_middle_name . ' ' . $names->applicant_last_name . ' ' . $names->applicant_sur_name.'</a></li>';
				}
			}

			$gender	.= '<li class="data_list_grid">'.$applicatnt->applicant_gender.'</li>';
			
            foreach ($professional['applicant_partners'] as $gender)
            {  
				if($gender!='') 
				{ 
					$gender .='<li class="data_list_grid">'.$gender->partner_gender.'</li>'; 
				}
			}

            foreach($phone as $ph)
            {
				if($ph!='') 
				{ 
					$phonenumber .='<li class="data_list_grid">'.$ph->applicant_phone.'</li>';
				}
			}

            $arr[] = array(
				"DT_RowId"			=>	$applicatnt->applicant_id.'_durar_lm',
                "رقم" 				=>	applicant_number($applicatnt->applicant_id),
                "الإسم" 				=>	$name,
				"صيغة المشروع"		=>	$applicatnt->applicant_type,
                "النوع" 			=>	$gender,
                "الرقم المدني" 	=>	$applicatnt->appliant_id_number,
                "الهاتف النقال" 	=>	$phonenumber,
                "المرحلة" 			=>	$type_name.' '.$decision_type,
                /*"تاريخ التسجيل" =>date('h:m:s', strtotime($inq->applicantdate)).'<span class="label label-warning">'.date("Y-m-d", strtotime($inq->applicantdate)).'</span>',*/
                "الإجرائات" 			=>	'<a data-toggle="modal" dataid="'.$applicatnt->applicant_id.'" datatype="'.$type_name.'" datatempvalue="'.$tempid_value.'" dataname="'.$app_name.'" onclick="open_dialog_tasjeel(this)" href="#quickLaunchModal"><i class="icon-eye-open"></i></a>
                <a href="'.base_url().'inquiries/viewzyaratlist/'.$applicatnt->applicant_id.'/'.$applicatnt->form_step.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>
				<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$applicatnt->applicant_id.'" data-url="'.base_url().'inquiries/delete_applicant/'.$applicatnt->applicant_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>
                ');

				unset($gender);
				unset($name);
       }
	   
        $ex['data'] = $arr;
        echo json_encode($ex);
    }
	
	public function listingaproval_view($type,$notwhere='',$branchid)
	{
		$query = "SELECT a.applicant_id,a.applicant_first_name,a.applicant_middle_name,a.applicant_last_name,a.applicant_sur_name, a.applicant_type,a.applicant_gender,a.appliant_id_number,a.form_step 
					FROM applicants AS a, admin_users AS au, comitte_decision AS cd
					WHERE a.`addedby`=au.`id` AND a.`applicant_id`=cd.`applicant_id` AND cd.commitee_decision_type='".$type."' "; 
			if($notwhere!='')
			{	$query .= "	AND cd.committee_decision_is_aproved!='".$notwhere."'" ;	}
			
			if($branchid!='' && $branchid!=0)
			{	$query .= "	AND au.`branch_id`='".$branchid."'";	}
				$query .= " ORDER BY a.applicant_id DESC";
				$tempNotes = $this->db->query($query);
		if($tempNotes->num_rows() > 0)
		{
			foreach($tempNotes->result() as $tempdata)
			{
				$fTotal = getCountsById('financial_returns',$tempdata->applicant_id);
				$a1 = $fTotal->total;
				
				$steps = get_lable('step-'.$tempdata->form_step);
				$actions  =' <a data-toggle="modal" dataid="'.$tempdata->applicant_id.'" onclick="open_dialog_kashaf(this,1);" href="#quickLaunchModal"><i class="icon-check"></i></a>';
				if(check_other_permission(68,'u')==1)
				{	$actions .=' <a href="'.base_url().'inquiries/requestmuwafiqawalia/'.$tempdata->applicant_id.'/h5"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
                if(check_other_permission(45,'d')==1)
				{	$actions .='<a data-toggle="modal" href="#1" onclick="show_delete_diag(this);" id="'.$tempdata->applicant_id.'" data-url="'.base_url().'inquiries/delete_applicant/'.$tempdata->applicant_id.'/inquery"><i style="color:#F00;" class="icon-remove-circle"></i></a>';	}
                	$actions .=' <a data-toggle="modal" href="#globalDiag" dataid="'.$tempdata->applicant_id.'" onclick="open_dialog_kashaf(this,2);"><i style="color:#9C0;" class="icon-search"></i></a>';
					$actions .=' <a href="'.base_url().'followup/viewfollowuplist/'.$tempdata->applicant_id.'/h5">('.$a1.')</a>';
					$actions .=' <a href="#" data-url="'.base_url().'inquiries/smsmodal/'.$tempdata->applicant_id.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
					
				$arr[] = array(
				"DT_RowId"=>$tempdata->applicant_id.'_durar_lm',
                "رقم" =>applicant_number($tempdata->applicant_id),
                "الإسم" =>'<a href="'.base_url().'inquiries/requestmuwafiqawalia/'.$tempdata->applicant_id.'/h5">'.$tempdata->applicant_first_name.' '.$tempdata->applicant_middle_name.' '.$tempdata->applicant_last_name.' '.$tempdata->applicant_sur_name.'</a>',
				"النوع" =>$tempdata->applicant_gender,
                "صيغة المشروعة" =>$tempdata->applicant_type,                
                "الرقم المدني" =>$tempdata->appliant_id_number,
                "المرحلة" =>$steps['ar'],
                "الإجرائات" =>$actions);
			}
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}

	public function all_applicant_for_users($userid='')
	{
		// Load inquiries_model.php
		$this->load->model('inquiries/inquiries_model', 'inq_ajax');
		
		$qx = "SELECT applicant_id,applicant_first_name,applicant_middle_name,applicant_last_name,applicant_sur_name,applicant_type,applicant_gender,appliant_id_number,form_step FROM applicants ";	
		if($userid!='' && $userid!=0)
		{	$qx .= " WHERE addedby = '".$userid."' "; 	}
			$qx .= " ORDER BY applicant_regitered_date DESC ";
			$query = $this->db->query($qx);
			
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $applicant)
			{

				$professional = $this->inq_ajax->getRequestInfo($applicant->applicant_id);
				$phone = $this->inq_ajax->applicant_phone_number($applicant->applicant_id);
				
				$name = '<li  class="data_list_grid">'.$applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name.'</li>';
				
				
				if(!empty($professional['applicant_partners']))
				{
					foreach($professional['applicant_partners'] as $partners)
					{
						$name .= '<li  class="data_list_grid">'.$partners->partner_first_name.' '.$partners->partner_middle_name.' '.$partners->partner_last_name.' '.$partners->partner_sur_name.'</li>';
					}
				}
				/////////GENDER
				$gender = '<li  class="data_list_grid">'.$applicant->applicant_gender.'</li>';
				
				if(!empty($professional['applicant_partners']))
				{
					foreach($professional['applicant_partners'] as $partners)
					{
						$gender .= '<li class="data_list_grid">'.$partners->partner_gender.'</li>';
					}
				}
				
				//PHONE NUMBERS
				$phonenumber = '';
				if(!empty($phone))
				{
					foreach($phone as $ph)
					{	$phonenumber .='<li class="data_list_grid">'.$ph->applicant_phone.'</li>';	}
				}
				
				if(!empty($professional['applicant_partners']['partner_phone']))
				{
					foreach($professional['applicant_partners']['partner_phone'] as $partners)
					{	$phonenumber .='<li class="data_list_grid">'.$partners->applicant_phone.'</li>';	}
				}
				
				//STEP NUMBER
				$steps = get_lable('step-'.$applicant->form_step);
				
				
				$actions .=' <a href="#1" onClick="showglobaldata(this);" data-url="'.base_url().'inquiries/get_applicant_data/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#C90;" class="icon-search"></i></a>';
                if(check_other_permission(69,'d')==1)
				{	$actions .=' <a data-toggle="modal" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$applicant->applicant_id.'" data-url="'.base_url().'inquiries/delete_applicant/'.$applicant->applicant_id.'"><i style="color:#F00;" class="icon-remove-circle"></i></a>';	}
                
                // if(check_permission($module,'u')==1) {}
				//echo applicant_number($applicant->applicant_id);
				 $arr[] = array(
				 "DT_RowId"=>$applicant->applicant_id.'_durar_lm',
                "رقم" => applicant_number($applicant->applicant_id),
                "الاسم" => $name,
				"صيغة المشروع" => $applicant->applicant_type,
                "النوع" =>$gender,
                "الرقم المدني" =>$applicant->appliant_id_number,
                "الهاتف" =>$phonenumber,
                "المرحلة" =>$steps['ar'],
				"الإجراءات" =>$actions);
				unset($actions);
			}
			$ex['data'] = $arr;
			//echo '<pre>'; print_r($ex);
			echo json_encode($ex);			
		}
	}	
	
	function getReschdual($status='0'){
		$sql= "SELECT 
			  a.*,
			  r.* 
			FROM
			  `applicants` AS a 
			  INNER JOIN `rescheduling` AS r 
				ON r.`applicant_id` = a.`applicant_id` 
			WHERE r.`is_approved` = '".$status."'";
			 
			$query = $this->db->query($sql);
		  	
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $applicant)
			{			
				//STEP NUMBER
				$steps = get_lable('step-'.$applicant->form_step);
				$applicant_name = $applicant->applicant_first_name." ".$applicant->applicant_middle_name." ".$applicant->applicant_last_name;			
				if(check_other_permission(56,'u')==1)
				{	$actions .=' <a href="'.base_url().'inquiries/requestphasefive/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';	}
				
				if(check_other_permission(56,'d')==1)
				{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$applicant->applicant_id.'" data-url="'.base_url().'inquiries/delete_applicant/'.$applicant->applicant_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
				
				if(!$applicant->is_reject && $applicant->is_reject !=""){
					$reponse = '<span style="background: none repeat scroll 0% 0% green; color: white; font-size: 15px; padding: 5px;">متفق عليه <i style="color:#4096EE;" class="icon-thumbs-up-alt"></i></span>';
				}
				else{
					//$reponse = '<a onclick="open_dialog_approve(this);" href="#bankResponse" data-type="2" dataappname="" data-id="" dataappname="" id='' data-toggle="modal"><span style="background: none repeat scroll 0% 0% red; color: white; font-size: 15px; padding: 5px;">متفق عليه <i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></span>';
					//$reponse = '<a onclick="open_dialog_approve(this);" href="#bankResponse" data-type="2" datapdetails="'.$applicant->reject_reason.'" dataappname="'.$applicant_name.'" data-id="'.$applicant->applicant_id.'" dataappname="'.$applicant_name.'" id="'.$applicant->applicant_id.'" data-toggle="modal"><span background: none repeat scroll 0% 0% red; color: white; font-size: 15px; padding: 5px;>اختلف<i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></span></a>';
					$reponse = '<span style="background: none repeat scroll 0% 0% red; color: white; font-size: 15px; padding: 5px;cursor:pointer;" data-response="'.$applicant->reject_reason.'" onclick="viewBankResponse(this)">متفق عليه <i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></span>';
					
				}		
				
				$actions .=' <a  onclick="open_dialog_approve_new(this);" href="#bankModelNew" data-type="1" dataappname="'.$applicant_name.'" data-id="'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'" data-pk="'.$applicant->id.'" data-toggle="modal"><i style="color:#4096EE;" class="icon-thumbs-up-alt"></i></a> ';
				$actions .=' <a onclick="open_dialog_approve_new(this);" href="#bankModelNew" data-type="2" dataappname="'.$applicant_name.'" data-id="'.$applicant->applicant_id.'" dataappname="'.$applicant_name.'" id="'.$applicant->applicant_id.'" data-pk="'.$applicant->id.'" data-toggle="modal"><i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></a> ';
				$actions .=' <a href="#1" onClick="showglobaldata(this);" data-url="'.base_url().'inquiries/get_applicant_data/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#0099FF;" class="icon-search"></i></a> ';
				$actions .=' <a href="#1" onClick="showglobaldata(this);" data-url="'.base_url().'inquiries/requestdocument_data/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#0099FF;" class="icon-file-alt"></i></a> ';
				$actions .=' <a href="'.base_url().'followup/bankfollowup/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#8EA928;" class="icon-usd"></i></a> ';
                
              	$name = '<a href="'.base_url().'inquiries/requestphasefive/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'">'.$applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name.'</a>';
				 $arr[] = array(
				 "DT_RowId"=>$applicant->applicant_id.'_durar_lm',
                "رقم" => applicant_number($applicant->applicant_id),
                "الاسم" => $name,
				"صيغة المشروع" => $applicant->applicant_type,
                "النوع" =>$applicant->applicant_gender,
                "الرقم المدني" =>$applicant->appliant_id_number,               
                "عدد الأقساط" =>$applicant->installment_number,
				//قرار
				"الإجراءات" =>$actions);
				unset($actions);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}	
	}
	
	function getReschdualApprove($status='0'){
		$sql= "SELECT 
			  a.*,
			  r.* 
			FROM
			  `applicants` AS a 
			  INNER JOIN `rescheduling` AS r 
				ON r.`applicant_id` = a.`applicant_id` 
			WHERE r.`is_approved` = '".$status."'";
			 
			$query = $this->db->query($sql);
		  	
		if($query->num_rows() > 0)
		{
			//echo "<pre>";
			//print_r($query->result());
			//exit;
			foreach($query->result() as $applicant)
			{			
				//STEP NUMBER
				$steps = get_lable('step-'.$applicant->form_step);
				$applicant_name = $applicant->applicant_first_name." ".$applicant->applicant_middle_name." ".$applicant->applicant_last_name;			
				if(check_other_permission(56,'u')==1)
				{	$actions .=' <a href="'.base_url().'inquiries/requestphasefive/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';	}
				
				if(check_other_permission(56,'d')==1)
				{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$applicant->applicant_id.'" data-url="'.base_url().'inquiries/delete_applicant/'.$applicant->applicant_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
				
				if(!$applicant->is_reject && $applicant->is_reject !=""){
					$reponse = '<span style="background: none repeat scroll 0% 0% green; color: white; font-size: 15px; padding: 5px;">متفق عليه <i style="color:#4096EE;" class="icon-thumbs-up-alt"></i></span>';
				}
				else{
					//$reponse = '<a onclick="open_dialog_approve(this);" href="#bankResponse" data-type="2" dataappname="" data-id="" dataappname="" id='' data-toggle="modal"><span style="background: none repeat scroll 0% 0% red; color: white; font-size: 15px; padding: 5px;">متفق عليه <i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></span>';
					//$reponse = '<a onclick="open_dialog_approve(this);" href="#bankResponse" data-type="2" datapdetails="'.$applicant->reject_reason.'" dataappname="'.$applicant_name.'" data-id="'.$applicant->applicant_id.'" dataappname="'.$applicant_name.'" id="'.$applicant->applicant_id.'" data-toggle="modal"><span background: none repeat scroll 0% 0% red; color: white; font-size: 15px; padding: 5px;>اختلف<i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></span></a>';
					$reponse = '<span style="background: none repeat scroll 0% 0% red; color: white; font-size: 15px; padding: 5px;cursor:pointer;" data-response="'.$applicant->reject_reason.'" onclick="viewBankResponse(this)">متفق عليه <i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></span>';
					
				}
				//$reject_reason = ' <a href="#reject_popup" onClick="show_reject_dialog(this);" id="'.$applicant->applicant_id.'" data-reason="'.$applicant->reject_reason.'">'.substr($applicant->reject_reason,0,100).'</a>';		
				
				$actions .=' <a  onclick="open_dialog_approve_new(this);" href="#bankModelNew" data-type="1" dataappname="'.$applicant_name.'" data-id="'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'" data-pk="'.$applicant->id.'" data-toggle="modal"><i style="color:#4096EE;" class="icon-thumbs-up-alt"></i></a> ';
				$actions .=' <a onclick="open_dialog_approve_new(this);" href="#bankModelNew" data-type="2" dataappname="'.$applicant_name.'" data-id="'.$applicant->applicant_id.'" dataappname="'.$applicant_name.'" id="'.$applicant->applicant_id.'" data-pk="'.$applicant->id.'" data-toggle="modal"><i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></a> ';
				$actions .=' <a href="#1" onClick="showglobaldata(this);" data-url="'.base_url().'inquiries/get_applicant_data/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#0099FF;" class="icon-search"></i></a> ';
				$actions .=' <a href="#1" onClick="showglobaldata(this);" data-url="'.base_url().'inquiries/requestdocument_data/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#0099FF;" class="icon-file-alt"></i></a> ';
				$actions .=' <a href="'.base_url().'followup/bankfollowup/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#8EA928;" class="icon-usd"></i></a> ';
                
              	$name = '<a href="'.base_url().'inquiries/requestphasefive/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'">'.$applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name.'</a>';
				 $arr[] = array(
				 "DT_RowId"=>$applicant->applicant_id.'_durar_lm',
                "رقم" => applicant_number($applicant->applicant_id),
                "الاسم" => $name,
				"صيغة المشروع" => $applicant->applicant_type,
                "النوع" =>$applicant->applicant_gender,
                "الرقم المدني" =>$applicant->appliant_id_number,
				"مبلغ الأقساط" =>$applicant->approved_amount,               
                "عدد الأقساط" =>$applicant->installment_number,
				//قرار
				"الإجراءات" =>$actions);
				unset($actions);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}	
	}

	function getReschdualReject($status='0'){
		$sql= "SELECT 
			  a.*,
			  r.* 
			FROM
			  `applicants` AS a 
			  INNER JOIN `rescheduling` AS r 
				ON r.`applicant_id` = a.`applicant_id` 
			WHERE r.`is_approved` = '".$status."'";
			 
			$query = $this->db->query($sql);
		  	
		if($query->num_rows() > 0)
		{
			//echo "<pre>";
			//print_r($query->result());
			//exit;
			foreach($query->result() as $applicant)
			{			
				//STEP NUMBER
				$steps = get_lable('step-'.$applicant->form_step);
				$applicant_name = $applicant->applicant_first_name." ".$applicant->applicant_middle_name." ".$applicant->applicant_last_name;			
				if(check_other_permission(56,'u')==1)
				{	$actions .=' <a href="'.base_url().'inquiries/requestphasefive/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';	}
				
				if(check_other_permission(56,'d')==1)
				{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$applicant->applicant_id.'" data-url="'.base_url().'inquiries/delete_applicant/'.$applicant->applicant_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
				
				if(!$applicant->is_reject && $applicant->is_reject !=""){
					$reponse = '<span style="background: none repeat scroll 0% 0% green; color: white; font-size: 15px; padding: 5px;">متفق عليه <i style="color:#4096EE;" class="icon-thumbs-up-alt"></i></span>';
				}
				else{
					//$reponse = '<a onclick="open_dialog_approve(this);" href="#bankResponse" data-type="2" dataappname="" data-id="" dataappname="" id='' data-toggle="modal"><span style="background: none repeat scroll 0% 0% red; color: white; font-size: 15px; padding: 5px;">متفق عليه <i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></span>';
					//$reponse = '<a onclick="open_dialog_approve(this);" href="#bankResponse" data-type="2" datapdetails="'.$applicant->reject_reason.'" dataappname="'.$applicant_name.'" data-id="'.$applicant->applicant_id.'" dataappname="'.$applicant_name.'" id="'.$applicant->applicant_id.'" data-toggle="modal"><span background: none repeat scroll 0% 0% red; color: white; font-size: 15px; padding: 5px;>اختلف<i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></span></a>';
					$reponse = '<span style="background: none repeat scroll 0% 0% red; color: white; font-size: 15px; padding: 5px;cursor:pointer;" data-response="'.$applicant->reject_reason.'" onclick="viewBankResponse(this)">متفق عليه <i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></span>';
					
				}
				$reject_reason = ' <a href="#reject_popup" onClick="show_reject_dialog(this);" id="'.$applicant->applicant_id.'" data-reason="'.$applicant->reject_reason.'">'.substr($applicant->reject_reason,0,100).'</a>';		
				
				$actions .=' <a  onclick="open_dialog_approve_new(this);" href="#bankModelNew" data-type="1" dataappname="'.$applicant_name.'" data-id="'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'" data-pk="'.$applicant->id.'" data-toggle="modal"><i style="color:#4096EE;" class="icon-thumbs-up-alt"></i></a> ';
				$actions .=' <a onclick="open_dialog_approve_new(this);" href="#bankModelNew" data-type="2" dataappname="'.$applicant_name.'" data-id="'.$applicant->applicant_id.'" dataappname="'.$applicant_name.'" id="'.$applicant->applicant_id.'" data-pk="'.$applicant->id.'" data-toggle="modal"><i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></a> ';
				$actions .=' <a href="#1" onClick="showglobaldata(this);" data-url="'.base_url().'inquiries/get_applicant_data/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#0099FF;" class="icon-search"></i></a> ';
				$actions .=' <a href="#1" onClick="showglobaldata(this);" data-url="'.base_url().'inquiries/requestdocument_data/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#0099FF;" class="icon-file-alt"></i></a> ';
				$actions .=' <a href="'.base_url().'followup/bankfollowup/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#8EA928;" class="icon-usd"></i></a> ';
                
              	$name = '<a href="'.base_url().'inquiries/requestphasefive/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'">'.$applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name.'</a>';
				 $arr[] = array(
				 "DT_RowId"=>$applicant->applicant_id.'_durar_lm',
                "رقم" => applicant_number($applicant->applicant_id),
                "الاسم" => $name,
				"صيغة المشروع" => $applicant->applicant_type,
                "النوع" =>$applicant->applicant_gender,
                "الرقم المدني" =>$applicant->appliant_id_number,
				"رفض السبب" =>$reject_reason,               
                "عدد الأقساط" =>$applicant->installment_number,
				//قرار
				"الإجراءات" =>$actions);
				unset($actions);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}	
	}
	
	public function applicant_check_list()
	{
		$query = $this->db->query("SELECT 
						  a.applicant_id,
						  a.applicant_first_name,
						  a.applicant_middle_name,
						  a.applicant_last_name,
						  a.applicant_sur_name,
						  a.applicant_type,
						  a.applicant_gender,
						  a.appliant_id_number,
						  br.*,	
						  a.form_step 
						FROM
						  `applicants` AS a 
						  INNER JOIN `check_list` AS cl 
							ON cl.`applicant_id` = a.`applicant_id` 
						  INNER JOIN `comitte_decision` AS cd 
							ON cd.`applicant_id` = a.`applicant_id` 
						  LEFT JOIN `bank_response` AS br 
						  ON br.`applicant_id`=a.`applicant_id`
						WHERE cl.`sealed_company` = '1' 
						  AND cl.`registration_zip` = '1' 
						  AND cl.`municipal_contractrent` = '1' 
						  AND cl.`open_account` = '1' 
						  AND cl.`membership_certificate` = '1' 
						  AND cl.`company_general_authority` = '1' 
						  AND cl.`commercial_papers` = '1' 
						  AND cl.`check_book` = '1' 
						  AND `cd`.`commitee_decision_type` = 'approved'");
		  	
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $applicant)
			{			
				//STEP NUMBER
				$steps = get_lable('step-'.$applicant->form_step);
				$applicant_name = $applicant->applicant_first_name." ".$applicant->applicant_middle_name." ".$applicant->applicant_last_name;			
				if(check_other_permission(56,'u')==1)
				{	$actions .=' <a href="'.base_url().'inquiries/requestphasefive/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';	}
				
				if(check_other_permission(56,'d')==1)
				{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$applicant->applicant_id.'" data-url="'.base_url().'inquiries/delete_applicant/'.$applicant->applicant_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
				
				if(!$applicant->is_reject && $applicant->is_reject !=""){
					$reponse = '<span style="background: none repeat scroll 0% 0% green; color: white; font-size: 15px; padding: 5px;">متفق عليه <i style="color:#4096EE;" class="icon-thumbs-up-alt"></i></span>';
				}
				elseif($applicant->is_reject ==""){
					$reponse = '<span style="background: none repeat scroll 0% 0% yellow; color: white; font-size: 15px; padding: 5px;">في انتظار من قبل البنك <i style="color:#4096EE;" class="icon-thumbs-up-alt"></i></span>';
				}
				else{
					//$reponse = '<a onclick="open_dialog_approve(this);" href="#bankResponse" data-type="2" dataappname="" data-id="" dataappname="" id='' data-toggle="modal"><span style="background: none repeat scroll 0% 0% red; color: white; font-size: 15px; padding: 5px;">متفق عليه <i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></span>';
					//$reponse = '<a onclick="open_dialog_approve(this);" href="#bankResponse" data-type="2" datapdetails="'.$applicant->reject_reason.'" dataappname="'.$applicant_name.'" data-id="'.$applicant->applicant_id.'" dataappname="'.$applicant_name.'" id="'.$applicant->applicant_id.'" data-toggle="modal"><span background: none repeat scroll 0% 0% red; color: white; font-size: 15px; padding: 5px;>اختلف<i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></span></a>';
					$reponse = '<span style="background: none repeat scroll 0% 0% red; color: white; font-size: 15px; padding: 5px;cursor:pointer;" data-response="'.$applicant->reject_reason.'" onclick="viewBankResponse(this)">غير موفق<i style="color:#4096EE;" class="icon-thumbs-down-alt"></i></span>';
					
				}		
				
				$actions .=' <a href="#1" onClick="showglobaldata(this);" data-url="'.base_url().'inquiries/get_applicant_data/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#0099FF;" class="icon-search"></i></a> ';
				$actions .=' <a href="#1" onClick="showglobaldata(this);" data-url="'.base_url().'inquiries/requestdocument_data/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#0099FF;" class="icon-file-alt"></i></a> ';
				$actions .=' <a href="'.base_url().'followup/bankfollowup/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'"><i style="color:#8EA928;" class="icon-usd"></i></a> ';
                $actions .=' <a href="#" data-url="'.base_url().'inquiries/smsmodal/'.$applicant->applicant_id.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
				
              	$name = '<a href="'.base_url().'inquiries/requestphasefive/'.$applicant->applicant_id.'" id="'.$applicant->applicant_id.'">'.$applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name.'</a>';
				 $arr[] = array(
				 "DT_RowId"=>$applicant->applicant_id.'_durar_lm',
                "رقم" => applicant_number($applicant->applicant_id),
                "الاسم" => $name,
				"صيغة المشروع" => $applicant->applicant_type,
                "النوع" =>$applicant->applicant_gender,
                "الرقم المدني" =>$applicant->appliant_id_number,               
                "المرحلة" =>$steps['ar'],
				"البنك قرار" =>$reponse,
				//قرار
				"الإجراءات" =>$actions);
				unset($actions);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
	
	
	function get_document_data(){
						
	}
	public function bank_listing_page()
	{
		$query = $this->db->query("
		SELECT 
		  admin_users.id,
		  admin_users.user_name,
		  admin_users.firstname,
		  admin_users.lastname,
		  admin_users.email,
		  admin_users.number,
		  bb.branch_name 
		FROM
	  	`admin_users` 
	  	INNER JOIN 
		`bank_branches` AS bb 
		ON bb.`branch_id` = admin_users.`bank_branch_id` 
		ORDER BY admin_users.`firstname` ASC 
		");
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $applicant)
			{
				if(check_other_permission(58,'u')==1)
				{
					$actions .=' <a href="'.base_url().'inquiries/add_user_registeration/'.$applicant->id.'" id="'.$applicant->id.'"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
				}
				
				if(check_other_permission(58,'d')==1)
				{
					$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$applicant->id.'" data-url="'.base_url().'inquiries/delete_user/'.$applicant->id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';
				}
				
				
				$name = $applicant->firstname.' '.$applicant->lastname;
				 
				 $arr[] = array(
				"DT_RowId"			=>	$applicant->id.'_durar_lm',
                "اسم"				=>	$name,
                "اسم المستخدم"		=>	$applicant->user_name,
				"البريد الإلكتروني"	=>	$applicant->email,
                "رقم الهاتف"		=>	$applicant->number,
                "فرع"				=>	$applicant->branch_name,               
				"الإجراءات"			=>	$actions);
				
				unset($actions);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
		}
	}
	
	public function list_qaimatulbarnamaj()
	{
		$this->db->select('loan_caculate_id,loan_category_name,parent_id');
		$this->db->where_in('parent_id',0);
		$query = $this->db->get('loan_calculate');
		foreach($query->result() as $lc)
		{
				if(check_other_permission(34,'a')==1)
				{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'lease_programe/add_child_barnamij/'.$lc->loan_caculate_id.'/" id="'.$lc->loan_caculate_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> ';	}
								
				if(check_other_permission(35,'u')==1)
				{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'lease_programe/add_data_for_barnamij/'.$lc->loan_caculate_id.'/parent" id="'.$lc->loan_caculate_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';	}
				
				if(check_other_permission(35,'d')==1)
				{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->loan_caculate_id.'" data-url="'.base_url().'lease_programe/delete/'.$lc->loan_caculate_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
				
					$count = $this->qaimatulbarnamaj_count($lc->loan_caculate_id);
					
				 	$actions .=' <a href="'.base_url().'lease_programe/child_listing/'.$lc->loan_caculate_id.'">عرض الكل ('.$count.')</a>'; 
				 $arr[] = array(
				"DT_RowId"=>$lc->loan_caculate_id.'_durar_lm',
                "قائمة البرامج" =>$lc->loan_category_name,              
				"الإجراءات" =>$actions);
				unset($actions);
		}
		$ex['data'] = $arr;
			echo json_encode($ex);
	}
	
	public function list_qaimatulbarnamaj_child($parentid)
	{
		$this->db->where('parent_id',$parentid);
		$query = $this->db->get('loan_calculate');
		foreach($query->result() as $lc)
		{
			if(check_other_permission(70,'u')==1)
				{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'lease_programe/add_child_barnamij/'.$lc->parent_id.'/'.$lc->loan_caculate_id.'" id="'.$lc->loan_caculate_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';	}
				
				if(check_other_permission(70,'d')==1)
				{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->loan_caculate_id.'" data-url="'.base_url().'lease_programe/delete/'.$lc->loan_caculate_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
					$count = $this->qaimatulbarnamaj_count($lc->loan_caculate_id);
					
				 	
				 $arr[] = array(
				"DT_RowId"=>$lc->loan_caculate_id.'_durar_lm',
                "إسم" =>is_set($lc->loan_category_name),
				"من" =>is_set($lc->loan_start_amount),
				"إلى" =>is_set($lc->loan_end_amount),
				"نسبة الرسوم" =>is_set($lc->loan_percentage).'%',
				"فترة السماح" =>is_set($lc->loan_starting_day),
				"مدة السداد (سنة)" =>is_set($lc->loan_expire_day),
				"المساهمة الشخصية" =>is_set($lc->loan_applicant_percentage),          
				"الإجراءات" =>$actions);
				unset($actions);
		}
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
	
	public function qaimatulbarnamaj_count($id)
	{
		//$this->db->where("parent_id",$id);
		//$query = $this->db->get($this->_table_loan_calculate);
		 $sql = "SELECT COUNT(*) AS total FROM (`loan_calculate`) WHERE `parent_id` = '".$id."'";
		$q = $this->db->query($sql);		
	
		// Check if Result is Greater Than Zero
		if($q->num_rows() > 0)
		{
			 $oneRow = $q->row();
			 return $oneRow->total;
			 //print_r($oneRow);
		}
	}
	
	public function getArabicName($table)
	{
		$arabicQuery = $this->db->query("SELECT TABLE_NAME,TABLE_COMMENT FROM information_schema.tables WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='".$table."' AND TABLE_COMMENT!='' LIMIT 1;");
		foreach($arabicQuery->result() as $res)
		{
			return $res->TABLE_COMMENT;
		}
	}
	
	
	public function user_history()
	{
		$userid = $this->input->post('userid');
		$first_date = $this->input->post('first_date');
		$second_date = $this->input->post('second_date');
		$query = "SELECT admin_users.`firstname`, admin_users.`lastname`, application_activity.* FROM application_activity, admin_users WHERE application_activity.userid = admin_users.id AND ";
		$query .= " application_activity.`datatable` NOT IN ('temp_document','temp_main_applicant','temp_main_inquirytype','temp_main_notes','temp_main_phone') AND ";
		if($userid!='')
		{	$query .= "application_activity.userid='".$userid."' AND ";	}
		
		if($first_date!='' && $second_date!='')
		{
			$query .= "(DATE(application_activity.`activitytime`) >= '".$first_date."' AND DATE(application_activity.`activitytime`) <= '".$second_date."') AND ";
		}
		else if($first_date!='')
		{
			$query .= "DATE(application_activity.`activitytime`) = '".$first_date."' AND ";
		}
		else if($second_date!='')
		{
			$query .= "DATE(application_activity.`activitytime`) = '".$second_date."' AND ";
		}
		else
		{
			//$query .= "DATE(application_activity.`activitytime`) = '".date('Y-m-d')."' AND ";
		}
			$query .= " application_activity.activityid > 0 ORDER BY application_activity.`activitytime` DESC";
			
		$res = $this->db->query($query);
		foreach($res->result() as $tempdata)
		{
			if($tempdata->activitytype=='I')
			{	$div = 'alert-green';	
				$title = 'سجل المضافة في'; }
			else if($tempdata->activitytype=='U')
			{	$div = 'alert-yellow';	
				$title = 'سجل تحديثها في'; }
			else
			{	$div = 'alert-red';	
				$title = 'سجل محذوف من'; }
				
			if($tempdata->activityip=='::1')
			{	$ip = 'Testing';	}
			else
			{	$ip = $tempdata->activityip;	}
			
			$logdetail = base_url().'ajax/getlogdetail/'.$tempdata->activityid;
			$ipdetail = base_url().'ajax/getIplocation/'.$tempdata->activityid;
			$stringvalue = '<strong>'.$tempdata->firstname.' '.$tempdata->lastname.'</strong> '.$title.' <strong id="logbox'.$tempdata->activityid.'" data-id="'.$tempdata->activityid.'" class="tablenames"><a href="#" id="detail'.$tempdata->activityid.'" data-url="'.$logdetail.'" onClick="openFancyBox(this);">'.$this->getArabicName(str_replace('`','',$tempdata->datatable)).'</a></strong> في '.show_date($tempdata->activitytime,5);
			
			$arr[] = array(
				"DT_RowId"=>$tempdata->activityid.'_durar_lm',				
                "المستخدم" =>$stringvalue,
				"نوع" =>'<i class="icon-circle '.$div.'"></i>',
				"الأصل" =>'<i style="cursor:pointer;" onClick="openFancyBox(this);" data-url="'.$ipdetail.'" class="icon-hospital '.$div.'"></i>'
               );
			 unset($string,$title,$div,$logdetail,$ipdetail);  
		}
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
	
	///-------------------30/12/2014
	public function getIplocation($ip)
	{
		//$ip = $this->input->post('ip');
		if($ip=='Testing')
		{
			echo 'Testing Server';
		}
		else
		{
			$ipdata = file_get_contents('http://api.db-ip.com/addrinfo?addr='.$ip.'&api_key=77fd931df0fadd8577f9548612430df7d63c0be6');
			$iparray = json_decode($ipdata,TRUE);
			echo $iparray['city'].', '.$iparray['stateprov'].', '.$iparray['country'];
		}
	}
	
	public function getlogdetail($logid)
	{
		$this->db->select('dataid');
		$this->db->from('application_activity');
		$this->db->where('activityid',$logid); 
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{			
			foreach($query->result() as $data)
			{
				$logjson = explode(',',$data->dataid);
				$this->load->view('logdata',array('noor'=>$logjson));	
			}
			
		}
		else
		{
			echo('<center>البيانات لم يتم العثور</center>');
		}
	}
	
	function add_bank_schduale(){
		$p = $this->input->post();
		//echo "<pre>";
		//print_r($p);
		//exit;
		$app_id = $p['app_id'];
		$data['reject_reason'] = $p['reject_reason'];
		$data['approved_amount'] = $p['approved_amount'];
		$data['is_approved'] = $p['is_approved'];
		$this->db->where('id',$app_id);
		echo $this->db->update('rescheduling',$data);
		exit;
	}
	public function add_bank_response(){
				$p = $this->input->post();
				//echo "<pre>";
				//print_r($p);
				//exit;
			$list_id = $p['app_id'];
			
			if(isset($p['list_reason']) && $p['list_reason']!=""){
			$data['reject_reason'] = $p['list_reason'];
			 $data['is_reject'] =1;
			}
			else{
				$data['reject_reason'] = '';
				$data['is_reject'] =0;
				
				$data['loan_id'] = $p['list_accept'];
			}
			
			if(isset($p['app_id']))
			$data['applicant_id'] = $p['app_id'];
			else
			$data['applicant_id'] = '';
			
			
		$this->db->select('*');
		$this->db->from('bank_response');
		$this->db->where('applicant_id',$list_id); 
		$query = $this->db->get();
		if($query->num_rows() > 0){
			//echo "if";
			$data['loan_id'] ='';
			$this->db->where('applicant_id',$list_id);
			echo $this->db->update('bank_response',$data);

		}
		else{
			//echo "else";
			echo $this->db->insert('bank_response',$data);
		}
	}
	
	public function pick($tempid)
	{

		$nnn = get_mobilenumbers($tempid);	

		send_sms(1,$nnn,1,'شيبشسب شيبش شيبشيسب شيبشيب');

	}

	public function timeline($id,$step)
	{

		$data['id'] = $id;
		$data['s'] = $step;
		if($id!='')
		{
			$q = $this->db->query("SELECT * FROM applicants WHERE `applicant_id`='".$id."'");
			$data['r'] = $q->result();
		}
		$this->load->view('timeline',$data);

	}
	public function touwqiat($id)
	{
		$data['id'] = $id;
		if($id!='')
		{
			$q = $this->db->query("
			SELECT ap.applicant_id,ap.applicant_first_name,ap.applicant_middle_name,ap.applicant_last_name,ap.applicant_sur_name,sad.`tahleelfile`,sad.`touqyatid` 
			FROM applicants AS ap 
			INNER JOIN `study_analysis_demand` AS sad ON (ap.`applicant_id`=sad.`applicant_id`)
			WHERE ap.`applicant_id`='".$id."'");
			$data['r'] = $q->row();
		}
		
		$this->load->view('touwqiat-dialog-form',$data);

	}

	public function save_request_data()
	{

		$this->ajax->saveStep_One();	

	}

	public function ajaxSmsTest()
	{
		send_sms_steps_id();
		}
	
	function loadChild(){
		$p = $this->input->post();
		///echo "<pre>";
		//print_r($p);
		$id = $this->input->post('parentId');
		$loadChild = $this->input->post('child_id');
		
		echo loan_sub_category('loan_limit',$id,$loadChild);
	}
	
	function loadHiddenData(){
		$id = $this->input->post('id');
		$child = $this->input->post('child');
		$response ='';
		$loan_calculate = $this->ajax->get_loan($id,$child);
		echo json_encode($loan_calculate);		
	}

	function loandupdatehiddendata()
	{
		$id = $this->input->post('id');
		$child = $this->input->post('child');
		$q = $this->db->query("SELECT loan_percentage,loan_expire_day,loan_starting_day,loan_applicant_percentage,loan_start_amount,loan_end_amount,loan_expire_timeperiod	FROM `loan_calculate` WHERE `loan_caculate_id` = '".$id."' LIMIT 1");
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row)
			{
				$arr['loan_percentage'] = isset($row->loan_percentage)? $row->loan_percentage:0;
				$arr['loan_expire_day'] = isset($row->loan_expire_day)? $row->loan_expire_day:0;
				$arr['loan_starting_day'] = isset($row->loan_starting_day)? $row->loan_starting_day:0;
				$arr['loan_applicant_percentage'] = isset($row->loan_applicant_percentage)? $row->loan_applicant_percentage:0;
				$arr['loan_start_amount'] = isset($row->loan_start_amount)? $row->loan_start_amount:0;
				$arr['loan_end_amount'] = isset($row->loan_end_amount)? $row->loan_end_amount:0;				
				$arr['loan_expire_timeperiod'] = isset($row->loan_expire_timeperiod)? $row->loan_expire_timeperiod:0;
				$arr['loan_limit_text'] = arabic_date(number_format($row->loan_start_amount,0)).' - '.arabic_date(number_format($row->loan_end_amount,0));
			}
		}
		echo json_encode($arr);
	}

	function checkValue(){
		$id = $this->input->post('id');
		$cr = $this->input->post('cr');
		$number = $this->input->post('number');
		
		if(isset($id) && $id!=""){
			echo $this->ajax->check_applicant('id_number',$id);
		}
		elseif(isset($cr) && $cr!=""){
			//echo $cr;
			echo $this->ajax->check_applicant('cr_number',$cr);
		}
		elseif(isset($number) && $number!=""){
			echo $this->ajax->check_applicant('phone',$number);
		}
	}

	public function getWilayats()

	{	

		$province = $this->input->post('province');

		

		$this->db->select('WILAYATNAME,ID');

		$this->db->from('election_wilayats');

		$this->db->where('REIGONID',$province); 		

		$this->db->order_by("WILAYATNAME", "ASC");

		$query = $this->db->get();

		

		if($query->num_rows() > 0)

		{

			$html = '<option value="">اختر الولاية</option>';

			foreach($query->result() as $data)

			{

				$html .='<option value="'.$data->ID.'">'.$data->WILAYATNAME.'</option>';

			}

			echo $html;

		}

	}

    /////////14-01-2015////////////////////////////////START
    public function getpermission()
    {
        $uid = $this->input->post('uid');
        $q = $this->db->query("SELECT
				`system_permission`.`permission`
				, `system_permission`.`pupdate`
				, `admin_users`.`landingpage`
				, `admin_users`.`user_parent_role`
				, `admin_users`.`user_role_id`
				, `admin_users`.`firstname`
				, `admin_users`.`lastname`
				, `admin_users`.`user_name`
			FROM
				`system_permission`
				INNER JOIN `admin_users` 
					ON (`system_permission`.`userid` = `admin_users`.`id`) WHERE `admin_users`.id='".$uid."';");
		$users = $q->result_array();	
		echo json_encode($users[0]);
		exit();		
        foreach($q->result() as $users)
        {
            $arr['permission'] = $users->permission;
			$arr['landingpage'] = $users->landingpage;
			echo json_encode($users);
        }
    }
    public function roleandusers()
    {
        $user_parent_role = $this->input->post('user_parent_role');
        $user_role_id = $this->input->post('user_role_id');
        if($user_parent_role!='' && $user_role_id=='')
        {
            $this->db->select('role_id,role_name');
            $this->db->from('user_roles');
            $this->db->where('role_parent_id',$user_parent_role);
            $this->db->order_by("role_name", "ASC");
            $query = $this->db->get();
            $dropdown = '<option value="">اختر الوظيفة</option>';
            foreach($query->result() as $row)
            {	$dropdown .= '<option value="'.$row->role_id.'" >'.$row->role_name.'</option>';	}
            $arr['role'] = $dropdown;
        }

        $this->db->select('id,user_name,firstname,lastname,user_role_id');
        $this->db->from('admin_users');
        $this->db->where('status',1);
        if($user_parent_role!='')
        {	$this->db->where('user_parent_role',$user_parent_role);	}

        if($user_role_id!='')
        {	$this->db->where('user_role_id',$user_role_id);	}

        $this->db->order_by("firstname", "ASC");
        $uquery = $this->db->get();
        $arr['uc'] = '('.arabic_date($uquery->num_rows()).')';
        $html = '';
        foreach($uquery->result() as $ures)
        {
            $html .= '<div id="win_'.$ures->id.'" class="mcontent u_count"><input id="chk_'.$ures->id.'" type="checkbox" class="ucount" name="user[]" value="'.$ures->id.'" />  '.$ures->firstname.' '.$ures->lastname.' ('.$ures->user_name.') ';
            if($this->checkPer($ures->id)>0)
            {	$html .= '<i onclick="editpermission(\''.$ures->id.'\');"  class="icon-edit editperm"></i>'; }
            $html .= '</div>';
        }
        $arr['user'] = $html;
        echo json_encode($arr);
    }

    public function justuser()
    {
        $this->db->select('id,user_name,firstname,lastname');
        $this->db->from('admin_users');
        $this->db->where('status',1);
		$this->db->where('user_parent_role !=',19);
        $this->db->order_by("firstname", "ASC");
        $uquery = $this->db->get();
		
		//echo $this->db->last_query();
        $arr['role'] = '';
        $arr['uc'] = '('.arabic_date($uquery->num_rows()).')';
        $html = '';
		
        foreach($uquery->result() as $ures)
        {
			if($ures->lastname	==	'XXX')
			{
				$last_name	=	'';
			}
			else
			{
				$last_name	=	$ures->lastname;
			}
			
            	$html .= '<div id="win_'.$ures->id.'" class="mcontent u_count">
				<input id="chk_'.$ures->id.'" type="checkbox" class="ucount" name="user[]" value="'.$ures->id.'" />  '.$ures->firstname.' '.$last_name.' ('.$ures->user_name.') ';
				
            if($this->checkPer($ures->id)>0)
            {
				$html .= '<i onclick="editpermission(\''.$ures->id.'\');"  class="icon-edit editperm"></i>';
			}
            	$html .= '</div>';
        }
		
        $arr['user'] = $html;
        echo json_encode($arr);
    }

    public function checkPer($uid)
    {
        $nQuery = $this->db->query("SELECT permid FROM system_permission WHERE userid='".$uid."'");
        return $nQuery->num_rows();
    }
    /////////14-01-2015////////////////////////////////END

	public function getParentsRoles()
	{

		$name='user_role_id';

		$value='';

		$req='req';

		$parent_role_id = $this->input->post('parent_role_id');

		

		$this->db->select('role_id,role_name,role_parent_id');

		$this->db->from('user_roles');

		$this->db->where('role_parent_id',$parent_role_id); 		

		$this->db->order_by("role_name", "ASC");

		$query = $this->db->get();

		

		//$dropdown = '<select  name="'.$name.'" id="'.$name.'" placeholder="اختر الوظيفة" >';

		$dropdown .= '<option value="">اختر الوظيفة</option>';



		foreach($query->result() as $row)

		{

			$dropdown .= '<option value="'.$row->role_id.'" ';

			if($value==$row->role_id)

			{

				$dropdown .= 'selected="selected"';

			}

			$dropdown .= '>'.$row->role_name.'</option>';

		}

		//$dropdown .= '</select>';

		echo($dropdown);

	}

	

	function getChildData(){

		$name='loan_category_child_id';

		$value='';

		$req='req';

		$parent_role_id = $this->input->post('parent_role_id');

		

		//$this->db->select('role_id,role_name,role_parent_id');

		//$this->db->from('user_roles');

		//$this->db->where('role_parent_id',$parent_role_id); 		

		//$this->db->order_by("role_name", "ASC");

		$this->db->select('*');

		$this->db->from('loan_category');

		$this->db->where('parent_id',$parent_role_id); 

		$query = $this->db->get();

		

		$dropdown = '<select  name="'.$name.'" id="'.$name.'" placeholder="اختر " >';

		$dropdown .= '<option value="">اختر </option>';



		foreach($query->result() as $row)

		{

			$dropdown .= '<option value="'.$row->loan_category_id.'" ';

			if($value==$row->loan_category_id)

			{

				$dropdown .= 'selected="selected"';

			}

			$dropdown .= '>'.$row->loan_category_name.'</option>';

		}

		$dropdown .= '</select>';

		echo($dropdown);

	}

	

	public function checkLoginID()

	{	

		$loginid = $this->input->post('loginid');		

		$this->db->select('id');

		$this->db->from('admin_users');

		$this->db->where('user_name',$loginid);

		$query = $this->db->get();		

		if($query->num_rows() > 0)

		{

			$ar['result'] = 'error';

		}

		else

		{

			$ar['result'] = 'success';

		}

		$query->free_result();

		echo json_encode($ar);

	}	

	

	public function getChilds()

	{	

		$role_id = trim($this->input->post('role_id'));

		

		$this->db->select('role_id,role_name');

		$this->db->from('user_roles');

		$this->db->where_in('role_parent_id',$role_id); 		

		$this->db->order_by("role_name", "ASC");

		$query = $this->db->query("SELECT role_id, role_name FROM user_roles WHERE role_parent_id IN ($role_id) ORDER BY role_name ASC");

		// $query = $this->db->get();

		//echo $this->db->last_query();

		//exit();

		

		if($query->num_rows() > 0)

		{

			$html = '<option value="">اختر الولاية</option>';

			foreach($query->result() as $data)

			{

				$html .='<option value="'.$data->role_id.'">'.$data->role_name.'</option>';

			}

			echo $html;

		}

	}

	

	public function getLoanAmount()

	{	

		$loan_limit = $this->input->post('loan_limit');

		$loan_amount = $this->input->post('loan_amount');	

		//Get Loan Query

		$qx = $this->db->query("SELECT loan_start_amount,loan_end_amount FROM loan_calculate WHERE loan_category_id='".$loan_limit."' LIMIT 0,1");

		foreach($qx->result() as $loan)

		{

			$msg_xx = arabic_date($loan->loan_start_amount).' ~ '.arabic_date($loan->loan_end_amount);

		}

		

		////////////////

		$this->db->select('*');

		$this->db->from('loan_calculate');

		$this->db->where('loan_category_id',$loan_limit); 		

		$this->db->where('loan_start_amount <=', $loan_amount);

		$this->db->where('loan_end_amount >=', $loan_amount);

		$this->db->limit(1);		

		$query = $this->db->get();

			

				

		if($query->num_rows() > 0)

		{

			foreach($query->result() as $d)

			{			

				$startingdate = date('d/m/Y', strtotime('+1 years'));

				$data['percentage'] = arabic_date($d->loan_percentage).'&nbsp;&nbsp;%';

				$data['current_year'] = date('d/m/Y');

				$data['starting_year'] = $startingdate;

				$data['total_year'] = $d->loan_expire_day.' '.$d->loan_expire_timeperiod.'s';

				$data['loan_age'] = date("d/m/Y", strtotime(date("d/m/Y", strtotime($startingdate)) . " + ".$d->loan_expire_day." ".$d->loan_expire_timeperiod.""));

				$data['amount'] = $loan_amount;

				$data['er'] = '';

				for($i=1; $i<$d->loan_expire_day; $i++)

				{

					$catData = date("d/m/Y", strtotime(date("d/m/Y", strtotime($startingdate)) . " + ".$i." ".$d->loan_expire_timeperiod.""));

					$data['cat'][] = $catData;

					

					if($d->loan_expire_timeperiod=='year')

					{	$data['loan_amount'][] = calcPay($loan_amount, $d->loan_expire_day, 0, $d->loan_percentage,2, 12,0)+rand(0.1,0.99);	

						$data['percent_amount'] = calcPay($loan_amount, $d->loan_expire_day, 0, $d->loan_percentage,2, 12,0,'A');	

					}

					//$data['loan_amount'][] = rand(0.1,0.99);	}	

					else if($d->loan_expire_timeperiod=='month')

					{	$data['loan_amount'][] = calcPay($loan_amount,0 , $d->loan_expire_day, $d->loan_percentage,2, 12,0);

						$data['percent_amount'] = calcPay($loan_amount,0, $d->loan_expire_day, $d->loan_percentage,2, 12,0,'A');	

					}

				}

				//$data['calculation'][] = 	

				echo json_encode($data);

			}

		}

		else

		{

			$data['er'] = 'عذرا لا يمكو كتابة هذا المبلغ في هذا البرنامج لابد ان يكةن اقل من '.$msg_xx;

			echo json_encode($data);

		}

	}

	

	public function history($tempid='')

	{

		$this->_data['history'] = $this->ajax->getHistory($tempid);

		$this->load->view('history',$this->_data);

	}

	

	public function getfulldetail($idcard='1231231321')

	{

		$this->ajax->getDetail($idcard);

		

	}

	



	

	public function getApplicantPhone()
	{
		$term = $this->input->get('term');
		$query = $this->db->query("SELECT `main_phone`.`tempid`,`main_phone`.`applicantid`,CONCAT(`main_applicant`.`first_name`,' ',`main_applicant`.`middle_name`,' ',`main_applicant`.`last_name`,' ',`main_applicant`.`family_name`,' (',`main_phone`.`phonenumber`,')') AS aname
						FROM
							`main_phone`
							INNER JOIN `main_applicant` ON (`main_phone`.`applicantid` = `main_applicant`.`applicantid`)
							WHERE `main_phone`.`phonenumber`!='' AND `main_phone`.`phonenumber` LIKE '%".$term."%'
							ORDER BY aname ASC;");
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $data)
			{
				$arr[] = array('id'=>$data->tempid.':'.$data->applicantid,'label'=>$data->aname,'value'=>$data->aname);
			}
			echo json_encode($arr);
		}

	}	

	
	public function saveTempInfo()
	{
		$reviews = $this->input->post('reviews');
		$value = $this->input->post('value');
		$tempid = $this->input->post('tempid');
		$applicantid = $this->input->post('applicantid');
		$type = $this->input->post('type');
		$column = $this->input->post('column');
		$phoneid = $this->input->post('phoneid');
		$txt = $this->input->post('txt');
		if($reviews=='')
		{	$tmp = 'temp_';	}

		switch($type)
		{
			case 'Applicant';

				$exp = explode('_',$column);
				if($exp[0]=='datepicker')
				{
					$data = array($exp[0] => $value);
				}
				else
				{				
					$data = array($column => $value);					
				}
				if($applicantid!='' && $applicantid!='0')
				{
					$this->db->where('applicantid', $applicantid);
					$this->db->update($tmp.'main_applicant', $data); 
				}
			break;
			case 'Phone';
				$data = array('phonenumber' => $value);
				$this->db->where('phoneid', $phoneid);
				$this->db->update($tmp.'main_phone', $data);
			break;
			case 'Main';
				if($tempid!='' && $tempid!='0')
				{
					$data = array($column => $value);		
					$this->db->where('tempid', $tempid);
					$this->db->update($tmp.'main', $data);
				}
			break;
			case 'Inquiry';
				if($tempid!='' && $tempid!='0')
				{
					$this->db->insert($tmp.'main_inquirytype',array('tempid'=>$tempid,$column=>$value));
				}
			break;
			case 'Notes';	
				
					if($tempid!='' && $tempid!='0')
					{
						$this->db->insert($tmp.'main_notes',array('tempid'=>$tempid,$column=>$value,'userid'=>$this->session->userdata('userid'),'notesip'=>$_SERVER['REMOTE_ADDR'],'inquerytype'=>$txt));
						echo 1;
					}
					else
					{
						echo 0;
					}

			break;

		}



	}

	public function getListofApplicant()

	{

		$term = $this->input->get('term');

		$checkingNumber = substr($term,0,1);

		$checkingid = substr($term,0,2);

		if($checkingNumber==9)

		{

			$query = $this->db->query("SELECT a.phonenumber,a.tempid,CONCAT(b.first_name,' ',b.middle_name,' ',b.last_name,' ',b.family_name) AS applicant_name FROM `main_phone` AS a,`main_applicant` AS b WHERE b.`tempid`=a.`tempid` AND a.phonenumber LIKE '%".$term."%' Order by b.first_name ASC;");

			if($query->num_rows() > 0)

			{

				foreach($query->result() as $data)

				{

					$arr[] = array('id'=>$data->tempid,'label'=>$data->applicant_name,'value'=>$data->tempid);

				}

				echo json_encode($arr);

			}

		}

		else if($checkingid=='00')

		{

			

			$query = $this->db->query("SELECT a.tempid, CONCAT(b.first_name,' ',b.middle_name,' ',b.last_name,' ',b.family_name) AS applicant_name FROM `main` AS a,`main_applicant` AS b WHERE b.`tempid`=a.`tempid` AND a.`tempid_value` LIKE '%".$term."%' GROUP BY a.tempid Order by b.first_name ASC;");

			if($query->num_rows() > 0)

			{

				foreach($query->result() as $data)

				{

					$arr[] = array('id'=>$data->tempid,'label'=>$data->applicant_name,'value'=>$data->tempid);

				}

				echo json_encode($arr);

			}

		}

		else

		{

			$query = "SELECT tempid, CONCAT(first_name,' ',middle_name,' ',last_name,' ',family_name) as applicant_name FROM main_applicant WHERE ";

			if (is_numeric($term))

			{

				$query .= " idcard LIKE '%".$term."%' ";

			}

			else

			{

				$term_explode = explode(' ',$term);

				if($term_explode[0]!='')

				{

					$query .= " first_name LIKE '%".$term_explode[0]."%' AND ";

				}

				if($term_explode[1]!='')

				{

					$query .= " middle_name LIKE '%".$term_explode[1]."%' AND ";

				}

				if($term_explode[2]!='')

				{

					$query .= " last_name LIKE '%".$term_explode[2]."%' AND ";

				}

				if($term_explode[3]!='')

				{

					$query .= " family_name LIKE '%".$term_explode[3]."%' AND ";

				}

					$query .= " idcard!='' ";								

			}

			$query .= " Order by first_name ASC ";

			

			$query = $this->db->query($query);

			if($query->num_rows() > 0)

			{

				foreach($query->result() as $data)

				{

					//{"id":"Upupa epops","label":"Eurasian Hoopoe","value":"Eurasian Hoopoe"}

					$arr[] = array('id'=>$data->tempid,'label'=>$data->applicant_name,'value'=>$data->tempid);

				}

				echo json_encode($arr);

			}

		}		

	}

	

	

	

	public function addNewPartner()

	{
		for($a=1;$a<=4;$a++){
	
		$tempid = $this->input->post('tempid');

		$this->db->insert('temp_main_applicant',array('tempid'=>$tempid,'applicanttype'=>'ذكر'));

		$applicantids[] = $this->db->insert_id();

						$this->db->insert('temp_main_phone',array('tempid'=>$tempid,'applicantid'=>$applicantid));

		}

		return json_encode($applicantids);
		//$this->load->view('new_partner',array('a'=>$applicantid,'t'=>$tempid));

	}

	

	public function removePartner()

	{

		$applicant = $this->input->post('applicant');

		$this->db->delete('temp_main_applicant', array('applicantid' => $applicant));		

	}

		

	public function new_phone()

	{

		$tempid = $this->input->post('tempid');

		$applicantid = $this->input->post('applicantid');

		$reviews = $this->input->post('reviews');

		if($reviews==1)

		{

			$temp = '';

		}

		else

		{

			$temp = 'temp_';

		}

		$this->db->insert($temp.'main_phone',array('tempid'=>$tempid,'applicantid'=>$applicantid));

		$phoneid = $this->db->insert_id();

		

		$this->load->view('new_phone',array('a'=>$applicantid,'t'=>$tempid,'p'=>$phoneid));						

	}

	

	function new_musanif(){

		//musanifIndex

		$musanifIndex = $this->input->post('musanifIndex');
		$musanifIndex++;

		$this->load->view('new_musanif',array('a'=>$musanifIndex));

	}

	

	public function removePhone()

	{

		$phoneid = $this->input->post('phoneid');

		$this->db->delete('temp_main_phone', array('phoneid' => $phoneid));		

	}
	
	public function barcode($code)
	{
		$text = $code;
		$size = "60";
		$orientation = "horizontal";
		$code_type = "code128";
		$code_string = $code;
	
		// Translate the $text into barcode the correct $code_type
		if ( strtolower($code_type) == "code128" ) {
			$chksum = 104;
			// Must not change order of array elements as the checksum depends on the array's key to validate final code
			$code_array = array(" "=>"212222","!"=>"222122","\""=>"222221","#"=>"121223","$"=>"121322","%"=>"131222","&"=>"122213","'"=>"122312","("=>"132212",")"=>"221213","*"=>"221312","+"=>"231212",","=>"112232","-"=>"122132","."=>"122231","/"=>"113222","0"=>"123122","1"=>"123221","2"=>"223211","3"=>"221132","4"=>"221231","5"=>"213212","6"=>"223112","7"=>"312131","8"=>"311222","9"=>"321122",":"=>"321221",";"=>"312212","<"=>"322112","="=>"322211",">"=>"212123","?"=>"212321","@"=>"232121","A"=>"111323","B"=>"131123","C"=>"131321","D"=>"112313","E"=>"132113","F"=>"132311","G"=>"211313","H"=>"231113","I"=>"231311","J"=>"112133","K"=>"112331","L"=>"132131","M"=>"113123","N"=>"113321","O"=>"133121","P"=>"313121","Q"=>"211331","R"=>"231131","S"=>"213113","T"=>"213311","U"=>"213131","V"=>"311123","W"=>"311321","X"=>"331121","Y"=>"312113","Z"=>"312311","["=>"332111","\\"=>"314111","]"=>"221411","^"=>"431111","_"=>"111224","\`"=>"111422","a"=>"121124","b"=>"121421","c"=>"141122","d"=>"141221","e"=>"112214","f"=>"112412","g"=>"122114","h"=>"122411","i"=>"142112","j"=>"142211","k"=>"241211","l"=>"221114","m"=>"413111","n"=>"241112","o"=>"134111","p"=>"111242","q"=>"121142","r"=>"121241","s"=>"114212","t"=>"124112","u"=>"124211","v"=>"411212","w"=>"421112","x"=>"421211","y"=>"212141","z"=>"214121","{"=>"412121","|"=>"111143","}"=>"111341","~"=>"131141","DEL"=>"114113","FNC 3"=>"114311","FNC 2"=>"411113","SHIFT"=>"411311","CODE C"=>"113141","FNC 4"=>"114131","CODE A"=>"311141","FNC 1"=>"411131","Start A"=>"211412","Start B"=>"211214","Start C"=>"211232","Stop"=>"2331112");
			$code_keys = array_keys($code_array);
			$code_values = array_flip($code_keys);
			for ( $X = 1; $X <= strlen($text); $X++ ) {
				$activeKey = substr( $text, ($X-1), 1);
				$code_string .= $code_array[$activeKey];
				$chksum=($chksum + ($code_values[$activeKey] * $X));
			}
			$code_string .= $code_array[$code_keys[($chksum - (intval($chksum / 103) * 103))]];
	
			$code_string = "211214" . $code_string . "2331112";
		} elseif ( strtolower($code_type) == "code39" ) {
			$code_array = array("0"=>"111221211","1"=>"211211112","2"=>"112211112","3"=>"212211111","4"=>"111221112","5"=>"211221111","6"=>"112221111","7"=>"111211212","8"=>"211211211","9"=>"112211211","A"=>"211112112","B"=>"112112112","C"=>"212112111","D"=>"111122112","E"=>"211122111","F"=>"112122111","G"=>"111112212","H"=>"211112211","I"=>"112112211","J"=>"111122211","K"=>"211111122","L"=>"112111122","M"=>"212111121","N"=>"111121122","O"=>"211121121","P"=>"112121121","Q"=>"111111222","R"=>"211111221","S"=>"112111221","T"=>"111121221","U"=>"221111112","V"=>"122111112","W"=>"222111111","X"=>"121121112","Y"=>"221121111","Z"=>"122121111","-"=>"121111212","."=>"221111211"," "=>"122111211","$"=>"121212111","/"=>"121211121","+"=>"121112121","%"=>"111212121","*"=>"121121211");
	
			// Convert to uppercase
			$upper_text = strtoupper($text);
	
			for ( $X = 1; $X<=strlen($upper_text); $X++ ) {
				$code_string .= $code_array[substr( $upper_text, ($X-1), 1)] . "1";
			}
	
			$code_string = "1211212111" . $code_string . "121121211";
		} elseif ( strtolower($code_type) == "code25" ) {
			$code_array1 = array("1","2","3","4","5","6","7","8","9","0");
			$code_array2 = array("3-1-1-1-3","1-3-1-1-3","3-3-1-1-1","1-1-3-1-3","3-1-3-1-1","1-3-3-1-1","1-1-1-3-3","3-1-1-3-1","1-3-1-3-1","1-1-3-3-1");
	
			for ( $X = 1; $X <= strlen($text); $X++ ) {
				for ( $Y = 0; $Y < count($code_array1); $Y++ ) {
					if ( substr($text, ($X-1), 1) == $code_array1[$Y] )
						$temp[$X] = $code_array2[$Y];
				}
			}
	
			for ( $X=1; $X<=strlen($text); $X+=2 ) {
				if ( isset($temp[$X]) && isset($temp[($X + 1)]) ) {
					$temp1 = explode( "-", $temp[$X] );
					$temp2 = explode( "-", $temp[($X + 1)] );
					for ( $Y = 0; $Y < count($temp1); $Y++ )
						$code_string .= $temp1[$Y] . $temp2[$Y];
				}
			}
	
			$code_string = "1111" . $code_string . "311";
		} elseif ( strtolower($code_type) == "codabar" ) {
			$code_array1 = array("1","2","3","4","5","6","7","8","9","0","-","$",":","/",".","+","A","B","C","D");
			$code_array2 = array("1111221","1112112","2211111","1121121","2111121","1211112","1211211","1221111","2112111","1111122","1112211","1122111","2111212","2121112","2121211","1121212","1122121","1212112","1112122","1112221");
	
			// Convert to uppercase
			$upper_text = strtoupper($text);
	
			for ( $X = 1; $X<=strlen($upper_text); $X++ ) {
				for ( $Y = 0; $Y<count($code_array1); $Y++ ) {
					if ( substr($upper_text, ($X-1), 1) == $code_array1[$Y] )
						$code_string .= $code_array2[$Y] . "1";
				}
			}
			$code_string = "11221211" . $code_string . "1122121";
		}
	
		// Pad the edges of the barcode
		$code_length = 20;
		for ( $i=1; $i <= strlen($code_string); $i++ )
			$code_length = $code_length + (integer)(substr($code_string,($i-1),1));
	
		if ( strtolower($orientation) == "horizontal" ) {
			$img_width = $code_length;
			$img_height = $size;
		} else {
			$img_width = $size;
			$img_height = $code_length;
		}
	
		$image = imagecreate($img_width, $img_height);
		$black = imagecolorallocate ($image, 0, 0, 0);
		$white = imagecolorallocate ($image, 255, 255, 255);
	
		imagefill( $image, 0, 0, $white );
	
		$location = 10;
		for ( $position = 1 ; $position <= strlen($code_string); $position++ ) {
			$cur_size = $location + ( substr($code_string, ($position-1), 1) );
			if ( strtolower($orientation) == "horizontal" )
				imagefilledrectangle( $image, $location, 0, $cur_size, $img_height, ($position % 2 == 0 ? $white : $black) );
			else
				imagefilledrectangle( $image, 0, $location, $img_width, $cur_size, ($position % 2 == 0 ? $white : $black) );
			$location = $cur_size;
		}
		// Draw barcode to the screen
		header ('Content-type: image/png');
		imagepng($image);
		imagedestroy($image);
	}		

}

}



?>
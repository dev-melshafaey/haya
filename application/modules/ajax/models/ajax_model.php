<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ajax_model extends CI_Model {
	
	/*
	* Properties
	*/
	private $_table_users;	
	private $_table_donate;	
	private $_table_applied_donation;
//----------------------------------------------------------------------

	function __construct()
    {
        parent::__construct();
    }
	
	public function allUsers()
	{		
		$this->db->select('ah_userprofile.fullname, ah_users.userlogin AS user_name, ah_branchs.branchname AS branch_name');
        $this->db->from('ah_users');
        $this->db->join('ah_branchs','ah_users.branchid=ah_branchs.branchid');
		$this->db->join('ah_userprofile','ah_users.userid=ah_userprofile.userid');
		$this->db->where('ah_users.userroleid != ',19);
        $this->db->order_by("ah_userprofile.fullname", "ASC");
		$query = $this->db->get();		
		if($query->num_rows() > 0)
		{	return $query->result();	}
	}
	

	function get_appointment()
	{		
		$this->db->select('ah_userprofile.fullname, appointment.province AS REIGONNAME, appointment.wilaya AS WILAYATNAME, appointment.name, appointment.appoid, appointment.appointmentdate, appointment.appointstatus, appointment.appointpriority');
		$this->db->from('ah_userprofile');		
		$this->db->join('appointment','ah_userprofile.userid=appointment.addedby');
		$this->db->join('ah_listmanagement','ah_listmanagement.list_id=appointment.province');
		$this->db->where('appointment.appointstatus != ','done');
		$this->db->where('DATE(appointment.appointmentdate)',date('Y-m-d'));
		$this->db->where('appointment.appointmentuser',$this->session->userdata('userid'));		
		$this->db->order_by("appointment.appointpriority", "ASC");		
		$query = $this->db->get();		
		if($query->num_rows() > 0)
		{	return $query->result();	}

	}
	
	function appointment($id)
	{
		$this->db->select('ah_userprofile.fullname, appointment.province AS REIGONNAME, appointment.wilaya AS WILAYATNAME, appointment.name, appointment.subject, appointment.phonenumber, appointment.email,  appointment.appoid, appointment.appointmentdate, appointment.appointstatus, appointment.appointpriority');
		$this->db->from('ah_userprofile');		
		$this->db->join('appointment','ah_userprofile.userid=appointment.addedby');
		$this->db->join('ah_listmanagement','ah_listmanagement.list_id=appointment.province');
		$this->db->where('appointment.appoid',$id);	
		$query = $this->db->get();		
		if($query->num_rows() > 0)
		{	return $query->row();	} 		
	}
}

?>
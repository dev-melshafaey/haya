<?PHP 
$fullname = $r[0]->applicant_first_name.' '.$r[0]->applicant_middle_name.' '.$r[0]->applicant_last_name.' '.$r[0]->applicant_sur_name; 
$sx = $s+1;
?>
<div class="col-md-12" style="background-color:#FFF !important; padding: 4px 16px;">
<h4><?PHP echo $fullname; ?> (<?PHP echo arabic_date(applicant_number($r[0]->applicant_id)); ?>)</h4>
</div>
<?PHP
foreach(timelinesteps() as $modulekey => $moduleid) 
{ 
	$minfo = get_module_name_icon($moduleid);
	if($sx>=$modulekey)
	{
		$class_circle = 'greencircle';
		$class_line = 'greenline';
	}
	else
	{
		$class_circle = 'graycircle';
		$class_line = 'grayline';
	}	
?>
<div class="col-md-3 <?PHP echo $class_line; ?>">
  <div class="<?PHP echo $class_circle; ?> col-md-1"><i class="<?PHP echo $minfo[0]->module_icon; ?> iconsize"></i></div>
  <div class="greenarrow col-md-1"><i class="icon-double-angle-left iconsize_small"></i></div>
  <h5 class="namecircle"><?PHP echo $minfo[0]->module_name; ?></h5>
</div>
<?PHP } ?>
<br clear="all">
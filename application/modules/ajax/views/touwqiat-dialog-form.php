<?PHP 
$fullname = $r->applicant_first_name.' '.$r->applicant_middle_name.' '.$r->applicant_last_name.' '.$r->applicant_sur_name; 
$sx = $s+1;
$touqyatid = json_decode($r->touqyatid,TRUE);
?>
<p align="center"><img src="<?php echo base_url(); ?>images/proton-logo.png" width="96" height="86"><br />
  <strong>إسم المراجع:</strong> <span><?php echo $fullname; ?></span><br />
  <strong>رقم التسجيل:</strong> <span><?PHP echo arabic_date(applicant_number($r->applicant_id)); ?></span></p>
<div class="col-md-12" style="background-color: #FFF; padding-top: 10px;">
<form name="frmmustakhdeem" id="frmmustakhdeem" method="post">
<input type="hidden" id="applicant_id" name="applicant_id" value="<?PHP echo $r->applicant_id; ?>">
<input type="hidden" id="applicant_name" name="applicant_name" value="<?PHP echo $fullname; ?>">
  <div class="col-md-6">
    <h4 class="section-title">جميع المستخدمين</h4>
    <div class="multibox">
      <?php $users	=	$this->ajax->get_all_users();?>
      <?php if(!empty($users)):?>
      <?php foreach($users	as $user):?>
      <div class="col-md-11">
        <input id="inquirytypeid" <?PHP if(in_array($user->id,$touqyatid)) { ?> checked <?PHP } ?> type="checkbox" name="user_role_id[]" class="usertoqyat" value="<?php echo $user->id;?>">
        <?php echo $user->firstname;?> </div>
      <?php endforeach;?>
      <?php endif;?>
    </div>
  </div>
  <div class="col-md-6">
    <h4 class="section-title">تحميل</h4>
    <div id="drag0" class="tahleelfileuploader">
      <div class="browser">
        <input accept="application/pdf" type="file" id="tahleelfilex" name="tahleelfilex" title="تحميل">
        <input type="hidden" name="tahleelfile" id="tahleelfile" value="<?PHP echo $r->tahleelfile; ?>">
      </div>
    </div>
    <?PHP if($r->tahleelfile) { ?>
    <div class="col-md-11 setMarginx" style="text-align:center;">
    	<a target="_blank" href="<?PHP echo base_url().'upload_files/documents/touqyat/'.$r->tahleelfile; ?>"><?PHP echo $r->tahleelfile; ?></a>
    </div>
    <?PHP } ?>
    <div class="col-md-11 setMarginx" id="upmessage" style="text-align:right; color:#064E01; font-size:16px; font-weight:bold;"></div>
  </div>
  
  <div class="col-md-11 setMarginx" id="tahleelloader">
    <button type="button" id="add_altowdiat" class="btn btn-success">حفظ</button>
  </div>
  <br clear="all">
</div>
</form>
<script>
$(function(){
	
	 $('#add_altowdiat').click(function(){
		 	$('#upmessage').html(' ');
			var touqyatCount = $('.usertoqyat:checked').length;
			var tahleelfile = $('#tahleelfile').val();
			if(touqyatCount > 0 && tahleelfile!='')
			{				
				$.ajax({
					url: config.BASE_URL+'inquiries/updateToqyatData/',
					type: "POST",
					data:$('#frmmustakhdeem').serialize(),
					dataType: "html",
					beforeSend: function(){	$('#tahleelloader').html('<img src="'+config.BASE_URL+'images/hourglass.gif">'); },
					success: function(msg) {
						location.href = config.BASE_URL+'inquiries/requestphasefive/'+$('#applicant_id').val();
					}
				});
			}
			else
			{
				$('#upmessage').html('فضلاً قم باختيار ملف واحد  وشخص واحد على الأقل');
			}
	  });
	 $('.tahleelfileuploader').dmUploader({
        url: config.BASE_URL + 'inquiries/uploadTouqyat',
        dataType: 'json',
        allowedTypes: 'application/pdf',
        onInit: function () {},
        onBeforeUpload: function (id) {	check_my_session();	},
        onNewFile: function (id, file) {
            idd = $(this).attr('id');          
            demoFile = '#tahleelfilex';           
            $.danidemo.addFile(demoFile, id, file);	 },
        onComplete: function () {},
        onUploadProgress: function (id, percent) {},
        onUploadSuccess: function (id, data) {	$('#tahleelfile').val(data.filename);	$('#upmessage').html('تم رفع الملاف بنجاح'); },
        onUploadError: function (id, message) {
            $.danidemo.updateFileStatus(id, 'error', message);
			show_notification_error_end('فشل في تحميل ملف');
        },
        onFileTypeError: function (file) {
			show_notification_error_end('ملف \'' + file.name + '\' لا يمكن إضافة : يجب أن يكون صورة');
        },
        onFileSizeError: function (file) {
			show_notification_error_end('ملف \'' + file.name + '\' لا يمكن إضافة : حجم الفائض الحد');
        },
        onFallbackMode: function (message) {
			show_notification_error_end('المتصفح غير معتمد ( تفعل شيئا آخر هنا !): ' + message);
			
        }
    });
});

function clear_space()
{
	
}
</script>
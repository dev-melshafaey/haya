<form name="frm_appointment_ajax" id="frm_appointment_ajax" method="post" action="<?PHP base_url(); ?>ajax/addappointment" autocomplete="off">
  <?PHP $ap = $app; ?>
  <?php //echo '<pre>'; print_r($ap);?>
  <input type="hidden" name="appoid" id="appoid" value="<?PHP echo $ap->appoid; ?>">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group col-md-4">
        <label class="text-warning">الاسم :</label>
        <input name="name" value="<?PHP echo $ap->name; ?>" placeholder="الاسم" id="name" type="text" class="req form-control">
      </div>
      <div class="form-group col-md-4">
        <label class="text-warning">موضوع :</label>
        <input name="subject" placeholder="موضوع" value="<?PHP echo $ap->subject; ?>" id="subject" type="text" class="req form-control">
      </div>
      <div class="form-group col-md-4">
        <label class="text-warning">رقم الهاتف :</label>
        <input name="phonenumber" value="<?PHP echo $ap->phonenumber; ?>" placeholder="رقم الهاتف" maxlength="8" id="phonenumber" type="text" class="req form-control NumberInput">
      </div>
      <div class="form-group col-md-4">
        <label class="text-warning">البريد الإلكتروني :</label>
        <input name="email" value="<?PHP echo $ap->email; ?>" placeholder="البريد الإلكتروني" id="email" type="text" class="form-control">
      </div>
      <div class="form-group col-md-4">
        <label class="text-warning">اختر المحافظة :</label>
        <?PHP echo $this->haya_model->create_dropbox_list('province','regions',$ap->REIGONNAME,0,'req'); ?>
      </div>
      <div class="form-group col-md-4">
        <label class="text-warning">الولاية :</label>
        <?PHP echo $this->haya_model->create_dropbox_list('wilaya','wilaya',$ap->WILAYATNAME,$ap->REIGONNAME,'req'); ?>
      </div>
      <div class="form-group col-md-4">
        <label class="text-warning">تاريخ موعد :</label>
        <input name="appointmentdate"  value="<?PHP echo date('Y-m-d',strtotime($ap->appointmentdate)); ?>" placeholder="تاريخ موعد" id="appointmentdate" type="text" class="req form-control appointment dateinput">
      </div>
      <?PHP if($ap->appoid!='') { ?>
      <div class="form-group col-md-4">
        <label class="text-warning">اعادة التوجية :</label>
        <?PHP echo appointment_status(8,'appointmentuser',$ap->appointmentuser); ?>
      </div>
      <div class="form-group col-md-4">
        <label class="text-warning">الرسال رساله:</label><br>
        <input type="checkbox" name="sendmessage" value="1">
      </div>
      <?PHP } ?>
      <br clear="all">
      <div class="form-group col-md-6">
        <label class="text-warning">التفاصيل موعد :</label>
        <textarea name="appodetail" id="appodetail" placeholder="التفاصيل موعد" class="req form-control exTextarea"><?PHP echo $ap->appodetail; ?></textarea>
      </div>
      <div class="form-group col-md-6">
        <label class="text-warning">ملاحظات (توجيهات الرئيس التنفيذي)  :</label>
        <textarea name="appointmentnotes" id="appointmentnotes" placeholder="ملاحظات" class="req form-control exTextarea"><?PHP echo $ap->appointmentnotes; ?></textarea>
      </div>
      <div class="form-group col-md-4">
        <label class="text-warning">أفضلية :</label>
        <?PHP appointment_status(1,'appointpriority',$ap->appointpriority); ?>
      </div>
      
      <?PHP if($ap->appoid!='') { ?>
      <div class="form-group col-md-4">
        <label class="text-warning">الحالات :</label>
        <?PHP appointment_status(2,'appointstatus',$ap->appointstatus); ?>
      </div>
     
      <div class="form-group col-md-4" id="venuex" <?PHP if($ap->appointstatus=='sms') { echo('style="display:block !important;"'); } else {  echo('style="display:none !important;"');  } ?>>
      <textarea name="venue" id="venue" placeholder="الرسال رسالة" class="form-control exTextarea"><?PHP echo $ap->venue; ?></textarea>
      </div>
      <?PHP } ?>
      <br clear="all">
      <div class="form-group col-md-12">
      <button type="button" style="display:none;" id="appointmentloader" class="btn btn-success"><img src="<?PHP echo base_url(); ?>images/hourglass.gif"></button>
        <button type="button" onClick="save_appointment();" id="appointmentbutton" class="btn btn-success"><i class="icon-save"></i> حفظ </button>
      </div>
    </div>
  	<div class="12" style="margin-top:10px;">
    <div class="col-md-12" style="background-color:#FFF !important; padding:5px 0px !important; margin-bottom: 5px;">
                  <div class="col-md-5">
                    <div class="col-md-2 text-warning">أفضلية:</div>
                    <?PHP foreach(appointment_status(6) as $priority => $pvalue) { 
										if($priority==1)
										{	$color = "class1";	}
										else if($priority==2)
										{	$color = "class2";	}
										else
										{	$color = "class3";	}
								?>
                    <div class="col-md-3"><i style="font-size: 13px;" class="icon-circle <?PHP echo $color; ?>"> <?PHP echo $pvalue; ?></i></div>
                    <?PHP } ?>
                  </div>
                  <div class="col-md-7">
                    <div class="col-md-2 text-warning">الحالات:</div>
                    <?PHP foreach(appointment_status(7) as $statuskey => $statusvalue) {
							if($statuskey!='sms') {  ?>
                    <div class="col-md-2"><i  style="font-size: 13px;" class="icon-flag <?PHP echo $statuskey; ?>"> <?PHP echo $statusvalue; ?></i></div>
                    <?PHP }  } ?>
                  </div>
                  <br clear="all">
                </div>
    	<table class="table table-bordered table-striped dataTable" id="ajaxTable" >
                <thead>
                  <tr role="row">
                    <th style="text-align:center;">الحالات/أفضلية</th>
                    <th style="text-align:center;">تاريخ موعد</th>
                    <th style="text-align:center;">التفاصيل موعد</th>
                    <th style="text-align:center;">ملاحظات</th>
                
                  </tr>
                </thead>
                <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?PHP 
			
				foreach($appointments as $apn) { ?>
                 <tr role="row">
                    <td style="text-align:center;"><?PHP echo $apn['status']; ?></td>
                    <td style="text-align:center;"><?PHP echo $apn['appointmentdate']; ?></td>
                    <td style="text-align:center;"><?PHP echo $apn['appodetail']; ?></td>
                    <td style="text-align:center;"><?PHP echo $apn['appointmentnotes']; ?></td>
                
                  </tr>
                <?PHP } ?>
                </tbody>
              </table>
    </div>
  </div>
  
 <script>
 	$(function(){
		$( "#appointmentdate" ).datepicker({
			 showAnim:'slide',
			  minDate: -0, 
			  maxDate: "+2M +10D",
			  dateFormat:'yy-mm-dd'});	
	});
 </script> 
</form>
<?php $this->load->view('common/ajaxfooter'); ?>
<div class="row rowBorder ms<?php echo $a ?>" style="padding-right: 35px; padding-top: 11px;  padding-left: 35px; border-top:1px solid #ddd;">
  <input type="hidden" name="document_file[]" id="document_file<?php echo $a; ?>" value="<?php echo rQuote($study_analysis_demand->document_file); ?>">
  <div class="col-md-4 form-group">
    <label class="text-warning">الجهة التمويلية:</label>
    <input name="financing[]" id="financing"  placeholder="الجهة التمويلية" type="text" class="ssForm form-control ">
  </div>
  <div class="col-md-4 form-group">
    <label class="text-warning">مبلغ القرض:</label>
    <input name="loan_amount[]" id="loan_amount"  placeholder="مبلغ القرض" type="text" class="ssForm form-control NumberInput">
  </div>
  <div class="col-md-3 form-group">
    <label class="text-warning">المبلغ المسدد:</label>
    <input name="amount_paid[]" id="amount_paid"  placeholder="المبلغ المسدد" type="text" class="ssForm form-control NumberInput">
  </div>
  <div class="col-md-1 form-group">
    <label class="text-warning"></label>
    <button onclick="removeMusanif('<?php echo $a ?>')" class="btn btn-danger" id="remove" value="" type="button" style="float:left !important; margin-top: 22px;"><i class="icon-remove"></i> حذف</button>
  </div>
  <div class="col-md-4 form-group">
    <label class="text-warning">المتبقي:</label>
    <input name="residual[]" id="residual"  placeholder="المتبقي" type="text" class="ssForm form-control">
  </div>
  <div class="col-md-4 form-group">
    <label class="text-warning">القسط الشهري:</label>
    <input name="monthly_installment[]" id="monthly_installment"  placeholder="القسط الشهري" type="text" class="ssForm form-control">
  </div>
  <div class="col-md-4 form-group">
    <label class="text-warning">الملاحظات:</label>
    <textarea name="musanif_notes[]" id="notes" placeholder="الملاحظات"  class="ssForm form-control" ></textarea>
  </div>
  <div class="col-md-11 form-group uploader" id="drag<?php echo $a ?>">
    <label class="text-warning">تحميل:</label>
    <div class="browser">
      <input type="file" name="files[]" multiple title='تحميل'>
    </div>
  </div>
</div>
<script>
counter = '<?php echo $counter; ?>';
$('.uploader').dmUploader({
					url: config.BASE_URL+'inquiries/uploadFile',
					dataType: 'json',
					allowedTypes: 'image/*',
					/*extFilter: 'jpg;png;gif',*/
					onInit: function(){
					 idd = $(this).attr('id');
					 console.log(idd);
					 id_index = idd.substring(4,5);
					 // $.danidemo.addLog('#demo-debug', 'default', 'Plugin initialized correctly');
					},
					onBeforeUpload: function(id){
					 // $.danidemo.addLog('#demo-debug', 'default', 'Starting the upload of #' + id);

					  //$.danidemo.updateFileStatus(id, 'default', 'Uploading...');
					},
					onNewFile: function(id, file){
						//alert(id);
						demoFile = '#demo-files'+id_index;
						id = idd.substring(4,5);
						//alert(id);
					  $.danidemo.addFile(demoFile, id, file);
					},
					onComplete: function(){
					 // $.danidemo.addLog('#demo-debug', 'default', 'All pending tranfers completed');
					},
					onUploadProgress: function(id, percent){
					  var percentStr = percent + '%';
						id = idd.substring(4,5);
						//alert(id);
					  $.danidemo.updateFileProgress(id, percentStr);
					},
					onUploadSuccess: function(id, data){
					 // $.danidemo.addLog('#demo-debug', 'success', 'Upload of file #' + id + ' completed');

					  //$.danidemo.addLog('#demo-debug', 'info', 'Server Response for file #' + id + ': ' + JSON.stringify(data));

					 // $.danidemo.updateFileStatus(id, 'success', 'Upload Complete');
						id = idd.substring(4,5);
						//alert(id);
				
					  $.danidemo.updateFileProgress(id, '100%');
						ff = data;	
						//$("#document_file").val(ff.filename);
						$("#document_file"+id).val(ff.filename);
						//createElement(counter,ff.filename);
					 console.log(JSON.stringify(data));
					},
					onUploadError: function(id, message){
					  $.danidemo.updateFileStatus(id, 'error', message);

					  $.danidemo.addLog('#demo-debug', 'error', 'Failed to Upload file #' + id + ': ' + message);
					},
					onFileTypeError: function(file){
					  $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: must be an image');
					},
					onFileSizeError: function(file){
					  $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: size excess limit');
					},
					/*onFileExtError: function(file){
					  $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' has a Not Allowed Extension');
					},*/
					onFallbackMode: function(message){
					  $.danidemo.addLog('#demo-debug', 'info', 'Browser not supported(do something else here!): ' + message);
					}
				  });
</script>
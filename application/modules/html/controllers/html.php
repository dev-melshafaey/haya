<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Html extends CI_Controller {

	/*
	 * Properties
	 */
	private $_data			=	array();
	private $_login_userid	=	NULL;

//-----------------------------------------------------------------------

	/*
	 * Constructor
	 */
	function __construct()
	{
		 parent::__construct();		
	}
//-----------------------------------------------------------------------

	/*
	 * Constructor
	 */
	function index()
	{
		$this->load->view('html');
	}
	
	function add()
	{
		$this->load->view('html2');
	}
	
	function update()
	{
		$this->load->view('html3');
	}
//-----------------------------------------------------------------------
/* End of file yateem.php */
/* Location: ./application/modules/yateem/yateem.php */
}
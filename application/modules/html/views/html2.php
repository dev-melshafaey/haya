<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Receipt</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
    <style>
        html, body {overflow-x: hidden;}
        .recty {border-radius:2em;border-color:#000;}
        .recty img {margin:1em auto; display:block;}
        .topbox {border: 1px solid #000;margin: 1em auto; display: block;width: 32%;padding: 1em;font-size: 16px;font-weight: bold;}
        input {border-top:none;border-left:none;border-right:none;border-bottom:1px dotted #333;text-align: right;}
        .form-control {border-top:none;border-left:none;border-right:none;border-bottom:1px dotted #333;box-shadow: none; border-radius: 0;}
        .form-control:focus {box-shadow:none;}
        ul.list-inline-row {float: left;list-style: none;}
        ul.list-inline-row li {float: left;}
        .table {
            margin: 1.5em auto;
            max-width: 95%;
            width: 100%;
        }
        table tr th {text-align: center;font-size: 16px;}
        .table-bordered input {border-bottom: none;}
        label {line-height: 3em;text-align: right; padding-right: 6em !important;}
        .table-bordered {border: 1px solid #000;} 
        .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {border: 1px solid #000 !important;}
    </style>
  <body>
<!-- Row start -->
      <div class="container">
        <div class="row">
            <div class="col-sm-12">    
                <div class="panel panel-default recty">
                   <img src="<?php echo base_url();?>assets/images/logox.png" class="img-responsive" >
                </div>
                <div class="panel panel-default recty">
                   <div class="row">  
                       <div class="topbox">
                            <ul class="list-inline">
                                <li>( <input name="" type="text" class=""> صندوق)</li>
                                <li>سند صرف</li>
                            </ul> 
                       </div>
                   </div>
                   <div class="row">  
                       <div class="col-sm-6">
                           <div class="pull-left" style="margin-left:2em;">
                             <ul class="list-inline">
                                <li><input name="" type="text" class=""></li>
                                <li>: الرقم</li>
                             </ul> 
                           </div>
                       </div>
                       <div class="col-sm-6">
                           <div class="pull-right" style="margin-right:15em;">No.</div>
                       </div>
                   </div>
                   <div class="row"> 
                       <div class="col-sm-6">
                           <div class="pull-left" style="margin-left:2em;">
                             <ul class="list-inline">
                                <li><input name="" type="text" class=""></li>
                                <li>: التاريخ</li>
                             </ul> 
                           </div>
                       </div> 
                       <div class="col-sm-6"></div>
                   </div>
                   <div class="row">
                       <div class="col-sm-12">
                           <div class="form-group">
                                <div class="col-xs-10">
                                    <input type="text" class="form-control" id="" placeholder="">
                                </div>
                                <label for="inputEmail" class="control-label col-xs-2">: اسم المستفيد</label>
                            </div>
                       </div>
                   </div>
                   <div class="row">
                       <div class="col-sm-12">
                           <div class="form-group">
                                <div class="col-xs-10">
                                    <input type="text" class="form-control" id="" placeholder="">
                                </div>
                                <label for="inputEmail" class="control-label col-xs-2">: عنوان المستفيد</label>
                            </div>
                       </div>
                   </div>
                   <div class="row">
                     
                     <div class="col-sm-6">
                     <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th colspan="2">المبلغ </th>
                            </tr>
                            <tr>
                                <th>ريال  </th>
                                <th>بيسة  </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                        </tbody>
                    </table> 
    
                     </div>
                     <div class="col-sm-6">
                        <table class="table table-bordered">
                            <thead>
                                <tr style="height:5.5em;">
                                    <th>البيان </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="text" class="form-control" id="" placeholder=""></td>
                                </tr>
                                <tr>
                                    <td><input type="text" class="form-control" id="" placeholder=""></td>
                                </tr>
                                <tr>
                                    <td><input type="text" class="form-control" id="" placeholder=""></td>
                                </tr>
                                <tr>
                                    <td><input type="text" class="form-control" id="" placeholder=""></td>
                                </tr>
                                <tr>
                                    <td><input type="text" class="form-control" id="" placeholder=""></td>
                                </tr>
                                <tr>
                                    <td><input type="text" class="form-control" id="" placeholder=""></td>
                                </tr>
                                <tr>
                                    <td><input type="text" class="form-control" id="" placeholder=""></td>
                                </tr>
                             </tbody>
                        </table> 
                    </div>
                   </div>
                   <div class="row">
                       <div class="col-sm-12">
                           <div class="form-group">
                                <div class="col-xs-10">
                                    <input type="text" class="form-control" id="" placeholder="">
                                </div>
                                <label for="inputEmail" class="control-label col-xs-2">: المبلغ بالحروف</label>
                            </div>
                       </div>
                   </div>
                   <div class="row">
                       <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>مسحوب على</th>
                                <th>تاريخ الشيك  / التحويل </th>
                                <th>رقم الشيك / التحويل </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                        </tbody>
                       </table> 
                   </div>
                   <div class="row">
                   <p style="text-align:right;margin-right:2em;"> استلمت المبلغ نقدا / الشيك / خطاب التحويل الموضحة بياناته أعلاه </p>
                   </div>
                   <div class="row">
                    <div class="col-sm-6">
                     <div class="pull-left" style="margin-left:1.5em;">
                        <ul class="list-inline">
                                <li><input name="" type="text" class=""></li>
                                <li>: الإسم </li>
                             </ul> 
                         <ul class="list-inline">
                                <li><input name="" type="text" class=""></li>
                                <li>: التاريخ</li>
                             </ul> 
                         <ul class="list-inline">
                                <li><input name="" type="text" class=""></li>
                                <li>: التوقيع</li>
                             </ul> 
                     </div>
                    </div>
                    <div class="col-sm-6">
                     <div class="pull-right" style="text-align:center;margin-right:2em;">عادة
                        <br><input type="text" class="form-control" id="" placeholder=""></div>
                    </div>
                   </div>
                   <div class="row">
                   <p style="text-align:center;">أعتماد الرئيس التنفيذي </p>
                   </div>    
                </div>
            </div>
        </div>
      </div>
<!-- Row end -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  </body>
</html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Receipt</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <style>
	.rec input {border-top:none;border-left:none;border-right:0;border-bottom:1px dotted #000;}
	.rec .form-control {box-shadow:none;border-radius:0;}
	.rec p {float:left;}
	.rect {border-radius:2em;overflow:auto;border-color:#000;}
	.rect img {margin:1em auto; display:block;}
	.receipt {line-height:3em;padding:2em 0;}
	.rec ul {float:left;list-style:none;}
	.rec ul li {float:left;}
	.receipt {border-radius:2em;overflow:auto;border-color:#000;}
	.receipt h1 {text-align:center;}
	.rtnum {float:right; margin:1em;}
	.receipt img {margin:1em auto; display:block;}
	.sqrate {border:1px solid #000;margin:1.5em;text-align:center;}
	.sqrate thead tr td, .sqrate tbody tr td  {border:1px solid #000;}
  </style>
  <body>
    <!-- Row start -->
<div class="container rec">
  <div class="row">
    <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="panel panel-default rect">
	   <img src="<?php echo base_url();?>assets/images/logox.png" >
      </div>
	  <div class="panel panel-default receipt">
	    <div class="col-sm-4">
			<table class="sqrate">
				<thead>
				 <tr>
					<td>R.O</td>
					<td>Bz.</td>
				 </tr>
				</thead>
				<tbody>
					<td><input type="text"></td>
					<td><input type="text"></td>
				</tbody>
			</table>
		</div>
		<div class="col-sm-4"><h1>Receipt Voucher</h1></div>
		<div class="col-sm-4">
		    <div class="rtnum"><strong>No:</strong> 10263<br><br>
			Date: <input type="text"> التاريخ</div>
		</div>
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-12">
				<div class="col-sm-2">Received from Mr./M/s.</div>
				<div class="col-sm-8"><input type="text" class="form-control"></div>
				<div class="col-sm-2">Received from Mr./M/s.</div>
				</div>
		    </div>
			<div class="row">
				<div class="col-sm-12">
				<input type="text" class="form-control">
				</div>
		    </div>
			<div class="row">
				<div class="col-sm-12">
				<div class="col-sm-2">The Sum of Omani</div>
				<div class="col-sm-8"><input type="text" class="form-control"></div>
				<div class="col-sm-2">The Sum of Omani</div>
				</div>
		    </div>
			<div class="row">
				<div class="col-sm-12">
				<div class="col-sm-4"><table><tbody><tr><td>By Cash/Cheque No.</td> <td><input type="text" class="form-control" style="width:auto;"></td></tr></tbody></table></div>
				<div class="col-sm-4"><table><tbody><tr><td>Dated</td> <td><input type="text" class="form-control" style="width:auto;"></td> <td>التاريخ</td></tr></tbody></table></div>
				<div class="col-sm-4"><table><tbody><tr><td>Bank</td> <td><input type="text" class="form-control" style="width:auto;"></td> <td> بنك</td></tr></tbody></table></div>
				</div>
		    </div>
			<div class="row">
				<div class="col-sm-12">
				<div class="col-sm-2">Being</div>
				<div class="col-sm-8"><input type="text" class="form-control"></div>
				<div class="col-sm-2">Being</div>
				</div>
		    </div>
			<div class="row">
				<div class="col-sm-12">
				<div class="col-sm-4"></div>
				<div class="col-sm-4"></div>
				<div class="col-sm-4"><table><tbody><tr><td>Signature</td> <td><input type="text" class="form-control" style="width:auto;"></td> <td> التوقيع</td></tr></tbody></table></div>
				</div>
		    </div>
      </div>
	</div>
	<P>P.O.Box : 1998, Ruwi, Postal Code: 112, Tel.:24487998, Fax : 24487997</P>
  </div>
 </div>
  <!-- Row end -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  </body>
</html>
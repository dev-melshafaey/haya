<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Receipt</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
    <style>
        html, body {overflow-x: hidden;}
        .recty {border-radius:2em;border-color:#000;}
        .recty img {margin:1em auto; display:block;}
        .topbox {border: 1px solid #000;margin: 1em auto; display: block;width: 32%;padding: 1em;font-size: 16px;font-weight: bold;}
        input {border-top:none;border-left:none;border-right:none;border-bottom:1px dotted #333;text-align: right;}
        .form-control {border-top:none;border-left:none;border-right:none;border-bottom:1px dotted #333;box-shadow: none; border-radius: 0;}
        .form-control:focus {box-shadow:none;}
        ul.list-inline-row {float: left;list-style: none;}
        ul.list-inline-row li {float: left;}
        .table {
            margin: 1.5em auto;
            max-width: 95%;
            width: 100%;
        }
        table tr th {text-align: center;font-size: 16px;}
        .table-bordered input {border-bottom: none;}
        label {line-height: 3em;text-align: right; padding-right: 6em !important;}
        .table-bordered {border: 1px solid #000;} 
        .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {border: 1px solid #000 !important;}
        h1,p {text-align: right;margin-right: 1em;}
        .list-inline {margin-right: 5em;}
    </style>
  <body>
<!-- Row start -->
      <div class="container">
        <div class="row">
            <div class="col-sm-12">    
                <div class="panel panel-default recty">
                   <img src="<?php echo base_url();?>assets/images/logox.png" class="img-responsive" >
                </div>
                <div class="panel panel-default recty">
                    <div class="row">.</div>    
                   <div class="row">  
                       <div class="col-sm-6">
                          <div class="pull-left" style="margin-left:2em;">
                             <ul class="list-inline">
                                <li>No. O.C.O</li> 
                                <li><input name="" type="text" class=""></li>
                             </ul>
                             <ul class="list-inline">
                                <li>Date</li> 
                                <li><input name="" type="text" class=""></li>
                             </ul>
                           </div>
                       </div>
                       <div class="col-sm-6">
                            <div style="margin-right:1.5em;" class="pull-right">
                            <ul class="list-inline">
                                    <li><input type="text" class="" name=""></li>
                                    <li>: الرقم هه ع ا خ</li>
                                 </ul> 
                             <ul class="list-inline">
                                    <li><input type="text" class="" name=""></li>
                                    <li>: التاريخ</li>
                                 </ul> 
                             <ul class="list-inline">
                                    <li><input type="text" class="" name=""></li>
                                    <li>: الموافق</li>
                             </ul> 
                           </div>
                       </div>
                   </div>
                   <div class="row">  
                       <div class="col-sm-6">
                           <h1>المحترم </h1>
                       </div>
                       <div class="col-sm-6">
                          <h1>الفاضل / مدير بنك مسقط </h1>
                       </div>
                   </div>
                   <div class="row"> 
                       <div class="col-sm-12">
                           <h1>فرى الحي التجاري - سلطنة عمان - مسقط</h1>
                       </div> 
                   </div>
                    <div class="row"> 
                       <div class="col-sm-12">
                           <p>،،، اسلام عليكم و رحمة الله و بركاته</p>
                       </div> 
                   </div>
                   <div class="row"> 
                       <div class="col-sm-12">
                           <p>ير جى التكرم بالا يع ز لمن يلزم بتحويل مبلغ (<input type="text" class="" id="" placeholder="">) تسعة آلاف و مائتان و أربعة و ستون ريال عماني لا غير ،بالدينار الأردني على الحساب التالي  </p>
                       </div> 
                   </div>    
                   <div class="row">
                     
                     <div class="col-sm-12">
                     <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Account Holder :</td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                            <tr>
                                <td>Bank Name :</td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                            <tr>
                                <td>Branch :</td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                            <tr>
                                <td>Bank Address :</td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                            <tr>
                                <td>Swift Code :</td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                            <tr>
                                <td>Account No :</td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                            <tr>
                                <td>IBAN No :</td>
                                <td><input type="text" class="form-control" id="" placeholder=""></td>
                            </tr>
                        </tbody>
                    </table> 
    
                     </div>
                     
                   </div>
                   <div class="row">
                       <div class="col-sm-12">
                           <div class="form-group">
                                <div class="col-xs-8">
                                    <input type="text" class="form-control" id="" placeholder="">
                                </div>
                                <label for="inputEmail" class="control-label col-xs-4">: و ذلك خصما من حساب الهية طرفكم رقم</label>
                            </div>
                       </div>
                   </div>
                   <div class="row">
                       <div class="col-sm-12">
                           <ul class="list-inline pull-right">
                                    <li><input type="text" class="" name=""></li>
                                    <li>حي</li>
                                    <li><input type="text" class="" name=""></li>
                                    <li>قيمة مخصصات السنوية للمكتب الهية بقطاع غزة بدولة فلسطين لمدة سنة من</li>
                                    
                           </ul> 
                       </div>
                   </div>
                    <div class="row"> 
                       <div class="col-sm-12">
                           <div class="col-sm-6">
                               <span><br><br</span>
                               <span><br><br</span>
                               <h1>علي بن إبراهيم بن شنون الرئيسي <br>
								الرئيس التنفيذي </h1>
                           </div>
                           <div class="col-sm-6">
                               <span><br><br</span>
                               <span><br><br</span>
                               <h1>محمد بن حمود بن سعيد الذهلي <br>
								الرئيس التنفيذي المساعد </h1>
                           </div>
                       </div> 
                     </div>      
                </div>
            </div>
        </div>
      </div>
<!-- Row end -->
    <div class="container">
    <p class="pull-left">P.O.Box : 1998, Ruwi, - Postal Code 112 -  Tel: (+968) 24297960 - Fax : (+968) 24487997</p>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  </body>
</html>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends CI_Controller
{
	// Define Properties
	private $_data			=	array();
	private $_login_userid	=	NULL;
	
	public function __construct()
	{
		parent::__construct();
		$this->_data['module']			=	$this->haya_model->get_module();
		$this->_login_userid			=	$this->session->userdata('userid');
		$this->_data['login_userid']	=	$this->_login_userid;	
		$this->_data['user_detail'] 	= 	$this->haya_model->get_user_detail($this->_login_userid);
			
		$this->load->model('messages_model', 'mes');

	}
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/	
	public function newmessage()
	{
		$this->_data['fox'] = $this->mes->alluser();
		$this->load->view('newmessage',$this->_data);
	}
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/	
	public function get_filtered_users()
	{
		$rname	=	$this->input->post('rname');
		$html 	=	'';
		
		foreach($this->mes->alluser($rname) as $list)
		{
			$html .= '<div class="col-md-11 userxlist">';
        	$html .= '<input id="recevierid" type="checkbox" name="recevierid[]" value="'.$list->userid.'" /><label style="  margin-right: 4px;" class="text-warning">'.$list->fullname.'</label>';
			$html .= '</div>';
		}
		
		echo $html;		
	}
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/	
	public function getusername()
	{
		$term 	=	$this->input->get('term');
		$query 	=	$this->db->query("SELECT 
											userid, 
											CONCAT(fullname) as username 
											FROM 
											ah_userprofile 
											WHERE fullname LIKE '%".$term."%' 
											AND 
											userid!='".$this->_login_userid."' Order by fullname ASC");
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $data)
			{
				$arr[] = array('id'=>$data->userid,'label'=>$data->username,'value'=>$data->username);
			}
			echo json_encode($arr);
		}
	}
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/	
	public function addmessage()
	{
		$recevierid = $this->input->post('recevierid');
		$senderid 	= $this->_login_userid;
		$branchid 	= $this->haya_model->get_branch_id($senderid);
		$messageip 	= $_SERVER['REMOTE_ADDR'];
		$message 	= $this->input->post('message');
		
		foreach($recevierid as $rvid)
		{
			$data 		=	array('recevierid'=>$rvid,'senderid'=>$senderid,'branchid'=>$branchid,'messageip'=>$messageip,'message'=>$message);
			$json_data	=	json_encode($data);
			
			$this->db->insert('system_messages',$data,$json_data,$this->session->userdata('userid'));
		}
		
		$this->getMessages();
	}
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/	
	public function getMessages()
	{
		$user_role	=	$this->_data['user_detail']['profile']->userroleid;
		$manager_id	=	$this->_data['user_detail']['profile']->manager_id;
		$senderid 	=	$this->_login_userid;
		
		$this->db->select('ah_userprofile.fullname,system_messages.messid,system_messages.messagedate,system_messages.message,system_messages.senderid,system_messages.recevierid');
		$this->db->from('system_messages');
		$this->db->join('ah_userprofile','system_messages.senderid = ah_userprofile.userid');
		$this->db->where('system_messages.recevierid',$senderid);
		$this->db->where('system_messages.isviewed','0');
		$this->db->group_by('system_messages.senderid');
		$this->db->order_by("system_messages.messagedate", "DESC");
		
		$messageQuery 			=	$this->db->get();
		$arrx['number_count']	=	$messageQuery->num_rows();
				
		$html = '';
		
		foreach($messageQuery->result() as $mx)
		{
			$html .= '<li onClick="messagechat(this);" data-name="'.$mx->fullname.'" data-url="'.base_url().'messages/messagechat/'.$mx->messid.'/'.$mx->senderid.'/'.$mx->recevierid.'/'.md5($mx->messagedate).'" class="list-group-item oldmessages"><div class="text-holder"> <span class="title-text">'.$mx->fullname.'</span> <span class="description-text"  style="font-size:12px;">'.$mx->message.'</span> </div> <span class="time-ago" style="font-size:11px;"> '.arabic_date($mx->messagedate).' </span> </li>';
		}
		
		$arrx['mess'] = $html;
		
		//-------------------------------------------------------------------
		$this->db->select("leaveid,userid,reason,start_date,end_date,leave_application,approved,delete_record");
		$this->db->from('ah_leave_request');
		$this->db->where('approved','0');
		
		if($user_role	== '102')
		{
			$this->db->where('ah_leave_request.userid',$this->_login_userid);
			$this->db->or_where('ah_leave_request.manager_id',$this->_login_userid);
		}
		
		$query = $this->db->get();		
		$arrx['hr_count'] = $query->num_rows();
		
		echo json_encode($arrx);
	}
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/	
	public function messagechat($messageid,$senderid,$recevierid,$gcode)
	{
		$this->_data['messages'] =	$this->mes->loadChat($messageid,$senderid,$recevierid);
		$this->_data['messid'] = $messageid;
		$this->_data['senderid'] = $senderid;
		$this->_data['recevierid'] = $recevierid;
		$this->load->view('chatmessage',$this->_data);
	}
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/	
	public function add_new_message()
	{	
		$branchid 	= $this->_data['user_info']['profile']->branchid;		
		$fullname 	= $this->_data['user_detail']['profile']->fullname;
			
		$senderid 	= $this->_login_userid;
		$recevierid = $this->input->post('senderid');
		$replyid 	= $this->input->post('messid');
		$messageip 	= $this->input->post('recevierid');
		$message 	= $this->input->post('message');
		
		$insertData = array('branchid'=>$branchid,'senderid'=>$senderid,'recevierid'=>$recevierid,'replyid'=>$replyid,'messageip'=>$_SERVER['REMOTE_ADDR'],'message'=>$message);
		
		$query 		=	$this->db->insert('system_messages',$insertData);
		$res 		=	$this->mes->loadChat($messid,$recevierid,$senderid);		
		$i			=	0;
		$html 		=	'';
		
	    foreach($res as $mes) 
		{
			$html .= '<div class="col-md-12 ';
			if ($fullname == $mes['sendername']) 
			{
				$html .= 'recevier';
			} 
			else 
			{
				$html .= 'sender';
			}
			 
			$html .= '">';
          	$html .= '<label class="text-warning"><strong>'.$mes['sendername'].'</strong> <span> : '.arabic_date($mes['messagedate']).'</span></label><br>';
          	$html .= $mes['message'].'</div>';
	  		$i++;
		}
				
		echo $html; 
	}
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/	
	public function inbox()
	{
		$this->_data['myfriends'] = $this->mes->listofusers();
		
		$this->load->view('inbox',$this->_data);
	}
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/	
	public function viewmessage()
	{
		$senderid = $this->input->post('senderid');
		
		$this->_data['messages'] =	$this->mes->loadChat($messageid,$senderid,$this->_login_userid);
		$this->_data['messid'] = $messageid;
		$this->_data['senderid'] = $senderid;
		$this->_data['recevierid'] = $recevierid;
		$this->_data['login_user_name']	=	$this->_data['user_detail']['profile']->fullname;
		$this->load->view('chatmessage',$this->_data);
	}
//------------------------------------------------------------------------

  	/**
   	* Dynamic Forms Listing Page
   	* @param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]  = $this->haya_model->get_all_custom_form($moduleid);
		
		$this->_data["userid"] = $this->_login_userid;
		
		$this->_data['formid']	=	$moduleid;
		 
		 // Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);	 
	 }	
}
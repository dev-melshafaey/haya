<!doctype html>
<?PHP $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?PHP $this->load->view('common/bodyscript'); ?>
<?PHP $this->load->view('common/menu'); ?>
<?PHP if($main->tempid!='' && !empty($h)) { 
	$this->load->view('common/leftpanel',array('history'=>$h)); 
} ?>
<section class="wrapper scrollable">
<?PHP $this->load->view('common/logo'); ?>
<?PHP $this->load->view('common/usermenu'); ?>
<?PHP $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
<?PHP $this->load->view('common/quicklunchbar'); ?>
<div class="row">
  <div class="col-md-12">
    <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
    <!----------------->
    <div class="col-md-8">
      <div class="panel-default panel-block">
        <div class="nav nav-tabs panel panel-default panel-block" id="messagelistnew">
          <div class="nodatamessage">لم يتم العثور على رسالة</div>
        </div>
      </div>
    </div>
    <!------------------------>
    <div class="col-md-4">
      <div class="panel panel-default panel-block">
      <?php //echo '<pre>'; print_r($myfriends);exit();?>
        <?PHP foreach($myfriends as $mf) { ?>
        <div class="list-group mybox" id="<?PHP echo $mf->userid; ?>"> <?PHP echo $mf->fullname; ?> <span class="lastmessage"><?PHP echo arabic_date($mf->messagedate); ?></span> </div>
        <?PHP } ?>
      </div>
    </div>
    
    <!-----------------> 
  </div>
</div>
<?php $this->load->view('common/footer');?>
</body>
</html>
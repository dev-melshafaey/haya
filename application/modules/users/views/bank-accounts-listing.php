<!doctype html>
<?php $userid	=	$this->uri->segment(3);?>

<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $this->load->view('common/user-tabs'); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <p>
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  
				<?php $permissions	=	$this->haya_model->check_other_permission(array('130'));?>
          		  <?php if($permissions[130]['a']	==	1):?>
                  	<div class="row table-header-row"> <a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url();?>users/add_bank_account/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة حساب البنك">إضافة حساب البنك</a> </div>
                  <?php endif;?>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">اسم البنك</th>
                        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">اسم الفرع</th>
                        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">رقم الحساب</th>
                        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">اسم صاحب الحساب</th>
                        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view(
'common/footer',array('ajax_url'=>base_url().'users/bank_accounts_listing/'.$userid,
'columns_array'=>'{"data":"‫اسم البنك"},
				  {"data":"اسم الفرع"},
				  {"data":"رقم الحساب"},
				  {"data":"اسم صاحب الحساب"},
				  {"data":"الإجراءات"}')); ?>
<!-- /.modal-dialog -->
</div>
</body>
</html>
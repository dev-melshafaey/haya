<div class="col-md-12" style="    text-align: center;
    margin-bottom: 4px;
    padding: 0px !important;">
<button type="button" id="" onclick="printthepage('tlist');" class="btn btn-default">طباعة</button>
</div>
<div class="col-md-12" style="padding: 0px !important; direction:rtl;" id="tlist">
  <table class="table" style="border:1px solid #CCC;">
    <thead>
    <tr>
    	<th colspan="7" style="text-align:center !important; border-bottom:2px solid #CCC !important;"><?PHP 		
			echo '<h4>'.$u['profile']->fullname.'</h4><br>';
			echo $u['profile']->branchname.'<br>';
			echo $u['profile']->branchaddress;
		?></th>
    </tr>
      <tr>
        <th style="text-align:center;">م</th>
        <th style="text-align:center;">رقم المعاملة</th>
        <th style="width:200px !important; text-align:right;">الإسم</th>
        <th style="text-align:right;">نوع المساعدات</th>
        <th style="text-align:right;">البطاقة الشخصة</th>
        <th style="text-align:right; width:200px !important;">المحافظة / ولاية</th>       
        <th style="text-align:center;">تاريخ التسجيل</th>
      </tr>
    </thead>
    <tbody>
      <?PHP 
	  	$cnt = 1;
	  	foreach($d as $dx) { ?>
      <tr>
        <td style="text-align:center;"><?PHP echo a_date($cnt); ?></td>
        <td style="text-align:center;"><?PHP echo a_date($dx->applicantcode); ?></td>
        <td style="width:200px !important;  text-align:right;"><?PHP echo $dx->fullname; ?></td>
        <td style="text-align:right;"><?PHP echo $dx->charity_type; ?></td>
        <td style="text-align:right;"><?PHP echo a_date($dx->idcard_number); ?></td>
        <td style="text-align:right;  width:200px !important;"><?PHP echo $dx->province; ?> / <?PHP echo $dx->wilaya; ?></td>
        <td style="text-align:center;"><?PHP echo a_date(date('Y-m-d',strtotime($dx->registrationdate))); ?></td>
      </tr>
      <?PHP 
	  		$cnt++;
	  } ?>
    </tbody>
  </table>
</div>
<div class="col-md-12" style="    text-align: center;
    margin-top: 4px;
    padding: 0px !important;">
<button type="button" id="" onclick="printthepage('tlist');" class="btn btn-default">طباعة</button>
</div>
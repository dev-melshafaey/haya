<?php $segment	=	$this->uri->segment(1);?>
<?php $text		=	$this->lang->line($segment);?>
<?php $labels	=	$text['users']['form'];?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <?php $this->load->view('common/user-tabs'); ?>
        <form action="<?php echo current_url();?>" method="POST" id="user_form" name="user_form" enctype="multipart/form-data" autocomplete="off">
          <div class="col-md-12">
            <div class="panel panel-default panel-block">
              <div class="list-group">
                <div class="list-group-item" id="input-fields">
                  <div class="form-group col-md-3">
                    <label class="text-warning"><?php echo $labels['fullname']?></label>
                    <input type="text" class="form-control req" name="fullname" id="fullname" placeholder="<?php echo $labels['fullname']?>" value="<?php echo $fullname;?>"/>
                  </div>
                  <div class="form-group col-md-3">
                    <label class="text-warning"><?php echo $labels['fathername']?></label>
                    <input type="text" class="form-control req" name="fathername" id="fathername" placeholder="<?php echo $labels['fathername']?>" value="<?php echo $fathername;?>"/>
                  </div>
                  <div class="form-group col-md-3">
                    <label class="text-warning"><?php echo $labels['userlogin']?></label>
                    <input type="text" class="form-control req" name="userlogin" id="userlogin" placeholder="<?php echo $labels['userlogin']?>"  value="<?php echo $userlogin;?>"/>
                  </div>
                  <div class="form-group col-md-3">
                    <label class="text-warning"><?php echo $labels['email']?></label>
                    <input type="text" class="form-control req" name="email" id="email" placeholder="<?php echo $labels['email']?>" value="<?php echo $email;?>"/>
                  </div>
                  <div class="form-group col-md-3">
                    <label class="text-warning"><?php echo $labels['userpassword']?></label>
                    <input type="password" class="form-control <?php if(!$userpassword):?> req <?php endif;?>" name="user_password" id="user_password" />
                  </div>
                  <div class="form-group  col-md-3">
                    <label class="text-warning"><?php echo $labels['userroleid']?></label>
                    <?php echo $this->haya_model->create_dropbox_list('userroleid','user_role',$userroleid);?> </div>
                  <div class="form-group  col-md-3">
                    <label class="text-warning"><?php echo $labels['gender']?></label>
                    <select name="gender" class="form-control req">
                      <option value=""><?php echo $labels['gender']?></option>
                      <option value="ذكر" <?php if($gender	==	'ذكر'):?>selected="selected"<?php endif;?> >ذكر</option>
                      <option value="أنثى" <?php if($gender	==	'أنثى'):?>selected="selected"<?php endif;?> >أنثى</option>
                    </select>
                  </div>
                  <div class="form-group  col-md-3">
                    <label class="text-warning"><?php echo $labels['maritialstatus']?></label>
                    <?php echo $this->haya_model->create_dropbox_list('maritialstatus','marital_status',$maritialstatus,0,'req'); ?> </div>
                  <div class="form-group  col-md-3">
                    <label class="text-warning">مهنة</label>
                    <?php echo $this->haya_model->create_dropbox_list('profession','profession',$profession,0,'req'); ?> </div>
                  <div class="form-group  col-md-3">
                    <label class="text-warning"><?php echo $labels['nationality']?></label>
                    <?php echo $this->haya_model->create_dropbox_list('nationality','nationality',$nationality,0,'req'); ?> </div>
                  <div class="form-group  col-md-3">
                    <label class="text-warning"><?php echo $labels['numberofdependens']?></label>
                    <select name="numberofdependens" class="form-control">
                      <?php for($i	=	1;	$i	<=	20;	$i++):?>
                      <option value="<?php echo $i;?>" <?php if($numberofdependens	==	$i):?>selected="selected"<?php endif;?> ><?php echo $i;?></option>
                      <?php endfor;?>
                    </select>
                  </div>
                  <div class="form-group col-md-3">
                    <label class="text-warning"><?php echo $labels['branchid']?></label>
                    <select name="branchid" class="form-control">
                      <?php foreach($this->users->get_all_branches() as $branch):?>
                      <option value="<?php echo $branch->branchid;?>" <?php if($branch->branchid	==	$branchid):?>selected="selected"<?php endif;?> ><?php echo $branch->branchname;?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                  <div class="form-group col-md-4">
                    <label class="text-warning"><?php echo $labels['profilepic']?></label>
                    <input style="border: 0px !important; color: #d09c0d;" type="file" name="profilepic" title='تحميل'>
                  </div>
                  <?php if($is_profilepic):?>
                  <div class="form-group col-md-4"> <img src="<?php echo base_url();?>resources/users/<?php echo $userid;?>/<?php echo $is_profilepic;?>" width="150px" height="150px"/> </div>
                  <?php endif;?>
                  <div class="form-group col-md-4 ">
                    <label class="text-warning"><?php echo $labels['resume']?></label>
                    <input style="border: 0px !important; color: #d09c0d;" type="file" name="resume" title='تحميل'>
                  </div>
                  <?php if($is_resume):?>
                  <!--<div class="form-group col-md-3">
                           <img src="<?php echo base_url();?>resources/applicants/<?php echo $userid;?>/<?php echo $is_resume;?>" width="150px" height="150px"/>
                      </div>-->
                  <?php endif;?>
                  <!--<div class="col-md-4  panel panel-default panel-block" style="border-right: 2px solid #eee;">
                            <div class="col-md-12 form-group">
                              <h4>خبرة العمل
                                <button type="button" id="add_experience" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px;">اضافة</button>
                              </h4>
                              <table class="table table-bordered table-striped dataTable" id="educational-info">
                                <thead>
                                  <tr role="row">
                                    <th><label class="text-warning">اسم المكتب</label></th>
                                    <th><label class="text-warning">تاريخ البدء</label></th>
                                    <th><label class="text-warning">تاريخ الانتهاء</label></th>
                                    <th>الإجراءات</th>
                                  </tr>
                                </thead>
                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                  <tr>
                                    <td><input name="office_name[]" value="" placeholder="اسم المكتب" id="office_name" type="text" class="form-control req"></td>
                                    <td><input name="start_data[]" value="" placeholder="تاريخ البدء" id="start_data" type="text" class="datepicker form-control req"></td>
                                    <td><input name="end_date[]" value="" placeholder="تاريخ الانتهاء" id="end_date" type="text" class="datepicker form-control req"></td>
                                    <td></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>--> 
                  <br clear="all" />
                  <?php if($is_profilepic):?>
                  <input type="hidden" name="is_profilepic" id="is_profilepic" value="<?php echo $is_profilepic;?>" />
                  <?php endif;?>
                  <?php if($is_resume):?>
                  <input type="hidden" name="is_resume" id="is_resume" value="<?php echo $is_resume;?>" />
                  <?php endif;?>
                  <?php if($userid):?>
                  <input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>" />
                  <?php endif;?>
                  <input type="submit" id="submit_user" class="btn btn-success btn-lrg" name="submit_user"  value="حفظ"  />
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<!-- /.modal-dialog --> 
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
		});
	});
</script>
</div>
</body>
</html>
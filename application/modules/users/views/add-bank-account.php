<?php $this->uri->segment(3);
  //$fullname = $user_detail['profile']->fullname;
 // if($accountfullname=='')
  //{
   // $accountfullname = $fullname;
  //}
  
?>
<div class="row">
  <div class="row col-md-12">
    <form action="" method="POST" id="add_account" name="add_account" enctype="multipart/form-data" autocomplete="off">
      <div class="col-md-3 form-group">
        <label class="text-warning">إسم البنك:</label>
        <?PHP echo $this->haya_model->create_dropbox_list('bankid','bank',$bankid,0,'req','','','branchid'); ?> </div>
      <div class="col-md-3 form-group">
        <label class="text-warning">الفرع:</label>
        <?PHP echo $this->haya_model->create_dropbox_list('branchid','bank_branch',$branchid,$bankid,0,'req'); ?> </div>
      <div class="col-md-3 form-group">
        <label class="text-warning">رقم الحساب</label>
        <input type="text" class="form-control req NumberInput" name="accountnumber"  id="accountnumber"  placeholder="رقم الحساب" value="<?php echo $accountnumber;?>" onkeyup="only_numeric(this);"/>
      </div>
      <div class="col-md-3 form-group">
        <label class="text-warning">اسم صاحب الحساب‎</label>
        <input type="text" name="accountfullname" id="accountfullname" class="form-control req" placeholder="اسم صاحب الحساب" value="<?php echo $accountfullname;?>" />
      </div>
      <div class="form-group col-md-4">
       <label class="text-warning">وثيقة</label>
       <input style="border: 0px !important; color: #d09c0d;" type="file" name="bank_doc" id="bank_doc" title='وثيقة'>
      </div>
      <input type="hidden" name="bankaccountid" id="bankaccountid" value="<?php echo $bankaccountid;?>"/>
      <input type="hidden" name="data_table_id" id="data_table_id" value="<?php echo '2';?>"/>
      
      <input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>"/>
      
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group  col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="add_bank_account();" value="حفظ" />
    </div>
  </div>
</div>
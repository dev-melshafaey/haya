<?php $segment	=	$this->uri->segment(1);?>
<?php $text		=	$this->lang->line($segment);?>
<?php $labels	=	$text['users']['listing'];?>

<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="col-md-12">
          <form class="mws-form" method="post" action="<?php echo current_url();?>" id="date_range" name="date_range">
            <div class="col-md-4 form-group">
              <label class="text-warning">إلى‎</label>
              <input type="text" name="startdate" id="startdate" class="datepicker form-control req" placeholder="تاريخ البدء‎" />
            </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">من</label>
              <input type="text" name="enddate" id="enddate" class="datepicker form-control req" placeholder="تاريخ الانتهاء"/>
            </div>
            <div class="form-group  col-md-4">
              <input style="margin-top: 24px !important;" type="submit" class="btn btn-success btn-lrg" name="submit"  id="submit" value="حفظ" />
            </div>
          </form>
        </div>
        <br clear="all" />
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <?PHP $this->load->view("common/globalfilter",array('type'=>'users')); ?>
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th>الأسم الكامل</th>
                        <th>سبب</th>
                        <th>من</th>
                        <th>إلى</th>
                        <th>إجازة يوم</th>
                        <th>ترك طلب الحالة</th>
                        <th>الإجرائات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <?php $date	=	date('Y-m-d')?>
                   <?php
					if(!empty($response)):
						foreach($response as $leave):
							if($leave->leave_application)
							{
								$actions	= '&nbsp;&nbsp;<a href="'.base_url().'users/download_file/'.$leave->userid.'/'.$leave->leave_application.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
							}
							
							$actions 	 .=  '	<a  onclick="alatadad(this);" data-url="'.base_url().'users/view_request/'.$leave->leaveid.'"  href="#"><i class="icon-eye-open"></i></a>';
							
							if($this->haya_model->check_other_permission(143,'u')==1)
							{
								$actions 	.=	'<a href="#addingDiag" onclick="alatadad(this);" data-url="'.base_url().'users/add_leave_request/'.$leave->leaveid.'" id="'.$leave->leaveid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
							}
							if($leave->approved	==	'1')
							{
								$request	=	'<img src="'.base_url().'assets/images/approved.png" style="width: 24px;" alt="Approved" title="Approved"/>';
							}
					?>
                      <tr role="row" id="<?php echo $leave->leaveid;?>_durar_lm">
                        <td  style="text-align:center;"><?php echo $leave->fullname;?></td>
                        <td  style="text-align:center;"><?php echo $leave->reason;?></td>
                        <td  style="text-align:center;"><?php echo date('d-m-y',strtotime($leave->start_date));?></td>
                        <td  style="text-align:center;"><?php echo date('d-m-y',strtotime($leave->end_date));?></td>
                        <td  style="text-align:center;"><?php echo $leave->total_leaves;?></td>
                        <td  style="text-align:center;"><?php echo $request;?></td>
                        <td><?php echo $actions;?></td>
                      </tr>
                      <?php unset($actions); endforeach;?>
                   <?php endif;?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
$(function(){
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});
</script>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
<?php $permissions	=	$this->haya_model->check_other_permission(array('142'));?>

<?php
	if($user_data->approved	==	'0')
	{	
		$request	=	'تحت المعالجة';
	}
	elseif($user_data->approved	==	'1')
	{
		$request	=	'وافق';
	}
	else
	{
		$request	=	'غير موافق';
	}
?>
<div id="printable" style="direction:rtl;display:none;">
<div class="form-group">
  <h4>تفاصيل طلب الإجازة</h4>
  <table class="table table-bordered table-striped dataTable no-footer">
    <tbody>
      <tr>
        <td><strong>الأسم بالكامل</strong></td>
        <td><?php echo $user_data->fullname; ?></td>
        <td><strong>الدرجة</strong></td>
        <td><?php echo $this->users->get_level_name($user_data->level_id); ?></td>
      </tr>
      <tr>
        <td><strong>الرقم الوظيفي</strong></td>
        <td><?php echo arabic_date($user_data->id_number); ?></td>
        <td><strong>الفرع</strong></td>
        <td><?php echo $this->haya_model->get_name_from_list($user_data->branchid); ?></td>
      </tr>
      <tr>
        <td><strong>القسم</strong></td>
        <td><?php echo $this->haya_model->get_name_from_list($user_data->profession); ?></td>
        <td><strong>الدائرة</strong></td>
        <td><?php echo $this->haya_model->get_name_from_list($user_data->sub_profession); ?></td>
      </tr>
    </tbody>
  </table>
</div>
<div class="form-group">
 
  <table class="table table-bordered table-striped dataTable no-footer">
    <tbody>
      <tr>
        <td><strong>سبب</strong></td>
        <td><?php echo $user_data->reason; ?></td>
        <td><strong>من</strong></td>
        <td><?php echo arabic_date(date('d-m-y',strtotime($user_data->start_date))); ?></td>
      </tr>
      <tr>
        <td><strong>إلى</strong></td>
        <td><?php echo arabic_date(date('d-m-y',strtotime($user_data->end_date))); ?></td>
        <td><strong>عدد الأيام</strong></td>
        <td><?php echo arabic_date($user_data->total_leaves); ?></td>
        <td><strong>الإجازات المتبقية</strong></td>
        <td><?php echo arabic_date($this->users->get_total_leaves($level_id) - $user_data->total_leaves); ?></td>
      </tr>
      <tr>
        <td><strong>حالة الطلب</strong></td>
        <td><?php echo $request; ?></td>
        <td><strong>ملاحظات من مقدم الطلب</strong></td>
        <td><?php echo $user_data->notes; ?></td>
      </tr>
      <tr>
      	<td><strong>ملاحظات من مقدم الطلب</strong></td>
        <td colspan="3"><?php echo $user_data->notes; ?></td>
      </tr>
    </tbody>
  </table>
</div>

<?php if($user_role	==	'101'):?>
<?php $request_detail	=	$this->users->get_manager_hr_response($user_data->leaveid);?>
<?php $manager			=	$this->haya_model->get_user_detail($request_detail->manager_id)?>
<?php $hr				=	$this->haya_model->get_user_detail($request_detail->hr_id)?>
<?php if(!empty($request_detail)):?>
<div class="form-group">
  <h4>المدير المسؤل</h4>
  <table class="table table-bordered table-striped dataTable no-footer">
    <tbody>
      <tr>
        <td><strong>السبب</strong></td>
        <td><?php echo $this->haya_model->get_name_from_list($request_detail->choose_reason); ?></td>
        <td><strong>ملاحظات</strong></td>
        <td><?php echo $request_detail->manager_comment; ?></td>
      </tr>
      <tr>
        <td><strong>اسم</strong></td>
        <td><?php echo $manager['profile']->fullname;?></td>
        <td><strong>تاريخ</strong></td>
        <td><?php echo arabic_date(date('Y-m-d',strtotime($request_detail->approved_date))); ?></td>
      </tr>
    </tbody>
  </table>
</div>
<?php if($request_detail->status	==	'1'):?>
<div class="form-group">
  <h4>رد قسم الموارد البشرية</h4>
  <table class="table table-bordered table-striped dataTable no-footer">
    <tbody>
      <tr>
        <td><strong>ملاحظات</strong></td>
        <td colspan="3"><?php echo $request_detail->hr_comment; ?></td>
      </tr>
      <tr>
        <td><strong>اسم</strong></td>
        <td><?php echo $hr['profile']->fullname;?>‎</td>
        <td><strong>تاريخ</strong></td>
        <td><?php echo arabic_date(date('Y-m-d',strtotime($request_detail->hr_date))); ?></td>
      </tr>
    </tbody>
  </table>
</div>
<?php endif;?>
<?php endif;?>
<?php endif;?>
</div>
<div>
<div class="row">
  <div class="col-md-12 fox leftborder">
    <h4 class="panel-title customhr"> تفاصيل طلب الإجازة</h4>
    <div class="col-md-4 form-group">
      <label class="text-warning">الأسم بالكامل :</label>
      <strong><?php echo $user_data->fullname; ?> </strong> </div>
    <!--<div class="col-md-4 form-group">
      <label class="text-warning"> صورة):</label>
      <?php $statmentURL = base_url().'resources/users/'.$user_id.'/'.$user_data->profilepic; ?>
          <img src="<?php echo $statmentURL; ?>"  width="100"/>
    </div>-->
    <div class="col-md-4 form-group">
      <label class="text-warning">الدرجة : </label>
      <strong><?php echo $this->users->get_level_name($user_data->level_id); ?></strong></div>
    <div class="col-md-4 form-group">
      <label class="text-warning">الرقم الوظيفي : </label>
      <strong><?php echo arabic_date($user_data->id_number); ?></strong></div>
    <div class="col-md-4 form-group">
      <label class="text-warning">الفرع : </label>
      <strong><?php echo $this->haya_model->get_name_from_list($user_data->branchid); ?></strong></div>
    <div class="col-md-4 form-group">
      <label class="text-warning">القسم : </label>
      <strong><?php echo $this->haya_model->get_name_from_list($user_data->profession); ?></strong></div>
          <div class="col-md-4 form-group">
      <label class="text-warning">الدائرة : </label>
      <strong><?php echo $this->haya_model->get_name_from_list($user_data->sub_profession); ?></strong></div>
  </div>
  <div class="col-md-12 fox leftborder">
    <div class="col-md-4 form-group">
      <label class="text-warning">سبب: </label>
      <strong><?php echo $user_data->reason; ?></strong> </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">من: </label>
      <strong><?php echo arabic_date(date('d-m-y',strtotime($user_data->start_date))); ?></strong> </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">إلى: </label>
      <strong><?php echo arabic_date(date('d-m-y',strtotime($user_data->end_date))); ?></strong> </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">عدد الأيام: </label>
      <strong><?php echo arabic_date($user_data->total_leaves); ?></strong> </div>
     <div class="col-md-4 form-group">
      <label class="text-warning">الإجازات المتبقية: </label>
      <strong><?php echo arabic_date($this->users->get_total_leaves($level_id) - $user_data->total_leaves); ?></strong> </div> 
    <div class="col-md-4 form-group">
      <label class="text-warning">حالة الطلب: </label>
      <?php
        if($user_data->approved	==	'0')
        {	
            $request	=	'تحت المعالجة';
        }
        elseif($user_data->approved	==	'1')
        {
            $request	=	'وافق';
        }
        else
        {
            $request	=	'غير موافق';
        }
        ?>
      <strong><?php echo $request; ?></strong> </div>
  </div>
  <div class="col-md-12 form-group">
    <label class="text-warning">ملاحظات: </label>
    <strong><?php echo $user_data->notes; ?></strong> </div>
</div>
<br clear="all" />
<?php if($permissions[142]['v']	==	1):?>

<?php if($user_role	==	'102'):?>
<div align="center">
  <?php if($user_data->approved	==	0):?>
  	<button type="button" class="btn btn-lg btn-success hide-btn" onclick="request_response('<?php echo $user_data->leaveid;?>','1');">وافق</button>
  	<button type="button" class="btn btn-lg btn-danger hide-btn" onclick="request_response('<?php echo $user_data->leaveid;?>','2');">غير موافق</button>
  <?php elseif($user_data->approved	==	1):?>
  	<button type="button" class="btn btn-lg btn-danger hide-btn" onclick="request_response('<?php echo $user_data->leaveid;?>','2');">غير موافق</button>
  <?php elseif($user_data->approved	==	2):?>
  	<button type="button" class="btn btn-lg btn-success hide-btn" onclick="request_response('<?php echo $user_data->leaveid;?>','1');">وافق</button>
  <?php endif;?>
</div>
<br clear="all" />
<div id="show-manager-form" class="row" style="display:none;">
  <div class="row col-md-12">
    <form action="" method="POST" id="for-manager" name="for-manager">
      <div class="col-md-4 form-group" id="choose-box">
        <label class="text-warning">اسباب عدم الحضور :</label>
        <select name="reason" id="reason" class="form-control req">
          <option value="">أنواع السبب</option>
          <?php foreach($this->haya_model->get_leave_types() as $value):?>
          <option value="<?php echo $value;?>" <?php  echo($reason  ==  $value ? 'selected="selected"' : NULL); ?>><?php echo $value;?></option>
          <?php endforeach;?>
        </select> </div>
      <div class="col-md-8 form-group">
        <label class="text-warning">الملاحظات‎ :</label>
        <textarea name="manager_comment" id="manager_comment" placeholder="الملاحظات" style="margin-top: 0px; margin-bottom: 0px; height: 219px;"><?php echo $notes;?></textarea>
      </div>
      <div class="col-md-6 form-group">
        <label class="text-warning">اسم : <?php echo $user_detail['profile']->fullname;?>‎</label>
      </div>
      <input type="hidden" name="leaveid" id="leaveid" />
      <input type="hidden" name="val" id="val" />
      <input type="hidden" name="manager_id" id="manager_id" value="<?php echo $user_id;?>"/>
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group  col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="manager_response();" value="حفظ" />
    </div>
  </div>
</div>
<?php endif;?>
<?php if($user_role	==	'101'):?>
<?php $request_detail	=	$this->users->get_manager_hr_response($user_data->leaveid);?>
<?php if(!empty($request_detail)):?>
<?php if($request_detail->status	==	'0'):?>
<br clear="all" />
<div id="show-manager-form" class="row">
  <div class="row col-md-12">
    <form action="" method="POST" id="for-hr" name="for-hr">
      <div class="col-md-8 form-group">
        <label class="text-warning">الملاحظات‎ :</label>
        <textarea name="hr_comment" id="hr_comment" placeholder="الملاحظات" style="margin-top: 0px; margin-bottom: 0px; height: 219px;"><?php echo $notes;?></textarea>
      </div>
      <input type="hidden" name="leaveid" id="leaveid" />
      <input type="hidden" name="status" id="status" value="1"/>
      <input type="hidden" name="hr_id" id="hr_id" value="<?php echo $user_id;?>"/>
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group  col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="hr_response('<?php echo $user_data->leaveid;?>');" value="حفظ" />
    </div>
  </div>
</div>
<?php else:?>
<?php $manager	=	$this->haya_model->get_user_detail($request_detail->manager_id)?>
<?php $hr		=	$this->haya_model->get_user_detail($request_detail->hr_id)?>
<div class="col-md-12 fox leftborder">
  <h4 class="panel-title customhr">المدير المسؤل</h4>

  <div class="col-md-4 form-group">
    <label class="text-warning">السبب : </label>
    <strong><?php echo $this->haya_model->get_name_from_list($request_detail->choose_reason); ?></strong> </div>  <div class="col-md-12 form-group">
    <label class="text-warning">ملاحظات المدير : </label>
    <strong><?php echo $request_detail->manager_comment; ?></strong> </div>
      <div class="col-md-4 form-group">
    <label class="text-warning">اسم المدير : </label>
    <strong><?php echo $manager['profile']->fullname;?>‎</strong> </div>
  <div class="col-md-4 form-group">
    <label class="text-warning">تاريخ الموافقة : </label>
    <strong><?php echo arabic_date(date('Y-m-d',strtotime($request_detail->approved_date))); ?></strong> </div>
</div>
<div class="col-md-12 fox leftborder">
  <h4 class="panel-title customhr">رد قسم الموارد البشرية</h4>
    <div class="col-md-12 form-group">
    <label class="text-warning">ملاحظات مدير قسم الموارد البشرية : </label>
    <strong><?php echo $request_detail->hr_comment; ?></strong> </div>
  <div class="col-md-6 form-group">
    <label class="text-warning">اسم موظف الموارد البشرية : </label>
    <strong><?php echo $hr['profile']->fullname;?>‎</strong> </div>
  <div class="col-md-6 form-group">
    <label class="text-warning">تاريخ موافقة قسم الموارد البشرية : </label>
    <strong><?php echo arabic_date(date('Y-m-d',strtotime($request_detail->hr_date))); ?></strong> </div>
  <?php //echo '<pre>'; print_r($request_detail);?>
</div>
</div>
<br clear="all" />
<div class="row" style="text-align:center;">
	<button type="button" id="" onclick="printthepage('printable');" class="btn btn-default">طباعة</button>
</div>
<?php endif?>
<?php endif;?>
<?php endif;?>
<?php endif;?>
<script>
function manager_response()
{
	var form_data	=	$('#for-manager').serialize();
	
	var request = $.ajax({
	  url: config.BASE_URL+'users/request_response',
	  type: "POST",
	  data: form_data,
	  dataType: "html",
	  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
	  success: function(msg)
	  {
		  window.location.href = config.BASE_URL+'users/leave_requests';
	  }
	});
}
function hr_response(leaveid)
{
	$("#leaveid").val(leaveid);
	
	var form_data	=	$('#for-hr').serialize();
	
	var request = $.ajax({
	  url: config.BASE_URL+'users/hr_response',
	  type: "POST",
	  data: form_data,
	  dataType: "html",
	  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
	  success: function(msg)
	  {
		 window.location.href = config.BASE_URL+'users/leave_requests';
	  }
	});
}
function request_response(leaveid,val)
{
	$(".hide-btn").hide();
	$("#show-manager-form").show();
	
	if(val	==	'2')
	{
		$("#choose-box").hide();
	}
	
	$("#leaveid").val(leaveid);
	$("#val").val(val);
	
	/*var request = $.ajax({
	  url: config.BASE_URL+'users/request_response',
	  type: "POST",
	  data: "val="+val+"&leaveid="+leaveid,
	  dataType: "html",
	  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
	  success: function(msg)
	  {
		  window.location.href = config.BASE_URL+'users/leave_requests';
		  
		 $('#addingDiag').modal('hide');	  
		 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
		 $('.dataTable').dataTable().fnDestroy();
		 $('#ajax_action').hide();	
		 create_data_table(1);
	  }
	});*/
}
function not_approved(leaveid,val)
{
	var request = $.ajax({
	  url: config.BASE_URL+'users/request_response',
	  type: "POST",
	  data: "val="+val+"&leaveid="+leaveid,
	  dataType: "html",
	  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
	  success: function(msg)
	  {
		  window.location.href = config.BASE_URL+'users/leave_requests';
		  
		 /*$('#addingDiag').modal('hide');	  
		 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
		 $('.dataTable').dataTable().fnDestroy();
		 $('#ajax_action').hide();	
		 create_data_table(1);*/
	  }
	});
}
</script>
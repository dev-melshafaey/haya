<div class="row">
  <div class="row col-md-12">
    <form action="" method="POST" id="add_level" name="add_level">
      <div class="col-md-6 form-group">
        <label class="text-warning">مسمى الدرجة</label>
        <input type="text" name="level_name" id="level_name" class="form-control req" placeholder="مسمى الدرجة" value="<?php echo $level_name;?>" />
      </div>
      <div class="col-md-6 form-group">
        <label class="text-warning">عدد ايام الاجازة</label>
        <input type="text" name="level_holiday" id="level_holiday" class="form-control req" placeholder="عدد ايام الاجازة" value="<?php echo $level_holiday;?>" />
      </div>
      <input type="hidden" name="level_id" id="level_id" value="<?php echo $level_id;?>"/>
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group  col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="add_level_record();" value="حفظ" />
    </div>
  </div>
</div>
<script>
function add_level_record()
{
	check_my_session();
	$('#add_level .req').removeClass('parsley-error');
    $('#add_level .req').each(function (index, element) {
        if ($.trim($(this).val()) == '') {
            $(this).addClass('parsley-error');
        }
    });
    var len = $('#add_level .parsley-error').length;

	if(len<=0)
	{
		var str_data = 	$('#add_level').serialize();
		
		var request = $.ajax({
		  url: config.BASE_URL+'users/add_level',
		  type: "POST",
		  data: str_data,
		  dataType: "html",
		  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(msg)
		  {
			  window.location.href = msg;

		  }
		});
	}}
</script>

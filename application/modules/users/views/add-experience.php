<div class="row">
  <div class="row col-md-12">
    <form action="" method="POST" id="add_experience" name="add_experience" enctype="multipart/form-data" autocomplete="off">
      <div class="col-md-4 form-group">
        <label class="text-warning">نوع الوثيقة</label>
        <label class="text-warning">موقف / العنوان‎</label>
        <input type="text" name="exe_title" id="exe_title" class="form-control req" placeholder="موقف / العنوان" value="<?php echo $exe_title;?>" />
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">بلد</label>
        <?php echo $this->haya_model->create_dropbox_list('country','issuecountry',$country,0,'req'); ?></div>
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ البدء</label>
        <input type="text" name="start_date" id="start_date" class="datepicker form-control req" placeholder="تاريخ البدء‎" value="<?php echo $start_date;?>" />
      </div>
     <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ الانتهاء</label>
        <input type="text" name="end_date" id="end_date" class="datepicker form-control req" placeholder="تاريخ الانتهاء" value="<?php echo $end_date;?>" />
     </div>
    <div class="form-group col-md-4">
       <label class="text-warning">شهادة</label>
       <input style="border: 0px !important; color: #d09c0d;" type="file" name="certificate" id="certificate" title='تحميل'>
    </div>
      <div class="col-md-12 form-group">
        <label class="text-warning">التفاصيل</label>
       <textarea name="detail" id="detail" placeholder="التفاصيل" style="margin-top: 0px; margin-bottom: 0px; height: 219px;"><?php echo $detail;?></textarea>
      </div>
      <input type="hidden" name="exe_id" id="exe_id" value="<?php echo $exe_id;?>"/>
      <input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>"/>
      <input type="hidden" name="data_table_id" id="data_table_id" value="<?php echo '7';?>"/>
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group  col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="add_exe();" value="حفظ" />
    </div>
  </div>
</div>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});
</script>
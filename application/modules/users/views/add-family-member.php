 <div class="row">
  <div class="row col-md-12">
    <form action="" method="POST" id="add_family" name="add_family" enctype="multipart/form-data" autocomplete="off">
      <div class="col-md-3 form-group">
        <label class="text-warning">الاسم بالكامل‎</label>
        <input type="text" name="fullname" id="fullname" class="form-control req" placeholder="الاسم الكامل" value="<?php echo $fullname;?>" />
      </div>
      <div class="col-md-3 form-group">
        <label class="text-warning">صلة القرابة</label>
        <?php echo $this->haya_model->create_dropbox_list('relation','user_relation',$relation,0,'req'); ?> </div>
     <div class="col-md-3 form-group">
        <label class="text-warning">تاريخ الميلاد</label>
        <input type="text" name="dateofbirth" id="dateofbirth" class="datepicker form-control req" placeholder="تاريخ الميلاد" value="<?php echo $dateofbirth;?>" />
      </div>
      <div class="form-group col-md-3">
       <label class="text-warning">وثيقة</label>
       <input style="border: 0px !important; color: #d09c0d;" type="file" name="family_doc" id="family_doc" title='وثيقة'>
      </div>
      <input type="hidden" name="ufamilyid" id="ufamilyid" value="<?php echo $ufamilyid;?>"/>
      <input type="hidden" name="data_table_id" id="data_table_id" value="<?php echo '4';?>"/>
      <input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>"/>
      
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group  col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="add_family_mem();" value="حفظ" />
    </div>
  </div>
</div>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
		});
	});
</script>
<?php $segment		=	$this->uri->segment(1);?>
<?php $text			=	$this->lang->line($segment);?>
<?php $labels		=	$text['users']['form'];?>
<?php $permissions	=	$this->haya_model->check_other_permission(array('129','130','131','132','133','134','142'));?>

<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="col-md-12">
          <ul class="nav nav-tabs panel panel-default panel-block">
            <li class="tabsdemo-1 active"><a href="#tabsdemo-1" data-toggle="tab">تعديل المستخدم</a></li>
            <li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
            <?php if($permissions[130]['v']	==	1):?>
            	<li class="tabsdemo-2"><a href="#tabsdemo-2" data-toggle="tab">إضافة حساب البنك</a></li>
            	<li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
            <?php endif;?>
            <?php if($permissions[131]['v']	==	1):?>
            	<li class="tabsdemo-3"><a href="#tabsdemo-3" data-toggle="tab">إضافة الاتصالات</a></li>
            	<li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
            <?php endif;?>
            <?php if($permissions[132]['v']	==	1):?>
            	<li class="tabsdemo-4"><a href="#tabsdemo-4" data-toggle="tab">إضافة أفراد الأسرة</a></li>
            	<li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
            <?php endif;?>
            <?php if($permissions[133]['v']	==	1):?>
            	<li class="tabsdemo-5"><a href="#tabsdemo-5" data-toggle="tab">إضافة الوثيقة</a></li>
            	<li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
            <?php endif;?>
            <?php if($permissions[129]['v']	==	1):?>
            	<li class="tabsdemo-6"><a href="#tabsdemo-6" data-toggle="tab">إضافة الراتب</a></li>
            	<li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
            <?php endif;?>
            <?php if($permissions[134]['v']	==	1):?>
            	<li class="tabsdemo-7"><a href="#tabsdemo-7"  data-toggle="tab">إضافة الخبرة</a></li>
            	<li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
            <?php endif;?>
            <?php if($permissions[142]['v']	==	1):?>
            <!--            <li class="tabsdemo-8"><a href="#tabsdemo-8"  data-toggle="tab">طلب مغادرة</a></li>
                <li style="display:none;"><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>-->
            <?php endif;?>
            <?php if($permissions[129]['v']	==	1):?>
            <!-- <li class="tabsdemo-9" ><a href="#tabsdemo-9"  data-toggle="tab">الراتب الاساسي</a></li>-->
            <?php endif;?>
            	<li class="tabsdemo-10" ><a href="#tabsdemo-10"  data-toggle="tab">علاوات‎</a></li>
            	<li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
            	<li class="tabsdemo-11" ><a href="#tabsdemo-11"  data-toggle="tab">خصومات‎</a></li>
            	<li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
            	<li class="tabsdemo-12" ><a href="#tabsdemo-12"  data-toggle="tab">التأمينات‎</a></li>
            <?PHP foreach($flist as $res) { ?>
            <?php if($res->module_parent	==	'47'):?>
            <?php $permissions	=	$this->haya_model->check_other_permission(array($res->moduleid));?>
				<?php if($permissions[$res->moduleid]['v']	==	1):?>
                    <li><img style="margen-top:5px;" src="<?php echo base_url();?>assets/images/sep.png" width="10" height="30"></li>
                    <!--<li class="tabsdemo-12" ><a onclick="alatadad(this);" data-icon="<?PHP echo($res->module_icon); ?>" data-heading="<?PHP echo($res->module_name); ?>" href="#" data-url="<?PHP echo base_url().$res->module_controller; ?>"><?PHP echo($res->module_name); ?></a></i>-->
                    <li class="tabsdemo-m-<?php echo $res->moduleid;?>" ><a href="#tabsdemo-m-<?php echo $res->moduleid;?>"  data-toggle="tab"><?PHP echo($res->module_name); ?>‎</a></li>
                <?php endif;?>
			<?php endif;?>
            <?PHP } ?>
          </ul>
        </div>
        <div class="col-md-12">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <?php if($permissions[142]['v']	==	1):?>
              <?php else:?>
              <?php endif?>
              <form action="<?php echo current_url();?>" method="POST" id="add_user_form" name="add_user_form" enctype="multipart/form-data" autocomplete="off">
                <div class="col-md-12">
                  <div class="panel panel-default panel-block">
                    <div class="list-group">
                      <div class="list-group-item" id="input-fields">
                        <div class="form-group col-md-2">
                          <label class="text-warning">الرقم الوظيفي:</label>
                          <input type="text" class="form-control req NumberInput" name="id_number" id="id_number" placeholder="الرقم الوظيفي" value="<?php echo $id_number;?>"/>
                        </div>
                        <div class="form-group col-md-2">
                          <label class="text-warning">الرقم المدني:</label>
                          <input type="text" class="form-control req NumberInput" name="idcardNumber" id="idcardNumber" placeholder="الرقم المدنية" value="<?php echo $idcardNumber;?>"/>
                        </div>
                        <div class="form-group col-md-4">
                          <label class="text-warning"><?php echo $labels['fullname']?>:</label>
                          <input type="text" class="form-control req" name="fullname" id="fullname" placeholder="<?php echo $labels['fullname']?>" value="<?php echo $fullname;?>"/>
                        </div>
                        <div class="form-group col-md-2">
                          <label class="text-warning"><?php echo $labels['userlogin']?>:</label>
                          <input type="text" class="form-control req" name="userlogin" id="userlogin" placeholder="<?php echo $labels['userlogin']?>"  value="<?php echo $userlogin;?>"/>
                        </div>
                        <div class="form-group col-md-2">
                          <label class="text-warning"><?php echo $labels['userpassword']?>:</label>
                          <input type="password" class="form-control <?php if(!$userpassword):?> req <?php endif;?>" name="user_password" id="user_password" autocomplete="off"/>
                        </div>
                        <div class="form-group col-md-3">
                          <label class="text-warning"><?php echo $labels['email']?>:</label>
                          <input type="text" class="form-control req" name="email" id="email" placeholder="<?php echo $labels['email']?>" value="<?php echo $email;?>"/>
                        </div>
                        <div class="form-group  col-md-3" >
                          <label class="text-warning"><?php echo $labels['userroleid']?></label>
                          <?php echo $this->haya_model->create_dropbox_list('userroleid','user_role',$userroleid);?> </div>
                        <div class="form-group  col-md-3">
                          <label class="text-warning"><?php echo $labels['gender']?></label>
                          <select name="gender" class="form-control req">
                            <option value=""><?php echo $labels['gender']?></option>
                            <option value="ذكر" <?php if($gender	==	'ذكر'):?>selected="selected"<?php endif;?> >ذكر</option>
                            <option value="أنثى" <?php if($gender	==	'أنثى'):?>selected="selected"<?php endif;?> >أنثى</option>
                          </select>
                        </div>
                        <div class="form-group  col-md-3">
                          <label class="text-warning"><?php echo $labels['maritialstatus']?></label>
                          <?php echo $this->haya_model->create_dropbox_list('maritialstatus','marital_status',$maritialstatus,0,'req'); ?> </div>
                        <div class="form-group  col-md-3">
                          <label class="text-warning">مهنة</label>
                          <?php echo $this->haya_model->create_dropbox_list('profession','profession',$profession,0,'req'); ?> </div>
                        <div class="form-group  col-md-3">
                          <label class="text-warning">مهنة</label>
                          <?php echo $this->haya_model->create_dropbox_list('sub_profession','profession',$sub_profession,$profession,'req'); ?> </div>
                        <div class="form-group  col-md-3">
                          <label class="text-warning"><?php echo $labels['nationality']?></label>
                          <?php echo $this->haya_model->create_dropbox_list('nationality','nationality',$nationality,0,'req'); ?> </div>
                        <div class="form-group  col-md-3">
                          <label class="text-warning"><?php echo $labels['numberofdependens']?></label>
                          <select name="numberofdependens" class="form-control">
                            <?php for($i	=	1;	$i	<=	20;	$i++):?>
                            <option value="<?php echo $i;?>" <?php if($numberofdependens	==	$i):?>selected="selected"<?php endif;?> ><?php echo $i;?></option>
                            <?php endfor;?>
                          </select>
                        </div>
                        <?php //if($branchid	==	NULL):?>
                        <div class="form-group col-md-3">
                          <label class="text-warning"><?php echo $labels['branchid']?></label>
                          <select name="branchid" class="form-control">
                            <?php foreach($this->users->get_all_branches() as $branch):?>
                            <option value="<?php echo $branch->branchid;?>" <?php if($branch->branchid	==	$branchid):?>selected="selected"<?php endif;?> ><?php echo $branch->branchname;?></option>
                            <?php endforeach;?>
                          </select>
                        </div>
                        <div class="form-group col-md-3">
                          <label class="text-warning">اختيار مدير</label>
                          <select name="manager_id" id="manager_id" class="form-control req">
                            <option value="">اختيار مدير</option>
                            <?php foreach($this->users->get_all_managers() as $manager):?>
                            <option value="<?php echo $manager->userid;?>" <?php if($manager->userid	==	$manager_id):?>selected="selected" <?php endif;?> ><?php echo $manager->fullname;?></option>
                            <?php endforeach;?>
                          </select>
                        </div>
                        <?php //endif;?>
                        <div class="col-md-2 form-group">
                          <label class="text-warning">تاريخ الميلاد:</label>
                          <input type="text" name="date_of_birth" id="date_of_birth" class="datepicker form-control req" placeholder="تاريخ الميلاد‎" value="<?php echo $date_of_birth;?>" />
                        </div>
                        <div class="col-md-2 form-group">
                          <label class="text-warning">تاريخ الانضمام:</label>
                          <input type="text" name="joining_date" id="joining_date" class="datepicker form-control req" placeholder="تاريخ الانضمام" value="<?php echo $joining_date;?>" />
                        </div>
                        <div class="col-md-2 form-group">
                          <label class="text-warning">الدرجة</label>
                          <select class="form-control req" name="level_id"  id="level_id"  placeholder="الدرجة">
                            <?php foreach($this->users->get_all_levels() as $level):?>
                            <option value="<?php echo $level->level_id;?>" <?php if($level->level_id	==	$level_id):?>selected="selected" <?php endif;?>><?php echo $level->level_name;?></option>
                            <?php endforeach;?>
                          </select>
                        </div>
                        <br clear="all" />
                        <div class="form-group col-md-4">
                          <label class="text-warning"><?php echo $labels['profilepic']?></label>
                          <input style="border: 0px !important; color: #d09c0d;" type="file" name="profilepic" title='تحميل'>
                        </div>
                        <?php if($is_profilepic):?>
                        <div class="form-group col-md-4"> <img class="img-thumbnail" style="width:150px !important;" src="<?php echo base_url();?>resources/users/<?php echo $userid;?>/<?php echo $is_profilepic;?>"/> </div>
                        <?php endif;?>
                        <div class="form-group col-md-4 ">
                          <label class="text-warning"><?php echo $labels['resume']?></label>
                          <input style="border: 0px !important; color: #d09c0d;" type="file" name="resume" title='تحميل'>
                        </div>
                        <?php if($is_resume):?>
                        <!--<div class="form-group col-md-3">
                           <img src="<?php echo base_url();?>resources/applicants/<?php echo $userid;?>/<?php echo $is_resume;?>" width="150px" height="150px"/>
                        </div>-->
                        <?php endif;?>
                        <br clear="all" />
                        <?php if($is_profilepic):?>
                        <input type="hidden" name="is_profilepic" id="is_profilepic" value="<?php echo $is_profilepic;?>" />
                        <?php endif;?>
                        <?php if($is_resume):?>
                        <input type="hidden" name="is_resume" id="is_resume" value="<?php echo $is_resume;?>" />
                        <?php endif;?>
                        <?php if($userid):?>
                        	<input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>" />
                        <?php endif;?>
                        <input type="button" id="submit_user" class="btn btn-success btn-lrg" name="submit_user"  value="حفظ"  onclick="users_validations();"/>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-2">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                <?php //if($permissions[130]['a']	==	1):?>
                <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>users/add_bank_account/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة حساب البنك">إضافة حساب البنك</a> </div>
                <?php //endif;?>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;">اسم البنك</th>
                      <th style="text-align:center;">اسم الفرع</th>
                      <th style="text-align:center;">رقم الحساب</th>
                      <!--<th style="text-align:center;">اسم صاحب الحساب</th>-->
                      <th style="text-align:center;">الإجراءات</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $all_bank_accounts	=	$this->users->get_all_bank_accounts($userid);?>
                    <?php if(!empty($all_bank_accounts)):?>
                    <?php foreach($all_bank_accounts as $account):?>
                    <?php
						if($permissions[130]['d']	==	1)
						{
							$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$account->bankaccountid.'" data-url="'.base_url().'users/delete_bank_account/'.$account->bankaccountid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
						}
						if($permissions[130]['u']	==	1)
						{
							$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_bank_account/'.$userid.'/'.$account->bankaccountid.'" id="'.$account->bankaccountid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
						}
						if($account->bank_doc)
						{
							$actions	.=	'&nbsp;<a href="'.base_url().'users/download_file/'.$account->userid.'/'.$document->bank_doc.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
						}
					?>
                    <tr role="row" id="<?php echo $account->bankaccountid;?>_durar_lm">
                      <td  style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($account->bankid);?></td>
                      <td  style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($account->branchid);?></td>
                      <td  style="text-align:center;"><?php echo $account->accountnumber;?></td>
                      <!--<td  style="text-align:center;"><?php echo $account->accountfullname;?></td>-->
                      <td><?php echo $actions;?></td>
                    </tr>
                    <?php unset($actions); endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-3">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                <?php //if($permissions[131]['a']	==	1):?>
                <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>users/add_communication/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                <?php //endif;?>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">نوع الاتصال</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">تفاصيل الاتصال</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">التاريخ</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $all_communications	=	$this->users->get_all_communications($userid);?>
                    <?php if(!empty($all_communications)):?>
                    <?php foreach($all_communications as $communication):?>
                    <?php
							if($permissions[131]['d']	==	1)
							{
								$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$communication->communicationid.'" data-url="'.base_url().'users/delete_communication/'.$communication->communicationid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
							}
							if($permissions[131]['u']	==	1)
							{
								$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_communication/'.$userid.'/'.$communication->communicationid.'" id="'.$communication->communicationid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
							}
							if($communication->communication_doc)
							{
								$actions	.=	'&nbsp;<a href="'.base_url().'users/download_file/'.$communication->userid.'/'.$communication->communication_doc.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
							} 
					?>
                    <tr role="row" id="<?php echo $communication->communicationid;?>_durar_lm">
                      <td style="text-align:center;" ><?php echo $this->haya_model->get_name_from_list($communication->communicationtype);?>
                        </th>
                      <td style="text-align:center;" ><?php echo $communication->communicationvalue;?></td>
                      <td style="text-align:center;" ><?php echo $communication->addingdate;?></td>
                      <td style="text-align:center;" ><?php echo $actions;?></td>
                    </tr>
                    <?php unset($actions); endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-4">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                <?php //if($permissions[132]['a']	==	1):?>
                <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>users/add_family_member/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                <?php //endif;?>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الاسم الكامل</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">العلاقة</th>
<!--                  <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">تاريخ الميلاد</th>
-->
                     <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">رقم الاتصال</th>
                     <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $all_family_members	=	$this->users->get_all_family_members($userid);?>
                    <?php if(!empty($all_family_members)):?>
                    <?php foreach($all_family_members as $member):?>
                    <?php
						if($permissions[132]['d']	==	1)
						{
							$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$member->ufamilyid.'" data-url="'.base_url().'users/delete_family_member/'.$member->ufamilyid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
						}
						if($permissions[132]['u']	==	1)
						{
							$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_family_member/'.$userid.'/'.$member->ufamilyid.'" id="'.$member->ufamilyid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
						}
						if($member->family_doc)
						{
							$actions	.=	'&nbsp;<a href="'.base_url().'users/download_file/'.$member->userid.'/'.$member->family_doc.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
						} 
					?>
                    <tr role="row" id="<?php echo $member->ufamilyid;?>_durar_lm">
                      <td style="text-align:center;" ><?php echo $member->fullname;?></td>
                      <td style="text-align:center;" ><?php echo $this->haya_model->get_name_from_list($member->relation);?></td>
                      <td style="text-align:center;" ><?php echo $member->dateofbirth;?></td>
                      <td style="text-align:center;" ><?php echo $actions;?></td>
                    </tr>
                    <?php unset($actions); endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-5">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                <?php //if($permissions[133]['a']	==	1):?>
                <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>users/add_document/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                <?php //endif;?>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">نوع الوثيقة</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">البلد</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">تاريخ الإصدار</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">تاريخ انتهاء الصلاحية</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $all_documents	=	$this->users->get_all_documents($userid);?>
                    <?php if(!empty($all_documents)):?>
                    <?php foreach($all_documents as $document) :?>
                    <?php
						if($permissions[133]['d']	==	1)
						{
							$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$document->udocid.'" data-url="'.base_url().'users/delete_document/'.$document->udocid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
						}
						if($permissions[133]['u']	==	1)
						{
							$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_document/'.$userid.'/'.$document->udocid.'" id="'.$document->udocid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
						}
						if($document->document)
						{
							$actions	.=	'&nbsp;<a href="'.base_url().'users/download_file/'.$document->userid.'/'.$document->document.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
						}
					?>
                    <tr role="row" id="<?php echo $document->udocid.'_durar_lm';?>">
                      <td style="text-align:center;" ><?php echo $this->haya_model->get_name_from_list($document->documenttype);?></td>
                      <td style="text-align:center;" ><?php echo $this->haya_model->get_name_from_list($document->issuecountry);?></td>
                      <td style="text-align:center;" ><?php echo $document->issuedate;?></td>
                      <td style="text-align:center;" ><?php echo $document->expirydate;?></td>
                      <td style="text-align:center;" ><?php echo $actions;?></td>
                    </tr>
                    <?php unset($actions); endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-6">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                <?php //if($permissions[129]['a']	==	1):?>
                <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>users/add_salary/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة الراتب">إضافة الراتب</a> </div>
                <?php //endif;?>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">رقم الحساب</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">نوع الراتب</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">مبلغ</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الراتب الاساسي</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">بدلات</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">اجمالي الراتب</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الراتب الشهري</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $all_salaries	=	$this->users->get_all_salaries($userid);?>
                    <?php //echo '<pre>';print_r($all_salaries);?>
                    <?php if(!empty($all_salaries)):?>
                    <?php foreach($all_salaries as $salary):?>
                    <?php
						if($permissions[129]['d']	==	1)
						{
							$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$salary->salaryid.'" data-url="'.base_url().'users/delete_salary/'.$salary->salaryid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
						}
						if($permissions[129]['u']	==	1)
						{
							$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_salary/'.$userid.'/'.$salary->salaryid.'" id="'.$salary->salaryid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
						}
						
						if($salary->certificate)
						{
							$actions	.=	'&nbsp;<a href="'.base_url().'users/download_file/'.$salary->userid.'/'.$salary->certificate.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
						}
						
						$total_salary	=	($salary->basic_salary + $salary->allownces);
					?>
                    <?php if($salary->basic_salary):?>
                    <tr role="row" id="<?php echo $salary->salaryid.'_durar_lm';?>">
                      <td style="text-align:center;" ><?php echo $this->haya_model->get_accnumber_by_id($salary->bankaccoundid);?></td>
                      <td style="text-align:center;" ><?php echo $this->haya_model->get_name_from_list($salary->salary_type);?></td>
                      <td style="text-align:center;" ><?php echo $salary->total_salary;?></td>
                      <td style="text-align:center;" ><?php echo $salary->basic_salary;?></td>
                      <td style="text-align:center;" ><?php echo $salary->allownces;?></td>
                      <td style="text-align:center;" ><?php echo $total_salary;?></td>
                      <td style="text-align:center;" ><?php echo $salary->salarydate;?></td>
                      <td style="text-align:center;" ><?php echo $actions;?></td>
                    </tr>
                    <?php endif;?>
                    <?php unset($actions); endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-7">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                <?php //if($permissions[134]['a']	==	1):?>
                <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>users/add_experience/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                <?php //endif;?>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">العنوان</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">البلد</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">تاريخ البدء</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">تاريخ الانتهاء</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">التفاصيل</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $all_experiences	=	$this->users->get_all_experiences($userid);?>
                    <?php if(!empty($all_experiences)):?>
                    <?php foreach($all_experiences as $experience):?>
                    <?php
							if($permissions[134]['d']	==	1)
							{
								$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$experience->exe_id.'" data-url="'.base_url().'users/delete_experience/'.$experience->exe_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
							}
							if($permissions[134]['u']	==	1)
							{
								$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_experience/'.$userid.'/'.$experience->exe_id.'" id="'.$experience->exe_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
							}
							if($experience->certificate)
							{
								$actions	.=	'&nbsp;<a href="'.base_url().'users/download_file/'.$experience->userid.'/'.$experience->certificate.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
							}
						?>
                    <tr role="row" id="<?php echo $experience->udocid.'_durar_lm';?>">
                      <td style="text-align:center;" ><?php echo $experience->exe_title;?></td>
                      <td style="text-align:center;" ><?php echo $this->haya_model->get_name_from_list($experience->country);?></td>
                      <td style="text-align:center;" ><?php echo date('Y-m-d',strtotime($experience->start_date));?></td>
                      <td style="text-align:center;" ><?php echo date('Y-m-d',strtotime($experience->end_date));?></td>
                      <td style="text-align:center;" ><?php echo $experience->detail;?></td>
                      <td style="text-align:center;" ><?php echo $actions;?></td>
                    </tr>
                    <?php unset($actions); endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-8">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                <?php //if($permissions[142]['a']	==	1):?>
                <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>users/add_leave_request/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                <?php //endif;?>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الأسم الكامل</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">السبب</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">من</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">إلى</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجازة اليومية</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">ترك طلب الحالة</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $leave_requests	=	$this->users->get_all_leave_requests($userid);?>
                    <?php if(!empty($leave_requests)):?>
                    <?php foreach($leave_requests as $leave):?>
                    <?php
							$actions 	 =  '	<a  onclick="alatadad(this);" data-url="'.base_url().'users/view_request/'.$leave->leaveid.'"  href="#"><i class="icon-eye-open"></i></a>';
			
							if($permissions[142]['u']	==	1)
							{
								//$actions 	.=	'<a href="#addingDiag" onclick="alatadad(this);" data-url="'.base_url().'users/add_leave_request/'.$leave->leaveid.'" id="'.$leave->leaveid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
							}
							
							if($leave->approved	==	'0')
							{	
								$request	=	'تحت المعالجة';
							}
							elseif($leave->approved	==	'1')
							{
								$request	=	'<img src="'.base_url().'assets/images/approved.png" style="width: 24px;" alt="Approved" title="Approved"/>';
							}
							else
							{
								$request	=	'<img src="'.base_url().'assets/images/not-approved.png" style="width: 24px;" alt="Reject" title="Reject"/>';
							}
						?>
                    <tr role="row" id="<?php echo $leave->leaveid.'_durar_lm';?>">
                      <td style="text-align:center;" ><?php echo $leave->fullname;?></td>
                      <td style="text-align:center;" ><?php echo $this->haya_model->get_name_from_list($leave->reasonid);?></td>
                      <td style="text-align:center;" ><?php echo date('d-m-Y',strtotime($leave->start_date));?></td>
                      <td style="text-align:center;" ><?php echo date('Y-m-d',strtotime($leave->end_date));?></td>
                      <td style="text-align:center;" ><?php echo $leave->total_leaves;?></td>
                      <td style="text-align:center;" ><?php echo $request;?></td>
                      <td style="text-align:center;" ><?php echo $actions;?></td>
                    </tr>
                    <?php unset($actions); endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-9">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                <?php //if($permissions[129]['a']	==	1):?>
                <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>users/add_basic_salary/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة الراتب">إضافة الراتب</a> </div>
                <?php // endif;?>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">رقم الحساب</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الملاحظات</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">التاريخ</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $all_salaries	=	$this->users->get_all_basic_salaries($userid);?>
                    <?php if(!empty($all_salaries)):?>
                    <?php foreach($all_salaries as $salary):?>
                    <?php
						if($permissions[129]['d']	==	1)
						{
							$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$salary->salaryid.'" data-url="'.base_url().'users/delete_basic_salary/'.$salary->salaryid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
						}
						if($permissions[129]['u']	==	1)
						{
							$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_basic_salary/'.$userid.'/'.$salary->salaryid.'" id="'.$salary->salaryid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
						}
					?>
                    <tr role="row" id="<?php echo $salary->salaryid.'_durar_lm';?>">
                      <td style="text-align:center;" ><?php echo $salary->amount;?></td>
                      <td style="text-align:center;" ><?php echo $salary->notes;?></td>
                      <td style="text-align:center;" ><?php echo $salary->submit_date;?></td>
                      <td style="text-align:center;" ><?php echo $actions;?></td>
                    </tr>
                    <?php unset($actions); endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-10">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>users/add_extra_things/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">اسم علاوات‎‎‎</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">القيمة</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">تاريخ</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الملاحظات</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $all_extra_things	=	$this->users->get_extra_things($userid,'EXTRA');?>
                    <?php if(!empty($all_extra_things)):?>
                    <?php foreach($all_extra_things as $thing):?>
                    <?php 
						$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_extra_things/'.$userid.'/'.$thing->extra_id.'" id="'.$thing->extra_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
						if($thing->certificate)
						{
							$actions	.=	'&nbsp;<a href="'.base_url().'users/download_file/'.$thing->userid.'/'.$thing->certificate.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
						}
					?>
                    <tr role="row" id="<?php echo $thing->extra_id.'_durar_lm';?>">
                      <td style="text-align:center;" ><?php echo $thing->title;?></td>
                      <td style="text-align:center;" ><?php echo $thing->amount;?></td>
                      <td style="text-align:center;" ><?php echo date('Y-m-d',strtotime($thing->submit_date));?></td>
                      <td style="text-align:center;" ><?php echo $thing->notes;?></td>
                      <td style="text-align:center;" ><?php echo $actions;?></td>
                    </tr>
                    <?php unset($actions); endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-11">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>users/add_deduction_things/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">اسم خصومات‎</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">القيمة</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">تاريخ</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الملاحظات</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $all_extra_things	=	$this->users->get_extra_things($userid,'DEDUCTION');?>
                    <?php if(!empty($all_extra_things)):?>
                    <?php foreach($all_extra_things as $thing):?>
                    <?php 
						$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_deduction_things/'.$userid.'/'.$thing->extra_id.'" id="'.$thing->extra_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
						if($thing->certificate)
						{
							$actions	.=	'&nbsp;<a href="'.base_url().'users/download_file/'.$thing->userid.'/'.$thing->certificate.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
						}
					?>
                    <tr role="row" id="<?php echo $thing->extra_id.'_durar_lm';?>">
                      <td style="text-align:center;" ><?php echo $thing->title;?></td>
                      <td style="text-align:center;" ><?php echo $thing->amount;?></td>
                      <td style="text-align:center;" ><?php echo date('Y-m-d',strtotime($thing->submit_date));?></td>
                      <td style="text-align:center;" ><?php echo $thing->notes;?></td>
                      <td style="text-align:center;" ><?php echo $actions;?></td>
                    </tr>
                    <?php unset($actions); endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane list-group" id="tabsdemo-12">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                <div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>users/add_insurance_things/<?php echo $userid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">اسم التأمينات‎‎</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">القيمة</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">تاريخ</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الملاحظات</th>
                      <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $all_extra_things	=	$this->users->get_extra_things($userid,'INSURANCE');?>
                    <?php if(!empty($all_extra_things)):?>
                    <?php foreach($all_extra_things as $thing):?>
                    <?php 
						$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'users/add_insurance_things/'.$userid.'/'.$thing->extra_id.'" id="'.$thing->extra_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
						if($thing->certificate)
						{
							$actions	.=	'&nbsp;<a href="'.base_url().'users/download_file/'.$thing->userid.'/'.$thing->certificate.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
						}
					?>
                    <tr role="row" id="<?php echo $thing->extra_id.'_durar_lm';?>">
                      <td style="text-align:center;" ><?php echo $thing->title;?></td>
                      <td style="text-align:center;" ><?php echo $thing->amount;?></td>
                      <td style="text-align:center;" ><?php echo date('Y-m-d',strtotime($thing->submit_date));?></td>
                      <td style="text-align:center;" ><?php echo $thing->notes;?></td>
                      <td style="text-align:center;" ><?php echo $actions;?></td>
                    </tr>
                    <?php unset($actions); endforeach;?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <?php if(!empty($flist)):?>
            <?php foreach($flist as $res) { ?>
            <?php
            	$cu 			=	$res->custom_form;	
                $custom_form 	=	json_decode($cu, true);
             ?>
            <div class="tab-pane list-group" id="tabsdemo-m-<?php echo $res->moduleid;?>">
              <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <?php if($userid): ?>
                	<div class="row table-header-row"> <a class="btn btn-success" style="float:right;margin-top:15px;margin-right: 10px;" href="#globalDiag" onClick="alatadad(this);" data-url="<?php echo base_url()."lease_programe/customform/".$res->moduleid."/".$userid; ?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                <?php endif;?>
                <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <?php foreach($custom_form['data'] as $cfkey => $cfvalue) { ?>
                      <th style="text-align:center;"><?php echo $cfvalue['fn']; ?>‎</th>
                      <?php }?>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $formValue		=	$this->users->getCustomeFormValue($res->moduleid,$userid);?>
                    <?php //echo $res->moduleid.$userid;?>
                    <?php //print_r($formValue);?>
                    <?php if($formValue):?>
                    <?php foreach($formValue as $fv) { ?>
                    <tr role="row" id="">
                      <?php $json_decode	=	json_decode($fv->moduledata);?>
                      <?php //echo '<pre>'; print_r($json_decode);?>
                      <?php //echo '<pre>'; print_r($custom_form['data']);?>
                      <?php foreach($custom_form['data'] as $cfkey => $cfvalue) { ?>
                      <?php $key_value	=	$cfvalue['fn'];?>
                      <td style="text-align:center;"><?php if($cfvalue['ft']	==	'select'):?>
                        <?php echo $this->haya_model->get_name_from_list($json_decode->$key_value);?>
                        <?php elseif($cfvalue['ft']	==	'radio'):?>
                        <?php echo $this->haya_model->get_name_from_list($json_decode->$key_value);?>
                        <?php elseif($cfvalue['ft']	==	'checkbox'):?>
                        <?php $get_check_box_value	=	$this->users->query_in($json_decode->$key_value);?>
                        <?php if(!empty($get_check_box_value)):?>
                        <?php foreach($get_check_box_value	as	$bval):?>
                        <li><?php echo $bval->list_name;?></li>
                        <?php endforeach;?>
                        <?php endif;?>
                        <?php else:?>
                        <?php echo $json_decode->$key_value;?>
                        <?php endif;?></td>
                      <?php }?>
                    </tr>
                    <?php }?>
                    <?php endif;?>
                  </tbody>
                </table>
              </div>
            </div>
            <?php }?>
            <?php endif;?>
            <!-- ---------------------------------------------------------------------- --> 
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<!-- /.modal-dialog --> 
<script>

$(function(){
    var myDate = new Date();
    var maxYearforBirth = myDate.getFullYear()-17;
    var minYearforBirth = maxYearforBirth-80;
   
	$( "#date_of_birth" ).datepicker({
		changeMonth: true,
		changeYear: true,
        yearRange:minYearforBirth+':'+maxYearforBirth,
		dateFormat:'yy-mm-dd'
		});
    
    	$( "#joining_date" ).datepicker({
		changeMonth: true,
		changeYear: true,
        yearRange:minYearforBirth+':'+myDate.getFullYear(),
		dateFormat:'yy-mm-dd'
		});
	});
$('.nav-tabs a[href="#tabsdemo-<?php echo $table_id;?>"]').tab('show');

</script>
</div>
</body>
</html>
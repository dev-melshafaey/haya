<?php $segment	=	$this->uri->segment(1);?>
<?php $text		=	$this->lang->line($segment);?>
<?php $labels	=	$text['users']['listing'];?>

<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
<div id="baseDateControl">
 <div class="dateControlBlock">
        Between <input type="text" name="dateStart" id="dateStart" class="datepicker" value="09/01/2010" size="8" /> and 
        <input type="text" name="dateEnd" id="dateEnd" class="datepicker" value="12/31/2010" size="8"/>
    </div>
</div>

<table id="commentTable" class="dataTablesTable" border="1" cellpadding="2" cellspacing="2" width="100%">
	<thead>
		<tr id="theadRow">
			<th><span>Date Submitted</span></th>
            <th><span>Commenter</span></th>
            <th width="60%"><span>Comment</span></th>
		</tr>
	</thead>
	<tbody>
		<tr>
        	<td>09/05/2010</td>
			<td>Donald Justice</td>
			<td>I'm really a big fan of the dataTables jQuery plugin.  It's simple to set up for basic 
            table managment yet provides you with the flexibility to do more complex tasks.</td>
		</tr>
        <tr>
        	<td>09/20/2010</td>
			<td>Walter Fowler</td>
			<td>It never ceases to amaze me how easy it can be to build solid user interfaces when you have the right tools at your disposal.</td>
		</tr>
		<tr>
			<td>10/13/2010</td>
            <td>Ferris Butler</td>
			<td>I'm definitely going to check out this plugin.</td>
		</tr>
		<tr>
			<td>10/29/2010</td>
            <td>Lonnie Belfort</td>
			<td>Does anyone know if there's a way to configure dataTables so that it'll retain the current table state (the filter, the sorting) if you move to another page then come back to it?</td>
		</tr>
		<tr>
			<td>11/05/2010</td>
            <td>George Park</td>
			<td>Lonnie:  Yes, there is a bStateSave boolean setting you can set to true to do that.  It'll then save the current state of the table in a cookie.</td>
		</tr>
		<tr>
			<td>12/06/2010</td>
            <td>Lonnie Belfort</td>
			<td>Hey, George, thanks for the info!</td>	
		</tr>
	</tbody>
</table>

<div id="endBlock"></div>


</div>
  </div>
</section>
<script>
/*$(function(){
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});*/
</script>
<script>
// The plugin function for adding a new filtering routine
$.fn.dataTableExt.afnFiltering.push(
		function(oSettings, aData, iDataIndex){
			var dateStart = parseDateValue($("#dateStart").val());
			var dateEnd = parseDateValue($("#dateEnd").val());
			// aData represents the table structure as an array of columns, so the script access the date value 
			// in the first column of the table via aData[0]
			var evalDate= parseDateValue(aData[0]);
			
			if (evalDate >= dateStart && evalDate <= dateEnd) {
				return true;
			}
			else {
				return false;
			}
			
		});

// Function for converting a mm/dd/yyyy date value into a numeric string for comparison (example 08/12/2010 becomes 20100812
function parseDateValue(rawDate) {
	var dateArray= rawDate.split("/");
	var parsedDate= dateArray[2] + dateArray[0] + dateArray[1];
	return parsedDate;
}


$(function() {
	// Implements the dataTables plugin on the HTML table
	var $dTable= $("table.dataTablesTable").dataTable( {
			"iDisplayLength": 200,
			"bStateSave": false,
			"oLanguage": {
			"sLengthMenu": 'Show <select><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option></select> entries'
		},
			"aaSorting": [[0,'asc']],
			"aoColumns": [
          		{ "sType": "date" },
				null,
           		null
        ]
	});
	
	// The dataTables plugin creates the filtering and pagination controls for the table dynamically, so these 
	// lines will clone the date range controls currently hidden in the baseDateControl div and append them to 
	// the feedbackTable_filter block created by dataTables
	$dateControls= $("#baseDateControl").children("div").clone();
	$("#feedbackTable_filter").prepend($dateControls);

	// Implements the jQuery UI Datepicker widget on the date controls
	$('.datepicker').datepicker(
		{showOn: 'button', buttonImage: '/thoughts/demos/dataTablesDateRange/images/datePicker/calendar.gif', buttonImageOnly: true}
	);		
	
	// Create event listeners that will filter the table whenever the user types in either date range box or
	// changes the value of either box using the Datepicker pop-up calendar
	$("#dateStart").keyup ( function() { $dTable.fnDraw(); } );
	$("#dateStart").change( function() { $dTable.fnDraw(); } );
	$("#dateEnd").keyup ( function() { $dTable.fnDraw(); } );
	$("#dateEnd").change( function() { $dTable.fnDraw(); } );

});

</script>

<?php $this->load->view('common/footer'); ?>
</div>

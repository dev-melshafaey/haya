<div id="user-profile" style="background-color:#FFF !important;">
  <h4 class="panel-title customhr">بيانات المستخدم</h4>
  <div class="col-md-10">
    <div class="col-md-4 form-group">
      <label class="text-warning">الأسم بالكامل:</label><br><?php echo $user_data['users']->fullname; ?></div>
    <div class="col-md-4 form-group">
      <label class="text-warning">اسم المستخدم:</label><br><?php echo $user_data['users']->userlogin; ?></div>            
    <div class="col-md-4 form-group">
      <label class="text-warning">البريد الإلكتروني:</label><br><?php echo $user_data['users']->email; ?></div>
      
    <div class="col-md-4 form-group">
      <label class="text-warning">نوع المستخدم في النظام:</label><br><?php echo $this->haya_model->get_name_from_list($user_data['users']->userroleid); ?></div>      
    <div class="col-md-4 form-group">
      <label class="text-warning">الجنس:</label><br><?php echo $user_data['users']->gender; ?></div>
    <div class="col-md-4 form-group">
      <label class="text-warning">الحالة الإجتماعية:</label><br><?php echo $this->haya_model->get_name_from_list($user_data['users']->maritialstatus); ?></div>
    
    <div class="col-md-4 form-group">
      <label class="text-warning">الجنسية:</label><br><?php echo $this->haya_model->get_name_from_list($user_data['users']->nationality); ?></div>  
    <div class="col-md-4 form-group">
      <label class="text-warning">عدد أشخاص المعالين:</label><br><?php echo a_date($user_data['users']->numberofdependens); ?></div>
    <div class="col-md-4 form-group">
      <label class="text-warning">الفرع:</label>
      <?PHP
        		$bDetail = $this->haya_model->get_branch_information($user_data['users']->branchid);
		?>
      <br><?php echo $bDetail->branchname; ?></div>  
    
    <div class="col-md-4 form-group">
      <label class="text-warning">تاريخ الميلاد:</label><br><?php echo a_date(date("Y-m-d",strtotime($user_data['users']->date_of_birth))); ?></div>
    <div class="col-md-4 form-group">
      <label class="text-warning">الدرجة:</label><br><?php echo $this->users->get_level_name($user_data['users']->level_id); ?></div>  
    <div class="col-md-4 form-group" style="    margin-bottom: 0px !important; padding: 0px !important;">
        <form method="post" id="user-pass" name="user-pass" autocomplete="off">
          <div class="col-md-10" style="padding:0px !important;">
            <label class="text-warning">كلمة المرور</label>
            <input type="password" class="form-control" name="new_pass" id="new_pass" autocomplete="off">
          </div>
          <div class="col-md-2" style="padding:0px !important;">
            <input type="hidden" name="userid" id="userid" value="<?php echo $user_id;?>">
            <input style="margin-top:26px" type="button" id="update_user_pass" class="btn btn-success btn-lrg" name="update_user_pass" onclick="update_user();" value="تحديث">
            <span id="show-msg" style="margin-top:10px;"></span> </div>
        </form>
      </div>
    
    <div class="col-md-12 form-group">
      <label class="text-warning">مهنة:</label><br><?php echo $this->haya_model->get_name_from_list($user_data['users']->profession); ?> | <?php echo $this->haya_model->get_name_from_list($user_data['users']->sub_profession); ?></div>
  
  </div>
  <div class="col-md-2"> <img class="img-responsive" src="<?PHP echo base_url().'resources/users/'.$user_id.'/'.$user_data['users']->profilepic; ?>"> </div>
  
  
  <div class="row">
    <div class="col-md-12 fox leftborder">
      
      <div class="col-md-12 form-group">
        <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
          <h4 class="panel-title customhr">بيانات حساب البنك</h4>
          <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
            <?php
									  if(!empty($user_data['bankaccounts'])){
									  
											  foreach($user_data['bankaccounts'] as $banks){
									  ?>
            <div class="col-md-6 form-group">
              <label class="text-warning">إسم البنك:</label>
              <strong><?php echo $this->haya_model->get_name_from_list($banks->bankid); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الفرع:</label>
              <strong><?php echo $this->haya_model->get_name_from_list($banks->branchid); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الحساب:</label>
              <strong><?php echo $banks->accountnumber; ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">اسم صاحب الحساب:</label>
              <strong><?php echo $banks->accountfullname; ?></strong> </div>
            <div class="col-md-12 form-group"></div>
            <?php
											  }
									  }
							  ?>
          </div>
        </div>
      </div>
      <div class="col-md-12 form-group">
        <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
          <h4 class="panel-title customhr">بيانات الاتصالات</h4>
          <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
            <?php
            if(!empty($user_data['communication'])){
				foreach($user_data['communication'] as $communication){
													  ?>
            <div class="col-md-6 form-group">
<!--              <label class="text-warning">نوع الاتصال:</label>-->
              <label class="text-warning"><?php echo $this->haya_model->get_name_from_list($communication->communicationtype); ?> : </strong></label>
              <strong><?php echo $communication->communicationvalue; ?></strong> </div>
            <!--<div class="col-md-6 form-group">
              <label class="text-warning">رقم الاتصالات‎:</label>
              <strong><?php echo $communication->communicationvalue; ?></strong> </div>-->
          </div>
          <?php
											}
									}
							?>
        </div>
      </div>
      <div class="col-md-12 form-group">
        <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
          <h4 class="panel-title customhr">بيانات أفراد الأسرة</h4>
          <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
            <?php
											  if(!empty($user_data['family'])){
											  foreach($user_data['family'] as $family){
													  ?>
            <div class="col-md-6 form-group">
              <label class="text-warning">الاسم بالكامل:</label>
              <strong><?php echo $family->fullname; ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">صلة القرابة:</label>
              <strong><?php echo $this->haya_model->get_name_from_list($family->relation); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">تاريخ الميلاد:</label>
              <strong><?php echo arabic_date($family->dateofbirth); ?></strong> </div>
            <br clear="all" />
            <br clear="all" />
            <?php
											  }
									  }
									  ?>
          </div>
        </div>
      </div>
      <div class="col-md-12 form-group">
        <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
          <h4 class="panel-title customhr">البيانات الوثيقة</h4>
          <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
            <?php
			//echo '<pre>'; print_r($user_data['document']);
											  if(!empty($user_data['document'])){
													  foreach($user_data['document'] as $document){
									  ?>
                                    
            <div class="col-md-6 form-group">
              <label class="text-warning">نوع الوثيقة:</label>
              <strong><?php echo $this->haya_model->get_name_from_list($document->documenttype); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">بلد:</label>
              <strong><?php echo $this->haya_model->get_name_from_list($document->issuecountry); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">تاريخ الإصدار:</label>
              <strong><?php echo arabic_date($document->issuedate); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">تاريخ انتهاء الصلاحية:</label>
              <strong><?php echo arabic_date($document->expirydate); ?></strong> </div>
            <!--<div class="col-md-6 form-group">
              <label class="text-warning">وثيقة:</label>
              <strong><?php echo $yateem_banks_data->account_title; ?></strong> </div>-->
            <?php $url = 'resources/users/'.$user_id.'/'.$document->document;?>
           <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                      <div class="panel-heading" style="padding:10px 3px;" id="head<?PHP echo $document->udocid;?>">
                        <h4 class="panel-title" style="font-size:15px;">
                          <?PHP if($document->udocid!='') { ?>
                          <span class="icons" id="removeicons<?PHP echo $document->udocid; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$document->documenttype,$url); ?> 
                          <!--<i onClick="removeDocument(this);" data-id="<?PHP echo $document->udocid; ?>" data-remove="<?PHP echo $document->udocid; ?>" class="icon-remove-sign" style="color:#FF0000; cursor:pointer;"></i>--> 
                          </span>
                          <?PHP } ?>
                          <a style="width:95% !important;" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $document->udocid;?>"><?php echo $this->haya_model->get_name_from_list($document->documenttype);?></a> </h4>
                      </div>
                      <div id="demo-collapse<?PHP echo $document->udocid;?>" class="panel-collapse collapse">
                      </div>
                    </div>
          </div>
        </div>
        <?php
			 }
			}
		?>
      </div>
      <div class="col-md-12 form-group">
        <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
          <h4 class="panel-title customhr">البيانات الراتب</h4>
          <?php
									if(!empty($user_data['main_salary'])){  
											foreach($user_data['main_salary'] as $main_salary){
							 ?>
          <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الحساب:</label>
              <strong><?php echo $this->haya_model->get_name_from_list($main_salary['bankaccoundid']); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">نوع الراتب:</label>
              <strong><?php echo $this->haya_model->get_name_from_list($main_salary['salary_type']); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">مبلغ:</label>
              <strong><?php echo $main_salary['amount']; ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">شهر الراتب:</label>
              <strong><?php echo arabic_date($main_salary['salarydate']); ?></strong> </div>
            <?php
						  if(!empty($main_salary['detail'])){
											  foreach($main_salary['detail'] as $details){
									  ?>
            <div class="col-md-6 form-group">
              <label class="text-warning"><?php echo $this->haya_model->get_name_from_list($details->salarypaymenttype); ?>:</label>
              <strong><?php echo $details->amount; ?></strong> </div>
            <?php
							  }
									  }
									  ?>
          </div>
          <div class="col-md-12 form-group"></div>
          <?php
											}
									}
							?>
        </div>
      </div>
      <div class="col-md-12 form-group">
        <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
          <h4 class="panel-title customhr">بيانات الخبرة</h4>
          <?php
					  if(!empty($user_data['exeperience'])){
											
											foreach($user_data['exeperience'] as $experience){
							?>
          <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
            <div class="col-md-6 form-group">
              <label class="text-warning">نوع الوثيقة موقف / العنوان‎:</label>
              <strong><?php echo $experience->exe_title; ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الدولة:</label>
              <strong><?php echo $this->haya_model->get_name_from_list($experience->country); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">تاريخ البدء:</label>
              <strong><?php echo arabic_date($experience->start_date); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">تاريخ الانتهاء:</label>
              <strong><?php echo arabic_date($experience->end_date); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">التفاصيل:</label>
              <strong><?php echo $experience->detail; ?></strong> </div>
          </div>
          <?php
          if($experience->certificate !=""){
			$url = 'resources/users/'.$user_id.'/'.$experience->certificate;?>
          <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
            <div class="panel-heading" style="padding:10px 3px;" id="head<?php echo $experience->exe_id;?>">
              <h4 class="panel-title" style="font-size:15px;">
                <?php if($experience->exe_id!='') { ?>
                <span class="icons" id="removeicons<?php echo $experience->exe_id;?>" style="float: left; font-size:12px;"><?php echo getFileResult($url,'',$url); ?></span>
                <?php } ?>
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $experience->exe_id;?>"><?php //echo arabic_date($doccount); ?> <?php echo $this->haya_model->get_name_from_list($experience->exe_id);?></a></h4>
            </div>
            <div id="demo-collapse<?php echo $experience->exe_id;?>" class="panel-collapse collapse">
              <div class="panel-body" style="text-align:right;"></div>
            </div>
          </div>
          <?php
          }
		  ?>
        </div>
      </div>
      <?php
				}
				  }
				  ?>
    </div>
  </div>
</div>
<div class="row" style="display:none;">
  <button type="button" id="" onclick="printthepage('user-profile');" class="btn btn-default">طباعة</button>
</div>
<script type="text/javascript">
function update_user()
  {
		check_my_session();
		$('#user-pass .req-update').removeClass('parsley-error');
	$('#user-pass .req-update').each(function (index, element) {
		if ($.trim($(this).val()) == '') {
			$(this).addClass('parsley-error');
		}
	});
	var len = $('#user-pass .parsley-error').length;

		if(len<=0)
		{
				document.getElementById("update_user_pass").disabled = true;
				
				var str_data =  $('#user-pass').serialize();
				
				var request = $.ajax({
				  url: config.BASE_URL+'users/update_user_password',
				  type: "POST",
				  data: str_data,
				  dataType: "html",
				  beforeSend: function(){       $('#ajax_action').show(); $(this).hide();       },
				  success: function(msg)
				  {
						  $("#show-msg").html(msg);
				  }
				});
		}
  }
  </script> 

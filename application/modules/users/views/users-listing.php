<?php $segment	=	$this->uri->segment(1);?>
<?php $text		=	$this->lang->line($segment);?>
<?php $labels	=	$text['users']['listing'];?>

<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
              <?PHP $this->load->view("common/globalfilter",array('type'=>'users')); ?>
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                 <?php $permissions	=	$this->haya_model->check_other_permission(array('47'));?>
          		 <div class="row table-header-row" style="text-align: right !important;">
				 <?php if($permissions[47]['a']	==	1):?>
                   <a class="btn btn-success" href="<?php echo base_url();?>users/add_user" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> 
                <?php endif;
					  foreach($this->haya_model->get_list_from_ah_listmanagement('user_role') as $glfb) {
						echo('<a style="line-height:2.428571 !important;" class="btn btn-warning" style="margin-right:2px;" href="'.base_url().'users/all_users/'.$glfb->list_id.'" id="0">'.$glfb->list_name.' ('.$this->haya_model->admin_user_connt($glfb->list_id).')</a> ');
						
					  }
				?>
				</div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th class="serial_number">م</th>
                        <th class="total_numbers">مجموع المعاملات</th>
                        <th class="employee_numbers">الرقم الوظيفي</th>
                        <th class="employee_name right">الأسم الكامل</th>
                        <th class="branch_name right">اسم الفرع</th>
                        <th class="serial_number">قائمة الاسم</th>                  
                        <th class="employee_actions">الإجرائات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'users/all_users_list/'.$userrole,'columns_array'=>'
{ "data": "م" },
{ "data": "مجموع المعاملات" },
{ "data": "الرقم الوظيفي" },
{ "data": "الأسم الكامل" },
{ "data": "اسم الفرع" },
{ "data": "قائمة الاسم" },
{ "data": "الإجرائات" }')); ?>
</div>
</body>
</html>
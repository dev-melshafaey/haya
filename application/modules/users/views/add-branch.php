<?php $segment	=	$this->uri->segment(1);?>
<?php $text		=	$this->lang->line($segment);?>
<?php $labels	=	$text['branchs']['form'];?>

<div class="row col-md-12">
  <form class="mws-form" method="post" action="<?php echo current_url();?>" id="add_branch_detail" name="add_branch_detail">
      <div class="col-md-6 form-group">
        <label class="text-warning">المحافظة \ المنطقة:</label>
        <?PHP echo $this->haya_model->create_dropbox_list('province','regions',$provinceid,0,'req'); ?> </div>
      <div class="col-md-6 form-group">
        <label class="text-warning">ولاية:</label>
        <?PHP echo $this->haya_model->create_dropbox_list('wilaya','wilaya',$wiliayaid,$provinceid,'req'); ?> </div>
      <div class="col-md-6 form-group">
        <label class="text-warning"><?php echo $labels['branchname'];?>:&nbsp;</label>
        <input type="text" class="form-control req" name="branchname" id="branchname" value="<?php echo $branchname;?>">
      </div>
      <div class="col-md-6 form-group">
        <label class="text-warning"><?php echo $labels['branchstatus'];?>:&nbsp;</label>
        <select name="branchstatus" class="form-control req">
         <option value=""><?php echo $labels['branchstatus'];?></option>
          <option value="0" <?php echo((isset($branchstatus) AND $branchstatus ==	'0' )? 'selected="selected"' : NULL);?> >غير نشط</option>
          <option value="1" <?php echo((isset($branchstatus) AND $branchstatus ==	'1' )? 'selected="selected"' : NULL);?>>نشط</option>
        </select>
      </div>
      <div class="col-md-8 form-group">
        <label class="text-warning"> <?php echo $labels['branchaddress'];?>:&nbsp;</label>
        <textarea class="form-control" name="branchaddress" id="branchaddress" style="margin: 0px; width: 620px; height: 195px;"><?php echo $branchaddress;?></textarea>
      </div>
      <div id="resp"></div>
      <br clear="all" />
      <div class="col-md-4 form-group">
        <label for="basic-input"></label>
        <input type="hidden" name="userid" id="userid" value="<?php echo $login_userid;?>"/>
        <input type="hidden" name="branchid" id="branchid" value="<?php echo $branchid;?>"/>
        <input type="button" value="<?php echo $labels['submit'];?>" name="submit" class="btn btn-success mws-login-button" onclick="submit_branch();">
      </div>
  </form>
</div>
<script>
function bindWillaya(type) 
{
	var provinceid	=	$("#provinceid").val();

	$.ajax({
			url: '<?php echo base_url();?>haya/get_sub_category',
			type: "POST",
			data:'provinceid='+provinceid,
			dataType: "html",
			success: function(msg)
			{
				$("#show-willaya").html(msg);
			}
		});
}
</script>
<?php //$total_leaves		=	$this->users->get_total_leaves($user_detail['profile']->level_id);?>
<?php $total_leaves		=	$total_holidays;?>
<?php $total_absents	=	$this->users->get_total_absents($this->session->userdata('userid'));?>
<?php $remaining_leaves	=	($total_leaves-$total_absents);?>

<div class="row">
  
  <div class="row col-md-12">
    <form class="mws-form" method="post" action="<?php echo current_url();?>" id="user_leave_request" name="user_leave_request" enctype="multipart/form-data">
    <input type="hidden" id="ars_id" name="ars_id" value="<?PHP echo $this->haya_model->get_request_setting_id(3); ?>">
      <div class="col-md-4 form-group"><label class="text-warning" style="margin-left:10px;">الإجازات:</label><?php echo 'الدرجة:'.$user_detail['profile']->level;?></div>
      <div class="col-md-4 form-group"><label class="text-warning" style="margin-left:10px;">الإجازات المتبقية:</label><?php echo $remaining_leaves;?></div>
      <div class="col-md-4 form-group"><label class="text-warning" style="margin-left:10px;">مجموع الإجازات:</label><?php echo $total_leaves;?></div>
     
      <div class="col-md-3 form-group">
        <label class="text-warning">سبب:</label>
        <select name="reason" id="reason" class="form-control req">
          <option value="">أنواع السبب</option>
          <?php foreach($this->haya_model->get_leave_types() as $value):?>
          <option value="<?php echo $value;?>" <?php  echo($reason	==	$value ? 'selected="selected"' : NULL); ?>><?php echo $value;?></option>
          <?php endforeach;?>
        </select>
      </div>
      <div class="col-md-3 form-group">
        <label class="text-warning">تاريخ البدء:</label>
        <input type="text" name="start_date" id="start_date" class="datepicker form-control req" placeholder="تاريخ البدء‎" value="<?php if($start_date){ echo date('Y-m-d',strtotime($start_date)); }?>" />
      </div>
      <div class="col-md-3 form-group">
        <label class="text-warning">تاريخ الانتهاء:</label>
        <input type="text" name="end_date" id="end_date" class="datepicker form-control req" placeholder="تاريخ الانتهاء" value="<?php if($end_date){ echo date('Y-m-d',strtotime($end_date)); }?>" />
      </div>
      <div class="form-group col-md-3">
        <label class="text-warning">وثيقة:</label>
        <input style="border: 0px !important; color: #d09c0d;" type="file" name="leave_application" id="leave_application" title='وثيقة'>
      </div>
      <div class="col-md-12 form-group">
      	<div id="days" class="text-warning"></div>
      </div>
      <div class="col-md-12 form-group">
        <label class="text-warning">ملاحظات:</label>
        <textarea tabindex="8" class="form-control" name="notes" id="notes" style="margin: 0px; height: 100px;"><?php echo $notes;?></textarea>
      </div>
     
      <div class="col-md-12 form-group">
        <label for="basic-input"></label>
        <input type="hidden" name="userid" id="userid" value="<?php echo $this->session->userdata('userid');?>"/>
        <input type="hidden" name="leaveid" id="leaveid" value="<?php echo $leaveid;?>"/>
        <input type="hidden" name="manager_id" id="manager_id" value="<?php echo $manager_id;?>"/>
        <input type="hidden" name="leave_application_old" id="leave_application_old" value="<?php echo $leave_application;?>"/>
        <?php if($remaining_leaves	==	0 || $remaining_leaves < 0):?>
        لا يمكنك تطبيق لطلب إجازة لأن برنامجك مجموع الأوراق وقد تم الانتهاء
        <?php else:?>
        <input type="button" value="Submit" name="submit" class="btn btn-success mws-login-button" onclick="add_leave();"/>
        <?php endif;?>
      </div>
      <br clear="all" />
    </form>
  </div>
</div>
<script>
$(function(){
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
		onSelect: function(selected,evnt)
		{
			if(evnt.id	==	'end_date')
			{
				$("#days").text("عدد الأيام بين تاريخين : " + days_between($('#start_date').val(), $('#end_date').val()));
			}
		}
	});
});

function days_between(date1, date2) 
{
	// First we split the values to arrays date1[0] is the year, [1] the month and [2] the day
	date1 = date1.split('-');
	date2 = date2.split('-');
	
	// Now we convert the array to a Date object, which has several helpful methods
	date1 = new Date(date1[0], date1[1], date1[2]);
	date2 = new Date(date2[0], date2[1], date2[2]);
	
	// We use the getTime() method and get the unixtime (in milliseconds, but we want seconds, therefore we divide it through 1000)
	date1_unixtime = parseInt(date1.getTime() / 1000);
	date2_unixtime = parseInt(date2.getTime() / 1000);
	
	// This is the calculated difference in seconds
	var timeDifference = date2_unixtime - date1_unixtime;
	
	// in Hours
	var timeDifferenceInHours = timeDifference / 60 / 60;
	
	// and finaly, in days :)
	var timeDifferenceInDays = timeDifferenceInHours  / 24;
	
	return timeDifferenceInDays;
}

function add_leave()
{
	check_my_session();
	$('#user_leave_request .req').removeClass('parsley-error');
    $('#user_leave_request .req').each(function (index, element) {
	if ($.trim($(this).val()) == '')
	{
		$(this).addClass('parsley-error');
	}
    });
    var len = $('#user_leave_request .parsley-error').length;

	if(len<=0)
	{
		var str_data	=	$('#user_leave_request').serialize();
		var fd 			=	new FormData(document.getElementById("user_leave_request"));
		
		var request = $.ajax({
		  url: config.BASE_URL+'users/add_leave_request',
		  type: "POST",
		  data: fd,
		  enctype: 'multipart/form-data',
		  dataType: "html",
		  processData: false,  // tell jQuery not to process the data
          contentType: false ,  // tell jQuery not to set contentType
		  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(msg)
		  {
			  //window.location.href = msg;
			  
		     $('#addingDiag').modal('hide');	  
			 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
			 $('.dataTable').dataTable().fnDestroy();
			 $('#ajax_action').hide();	
			 create_data_table(1);
		  }
		});
	}
}
</script>
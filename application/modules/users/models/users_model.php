<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model
{
	/*
	*  Properties
	*/
	private $_table_users;
	private $_table_branchs;
	private $_table_listmanagement;
	private $_table_userprofile;
	private $_table_users_bankaccount;
	private	$_table_users_salary;
	private	$_table_users_basic_salary;
	private $_table_users_salary_detail;
	private $_table_users_communications;
	private $_table_users_family;
	private $_table_users_documents;
	private $_table_experiences;
	private $_table_leave_request;
	private $_table_attendence;
 	
//-------------------------------------------------------------------

	/*
	*  Constructor
	*/
	function __construct()
	{
		parent::__construct();

		//Get Table Names from Config 
		$this->_table_users 				= 	$this->config->item('table_users');
		$this->_table_branchs 				= 	$this->config->item('table_branchs');
		$this->_table_listmanagement 		= 	$this->config->item('table_listmanagement');
		$this->_table_userprofile 			= 	$this->config->item('table_userprofile');
		$this->_table_users_bankaccount 	= 	$this->config->item('table_users_bankaccount');
		$this->_table_users_salary 			= 	$this->config->item('table_users_salary');
		$this->_table_users_salary_detail 	= 	$this->config->item('table_users_salary_detail');
		$this->_table_users_family 			= 	$this->config->item('table_users_family');
		$this->_table_users_communications 	= 	$this->config->item('table_users_communications');
		$this->_table_users_documents 		= 	$this->config->item('table_users_documents');
		$this->_table_experiences 			= 	$this->config->item('table_experiences');
		$this->_table_leave_request 		= 	$this->config->item('table_leave_request');
		$this->_table_users_basic_salary 	= 	$this->config->item('table_users_basic_salary');
		$this->_table_attendence			=	$this->config->item('table_attendence');
	}
//-------------------------------------------------------------------

   /*
	* Get all User Information
	* @param $userid integer
	*/
	function getAllUserInfo($userid)
	{
		$user_info	=	array();
		
		// Get data from Users Table
		$users	=	$this->db->query("
			SELECT
				`ah_users`.`userlogin`
				, `ah_users`.`userroleid`
				, `ah_users`.`userstatus`
				, `ah_users`.`level_id`
				, `ah_users`.`branchid`
				, `ah_users`.`permissionjson`
				, `ah_userprofile`.`fullname`
				, `ah_userprofile`.`fathername`
				, `ah_userprofile`.`email`
				, `ah_userprofile`.`gender`
				, `ah_userprofile`.`maritialstatus`
				, `ah_userprofile`.`maritialstatus_text`
				, `ah_userprofile`.`profession`
				, `ah_userprofile`.`sub_profession`
				, `ah_userprofile`.`dateofbirth`
				, `ah_userprofile`.`nationality`
				, `ah_userprofile`.`nationality_text`
				, `ah_userprofile`.`profilepic`
				, `ah_userprofile`.`resume`
				, `ah_userprofile`.`numberofdependens`
				, `ah_userprofile`.`date_of_birth`
			FROM
				`ah_users`
				INNER JOIN `ah_userprofile` 
        ON (`ah_users`.`userid` = `ah_userprofile`.`userid`) WHERE `ah_users`.`userid`='".$userid."' AND `ah_users`.`delete_record`='0';");
		$user_info['users'] = $users->row();
		
		// Get Bank Account Data
		$this->db->select('*');
		$this->db->from($this->_table_users_bankaccount);			
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		$account = $this->db->get();
		
		$user_info['bankaccounts'] = $account->result();
		
		// Get Family Data
		$this->db->select('*');
		$this->db->from($this->_table_users_salary);			
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		
		$family 		=	$this->db->get();
		$main_salary	=	$family->result();
		
		if(!empty($main_salary ))
		{
			foreach($main_salary  as $salary)
			{
				$user_info['main_salary'][$salary->salaryid]	=	array('salaryid'	=> $salary->salaryid,'userid'	=> $salary->userid,'bankaccoundid'	=> $salary->bankaccoundid,'salary_type'	=> $salary->salary_type,'salary_type_text'	=> $salary->salary_type_text,'amount'	=> $salary->amount,'salarydate'	=> $salary->salarydate);
				$user_info['main_salary'][$salary->salaryid]['detail']	=	$this->get_salary_detail($salary->salaryid);				
			}
		}
		
		// Get Family Memeber Data
		$this->db->select('*');
		$this->db->from($this->_table_users_family);			
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		$family = $this->db->get();
		$user_info['family'] = $family->result();
		
		// Get Comunication Data
		$this->db->select('*');
		$this->db->from($this->_table_users_communications);			
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		$communic = $this->db->get();
		$user_info['communication'] = $communic->result();
		
		// Get Users Documents Data
		$this->db->select('*');
		$this->db->from($this->_table_users_documents);			
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		$doc = $this->db->get();
		$user_info['document'] = $doc->result();
		
		// Get User Experience Data
		$this->db->select('*');
		$this->db->from($this->_table_experiences);			
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		$exe = $this->db->get();
		$user_info['exeperience'] = $exe->result();
		
		return $user_info;
		
	}

//-------------------------------------------------------------------
	/*
	 * For Login
	 * if user axist set session
	 * @param String $username
	 * @param String $pass
	 * Return true or false
	 */
	 function get_salary_detail($salaryid)
	 {
		$salary	=	$this->db->query("
		SELECT
		`ah_users_salary_detail`.`salaryid`
		,`ah_users_salary_detail`.`notes`
		, `ah_users_salary_detail`.`amount`
		, `ah_users_salary_detail`.`salarypaymenttype`
		FROM
		`ah_users_salary`
		INNER JOIN `ah_users_salary_detail` 
		ON (`ah_users_salary`.`userid` = `ah_users_salary_detail`.`userid`) WHERE `ah_users_salary`.`salaryid`='".$salaryid."';");
		
		return $salary->result();
	 }

//-------------------------------------------------------------------
	/*
	 * For Login
	 * if user axist set session
	 * @param String $username
	 * @param String $pass
	 * Return true or false
	 */
	function login_admin($username,$pass)
	{
		$this->db->where("userlogin",$username);
		$this->db->where("userpassword",md5($pass));
		$query = $this->db->get($this->_table_users);
		
		if($query->num_rows() > 0)
		{
			$this->session->set_userdata("userid",$query->row()->userid);
			
			return TRUE;
		}
		else
		{
			return FALSE;	
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Provinces
	 * Return OBJECT
	 */
		
	function get_provinces()
	{
		$this->db->select('list_id,list_name,list_type');
		$this->db->where("list_type",'regions');
		$this->db->where("list_parent_id",'0');
		$query = $this->db->get($this->_table_listmanagement);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
//-------------------------------------------------------------------
	/*
	* Add List 
	* @param $data ARRAY
	* return TRUE
	*/

	function add_branch($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_branchs,$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* Get all Data 
	* return OBJECT
	*/

	function update_branch($branchid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('branchid',$branchid);
		$this->db->update($this->_table_branchs,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }	
	
//-------------------------------------------------------------------
	/*
	 * Get All Branches
	 * Return OBJECT
	 */
		
	function get_all_branches()
	{
		$this->db->select('branchid,province,wilaya,branchname,branchaddress');
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_branchs);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
//-------------------------------------------------------------------
	/*
	 * Get All Branches
	 * Return OBJECT
	 */
		
	function get_single_branch($branchid)
	{
		$this->db->select('branchid,province,wilaya,branchname,branchaddress,branchstatus');
		$this->db->where('branchid',$branchid);
		$query = $this->db->get($this->_table_branchs);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}

	}	
	
//-------------------------------------------------------------------
	/*
	 * Delete Branch
	 * @param $branchid integer
	 * Return TRUE
	 */
		
	function delete_branch($branchid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','branchid'	=>	$branchid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('branchid',$branchid);

		$this->db->update($this->_table_branchs,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;

	}	
//-------------------------------------------------------------------
	/*
	 * Get All Bank Accounts Detail
	 * Return OBJECT
	 */
		
	function get_all_bank_accounts($userid)
	{
		$this->db->select('bankaccountid,userid,bankid,branchid,accountnumber,accountfullname,bank_doc');
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_users_bankaccount);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	* Add Bank Account Detail
	* @param $data ARRAY
	* return TRUE
	*/

	function add_bank_account($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_users_bankaccount,$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* Update Bank Account Detail
	* return OBJECT
	*/

	function update_bank_account($branchid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('bankaccountid',$branchid);
		$this->db->update($this->_table_users_bankaccount,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Get Single Account Detail
	 * Return OBJECT
	 */
		
	function get_account_detail($accountid)
	{
		$this->db->select('bankaccountid,userid,bankid,branchid,accountnumber,accountfullname');
		$this->db->where('bankaccountid',$accountid);
		$query = $this->db->get($this->_table_users_bankaccount);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}

	}
//-------------------------------------------------------------------
	/*
	 * Delete Bank Account
	 * @param $accountid integer
	 * Return TRUE
	 */
		
	function delete_bank_account($accountid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','bankaccountid'	=>	$accountid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('bankaccountid',$accountid);

		$this->db->update($this->_table_users_bankaccount,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;

	}	
//-------------------------------------------------------------------
	/*
	 * Delete User
	 * @param $userid integer
	 * Return TRUE
	 */
		
	function delete_user($userid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','userid'	=>	$userid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('userid',$userid);

		$this->db->update($this->_table_users,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;

	}
	
//-------------------------------------------------------------------
	/*
	 * Get all users
	 * Return OBJECT
	 */
		
	function get_all_users($roleid	=	NULL)
	{
		$this->db->select('
			'.$this->_table_users.'.userid,
			'.$this->_table_users.'.id_number,
			'.$this->_table_users.'.registerdate,
			'.$this->_table_users.'.total_holidays,
			'.$this->_table_users.'.level_id,
			'.$this->_table_listmanagement.'.list_name,
			'.$this->_table_branchs.'.branchname,
			'.$this->_table_userprofile.'.fullname,
			'.$this->_table_userprofile.'.fathername,
			'.$this->_table_userprofile.'.joining_date,
			'.$this->_table_users.'.permissionjson,
			'.$this->_table_users.'.landingpage
			');
        $this->db->from('ah_users');
		$this->db->join($this->_table_listmanagement,$this->_table_listmanagement.'.list_id='.$this->_table_users.'.userroleid');
		$this->db->join($this->_table_branchs,$this->_table_branchs.'.branchid = '.$this->_table_users.'.branchid');
		$this->db->join($this->_table_userprofile,$this->_table_userprofile.'.userid='.$this->_table_users.'.userid');
        $this->db->where($this->_table_users.'.userstatus','1');
		$this->db->where($this->_table_users.'.isemploy','1');
		$this->db->where($this->_table_users.'.delete_record','0');
		if($roleid) 
		{
			$this->db->where($this->_table_users.'.userroleid',$roleid);
		}
        $this->db->order_by($this->_table_userprofile.".fullname", "ASC");
		
		$query = $this->db->get();
	
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
	
	public function count_data_by_user($userid)
	{
		$this->db->from("ah_applicant");
		$this->db->where('userid',$userid);
		return $this->db->get()->num_rows();
	}
	
	
	function get_inquiries_list_by_user($userid)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.charity_type_id,ah_applicant.step,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');		
		$this->db->where("ah_applicant.userid",$userid);		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->order_by('ah_applicant.registrationdate','DESC');		
		$query = $this->db->get();
		return	$query->result();	
	}
	//-------------------------------------------------------------------
	/*
	 * Get all users Record By Year
	 * Return OBJECT
	 */
		
	function get_all_users_by_year($year)
	{
		$this->db->select('
			'.$this->_table_users.'.userid,
			'.$this->_table_users.'.id_number,
			'.$this->_table_users.'.registerdate,
			'.$this->_table_users.'.total_holidays,
			'.$this->_table_users.'.level_id,
			'.$this->_table_userprofile.'.joining_date,
			');
        $this->db->from('ah_users');
		$this->db->join($this->_table_userprofile,$this->_table_userprofile.'.userid='.$this->_table_users.'.userid');
        $this->db->where($this->_table_users.'.userstatus','1');
		$this->db->where($this->_table_users.'.isemploy','1');
		$this->db->where($this->_table_users.'.delete_record','0');
		$this->db->where($this->_table_users.'.year <',$year);
        $this->db->order_by($this->_table_userprofile.".fullname", "ASC");
		
		$query = $this->db->get();
	
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* UPDATE User Auth Information
	* return OBJECT
	*/

	function update_user_holidays($userid,$level_id,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('userid',$userid);
		$this->db->where('level_id',$level_id);
		$this->db->update($this->_table_users,$json_data,$this->session->userdata('userid'),$data);

		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Get all users
	 * Return OBJECT
	 */
		
	function get_single_user($userid)
	{
		$this->db->select('
			'.$this->_table_users.'.*,
			'.$this->_table_userprofile.'.*,
			');
        $this->db->from('ah_users');
		$this->db->join($this->_table_userprofile,$this->_table_userprofile.'.userid='.$this->_table_users.'.userid');
        $this->db->where($this->_table_users.'.userstatus',1);
		$this->db->where($this->_table_users.'.userid',$userid);
        $this->db->order_by($this->_table_userprofile.".fullname", "ASC");
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	* Add User Auth Information
	* @param $data ARRAY
	* return TRUE
	*/

	function add_user_auth_info($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_users,$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
	
//----------------------------------------------------------------------

	/*
	* UPDATE User Auth Information
	* return OBJECT
	*/

	function update_user_auth_info($userid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('userid',$userid);
		$this->db->update($this->_table_users,$json_data,$this->session->userdata('userid'),$data);
		
		return $userid;
    }
	
//-------------------------------------------------------------------
	/*
	* Add User Detail Information
	* @param $data ARRAY
	* return TRUE
	*/

	function add_user_detail_info($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_userprofile,$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* UPDATE User Auth Information
	* return OBJECT
	*/

	function update_user_detail_info($userid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('userid',$userid);
		$this->db->update($this->_table_userprofile,$json_data,$this->session->userdata('userid'),$data);
		
		return $userid;
    }
//-------------------------------------------------------------------
	/*
	 * Get Single Account Detail
	 * Return OBJECT
	 */
		
	function get_accounts_by_userid($userid)
	{
		$this->db->select('bankaccountid,userid,bankid,branchid,accountnumber,accountfullname');
		$this->db->where('userid',$userid);
		$query = $this->db->get($this->_table_users_bankaccount);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}	
	
//-------------------------------------------------------------------
	/*
	 * Get All Bank Accounts Detail
	 * Return OBJECT
	 */
		
	function get_all_salaries($userid)
	{
		$query = $this->db->query("SELECT
			`ah_users_salary`.`amount` AS total_salary
			,`ah_users_salary`.`basic_salary` 
			,`ah_users_salary`.`certificate`
			, SUM(`ah_users_salary_detail`.`amount`) AS allownces
			, `ah_users_salary`.`userid`
			, `ah_users_salary`.`bankaccoundid`
			, `ah_users_salary`.`salary_type`
			, `ah_users_salary`.`salary_type_text`
			, `ah_users_salary`.`delete_record`
			, `ah_users_salary_detail`.`salarypaymenttype`
			, `ah_users_salary_detail`.`notes`
			, `ah_users_salary`.`salarydate`
			, `ah_users_salary`.`salaryid`
		FROM
			`ah_users_salary`
			INNER JOIN `ah_users_salary_detail` 
				ON (`ah_users_salary`.`salaryid` = `ah_users_salary_detail`.`salaryid`)
		WHERE `ah_users_salary`.`userid`='".$userid."' AND `ah_users_salary`.`delete_record`='0';");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get All Bank Accounts Detail
	 * Return OBJECT
	 */
		
	function get_all_basic_salaries($userid)
	{
		$this->db->select('salaryid,userid,amount,notes,submit_date');
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_users_basic_salary);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
	
//-------------------------------------------------------------------
	/*
	 * Get All Bank Accounts Detail
	 * Return OBJECT
	 */
		
	function get_paymenttype_list($type)
	{
		$this->db->select('*');
		$this->db->where('list_type',$type);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_listmanagement);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
//-------------------------------------------------------------------
	/*
	* Add Salary
	* @param $data ARRAY
	* return TRUE
	*/

	function add_salary($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_users_salary,$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	* Add Basic Salary
	* @param $data ARRAY
	* return TRUE
	*/

	function add_basic_salary($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_users_basic_salary,$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	* Add Salary Detail Information
	* @param $data ARRAY
	* return TRUE
	*/

	function add_salary_detail($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_users_salary_detail,$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Delete Salary
	 * @param $branchid integer
	 * Return TRUE
	 */
		
	function delete_salary($salaryid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','salaryid'	=>	$salaryid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('salaryid',$salaryid);

		$this->db->update($this->_table_users_salary,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;

	}
//-------------------------------------------------------------------
	/*
	 * Delete Basic Salary
	 * @param $branchid integer
	 * Return TRUE
	 */
		
	function delete_basic_salary($salaryid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','salaryid'	=>	$salaryid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('salaryid',$salaryid);

		$this->db->update($this->_table_users_basic_salary,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	 * Delete Salary
	 * @param $branchid integer
	 * Return TRUE
	 */
		
	/*function get_salary_detail($salaryid)
	{		
		
		$query	=	$this->db->query("SELECT
    `ah_users_salary`.`userid`
    , `ah_users_salary`.`activity_user_id`
    , `ah_users_salary`.`bankaccoundid`
    , `ah_users_salary`.`salary_type`
    , `ah_users_salary`.`salary_type_text`
    , `ah_users_salary`.`amount` AS firstamount
    , `ah_users_salary`.`delete_record`
    , `ah_users_salary`.`salarydate`
    , `ah_users_salary`.`saladdingdate`
    , `ah_users_salary_detail`.`usdid`
    , `ah_users_salary_detail`.`salarypaymenttype`
    , `ah_users_salary_detail`.`amount` AS secondamount
    , `ah_users_salary_detail`.`notes`
FROM
    `ah_users_salary`
    INNER JOIN `ah_users_salary_detail` 
        ON (`ah_users_salary`.`salaryid` = `ah_users_salary_detail`.`salaryid`) WHERE `ah_users_salary`.`salaryid` = ".$salaryid.";");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}*/	
//-------------------------------------------------------------------
	/*
	 * Get All Bank Accounts Detail
	 * Return OBJECT
	 */
		
	function get_salary_data($salaryid)
	{
		$this->db->select('*');
		$this->db->where('salaryid',$salaryid);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_users_salary);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get All Bank Accounts Detail
	 * Return OBJECT
	 */
		
	function get_basic_salary_data($salaryid)
	{
		$this->db->select('*');
		$this->db->where('salaryid',$salaryid);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_users_basic_salary);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get All Bank Accounts Detail
	 * Return OBJECT
	 */
		
	function get_basic_salary($userid)
	{
		$this->db->select('amount');
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		$this->db->order_by('salaryid','DESC');
		$this->db->limit('1');
		
		
		$query = $this->db->get($this->_table_users_basic_salary);
		
		if($query->num_rows() > 0)
		{
			return $query->row()->amount;
		}
	}
//-------------------------------------------------------------------
	/*
	 * 
	 * 
	 */
		
	function get_paymenttype_data($salaryid,$listid)
	{
		$this->db->select('*');
		$this->db->where('salaryid',$salaryid);
		$this->db->where('salarypaymenttype',$listid);
		
		$query = $this->db->get($this->_table_users_salary_detail);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//--------------------------------------------------------------------------
	/*
	 * 
	 * 
	 */	
	public function update_salary($salaryid,$data)
	{
		$json_data	=	json_encode(array('data'=>$data));
		
		$this->db->where('salaryid',$salaryid);
		$this->db->update($this->_table_users_salary,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
	//--------------------------------------------------------------------------
	/*
	 * 
	 * 
	 */	
	public function update_basic_salary($salaryid,$data)
	{
		$json_data	=	json_encode(array('data'=>$data));
		
		$this->db->where('salaryid',$salaryid);
		$this->db->update($this->_table_users_basic_salary,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//--------------------------------------------------------------------------
	/*
	 * 
	 * 
	 */	
	public function update_salary_detail($salaryid,$paymenttypeid,$data)
	{
		$json_data	=	json_encode(array('data'=>$data));
		
		$this->db->where('salaryid',$salaryid);
		$this->db->where('salarypaymenttype',$paymenttypeid);
		$this->db->update($this->_table_users_salary_detail,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	 * Get Communications Detail
	 * Return OBJECT
	 */
		
	function get_all_communications($userid)
	{
		$this->db->select('communicationid,userid,communicationtype,communicationvalue,addingdate,communication_doc');
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_users_communications);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
//-------------------------------------------------------------------
	/*
	 * Get Communications Detail
	 * Return OBJECT
	 */
		
	function get_communications_detail($cominid)
	{
		$this->db->select('communicationid,userid,communicationtype,communicationvalue,addingdate');
		$this->db->where('communicationid',$cominid);
		$query = $this->db->get($this->_table_users_communications);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}

	}
//-------------------------------------------------------------------
	/*
	* Add Communications Detail
	* @param $data ARRAY
	* return TRUE
	*/

	function add_communication($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_users_communications,$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* Update Bank Account Detail
	* return OBJECT
	*/

	function update_communication($comicid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('communicationid',$comicid);
		$this->db->update($this->_table_users_communications,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Delete Family Member
	 * @param $docid integer
	 * Return TRUE
	 */
		
	function delete_communication($comid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','ufamilyid'	=>	$comid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('communicationid',$comid);

		$this->db->update($this->_table_users_communications,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	 * Get Communications Detail
	 * Return OBJECT
	 */
		
	function get_all_family_members($userid)
	{
		$this->db->select('ufamilyid,userid,fullname,relation,dateofbirth,family_doc');
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_users_family);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
//-------------------------------------------------------------------
	/*
	 * Get Family Members Detail
	 * Return OBJECT
	 */
		
	function get_family_detail($fmid)
	{
		$this->db->select('ufamilyid,userid,fullname,relation,dateofbirth');
		$this->db->where('ufamilyid',$fmid);
		$query = $this->db->get($this->_table_users_family);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}

	}
//-------------------------------------------------------------------
	/*
	* Add Family Detail
	* @param $data ARRAY
	* return TRUE
	*/

	function add_family_member($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_users_family,$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* Update Family
	* return OBJECT
	*/

	function update_family_member($fmid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('ufamilyid',$fmid);
		$this->db->update($this->_table_users_family,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Delete Family Member
	 * @param $docid integer
	 * Return TRUE
	 */
		
	function delete_family_member($fmid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','ufamilyid'	=>	$fmid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('ufamilyid',$fmid);

		$this->db->update($this->_table_users_family,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;

	}
//-------------------------------------------------------------------
	/*
	 * Get All Documents Detail BY userid
	 * Return OBJECT
	 */
		
	function get_all_documents($userid)
	{
		$this->db->select('udocid,userid,documenttype,issuedate,expirydate,document,issuecountry,submissiondate');
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_users_documents);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Documents Detail BY Docid
	 * Return OBJECT
	 */
		
	function get_documents_detail($docid)
	{
		$this->db->select('udocid,userid,documenttype,issuedate,expirydate,issuecountry,submissiondate');
		$this->db->where('udocid',$docid);
		$query = $this->db->get($this->_table_users_documents);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	* Add Document Detail
	* @param $data ARRAY
	* return TRUE
	*/

	function add_document($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_users_documents,$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* Update Document Detail
	* return OBJECT
	*/

	function update_document($docid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('udocid',$docid);
		$this->db->update($this->_table_users_documents,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Delete Document
	 * @param $docid integer
	 * Return TRUE
	 */
		
	function delete_document($docid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','udocid'	=>	$docid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('udocid',$docid);

		$this->db->update($this->_table_users_documents,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;

	}
//-------------------------------------------------------------------
	/*
	 * Get All Documents Detail BY userid
	 * Return OBJECT
	 */
		
	function get_all_experiences($userid)
	{
		$this->db->select('exe_id,userid,exe_title,country,start_date,end_date,detail,delete_record,add_date,certificate');
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_experiences);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Documents Detail BY Docid
	 * Return OBJECT
	 */
		
	function get_experiences_detail($exeid)
	{
		$this->db->select('exe_id,userid,exe_title,country,start_date,end_date,detail,delete_record,add_date');
		$this->db->where('exe_id',$exeid);
		$query = $this->db->get($this->_table_experiences);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	* Add Document Detail
	* @param $data ARRAY
	* return TRUE
	*/

	function add_experience($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_experiences,$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* Update Document Detail
	* return OBJECT
	*/

	function update_experience($exeid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('exe_id',$exeid);
		$this->db->update($this->_table_experiences,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Delete Document
	 * @param $docid integer
	 * Return TRUE
	 */
		
	function delete_exeid($exe_id)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','exe_id'	=>	$exe_id));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('exe_id',$exe_id);

		$this->db->update($this->_table_experiences,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;

	}
//-------------------------------------------------------------------
	/*
	 * Get Documents Detail BY Docid
	 * Return OBJECT
	 */
		
	function get_leave_req_record($leaveid)
	{
		$this->db->select('leaveid,userid,reason,start_date,end_date,leave_application,notes,approved,delete_record');
		$this->db->where('leaveid',$leaveid);
		$query = $this->db->get($this->_table_leave_request);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	* Add Leave Request 
	* @param $data ARRAY
	* return TRUE
	*/

	function add_leave_request($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_leave_request,$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* Update Leave Request Detail
	* return OBJECT
	*/

	function update_leave_request($leaveid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('leaveid',$leaveid);
		$this->db->update($this->_table_leave_request,$json_data,$this->session->userdata('userid'),$data);
		

		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Delete Leave Request
	 * @param $leaveid integer
	 * Return TRUE
	 */
		
	function delete_leave_requestt($leaveid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','leaveid'	=>	$leaveid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('leaveid',$leaveid);

		$this->db->update($this->_table_leave_request,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;

	}
//-------------------------------------------------------------------
	/*
	 * Get All Leave Requests
	 * Return OBJECT
	 */
		
	function get_all_leave_requests($userid	=	NULL,$status	=	NULL,$user_role	=	NULL,$manager_id	=	NULL)
	{
		$this->db->select('
			'.$this->_table_leave_request.'.*,
			'.$this->_table_userprofile.'.*,
			');
		
        $this->db->from($this->_table_userprofile);
		$this->db->join($this->_table_leave_request,$this->_table_leave_request.'.userid='.$this->_table_userprofile.'.userid');
		
		if($user_role	!= '101')
		{
			$this->db->where($this->_table_leave_request.'.userid',$userid);
			$this->db->or_where($this->_table_leave_request.'.manager_id',$userid);
		}

		if($status	==	'A')
		{
			$this->db->where($this->_table_leave_request.'.approved','1');
		}
		if($status	==	'R')
		{
			$this->db->where($this->_table_leave_request.'.approved','2');
		}
		$this->db->order_by($this->_table_leave_request.'.leaveid','DESC');
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Single Leave Requests
	 * Return OBJECT
	 */
		
	function leave_request_view($leaveid)
	{
		$this->db->select('
			'.$this->_table_leave_request.'.*,
			'.$this->_table_userprofile.'.*,
			'.$this->_table_users.'.level_id,
			'.$this->_table_users.'.id_number,
			'.$this->_table_users.'.branchid,
			');
		
        $this->db->from($this->_table_userprofile);
		$this->db->join($this->_table_leave_request,$this->_table_leave_request.'.userid='.$this->_table_userprofile.'.userid');
		$this->db->join($this->_table_users,$this->_table_users.'.userid='.$this->_table_userprofile.'.userid');
		$this->db->where($this->_table_leave_request.'.leaveid',$leaveid);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get All Leave Requests
	 * Return OBJECT
	 */
		
	function all_leave_request($userid)
	{
		$this->db->select('
			'.$this->_table_leave_request.'.*,
			'.$this->_table_userprofile.'.*,
			');
		
        $this->db->from($this->_table_userprofile);
		$this->db->join($this->_table_leave_request,$this->_table_leave_request.'.userid='.$this->_table_userprofile.'.userid');
		$this->db->where($this->_table_leave_request.'.approved	!=','0');
		$this->db->where($this->_table_userprofile.'.userid',$userid);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get All Leave Requests
	 * Return OBJECT
	 */
		
	function get_all_approved_leave_requests($start_date,$end_date)
	{
		$this->db->select('
			'.$this->_table_leave_request.'.*,
			'.$this->_table_userprofile.'.*,
			');
		
        $this->db->from($this->_table_userprofile);
		$this->db->join($this->_table_leave_request,$this->_table_leave_request.'.userid='.$this->_table_userprofile.'.userid');

		$this->db->where($this->_table_leave_request.'.approved','1');
		$this->db->where('ah_leave_request.start_date >= ',$start_date);
		$this->db->where('ah_leave_request.end_date <= ',$end_date);
		
		$this->db->order_by($this->_table_leave_request.'.leaveid','DESC');
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* Update Leave Request Detail
	* return OBJECT
	*/

	function update_leave_request_response($leaveid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('leaveid',$leaveid);
		$this->db->update($this->_table_leave_request,$json_data,$this->session->userdata('userid'),$data);
		

		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Get Documents Detail BY Docid
	 * Return OBJECT
	 */
		
	function get_user_id($leaveid)
	{
		$this->db->select('userid');
		$this->db->where('leaveid',$leaveid);
		$query = $this->db->get($this->_table_leave_request);
		
		if($query->num_rows() > 0)
		{
			return $query->row()->userid;
		}
	}	
//-------------------------------------------------------------------
	/*
	 * Get User Leave Request Information
	 * Return OBJECT
	 */
		
	function get_leave_information($leaveid)
	{
		$this->db->where('leaveid',$leaveid);
		$query = $this->db->get($this->_table_leave_request);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
		
//-------------------------------------------------------------------
	/*
	* Add Document Detail
	* @param $data ARRAY
	* return TRUE
	*/

	function add_attendence($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_attendence,$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Delete Branch
	 * @param $branchid integer
	 * Return TRUE
	 */
		
	function delete_attendence($userid,$date)
	{
		
		$query	=	$this->db->query("DELETE FROM ah_attendence WHERE userid	=	'".$userid."' AND DATE(attendence_date) = '".$date."'");

		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	 * Get All Holidays Levels
	 * Return OBJECT
	 */
		
	function get_all_levels()
	{
		$this->db->select('level_id,level_name,level_holiday');
		$query = $this->db->get('ah_levels');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Holidays Levels By ID
	 * Return OBJECT
	 */
		
	function get_level_by_id($level_id)
	{
		$this->db->select('level_id,level_name,level_holiday');
		$this->db->where('level_id',$level_id);
		$query = $this->db->get('ah_levels');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}

//----------------------------------------------------------------------

	/*
	* Update Holidays Levels
	* return OBJECT
	*/

	function update_level($level_id,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('level_id',$level_id);
		$this->db->update('ah_levels',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Get Documents Detail BY Docid
	 * Return OBJECT
	 */
		
	function get_extra_things_by_id($extra_id)
	{
		$this->db->select('extra_id,userid,title,amount,notes,record_type,submit_date,delete_record');
		$this->db->where('extra_id',$extra_id);
		$query = $this->db->get('ah_extra_things');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}	
//-------------------------------------------------------------------
	/*
	* Add Extra Things
	* @param $data ARRAY
	* return TRUE
	*/

	function add_extra_things($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_extra_things',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* Update Extra Things
	* return OBJECT
	*/

	function update_extra_things($extra_id,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('extra_id',$extra_id);
		$this->db->update('ah_extra_things',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Get All Extra Detail
	 * Return OBJECT
	 */
		
	function get_extra_things($userid,$type)
	{
		$this->db->select('extra_id,userid,title,amount,notes,record_type,submit_date,delete_record,certificate');
		$this->db->where('userid',$userid);
		$this->db->where('record_type',$type);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get('ah_extra_things');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* Update Extra Things
	* return OBJECT
	*/

	function update_level_into_users($extra_id,$data)
    {
		//$json_data	=	json_encode(array('data'	=>	$data));
		
		//$this->db->where('extra_id',$extra_id);
		//$this->db->update('ah_extra_things',$json_data,$this->session->userdata('userid'),$data);
		
		//return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Get Holidays Levels By ID
	 * Return OBJECT
	 */
		
	function get_level_name($level_id)
	{
		$this->db->select('level_name');
		$this->db->where('level_id',$level_id);
		$query = $this->db->get('ah_levels');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->level_name;
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get All Branches
	 * Return OBJECT
	 */
		
	function get_all_managers()
	{
		$query = $this->db->query("SELECT
    `ah_users`.`userroleid`
    , `ah_userprofile`.`fullname`
    , `ah_users`.`userid`
FROM
    `ah_userprofile`
    INNER JOIN `ah_users` 
        ON (`ah_userprofile`.`userid` = `ah_users`.`userid`) WHERE `ah_users`.`userroleid`='102';");
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
//-------------------------------------------------------------------
	/*
	* Add HR Response
	* @param $data ARRAY
	* @param $leave_req_id ARRAY
	* return TRUE
	*/

	function add_hr_response($leave_req_id,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('leave_req_id',$leave_req_id);
		$this->db->update('ah_leave_request_comments',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Add Manager Response
	* @param $data ARRAY
	* return TRUE
	*/

	function add_manager_response($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_leave_request_comments',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Update Manager Response
	* @param $data ARRAY
	* return TRUE
	*/

	function update_manager_response($leave_req_id,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('leave_req_id',$leave_req_id);
		$this->db->update('ah_leave_request_comments',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
	
//-------------------------------------------------------------------
	/*
	* Manager Response Exist OR NOT
	* @param $data ARRAY
	* return TRUE
	*/

	function manager_response_exist($manager_id,$leave_req_id)
    {
		$this->db->select('manager_id,leave_req_id');
		
		$this->db->where('leave_req_id',$leave_req_id);
		$this->db->where('manager_id',$manager_id);
		
		$query = $this->db->get('ah_leave_request_comments');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
    }
//-------------------------------------------------------------------
	/*
	* Manager Response Exist OR NOT
	* @param $data ARRAY
	* return TRUE
	*/

	function get_manager_hr_response($leave_req_id)
    {
		$this->db->where('leave_req_id',$leave_req_id);
		
		$query = $this->db->get('ah_leave_request_comments');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
    }
//-------------------------------------------------------------------
	/*
	* Manager Response Exist OR NOT
	* @param $data ARRAY
	* return TRUE
	*/

	function get_total_leaves($level_id)
    {
		$this->db->select('level_holiday');
		$this->db->where('level_id',$level_id);
		
		$query = $this->db->get('ah_levels');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->level_holiday;
		}
    }
//-------------------------------------------------------------------
	/*
	* Manager Response Exist OR NOT
	* @param $data ARRAY
	* return TRUE
	*/

	function get_total_absents($userid)
    {
		$query = $this->db->query("SELECT COUNT(attendence_status) AS absent, 
(SELECT COUNT(attendence_status) FROM `ah_attendence` WHERE attendence_status='P') AS present 
FROM `ah_attendence` WHERE attendence_status='A' AND `userid` = ".$userid." ");
		
		if($query->num_rows() > 0)
		{
			return $query->row()->absent;
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Custom Form Data
	* @param $moduleid integer
	* @param $userid integer
	* return TRUE
	*/

	function getCustomeFormValue($moduleid,$userid)
    {
		$this->db->select('mmdid,moduleid,userid,moduledata,custom_file_name,savingdate');
		$this->db->where('userid',$userid);
		$this->db->where('moduleid',$moduleid);
		
		$query = $this->db->get('mh_module_data');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Custom Form Data
	* @param $moduleid integer
	* @param $userid integer
	* return TRUE
	*/

	function query_in($array)
    {
		$this->db->select('list_name');
		$this->db->where_in('list_id',$array);
		
		$query = $this->db->get('ah_listmanagement');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* 
	* Before Submit Check ID NUMBER exist or NOT
	* @param $id_number ineger
	* RETURN INTEGER
	* 
	*/

	function user_idnumber_exist($id_number)
    {
		$this->db->select('id_number');
		$this->db->where('id_number',$id_number);
		
		$query = $this->db->get($this->_table_users);
		
		if($query->num_rows() > 0)
		{
			return $query->row()->id_number;
		}
		else
		{
			return '0';
		}
    }
	
/*-------------------------------------------------------------------------*/		
/* End of file admin_model.php */
/* Location: ./appliction/modules/admin/admin_model.php */
}

?>
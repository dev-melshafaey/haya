<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bank_management extends CI_Controller 
{
//-------------------------------------------------------------------------------	
	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;
	
//-------------------------------------------------------------------------------
	/*
	* Costructor
	*/

	public function __construct()
	{
		parent::__construct();	
		
		// lOAD Models	
		$this->load->model('bank_management_model','outside');
			
		$this->_data['module']			=	$this->haya_model->get_module();			
		$this->_login_userid 			=	$this->session->userdata('userid');
		$this->_data['login_userid'] 	=	$this->_login_userid;			
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);	
	}
//-----------------------------------------------------------------------
	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
    public function index()
    {
		$this->load->view('alhaya-bank-management',$this->_data);
    }
//-----------------------------------------------------------------------
	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
    public function other_categories()
    {
		$this->load->view('other-categories-bank-management',$this->_data);
    }
//-----------------------------------------------------------------------

	/*
	* Add / Edit Country Registration
	* @param $country_id
	*
	*/
	public function country_regestration($country_id,$type	=	NULL)
	{
		$this->_data['type']	=	$type;
		
		if($this->input->post())
		{
			// Get all values from POST Method
			$data	=	$this->input->post();
			
			unset($data['country_listmanagement_text']); // UNSET value from ARRAY
			
			$country_detail['country_listmanagement']		=	$data['country_listmanagement'];
			$country_detail['charity_type_id']				=	$data['charity_type_id'];
			$country_detail['bank_type']					=	$data['bank_type'];
			
			if($data['opening_acount_budget'])
			{
				$country_detail['opening_acount_budget']	=	$data['opening_acount_budget'];
			}
			
			$country_detail['budget_year']					=	$data['budget_year'];
			

			if($data['register_country_id'])
			{
				// Update data into database
				$this->outside->update_country_registration($data['register_country_id'],$country_detail);
				
				$total_divs	=	count($data['bankid']);
				
				for($i	=	1;$i	<=	$total_divs;	$i++)
				{	
					$bank_detail	=	array(
										'register_country_id'	=>	$data['register_country_id'],
										'bankid'				=>	$data['bankid'][$i],
										'branchid'				=>	$data['branchid'][$i],
										'account_no'			=>	$data['account_no'][$i],
										'amount_in_account'		=>	$data['amount_in_account'][$i],
										'phone_no'				=>	$data['phone_no'][$i],
										'fax_no'				=>	$data['fax_no'][$i],
										'email_address'			=>	$data['email_address'][$i],
										'swiftcode'				=>	$data['swiftcode'][$i],
										'index_num'				=>	$i
										);
										
										
					//echo '<pre>';print_r($bank_detail);
					
					$bank_id	=	$this->outside->bankid_exist($data['bank_id'][$i]);
					
					if($bank_id)
					{
						$this->outside->update_bank_details($bank_id,$data['register_country_id'],$bank_detail); // Add Bank Detail
					}
					else
					{
						$this->outside->add_bank_details($bank_detail); // Add Bank Detail
					}
				}
			}
			else
			{
				// ADD data into database
				$register_country_id	=	$this->outside->add_country_registration($country_detail);
				
				$total_divs	=	count($data['bankid']);
				for($i	=	1;$i	<=	$total_divs;	$i++)
				{
					$bank_detail	=	array(
										'register_country_id'	=>	$register_country_id,
										'bankid'				=>	$data['bankid'][$i],
										'branchid'				=>	$data['branchid'][$i],
										'account_no'			=>	$data['account_no'][$i],
										'amount_in_account'		=>	$data['amount_in_account'][$i],
										'phone_no'				=>	$data['phone_no'][$i],
										'fax_no'				=>	$data['fax_no'][$i],
										'email_address'			=>	$data['email_address'][$i],
										'swiftcode'				=>	$data['swiftcode'][$i],
										'index_num'				=>	$i
										);
										
					$this->outside->add_bank_details($bank_detail); // Add Bank Account Detail
				}
			}
			
			if($data['bank_type']	==	'alhaya')
			{
				// Redirect to listing page
				/*redirect(base_url().'bank_management');
				exit();*/
				
				$this->load->library('user_agent');
				if ($this->agent->is_referral())
				{
					$url	=	 $this->agent->referrer();
				}
				
				$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
				redirect($url);
				exit();
			}
			else
			{
				// Redirect to listing page
				/*redirect(base_url().'bank_management/other_categories');
				exit();*/
				
				$this->load->library('user_agent');
				if ($this->agent->is_referral())
				{
					$url	=	 $this->agent->referrer();
				}
				
				$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
				redirect($url);
				exit();
			}
		}
		else
		{
			if($type	==	'alhaya')
			{
				$this->_data['list_types']	=	$this->outside->get_default_types(); // Get Default Charity Types
			}
			else
			{
				$this->_data['list_types']	=	$this->outside->get_other_types(); // Get Other Charity Types
			}
				
			if($country_id)
			{
				$this->_data['country_detail']	=	$this->outside->get_country_detail($country_id); // Get Bank Accounts Detail For Edit Form
				
				$this->load->view('country-registration-form-edit',$this->_data);
			}
			else
			{		
				$this->load->view('country-registration-form',$this->_data);
			}
		}
	}
//-----------------------------------------------------------------------
	/*
	* Add Multifields into Country Registration Form
	*
	*/
	
	function add_multifields()
	{
		$this->_data['total_divs']	=	$this->input->post('total_divs'); // Total Counts of Multi fields
		
		echo $html	=	$this->load->view('add_multifields',$this->_data,TRUE); // Multifields form
	}
	
//-----------------------------------------------------------------------
	/*
	* All Register Companies Listing
	* 
	*/	
	public function ajax_countries_list($type)
	{
		$response		=	$this->outside->get_all_registerd_countries($type);

		$permissions	=	$this->haya_model->check_other_permission(array('196'));	
		
		foreach($response as $inq)
        {
			$current_balance	=	$inq->opening_acount_budget;

			$debit	=	$this->outside->total_amount_in_accounts(NULL,'DEBIT',$inq->charity_type_id);
			$credit	=	$this->outside->total_amount_in_accounts(NULL,'CREDIT',$inq->charity_type_id);

			$grand_amount	=	($current_balance	+	$debit	-	$credit);

			$action = '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'bank_management/get_outside_country_detail/'.$inq->register_country_id.'"><i class="icon-eye-open"></i></a>';
			//if($permissions[196]['u']	==	1)
			//{
				$action .= '&nbsp;<a class="iconspace" href="'.base_url().'bank_management/country_regestration/'.$inq->register_country_id.'/'.$inq->bank_type.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>'; 
			//}
			
			$arr[] = array(
				"DT_RowId"					=>	$inq->register_country_id.'_durar_lm',				
                "الدولة"					=>	$this->haya_model->get_name_from_list($inq->country_listmanagement),
				"المساعدات"					=>	$this->haya_model->get_name_from_list($inq->charity_type_id),
				"الميزانية فتح الحساب"		=>	number_format($grand_amount,3),
				"عام"						=>	$inq->budget_year,           
                "الإجرائات"					=>	$action);
			
		 	unset($action,$english,$arabic); // UNSET Variables
       }
	   
	   $ex['data'] = $arr;
       echo json_encode($ex);	
	}
//-----------------------------------------------------------------------
	/*
	*	Outside Country Detail page
	*	@param $country_id	int
	*/	
	public function get_outside_country_detail($country_id)
	{
		$this->_data['country_detail']	=	$this->outside->get_country_detail($country_id); // GET country Detail
		
		//$this->_data['project_budgets']	=	$this->outside->get_country_project_budget($country_id);
		$this->load->view('country-detail-page',$this->_data);	
	}
//-----------------------------------------------------------------------
	/*
	*	Reaceive Donation
	*	@param $country_id	int
	*/	
	public function receive_donation($country_id)
	{
		if($this->input->post())
		{
			$form_data		=	$this->input->post(); // Get all data from POST
			
			//$get_country_id	=	$this->outside->get_country_id($form_data['account_number']);
			
			//$this->outside->update_charity_amount($get_country_id,$form_data['amount']); // Update Opening Account Budget
			
			
			if($_FILES["document"]["tmp_name"])
			{
				$document				=	$this->upload_file($form_data['login_user_id'],'document','resources/users');
				$form_data['document']	=	$document;
			}
			
			$transaction_id	=	$this->outside->save_transaction_data($form_data); // Save transaction detail into database
			
			echo $url	=	base_url().'bank_management/transactions';
			exit();
		}
		else
		{
			$this->_data['list_types']	=	$this->outside->get_all_types();
		
			$this->load->view('receive-donation-form',$this->_data);	
		}
	}
//-----------------------------------------------------------------------
	/*
	*	Give Donation
	*	@param $country_id	int
	*/	
	public function give_donation($country_id)
	{
		if($this->input->post())
		{
			$form_data		=	$this->input->post(); // Get all data from POST
			
			echo '<pre>';
			print_r($form_data);
			exit();
			
			//$get_country_id	=	$this->outside->get_country_id($form_data['account_number']);
			
			//$this->outside->update_charity_amount($get_country_id,$form_data['amount']); // Update Opening Account Budget
			
			$transaction_id	=	$this->outside->save_transaction_data($form_data); // Save transaction detail into database
			
			/*redirect(base_url().'bank_management/transactions');
			exit();*/
			
			$this->load->library('user_agent');
			if ($this->agent->is_referral())
			{
				$url	=	 $this->agent->referrer();
			}
			
			$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
			redirect($url);
			exit();
			
		}
		else
		{
			$this->_data['list_types']	=	$this->outside->get_all_types();
		
			$this->load->view('give-donation-form',$this->_data);	
		}
	}
//-----------------------------------------------------------------------
	/*
	*	List of All Transactions
	*/	
	public function transactions()
	{
		$this->load->view('transactions',$this->_data);
	}
	
//-----------------------------------------------------------------------
	/*
	*	Outside Country Detail page
	*	@param $country_id	int
	*/	
	public function get_account_number($type)
	{
		$charity_id	=	$this->input->post('charity_id');
		
		$account_numbers	=	$this->outside->get_account_numbers($charity_id);
		
		$current_balance	=	$account_numbers['balance'];
		
		$debit	=	$this->outside->total_amount_in_accounts(NULL,'DEBIT',$charity_id);
		$credit	=	$this->outside->total_amount_in_accounts(NULL,'CREDIT',$charity_id);
		
		$grand_amount	=	($current_balance	+	$debit	-	$credit);
		
		$html	 =	'<label class="text-warning">رقم حساب  : <strong> '.$grand_amount.' </strong></label>';
		$html	.=	'<select class="form-control req" name="bank_id" id="bank_id">';
		$html	.=	'<option value="">نوع المعاملة</option>';
		
		foreach($account_numbers['accounts']	as $acc)
		{
			$account_debit	=	$this->outside->total_amount_in_accounts($acc->bank_id,'DEBIT'); // Get SUM of  DEBIT
			$credit_debit	=	$this->outside->total_amount_in_accounts($acc->bank_id,'CREDIT'); // Get SUM of CREDIT
			
			$total_amount_in_account	=	($acc->amount_in_account	+	$account_debit	-	$credit_debit); // GET total SUM of Single Account
			
			$html	.=	'<option value="'.$acc->bankid.'">'.$this->haya_model->get_name_from_list($acc->bankid).' - '.$this->haya_model->get_name_from_list($acc->branchid).' ( '.$acc->account_no.' ) &nbsp;( '.$total_amount_in_account.' )</option>';
		}
		
		$html	.=	'</select>';

		echo $html;
	}
	
//-----------------------------------------------------------------------

	/*
	*	File Uploading
	*	@param $userid integer
	*	@param $filefield integer Input File name
	*	@param $folder integer older name where Image Upload
	*	@param $width integer
	*	@param $height integer
	*/
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
				
		if (!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Sending Email Function
	* @param $to string
	* @param $from string
	* @param $subject string
	* @param $message string
	* @param $path string
	*
	*/
	public function send_email($to	=	NULL,$from	=	NULL,$subject	=	NULL,$message = NULL,$path	=	NULL) 
	{
		$config = Array(
			'protocol' 	=>	'smtp',
			'smtp_host' =>	SMTPHOST,
			'smtp_port' =>	465,
			'smtp_user'	=>	SMTPEMAIL,
			'smtp_pass'	=>	SMTPPASSWORD,
			'mailtype'	=>	'html',
			'charset'	=>	'utf-8',
			'wordwrap' 	=>	TRUE
			);
		
		$this->load->library('email', $config);

		$this->email->clear(true);
		$this->email->set_newline("\r\n");
		$this->email->from(SMTPEMAIL,'Al-Haya');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		
		if($path)
		{
		 	$this->email->attach($path);
		}
		
		// Send Email
		$this->email->send();
	}
//-----------------------------------------------------------------------

	/*
	*	Download File
	*	@param $userid integer
	*	@param $file_name string
	*/
	
	public function download_file($userid,$file_name)
	{
		$path	= 'resources/users/'.$userid.'/'.$file_name;

		// Download File
		downloadFile($path,$file_name);
	}
//------------------------------------------------------------------------

  	/**
   	* Dynamic Forms Listing Page
   	* @param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]  = $this->haya_model->get_all_custom_form($moduleid);
		
		$this->_data["userid"] = $this->_login_userid;
		
		$this->_data['formid']	=	$moduleid;
		 
		// Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);	 
	 }
//------------------------------------------------------------------------
}
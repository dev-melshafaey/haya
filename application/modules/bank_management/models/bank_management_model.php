<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bank_management_model extends CI_Model {

	/*
	* Constructor
	*
	*/
	function __construct()
    {
        parent::__construct();
		
		$this->_login_userid = $this->session->userdata('userid');
    }

//-------------------------------------------------------------------
	/*
	* Add Country Registration Detail 
	* @param $data ARRAY
	* return LAST inserted ID
	*/

	function add_country_registration($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('alhaya_bank_management',$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	* Add Bank Detail according to there Countries
	* @param $data ARRAY
	* return TRUE
	*/

	function add_bank_details($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('alhaya_banks_detail',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Get All Registerd Companies List
	* return OBJECT
	*/

	function get_all_registerd_countries($type)
    {
		$this->db->select('register_country_id,country_listmanagement,charity_type_id,opening_acount_budget,budget_year,bank_type,created_date');
		$this->db->where('bank_type',$type);
		
		$query	=	$this->db->get('alhaya_bank_management');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get All Registerd Countres List
	* return OBJECT
	*/

	function get_country_budget_detail($register_country_id)
    {
		$this->db->select('register_country_id,country_listmanagement,charity_type_id,opening_acount_budget,budget_year,bank_type,created_date');
		$this->db->where('register_country_id',$register_country_id);
		
		$query	=	$this->db->get('alhaya_bank_management');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
    }
//-------------------------------------------------------------------
	/*
	* Add Project Type Budgets
	* @param $data ARRAY
	* return TRUE
	*/

	function add_project_type_budget($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('outside_projects_budget',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* GET Project Type Budget Values
	* return OBJECT
	*/	
	function get_project_budget_by_country_id($project_budget_id)
	{
		$this->db->select('project_budget_id,project_type,project_budget');
		$this->db->where('project_budget_id',$project_budget_id);

		$query = $this->db->get('outside_projects_budget');
		
		$project_values	=	array();
		
		if($query->num_rows() > 0)
		{
			$result	=	$query->result();

			foreach($result	as	$value)
			{
				$project_values[$value->project_type]	=	$value->project_budget_id.'_'.$value->project_budget;
			}

			return $project_values;
		}
	}	
//-------------------------------------------------------------------

	/*
	*
	*
	*/
	function get_country_detail($country_id)
	{
		$data	=	array();
		$this->db->select('register_country_id,country_listmanagement,charity_type_id,opening_acount_budget,budget_year,bank_type,created_date');
		$this->db->where('register_country_id',$country_id);
		$query	=	$this->db->get('alhaya_bank_management');
		
		if($query->num_rows() > 0)
		{
			$data['basic']	=	 $query->row();
		}
		
		$this->db->select('bank_id,register_country_id,bankid,branchid,account_no,amount_in_account,phone_no,fax_no,email_address,index_num');
		$this->db->where('register_country_id',$country_id);
		$query	=	$this->db->get('alhaya_banks_detail');
		
		if($query->num_rows() > 0)
		{
			$data['bank_detail']	=	 $query->result();
		}
		
		return $data;
	}
//-------------------------------------------------------------------

	/*
	* Update Country Basic record
	* @param $register_country_id int
	* @param $country_detail int
	* return TRUE
	*/
	function update_country_registration($register_country_id,$country_detail)
	{
		$json_data	=	json_encode(array('data'	=>	$country_detail));
		
		$this->db->where('register_country_id',$register_country_id);
		$this->db->update('alhaya_bank_management',$json_data,$this->session->userdata('userid'),$country_detail);
		
		return TRUE;
	}
//-------------------------------------------------------------------

	/*
	* Update Country Banks record
	* @param $bank_id int
	* @param $register_country_id int
	* @param $bank_detail ARRAY
	* return TRUE
	*/
	function update_bank_details($bank_id,$register_country_id,$bank_detail)
	{
		$json_data	=	json_encode(array('data'	=>	$bank_detail));
		
		$this->db->where('bank_id',$bank_id);
		$this->db->where('register_country_id',$register_country_id);
		
		$this->db->update('alhaya_banks_detail',$json_data,$this->session->userdata('userid'),$bank_detail);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	* Check this bank ID exist OR NOT
	* @param $bank_id int
	* return str
	*/

	function bankid_exist($bank_id)
    {
		$this->db->select('bank_id');
		$this->db->where('bank_id',$bank_id);
		$query	=	$this->db->get('alhaya_banks_detail');

		if($query->num_rows() > 0)
		{
			return $query->row()->bank_id;
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Default Charity Types
	* return OBJECT
	*/

	function get_default_types()
    {
		$list_ids	=	array('78','79','80','81');
		$this->db->select('list_id,list_name');
		$this->db->where_in('list_id', $list_ids);
		
		$this->db->where('list_type','charity_type');
		$this->db->where('list_status', '1');
		$this->db->where('delete_record', '0');
		
		$query	=	$this->db->get('ah_listmanagement');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Other Charity Types
	* return OBJECT
	*/

	function get_other_types()
    {
		$list_ids	=	array('78','79','80','81','205','206');
		
		$this->db->select('list_id,list_name');
		$this->db->where_not_in('list_id', $list_ids);
		$this->db->where('list_type','charity_type');
		
		$this->db->where('list_status', '1');
		$this->db->where('delete_record', '0');
		
		$query	=	$this->db->get('ah_listmanagement');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Other Charity Types
	* return OBJECT
	*/

	function get_all_types()
    {
		$list_ids	=	array('205','206');
		$this->db->select('list_id,list_name');
		$this->db->where_not_in('list_id', $list_ids);
		$this->db->where('list_type','charity_type');
		
		$this->db->where('list_status', '1');
		$this->db->where('delete_record', '0');
		
		$query	=	$this->db->get('ah_listmanagement');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Account Numbers By Type
	* @param $charity_id int
	* return OBJECT
	*/

	function get_account_numbers($charity_id)
    {
		$data	=	array();
		
		$this->db->select('opening_acount_budget');
		//$this->db->where('register_country_id',$country_id);
		$this->db->where('charity_type_id',$charity_id);
		
		$budget	=	$this->db->get('alhaya_bank_management');
		
		if($budget->num_rows() > 0)
		{
			$data['balance']	=	 $budget->row()->opening_acount_budget;
		}
		
		$query	=	$this->db->query("SELECT
		`bd`.`account_no`,`bd`.`bank_id`,`bd`.`bankid`,`bd`.`branchid`,bd.amount_in_account
		FROM
		`alhaya_banks_detail` AS bd
		INNER JOIN `alhaya_bank_management` AS bm 
        ON (`bd`.`register_country_id` = `bm`.`register_country_id`) WHERE bm.`charity_type_id`='".$charity_id."';");

		if($query->num_rows() > 0)
		{
			$data['accounts']	=	$query->result();
		}
		
		return	$data;
    }
//-------------------------------------------------------------------
	/*
	* Insert Al Haya Transaction Detail
	* @param $data ARRAY
	* return LAST inserted ID
	*/

	function save_transaction_data($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('alhaya_transactions',$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	* Insert Al Haya Local Transaction Detail
	* @param $data ARRAY
	* return LAST inserted ID
	*/

	function save_local_transaction_data($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('alhaya_local_transactions',$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	* Get All Transactions
	* return OBJECT
	*/

	function get_all_transactions()
    {
		$this->db->select('transaction_id,login_user_id,charity_type_id,account_number,user_name,user_address,user_mobile_number,transaction_type,amount,bank_id,submit_date');

		$budget	=	$this->db->get('alhaya_transactions');
		
		if($budget->num_rows() > 0)
		{
			return $budget->result();
		}
	}
//-------------------------------------------------------------------
	/*
	* Get Country ID By Account Number
	* @param $account_no int
	* return str
	*/

	function get_country_id($account_no)
    {
		$this->db->select('register_country_id');
		$this->db->where('account_no',$account_no);
		
		$query	=	$this->db->get('alhaya_banks_detail');

		if($query->num_rows() > 0)
		{
			return $query->row()->register_country_id;
		}
    }
//-------------------------------------------------------------------

	/*
	* Update Country Opening Account Budget
	* @param $register_country_id int
	* @param $data ARRAY
	* return TRUE
	*/
	function update_charity_amount($register_country_id,$amount)
	{
		// First Get Opening Amount Budget
		$this->db->select('opening_acount_budget');
		$this->db->where('register_country_id',$register_country_id);
		
		$query	=	$this->db->get('alhaya_bank_management');
		$opening_acount_budget	=	 $query->row()->opening_acount_budget;
		
		$total_amount	=	($opening_acount_budget	+	$amount);
		
		// Add Receive/Give amount into Opening Amount and update the Openining amount Budget
		$data	=	array('opening_acount_budget'	=>	$total_amount);
		
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('register_country_id',$register_country_id);
		$this->db->update('alhaya_bank_management',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	* Get Total amount in account
	* @param $bank_id int
	* @param $type str
	* @param $type_id int
	* return OBJECT
	*/

	function total_amount_in_accounts($bank_id,$type,$type_id	=	NULL)
    {
		$this->db->select('SUM(amount) AS total_amount');
		
		if($bank_id)
		{
			$this->db->where('bank_id',$bank_id);
		}
		
		$this->db->where('transaction_type',$type);
		
		if($type_id)
		{
			$this->db->where('charity_type_id',$type_id);
		}
		
		$query	=	$this->db->get('alhaya_transactions');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->total_amount;
		}
	}
//-------------------------------------------------------------------
}
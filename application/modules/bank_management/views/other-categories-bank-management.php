<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
      
        <div class="nav nav-tabs panel panel-default panel-block">
        <?php $permissions	=	$this->haya_model->check_other_permission(array('196'));?>
          	<?php //if($permissions[196]['u']	==	1):?>
          		<div class="row table-header-row"> <a class="btn btn-success" style="float:right; margin-right:20px;margin-top: 10px;" href="<?php echo base_url();?>bank_management/country_regestration/0/other"  id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
          	<?php //endif;?>
          <div class="tab-pane list-group active">

            <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th class="sorting">الدولة</th>
                  <th class="sorting">الميزانية فتح الحساب</th>
                  <th class="sorting">عام</th>
                  <th class="sorting">المساعدات</th>
                  <th class="sorting">الإجرائات</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'bank_management/ajax_countries_list/other','columns_array'=>'
                { "data": "الدولة" },
				{ "data": "الميزانية فتح الحساب" },
				{ "data": "عام" },
				{ "data": "المساعدات" },
                { "data": "الإجرائات" }')); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
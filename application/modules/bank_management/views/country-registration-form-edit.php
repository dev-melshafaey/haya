<?php 
$current_balance	=	$country_detail['basic']->opening_acount_budget;

$debit	=	$this->outside->total_amount_in_accounts(NULL,'DEBIT',$country_detail['basic']->charity_type_id);
$credit	=	$this->outside->total_amount_in_accounts(NULL,'CREDIT',$country_detail['basic']->charity_type_id);

$grand_amount	=	($current_balance	+	$debit	-	$credit);
?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
      <?php if($msg):?>
          <div class="col-md-12">
            <div style="padding: 22px 20px !important; background:#c1dfc9;">
                <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
            </div>
          </div>
      <?php endif;?>
      <div class="col-md-12 form-group">
        <form name="frm_country_registration" id="frm_country_registration"  method="post" autocomplete="off">
          <input type="hidden" id="register_country_id" name="register_country_id" value="<?php echo $country_detail['basic']->register_country_id; ?>">
          <input type="hidden" id="country_listmanagement" name="country_listmanagement" value="<?php echo '200'; ?>">
          <input type="hidden" id="bank_type" name="bank_type" value="<?php echo $country_detail['basic']->bank_type;?>">
          <div class="nav nav-tabs panel panel-default panel-block">
            <div class="tab-pane list-group active">
              <div class="col-md-12 form-group" style="margin-top: 10px; border-left: 1px solid #EFEFEF;">
                <div class="col-md-4 form-group">
                  <label class="text-warning">المساعدات :</label>
                  <select class="form-control req" name="charity_type_id" id="charity_type_id">
                  <option value="">المساعدات</option>
                  <?php if($list_types):?>
                  	<?php foreach($list_types as $type):?>
                  	<option value="<?php echo $type->list_id;?>" <?php if($type->list_id	==	$country_detail['basic']->charity_type_id):?> selected <?php endif;?>><?php echo $type->list_name;?></option>
                    <?php endforeach;?>
                  <?php endif;?>
                  </select>
                  </div>
                  <div class="col-md-4 form-group">
                  <label class="text-warning">الميزانية فتح الحساب : الرصيد الحالي ( <?php echo number_format($grand_amount,3);?> ) </label>
                  <input name="opening_acount_budget" value="<?php echo($country_detail['basic']->opening_acount_budget); ?>" placeholder="الميزانية فتح الحساب" id="opening_acount_budget" type="text" class="form-control req"onKeyUp="only_numeric(this);" readonly >
                </div>
                <div class="col-md-4 form-group">
                  <label class="text-warning">عام :</label>
                  <input name="budget_year" value="<?php echo($country_detail['basic']->budget_year); ?>" placeholder="الميزانية فتح الحساب" id="budget_year" type="text" class="form-control req" onKeyUp="only_numeric(this);">
                </div>
                <br clear="all" />
               	<h4 style="border-bottom: 2px solid #EEE;">تفاصيل البنك:</h4>
                <?php foreach($country_detail['bank_detail']	as $bank):?>
                    <?php
							$account_debit	=	$this->outside->total_amount_in_accounts($bank->bank_id,'DEBIT'); // Get SUM of  DEBIT
							$credit_debit	=	$this->outside->total_amount_in_accounts($bank->bank_id,'CREDIT'); // Get SUM of CREDIT
							
							$total_amount_in_account	=	($bank->amount_in_account	+	$account_debit	-	$credit_debit); // GET total SUM of Single Account
					?>
                <input type="hidden" name="bank_id[<?php echo $bank->index_num;?>]" id="bank_id" value="<?php echo $bank->bank_id;?>" />
                <div class="multiple-block">
                  <div class="col-md-3 form-group">
                    <label class="text-warning">إسم البنك:</label>
                    <?PHP echo $this->haya_model->create_dropbox_list_help('bankid','bank',$bank->bankid,0,'req','',$bank->index_num,'branchid'.$bank->index_num); ?> </div>
                  <div class="col-md-3 form-group">
                    <label class="text-warning">الفرع:</label>
                    <?PHP echo $this->haya_model->create_dropbox_list_help('branchid','bank_branch',$bank->branchid,$bank->bankid,'req','',$bank->index_num); ?> </div>
                  <div class="col-md-3 form-group">
                    <label class="text-warning">رقم حساب  :</label>
                    <input name="account_no[<?php echo $bank->index_num;?>]"  placeholder="رقم حساب" id="account_no" type="text" class="form-control req" value="<?php echo $bank->account_no;?>" onKeyUp="only_numeric(this);">
                  </div>
                 <div class="col-md-3 form-group">
                    <label class="text-warning">القيمة  : الرصيد الحالي ( <?php echo number_format($total_amount_in_account,3);?> )</label>
                    <input name="amount_in_account[<?php echo $bank->index_num;?>]"  placeholder="القيمة" id="amount_in_account" type="text" class="form-control amount_in_account req" value="<?php echo $bank->amount_in_account;?>" onKeyUp="add_total_budget(this);">
                  </div>
                  <div class="col-md-3 form-group">
                    <label class="text-warning">رقم الهاتف  :</label>
                    <input name="phone_no[<?php echo $bank->index_num;?>]"  placeholder="رقم الهاتف" id="phone_no" type="text" class="form-control req" value="<?php echo $bank->phone_no;?>" onKeyUp="only_numeric(this);">
                  </div>
                  <div class="col-md-3 form-group">
                    <label class="text-warning">رقم الفاكس  :</label>
                    <input name="fax_no[<?php echo $bank->index_num;?>]"  placeholder="رقم الفاكس" id="fax_no" type="text" class="form-control req" value="<?php echo $bank->fax_no;?>" onKeyUp="only_numeric(this);">
                  </div>
                  <div class="col-md-3 form-group">
                    <label class="text-warning">البريد الإلكتروني  :</label>
                    <input name="email_address[<?php echo $bank->index_num;?>]"  placeholder="البريد الإلكتروني" id="email_address" type="text" class="form-control req" value="<?php echo $bank->email_address;?>">
                  </div>
                </div>
                <br clear="all"/>
                <hr>
                <?php endforeach;?>
                <div class="col-md-12 form-group"> <a class="add_multifields" href="#_" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> </div>
                <br clear="all"/>
                <div class="col-md-12 form-group" id="multifields"></div>
                <div class="col-md-12 form-group">
                  <button type="button" id="save_country_registration" class="btn btn-success">Save</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
	$(function()
	{
		$(".add_multifields").click(function(){
			
		var total_divs	=	$(".multiple-block").length+1;
		$('#frmbuild .req').removeClass('parsley-error');
		var form_action = $('#frmbuild').attr('action');
		var ht = '<ul>';
		
		$('#frmbuild .req').each(function(index, element)
		{
			if($(this).val()=='')
			{
				$(this).addClass('parsley-error');
				ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
			}
		});
		
		var redline = $('#frmbuild .parsley-error').length;
		ht += '</ul>';
		if (redline == 9) 
		{
			show_notification_error_end(ht);
		} 
		else 
		{
			$.ajax({
			url: config.BASE_URL+'bank_management/add_multifields',
			type:"POST",
			data:{total_divs:total_divs},
			dataType:"html",				
			success: function(msg)
			{
				$("#multifields").after(msg);
			}
			});
		}
	});
	 $('#save_country_registration').click(function(){
		 $('#frm_country_registration .req').removeClass('parsley-error');
			var ht = '<ul>';
			$('#frm_country_registration .req').each(function (index, element) {
				if ($(this).val() == '') 
				{
					$(this).addClass('parsley-error');
					ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
				}
			});
			var redline = $('#frm_country_registration .parsley-error').length;
			ht += '</ul>';
						
			if (redline <= 0) 
			{	
				$("#frm_country_registration").submit();	
			}
			else 
			{	
				show_notification_error_end(ht);
			}	 
		 
	});
	});

/* Add account Budgets into Opening ccount Budgets*/	
function add_total_budget(a)
{
	only_numeric(a); // Allowed Only Numbers
	var total_amount	=	0;
	$('.amount_in_account').each(function(index, element) {
		if ($(this).val() != '') 
		{
			//$("#opening_acount_budget").val('');
			total_amount	+=	parseInt($(this).val()); // Get all amounts of Banks.
		}
	});
	
	// SET amount in Opening Budget Account
	$("#opening_acount_budget").val(total_amount);
}
</script>
</div>
</body>
</html>
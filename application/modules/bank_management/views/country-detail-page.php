<?php 
$current_balance	=	$country_detail['basic']->opening_acount_budget;

$debit	=	$this->outside->total_amount_in_accounts(NULL,'DEBIT',$country_detail['basic']->charity_type_id);
$credit	=	$this->outside->total_amount_in_accounts(NULL,'CREDIT',$country_detail['basic']->charity_type_id);

$grand_amount	=	($current_balance	+	$debit	-	$credit);
?>
<div class="col-md-12" id="myprint"  style="direction:rtl;">
  <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tr>
      <td colspan="7" class="customhr">بلد التفاصيل:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">الدولة :</label>
        <br />
        <strong><?php echo $this->haya_model->get_name_from_list($country_detail['basic']->country_listmanagement); ?></strong></td>
        <td class="right"><label class="text-warning">عام :</label>
        <br />
        <strong><?php echo $country_detail['basic']->budget_year; ?></strong></td>
        <td class="right"><label class="text-warning">المساعدات :</label>
        <br />
        <strong><?php echo $this->haya_model->get_name_from_list($country_detail['basic']->charity_type_id); ?></strong></td>
      <td class="right"><label class="text-warning">الميزانية فتح الحساب :</label>
        <br />
        <strong><?php echo number_format($grand_amount,3); ?></strong></td>
    </tr>
    <tr>
      <td colspan="7" class="customhr">تفاصيل البنك:</td>
    </tr>
    <?php foreach($country_detail['bank_detail'] as $bank):?>
    <?php
			$account_debit	=	$this->outside->total_amount_in_accounts($bank->bank_id,'DEBIT'); // Get SUM of  DEBIT
			$credit_debit	=	$this->outside->total_amount_in_accounts($bank->bank_id,'CREDIT'); // Get SUM of CREDIT
			
			$total_amount_in_account	=	($bank->amount_in_account	+	$account_debit	-	$credit_debit); // GET total SUM of Single Account
	?>
    <tr>
      <td class="right"><label class="text-warning">اسم البنك :</label>
        <br />
        <strong><?php echo $this->haya_model->get_name_from_list($bank->bankid); ?></strong></td>
      <td class="right"><label class="text-warning">الفرع :</label>
        <br />
        <strong><?php echo $this->haya_model->get_name_from_list($bank->branchid); ?></strong></td>
      <td class="right"><label class="text-warning">رقم الحساب :</label>
        <br />
        <strong><?php echo $bank->account_no; ?></strong></td>
        <td class="right"><label class="text-warning">كمية :</label>
        <br />
        <strong><?php echo number_format($total_amount_in_account,3); ?></strong></td>
      <td class="right"><label class="text-warning">رقم الهاتف :</label>
        <br />
        <strong><?php echo $bank->phone_no; ?><strong></td>
      <td class="right"><label class="text-warning">رقم الفاكس :</label>
        <br />
        <strong><?php echo $bank->fax_no; ?><strong></td>
      <td class="right"><label class="text-warning">البريد الإلكتروني :</label>
        <br />
        <strong><?php echo $bank->email_address; ?><strong></td>
    </tr>
    <?php endforeach;?>
  </table>
</div>
<div class="col-md-12 center">
  <button type="button" class="btn" id="print" onclick="printthepage('myprint');"><i class="icon-print"></i> طباعة</button>
</div>

<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12 form-group">
        <form name="frm_bank_management" id="frm_bank_management"  method="post" autocomplete="off">
        <input type="hidden" name="login_user_id" id="login_user_id" value="<?php echo $this->session->userdata('userid');?>" />
        <input type="hidden" name="transaction_type" id="transaction_type" value="DEBIT" />
        
          <div class="nav nav-tabs panel panel-default panel-block">
            <div class="tab-pane list-group active">
              <div class="col-md-12 form-group" style="margin-top: 10px; border-left: 1px solid #EFEFEF;">
             <div class="col-md-4 form-group">
                 <label class="text-warning">اسم المستخدم  :</label>
                 <input name="user_name"  placeholder="اسم المستخدم" id="user_name" type="text" class="form-control">
              </div>
              <div class="col-md-4 form-group">
                 <label class="text-warning">رقم الهاتف  :</label>
                 <input name="user_mobile_number"  placeholder="رقم الهاتف" id="user_mobile_number" type="text" class="form-control">
              </div>
             <div class="col-md-4 form-group">
                 <label class="text-warning">عنوان المستخدم  :</label>
                 <input name="user_address"  placeholder="عنوان المستخدم" id="user_address" type="text" class="form-control">
              </div>
                <div class="col-md-4 form-group">
                  <label class="text-warning">المساعدات :</label>
                  <select class="form-control req" name="charity_type_id" id="charity_type_id">
                  <option value="">المساعدات</option>
                  <?php if($list_types):?>
                  	<?php foreach($list_types as $type):?>
                  	<option value="<?php echo $type->list_id;?>"><?php echo $type->list_name;?></option>
                    <?php endforeach;?>
                  <?php endif;?>
                  </select>
                   </div>
               <div class="col-md-4 form-group" id="dynamic-data">
                  <label class="text-warning">رقم حساب  :</label>
                  <input name="bank_id"  placeholder="رقم حساب" id="bank_id" type="text" class="form-control req" readonly>
                </div>
                <div class="col-md-4 form-group">
                  <label class="text-warning">القيمة :</label>
                  <input name="amount" value="<?php //echo($company->english_name); ?>" placeholder="القيمة" id="amount" type="text" class="form-control req">
                </div>
                <div class="col-md-4 form-group">
                  <label class="text-warning">نوع الدفع :</label>
                  <select class="form-control req" name="payment_type" id="payment_type">
                  	<option value="">اختر</option>
                  	<option value="CHECK">شيك</option>
                    <option value="CASH">نقدا</option>
                    <option value="BANKTRANSFER">تحويل بنكي</option>
                  </select>
                   </div>
                <div class="col-md-4 form-group" style="display:none;" id="show-field">
                  <label class="text-warning">وثيقة :</label>
                  <input class="form-control" type="file" name="document" id="document" placeholder="وثيقة" title='وثيقة'>
                </div>
                <br clear="all"/>
                <div class="col-md-12 form-group">
                  <button type="button" id="save_transaction" class="btn btn-success">Save</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
$(function(){
	$('#charity_type_id').change(function(){
			
			var charity_id	=	$("#charity_type_id").val();
			
		var request = $.ajax({
		  url: config.BASE_URL+'bank_management/get_account_number/DEBIT',
		  type: "POST",
		  data: {charity_id:charity_id},
		  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(data)
		  {
			  $("#dynamic-data").html(data);
		  }
		});
	});
	$('#payment_type').change(function(){
			var payment_type	=	$("#payment_type").val();
			if(payment_type	==	'CHECK' || payment_type	==	'BANKTRANSFER')
			{
				$("#show-field").fadeIn();
			}
			else
			{
				$("#show-field").fadeOut();
			}
		});
	$('#save_transaction').click(function(){
		 $('#frm_bank_management .req').removeClass('parsley-error');
			var ht = '<ul>';
			$('#frm_bank_management .req').each(function (index, element) {
				if ($(this).val() == '') 
				{
					$(this).addClass('parsley-error');
					ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
				}
			});
			
			var redline = $('#frm_bank_management .parsley-error').length;
			ht += '</ul>';			
			if (redline <= 0) 
			{
				var fd = new FormData(document.getElementById("frm_bank_management"));
				
				var request = $.ajax({
				  url: config.BASE_URL+'bank_management/receive_donation',
				  type: "POST",
				  data: fd,
				  enctype: 'multipart/form-data',
				  dataType: "html",
				  processData: false,  // tell jQuery not to process the data
				  contentType: false ,  // tell jQuery not to set contentType
				  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
				  success: function(msg)
				  {
					  window.location.href = msg;
				  }
				});
			}
			else 
			{	
				show_notification_error(ht);
			}	 
		 
	});
});
</script>
</div>
</body>
</html>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
      <?php if($msg):?>
          <div class="col-md-12">
            <div style="padding: 22px 20px !important; background:#c1dfc9;">
                <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
            </div>
          </div>
      <?php endif;?>
      <div class="col-md-12 form-group">
        <form name="frm_bank_management" id="frm_bank_management"  method="post" autocomplete="off">
          <input type="hidden" name="login_user_id" id="login_user_id" value="<?php echo $this->session->userdata('userid');?>" />
          <input type="hidden" name="transaction_type" id="transaction_type" value="CREDIT" />
          <div class="nav nav-tabs panel panel-default panel-block">
            <div class="tab-pane list-group active">
              <div class="col-md-12 form-group" style="margin-top: 10px; border-left: 1px solid #EFEFEF;">
                <div class="col-md-4 form-group">
                  <label class="text-warning">اسم المستخدم  :</label>
                  <input name="user_name"  placeholder="اسم المستخدم" id="user_name" type="text" class="form-control">
                </div>
                <div class="col-md-4 form-group">
                  <label class="text-warning">رقم الهاتف  :</label>
                  <input name="user_mobile_number"  placeholder="رقم الهاتف" id="user_mobile_number" type="text" class="form-control">
                </div>
                <div class="col-md-4 form-group">
                  <label class="text-warning">عنوان المستخدم  :</label>
                  <input name="user_address"  placeholder="عنوان المستخدم" id="user_address" type="text" class="form-control">
                </div>
                <div class="col-md-4 form-group">
                  <label class="text-warning">المساعدات :</label>
                  <select class="form-control req" name="charity_type_id" id="charity_type_id">
                    <option value="">المساعدات</option>
                    <?php if($list_types):?>
                    <?php foreach($list_types as $type):?>
                    <option value="<?php echo $type->list_id;?>"><?php echo $type->list_name;?></option>
                    <?php endforeach;?>
                    <?php endif;?>
                  </select>
                </div>
                <div class="col-md-4 form-group" id="dynamic-data">
                  <label class="text-warning">رقم حساب  :</label>
                  <input name="account_number"  placeholder="رقم حساب" id="account_number" type="text" class="form-control req" readonly>
                </div>
                <div class="col-md-4 form-group">
                  <label class="text-warning">القيمة :</label>
                  <input name="amount" value="<?php //echo($company->english_name); ?>" placeholder="القيمة" id="amount" type="text" class="form-control req">
                </div>
                <br clear="all"/>
                <div class="col-md-12 form-group">
                  <button type="button" id="save_transaction" class="btn btn-success">Save</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
	$(function(){
		$('#charity_type_id').change(function(){
			
		var charity_id	=	$("#charity_type_id").val();
			
		var request = $.ajax({
		  url: config.BASE_URL+'bank_management/get_account_number/CREDIT',
		  type: "POST",
		  data: {charity_id:charity_id},
		  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(data)
		  {
			  $("#dynamic-data").html(data);
		  } 
		});
	});
	
	 $('#save_transaction').click(function(){
		 $('#frm_bank_management .req').removeClass('parsley-error');
			var ht = '<ul>';
			$('#frm_bank_management .req').each(function (index, element) {
				if ($(this).val() == '') 
				{
					$(this).addClass('parsley-error');
					ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
				}
			});
			var redline = $('#frm_bank_management .parsley-error').length;
			ht += '</ul>';			
			if (redline <= 0) 
			{	
				$("#frm_bank_management").submit();	
			}
			else 
			{	
				show_notification_error(ht);
			}	 
		 
	});
	});
</script>
</div>
</body>
</html>
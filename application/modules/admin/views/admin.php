<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 lt-ie10"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!---Software Engineer (Noor Khan)---->

<title>الهيئة العمانية للأعمال الخيرية</title>
<meta name="description" content="Durar Smart Solutions L.L.C">
<meta name="keywords" content="Durar Smart Solutions L.L.C">
<meta name="author" content="Durar Smart Solutions L.L.C">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

<!-- Fonts CSS: -->

<link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/d6220a84.bootstrap.css">
<link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/custom.css">

<!-- Page-specific Plugin CSS: -->

<link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/vendor/jquery.pnotify.default.css">
<link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/vendor/select2/select2.css">
<link href="<?PHP echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">

<!-- Proton CSS: -->

<link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/1b2c4b33.proton.css">
<link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/vendor/animate.css">
<link rel="stylesheet" href="<?PHP echo base_url(); ?>assets/css/helvatice.css" type="text/css" />
</head>

<body class="dashboard-page" style="direction:ltr !important;">
<div style="direction:rtl;"> <a href="<?PHP echo base_url(); ?>"> <img style="vertical-align: text-top !important;" src="<?PHP echo base_url();?>assets/admin/layout3/img/logo-big.png" alt="الهيئة العمانية للأعمال الخيرية"/> </a> </div>
<div class="row">
  <div class="col-md-12 animated fadeInDown ">
    <div class="col-md-3" style="background-color:#FFF;">
      <form class="mws-form" method="post" id="login_form" action="<?php echo base_url();?>admin/login_admin">
        <div class="col-md-11"  style="text-align: center;   margin-top: 1.5em;  margin-bottom: 1em;"> <!--<img src="<?PHP echo base_url();?>assets/admin/layout3/img/logo-big.png" alt="Al Haya" width="100%" height="60px">--> </div>
        <div class="col-md-11" style="text-align: center; font-size: 13px;  margin-bottom: 1em; font-weight: bold;">مرحباً الهيئة العمانية للأعمال الخيرية<br>
          أدخل إسم المستخدم و كلمة المرور الخاصة بك</div>
        <div class="col-md-11" style="text-align: center; color:#F5090D; font-size: 13px;  margin-bottom: 1em; font-weight: bold;"> <?PHP echo $this->session->flashdata('msg'); ?> </div>
        <div class="form-group col-md-12" style="direction:rtl;">
          <label class="control-label">إسم المستخدم</label>
          <input type="text" class="form-control placeholder-no-fix" name="username" id="username" autocomplete="off" placeholder="إسم المستخدم">
          <br clear="all">
        </div>
        <div class="form-group col-md-12" style="direction:rtl;">
          <label class="control-label">كلمة المرور</label>
          <input type="password" class="form-control placeholder-no-fix" autocomplete="off" name="password" id="pass_text" placeholder="كلمة المرور">
          <br clear="all">
        </div>
        <div class="form-group col-md-12" style="margin-bottom: 69px;">
          <input type="submit" class="btn btn-lg btn-success" value="تسجيل دخول"  />
        </div>
        <br clear="all">
      </form>
    </div>
    <div class="col-md-9">
      <ul class="bxslider" style="height:418px !important;">
        <?php foreach($slider_images as $bimage) {?>
        <li  style="height:418px !important;"> <a class="fancybox-button" rel="gallery1" href="<?PHP echo base_url(); ?>upload_files/banners/<?PHP echo $bimage->banner_image; ?>"> <img title="<?PHP echo $bimage->image_title; ?>" src="<?PHP echo base_url(); ?>/upload_files/banners/<?PHP echo $bimage->banner_image; ?>" /> </a> </li>
        <?php } ?>
      </ul>
    </div>
    <br clear="all">
  </div>
</div>
<footer class="ex_footer" style="text-align:center; margin-top:4px;">
  <div class="copy_right"> </div>
  <div class="copy_right"> جميع الحقوق محفوظة © 2015 الهيئة العمانية للأعمال الخيرية‎ <br />
    ‎<a href="http://www.durar-it.com"><img src="<?php echo base_url();?>assets/admin/layout3/img/c-logo.png" alt="تصميم و برمجة شركة درر للحلول الذكية" /> تصميم و برمجة شركة درر للحلول الذكية</a> </div>
</footer>
<script src="<?php echo base_url(); ?>assets/js/jquery/external/jquery/jquery.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/bxslider/jquery.bxslider.min.js"></script><!-- bxSlider CSS file -->

<link href="<?PHP echo base_url(); ?>assets/js/bxslider/jquery.bxslider.css" rel="stylesheet" />
<!-------------------------------------------> 
<!-------Fancy Box Script--------------------> 

<!-- Add mousewheel plugin (this is optional) --> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script> 

<!-- Add fancyBox main JS and CSS files --> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox-buttons.css?v=1.0.5" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox-buttons.js?v=1.0.5"></script> 

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox-thumbs.js?v=1.0.7"></script> 

<!-- Add Media helper (this is optional) --> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox-media.js?v=1.0.6"></script> 
<!-------------------------------------------> 
<script>

$(document).ready(function(){
  
  $('.bxslider').bxSlider({

	  auto:true,
	  controls: false,
	  pager: false,
	  captions: true

	  });
$(".fancybox-button").fancybox({prevEffect:'fade',nextEffect:'fade',closeBtn:true});
});

</script>
</body>
</html>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/*
	 * Properties
	 */
	private $_data	=	array();
	private $_login_userid	=	NULL;

//-----------------------------------------------------------------------

	/*
	 * Constructor
	 */
	function __construct()
	{
		 parent::__construct();	
	
		// Loade Admin Model
		$this->load->model('admin_model','admin');

		// SET Login USER ID
		$this->_login_userid	=	$this->session->userdata('userid');
		
		
	}
//-----------------------------------------------------------------------

	/*	Admin - Panel
	*	Home Page
	*/
	public function index()
	{
		
		is_logged_in($this->_login_userid);
		
		$this->load->view('admin_login',$this->_data);
	}
			
//-----------------------------------------------------------------------

	
	/*
	* Login Page
	* Enter description here ...
	*/
	public function login()
	{
		if ($this->_login_userid)
		{
			redirect(base_url().'dashboard');
			exit();
		}
		else
		{
			$this->_data['slider_images']	=	$this->admin->sliderImages();
		
			//$this->load->view('admin_login',$this->_data);
			$this->load->view('admin',$this->_data);
		}
	}	
//-----------------------------------------------------------------------			
		
	/*
	* Check User Exist
	* 
	*/
	public function login_admin()
	{
		$username	= 	$this->input->post('username');
		$password	= 	$this->input->post('password');
		$datareturn	=	$this->admin->login_admin($username,$password);	

		if($datareturn)
		{
			$userdata = $this->session->userdata("userdata");
			//echo "<pre>";
			//print_r($userdata);
			//exit;
			/*$moduleQuery = $this->db->query("SELECT module_controller FROM mh_modules WHERE `moduleid`='".$userData->landingpage."' LIMIT 1");
						 if($moduleQuery->num_rows() > 0)
						 {
							 foreach($moduleQuery->result() as $ml)
							 {
								 redirect(base_url().$ml->module_controller);
								 exit;
							 }						 
						 }
						 else
						 {
							redirect(base_url().'dashboard');
							exit;	
						 }
*/			redirect(base_url().$userdata->module_controller);
			exit();
		}
		else
		{
			redirect(base_url().'admin/login');
			exit();
		}
	}
//-----------------------------------------------------------------------

	/*
	* Destroy all SESSION
	* 
	*/			
	public function logout()
	{		
		$this->haya_model->activity_monitor($this->_login_userid,'OUT');
		$this->session->unset_userdata('userid');
		$this->session->sess_destroy();
		
		redirect(base_url().'admin');
		exit();
	}
	
	//-----------------------------------------------------------------------

	/*
	* Destroy all SESSION
	* 
	*/			
	public function logout_temporary()
	{		
		//$this->haya_model->activity_monitor($this->_login_userid,'OUT');
		$this->session->unset_userdata('userid');
		$this->session->sess_destroy();
		
		 $sessionData = $this->session->all_userdata();
		 foreach($sessionData as $key =>$val)
		 {
			if($key!='session_id' 
			   && $key!='last_activity' 
			   && $key!='ip_address' 
			   && $key!='user_agent' 
			   && $key!='userid'){
				 $this->session->unset_userdata($key);
			 }
		  }
		
		/*redirect(base_url().'admin');
		exit();*/
	}
//-----------------------------------------------------------------------
/* End of file admin.php */
/* Location: ./application/modules/admin/admin.php */
}
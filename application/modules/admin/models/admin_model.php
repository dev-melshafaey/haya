<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
	
	/*
	*  Properties
	*/
	private $_table_users;
//-------------------------------------------------------------------

	/*
	 *  Constructor
	 */
	function __construct()
	{
		parent::__construct();

		//Get Table Names from Config 
		$this->_table_users 				= 	$this->config->item('table_users');
	}
//-------------------------------------------------------------------

	/*
	* 
	* Check Login User 
	* return OBJECT
	* @param $username integer
	* @param $password integer
	*/
	
	function login_user($username,$password)
	{
		$query = $this->db->query("
		SELECT *
		FROM
		`ah_users`
		INNER JOIN `mh_modules` ON (`ah_users`.`landingpage` = `mh_modules`.`moduleid`) WHERE ah_users.`user_name`='".$username."' AND ah_users.`password`=MD5('".$password."');");
		if($query->num_rows() > 0)
		{
			$this->session->set_userdata('id',$query->row()->id);
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	 * For Login
	 * if user axist set session
	 * @param String $username
	 * @param String $pass
	 * Return true or false
	 */
		
	function login_admin($username,$pass)
	{
		$this->db->select("ah_users.userid,mh_modules.module_controller");
		$this->db->from($this->_table_users);
		$this->db->join('mh_modules','mh_modules.moduleid=ah_users.landingpage');
		$this->db->where("ah_users.userlogin",$username);
		$this->db->where("ah_users.userpassword",md5($pass));
		$this->db->where("ah_users.userstatus",1);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			//$this->session->set_userdata("userid",$query->row()->userid);
			//$this->session->set_userdata("userdata",$query->row());
			
			$data = array(
					'userid' 				=>	$query->row()->userid,
					'userdata' 				=>	$query->row(),
					'ADMIN_is_logged_in' 	=>	true
					);
			
			//$this->session->sess_expiration = '32140800'; //~ one year
			//$this->session->sess_expiration = '32140800';// expires in 4 hours
			$this->session->set_userdata($data);// set session
			
			ini_set('session.gc_maxlifetime', 18000);
				
			$this->haya_model->activity_monitor($query->row()->userid,'IN');		
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
/*-------------------------------------------------------------------------*/	
	/*
	*
	* Get all Images for Slider 
	* Get Record acording to Start and End Time
	*/
	public function sliderImages()
	{
		$imageQuery	=	$this->db->query("SELECT banner_image,image_title,start_date,end_date FROM `system_images` WHERE DATE(start_date) <= CURDATE() AND DATE(end_date) >= CURDATE() AND isactive='1' ORDER BY image_order ASC;");
		
		if($imageQuery->num_rows() > 0)
		{
			return $imageQuery->result();
		}
	}
/*-------------------------------------------------------------------------*/		
/* End of file admin_model.php */
/* Location: ./appliction/modules/admin/admin_model.php */
}
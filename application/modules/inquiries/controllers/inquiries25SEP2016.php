<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inquiries extends CI_Controller 
{
//-------------------------------------------------------------------------------	
	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;
//-------------------------------------------------------------------------------
	/*
	* Costructor
	*/
	public function __construct()
	{
		parent::__construct();
		
		// Load Models
		$this->load->model('inquiries_model','inq');
		
		$this->_data['module']			=	$this->haya_model->get_module(); // Get all Module Detail
		$this->_login_userid			=	$this->session->userdata('userid'); // Set userid into SESSION
		$this->_data['login_userid']	=	$this->_login_userid;
		$this->_data['user_detail'] 	= 	$this->haya_model->get_user_detail($this->_login_userid); // GET all detail about LOGIN User
	}
//-------------------------------------------------------------------------------
	/*
	* Show all ICONS 
	*/	
    public function index()
    {
		$this->load->view('haya/allicons', $this->_data);
    }
	
//-------------------------------------------------------------------------------

	/*
	*
	*/
	function test_send_sms()
	{
		$numbers	=	array('96898824404');
		
		send_general_sms('سب بيل س',$numbers);
	}
//-------------------------------------------------------------------------------

	/*
	* @functon newrequest
	* @param $applicantid integer
	* @param $step integer
	*/
	public function newrequest($applicantid=NULL, $step=NULL)
	{
		$this->_data['a_id']	=	$tempid;
		$this->_data['a_step']	=	$step;
		
		// New Request Form View
		$this->load->view('newrequest', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Add Helps data into Database 
	*/	
	public function submit_tasgeel()
	{
		// GET POST DATA and inserted into DATABASE
		$this->inq->submitfor();
	}
//-------------------------------------------------------------------------------
 
	/*
	* Delete APPLICANT DOCUMENT
	*/
	public function delete_document()
	{
		$docid = $this->input->post('docid'); // GET docid from POST
		
		$this->inq->delete_applicant_document(); // DELETE Document
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function medicalform($applicantid=NULL, $refil=NULL, $step=NULL)
	{
        $this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['_applicantid']	=	$applicantid;			
		$this->_data['charity_type_id'] =	80;
		$this->_data['refil_data'] 		=	$refil;
		$this->_data['form_name'] 		=	'medicalform';
		$this->_data['_applicant_data'] =	$this->haya_model->getRequestInfo($applicantid);
		
		$this->load->view('newrequest', $this->_data);	// New Request Form
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function housingform($applicantid=NULL, $refil=NULL, $step=NULL)
	{
        $this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['_applicantid']	= $applicantid;		
		$this->_data['charity_type_id'] = 78;
		$this->_data['refil_data'] 		= $refil;
		$this->_data['form_name'] 		= 'housingform';
		
		$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($applicantid);
		
		$this->load->view('newrequest', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function cashform($applicantid=NULL, $refil=NULL, $step=NULL)
	{
        $this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['a_id']			= $applicantid;	
		$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($applicantid);
		$this->_data['charity_type_id'] = 79;
		$this->_data['refil_data'] 		= $refil;
		$this->_data['form_name'] 		= 'cashform';
				
		$this->load->view('newrequest', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function educationform($applicantid=NULL, $refil=NULL)
	{
        $this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['a_id']			= $applicantid;	
		$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($applicantid);		
		$this->_data['charity_type_id'] = 81;
		$this->_data['refil_data'] 		= $refil;
		$this->_data['form_name'] 		= 'educationform';
					
		$this->load->view('newrequest', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	function get_document_list()
	{
		$charity_type_id_value	=	$this->input->post('charity_type_id_value');
		$document_list			=	$this->inq->allRequiredDocument($charity_type_id_value);
		
		echo json_encode($document_list);
	}
//-------------------------------------------------------------------------------

	/*
	*
	*
	*/
	public function get_charge_in_one()
	{
		$this->_data['get_detail_charges']	=	$this->inq->get_all_detail();
	}
//-------------------------------------------------------------------------------
	/*
	* functon Get All Document List
	*/
	public function socilservaylist($branchid=0,$charity_type=0)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		
		$this->load->view('socialservay-listing', $this->_data);	
	}
	
//-------------------------------------------------------------------------------
	/*
	* functon Get All Document List
	*/
	public function socilservaylist_completesteps($branchid=0,$charity_type=0)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		
		$this->load->view('socialservay-completesteps-listing', $this->_data);	
	}
	
	public function socilservaylistbystatus($branchid,$charity_type_id,$step)
	{
		$this->_data['branchid'] 		=	$branchid;
		$this->_data['charity_type'] 	=	$charity_type_id;
		$this->_data['step'] 			=	$step;
		
		$this->load->view("socialsurvay_list_status",$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get All Document List
	*/
	public function socilservaylist_by_types($branchid=0,$charity_type=0)
	{
		$branchid	=	$this->_data['user_detail']['profile']->branchid;
		$this->_data['moduleparent'] 	= 212;
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;		
		$this->_data["step"] 			=	1;
		$this->_data["done_step"]		=	2;
		$this->_data["method_step"] 	=	"socilservaylistbystatus";
		$this->_data['method_name']		=	'socilservaylist';
		$this->_data['complete_step']	=	'socilservaylist_completesteps';
		$this->_data['total_yateem']	=	$this->haya_model->get_yateem_counts();
		$this->_data['permission'] 		= 	json_decode($this->_data['user_detail']['profile']->permissionjson,true);
		$this->_data["bernamij_masadaat_types"]  = $this->inq->bernamij_almasadaat_types('1',$branchid); // Get all types for 1st step
			
		$this->load->view('socialservay-listing-by-types', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get All Document List
	*/
	public function socilservayresultfilledlist_by_types($branchid=0,$charity_type=0)
	{
		$branchid	=	$this->_data['user_detail']['profile']->branchid;
		$this->_data['moduleparent'] = 218;
		$this->_data['permission'] 		= 	json_decode($this->_data['user_detail']['profile']->permissionjson,true);
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data["step"] 			=	2;
		$this->_data["done_step"]		=	3;
		/*$this->_data['method_step'] 	=	"social_servay_result_return";*/
		$this->_data['method_step'] 	=	"socilservaylistbystatus";
		$this->_data['method_reject'] 	=	"social_servay_result_rejected";
		$this->_data['method_name']		=	'socilservayresultfilledlist';
		$this->_data['complete_step']	=	'socilservayresultfilledlist_completesteps';
		
		$this->_data["bernamij_masadaat_types"]  = $this->inq->bernamij_almasadaat_types('2',$branchid);// Get all types for 2nd step
		
		$this->load->view('socialservay-listing-by-types', $this->_data);	// Load View Page
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function finaldecission($branchid=0,$charity_type=0)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		
		$this->load->view('finaldecission-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get All Document List
	*/
	public function finaldecission_by_types($branchid=0,$charity_type=0)
	{
		$branchid						=	$this->_data['user_detail']['profile']->branchid;
		$this->_data['moduleparent'] = 224;
		$this->_data['permission'] 		= 	json_decode($this->_data['user_detail']['profile']->permissionjson,true);
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data['method_name']		=	'finaldecission';
		$this->_data["step"] 			=	3;
		$this->_data["done_step"]		=	4;
		$this->_data['complete_step']	=	'socilservayresultfilledlist_completesteps';
		
		$this->_data["bernamij_masadaat_types"]  =	$this->inq->bernamij_almasadaat_types('3',$branchid);
		
		$this->load->view('socialservay-listing-by-types', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socilservayresultfilledlist($branchid=0,$charity_type=0)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		
		$this->load->view('socialservay-listingspecial', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Compelted Steps
	*/	
	public function socilservayresultfilledlist_completesteps($branchid=0,$charity_type=0,$step)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data['step']			=	$step;
		
		$this->load->view('socialservay-listingspecial-completesteps', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function social_servay_result_return($charity_type=0)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['charity_type']	=	$charity_type;
			
		$this->load->view('socialservay-listingspecial-type', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function social_servay_result_rejected($charity_type=0)
	{
		//$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['charity_type']	=	$charity_type;
		
		$this->load->view('ssr_rejected', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get Information from ID Card
	*/

	public function getIDCardNumber()
	{	
		$term 				=	$this->input->get('term');
		$query = $this->db->query("SELECT ah_applicant.`applicantid`,ah_applicant.`charity_type_id`,ah_applicant.`date_of_birth`, ah_listmanagement.`list_name`,ah_applicant.`fullname`,ah_applicant.`idcard_number`,ah_branchs.`branchname`,province.`list_name` AS pro, wilaya.`list_name` AS wil FROM ah_applicant
									JOIN ah_listmanagement ON (ah_listmanagement.`list_id`=ah_applicant.`charity_type_id`)
									JOIN ah_listmanagement AS province ON (province.`list_id`=ah_applicant.`province`)
									JOIN ah_listmanagement AS wilaya ON (wilaya.`list_id`=ah_applicant.`wilaya`)
									JOIN ah_branchs ON (ah_applicant.`branchid`=ah_branchs.`branchid`)
									WHERE ah_applicant.`idcard_number` LIKE '%".$term."%' ORDER BY ah_applicant.`applicantid` ASC ");
		
		foreach($query->result() as $res) 
		{
			$refil_data_url = charity_edit_url($res->charity_type_id).$res->applicantid.'/refilData';
			$fullname = $res->fullname.' ('.$res->idcard_number.') فرع '.$res->branchname.' '.$res->pro.' ولاية '.$res->wil;
			$arr[] = array('id'=>$res->applicantid,'label'=>$fullname,'value'=>$res->idcard_number,'dob'=>$res->date_of_birth,'refil_data_url'=>$refil_data_url);
		}
		echo json_encode($arr);
		// GET DATA according to ID CARD NUMMBER
		//echo $result		=	$this->inq->get_detail_by_param($select_fields,$condition,$charity_type_id,'CARD');
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get Information from ID Card
	*/

	public function get_mother_id_card_number()
	{	
		$term 				=	$this->input->get('term');
		$charity_type_id 	=	$this->input->get('charity_type_id');
		$select_fields		=	'applicantid,fullname,mother_name,mother_id_card,charity_type_id';
		$condition 			=	array('mother_id_card'	=>	$term);
		
		// GET DATA according to ID CARD NUMMBER
		echo $result		=	$this->inq->get_detail_by_param($select_fields,$condition,$charity_type_id,'MOTHER CARD');
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get Information from ID Card
	*/

	public function get_father_id_card_number()
	{	
		$term 				=	$this->input->get('term');
		$charity_type_id 	=	$this->input->get('charity_type_id');
		$select_fields		=	'applicantid,fullname,father_name,father_id_card,charity_type_id';
		$condition 			=	array('father_id_card'	=>	$term);
		
		// GET DATA according to ID CARD NUMMBER
		echo $result		=	$this->inq->get_detail_by_param($select_fields,$condition,$charity_type_id,'FATHER CARD');
	}	
//-------------------------------------------------------------------------------

	/*
	* functon Get Information from ID Card
	*/
	public function getPassportNumber()
	{
		$term 				=	$this->input->get('term');
		$charity_type_id 	=	$this->input->get('charity_type_id');
		$select_fields		=	'applicantid,fullname,passport_number,charity_type_id';
		$condition 			=	array('passport_number'	=>	$term);
		
		// GET DATA according to PASSPORT NUMMBER
		echo $result		=	$this->inq->get_detail_by_param($select_fields,$condition,$charity_type_id,'PASSPORT');
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get Information from ID Card
	*/
	public function getPassportNumberWife()
	{
		$term = $this->input->get('term');
		$charity_type_id = $this->input->get('charity_type_id');
		
		echo $this->inq->get_passport_number_wife_info($term,$charity_type_id); // Only removes query from here an make function in Model.
	}			
//-------------------------------------------------------------------------------

	/*
	* functon Adding Social Servay Result
	*/	
	public function add_socilservayresult()
	{
		$sarvayid 					=	$this->input->post('sarvayid');
		$applicantid 				=	$this->input->post('applicantid');
		$userid 					=	$this->_login_userid;
		$health_condition 			=	$this->input->post('health_condition');
		$health_condition_text 		=	$this->input->post('health_condition_text');
		$housing_condition 			=	$this->input->post('housing_condition');
		$housing_condition_text 	=	$this->input->post('housing_condition_text');
		$numberofpeople 			=	$this->input->post('numberofpeople');
		$positioninfamily 			=	$this->input->post('positioninfamily');
		$economic_condition 		=	$this->input->post('economic_condition');
		$economic_condition_text 	=	$this->input->post('economic_condition_text');
		$casetype 					=	$this->input->post('casetype');
		$aps_name 					=	$this->input->post('aps_name');
		$aps_account 				=	$this->input->post('aps_account');
		$aps_filename 				=	$this->input->post('aps_filename');
		$aps_category 				=	$this->input->post('aps_category');
		$aps_category_text 			=	$this->input->post('aps_category_text');
		$aps_salaryamount 			=	$this->input->post('aps_salaryamount');
		$aps_date 					=	$this->input->post('aps_date');
		$aps_another_income 		=	json_encode($this->input->post('aps_another_income'));
		$whyyouwant 				=	$this->input->post('whyyouwant');
		$summary 					=	$this->input->post('summary');
		$review 					=	$this->input->post('review');
		$ajel 						=	$this->input->post('ajel');
		$maintenance_type 			=	$this->input->post('maintenance_type');
		$report_visit 				=	$this->input->post('report_visit');
		$certificate_date 			=	$this->input->post('certificate_date');
		$proposal_section 			=	$this->input->post('proposal_section');
		$aps_month 					=	$this->input->post('aps_month');
		$aps_year 					=	$this->input->post('aps_year');
		
		
		$ah_applicant_survayresult = array(
			'applicantid'				=>	$applicantid,
			'userid'					=>	$userid,
			'health_condition'			=>	$health_condition,
			'health_condition_text'		=>	$health_condition_text,
			'housing_condition'			=>	$housing_condition,
			'housing_condition_text'	=>	$housing_condition_text,
			'numberofpeople'			=>	$numberofpeople,
			'positioninfamily'			=>	$positioninfamily,
			'economic_condition'		=>	$economic_condition,
			'economic_condition_text'	=>	$economic_condition_text,
			'casetype'					=>	$casetype,
			'aps_name'					=>	$aps_name,
			'aps_account'				=>	$aps_account,
			'aps_filename'				=>	$aps_filename,
			'aps_category'				=>	$aps_category,
			'aps_category_text'			=>	$aps_category_text,
			'aps_salaryamount'			=>	$aps_salaryamount,
			'aps_date'					=>	$aps_date,
			'aps_another_income'		=>	$aps_another_income,
			'aps_month'					=>	$aps_month,
			'aps_year'					=>	$aps_year,
			'whyyouwant'				=>	$whyyouwant,
			'summary'					=>	$summary,
			'ajel'						=>	$ajel,
			'maintenance_type'			=>	$maintenance_type,
			'report_visit'				=>	$report_visit,
			'proposal_section'			=>	$proposal_section,
			'certificate_date'			=>	$certificate_date,
			'review'					=>	$review);
			
		$this->db->query("UPDATE `ah_applicant` SET `application_status`='' WHERE `applicantid`='".$applicantid."'");
		//$this->haya_model->save_steps($applicantid,3);
		
		$data	=	array(
		'bankid'		=>	$this->input->post('bankid'),
		'bankbranchid'	=>	$this->input->post('bankbranchid'),
		'accountnumber'	=>	$this->input->post('accountnumber'));
		
		$this->inq->update_applicant_in_socialservay($applicantid,$data);
			
		if($sarvayid!='')
		{
			$this->inq->update_survay($applicantid,$ah_applicant_survayresult); // UPDATE SURVAY
			
			$this->haya_model->save_steps($applicantid,2);	// SAVE STEP
			$this->haya_model->update_steps($applicantid,2);
		}
		else
		{
			$this->haya_model->save_steps($applicantid,2);	// SAVE STEP
			$this->haya_model->update_steps($applicantid,2);
			$this->inq->add_survay($ah_applicant_survayresult); // ADD SURVAY
		}
		
		
		$query_type		=	$this->db->query("SELECT charity_type_id FROM `ah_applicant` WHERE `applicantid`='".$applicantid."'");
		$type			=	$query_type->charity_type_id;
		
		/*if($type)
		{
			redirect(base_url().'inquiries/socilservaylist/'.$this->_login_userid.'/'.$type);
			exit();
		}*/
		redirect(base_url().'inquiries/socilservaylist_by_types/');
		exit();

	}
//-------------------------------------------------------------------------------

	/*
	* functon Checking Information
	*/	
	public function checkFurture()
	{
		$id 	=	$this->input->post('id');
		$type 	=	$this->input->post('type');
		
		switch($type)
		{
			case 'idcard_number';
				echo $this->inq->get_applicant_id($id); // RETURN applicationid 
			break;
			case 'passport_number';
				echo $this->inq->get_passport_number($id); // RETURN applicationid 
			break;
			case 'passport_number_wife';
				echo $this->inq->get_passport_number_wife($id); // RETURN applicationid 
			break;
		}
	}
//-------------------------------------------------------------------------------

	/*
	* functon Showing informtion according to the resule
	*/
	function checkbefore($id, $type)
	{		
		switch($type)
		{
			case 'idcard_number';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail', $this->_data);
			break;
			case 'passport_number';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail', $this->_data);
			break;
			case 'passport_number_wife';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail', $this->_data);
			break;			
		}
	}
//-------------------------------------------------------------------------------

	/*
	* functon Showing informtion according to the resule
	*/
	function checkbefore_print($id, $type)
	{		
		switch($type)
		{
			case 'idcard_number';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail-print', $this->_data);
			break;
			case 'passport_number';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail-print', $this->_data);
			break;
			case 'passport_number_wife';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail-print', $this->_data);
			break;	
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function send_sms_to_user()
	{
		$phone_numbers 	= implode(',',$this->input->post('phoneNumbers'));
		
		/*$dTime 			=  $this->input->post('date_time');
		if($dTime!='')
		{
			$dateTime = $dTime;	
		}
		else
		{
			$dateTime = date('Y-m-d');	
		}
		
		$id 	=	$this->input->post('hd_id');
		$msx 	=	$this->input->post('expiry_msg');	
		
		//$replace	=	array(arabic_date(applicant_number($id)),arabic_date($dateTime));
		//$message	=	str_replace(sms_lagend(),$replace,$msx);
		$sms_time	=	$this->input->post('sms_time');
		$type 		=	$this->input->post('type');*/
		$message	=	$this->input->post('expiry_msg');
		
		
		
		$return	=	send_general_sms($message,$phone_numbers); // Send SMS
		
		/*$numbers_arr = explode(',',$phone_numbers);
		if(!empty($numbers_arr)){
				foreach($numbers_arr as $new){

					$myData['sms_receiver_id'] = $id;
					$myData['sms_sender_id'] = $this->session->userdata('userid');
					$myData['sms_receiver_number'] = $new;
					$myData['sms_module_id'] = $sms_module_id;
					$myData['sms_sent_date'] = $dateTime;
					$myData['sms_message'] = $message;
					$myData['sms_sent_type_time'] = 'Now';
					$myData['type'] = 'sms';
					$myData['sms_list'] = $sms_list;
					$newData[] = $myData;
				}
		}*/
				
		//$this->db->insert_batch('ah_sms_history',$newData);
		
		if($return)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function bulksms()
	{
		$this->load->view('bulksms',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function smsmodal($id,$type)
	{
		$response	=	$this->inq->get_extratelephone($id); // GET EXETRA telephone number
		
		$this->_data['phone_numbers']	=	json_decode($response);
		$this->_data['applicantid']		=	$id;
		$this->_data['type'] 			=	$type;
		$this->_data['template'] 		=	$this->inq->get_template(); // GET template
		
		$this->load->view('smsmodal',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* SMS Template
	*/	
	public function smstemplate()
	{
		$this->haya_model->check_permission($this->_data['module'],'v'); //Checking View Permission
		$this->load->view('smstemplate', $this->_data); //Loading Template View
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function addupdate_sms_template()
	{
		$templateid = $this->input->post('template_id');
		
		if($this->input->post('from_form')	==	'1')
		{			
			$data = array(
				'addedby'			=>	$this->session->userdata('userid'),
				'templatesubject'	=>	$this->input->post('templatesubject'),
				'template'			=>	$this->input->post('template'));
			
			if($templateid!='')
			{
				$this->inq->update_sms_template($templateid,$data);	// Update SMS Template
			}
			else
			{
				$this->inq->add_sms_template($data); // Add SMS Template
			}
		}

		$this->_data['sms'] = $this->inq->get_all_sms_templates($templateid); // GET all SMS templates
		
		$this->load->view("addupdate_sms_template",$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function templatedelete($tempid)
	{
		if($tempid!='') { $this->db->query("DELETE FROM system_sms_template WHERE templateid='".$tempid."' "); }
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function smstemplate_ajax()
	{
		$permissions	=	$this->haya_model->check_other_permission(array('99'));
		
		$conut	=	0;
		
		$result	=	$this->inq->get_templates();	// GET SMS Templates
				
		foreach($result as $smsresult)
		{
			if($permissions[99]['u']	==	1)
			{
				$actions .='<a href="#1" id="'.$smsresult->templateid.'" onClick="add_update_sms_template(this);"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
            
			if($permissions[99]['d']	==	1)
			{
				$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$smsresult->templateid.'" data-url="'.base_url().'inquiries/templatedelete/'.$smsresult->templateid.'"><i style="color:#F00;" class="icon-remove-circle"></i></a>';
			}
                
			$arr[] = array(
				"DT_RowId"		=>	$smsresult->templateid.'_durar_lm',				
				"عنوان الرسئل" 	=>	$smsresult->templatesubject,              
				"تاريخ" 		=>	arabic_date($smsresult->tempatedate),
				"عدد الرسالة" 	=>	arabic_date(strlen($smsresult->template)),
				"الإجراءات" 		=>	$actions
				);
				unset($actions);
		}
		
		echo json_encode($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function inquiries_filter($userid='',$branchid='')
	{
		//check_permission($this->_data['module'],'v');		
		$branchid = $_GET['branch_code'];		
        //$this->_data['inquiries'] = $this->inq->getmaindata($branchid);
		$this->_data['branchid'] = $branchid;
        $this->_data['blist'] = $this->inq->muragain_branch_count();
		
        $this->load->view('inquiries_list', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function systempeople()
	{
		 check_permission($this->_data['module'],'v');
		 $this->load->view('systempeople', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function allappointments()
	{
		$this->load->view('allappointments', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function testevent()
	{
		send_sms_steps(1,'96898818663,96892463374,96897890223,96898824404,96892324717',35);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function findkey()
	{
		$keyvalue = (int)$this->input->post('keyvalue');
		$keytype = (int)$this->input->post('keytype');
		
		switch($keytype)
		{
			case '4';				
				$this->db->select('tempid');
				$this->db->from('main');		
				$this->db->where('tempid',$keyvalue);
				$query = $this->db->get();
				if($query->num_rows() > 0)
				{	$ar['url'] = base_url().'inquiries/newinquery/'.$keyvalue;	}
				else
				{	$ar['error'] = 'لا تتوافر بيانات';	}
				echo json_encode($ar);
			break;
			case '17';				
				$this->db->select('applicant_id');
				$this->db->from('applicants');		
				$this->db->where('applicant_id',$keyvalue);
				$query = $this->db->get();
				if($query->num_rows() > 0)
				{	$ar['url'] = base_url().'inquiries/newrequest/'.$keyvalue;	}
				else
				{	$ar['error'] = 'لا تتوافر بيانات';	}
				echo json_encode($ar);
			break;
		}
	}
	
//-------------------------------------------------------------------------------


// 29/12/2014	-------------------------------------	START
	

	public function userhistory()
	{
        check_permission($this->_data['module'],'v');
		$this->load->view('userhistory', $this->_data);
	}
	
// 29/12/2014	-------------------------------------	END
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function applicantsList(){
        check_permission($this->_data['module'],'v');
        $this->_data['all_applicatns']	=	$this->inq->getAprovalStepData();
		$this->load->view('banklist', $this->_data);
	
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function getsavedocument()
	{
		echo json_encode($this->inq->getsave_document());
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function get_sms_history($module_id,$userid)
	{
		$this->_data['module_id']	=	$module_id;
		$this->_data['userid']		=	$userid;
		
		$this->_data['sms_history']	=	$this->inq->get_sms_history($module_id,$userid);
		
		$this->load->view('history-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function list_sms_history($module_id,$userid)
	{
		$result	=	$this->inq->get_sms_history_list($module_id,$userid); // GET all SMS history List
		
		foreach($result as $lc)
		{
			if($module_id	==	'1')
			{
               $name	=	   $lc->first_name.' '.$lc->middle_name.' '.$lc->last_name.' '.$history->family_name;
			}
			else
			{
               $name	=   $lc->applicant_first_name.' '.$lc->applicant_middle_name.' '.$lc->applicant_last_name.' '.$lc->applicant_sur_name ;
			}
                  
			 $arr[] = array(
			"DT_RowId"		=>	$lc->sms_id.'_durar_lm',
			"اسم المتلقي" 	=>	$name,              
			"رقم المتلقي" 	=>	$lc->sms_receiver_number,
			"رسالة" 		=>	$lc->sms_message,
			"اسم المرسل" 	=>	$lc->firstname.' '.$lc->lastname,
			"التاريخ" 		=>	date("Y-m-d",strtotime($lc->sms_sent_date))
			);
		}
		
		echo json_encode($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function bank_users()
	{
        check_permission($this->_data['module'],'v');
		$this->_data['all_users']	=	$this->inq->get_all_banks();
		
		// Load Users Listing Page
		$this->load->view('bank-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function get_calc_data($applicantid	=	NULL)
	{
		$postData = $this->input->post();
		
		$this->_data['post_data']	= $postData;
		echo $HTML	=	$this->load->view('ajax-calc-html', $this->_data,TRUE);
	}

//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function newcompany($tempid='')
	{
        check_permission($this->_data['module'],'v');
		$this->_data['user_info'] = userinfo();
		if($tempid!='')
		{
			$this->_data['m'] = $this->inq->getLastDetail($tempid);
			$this->_data['t'] = 'review';
		}
		else
		{
			$this->_data['m'] = $this->inq->new_inquery();
		}
		
		$this->_data['page'] = 'addinq';
		
		$this->load->view('newinquery', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function requestProjectIlust(){
        check_permission($this->_data['module'],'v');
		$this->load->view('requestprojectIllus_view', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function loanEvolution(){
        check_permission($this->_data['module'],'v');
		$this->load->view('request_loan_view', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function user_applicants($user_id)
	{
    	//check_permission($this->_data['module'],'v');	
		$this->_data['user_id'] = $user_id;	 
		$this->load->view('applicant-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function socilservayresult($applicantid='',$temp2='',$temp3='')
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
			
		$this->_data['_applicantid']		=	$applicantid;			
		$this->_data['_applicant_data']		=	$this->haya_model->getRequestInfo($applicantid);
		
		$this->load->view('requestphasefive', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socialnoneditable($applicantid='',$temp2='',$temp3='')
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
			
		$this->_data['_applicantid']	=	$applicantid;			
		$this->_data['_applicant_data'] =	$this->haya_model->getRequestInfo($applicantid);	
		$this->_data['current_status'] 	=	$this->inq->getLast_decission($applicantid,2);
		$this->_data['all_status'] 		=	$this->inq->get_all_decission($applicantid,2);
		
		$this->load->view('requestphasefivenoneditable', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function add_decission()
	{
		$applicantid 		= $this->input->post('applicantid');
		$application_status = $this->input->post('application_status');
		
		if($application_status)
		{
			if($applicantid!='')
			{
				if($application_status=='إعادة نظر')
				{	$app_status = '1';	
					$ah_applicant['isOld'] = 1;
				}
				else if($application_status=='رفض')
				{	$app_status = '2';	}
				else
				{	$app_status = '3';	}
				$ah_applicant['application_status'] = $application_status;
				$ah_applicant['applicant_notes'] 	= $this->input->post('notes');
				
				
				
				$this->inq->update_decission($applicantid,$ah_applicant); // Update Record
				
				//Adding data into dicission table
				$ah_applicant_decission['applicantid'] 	=	$applicantid;
				$ah_applicant_decission['userid'] 		=	$this->_login_userid;
				$ah_applicant_decission['decission'] 	=	$application_status;
				$ah_applicant_decission['step'] 	=	2;
				$ah_applicant_decission['notes'] 		=	$this->input->post('notes');
				$ah_applicant_decission['decissionip'] 	=	$_SERVER['REMOTE_ADDR'];
				
				$this->haya_model->update_steps($applicantid,$app_status); //Update Record
				$this->inq->add_decission($ah_applicant_decission); // Add Record				
				$this->haya_model->save_steps($applicantid,$app_status);
				
				
				$get_branch_type	=	$this->inq->get_branch_type($applicantid); // Get Brnch ID and Charity Type ID
				
				redirect(base_url().'inquiries/socilservayresultfilledlist/'.$get_branch_type->branchid.'/'.$get_branch_type->charity_type_id);
				exit();
			}			
		}
	}
	
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function requestPhase()
	{
        check_permission($this->_data['module'],'v');
		$this->load->view('requestphasefive');
	}
	
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function uploadDocument()
	{
		$postData 			=	$this->input->post();
		$fileSesionName 	=	$this->session->userdata('fileSesionName');
		$fileSesionName;
		$session 			= 	explode('_',$fileSesionName);
		$f					=	$session[2];
		
		/*$ext 				=	pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
		$name 				= 	generateCode();
		$newname  			=  	$name.'.'.$ext;
		
		$targetPath = 	"./upload_files/documents/".$newname;
		
		if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetPath))
		{
			echo json_encode(array('status' => 'ok','name' => $newname,'type' => $f));
		}
		else
		{
			echo json_encode(array('status' => 'error'));
		}*/
/*****************************************************/		
// Muzaffar Updated this Function
// Date : 7-Feb-2016
/*---------------------------------------------------*/
		$config['upload_path'] 		=	'./upload_files/documents/';
		$config['allowed_types'] 	=	'gif|jpg|png';
		$config['max_size']			=	'500000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode(array('status' => 'error'));
		}
		else
		{
			$image_data		= 	$this->upload->data();
			$file_name 		= 	$image_data['file_name'];
			
			echo json_encode(array('status' => 'ok','name' => $file_name ,'type' => $f));
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function attachment($id)
	{
		$this->_data['mustarik']	=	$id;
		$this->load->view('view_attachment', $this->_data);
	}
//-------------------------------------------------------------------------------	    
	/*
	* update info of applicant comitte_decision
	* @param int $applicant_id
	* Created by M.Ahmed
	*/
	public function generateRandomString($length = 10) 
	{
		$characters 		=	'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength 	=	strlen($characters);
		$randomString 		=	'';
		
		for ($i = 0; $i < $length; $i++) 
		{
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		
		return $randomString;
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
    public function findExtension($filename)
	{
	   $filename 	=	strtolower($filename) ;
	   $exts 		=	explode(".", $filename) ;
	   $n 			=	count($exts)-1;
	   $exts 		=	$exts[$n];
	   
	   return $exts;
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function uploadFile(){
		
		if (!file_exists(FCPATH.'upload_files/documents/')) 
		{
			mkdir(FCPATH.'upload_files/documents/', 0777, true);
		}
		
		/*$file 	=	$_FILES['file']['name'];
		$name 		=	$this->generateRandomString(5);
		$ext 		=	$this->findExtension($file);
		$filename 	=	$name.".".$ext;
		
		if(move_uploaded_file($_FILES['file']['tmp_name'], FCPATH.'upload_files/documents/'.$filename))
		{
			$image	=	'<i class="delete-icon icon-remove-sign doc8remove0" style="color:#CC0000;cursor:pointer" onclick="deleteDoc(this,\''.$filename.'\');"></i>';
			echo json_encode(array('status' => 'ok','filename'=>$filename,'delicon'=>$image));
		}
		else
		{
			echo json_encode(array('status' => 'error'));
		}*/
		
		// Muzaffar Updated this Function
		// Date : 7-Feb-2016
		/*---------------------------------------------------*/
		$config['upload_path'] = './upload_files/documents/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '500000';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode(array('status' => 'error'));
		}
		else
		{
			$image_data		= 	$this->upload->data();
			$file_name 		= 	$image_data['file_name'];
			
			$image	=	'<i class="delete-icon icon-remove-sign doc8remove0" style="color:#CC0000;cursor:pointer" onclick="deleteDoc(this,\''.$file_name.'\');"></i>';
			echo json_encode(array('status' => 'ok','filename'=>$file_name,'delicon'=>$image));
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	function uploadGuranteeAttachment()
    {
		if (!file_exists(FCPATH.'upload_files/documents/')) 
		{
			mkdir(FCPATH.'upload_files/banners/', 0777, true);
		}
		
		//---------------------------------------------------
		$config['upload_path'] = './upload_files/documents/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '500000';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode(array('status' => $error,'errorfilename'=>$image_name));
		}
		else
		{
			$image_data		= 	$this->upload->data();
			$image_name 	= 	$image_data['file_name'];
			echo json_encode(array(
			'status'	=>	'ok',
			'filename'	=>	$image_name)
			);
		}
	}
	
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function uploadBanner(){
		
		if (!file_exists(FCPATH.'upload_files/banners/')) 
		{
			mkdir(FCPATH.'upload_files/banners/', 0777, true);
		}
		
		//---------------------------------------------------
		$config['upload_path'] = './upload_files/banners/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '500000';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode(array('status' => $error,'errorfilename'=>$image_name));
		}
		else
		{
			$image_data		= 	$this->upload->data();
			$image_name 	= 	$image_data['file_name'];
			
			$path			=	base_url().'upload_files/banners/'.$image_name;
			
			$image			=	'<i class="icon-remove-sign doc8remove0" style="color:#CC0000;cursor:pointer" onclick="deleteFile(\''.$image_name.'\');"></i>';
			$image		   .= 	'<img src="'.$path.'" width="400"/>';
			
			echo json_encode(array(
				'status'	=>	'ok',
				'filename'	=>	$image_name,
				'image'		=>	$image,
				'dataaaa'	=>	$id)
				);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function add_banner_detail()
	{		
		if($_POST['imageid'])
		{
			if($_POST['attachmentbanner'])
			{
				$image_name	=	$_POST['attachmentbanner'];
				
				if(file_exists(rootpath.'upload_files/banners/'.$_POST['db_banner_image']))
				{
					unlink(rootpath.'upload_files/banners/'.$_POST['db_banner_image']);
				}
			}
			else
			{
				$image_name	=	$_POST['db_banner_image'];
			}
			
			$update_data	=	array(
				'userid'		=>	$_POST['userid'],
				'branch_id'		=>	$_POST['branch_id'],
				'image_title'	=>	$_POST['image_title'],
				'image_order'	=>	$_POST['image_order'],
				'banner_image'	=>	$image_name	
				);
		
			$this->haya_model->update_banner($_POST['imageid'],$update_data);
		}
		else
		{
			$add_data	=	array(
			'userid'		=>	$_POST['userid'],
			'branch_id'		=>	$_POST['branch_id'],
			'image_title'	=>	$_POST['image_title'],
			'image_order'	=>	$_POST['image_order'],
			'banner_image'	=>	$_POST['attachmentbanner']
			);
			
			$this->haya_model->add_banner($add_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function update_status($status,$image_id)
	{	
		if($status	==	'0')
		{
			$data	=	array('isactive'	=>	'1');
		}
		else
		{
			$data	=	array('isactive'	=>	'0');
		}
			
		$this->haya_model->update_status($image_id,$data);
		
		redirect(base_url().'inquiries/bannerslisting');
		exit();
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function bannerslisting()
	{
		$this->load->view('banners-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function requestphasesix(){
        check_permission($this->_data['module'],'v');
		$this->load->view('requesphasesix', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function listingprelimanryAproval(){
        check_permission($this->_data['module'],'v');
		$this->load->view('listingaproval_view', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function listingaproval(){

		$this->load->view('listingaprovali_view', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function resetinq()
	{
		$this->inq->reset_inquery();
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function getInquirysms($type = 'sms')
	{

		$this->_data['type']	=	$type;
		
		$inqSms = $this->inq->getInquiriesSms($type);
		
		// For Saving
		if($this->input->post('submit'))
		{
			//-----------------------------------------------------------------
			$form_data		=	$this->input->post();
			unset($form_data['submit']);
			
			$firstData	=	array();
			$count		=	count($this->inq->getInquiriesSms($type));
			
			for($i=1;$i<=$count;$i++)
			{
				$sms_reminder	=	'sms_reminder_type_'.$i;
				$reminder_count	=	'reminder_count_'.$i;
				$sms_value		=	'sms_value_'.$i;
				$sms_id			=	'sms_id_'.$i;
				$sms_heading	=	'sms_heading_'.$i;
				
				$firstData['sms_heading']			= $form_data[$sms_heading];
				$firstData['sms_remider'] 			= $form_data[$sms_reminder];
				$firstData['sms_reminder_counter'] 	= $form_data[$reminder_count];
				$firstData['sms_value'] 			= $form_data[$sms_value];
				$firstData['type'] 					= $form_data['type'];
				
				$condition = array('sms_id'=>$form_data[$sms_id]);
				
				
				$return	=	$this->inq->update_db('sms_management',$firstData,$form_data[$sms_id]);

			}
			if($return)
			{
				$this->session->set_flashdata('msg', 'S');	
			}
			else
			{
				$this->session->set_flashdata('msg', 'E');	
			}
			
			// UNSET ARRAY key
			$this->_data['page'] = 'addinqsms';
			redirect('inquiries/getInquirysms/'.$type);
		}
		
		$this->_data['inq_info_sms']	=	$inqSms;
		
		// View Inq Type Messages		
		$this->load->view('inqType', $this->_data);
	
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function sendms()
	{
		$numbers = array('96898824404','96893338241');
		echo send_sms('1',$numbers,'mesage');
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function add($listid	= NULL)
	{
		if($listid)
		{
			$this->_data['single_list']	=	$this->listing->get_single_record($listid);	
		}
		
		if($this->input->post())
		{
			$data =	$this->input->post();
			// UNSET ARRAY key
			unset($data['submit']);
			if($this->input->post('list_id'))
			{
				$this->listing->update_list($this->input->post('list_id'),$data);
				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				}
				else
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
					redirect(base_url()."inquiries/listing");
					exit();
				}
				
			}
			else
			{

				$this->listing->add_list($data);
				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				}
				else
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
					redirect(base_url()."inquiries/listing");
					exit();
				}

			}
		}
		else
		{
			if($listid)
			{
				$this->_data['list_id']	=	$listid;
			}
			else
			{
				$this->_data['list_id']	=	'';
			}
			
            check_permission($this->_data['module'],'v');
			$this->load->view('inquiries/add', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function addrules1($listid	= NULL)
	{
		if($listid)
		{
			$this->_data['single_list']	=	$this->listing->get_single_record($listid);	
		}
		
		if($this->input->post())
		{
			$data		=	$this->input->post();
			
			// UNSET ARRAY key
			unset($data['submit']);
			
			if($this->input->post('list_id'))
			{
				$this->listing->update_list($this->input->post('list_id'),$data);
				
				$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				redirect(base_url()."inquiries/listing");
				exit();
			}
			else
			{
				$this->listing->add_list($data);
				
				$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				redirect(base_url()."inquiries/listing");
				exit();
			}
		}
		else
		{
			if($listid)
			{
				$this->_data['list_id']	=	$listid;
			}
			else
			{
				$this->_data['list_id']	=	'';
			}
            check_permission($this->_data['module'],'v');
			$this->load->view('inquiries/rules-listing', $this->_data);
		}
		
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function listing($type	=	NULL)
	{
		if($type)
		{
			$this->_data['type']	=	$type;

			// Get List Name By Type
			$this->_data['listing']	=	$this->listing->get_all_by_type($type);

			$this->_data['list_type_name']	=	$type;

			$this->load->view('type-listing', $this->_data);
		}
		else
		{
			$this->_data['marital_count']	=	$this->listing->total_count('maritalstatus');
			$this->_data['situation_count']	=	$this->listing->total_count('current_situation');
			$this->_data['inquiry_type']	=	$this->listing->total_count('inquiry_type');

			$this->load->view('listing', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function types_listing($type	=	NULL)
	{
        check_permission($this->_data['module'],'v');
		if($type)
		{
			// Get List Name By Type
			$this->_data['listing']	=	$this->listing->by_type($type);
			
			$this->_data['list_type_name']	=	$type;
			
			$this->load->view('types-listing', $this->_data);
		}
		else
		{
			$this->load->view('typeslisting', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function tadeel_qaima()
	{
		$segment = $this->uri->segment(3);
		
		foreach($this->listing->get_list_type() as $listdata) 
		{
			$typename = list_types($listdata->list_type);
			
			$action =' <a href="'.base_url().'inquiries/listing/'.$listdata->list_type.'">عرض الكل</a> '; 
			
			$action .= ' <button onClick="gototype(this);" type="button" data-url="'.base_url().'inquiries/listing/'.$listdata->list_type.'" class="btn btn-success">'.$this->listing->total_count($listdata->list_type).'</button>';
			
			
			$arr[] = array(
				"DT_RowId"		=>	$listdata->list_id.'_durar_lm',
                "‫نوع القائمة" 	=>	$typename['ar'],
				"إجمالي عدد" 	=>	$action);
				unset($actions);
		}
		
		echo json_encode($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function list_tadeel_qaima($type)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('6'));
		$result			=	$this->inq->get_list_taqareer_qaima($type);
		
		$checkbox	=	'---';
		
		foreach($result as $lc)
		{
			if($permissions[6]['a']	==	1)
			{
				if($lc->list_type != 'inquiry_type' AND $lc->list_type != 'rules' AND $lc->list_type != 'qualification' AND $lc->list_type != 'business_type' AND $lc->list_type != 'activity_project' AND $lc->list_type != 'project_employment' AND $lc->list_type != 'project_type')
				{
					if($lc->other)
					{
						$checked	=	'checked="checked"';
					}
					else
					{
						$checked	=	'';
					}
					
					$checkbox ='<div class="other1"> <input type="checkbox" onClick="other(this);" id="'.$lc->list_id.'" name="other'.$lc->list_id.'" '.$checked.'>
	
				  <div id="show'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;">&#10004;</div>
	
				  <div id="hide'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;"><i style="color:#CC0000;" class="icon-remove-sign"></i></div>
	
				  </div>';
				}
			}

			if($permissions[6]['a']	==	1)
			{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inquiries/add_child_qaima/'.$lc->list_id.'/" id="'.$lc->list_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> ';	}
							
			if($permissions[6]['u']	==	1)
			{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inquiries/add_data_for_qaima/'.$lc->list_id.'/parent" id="'.$lc->list_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';	}
			
			if($permissions[6]['d']	==	1)
			{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->list_id.'" data-url="'.base_url().'inquiries/delete/'.$lc->list_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
			
			
			$count = $this->tadeel_count($lc->list_id);
			$actions .=' <a href="'.base_url().'inquiries/child_listing/'.$lc->list_id.'">عرض الكل ('.$count.')</a>';
			 
			$arr[] = array(
				"DT_RowId"		=>	$lc->list_id.'_durar_lm',
                "قائمة البرامج" =>	$lc->list_name,              
				"أخرى" 			=>	$checkbox,
				"الإجراءات" 		=>	$actions);
				unset($actions);
		}
		
		echo json_encode($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
    public function tadeel_count($id)
	{	
		return $result	=	$this->inq->get_tadeel_count($id); // Get Total Count From Tadeel
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	    
	public function add_child_qaima($parentid)
	{
		
		$this->_data['type'] = 'child';
		$this->_data['parent_id'] = $parentid;
		
		$this->load->view('dialog/add-child-dialog',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function sub_child_listing($listid	=	NULL)
	{
		$this->_data['parent_id']	=	$listid;

		$this->_data['listing']	=	$this->listing->get_subchild_listing($listid);

		$this->load->view('subchilds-type-listing', $this->_data);

	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function add_data_for_qaima($parentid)
	{
		if($parentid!='' && $parentid!=0)
		{
			$this->_data['data'] = $this->listing->get_list_data($parentid);

		}
		
		$this->_data['type'] = 'parent';
		
		$this->load->view('dialog/add-child-dialog',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function rule_qualification_listing($type	=	NULL)
	{
		if($type)
		{
			$this->_data['type']	=	$type;

			// Get List Name By Type
			$this->_data['listing']	=	$this->listing->rules_qualification_by_type($type);

			$this->_data['list_type_name']	=	$type;

			$this->load->view('rule-qualification-type-listing', $this->_data);
		}
		else
		{
			$this->_data['marital_count']	=	$this->listing->total_count('maritalstatus');
			$this->_data['situation_count']	=	$this->listing->total_count('current_situation');
			$this->_data['inquiry_type']	=	$this->listing->total_count('inquiry_type');

			$this->load->view('rule-qualification-listing', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function taqareer_qaima()
	{
		$segment = $this->uri->segment(3);

		foreach($this->listing->get_list_type_rule_qualification() as $listdata) 
		{
			$typename = list_types($listdata->list_type);
			
			$action =' <a href="'.base_url().'inquiries/rule_qualification_listing/'.$listdata->list_type.'">عرض الكل</a> '; 
			
			$action .= ' <button onClick="gototype(this);" type="button" data-url="'.base_url().'inquiries/rule_qualification_listing/'.$listdata->list_type.'" class="btn btn-success">'.$this->listing->total_count($listdata->list_type).'</button>';
			
			
			$arr[] = array(
				"DT_RowId"=>$listdata->list_id.'_durar_lm',
                "‫نوع القائمة" =>$typename['ar'],
				/*"عرض الكل" =>$listdata->list_type,*/
				"إجمالي عدد" =>$action);
				unset($actions);
		}
		echo json_encode($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function list_taqareer_qaima($type)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('37'));
		$result			=	$this->inq->get_list_taqareer_qaima($type);
		
		$checkbox		=	'---';
		
		foreach($result	 as $lc)
		{
			if($permissions[37]['a']	==	1)
			{
				if($lc->list_type != 'inquiry_type' AND $lc->list_type != 'rules' AND $lc->list_type != 'business_type' AND $lc->list_type != 'activity_project' AND $lc->list_type != 'project_employment' AND $lc->list_type != 'project_type')
				{
					if($lc->other)
					{
						$checked	=	'checked="checked"';
					}
					else
					{
						$checked	=	'';
					}
				
					$checkbox ='<div class="other1"> <input type="checkbox" onClick="other(this);" id="'.$lc->list_id.'" name="other'.$lc->list_id.'" '.$checked.'>

				  <div id="show'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;">&#10004;</div>
	
				  <div id="hide'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;"><i style="color:#CC0000;" class="icon-remove-sign"></i></div>
	
				  </div>';
				}

			}
			
/*				if(check_other_permission(37,'a')==1)
			{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inquiries/add_child_qaima/'.$lc->list_id.'/" id="'.$lc->list_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> ';	}*/
							
			if($permissions[37]['u']	==	1)
			{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inquiries/add_data_for_qaima/'.$lc->list_id.'/parent" id="'.$lc->list_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';	}
			
			if($permissions[37]['d']	==	1)
			{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->list_id.'" data-url="'.base_url().'inquiries/delete/'.$lc->list_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
			
			if(function_exists('tadeel_count'))
			{
				$count = $this->tadeel_count($lc->list_id);
			}
			else
			{
				$count	=	'0';
			}
			
			 /*$actions .=' <a href="'.base_url().'inquiries/child_listing/'.$lc->list_id.'">عرض الكل ('.$count.')</a>'; */
			 $arr[] = array(
			"DT_RowId"=>$lc->list_id.'_durar_lm',
			"قائمة البرامج" =>$lc->list_name,              
			"أخرى" =>$checkbox,
			"الإجراءات" =>$actions);
			unset($actions);
		}
		
		$ex['data'] = $arr;
		//print_r($arr);
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function add_parent_taqareer($type	=	NULL)
	{
		if($type)
		{
			$this->_data['type'] = $this->listing->get_type_id($type);
			
			//$this->_data['single_list']	=	$this->listing->get_single_record($listid);	

		}
		else
		{
			//$this->_data['parent_id']	=	NULL;
		}
		
		$this->load->view('add-parent-taqareer',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function getList($type = 'sms')
	{
		$this->_data['type']		=	$type;
		$this->_data['module_id']	=	1;

		$this->_data['sms']	= $this->inq->getSms(1,$type);

		$this->load->view('listingsms', $this->_data);
		
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function getList_ajax($module_id,$type)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('89'));
		
		$this->db->where('sms_module_id',$module_id);
		$this->db->where('type',$type);
		
		if($module_id  == 1)
		{
			$this->db->join('register_auditors',"auditor_id=sms_receiver_id");
		}
		
		$this->db->order_by("sms_id", "DESC"); 
		$this->db->select('*');
		$query = $this->db->get('sms_history');
		
		$value	=	NULL;
		
		foreach($query->result() as $lc)
		{
			if($sm->sms_status == 1)
			{
				$class = "green_main_right_icon";
			}
			else
			{
				$class = "gray_main_right_icon";	
			}
					
			$concate_values	=	$lc->auditor_name .'|'. $lc->sms_receiver_number . '|'. $lc->sms_sent_date;
			
			if($lc->auditor_name)
			{
				$value	.=	'<li class="liinline">'.$lc->auditor_name.'</li>';
			}
			if($lc->sms_receiver_number)
			{
				$value	.=	'<li class="liinline">'.$lc->sms_receiver_number.'</li>';
			}
			if($lc->sms_sent_date)
			{
				$value	.=	'<li class="liinline">'.$lc->sms_sent_date.'</li>';
			}
							
			if($permissions[89]['d']	==	1)
			{	
				$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->sms_id.'" data-url="'.base_url().'inquiries/sms_delete/'.$lc->sms_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';
			}
			
			 $arr[] = array(
			"DT_RowId"=>$lc->sms_id.'_durar_lm',
			"DT_RowClass"=>'durar_right',
			"القائمة القصيرة‬‎" =>$value,
			"الإجراءات" =>$actions);
			unset($actions);
			unset($value);
		}
		
		echo json_encode($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function child_listing($listid	=	NULL)
	{
        //check_permission($this->_data['module'],'v');
		$this->_data['parent_id']	=	$listid;
		
		$this->_data['listing']	=	$this->listing->get_child_listing($listid);

		
		$this->load->view('child-type-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function add_parent($type	=	NULL)
	{
		if($type)
		{
			$this->_data['type'] = $this->listing->get_type_id($type);

		}
		else
		{
			//$this->_data['parent_id']	=	NULL;
		}
		
		$this->load->view('add-parent',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function add_child($parent_id)
	{
		if($parent_id)
		{
			$this->_data['parent_id']	=	$parent_id;
		}
		else
		{
			$this->_data['parent_id']	=	NULL;
		}
		
		$this->load->view('add-child',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function types_child_listing($listid	=	NULL)
	{
        check_permission($this->_data['module'],'v');
		$this->_data['listing']	=	$this->listing->get_child_listing($listid);
		$this->load->view('types-child-type-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function sub_child_listing1($listid	=	NULL)
	{
        check_permission($this->_data['module'],'v');
		$this->_data['listing']	=	$this->listing->get_subchild_listing($listid);
		$this->load->view('subchilds-type-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function types_sub_child_listing($listid	=	NULL)
	{
		$this->_data['listing']	=	$this->listing->get_subchild_listing($listid);
		$this->load->view('subchilds-type-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function add_new()
	{
		$parent_id	=	$this->input->post("parent_id");
		$add_sub	=	$this->input->post("add_sub");
		
		$data	=	array("list_parent_id"=>$parent_id,"list_name"=>$add_sub);
		
		$this->listing->add_list_child($data);		
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function other()
	{
		$list_id	=	$this->input->post("id");
		$entry		=	$this->input->post("entry");
		
		$data		=	array('other' => $entry);

		$this->listing->update_record($list_id, $data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function addrules($listid	=	NULL)
	{

		if($listid)
		{
			$this->_data['single_list']	=	$this->listing->get_single_record($listid);	
		}
		
		if($this->input->post())
		{
			$data		=	$this->input->post();
			
			// UNSET ARRAY key
			unset($data['submit']);
			
			if($this->input->post('list_id'))
			{
				$this->listing->update_list($this->input->post('list_id'),$data);
				
				$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				redirect(base_url()."inquiries/rules");
				exit();
			}
			else
			{
				$this->listing->add_list($data);
				
				$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				redirect(base_url()."inquiries/rules");
				exit();
			}
		}
		else
		{
			if($listid)
			{
				$this->_data['list_id']	=	$listid;
			}
			else
			{
				$this->_data['list_id']	=	'';
			}
            check_permission($this->_data['module'],'v');
			$this->load->view('inquiries/add-rule', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function rules()
	{
        check_permission($this->_data['module'],'v');
		$this->_data['listing']	=	$this->listing->get_rules();
		$this->load->view('rules-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function add_qualification($listid	=	NULL)
	{
		if($listid)
		{
			$this->_data['single_list']	=	$this->listing->get_single_record($listid);	
		}
		
		if($this->input->post())
		{
			$data		=	$this->input->post();
			
			// UNSET ARRAY key
			unset($data['submit']);
			
			if($this->input->post('list_id'))
			{
				$this->listing->update_list($this->input->post('list_id'),$data);
				
				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				}
				else
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
					redirect(base_url()."inquiries/qualification_listing");
					exit();
				}
			}
			else
			{
				$this->listing->add_list($data);
				
				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				}
				else
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
					redirect(base_url()."inquiries/qualification_listing");
					exit();
				}
			}
		}
		else
		{
			if($listid)
			{
				$this->_data['list_id']	=	$listid;
			}
			else
			{
				$this->_data['list_id']	=	'';
			}
            check_permission($this->_data['module'],'v');
			$this->load->view('inquiries/add-qualification', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Add Rule Form
	*/
	public function qualification_listing()
	{
		$this->_data['listing']	=	$this->listing->get_qualification();
		
		$this->load->view('qualification-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Add Rule Form
	*/
	public function add_rules_qualification($listid	=	NULL)
	{

		if($listid)
		{
			$this->_data['single_list']	=	$this->listing->get_single_record($listid);	
		}
		
		if($this->input->post())
		{
			$data		=	$this->input->post();
			
			// UNSET ARRAY key
			unset($data['submit']);
			
			if($this->input->post('list_id'))
			{
				$this->listing->update_list($this->input->post('list_id'),$data);
				
				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				}
				else
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
					redirect(base_url()."inquiries/rule_qualification_listing");
					exit();
				}
				

			}
			else
			{
				$this->listing->add_list($data);
				
				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				}
				else
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
					redirect(base_url()."inquiries/rule_qualification_listing");
					exit();
				}
			}
		}
		else
		{
			if($listid)
			{
				$this->_data['list_id']	=	$listid;
			}
			else
			{
				$this->_data['list_id']	=	'';
			}
			
			$this->load->view('inquiries/add-rules-qualification', $this->_data);
		}
	}	
//-------------------------------------------------------------------------------

	/*
	*
	* Add Rule Form
	*/
	public function rule_qualification_listing1($type	=	NULL)
	{
		if($type)
		{	
			// Get List Name By Type
			$this->_data['listing']			=	$this->listing->rules_qualification_by_type($type);
			$this->_data['list_type_name']	=	$type;

			$this->load->view('rule-qualification-type-listing', $this->_data);
		}
		else
		{
			$this->_data['listing']	=	$this->listing->get_rule_qualification();
			
			$this->load->view('rule-qualification-listing', $this->_data);
		}
			
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	
	public function delete_child($childlistid)
	{
		$this->listing->delete_child($childlistid);
		
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
		redirect(base_url().'listing_managment/listing/');
		exit();
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function delete_image()
	{
		$image = $this->input->post('image');
		$type = $this->input->post('type');
		
		if(isset($type) && $type !="")
		{
			echo $ret= $this->inq->delete_image($image,$type);
		}
		else
		{
			echo $ret= $this->inq->delete_image($image,'');
		}
  }
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	function deleteDoc()
	{
		$image = $this->input->post('image');
		
		echo $ret= $this->inq->delete_document($image);
	}
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	
	public function delete_banner($bannerid)
	{
		$this->inq->delete_banner($bannerid);
	}
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	public function delete_banner_ajax()
	{
		$image = $this->input->post('image');
		echo $this->inq->delete_banner_ajax($image);
	}	
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	public function delete($listid,$type)
	{
		$this->listing->delete($listid);
		
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
		redirect(base_url().'inquiries/listing/'.$type);
		exit();
	}
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	public function sms_delete($sms_id)
	{
		$this->listing->sms_delete($sms_id);
		
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
	}
	
//-------------------------------------------------------------------------------	
	public function get_list_data()
	{
		$list_id	=	$this->input->post('id');
		$data		=	$this->listing->get_list_data($list_id);
		echo  $data	=	json_encode(array('list_id'	=>	$data->list_id,'list_name'	=>	$data->list_name,'list_type'	=>	$data->list_type,'list_status'	=>	$data->list_status));
	}
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	public function transactionsprint()
	{
		$this->_data['all_applicatns']	=	$this->inq->get_all_applicatnts();
		
		$this->load->view('transactions-printlisting', $this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function transactions($branchid=0,$charity_type=0)
	{
		$this->_data['branchid'] 		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		
		//phpinfo();exit();	
		$this->load->view('transactions-listing', $this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function all_helps($charity_type,$id_card_number)
	{
		$this->_data['all_helps']	=	$this->inq->get_all_helps($charity_type,$id_card_number);
				
		$this->load->view('all-helps-by-card-number', $this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function applicants_mother_help($charity_type,$mother_id_number)
	{
		$this->_data['all_helps']	=	$this->inq->get_all_mother_helps($charity_type,$mother_id_number);
				
		$this->load->view('all-helps-by-card-number-for-mother', $this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function applicants_father_help($charity_type,$father_id_number)
	{
		$this->_data['all_helps']	=	$this->inq->get_all_father_helps($charity_type,$father_id_number);
				
		$this->load->view('all-helps-by-card-number-for-father', $this->_data);
	}	
	public function inquiry_notes($applicant_id)
	{
		$this->_data['applicant_id'] = $applicant_id;
		$this->load->view("inquiry_notes",$this->_data);
	}
	
	public function savemyprintdata()
	{
		$ah_applicant_notes['applicantid']	=	$this->input->post("applicantid");
		$ah_applicant_notes['userid']		=	$this->input->post("userid");
		$ah_applicant_notes['branchid']		=	$this->input->post("branchid");
		$ah_applicant_notes['notes'] 		=	$this->input->post("notes");
		$ah_applicant_notes['notestype']	=	'from_receipt';
		
		$this->inq->save_my_notes($ah_applicant_notes); // Add Notes into Database
	}
	
	public function savemynotes()
	{
		$ah_applicant_notes['applicantid']	=	$this->input->post("applicantid");
		$ah_applicant_notes['userid']		=	$this->input->post("userid");
		$ah_applicant_notes['branchid']		=	$this->input->post("branchid");
		$ah_applicant_notes['notes'] 		=	$this->input->post("notes");
		$ah_applicant_notes['notestype']	=	'from_notes';
		
		$this->inq->save_my_notes($ah_applicant_notes); // Add Notes into Database
				
		$myNotes	=	$this->inq->getNotes($this->input->post("applicantid"));
		$html		=	"";
		
		foreach($myNotes as $mn)
		{
			$html .= '<table width="100%" style="border:1px solid #000 !important; direction: rtl; margin-bottom:8px;" >';
			$html .= '<tr>';
			$html .= '<td style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>الموظف:</strong> '.$mn->fullname.'</td>';
			$html .= '<td  style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>فرع:</strong> '.$mn->branchname.'</td>';
			$html .= '<td  style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>تاريخ:</strong> '.arabic_date($mn->notesdate).'</td>';
			$html .= '</tr>';	
			$html .= '<tr>';
			$html .= '<td colspan="3" class="print_td">'.$mn->notes.'</td>';
			$html .= '</tr>';
			$html .= '</table>';
		}
		
		echo $html;
	}
	
	public function showallnotes()
	{
		$applicantid = $this->input->post("applicantid");
		$myNotes = $this->inq->getNotes($applicantid);
		$html = "";
		
		foreach($myNotes as $mn)
		{
			$html .= '<table width="100%" style="border:1px solid #000 !important; direction: rtl; margin-bottom:8px;" >';
			$html .= '<tr>';
			$html .= '<td style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>الموظف:</strong> '.$mn->fullname.'</td>';
			$html .= '<td  style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>فرع:</strong> '.$mn->branchname.'</td>';
			$html .= '<td  style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>تاريخ:</strong> '.arabic_date($mn->notesdate).'</td>';
			$html .= '</tr>';	
			$html .= '<tr>';
			$html .= '<td colspan="3" class="print_td">'.$mn->notes.'</td>';
			$html .= '</tr>';
			$html .= '</table>';
		}
		
		echo $html;
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function inquiries_list($branchid=0,$charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array(17));
		
		if($branchid!=0 && $branchid!='')
		{
			$branchid = $branchid;
		}
		else
		{
			$branchid = $this->haya_model->get_branch_id($branchid,$charity_type);
		}
		
		$result	=	$this->inq->get_inquiries_list($branchid,$charity_type);
		
		foreach($result as $lc)
		{
			$action   = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			$action  .= ' <a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore_print/'.$lc->applicantid.'/idcard_number"><i class="icon-print"></i></a>';
			//$action  .= ' <a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/inquiry_notes/'.$lc->applicantid.'"><i class="icon-book"></i></a>';
			$action  .= ' <a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			
			if($permissions[17]['u']	==	1) 
			{	
				$action .= ' <a class="iconspace" href="'.charity_edit_url($lc->charity_type_id).$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			if($permissions[17]['d']	==	1) 
			{
				//$action .= ' <a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			
			$steps	=	 config_item('steps');
			
			$arr[] = array(					
				'رقم المعاملة'		=>	arabic_date($lc->applicantcode),
				'الإسم'				=>	'<a class="iconspace" href="'.charity_edit_url($lc->charity_type_id).$lc->applicantid.'">'.$lc->fullname.'</a>',
				'نوع المساعدات'		=>	$lc->charity_type,
				'البطاقة الشخصة'	=>	arabic_date($lc->idcard_number),
				'المحافظة'			=>	$lc->province,
				'ولاية'				=>	$lc->wilaya,
				/*'تاريخ التسجيل'		=>	arabic_date($lc->registrationdate),*/
				'تاريخ التسجيل'		=>	date('Y-m-d',strtotime($lc->registrationdate)),  
				'المرحلة'			=>	$steps[$lc->step], 
				'كل الملاحظات'		=>	'<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/inquiry_notes/'.$lc->applicantid.'"><i class="icon-book"></i></a>'.' ('.$this->inq->getNotesCount($lc->applicantid).')',           
				'الإجرائات'			=>	$action);
							
				unset($action);
		}
					
		 checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socilservay_list($branchid=0,$charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('17'));
		$result			=	$this->inq->get_socilservay_list($branchid,$charity_type);
		
		foreach($result as $lc)
		{			
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			if($permissions[17]['u']	==	1) 
			{	$action .= '<a class="iconspace" href="'.base_url().'inquiries/socilservayresult/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
			
			if($permissions[17]['d']	==	1) 
			{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم"				=>	$lc->applicantcode,
				"الإسم"				=>	'<a class="iconspace" href="'.base_url().'inquiries/socilservayresult/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
				"نوع المساعدات"		=>	$lc->charity_type,
				"فرع"				=>	$lc->branchname,
				"البطاقة الشخصة"	=>	$lc->idcard_number,
				"المحافظة"			=>	$lc->province,
				"ولاية"				=>	$lc->wilaya,
				"تاريخ التسجيل"		=>	$lc->registrationdate,              
				"الإجرائات"			=>	$action);
				unset($action);
		}
		
		checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socilservay_list_completesteps($branchid=0,$charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('17'));
		$result			=	$this->inq->get_socilservay_list_completesteps($branchid,$charity_type);
		
		foreach($result as $lc)
		{			
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			//$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			if($permissions[17]['u']	==	1) 
			{
				//$action .= '<a class="iconspace" href="'.base_url().'inquiries/socilservayresult/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			if($permissions[17]['d']	==	1) 
			{
				//$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم"				=>	$lc->applicantcode,
				"الإسم"				=>	$lc->fullname,
				"نوع المساعدات"		=>	$lc->charity_type,
				"فرع"				=>	$lc->branchname,
				"البطاقة الشخصة"	=>	$lc->idcard_number,
				"المحافظة"			=>	$lc->province,
				"ولاية"				=>	$lc->wilaya,
				"تاريخ التسجيل"		=>	$lc->registrationdate,              
				"الإجرائات"			=>	$action);
				unset($action);
		}
		
		checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socilservay_list_step($branchid,$charity_type=0,$step)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('206'));
		
		/*$result	=	$this->inq->get_socilservay_list_step($branchid,$charity_type);*/ // Before
		$result	=	$this->inq->get_socilservay_list_special_slt($branchid,$charity_type,$step); // After changing the function for second step
		
		foreach($result as $lc)
		{
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';

			if(!isset($step) OR $lc->application_status	!= 'إعادة نظر')
			{
				$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			}	
			if($permissions[17]['u']	==	1) 
			{
				if(!isset($step) OR $lc->application_status	!= 'إعادة نظر')
				{
					$action .= '<a class="iconspace" href="'.base_url().'inquiries/socilservayresult/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				}
			}
			
			//if($permissions[17]['d']	==	1) 
			//{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
			
			if(!isset($step) OR $lc->application_status	!= 'إعادة نظر')
			{
				$name	=	'<a class="iconspace" href="'.base_url().'inquiries/socilservayresult/'.$lc->applicantid.'">'.$lc->fullname.'</a>';
			}
			else
			{
				$name	=	$lc->fullname;
			}
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم" 				=>	$lc->applicantcode,
				"الإسم" 				=>	$name,
				"نوع المساعدات" 	=>	$lc->charity_type,
				"فرع" 				=>	$lc->branchname,
				"البطاقة الشخصة" 	=>	$lc->idcard_number,
				"المحافظة" 			=>	$lc->province,
				"ولاية" 				=>	$lc->wilaya,
				"تاريخ التسجيل" 	=>	$lc->registrationdate,              
				"الإجرائات" 			=>	$action);
				unset($action);
		}
		
		checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function finaldecission_list($branchid=0,$charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('145'));
			
		$result	=	$this->inq->get_finaldecission_list($branchid,$charity_type); // Get finaldecession List 
		
		foreach($result as $lc)
		{				
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			
			if($permissions[145]['u']==1) 
			{	$action .= '<a class="iconspace" href="'.base_url().'inquiries/finaldecission_edit/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
			
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم"				=>	arabic_date($lc->applicantcode),
				"الإسم"				=>	'<a class="iconspace" href="'.base_url().'inquiries/finaldecission_edit/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
				"نوع المساعدات"		=>	$lc->charity_type,
				"فرع"				=>	$lc->branchname,
				"البطاقة الشخصة"	=>	arabic_date($lc->idcard_number),
				"المحافظة"			=>	$lc->province,
				"ولاية"				=>	$lc->wilaya,
				"الاجتماع رقم"		=>	$lc->meeting_number,
				"تاريخ التسجيل"		=>	arabic_date($lc->registrationdate),              
				"الإجرائات"			=>	$action);
				
				unset($action);
		}
		
		checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socilservay_list_special($branchid=0,$charity_type=0,$step	=	NULL)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('66'));
		
		$socilservay	=	$this->inq->get_socilservay_list_special($branchid,$charity_type,$step);
		
		//echo '<pre>'; print_r($socilservay);exit();
					
			foreach($socilservay as $lc)
			{
				
				$action 	= '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
				//$action 	.= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
				if($permissions[66]['u']	==	1) 
				{
					if(!isset($step))
					{
						$action .= '<a class="iconspace" href="'.base_url().'inquiries/socialnoneditable/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	
					}
				}
				
				if($permissions[66]['d']==1) 
				{
					if(!isset($step))
					{
						$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
					}
				}
				
				if(!isset($step))
				{
					$name	=	'<a class="iconspace" href="'.base_url().'inquiries/socialnoneditable/'.$lc->applicantid.'">'.$lc->fullname.'</a>';
				}
				else
				{
					$name	=	$lc->fullname;
				}
				
				$arr[] = array(
					"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
					"رقم" 				=>	arabic_date($lc->applicantcode),
					"الإسم" 				=>	$name,
					"نوع المساعدات" 	=>	$lc->charity_type,
					"فرع" 				=>	$lc->branchname,
					"البطاقة الشخصة" 	=>	arabic_date($lc->idcard_number),
					"المحافظة" 			=>	$lc->province,
					"ولاية" 				=>	$lc->wilaya,
					"عاجل" 				=>	$lc->ajel,
					"تاريخ التسجيل"		=>	arabic_date($lc->registrationdate),              
					"الإجرائات" 			=>	$action);
					
					unset($action);
			}
			
			checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socilservay_list_special_slt($charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('66'));
		$socilservay	=	$this->inq->get_socilservay_list_special_slt($charity_type);
								
			foreach($socilservay as $lc)
			{
				
				$action 	= '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
				$action 	.= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
				
				if($permissions[66]['u']	==	1) 
				{
					$action .= '<a class="iconspace" href="'.base_url().'inquiries/socialnoneditable/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
				
				//if($permissions[66]['d']==1) 
				//{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
				
				$arr[] = array(
					"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
					"رقم" 				=>	arabic_date($lc->applicantcode),
					"الإسم" 				=>	'<a class="iconspace" href="'.base_url().'inquiries/socialnoneditable/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
					"نوع المساعدات" 	=>	$lc->charity_type,
					"فرع" 				=>	$lc->branchname,
					"البطاقة الشخصة" 	=>	arabic_date($lc->idcard_number),
					"المحافظة" 			=>	$lc->province,
					"ولاية" 				=>	$lc->wilaya,
					"عاجل" 				=>	$lc->ajel,
					"تاريخ التسجيل"		=>	arabic_date($lc->registrationdate),              
					"الإجرائات" 			=>	$action);
					
					unset($action);
			}
			
			checkArraySize($arr);
	}	
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function ssr_rejected($charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('66'));
		$socilservay	=	$this->inq->get_ssr_rejected($charity_type);
						
		foreach($socilservay as $lc)
		{
			$action 	= '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			$action 	.= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			if($permissions[66]['u']	==	1) 
			{
				$action .= '<a class="iconspace" href="'.base_url().'inquiries/socialnoneditable/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
			
			//if($permissions[66]['d']==1) 
			//{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم" 				=>	arabic_date($lc->applicantcode),
				"الإسم" 				=>	'<a class="iconspace" href="'.base_url().'inquiries/socialnoneditable/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
				"نوع المساعدات" 	=>	$lc->charity_type,
				"فرع" 				=>	$lc->branchname,
				"البطاقة الشخصة" 	=>	arabic_date($lc->idcard_number),
				"المحافظة" 			=>	$lc->province,
				"ولاية" 				=>	$lc->wilaya,
				"عاجل" 				=>	$lc->ajel,
				"تاريخ التسجيل"		=>	arabic_date($lc->registrationdate),              
				"الإجرائات" 			=>	$action);
				
				unset($action);
		}
		
		checkArraySize($arr);
	}	
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function rejected_applicants()
	{
        check_permission($this->_data['module'],'v');
		
		$this->_data['all_applicatns']	=	$this->inq->get_all_reject_bank();
		
		$this->load->view('rejected-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function comitte_descions()
	{
        check_permission($this->_data['module'],'v');
		
		$this->_data['all_descisions']	=	$this->inq->get_all_decisions();
		
		$this->load->view('transactions-listing', $this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function get_applicant_data($applicantid	=	NULL)
	{
		
		$this->_data['applicatn_info']	=	$this->inq->get_single_applicatnt($applicantid);
		$this->_data['udetail'] 		=	$this->inq->getRequestInfo($applicantid);
		
		echo $html = $this->load->view('ajax-response-html', $this->_data,TRUE);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	function getHistory($applicantid	=	NULL){
				
		$this->load->model('ajax/ajax_model', 'ajax');
			
	    $this->_data['history'] =	$this->ajax->getHistory($applicantid);
		$this->_data['tempid']	=	$applicantid;
		
		echo $HTML	=	$this->load->view('ajax-history-response-html', $this->_data,TRUE);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function get_auditor_data($auditorid	=	NULL)
	{
		$this->_data['auditor_info']	=	$this->inq->get_single_auditor($auditorid);
		$this->_data['main_info']	=	$this->inq->get_main_info($auditorid);
		
		echo $HTML	=	$this->load->view('ajax-inquiries-response-html', $this->_data,TRUE);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function inqprint()
	{
		$inqdata = $this->input->post('inquery_id');
		$this->_data['auditor_info']	=	$this->inq->get_single_auditor($inqdata);
		$this->_data['main_info'] 		=	$this->inq->get_main_info($inqdata);
		$this->load->view('print_inquery',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function addbanners($banner_id	=	NULL)
	{
		if($banner_id)
		{
			$this->_data['banner_detail']	=	$this->haya_model->get_banner_detail($banner_id);
		}

		$this->load->view('add-banner',$this->_data);	// Add Banner Form
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function finaldecission_edit($applicantid)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		if($this->input->post())
		{
			$applicantid 		= $this->input->post('applicantid');
			$application_status = $this->input->post('application_status');
			
			if($application_status	==	"إعادة نظر")
			{
				$apStatus 				=	2;
				$case_close 			=	0;
				$ah_applicant['isOld'] 	=	1;
			}
			else
			{
				$apStatus 		=	4;
				$case_close 	=	1;
			}
			
			if($application_status)
			{
				
				
				if($applicantid	!=	'')
				{
					$ah_applicant['application_status'] =	$application_status;
					$ah_applicant['applicant_notes'] 	=	$this->input->post('notes');
					$ah_applicant['case_close'] 		=	$case_close;
                    $ah_applicant['meeting_number'] 	=	$this->input->post('meeting_number');
                    $ah_applicant['meeting_notes'] 		=	$this->input->post('meeting_notes');
					

					$this->inq->update_final_decission($applicantid,$ah_applicant); // UPDATE Final dession
					
					//Adding data into dicission table
					$ah_applicant_decission['applicantid'] 		=	$applicantid;
					$ah_applicant_decission['userid'] 			=	$this->_login_userid;
					$ah_applicant_decission['decission'] 		=	$application_status;
					$ah_applicant_decission['decission_amount'] =	$this->input->post('decission_amount');
					$ah_applicant_decission['meeting_number'] 	=	$this->input->post('meeting_number');
					$ah_applicant_decission['notes'] 			=	$this->input->post('notes');
					$ah_applicant_decission['decissionip'] 		=	$_SERVER['REMOTE_ADDR'];
					$ah_applicant_decission['meeting_notes'] 	=	$this->input->post('meeting_notes');
					
					if($_FILES["decission_doc"]["tmp_name"])
					{
						$document			=	$this->upload_file($this->_login_userid,'decission_doc','resources/applicants'); // Uplaod Decision Document
						$ah_applicant_decission['decission_doc']	=	$document;
					}
					
					$this->inq->add_final_decission($ah_applicant_decission); // ADD Final dession
					
					$this->haya_model->save_steps($applicantid,$apStatus); // ADD step
					$this->haya_model->update_steps($applicantid,$apStatus); // Update Step
					
					redirect(base_url().'inquiries/finaldecission_by_types');
					exit();
				}
			}
		}
				
		$this->_data['_applicantid']	= $applicantid;			
		$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($applicantid);
				
		$this->load->view('finaldecission_edit', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/    
    public function checkAmount()
    {
       $application_status 	=	$this->input->post('application_status');
       $totalfamilymember 	=	$this->input->post('totalfamilymember');
       $loanamount 			=	$this->input->post('loanamount');
       $familysalary 		=	$this->input->post('familysalary');
       
	   $result 				=	$this->inq->get_total_family_members($totalfamilymember,$familysalary); // Get Data 
        
       $html  = '<table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">';
       $html .= '<thead>';
       $html .= '<tr role="row">';
       $html .= '<th style="text-align:center;">عدد افراد الاسرة</th>';
       $html .= '<th style="text-align:center;">الفئة المستحقة</th>';
       $html .= '<th style="text-align:center;">المبلغ</th>';
       $html .= '<th style="text-align:center;">صافي الدخل</th>';
      
       $html .= '</tr>';
       $html .= '</thead>';
       $html .= '<tbody role="alert" aria-live="polite" aria-relevant="all">';
	   
       foreach($result as $lc)
       {
        	if($lc->people_start >= 0 && $lc->people_end<=1)
			{	
				$text = "فرد واحد";	
			}
			else if($lc->people_start >= 2 && $lc->people_end<=3)
			{
				$text = "فردين او ".arabic_date(3)." افراد";
			}
			else
			{	
				$text  = arabic_date($lc->people_start)." او ".arabic_date($lc->people_end)." افراد";
			}
				$html .= '<tr role="row">';
				$html .= '<td style="text-align:center;">'.$text.'</td>';
				$html .= '<td style="text-align:center;">'.number_drop_box_arabic('',$lc->priority,1).'</td>';
				$html .= '<td style="text-align:center;"><strong style="font-size: 14px; color: #F00;">'.arabic_date($lc->amount).'</strong></td>';
				$html .= '<td style="text-align:center;">'.arabic_date($lc->netincome).' ريال فما دون'.'</td>';
				$html .= '</tr>';
       }
	   
       $html .= '</tbody>';
       $html .= '</table>';
	   
	   $userTable['html_table'] =	$html;
	   $userTable['amount'] 	=	$lc->amount;
	   
       echo json_encode($userTable);	//	RETURN as JSON encode
    }
//-------------------------------------------------------------------------------

	/*
	* 
	*/    
    public function rejected($branchid=0,$charity_type=0)
    {
        $this->_data['branchid'] 		=	$branchid;
        $this->_data['charity_type'] 	=	$charity_type;
		
        $this->load->view('reject_list', $this->_data); 
    }
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
 	public function rejected_list($branchid=0,$charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('149'));
		
		$rejected_list	=	$this->inq->rejected_list($branchid,$charity_type); // Get Rejected List
	
		foreach($rejected_list as $lc)
		{
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			
			if($permissions[149]['u']	==	1) 
			{
				$action .= '<a class="iconspace" href="'.base_url().'inquiries/finaldecission_edit/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
			
			//if($permissions[149]['d']	==	1) 
			//{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
			
			$arr[] = array(
				"DT_RowId"				=>	$lc->applicantid.'_durar_lm',
				"رقم" 					=>	arabic_date($lc->applicantcode),
				"الإسم" 					=>	'<a class="iconspace" href="'.base_url().'inquiries/finaldecission_edit/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
				"نوع المساعدات" 		=>	$lc->charity_type,
				"فرع" 					=>	$lc->branchname,
				"عرض في الاجتماع رقم" 	=>	'<a href="#" onclick="alatadad(this);" data-url="'.base_url().'inquiries/get_meeting_number/'.$lc->applicantid.'">'.arabic_date($lc->meeting_number).'</a>',
				"المحافظة" 				=>	$lc->province,
				"ولاية" 					=>	$lc->wilaya,
				"تاريخ التسجيل" 		=>	arabic_date($lc->registrationdate),              
				"الإجرائات" 				=>	$action);
				unset($action);
		}
				
		checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/    
    public function get_meeting_number($applicantid)
    {
        $this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($applicantid);
        $this->load->view('meetingdata',$this->_data); 
    } 
//-------------------------------------------------------------------------------
	public function checkAllMessageForCurrent()
	{
		
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function givemoney()
	{
		$this->load->view('givemoney_view',$this->_data);
	}
// -------------------------------------------------------------------------------

	/*
	* 
	*/
	public function givemoney_list($branchid=0,$charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('145'));
		$result			=	$this->inq->get_money_last($branchid,$charity_type); // Give Money list
		
		foreach($result as $lc)
		{				
			$action  = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			
			if($permissions[145]['u']==1) 
			{
				$action .= '<a class="iconspace" href="'.base_url().'inquiries/finaldecission_edit/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
			
				$decission = $this->getLastDecission($lc->applicantid);
			
				$arr[] = array(
					"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
					"رقم" 				=>	arabic_date($lc->applicantcode),
					"الإسم" 				=>	'<a class="iconspace" href="'.base_url().'inquiries/finaldecission_edit/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
					"نوع المساعدات" 	=>	$lc->charity_type,
					"فرع" 				=>	$lc->branchname,
					"البطاقة الشخصة" 	=>	arabic_date($lc->idcard_number),					
					"القيمة" 			=>	arabic_date($decission->decission_amount),
					"تاريخ اللجنة" 		=>	arabic_date($decission->decissiontime),              
					"الإجرائات" 			=>	$action);
					
				unset($action);
		}
		
		checkArraySize($arr);
	}

// -------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function getLastDecission($applicant_id)
	{
		return $this->inq->get_last_decission($applicant_id);
	}
// --------------------------------------------------------------------------------

	/*
	* Upload Files First Visit
	* @param $applicant_id 
	*
	*/
	public function uploads_sakaniya_visits($applicant_id)
	{
		$path	=	'resources/users'; // Define Path where the files Upload
		
		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data =	$this->upload->data();
			$name		=	$image_data['file_name'];
			$data		=	array(
								'applicantid'	=>	$applicant_id,
								'userid'		=>	$this->_login_userid,
								'imagename'		=>	$name);
			
			$this->inq->save_first_ziyarah($data);	// Insert Data of First Visit	
			echo json_encode(array('status' => 'ok'));	// Response
		}
	}
// -----------------------------------------------------------------------

	/*
	*	File Uploading
	*	@param $userid integer
	*	@param $filefield integer Input File name
	*	@param $folder integer older name where Image Upload
	*	@param $width integer
	*	@param $height integer
	*/
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
				
		if(!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();		
			return $image_data['file_name'];
		}
	}
// ------------------------------------------------------------------------

  	/**
   	*	Dynamic Forms Listing Page
   	*	@param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]	=	$this->haya_model->get_all_custom_form($moduleid);
		$this->_data["userid"]	=	$this->_login_userid;
		
		$this->_data['formid']	=	$moduleid;
		 
		// Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);
	 }
//-----------------------------------------------------------------------
	/*
	* This Method copied Yateem Controller
	* Yateem Listing Page
	*/
	function yateem_list()
	{
		$this->load->view('yatem-listing',$this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	* This Method copied Yateem Controller
	* Yateem Listing Page
	*/	
	public function all_yateem_list($id	=	NULL,$type	=		NULL,$status	=	NULL)
	{
		$this->load->model('yateem/yateem_model','yateem'); // Load Yateem Model
		
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		
		if($id)
		{
			$all_users	=	$this->yateem->get_yateeem_list($id,$type,'1');
		}
		else
		{
			$all_users	=	$this->yateem->get_yateeem_list_new($status,'1');
		}

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				$action  = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				$action .= '<a class="iconspace" href="'.base_url().'yateem/add_yateem/'.$lc->orphan_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
/*				$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_yateeem/'.$lc->orphan_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
*/				$url 	= '<a class="iconspace" href="'.base_url().'inquiries/yateem_servay/'.$lc->orphan_id.'">'.is_set($lc->orphan_name).'</a>';

				$img = '<img src="'.base_url().'">';
				$arr[] = array(
					"DT_RowId"			=>	$lc->orphan_id.'_durar_lm',
					"رقم اليتيم"		=>	is_set(arabic_date($lc->orphan_id)),
					"اسم اليتيم" 		=>	$url,
					"الجنسية" 			=>	is_set($this->haya_model->get_name_from_list($lc->orphan_nationalty)),
					"تاريخ الميلاد" 		=>	is_set(arabic_date($lc->date_birth)),
					"الدولة" 			=>	is_set($this->haya_model->get_name_from_list($lc->country_id)),
					"المدينة" 			=>	is_set($this->haya_model->get_name_from_list($lc->city_id)),
					"تاريخ تقديم الطلب" =>	date('Y-m-d',strtotime($lc->created)),
					"الإجرائات" 			=>	$action);
					
					unset($action);
			}
			
			$ex['data'] = $arr;
						
			echo json_encode($ex);			
		}
	}
//--------------------------------------------------------------------------------//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function yateem_servay($orphan_id)
	{
		$this->load->model('yateem/yateem_model','yateem'); // Load Yateem Model
		
		//$this->haya_model->check_permission($this->_data['module'],'v');
		if(isset($orphan_id) && $orphan_id!="")
		{
			$this->_data['yateem_data'] 		=	$this->yateem->getYateemDataById($orphan_id);
			$this->_data['yateem_sisters'] 		=	$this->yateem->getBroSisNames($orphan_id,'sister');
			$this->_data['yateem_brothers'] 	=	$this->yateem->getBroSisNames($orphan_id,'brother');
			$this->_data['yateem_father_data'] 	=	$this->yateem->get_father_data($orphan_id);
			$this->_data['yateem_mother_data'] 	=	$this->yateem->get_mother_data($orphan_id);
			$this->_data['yateem_banks_data'] 	=	$this->yateem->getBanksData($orphan_id);
			$this->_data['yateem_others_data'] 	=	$this->yateem->getOthersData($orphan_id);
			$this->_data['yateem_docs'] 		=	$this->yateem->getYateemDocument($orphan_id);
			
			$this->_data['yateem_edu_data'] 	=	$this->yateem->get_yateem_others_data('ah_yateem_education',$orphan_id);
			$this->_data['yateem_can_data'] 	=	$this->yateem->get_yateem_others_data('ah_yateem_candidate',$orphan_id);
			
			$this->_data['orphan_id']			=	$orphan_id;
		}
		
		$this->load->view('yateem-step-2', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* functon Adding Social Servay Result
	*/	
	public function add_yateem_servay()
	{
		//echo '<pre>';print_r($this->input->post());exit();
		$orphan_id 					=	$this->input->post('orphan_id');
		$userid 					=	$this->_login_userid;
		$health_condition 			=	$this->input->post('health_condition');
		$health_condition_text 		=	$this->input->post('health_condition_text');
		$housing_condition 			=	$this->input->post('housing_condition');
		$housing_condition_text 	=	$this->input->post('housing_condition_text');
		$numberofpeople 			=	$this->input->post('numberofpeople');
		$positioninfamily 			=	$this->input->post('positioninfamily');
		$economic_condition 		=	$this->input->post('economic_condition');
		$economic_condition_text 	=	$this->input->post('economic_condition_text');
		$casetype 					=	$this->input->post('casetype');
		$aps_name 					=	$this->input->post('aps_name');
		$aps_account 				=	$this->input->post('aps_account');
		$aps_filename 				=	$this->input->post('aps_filename');
		$aps_category 				=	$this->input->post('aps_category');
		$aps_category_text 			=	$this->input->post('aps_category_text');
		$aps_salaryamount 			=	$this->input->post('aps_salaryamount');
		$aps_date 					=	$this->input->post('aps_date');
		$aps_another_income 		=	json_encode($this->input->post('aps_another_income'));
		$whyyouwant 				=	$this->input->post('whyyouwant');
		$summary 					=	$this->input->post('summary');
		$review 					=	$this->input->post('review');
		$ajel 						=	$this->input->post('ajel');
		$maintenance_type 			=	$this->input->post('maintenance_type');
		$report_visit 				=	$this->input->post('report_visit');
		$certificate_date 			=	$this->input->post('certificate_date');
		$proposal_section 			=	$this->input->post('proposal_section');
		$step 						=	$this->input->post('step');
		
		
		$ah_applicant_survayresult = array(
			'orphan_id'					=>	$orphan_id,
			'userid'					=>	$userid,
			'health_condition'			=>	$health_condition,
			'health_condition_text'		=>	$health_condition_text,
			'housing_condition'			=>	$housing_condition,
			'housing_condition_text'	=>	$housing_condition_text,
			'numberofpeople'			=>	$numberofpeople,
			'positioninfamily'			=>	$positioninfamily,
			'economic_condition'		=>	$economic_condition,
			'economic_condition_text'	=>	$economic_condition_text,
			'casetype'					=>	$casetype,
			'aps_name'					=>	$aps_name,
			'aps_account'				=>	$aps_account,
			'aps_filename'				=>	$aps_filename,
			'aps_category'				=>	$aps_category,
			'aps_category_text'			=>	$aps_category_text,
			'aps_salaryamount'			=>	$aps_salaryamount,
			'aps_date'					=>	$aps_date,
			'aps_another_income'		=>	$aps_another_income,
			'whyyouwant'				=>	$whyyouwant,
			'summary'					=>	$summary,
			'ajel'						=>	$ajel,
			'maintenance_type'			=>	$maintenance_type,
			'report_visit'				=>	$report_visit,
			'proposal_section'			=>	$proposal_section,
			'certificate_date'			=>	$certificate_date,
			'review'					=>	$review);
		
		$this->inq->add_yateem_servay($ah_applicant_survayresult);
		$this->inq->update_yateem_step($orphan_id,$step);
		
		
		redirect(base_url().'yateem/yateem_list/');
		exit();
	}
//---------------------------------------------------------------------
}
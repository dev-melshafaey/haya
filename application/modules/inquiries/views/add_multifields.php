<div id="block_<?php echo $key; ?>" class="col-md-12 multiple-block" style="padding:0px !important;"> <a href="#" style="float:left;color:#F00;" onclick="removethispanel('<?PHP echo($key); ?>');"><i class="icon-remove-sign"></i></a> <br clear="all" />
  <div class="col-md-4 form-group">
    <label class="text-warning">تدفع بعد إنجاز نصــف الأعـمـــــال المطلوبة :</label>
    <input name="percentage[<?php echo $total_divs;?>]"  placeholder="تدفع بعد إنجاز نصــف الأعـمـــــال المطلوبة" id="percentage<?php echo $total_divs;?>" type="text" class="form-control red" onKeyUp="only_numeric(this);">
  </div>
  <div class="col-md-4 form-group">
    <label class="text-warning">كمية  :</label>
    <input name="amount[<?php echo $total_divs;?>]"  placeholder="كمية" id="amount<?php echo $total_divs;?>" type="text" class="form-control red" onKeyUp="only_numeric(this);" readonly>
  </div>
  <div class="col-md-4 form-group">
    <label class="text-warning">ملاحظات  :</label>
    <input name="notes[<?php echo $total_divs;?>]"  placeholder="ملاحظات" id="notes" type="text" class="form-control req">
  </div>
</div>
<hr>
<script>
 $(document).ready(function(){
/*********************************************************************/
	$( "#percentage<?php echo $total_divs;?>" ).blur(function() {
	var field_val			=	(<?php echo $total_divs;?>	-	1)
	var percentage			=	$(this).val();
	//var total_amount		=	$("#amount"+field_val).val();
	var total_amount		=	$("#prjtotal5").val();
	//console.log(total_amount);
	//return;
	var percentage_amount	=	(Number(percentage) / 100) * Number(total_amount);

	$("#amount<?php echo $total_divs;?>").val(percentage_amount);
});		
/*********************************************************************/
});
</script>
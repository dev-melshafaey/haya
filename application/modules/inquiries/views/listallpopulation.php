<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<?php $segment = $this->uri->segment(3); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
              <h4>ثالثا: قسم المساعدات الأسكانية ( تفاصيل القوائم)</h4>
                <p>
                <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid">

                  <div class="row table-header-row"></div>

                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center;">الاسم</th>
                        <th style="text-align:center;">رقم الاجتماع</th>
                        <th style="text-align:center;">الولاية</th>
                        <th style="text-align:center;">الرقم المدني</th>
                        <th style="text-align:center;">نوع الصيانة</th>
                        <th style="text-align:center;">قرار اللجنة</th>
                        <th style="text-align:center;">قيمة المساعدة</th>
                        <th style="text-align:center;">رقم الهاتف</th>
                         <th style="text-align:center;">الإجرائات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'inquiries/listallpopulation/'.$branchid.'/'.$charity_type.'/'.$listid.'/list','columns_array'=>'
{ "data": "الاسم" },
{"data":"رقم الاجتماع"},
{"data":"الولاية"},
{ "data": "الرقم المدني" },
{"data":"نوع الصيانة"},
{"data":"قرار اللجنة"},
{"data":"قيمة المساعدة"},
{"data":"رقم الهاتف"},
{"data":"الإجرائات"}
')); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
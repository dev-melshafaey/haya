<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<?php $segment = $this->uri->segment(3); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <p>
                <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid">

                  <div class="row table-header-row">
                  <a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>inquiries/addrenewdata/<?php echo $applicantid;?>/<?php echo $appRewMid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">إضافة</a>

                  <table class="table table-bordered table-striped ">
                    <thead>
                      <tr role="row" style="background-color: #029625 !important; color:#fff;">
                       <th style="text-align:center;">تاريخ التجديد</th>
                        <th style="text-align:center;">السنة الدراسية</th>
                        <th style="text-align:center;">اخر معدل تراكمي</th>
                        <th style="text-align:center;">ملاحظات</th>
                         <th style="text-align:center;">توقع التخرج</th>
                        <th style="text-align:center;">الدرجة العلمية</th>
                        <th style="text-align:center;">الانذارات</th>
                        <th style="text-align:center;">المرفق</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php
					if(count($notes)>0){
						foreach($notes as $rows){?>
                        	 <tr role="row">
                               <td style="text-align:center;"><?php echo  $rows->currentdate ?></td>
                                <td style="text-align:center;"><?php echo  $rows->years ?></td>
                                <td style="text-align:center;"><?php echo  $rows->cumulative ?></td>
                                <td style="text-align:center;"><?php echo  $rows->notes ?></td>
                                <td style="text-align:center;"><?php echo  $rows->degree ?></td>
                                <td style="text-align:center;"><?php echo  $rows->degree_num ?></td>
                                <td style="text-align:center;"><?php echo  $rows->notes2 ?></td>
                                <td style="text-align:center;"><?php if($rows->attachment1 !=""){echo  $controller->getfileicon($rows->attachment1,base_url().'resources/inquiries/'.$login_userid,'ارفاق كشف الدرجات');}
								echo '<br>'; 
								if($rows->attachment2 !=""){ echo  $controller->getfileicon($rows->attachment2,base_url().'resources/inquiries/'.$login_userid,'ارفاق الخطة الدراسية');} ?></td>
                              </tr>
                        <?php
						}
					}
					
					?>
                    </tbody>
                  </table>
                   </div>
               
              </div> 
              <div style="margin-bottom:20px;"></div>
              <div style="border: 2px solid #029625; padding:20px;">
                   <form action="" method="POST" id="frm_expence" name="frm_expence" action="<?php echo base_url()?>aid/afteraddagreement" enctype="multipart/form-data" >
                   <input type="hidden" name="applicantid" id="applicantid" value="<?PHP echo $applicantid; ?>">
		<input type="hidden" name="userid" id="userid" value="<?PHP echo $user_detail['profile']->userid; ?>">
        <input type="hidden" name="appRewMid" id="appRewMid" value="<?PHP echo $appRewMid; ?>">
                   
           
                    <div class="row"> 
                     <div class="col-md-2 form-group">
                <label class="text-warning">نغير التخصص</label>
                            <input type="radio" <?php if(isset($main->conditions)){ if($main->conditions =="1"){echo "checked";}}else{echo "checked";}?>  name="chkoption" data-id="chkoption" id="chkoption_1" class="apstatus" value="1" tabindex="2" onclick="validate(this.value);">
                </div>
                </div>
                <div class="row" id="Option1">
                 <div class="form-group col-md-6">
                      <label for="basic-input">المرفق</label>
                      <input type="file" class="form-control " value="" placeholder="المرفق" name="attachment1[]" id="attachment1"  multiple="multiple"/>
                       <input type="hidden"  name="attachment1_old" id="attachment1_old" value=""  />
                       <br/>
                       <?php if($main->condition_attachment !="")echo $controller->getfileicon($main->condition_attachment,base_url().'resources/inquiries/'.$login_userid);?>
                   
                    </div>
                     <div class="form-group col-md-6">
      <label for="basic-input">السبب</label>
      <textarea  class="form-control"  placeholder="السبب" name="reason1" id="reason1"   ><?php echo $main->condition_text?></textarea>
    </div>
                </div>
                <div class="row">
                <div class="col-md-2 form-group">
                <label class="text-warning">طلب تغير التخصص</label>
                            <input type="radio"  name="chkoption" <?php if($main->conditions =="2"){echo "checked";}?>  data-id="chkoption" id="chkoption_2" class="apstatus" value="2" tabindex="2" onclick="validate(this.value);">
                </div>
                </div>
                <div class="row" id="Option2" style="display:none;">
                 <div class="form-group col-md-6" >
                      <label for="basic-input">المرفق</label>
                      <input type="file" class="form-control " value="" placeholder="المرفق" name="attachment2[]" id="attachment2"  multiple="multiple"/>
                       <input type="hidden"  name="attachment2_old" id="attachment2_old" value=""  />
                   
                    </div>
                     <div class="form-group col-md-6">
      <label for="basic-input">السبب</label>
      <textarea  class="form-control"  placeholder="السبب" name="reason2" id="reason2"    ><?php echo $main->condition_text?></textarea>
    </div>
                </div>
                
                <div class="row">
                 <div class="col-md-2 form-group">
                <label class="text-warning">طلب تأجيل</label>
                            <input type="radio"  name="chkoption" <?php if($main->conditions =="3"){echo "checked";}?>  data-id="chkoption" id="chkoption_3" class="apstatus" value="3" tabindex="2" onclick="validate(this.value);">
                </div>
                </div>
                <div class="row" id="Option3" style="display:none;">
                 <div class="form-group col-md-6">
                      <label for="basic-input">المرفق</label>
                      <input type="file" class="form-control " value="" placeholder="المرفق" name="attachment3[]" id="attachment3"  multiple="multiple"/>
                       <input type="hidden"  name="attachment3_old" id="attachment3_old" value=""  />
                   
                    </div>
                     <div class="form-group col-md-6">
      <label for="basic-input">السبب</label>
      <textarea  class="form-control"  placeholder="السبب" name="reason3" id="reason3"   ><?php echo $main->condition_text?></textarea>
    </div>
                </div>
                
                <div class="row">
                <div class="col-md-12 form-group">
                <label class="text-warning" style="float: right;" id="transfer">إحالة الطالب </label>
                            <input type="radio"  name="conditon_transfer" data-id="conditon_transfer" id="conditon_transfer" class="apstatus" value="Transfer" tabindex="2" style="float: right;"><span style="background-color: aliceblue; width: 70%; height: 40px; padding: 10px; margin-right: 20px; float: right;">إحالة الطالب لاتخاذ لبقرار ضمن قوائم مدرجة من جديد لجتماع اللجنة مع توضيح البيانات بالكامل</span>
                </div>
                </div>
                   <br style="clear:both;"/>
                   <div class="row">
                      <div class="form-group  col-md-3">
                         <input type="submit" class="btn btn-success btn-lrg" name="submit"  id="save_expence1" value="<?php if($labels !="")echo $labels;else echo 'حفظ';?>"   />
                      </div>
                      <!--<div class="form-group  col-md-3">
                         <input type="submit" class="btn btn-success btn-lrg" name="submit"  id="save_expence2" value="Un Renew"  />
                      </div>
                      <div class="form-group  col-md-3">
                         <input type="submit" class="btn btn-success btn-lrg" name="submit"  id="save_expence3" value="Transfer"  />
                      </div>
                       <div class="form-group  col-md-3">
                         <input type="submit" class="btn btn-success btn-lrg" name="submit"  id="save_expence4" value="Other"  />
                      </div>-->
                      
                    </div>

                              
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'inquiries/afteraddagreement/'.$applicantid.'/list','columns_array'=>'
{ "data": "الإسم" },
{"data":"رقم الاجتماع"},{"data":"اسم القائمة"},{"data":"المنطقة"},{"data":"ولاية"},{"data":"التخصص"},{"data":"السنة لدراسية"},{"data":"اخر معدل تراكمي"},{"data":"اسم الكلية أو الجامعة"},{"data":"مبلغ القرار"},{"data":"ملاحظات"},{"data":"الحالة"}')); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
<script>
<?php if($main->conditions !=""){
	
	?>
	validate('<?php echo $main->conditions?>');
	<?php
}
?>
function validate(val){
	if(val == "1")	{
		$('#Option1').show("slow");
		$('#Option2').hide("slow");
		$('#Option3').hide("slow");
	}
	else if(val == "2")	{
		$('#Option2').show("slow");
		$('#Option1').hide("slow");
		$('#Option3').hide("slow");
	}
	else if(val == "3")	{
		$('#Option3').show("slow");
		$('#Option1').hide("slow");
		$('#Option2').hide("slow");
	}
	
}
$(function(){
		$('#save_expence1').click(function(){
				var error = false;
				if($('#save_expence1').val() == "Transfer" ){
					if(document.frm_expence.conditon_transfer.checked== false){
						error = true;
						$('#conditon_transfer').css('border-color','#F00');
						$('#transfer').css('color','#F00');
					}
					if(!error){document.frm_expence.submit();}
					else{
						return false;	
					}
				}
		});
		});
</script>
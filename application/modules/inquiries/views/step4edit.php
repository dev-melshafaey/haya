<!doctype html>
<?php
  $ah_applicant             = $_applicant_data['ah_applicant'];
  $ah_applicant_wife          = $_applicant_data['ah_applicant_wife'];
  $ah_applicant_survayresult      = $_applicant_data['ah_applicant_survayresult'];
  $ah_applicant_relation        = $_applicant_data['ah_applicant_relation'];
  $ah_applicant_family        = $_applicant_data['ah_applicant_family'];
  $ah_applicant_economic_situation  = $_applicant_data['ah_applicant_economic_situation'];
  $ah_applicant_documents       = $_applicant_data['ah_applicant_documents'];
  $ah_applicant_decission       = $_applicant_data['ah_applicant_decission'];
  
?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <?php $msg  = $this->session->flashdata('msg');?>
  <?php if($msg):?>
      <div class="col-md-12">
        <div style="padding: 22px 20px !important; background:#c1dfc9;">
          <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
        </div>
      </div>
  <?php endif;?>
  <div class="row">
  <form id="finaldecission_edit" name="finaldecission_edit" method="post" action="<?PHP echo base_url().'inquiries/finaldecission_edit' ?>" autocomplete="off" enctype="multipart/form-data">   
    <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $ah_applicant->applicantid;?>" />
    <input type="hidden" name="sarvayid" id="sarvayid" value="<?php echo $ah_applicant_survayresult->sarvayid;?>" />
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module, 'headxxx' => $heading)); ?>
      <div class="col-md-12">
        <div class="col-md-5 fox leftborder">
          <h4 class="panel-title customhr">بيانات طلب مساعدة</h4>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الاستمارة: </label>
            <strong><?PHP echo arabic_date($ah_applicant->applicantcode); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">التاريخ: </label>
            <strong><?PHP echo show_date($ah_applicant->registrationdate,5); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">نوع الطلب: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->charity_type_id); ?> </strong> </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الاسم الرباعي والقبيلة: </label>
            <strong><?PHP echo $ah_applicant->fullname; ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم جواز سفر: </label>
            <strong><?PHP echo arabic_date($ah_applicant->passport_number); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم البطاقة الشخصة: </label>
            <strong><?PHP echo arabic_date($ah_applicant->idcard_number); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">المحافظة \ المنطقة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->province); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">ولاية: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->wilaya); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">البلدة \ المحلة: </label>
            <strong><?PHP echo $ah_applicant->address; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">هاتف المنزل: </label>
            <strong><?PHP echo arabic_date($ah_applicant->hometelephone); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الهاتف :</label>
            <strong><?PHP echo arabic_date(implode('<br>',json_decode($ah_applicant->extratelephone,TRUE))); ?></strong></div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة الجتماعية:</label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->marital_status); ?></strong> </div>
          <?PHP foreach($ah_applicant_wife as $wife) { ?>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم الزوج\الزوجة: </label>
            <strong><?PHP echo $wife->relationname; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الجواز\البطاقة: </label>
            <strong><?PHP echo arabic_date($wife->relationdoc); ?></strong> </div>
          <?PHP } ?>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم الام: </label>
            <strong><?PHP echo $ah_applicant->mother_name; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الجواز\البطاقة: </label>
            <strong><?PHP echo arabic_date($ah_applicant->mother_id_card); ?></strong> </div>
          <!-----------------Documents-------------------------->
            
          <!---------------------------------------------------->
          <div class="col-md-12 form-group">
            <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
              <h4 class="panel-title customhr">اترفق الوثائق والمستندات الازمة لطلب</h4>
              <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                <?PHP 
          $doccount = 0;          
          foreach($this->inq->allRequiredDocument($ah_applicant->charity_type_id) as $ctid) { $doccount++; 
            $doc = $ah_applicant_documents[$ctid->documentid];            
            $url = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$doc->document;
          ?>
                <div class="panel panel-default" style="border-bottom: 1px solid #ddd;">
                  <div class="panel-heading" style="padding:7px 3px !important;" id="head<?PHP echo $ctid->documentid;?>">
                    <h4 class="panel-title" style="font-size:12px; font-weight:normal !important;">
                      <?PHP if($doc->appli_doc_id!='') { ?>
                      <span class="icons" id="removeicons<?PHP echo $doc->appli_doc_id; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> </span>
                      <?PHP } ?>
                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $ctid->documentid;?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                  </div>
                </div>
                <?PHP } ?>
              </div>
            </div>
          </div>
          <h4 class="panel-title customhr">افراد الأسرة: (بما فهيم العاملين و غيرالعاملين) </h4>
          <div class="col-md-12 form-group">
            <table class="table table-bordered table-striped dataTable">
              <thead>
                <tr role="row">
                  <th>الاسم</th>
                  <th style="width: 50px;">السن</th>
                  <th>القرابة</th>
                  <th>المهنة</th>
                  <th style="width: 70px;">الدخل الشهري</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?PHP 
        
        if($ah_applicant_relation)
        { 
          foreach($ah_applicant_relation as $apr) { ?>
                <tr class="familycount">
                  <td><?PHP echo $apr->relation_fullname; ?></td>
                  <td><?PHP echo arabic_date($apr->age); ?></td>
                  <td><?PHP echo $this->haya_model->get_name_from_list($apr->relationtype); ?></td>
                  <td><?PHP echo $this->haya_model->get_name_from_list($apr->professionid); ?></td>
                  <td><?PHP echo arabic_date($apr->monthly_income); ?></td>
                </tr>
                <?PHP 
                    $salarys += $apr->monthly_income;
                }
        }
       ?>
             <input type="hidden" id="familysalary" value="<?PHP echo $ah_applicant->salary; ?>" />
              </tbody>
            </table>
          </div>
          <h4 class="panel-title customhr">االحالة الاقتصادية: </h4>
          <div class="col-md-6 form-group">
            <label class="text-warning">الدخل: </label>
            <strong><?PHP echo arabic_date($ah_applicant->salary); ?></strong> </div>
          <div class="col-md-6 form-group">
            <?PHP if($ah_applicant->source=='يوجد') { echo('يوجد: يوضح كالاتي'); } elseif($ah_applicant->source=='لايوجد') { echo('لايوجد: ويوضح مصدر المعيشة'); } ?>
          </div>
          <?PHP if($ah_applicant->source=='يوجد') { ?>
          <div class="col-md-6 form-group">
            <label class="text-warning">مصدره: </label>
            <strong><?PHP echo $ah_applicant->musadra; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">قيمتة: </label>
            <strong><?PHP echo $ah_applicant->qyama; ?></strong> </div>
          <?PHP } ?>
          <?PHP if($ah_applicant->source=='لايوجد') { ?>
          <div class="col-md-12 form-group">
            <label class="text-warning">مصدر المعيشة: </label>
            <strong><?PHP echo $ah_applicant->lamusadra; ?></strong> </div>
          <?PHP } ?>
          <div class="col-md-12 form-group">
            <label class="text-warning">وضف الحالة السكنية: </label>
            <?PHP echo $ah_applicant->current_situation; ?> </div>
          <div class="col-md-7 form-group">
            <label class="text-warning"><strong><?PHP echo $ah_applicant->ownershiptype; ?></strong> </label>
          </div>
          <div class="col-md-5 form-group"> <strong><?PHP echo arabic_date($ah_applicant->ownershiptype_amount); ?></strong> </div>
          <div class="col-md-12">
            <h4 class="panel-title customhr">مكونات المنزل:</h4>
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">نوع البناء:</label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->building_type); ?></strong> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">عدد الغرف:</label>
            <strong><?PHP echo arabic_date($ah_applicant->number_of_rooms); ?></strong> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">المرافق:</label>
            <strong><?PHP echo arabic_date($ah_applicant->utilities); ?></strong> </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">حالة الأثاث والموجودات:</label>
            <strong><?PHP echo $ah_applicant->furniture; ?><strong> </div>
           <div class="col-md-12">
            <h4 class="panel-title customhr">البيانات الدراسية:</h4>
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">شهادة ثنوية:</label>
            <strong><?PHP echo $ah_applicant->certificate_dualism; ?></strong> </div>
           <div class="col-md-4 form-group">
            <label class="text-warning">سنة التخرج:</label>
            <strong><?PHP echo $ah_applicant->graduation_year; ?></strong> </div>
            <div class="col-md-4 form-group">
            <label class="text-warning">النسبة :</label>
            <strong><?PHP echo $ah_applicant->school_percentage; ?></strong> </div>
          <div class="col-md-12">
            <h4 class="panel-title customhr">البيانات الجامعية :</h4>
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">اسم الكلية أو الجامعة:</label>
            <strong><?PHP echo $ah_applicant->college_name; ?></strong> </div>
           <div class="col-md-4 form-group">
            <label class="text-warning">التخصص :</label>
            <strong><?PHP echo $ah_applicant->specialization; ?></strong> </div>
            <div class="col-md-4 form-group">
            <label class="text-warning">السنة لدراسية :</label>
            <strong><?PHP echo $ah_applicant->year_of_study; ?></strong> </div>
            <div class="col-md-4 form-group">
            <label class="text-warning">اخر معدل تراكمي :</label>
            <strong><?PHP echo $ah_applicant->last_grade_average; ?></strong> </div>
          <div class="col-md-12">
            <h4 class="panel-title customhr">تفاصيل البنك:</h4>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم البنك:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant->bankid); ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الفرع:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant->branchid); ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الحساب:</label>
            <?PHP echo $ah_applicant->accountnumber; ?>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">نوع الحساب: جاري \ توفير : </label>
            <?PHP echo($ah_applicant->accounttype); ?>           
          </div>
          <?PHP if($ah_applicant->bankstatement) { ?>
          <div class="col-md-12 form-group">
            <label class="text-warning">يرفق صورة من كشف الحسابات (حديث الاصدار) :</label>           
            <?PHP
            $statmentURL = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->bankstatement;
      echo getFileResult($statmentURL,'يرفق صورة من كشف الحسابات (حديث الاصدار)',$statmentURL);
      ?>
          </div>
          <?PHP } ?>
        </div>
        <div class="col-md-7 fox">       
          <h4 class="panel-title customhr">بعد إجراء البحث الاجتماعي والاطلاع على المستندات المؤيدة اتضح مايلي :-</h4>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة الصحية :</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->health_condition); ?>
      
            
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة السكنية :</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->housing_condition); ?>
            
            
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">عدد أفراد الأسرة :</label>
            <?PHP echo arabic_date($ah_applicant_survayresult->numberofpeople); ?>
            
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">ترتيب مقدم الطلب بالأسرة :</label>
            <?PHP echo arabic_date($ah_applicant_survayresult->positioninfamily); ?>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الحالة الاقتصادية :</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->economic_condition); ?>            
            
          </div>
          <div class="col-md-12 form-group">
            <h4 class="panel-title customhr">نوع الحالة (<?PHP echo $ah_applicant_survayresult->casetype; ?>) :-</h4>           
         </div>
          <div class="col-md-12 form-group casetype" id="zamanya" <?PHP if($ah_applicant_survayresult->casetype=='للحالات الضمانية') { ?> style="display:block !important;" <?PHP } ?>>
            <div class="col-md-6 form-group">
              <label class="text-warning">الحالة الضمانية باسم :</label>
              <?PHP echo $ah_applicant_survayresult->aps_name; ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الحاسب :</label>
              <?PHP echo $ah_applicant_survayresult->aps_account; ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الملف :</label>
              <?PHP echo $ah_applicant_survayresult->aps_filename; ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الفئة :</label>
              <?PHP echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->aps_category); ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">قيمة المعاش :</label>
              <?PHP echo arabic_date($ah_applicant_survayresult->aps_salaryamount); ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">تاريخ الربط :</label>
              <?PHP echo arabic_date(date('d/m/Y',strtotime($ah_applicant_survayresult->aps_date))); ?>
            </div>
            
            <div class="col-md-12 form-group">
              <label class="text-warning">مصادر دخل أخرى :</label>
              <?PHP $json = json_decode($ah_applicant_survayresult->aps_another_income,TRUE);
            $inx = 0;
            for($mdi=1; $mdi<=4; $mdi++) 
          { 
            if($json[$inx]) 
            { 
              echo('<li>'.$json[$inx].'<li>');
              $inx++; 
            } 
          } 
        ?>
            </div>
          </div>
          <div class="col-md-12 form-group casetype" id="ghairzamanya"  <?PHP if($ah_applicant_survayresult->casetype=='للحالات غير الضمانية') { ?> style="display:block !important;" <?PHP } ?>>
            <div class="col-md-12 form-group">
              <label class="text-warning">مصادر دخل أخرى :</label>
              <?PHP $inxp = 0;
            for($mdix=1; $mdix<=4; $mdix++) 
          { if($json[$inxp])
            {
              echo('<li>'.$json[$inxp].'<li>');
              $inxp++; 
            }
          }?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">اجمالي الدخل الشهري للفرد :</label>
              <?PHP echo $ah_applicant_survayresult->aps_month; ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">اجمالي الدخل الشهري للاسرة :</label>
              <?PHP echo $ah_applicant_survayresult->aps_year; ?>
            </div>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الغرض من المساعدة :</label>
            <?PHP echo $ah_applicant_survayresult->whyyouwant; ?>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">ملخص الحالة :</label>
            <?PHP echo $ah_applicant_survayresult->summary; ?>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">رأي الباحث الاجتماعي و مبرراته :</label>
            <?PHP echo $ah_applicant_survayresult->review; ?>
          </div>
          <div class="col-md-12 form-group" style="background-color: #CDEB8B; padding-bottom: 9px; padding-top: 9px;">  
          <?PHP if($ah_applicant->case_close==0) {?>
       <div class="col-md-4 form-group">
              <label class="text-warning">رقم الاجتماع :</label>
              <?php echo $ah_applicant->application_status;?>
            </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">توصية اللجنة :</label>
              <?php echo $ah_applicant->application_status;?>
            </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">الملاحظات :</label>
                <?php echo $ah_applicant_decission->notes;?>
            </div>
      <div class="col-md-4 form-group">
              <label class="text-warning">المبلغ المعتمد للطالب :</label>
                <?php echo $ah_applicant_decission->decission_amount;?>
            </div>     
            <div class="col-md-4 form-group">
              <label class="text-warning">تاريخ بدء المنحة :</label>
                <?php echo $ah_applicant_decission->decission_amount;?>
            </div>    
           
           <div class="col-md-12 form-group" id="moneydiva" style="padding:0px; display:none;" >
            <div class="col-md-3 form-group"><label class="text-warning">الفئة المستحقة :</label></div>
            <div class="col-md-9 form-group" id="moneylista"></div>
          </div>
          
           <div class="col-md-12 form-group">
           <label class="text-warning">الملاحظات والقرار :</label><?php echo $ah_applicant->meeting_notes;?> 
            </div>
           <div class="col-md-12 form-group"><label class="text-warning">مرفقات :</label>
            <?php if($ah_applicant_decission->decission_doc !=""){
        $controller->getfileicon($ah_applicant_decission->decission_doc,base_url().'resources/aid/'.$login_userid);
        ?>
            
          
           <?php }?>
        </div>
        </div>  
       <?php } else {  ?>
           <div class="col-md-12 form-group"><label class="text-warning">عرض في الاجتماع رقم :</label><?PHP echo $ah_applicant->meeting_number; ?></div>           
           <div class="col-md-12 form-group"><label class="text-warning">الملاحظات والقرار :</label><?PHP echo $ah_applicant->meeting_notes; ?></div>
         
          <?PHP } ?>
            <div class="col-md-12">

        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <p>
                <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid">

                  <div class="row table-header-row">
                  <a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>inquiries/addrenewdata/<?php echo $applicantid;?>/<?php echo $appRewMid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">تحديث</a>

                  <table class="table table-bordered table-striped ">
                    <thead>
                      <tr role="row" style="background-color: #029625 !important; color:#fff;">
                       <th style="text-align:center;">تاريخ التجديد</th>
                        <th style="text-align:center;">السنة الدراسية</th>
                        <th style="text-align:center;">اخر معدل تراكمي</th>
                        <th style="text-align:center;">ملاحظات</th>
                         <th style="text-align:center;">توقع التخرج</th>
                        <th style="text-align:center;">الدرجة العلمية</th>
                        <th style="text-align:center;">الانذارات</th>
                        <th style="text-align:center;">المرفق</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php
          if(count($notes)>0){
            foreach($notes as $rows){?>
                           <tr role="row">
                               <td style="text-align:center;"><?php echo  $rows->currentdate ?></td>
                                <td style="text-align:center;"><?php echo  $rows->years ?></td>
                                <td style="text-align:center;"><?php echo  $rows->cumulative ?></td>
                                <td style="text-align:center;"><?php echo  $rows->notes ?></td>
                                <td style="text-align:center;"><?php echo  $rows->degree ?></td>
                                <td style="text-align:center;"><?php echo  $rows->degree_num ?></td>
                                <td style="text-align:center;"><?php echo  $rows->notes2 ?></td>
                                <td style="text-align:center;"><?php if($rows->attachment1 !=""){echo  $controller->getfileicon($rows->attachment1,base_url().'resources/inquiries/'.$login_userid,'ارفاق كشف الدرجات');}
                echo '<br>'; 
                if($rows->attachment2 !=""){ echo  $controller->getfileicon($rows->attachment2,base_url().'resources/inquiries/'.$login_userid,'ارفاق الخطة الدراسية');} ?></td>
                              </tr>
                        <?php
            }
          }
          
          ?>
                    </tbody>
                  </table>
                   </div>
               
              </div> 
              <div style="margin-bottom:20px;"></div>
              <div style="border: 2px solid #029625; padding:20px;">
                   <form action="" method="POST" id="frm_expence" name="frm_expence" action="<?php echo base_url()?>aid/afteraddagreement" enctype="multipart/form-data" >
                   <input type="hidden" name="applicantid" id="applicantid" value="<?PHP echo $applicantid; ?>">
    <input type="hidden" name="userid" id="userid" value="<?PHP echo $user_detail['profile']->userid; ?>">
        <input type="hidden" name="appRewMid" id="appRewMid" value="<?PHP echo $appRewMid; ?>">
                   
           
                    <div class="row"> 
                     <div class="col-md-6 form-group">
                <label class="text-warning">طلب تغيير التخصص</label>
                            <input type="radio" <?php if(isset($main->conditions)){ if($main->conditions =="1"){echo "checked";}}else{echo "checked";}?>  name="chkoption" data-id="chkoption" id="chkoption_1" class="apstatus" value="1" tabindex="2" onclick="validate(this.value);">
                </div>
                </div>
                <div class="row" id="Option1">
                 <div class="form-group col-md-6">
                      <label for="basic-input">المرفق</label>
                      <input type="file" class="form-control " value="" placeholder="المرفق" name="attachment1[]" id="attachment1"  multiple="multiple"/>
                       <input type="hidden"  name="attachment1_old" id="attachment1_old" value=""  />
                       <br/>
                       <?php if($main->condition_attachment !="")echo $controller->getfileicon($main->condition_attachment,base_url().'resources/inquiries/'.$login_userid);?>
                   
                    </div>
                     <div class="form-group col-md-6">
      <label for="basic-input">السبب</label>
      <textarea  class="form-control"  placeholder="السبب" name="reason1" id="reason1"   ><?php echo $main->condition_text?></textarea>
    </div>
    <div class="form-group col-md-12">
      <label for="basic-input">ملاحظات</label>
      <textarea  class="form-control"  placeholder="السبب" name="reason3" id="reason3"   ><?php echo $main->condition_text?></textarea>
    </div>
                </div>
                <div class="row">
                <div class="col-md-6 form-group">
                <label class="text-warning">طلب انتقال من مؤسسة تعليمية</label>
                            <input type="radio"  name="chkoption" <?php if($main->conditions =="2"){echo "checked";}?>  data-id="chkoption" id="chkoption_2" class="apstatus" value="2" tabindex="2" onclick="validate(this.value);">
                </div>
                </div>
                <div class="row" id="Option2" style="display:none;">
                 <div class="form-group col-md-6" >
                      <label for="basic-input">المرفق</label>
                      <input type="file" class="form-control " value="" placeholder="المرفق" name="attachment2[]" id="attachment2"  multiple="multiple"/>
                       <input type="hidden"  name="attachment2_old" id="attachment2_old" value=""  />
                   
                 
                   </div>
                     <div class="form-group col-md-6">
      <label for="basic-input">السبب</label>
      <textarea  class="form-control"  placeholder="السبب" name="reason2" id="reason2"    ><?php echo $main->condition_text?></textarea>
    </div>
    <div class="form-group col-md-12">
      <label for="basic-input">ملاحظات</label>
      <textarea  class="form-control"  placeholder="السبب" name="reason3" id="reason3"   ><?php echo $main->condition_text?></textarea>
    </div>
                </div>
                
                <div class="row">
                 <div class="col-md-6 form-group">
                <label class="text-warning">طلب تغيير درجة علمية</label>
                            <input type="radio"  name="chkoption" <?php if($main->conditions =="3"){echo "checked";}?>  data-id="chkoption" id="chkoption_3" class="apstatus" value="3" tabindex="2" onclick="validate(this.value);">
                </div>
                </div>
                <div class="row" id="Option3" style="display:none;">
                 <div class="form-group col-md-6">
                      <label for="basic-input">المرفق</label>
                      <input type="file" class="form-control " value="" placeholder="المرفق" name="attachment3[]" id="attachment3"  multiple="multiple"/>
                       <input type="hidden"  name="attachment3_old" id="attachment3_old" value=""  />
                   
                    </div>
                     <div class="form-group col-md-6">
      <label for="basic-input">السبب</label>
      <textarea  class="form-control"  placeholder="السبب" name="reason3" id="reason3"   ><?php echo $main->condition_text?></textarea>
    </div>
         <div class="form-group col-md-12">
      <label for="basic-input">ملاحظات</label>
      <textarea  class="form-control"  placeholder="السبب" name="reason3" id="reason3"   ><?php echo $main->condition_text?></textarea>
    </div>
                </div>

                 <div class="row">
                 <div class="col-md-6 form-group">
                <label class="text-warning">طلب تأجيل</label>
                            <input type="radio"  name="chkoption" <?php if($main->conditions =="4"){echo "checked";}?>  data-id="chkoption" id="chkoption_3" class="apstatus" value="4" tabindex="2" onclick="validate(this.value);">
                </div>
                </div>
                <div class="row" id="Option4" style="display:none;">
                 <div class="form-group col-md-6">
                      <label for="basic-input">المرفق</label>
                      <input type="file" class="form-control " value="" placeholder="المرفق" name="attachment3[]" id="attachment3"  multiple="multiple"/>
                       <input type="hidden"  name="attachment3_old" id="attachment3_old" value=""  />
                   
                    </div>
                     <div class="form-group col-md-6">
      <label for="basic-input">السبب</label>
      <textarea  class="form-control"  placeholder="السبب" name="reason3" id="reason3"   ><?php echo $main->condition_text?></textarea>
    </div>
         <div class="form-group col-md-12">
      <label for="basic-input">ملاحظات</label>
      <textarea  class="form-control"  placeholder="السبب" name="reason3" id="reason3"   ><?php echo $main->condition_text?></textarea>
    </div>
                </div>
                
                <div class="row">
                <div class="col-md-12 form-group">
                <label class="text-warning" style="float: right;" id="transfer">إحالة الطالب </label>
                            <input type="radio"  name="conditon_transfer" data-id="conditon_transfer" id="conditon_transfer" class="apstatus" value="Transfer" tabindex="2" style="float: right;"><span style="background-color: aliceblue; width: 70%; height: 40px; padding: 10px; margin-right: 20px; float: right;">إحالة الطالب لاتخاذ لبقرار ضمن قوائم مدرجة من جديد لجتماع اللجنة مع توضيح البيانات بالكامل</span>
                </div>
                </div>
                
          
      </div>
             <h4 class="panel-title customhr">الاسقاط: </h4>
 <div class="form-group col-md-6">
      <label for="basic-input">سبب الإسقاط</label>
      <input type="text"  class="form-control"  placeholder="السبب" name="reason3" id="reason3"   ><?php echo $main->condition_text?></textarea>
    </div>
       <div class="form-group col-md-6">
                      <label for="basic-input">المرفق</label>
                      <input type="file" class="form-control " value="" placeholder="المرفق" name="attachment3[]" id="attachment3"  multiple="multiple"/>
                       <input type="hidden"  name="attachment3_old" id="attachment3_old" value=""  />
                   
                    </div>
     <div class="form-group col-md-6">
      <label for="basic-input">بيان التخرج</label>
      <input type="text" class="form-control"  placeholder="بيان التخرج" name="reason3" id="reason3"   ><?php echo $main->condition_text?></textarea>
    </div>
        <div class="form-group col-md-6">
                      <label for="basic-input">المرفق</label>
                      <input type="file" class="form-control " value="" placeholder="المرفق" name="attachment3[]" id="attachment3"  multiple="multiple"/>
                       <input type="hidden"  name="attachment3_old" id="attachment3_old" value=""  />
                   
                    </div>
                       <br style="clear:both;"/>
                   <div class="row">
                      <div class="form-group  col-md-3">
                         <input type="submit" class="btn btn-success btn-lrg" name="submit"  id="save_expence1" value="<?php if($labels !="")echo $labels;else echo 'حفظ';?>"   />
                      </div>
                
          </div>
    </div>
    </div>

                    </div>
  </form>
  </div>
              
              </div>
            </div>
          </div>
        </div>
      </div>
   

  

<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'inquiries/afteraddagreement/'.$applicantid.'/list','columns_array'=>'
{ "data": "الإسم" },
{"data":"رقم الاجتماع"},{"data":"اسم القائمة"},{"data":"المنطقة"},{"data":"ولاية"},{"data":"التخصص"},{"data":"السنة لدراسية"},{"data":"اخر معدل تراكمي"},{"data":"اسم الكلية أو الجامعة"},{"data":"مبلغ القرار"},{"data":"ملاحظات"},{"data":"الحالة"}')); ?>

<!-- /.modal-dialog -->

</div>
</section>
<?php $this->load->view('common/footer'); ?>
<script src="<?PHP echo base_url(); ?>assets/js/tasgeel_js.js"></script>
</body>
</html>
<script>
function submitform(vals){
  if(vals=="Renew" || vals=="Transfer"){
    window.location.href ="<?php echo base_url()?>inquiries/afteraddagreement/<?php echo $ah_applicant->applicantid;?>/0/"+vals;  
  }
}
</script>
<script>
<?php if($main->conditions !=""){
  
  ?>
  validate('<?php echo $main->conditions?>');
  <?php
}
?>
function validate(val){
  if(val == "1")  {
    $('#Option1').show("slow");
    $('#Option2').hide("slow");
    $('#Option3').hide("slow");
    $('#Option4').hide("slow");
  }
  else if(val == "2") {
    $('#Option2').show("slow");
    $('#Option1').hide("slow");
    $('#Option3').hide("slow");
    $('#Option4').hide("slow");
  }
  else if(val == "3") {
    $('#Option3').show("slow");
    $('#Option1').hide("slow");
    $('#Option2').hide("slow");
    $('#Option4').hide("slow");
  }
    else if(val == "4") {
      $('#Option4').show("slow");
    $('#Option3').hide("slow");
    $('#Option1').hide("slow");
    $('#Option2').hide("slow");
  }
  
}
$(function(){
    $('#save_expence1').click(function(){
        var error = false;
        if($('#save_expence1').val() == "Transfer" ){
          if(document.frm_expence.conditon_transfer.checked== false){
            error = true;
            $('#conditon_transfer').css('border-color','#F00');
            $('#transfer').css('color','#F00');
          }
          if(!error){document.frm_expence.submit();}
          else{
            return false; 
          }
        }
    });
    });
</script>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12" style="padding:0px !important;">
        <?php //$this->load->view('common/globalfilter', array('type'=>'socialservay','b'=>$branchid,'c'=>$charity_type));		 ?>
        <div class="nav nav-tabs panel panel-default panel-block">
          <div class="tab-pane list-group active">           
           <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info" id="CustomerList">
              <thead>
                <tr role="row">
                  <th>رقم</th>
                  <th>الإسم</th>
                  <th>نوع المساعدات</th>
                  <th>فرع</th>
                  <th>البطاقة الشخصة</th>
                  <th>المحافظة</th>
                  <th>ولاية</th>                 
                  <th>تاريخ التسجيل</th>
                  <th>الإجرائات</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
              <!--<tr role="row">
              <?php $result			=	$this->inq->get_socilservay_list($branchid,$charity_type);?>
              <?php 
			  		foreach($result as $lc)
					{
						$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
						$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
						if($permissions[17]['u']	==	1) 
						{	$action .= '<a class="iconspace" href="'.base_url().'inquiries/socilservayresult/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
						
						if($permissions[17]['d']	==	1) 
						{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
						
			  ?>
                   <tr role="row" id="<?php echo $lc->applicantid;?>_durar_lm">
                  <td><?php echo $lc->applicantcode;?></td>
                  <td><?php echo $lc->fullname;?></td>
                  <td><?php echo $lc->charity_type;?></td>
                  <td><?php echo $lc->branchname;?></td>
                  <td><?php echo $lc->idcard_number;?></td>
                  <td><?php echo $lc->province;?></td>
                  <td><?php echo $lc->wilaya;?></td>                 
                  <td><?php echo show_date($lc->registrationdate,9);?></td>
                  <td>DELETE | UPDATE</td>
                  </tr>
                  <?php }?>-->
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
function loadDataTable() {

    var data,
      curIndex = 0,
      t = $('#CustomerList').DataTable(),
      AMOUNT_ROWS = 100;

    function addMoreRows() {

      var i, value, limit = curIndex + AMOUNT_ROWS;

      if (limit > data.length) limit = data.length;

      for (i = curIndex; i < limit; i++) {

        value = data[i];

        t.row.add([
          value.applicantid,
          value.applicantcode,
          value.fullname,
          value.charity_type,
          value.branchname,
          value.idcard_number,
          value.country,
		  value.province,
		  value.wilaya,
		  value.registrationdate,
          "<a class='btn btn-small btn-info' href='<?php echo URL::to('customer').'/';?>" + value.id + "/edit'><span class='glyphicon glyphicon glyphicon-edit' aria-hidden='true'></span></a>",
          "<form method='POST' action='<?php echo URL::to('inquiries').'/';?>" + value.id + "' accept-charset='UTF-8' class='pull-left' >" +
          "<input name='_method' type='hidden' value='DELETE'>" +
          "<button type='submit' class='btn btn-warning'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button>" + "</form>"
        ]).draw();

        curIndex = i + 1;

        if (curIndex != data.length) setTimeout(addMoreRows, 200);  // Give browser time to process other things

      }
    }

    $.ajax({
      url: config.BASE_URL+"inquiries/api/<?php echo $branchid;?>/<?php echo $charity_type;?>",
      type: 'GET',
      success: function(result) {
        data = $.parseJSON(result);
        addMoreRows();
      }
    });
  }

})();
loadDataTable();
</script>
</div>
</body>
</html>
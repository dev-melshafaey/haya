<?php $steps	=	 config_item('steps');?>
<div class="row col-md-12">
  <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
    <thead>
      <tr role="row">
        <th>رقم</th>
        <th>الإسم</th>
        <th>المرحلة</th>
        <th>القرابة</th>
        <th>رقم الجواز \ الرقم المدني</th>
        <th>تاريخ التسجيل</th>
      </tr>
    </thead>
    <tbody role="alert" aria-live="polite" aria-relevant="all">
      <?php if(!empty($all_helps)):?>
      <?php foreach($all_helps as $help):?>
            <?php $sons			=	$this->inq->total_count_son($help->applicantid);?>
            <?php $daughters	=	$this->inq->total_count_daughter($help->applicantid);?>
      		<?php $actions	   .=	' <a class="iconspace" href="'.charity_edit_url($help->charity_type_id).$help->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>'; ?>
      <tr role="row">
        <td style="text-align: right;"><?php echo arabic_date($help->applicantcode);?></td>
        <td style="text-align: right;"><?php echo '<a class="iconspace" href="'.charity_edit_url($help->charity_type_id).$help->applicantid.'">'.$help->father_name.'</a>';?></td>
        <td style="text-align: right;"><?php echo $steps[$help->step];?></td>
        <td style="text-align: right;">اخ 	(<?php echo $sons	;?>) أخت(<?php echo $daughters	;?>)</td>
        <td style="text-align: right;"><?php echo arabic_date($help->father_id_card);?></td>
        <td style="text-align: right;"><?php echo arabic_date($help->registrationdate);?></td>
      </tr>
      <?php unset($actions); endforeach;?>
      <?php endif;?>
    </tbody>
  </table>
</div>
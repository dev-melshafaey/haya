<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12" style="padding:0px;">
        <div class="col-md-12" style="padding: 12px 0px 12px 0px; background-color: #CCC; border-bottom: 2px solid #FFF;">
        <form>
          <div class="col-md-1">
            <label class="text-warning">رقم الحاسب</label>
            <input type="text" name="FORMID" placeholder="رقم الحاسب" class="form-control">
          </div>
          <div class="col-md-1">
            <label class="text-warning">رقم الاستمارة</label>
            <input type="text" name="FORMNO" placeholder="رقم الاستمارة" class="form-control">
          </div>
          <div class="col-md-2">
            <label class="text-warning">نوع</label>
            <select class="form-control" name="TYPEOFHELP">
              <option value="">نوع</option>
              <?PHP foreach($this->inq->oco_data('T',0,'YES') as $fx) { echo('<option value="'.$fx->TA_ASSISTANCEID.'">'.$fx->TA_ASSISTANCE_AR.'</option>') ?>
              <?PHP } ?>
            </select>
          </div>
          <div class="col-md-2">
            <label class="text-warning">حالة</label>
            <select class="form-control" name="FORMCASE">
              <option value="">حالة</option>
              <?PHP foreach($this->inq->oco_data('F',0,'YES') as $fx) { echo('<option value="'.$fx->CO_CASEID.'">'.$fx->CO_CASE_AR.'</option>') ?>
              <?PHP } ?>
            </select>
          </div>
          <div class="col-md-2">
            <label class="text-warning">المحافظة</label>
            <select class="form-control" name="REGION">
              <option value="">المحافظة</option>
              <?PHP foreach($this->inq->oco_data('R',0,'YES') as $fx) { echo('<option value="'.$fx->REG_ID.'">'.$fx->REG_DESC.'</option>') ?>
              <?PHP } ?>
            </select>
          </div>
          <div class="col-md-2">
            <label class="text-warning">ولاية</label>
            <select class="form-control" name="STATE">
              <option value="">ولاية</option>
              <?PHP foreach($this->inq->oco_data('W',0,'YES') as $fx) { echo('<option value="'.$fx->WIL_ID.'">'.$fx->WIL_DESC.'</option>') ?>
              <?PHP } ?>
            </select>
          </div>
          <div class="col-md-2">
            <label class="text-warning">البلدة\المحلة</label>
            <input type="text" name="TOWN" placeholder="البلدة\المحلة" class="form-control">
          </div>
          <br clear="all"> 
          <div class="col-md-3"><label class="text-warning">الاسم</label><input type="text" name="NAME" placeholder="الاسم" class="form-control"></div>
          <div class="col-md-1"><label class="text-warning">الرقم المدني</label><input type="text" name="LABOUR" placeholder="الرقم المدني" class="form-control"></div>
          <div class="col-md-1"><label class="text-warning">رقم جواز سفر</label><input type="text" name="PASSPORT" placeholder="رقم جواز سفر" class="form-control"></div>
          <div class="col-md-1"><span style="display:none !important;"><label class="text-warning">تاريخ</label><input type="text" name="CREATEDDATE" placeholder="تاريخ" class="form-control"></span></div>
          <div class="col-md-1"><label class="text-warning">جنس</label><select class="form-control" name="GENDER">
              <option value="">جنس</option>
              <?PHP foreach($this->inq->oco_data('G',0,'YES') as $fx => $fxvalue) { echo('<option value="'.$fx.'">'.$fxvalue.'</option>') ?>
              <?PHP } ?>
            </select></div>
          <div class="col-md-1"><label class="text-warning">هاتف المنزل</label><input type="text" name="PHONE" placeholder="هاتف المنزل" class="form-control"></div>
          <div class="col-md-1"><label class="text-warning">رقم الهاتف</label><input type="text" name="MOBILE" placeholder="رقم الهاتف" class="form-control"></div>
          <div class="col-md-1"><label class="text-warning">جنسية</label><select class="form-control" name="NATIONALITY_ID">
              <option value="">جنسية</option>
             
              <?PHP foreach($this->inq->oco_data('N',0,'YES') as $fx) { echo('<option value="'.$fx->NA_NATIONALITYID.'">'.$fx->NA_NATIONALITY_AR.'</option>') ?>
              <?PHP } ?>
            </select></div>
           <div class="col-md-2"><br><button type="submit" class="btn btn-danger">بحث</button><button data-url="<?PHP echo base_url(); ?>inquiries/old_missing_data" type="button" onclick="alatadad(this);" class="btn btn-success">تقرير البيانات المفقودة</button></div>  
           <br clear="all"> 
           </form>
        </div>
        <br clear="all">
        <table class="table table-bordered table-striped dataTable">
          <thead>          
            <tr>
              <th class="center">رقم الحاسب</th>
              <th class="center">رقم الاستمارة</th>
              <th class="right">نوع</th>
              <th class="right">حالة</th>
              <th class="right">الاسم</th>
              <th class="right">الرقم المدني</th>
              <th class="right">رقم جواز سفر</th>
              <th colspan="2" class="right">المحافظة / ولاية</th>
              <th class="right">البلدة\المحلة</th>
              <th class="center">تاريخ</th>
              <th class="center">جنس</th>
              <th class="center">هاتف المنزل</th>
              <th class="center">رقم الهاتف</th>
              <th class="right">جنسية</th>
            </tr>
          </thead>
          <tbody>
            <!--<tr>
              <td colspan="15" class="pageing"><?PHP echo $links; ?></td>
            </tr>-->
            <?PHP foreach($results as $res) { ?>
            <tr>
              <td class="center"><?PHP echo $res['FORMID']; ?></td>
              <td class="center"><?PHP echo $res['FORMNO']; ?></td>
              <td class="right"><?PHP echo $res['TYPEOFHELP']; ?></td>
              <td class="right"><?PHP echo $res['FORMCASE']; ?></td>
              <td class="right"><a href="#" onclick="alatadad(this);" data-url="<?PHP echo base_url(); ?>inquiries/oldviewdata/<?PHP echo $res['FORMNO']; ?>/<?PHP echo $res['FORMID']; ?>"><?PHP echo $res['NAME']; ?></a></td>
              <td class="right"><?PHP echo $res['LABOUR']; ?></td>
              <td class="right"><?PHP echo $res['PASSPORT']; ?></td>
              <td colspan="2" class="right"><?PHP echo $res['REGION']; ?> / <?PHP echo $res['STATE']; ?></td>
              <td class="right"><?PHP echo $res['TOWN']; ?></td>
              <td class="center"><?PHP echo date('Y-m-d',strtotime($res['CREATEDDATE'])); ?></td>
              <td class="center"><?PHP echo $res['GENDER']; ?></td>
              <td class="center"><?PHP echo $res['PHONE']; ?></td>
              <td class="center"><?PHP echo $res['MOBILE']; ?></td>
              <td class="right"><?PHP echo $res['NATIONALITY_ID']; ?></td>
            </tr>
            <?PHP } ?>
            <!--<tr>
              <td colspan="13" class="pageing"><?PHP echo $links; ?></td>
            </tr>-->
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
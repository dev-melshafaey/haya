<div class="row col-md-12">
  <form action="" method="POST" id="add_list_no_management" name="add_list_no_management" autocomplete="off">
    <div class="form-group col-md-6">
      <label for="basic-input">اسم القائمة</label>
      <input type="text" class="form-control req" value="<?PHP echo $list->listname; ?>" name="listname"  id="listname"  placeholder="اسم القائمة" <?php if($list->listname){?> readonly="readonly"<?php }?>/>
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">تاريخ</label>
      <input type="text" class="form-control datepicker req" value="<?PHP echo $list->listdate; ?>" name="listdate"  id="listdate"  placeholder="تاريخ"/>
    </div>
    <input type="hidden" name="listid" id="listid" value="<?php echo $list->listid;?>"/>
  </form>
  <br clear="all">
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="submit_list_management();" value="حفظ" />
  </div>
</div>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+20",
		dateFormat:'yy-mm-dd',
		});
	});
</script>
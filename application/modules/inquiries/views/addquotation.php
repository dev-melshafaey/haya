  <form  method="POST" id="frm_meeting" name="frm_meeting" action="<?php echo base_url()?>inquiries/addquotation/<?php echo $applicantid;?>" enctype="multipart/form-data" onsubmit="return uploadFiles();" >
<div class="row col-md-12">
    <div class="form-group col-md-4">
      <label for="basic-input">اسم المقاول</label>
      <input type="text" class="form-control " value="<?php echo $rows->contractName;?>" placeholder="اسم المقاول" name="contractName" id="contractName" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">اسم الشركة</label>
      <input type="text" class="form-control req" value="<?php echo $rows->company;?>" placeholder="اسم الشركة" name="company" id="company" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">الهاتف</label>
      <input type="text" class="form-control " value="<?php echo $rows->phone;?>" placeholder="الهاتف" name="phone" id="phone" onkeyup="only_numeric(this);" />
    </div>
    <div class="form-group col-md-8">
      <label for="basic-input">العنوان</label>
      <input type="text" class="form-control req"   placeholder="العنوان" name="address" id="address" value="<?php echo $rows->address;?>" />
    </div>
     
     <div class="form-group col-md-4">
      <label for="basic-input">النقال</label>
      <input type="text" class="form-control req" value="<?php echo $rows->mobile;?>" placeholder="النقال" name="mobile" id="mobile" onkeyup="only_numeric(this);" />
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">الفاكس</label>
      <input type="text" class="form-control " value="<?php echo $rows->fax;?>" placeholder="الفاكس" name="fax" id="fax"  onkeyup="only_numeric(this);" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">البريد الاكتروني</label>
      <input type="email" class="form-control " value="<?php echo $rows->email;?>" placeholder="البريد الاكتروني" name="email" id="email" />
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">أرفاق عروض الاسعار</label>
      <input type="file" class="form-control req"  placeholder="أرفاق عروض الاسعار" name="attachment[]" id="attachment" multiple="multiple" />
      <input type="hidden"  name="attachment_old" id="attachment_old" value="<?php echo $rows->attachment;?>"  />
   	<?php if($rows->attachment !=""){
				   $i=0;
				   $attachment = @explode(',',$rows->attachment);
				   foreach($attachment as $att){
					   if($att !=""){
						   $i++;
				   ?>
                   <div id="att_<?php echo $i;?>" style="float:right;">
              <?php echo $controller->getfileicon($att,base_url().'resources/inquiries/'.$login_userid);?>
            
              </div>
              <?php } } }?>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">الاجمالي</label>
      <input type="text" class="form-control " value="<?php echo $rows->total;?>" placeholder="الاجمالي" name="total" id="total"  />
    </div>
      <div class="form-group col-md-12">
      <label for="basic-input">الملاحظات</label>
      <textarea   class="form-control req"  placeholder="الملاحظات" name="notes" id="notes" ><?php echo $rows->notes;?></textarea>
    </div>
    
    <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $applicantid;?>"/>
     <input type="hidden" name="quotationsId" id="quotationsId" value="<?php echo $quotationsId;?>"/>
  
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="submit" class="btn btn-success btn-lrg" name="submit"  id="save_meeting" value="حفظ"  />
  </div>
</div>
</form>
<script>
function uploadFiles()
{
	//check_my_session();
        $('#frm_meeting .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_meeting .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_meeting .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {
	var form_data = new FormData(this); 
 $.ajax({ //ajax form submit
            url :  config.BASE_URL+'inquiries/addquotation',
            type: 'POST',
            data : form_data,
            dataType : "html",
            contentType: false,
            cache: false,
            processData:false,
			beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(msg)
		  {		
			  $('#ajax_action').hide();
			 $('#addingDiag').modal('hide');	  
			 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
			 
			listquotation('<?php echo $applicantid;?>');
		  }
        });
		}
		 else 
		{
            show_notification_error_end(ht);
			return false;
        }
		return false;
		
}
 $(document).ready(function(){

$('form').on('submit', uploadFiles);
});

</script>
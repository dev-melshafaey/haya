<div class="col-md-12" style="padding:0px;" id="printold">
<?PHP
		$ocop = $oco_person[0];
		$url = base_url().'inquiries/newformwitholddata/'.$ocop['HELPID'].'/'.$ocop['FORMID'].'/'.$ocop['FORMNO'];
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
  <tr>
  	<td class="right" colspan="4"><label class="text-warning">الاسم:</label><br><strong style="font-size: 19px; color: #000;"><?PHP echo $ocop['NAME']; ?></strong></td>
  </tr>
  <tr>
    <td class="right w25"><label class="text-warning">رقم:</label><br><?PHP echo $ocop['FORMID']; ?></td>
    <td class="right w25"><label class="text-warning">رقم إيصال:</label><br><?PHP echo $ocop['FORMNO']; ?></td>
    <td class="right w25"><label class="text-warning">تاريخ التسجيل:</label><br><?PHP echo $ocop['CREATEDDATE']; ?></td>
    <td class="right w25"><label class="text-warning">تاريخ التحديث:</label><br><?PHP echo $ocop['UPDATEDDATE']; ?></td>
  </tr>
   <tr>
    <td class="right"><label class="text-warning">الحالة الجتماعية:</label><br><?PHP echo $ocop['MARITAL_STATUS']; ?></td>
    <td class="right"><label class="text-warning">افراد الأسرة:</label><br><?PHP echo $ocop['FAMILYMEMBERS']; ?></td>
    <td class="right"><label class="text-warning">القرابة:</label><br><?PHP echo $ocop['RELATION']; ?></td>
    <td class="right"><label class="text-warning">راتب:</label><br><?PHP echo $ocop['SALARY']; ?></td>
  </tr> 
  <tr>
    <td class="right"><label class="text-warning">نوع:</label><br><?PHP echo $ocop['TYPEOFHELP']; ?></td>
    <td class="right"><label class="text-warning">حالة:</label><br><?PHP echo $ocop['FORMCASE']; ?></td>
    <td class="right"><label class="text-warning">جنسية:</label><br><?PHP echo $ocop['NATIONALITY_ID']; ?></td>
    <td class="right"><label class="text-warning">جنس:</label><br><?PHP echo $ocop['GENDER']; ?></td>
  </tr>
 <tr>
    <td class="right"><label class="text-warning">الرقم المدني:</label><br><?PHP echo $ocop['LABOUR']; ?></td>
    <td class="right"><label class="text-warning">رقم جواز سفر:</label><br><?PHP echo $ocop['PASSPORT']; ?></td>
    <td class="right"><label class="text-warning">هاتف المنزل:</label><br><?PHP echo $ocop['PHONE']; ?></td>
    <td class="right"><label class="text-warning">رقم الهاتف:</label><br><?PHP echo $ocop['MOBILE']; ?></td>    
  </tr> 
 <tr>
    <td class="right"><label class="text-warning">المحافظة\المنطقة:</label><br><?PHP echo $ocop['REGION']; ?></td>
    <td class="right"><label class="text-warning">ولاية:</label><br><?PHP echo $ocop['STATE']; ?></td>
    <td class="right"><label class="text-warning">البلدة\المحلة:</label><br><?PHP echo $ocop['TOWN']; ?></td>
    <td class="right"><label class="text-warning">مصدر الراتب:</label><br><?PHP echo $ocop['SALSOURCE']; ?></td>    
  </tr>
 <tr>
    <td class="right"><label class="text-warning">مسجل المعاملة:</label><br><?PHP echo $ocop['CREATEDBY']; ?></td>
    <td class="right"><label class="text-warning">محدث البيانات الجديدة:</label><br><?PHP echo $ocop['UPDATEDBY']; ?></td>
    <td class="right"></td>
    <td class="right"></td>    
  </tr>
<?PHP if(sizeof($oco_financial) > 0) { ?>   
<tr>
  	<td class="right" colspan="4"><h3 style="font-size: 19px; color: #000; margin: 0px;">ملاحظات المعاملات المالية</h3></td>
  </tr> 
  <?PHP foreach($oco_financial as $fin) { 
  
  			$finAddUser = $this->inq->oco_person_data($fin->CREATEDBY);
			$finEditUser = $this->inq->oco_person_data($fin->UPDATEDBY);
  ?>
 <tr>
    <td class="right"><label class="text-warning">الرمز:</label><br><?PHP echo check_null_oco($fin->FIN_CODE); ?></td>
    <td class="right"><label class="text-warning">المبلغ:</label><br><?PHP echo check_null_oco($fin->FIN_AMOUNT); ?></td>
    <td class="right"><label class="text-warning">الفئة:</label><br><?PHP echo check_null_oco($fin->FIN_CATEGORY); ?></td>
    <td class="right"><label class="text-warning">رقم الاجتماع:</label><br><?PHP echo check_null_oco($fin->FIN_NOOFMEETING); ?></td>    
  </tr>
   <tr>
    <td class="right"><label class="text-warning">تاريخ الملاحضات:</label><br><?PHP echo show_date(check_null_oco($fin->CREATEDDATE),9); ?></td>
    <td class="right"><label class="text-warning">الموظف:</label><br><?PHP echo $finAddUser->UN_USERNAME_AR; ?></td>
    <td class="right"><label class="text-warning">تاريخ التحديث:</label><br><?PHP echo show_date(check_null_oco($fin->UPDATEDDATE),9); ?></td>
    <td class="right"><label class="text-warning">الموظف:</label><br><?PHP echo $finEditUser->UN_USERNAME_AR; ?></td>    
  </tr>
  <tr>
    <td class="right" colspan="4"><?PHP echo $fin->FIN_NOTES; ?></td>    
  </tr>  
  <?PHP } } ?>
<?PHP if(sizeof($oco_house) > 0) { ?>   
<tr>
  	<td class="right" colspan="4"><h3 style="font-size: 19px; color: #000; margin: 0px;">ملاحظات المعاملات الاسكانية</h3></td>
  </tr> 
<?PHP foreach($oco_house as $hou) { ?>
 <tr>
    <td class="right"><label class="text-warning">التاريخ:</label><br><?PHP echo show_date(check_null_oco($hou->HOU_DATE),9); ?></td>
    <td class="right"><label class="text-warning">المبلغ:</label><br><?PHP echo check_null_oco($hou->HOU_AMOUNT); ?></td>
    <td class="right"><label class="text-warning">نوع وحجم العمل:</label><br><?PHP echo check_null_oco($hou->HOU_SIZEOFWORK); ?></td>
    <td class="right"><label class="text-warning">الدفعة:</label><br><?PHP echo check_null_oco($hou->HOU_BATCH); ?></td>    
  </tr>
  <?PHP if($hou->HOU_PROVIDER!='') { ?>
 <tr>
    <td class="right" colspan="4"><label class="text-warning">الجهة المقدمة:</label><br><?PHP echo check_null_oco($hou->HOU_PROVIDER); ?></td>   
  </tr> 
 <?PHP } ?>  
  <tr>
    <td class="right" colspan="4"><?PHP echo $hou->HOU_NOTES; ?></td>    
  </tr>  
  <?PHP } } ?>       
</table>
<br clear="all">
</div>
<div class="col-md-12  center">
<button type="button" class="btn btn-info" id="print" onclick="printthepage('printold');"><i class="icon-print"></i> طباعة</button>
<button type="button" class="btn btn-success" id="transfer" onclick="transferdata('<?PHP echo $url; ?>');"><i class="myicon icon-random"></i> نقل البيانات</button>
</div>
<script>
function transferdata(url)
{
	location.href = url;
}
</script>
<br clear="all">
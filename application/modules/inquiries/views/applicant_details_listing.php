<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<?php $segment = $this->uri->segment(3); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12"><h1>اسم القائمة الي فيها الاسماء</h1>
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
             
                <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"><?php echo add_button('inquiries/listalluserswithstatus/1','الطلبة الموافقين على المنحة',1); ?></div>                   
					<div class="row table-header-row"><?php echo add_button('inquiries/listalluserswithstatus/0','الطلبة الرافضين للمنحة',1); ?></div> 
                  <div class="row table-header-row">  </div>

                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                       <th style="text-align:center;">الإسم</th>
                       
                         <th style="text-align:center;">ولاية</th>
                         <th style="text-align:center;">السنة لدراسية</th>
                         <th style="text-align:center;">اخر معدل تراكمي</th>
                         <th style="text-align:center;">اسم الكلية أو الجامعة</th>
                          <th style="text-align:center;">المبلغ المعتمد</th>
                          <th style="text-align:center;">ملاحظات</th>
                          <th style="text-align:center;">الحالة</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'inquiries/applicant_details_listing/'.$applicantid.'/list','columns_array'=>'
{ "data": "الإسم" },
{"data":"ولاية"},{"data":"السنة لدراسية"},{"data":"اخر معدل تراكمي"},{"data":"اسم الكلية أو الجامعة"},{"data":"مبلغ القرار"},{"data":"ملاحظات"},{"data":"الحالة"}')); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
<?php $decission = $this->inq->get_last_decission($applicantid);?>
<?php
$percentage 		=	50;
$total_amount		=	$decission->decission_amount;
$percentage_amount  =	($percentage / 100) * $decission->decission_amount;
?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <?php $msg	=	$this->session->flashdata('msg');?>
  <?php if($msg):?>
  <div class="col-md-12">
    <div style="padding: 22px 20px !important; background:#c1dfc9;">
      <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
    </div>
  </div>
  <?php endif;?>
  <div class="row">
    <form id="finaldecission_edit" name="finaldecission_edit" method="post" action="<?PHP echo base_url().'inquiries/createcontract/'.$applicantid.'/'.$quotationsDataId ?>" autocomplete="off" enctype="multipart/form-data">
      <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $applicantid;?>" />
      <input type="hidden" name="quotationsDataId" id="quotationsDataId" value="<?php echo $quotationsDataId;?>" />
      <div class="col-md-12" style="margin-top:30px; background-color:#FFF; padding-bottom:50px;"> 
        
        <!----------------------------> 
        <!-- Reeja Soni 22-5-17 start-->

        
        <table class="table table-bordered table-striped" style="padding:10px !important;" >
          <thead>
            <tr role="row" style="background-color: #029625; color:#fff !important;">
              <th colspan="4">الــــــــــــــطـــرف الأول <?php //echo $number_format->format(1000); ?></th>
              <th colspan="5">الــــــــــــطرف الثاني</th>
              <th colspan="4">الــــــــــــطرف الثالث</th>
            </tr>
            <tr role="row" style=" color:#F00 !important;">
              <th style="background-color:#FDEABD;"  >المستفيد</th>
              <th style="background-color:#FDEABD;"  >العنوان</th>
              <th style="background-color:#FDEABD;"  >الهاتف</th>
              <th style="background-color:#FDEABD;"  >النقال</th>
              <th style="background-color: #D9B0FD;"  >اسم الشركة</th>
              <th style="background-color: #D9B0FD;" >أسم المقاول</th>
              <th style="background-color: #D9B0FD;" >العنوان</th>
              <th style="background-color: #D9B0FD;" >الهاتف</th>
              <th style="background-color: #D9B0FD;" >الفاكس</th>
              <th style="background-color:#FFCAFF;" >الممول</th>
              <th style="background-color:#FFCAFF;" >العنوان</th>
              <th style="background-color:#FFCAFF;" >بيانات الاتصال</th>
              <th style="background-color:#FFCAFF;" >الإجراءات</th>
           </tr>
          </thead>
          <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php if(count($quotation)>0){
			  $currentQut = $rows->quotationsId;
			  $quotation_detail	=	$this->inq->get_quatation_detail($currentQut);
			  $currentQtId = @explode(',',$currentQut);
			  $k =0;
			  foreach($quotation as $qt){
				  if(in_array($qt->quotationsId,$currentQtId)){
				$k++;	  
			  ?>
            <tr role="row" style=" color:#F00 !important;">
              <td><?php echo $application->fullname;?></td>
              <td><?php echo $application->address;?></td>
              <td><?php echo arabic_date(implode('<br>',json_decode($application->hometelephone,TRUE)));?></td>
              <td><?php echo arabic_date(implode('<br>',json_decode($application->extratelephone,TRUE)));?></td>
              <td><?php echo $qt->company; ?></td>
              <td><?php echo $qt->contractName; ?></td>
              <td><?php echo $qt->address; ?></td>
              <td><?php echo $qt->phone; ?></td>
              <td><?php echo $qt->fax; ?></td>
              <?php $thirdparty		=  $this->inq->getdata($qt->quotationsId,'ah_applicant_thirdparty','quotationsId');?>
              <?php $idthirdparty 	=  count($thirdparty)? $thirdparty->id :0;?>
              
              <td id="Financier<?php echo $k;?>" ><span id="Financiertxt<?php echo $k;?>"><?php echo count($thirdparty)?$thirdparty->funded :"الممول";?></span> <span id="Financierinp<?php echo $k;?>" style="display:none;">
                <input type="text" class="form-control req " value="<?php echo count($thirdparty)?$thirdparty->funded :"الممول";?>" placeholder="الممول" name="funded<?php echo $k;?>" id="funded<?php echo $k;?>"  />
                </span></td>
              <td id="Address<?php echo $k;?>" ><span id="Addresstxt<?php echo $k;?>"><?php echo count($thirdparty)?$thirdparty->address :"العنوان";?></span> <span id="Addressinp<?php echo $k;?>"  style="display:none;">
                <input type="text" class="form-control req " value="<?php echo count($thirdparty)?$thirdparty->address :"العنوان";?>" placeholder="العنوان" name="address<?php echo $k;?>" id="address<?php echo $k;?>"  />
                </span></td>
              <td id="ContactData<?php echo $k;?>" ><span id="ContactDatatxt<?php echo $k;?>"><?php echo count($thirdparty)?$thirdparty->contactData :"بيانات الاتصال";?></span> <span id="ContactDatainp<?php echo $k;?>"  style="display:none;">
                <input type="text" class="form-control req " value="<?php echo count($thirdparty)?$thirdparty->contactData :"بيانات الاتصال";?>" placeholder="بيانات الاتصال" name="contactData<?php echo $k;?>" id="contactData<?php echo $k;?>"  />
                </span></td>
              <td id="edit<?php echo $k;?>" ><span id="edittxt<?php echo $k;?>"><a onClick="edittd('<?php echo $k;?>','<?php echo $qt->quotationsId;?>');" ><i class="icon-pencil" ></i></a></span> <span id="editinp<?php echo $k;?>"  style="display:none;">
                <button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float:left; margin-bottom: 8px; " onclick="update_thirdparty('<?php echo $k;?>','<?php echo $qt->quotationsId;?>','<?php echo $idthirdparty;?>');">اضافة</button>
                <button type="button" id="btn_update_thirdparty" class="btn btn-sm btn-success" style="float:left; margin-bottom: 8px; " onclick="cancel_thirdparty('<?php echo $k;?>','<?php echo $qt->quotationsId;?>');">إلغاء</button>
                </span></td>
            </tr>
            <?php } } } ?>
          </tbody>
        </table>
        <script type="text/javascript">
		 function update_thirdparty(id,quotationsId,idthirdparty){
		   $.ajax({
			url: config.BASE_URL+'inquiries/update_thirdparty/',
			type: "POST",
			data:{'quotationsId':quotationsId,'id':idthirdparty,'funded':$('#funded'+id).val(),'address':$('#address'+id).val(),
			'ContactData':$('#contactData'+id).val() },
			dataType: "html",
			success: function(response)
			{	
				$('#Financiertxt'+id).html($('#funded'+id).val());
				$('#Addresstxt'+id).html($('#address'+id).val());
				$('#ContactDatatxt'+id).html($('#contactData'+id).val());

				$('#funded'+id).val($('#funded'+id).val());
				$('#address'+id).val($('#address'+id).val());
				$('#ContactData'+id).val($('#contactData'+id).val());
				
				$('#Financiertxt'+id).css('display','block');
				$('#Addresstxt'+id).css('display','block');
				$('#ContactDatatxt'+id).css('display','block');
				$('#edittxt'+id).css('display','block');
				
				$('#Financierinp'+id).css('display','none');
				$('#Addressinp'+id).css('display','none');
				$('#ContactDatainp'+id).css('display','none');
				$('#editinp'+id).css('display','none');
			
			$('#editinp'+id).html('<button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float:left; margin-bottom: 8px; " onclick="update_thirdparty('+id+','+quotationsId+','+response+');">اضافة</button><button type="button" id="btn_update_thirdparty" class="btn btn-sm btn-success" style="float:left; margin-bottom: 8px; " onclick="cancel_thirdparty('+id+','+quotationsId+');">إلغاء</button>');
			 return true;
			
			 }			
		});	
		 }
		 function edittd(id,quotationsId)
		 {
			$('#Financiertxt'+id).css('display','none');
			$('#Addresstxt'+id).css('display','none');
			$('#ContactDatatxt'+id).css('display','none');
			$('#edittxt'+id).css('display','none');
					 
			$('#Financierinp'+id).css('display','block');
			$('#Addressinp'+id).css('display','block');
			$('#ContactDatainp'+id).css('display','block');
			$('#editinp'+id).css('display','block');
		 }
		 
		 function cancel_thirdparty(id,quotationsId)
		 { 
			$('#Financiertxt'+id).css('display','block');
			$('#Addresstxt'+id).css('display','block');
			$('#ContactDatatxt'+id).css('display','block');
			$('#edittxt'+id).css('display','block');
					 
			$('#Financierinp'+id).css('display','none');
			$('#Addressinp'+id).css('display','none');
			$('#ContactDatainp'+id).css('display','none');
			$('#editinp'+id).css('display','none');
		 }
		 </script> 
        
        <!-----------------------------> 
         <!-- Reeja Soni 22-5-17 end--> 
        <!----------------------------->
        
      </div>
    </form>
    <form id="finaldecission" name="finaldecission" method="post" action="<?PHP echo base_url().'inquiries/createcontract/'.$applicantid.'/'.$quotationsDataId ?>" enctype="multipart/form-data">
      <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $applicantid;?>" />
      <input type="hidden" name="quotationsDataId" id="quotationsDataId" value="<?php echo $quotationsDataId;?>" />
      <input type="hidden" name="id" id="id" value="<?php echo $rowss->id;?>" />
      <div class="col-md-12" style="background-color: #CDEB8B; padding-bottom: 9px; padding-top: 9px; float:right;">
        <p><b>اتفاقية الصيانة
          الدفعة: الثانية والعشـرين   تسلسل (50)
          بتمويل من الهيئة العمانية للأعمال الخيرية </b></p>
        <div class="col-md-12"><span class="textclss">انه في يوم الموافق </span><input type="text" class="form-control datepicker req" name="agreement_date" id="agreement_date" style="width:15%;" value="<?php echo $rowss->agreement_date;?>"/> <span style="margin-top:206px;">م الاتفاق بين الطرفين الأول والثاني على إجراء أعمال الصيانة تحت إشراف الهيئة العمانية للأعمال الخيرية وذلك حسب الشروط والبيانات التالية :</span></div>
        <div class="col-md-3"  style="margin:5px; padding:5px; border:1px dashed #FFFFFF; border-radius:15px;">
          <div style=" text-align:center;"><b  >(1) ويعرف بالطرف الأول</b></div>
          <div class="col-md-12">
            <label class="text-warning">االمستفيده :</label>
            <input type="text" class="form-control req" name="beneficiary1" id="beneficiary1" value="<?php echo (isset($rowss->beneficiary1) ? $rowss->beneficiary1 :$application->fullname); ?>"  />
          </div>
          <div class="col-md-12">
            <label class="text-warning">العنــوان :</label>
            <input type="text" class="form-control req" name="title1" id="title1" value="<?php echo (isset($rowss->title1) ? $rowss->title1:$application->address);// ?> "  />
          </div>
          <div class="col-md-12">
            <label class="text-warning">الهاتــف :</label>
            <input type="text" class="form-control req" name="phone1" id="phone1" value="<?php echo (isset($rowss->hometelephone) ? $rowss->hometelephone :$rowss->phone1); ?>"  />
          </div>
          <div class="col-md-12">
            <label class="text-warning">النقــــال :</label>
            <input type="text" class="form-control req" name="mobile1" id="mobile1" value="<?php echo arabic_date(implode('',json_decode($application->extratelephone,TRUE)));//$rowss->mobile1 ?>"  />
          </div>
          <br/>
          <br/>
        </div>
        <div class="col-md-3"  style="margin:5px; padding:5px; border:1px dashed #FFFFFF; border-radius:15px;">
          <div style=" text-align:center;"><b  >(2)ويعرف بالطرف الثاني</b></div>
          <div class="col-md-12">
            <label class="text-warning">اسم الشركة :</label>
            <input type="text" class="form-control req" name="company2" id="company2" value="<?php echo $quotation_detail->company;//$rowss->company2 ?>"  />
          </div>
          <div class="col-md-12">
            <label class="text-warning">العنوان :</label>
            <input type="text" class="form-control req" name="title2" id="title2" value="<?php echo $quotation_detail->address;//$rowss->title2 ?>"  />
          </div>
          <div class="col-md-12">
            <label class="text-warning">الهاتــف :</label>
            <input type="text" class="form-control req" name="phone2" id="phone2" value="<?php echo $quotation_detail->phone;//$rowss->phone2 ?>"  />
          </div>
          <div class="col-md-12">
            <label class="text-warning">الفـاكس :</label>
            <input type="text" class="form-control req" name="fax2" id="fax2" value="<?php echo $quotation_detail->fax;//$rowss->fax2 ?>"  />
          </div>
          <br/>
          <br/>
        </div>
        <div class="col-md-5"  style="margin:5px; padding:5px; border:1px dashed #FFFFFF; border-radius:15px;">
          <div style=" text-align:center;"><b  >(3)وتعرف بالطرف الثالث</b></div>
          <div class="col-md-12">
            <label class="text-warning"> الممول :</label>
            <input type="text" class="form-control req" name="fundedby3" id="fundedby3" value="<?php if($rowss->fundedby3 !="") echo $rowss->fundedby3;else echo "الهيئة العمانية للأعمال الخيرية"; ?>"  />
          </div>
          <div class="col-md-12">
            <label class="text-warning">عنوانها :</label>
            <input type="text" class="form-control req" name="title3" id="title3" value="<?php if($rowss->title3 !="") echo $rowss->title3; else echo "سلطنة عمان- مسقط، ص"; ?>"  />
          </div>
          <div class="col-md-6">
            <label class="text-warning">. ب:</label>
            <input type="text" class="form-control req" name="b3" id="b3" value="<?php if($rowss->b3 !="") echo $rowss->b3; else echo "1998"; ?>"  />
          </div>
          <div class="col-md-6">
            <label class="text-warning"> الرمـز البريـدي :</label>
            <input type="text" class="form-control req" name="postalcode3" id="postalcode3" value="<?php if($rowss->postalcode3 !="") echo $rowss->postalcode3; else echo "112 روي"; ?>"  />
          </div>
          <div class="col-md-6">
            <label class="text-warning">هاتـــف :</label>
            <input type="text" class="form-control req" name="tel3" id="tel3" value="<?php if($rowss->tel3 !="") echo $rowss->tel3; else echo "+96824297951"; ?>"  />
          </div>
          <div class="col-md-6">
            <label class="text-warning">فاكـــس :</label>
            <input type="text" class="form-control req" name="fax3" id="fax3" value="<?php if($rowss->fax3 !="") echo $rowss->fax3; else echo "+96824487997"; ?>"  />
          </div>
          <b> المساهمة في تكاليف أعمال الصيانة المذكورة في (البند رقم 7) بالمبلغ المذكـور في ( البند رقم 5 ).</b> </div>
        <div class="col-md-11"  style="margin:20px; padding:10px; border:1px dashed #FFFFFF; border-radius:15px; float:right;">
          <div style=" text-align:center; margin-bottom:10px;"><b  >(4) التفاصيل الجغرافية لموقع الصيانة</b></div>
          <div class="col-md-12"> <span class="textclss">يتم تنفيذ المشروع على قطعة الأرض</span>
            <input type="text" class="form-control textclss req" name="housing4" id="housing4" value="<?php echo $rowss->housing4; ?>" placeholder="سكنية" style="width:120px;"  />
            <span class="textclss"> رقم </span>
            <input type="text" class="form-control textclss req" name="landnumber4" id="landnumber4" value="<?php echo $rowss->landnumber4; ?>" placeholder="سكنية رقم" style="width:120px;"  />
            <span class="textclss"> بالمربع </span>
            <input type="text" class="form-control textclss req" name="square4" id="square4" value="<?php echo $rowss->square4; ?>" placeholder="هـ" style="width:120px;"  />
            <span class="textclss">الكائنة في ولاية </span>
            <input type="text" class="form-control textclss req" name="state4" id="state4" value="<?php echo $rowss->state4 ?>" placeholder="في ولاية" style="width:120px;"  />
            <span class="textclss">بلدة </span>
            <input type="text" class="form-control textclss req" name="town4" id="town4" value="<?php echo $rowss->town4 ?>" placeholder="بلدة" style="width:120px;"  />
            <span class="textclss"> والبالغ مساحته </span> <br style="clear:both;"/>
            <input type="text" class="form-control textclss req" name="squarearea4" id="squarearea4" value="<?php echo $rowss->squarearea4; ?>" placeholder="مساحته" style="width:120px;"  />
            <span class="textclss"> مترا مربعا،والمسجلة باسم /</span>
            <input type="text" class="form-control textclss req" name="registeredas4" id="registeredas4" value="<?php echo $rowss->registeredas4; ?>" placeholder="المسجلة باسم" style="width:120px;"  />
            <br/>
            <span class="textclss">.يلتزم الطرفان الأول والثاني (المستفيد والمقاول) البناء على قطعة الأرض المحددة في (البند رقم 4)، ولايجوز تغيير الموقع الذي تمت فيه المعاينة الا بموافقة خطية من الطرف الثالث ( الممول ) وفي حالة ثبوت أي تغيير في الموقع دون علم أو موافقة الهيئة في التغيير فمن حق الهيئة عدم الالتزام بهذه الاتفاقية والغائها دون تحمل الهيئة أية مسؤولية قانونية او الالتزام بقيمة المساعدة  . </span> </div>
        </div>
        <div class="col-md-11"  style="margin:20px; padding:10px; border:1px dashed #FFFFFF; border-radius:15px; float:right;">
          <div style=" text-align:center; margin-bottom:10px;"><b  >(5) التفاص</b></div>
          <div class="col-md-12"> <span class="textclss">تم الاتفاق بين الطرفين الأول والثاني على القيام بالأعمال المذكورة لاحقا في (البند رقم 7) بقيمة إجمالية وقدرها </span>
            <input type="text" class="form-control textclss req" name="total5" id="total5" value="<?php echo (isset($rows->total5) ? $rowss->total5 : $quotation_detail->total); ?>" placeholder="بقيمة إجمالية" style="width:120px;"  />
            <span class="textclss">ر.ع </span>
            <?php
			if($rowss->total5)
			{
				$totalintext5	=	$rowss->total5;
			}
			else
			{
				$totalintext5	=	$quotation_detail->total;
			}
				
			?>
            <?php //$number_format->format($totalintext5);?>
            <input type="text" class="form-control textclss req" name="totalintext5" id="totalintext5" value="<?php echo (isset($rowss->totalintext5) ? $rowss->totalintext5: NULL); ?>" placeholder=" ريال عماني فقط لاغير" style="width:300px;"  />
            <br style="clear:both;"/>
            <span class="textclss">قيمة مساهمة الهيئة في هذا المشروع ( </span>
            <input type="text" class="form-control textclss req" name="prjtotal5" id="prjtotal5" value="<?php echo (isset($rowss->prjtotal5) ? $rowss->prjtotal5 : $decission->decission_amount); ?>" placeholder="بقيمة إجمالية" style="width:120px;"  />
            <?php
			if($rowss->prjtotal5)
			{
				$prjtotalintext5	=	$rowss->prjtotal5;
			}
			else
			{
				$prjtotalintext5	=	$decission->decission_amount;
			}
				
			?>
            <?PHP //$number_format->format($prjtotalintext5);?>
            <span class="textclss"> ر.ع) بالحروف </span>
            <input type="text" class="form-control textclss req" name="prjtotalintext5" id="prjtotalintext5" value="<?php echo (isset($rowss->prjtotalintext5) ? $rowss->prjtotalintext5: NULL); ?>" placeholder=" ريال عماني فقط لاغير" style="width:300px;"  />
            <span class="textclss"> عماني</span>
            <input type="text" class="form-control textclss req" name="other5" id="other5" value="<?php echo $rowss->other5; ?>" placeholder="فقط لا غير" style="width:300px; display:none;"  />
            <br style="clear:both;"/>
            <span class="textclss"> التزام الطرف الثاني ( الشركة المنفذه بالمساهمة مع صاحبة المنزل بالمبلغ المتبقي ) </span> <br style="clear:both;"/>
            <span class="textclss">يتم تسديد قيمة مساهمة الهيئة على النحو الآتي : -</span> <br style="clear:both;"/>
            <ol>
              <li style="float:right;"> <span class="textclss">الدفعة الأولى : بقيمة : ( </span>
                <input type="text" class="form-control textclss req" name="firstpayment5" id="firstpayment5" value="<?php echo (isset($rowss->firstpayment5) ? $rowss->firstpayment5 : $percentage_amount); ?>" placeholder="الدفعة الأولى" style="width:80px;"  />
                            <?php
			if($rowss->firstpayment5)
			{
				$firstpaymenttext5	=	$rowss->firstpayment5;
			}
			else
			{
				$firstpaymenttext5	=	$percentage_amount;
			}
				
			?>
            <?PHP //$number_format->format($firstpaymenttext5);?>
                <span class="textclss"> ر.ع) بالحروف </span>
                <input type="text" class="form-control textclss req" name="firstpaymenttext5" id="firstpaymenttext5" value="<?php echo (isset($rowss->firstpaymenttext5) ? $rowss->firstpaymenttext5 : NULL); ?>" placeholder="بالحروف" style="width:150px;"  />
                <span class="textclss">. تدفع بعد إنجاز نصــف الأعـمـــــال المطلوبة    50 % .</span></li>
              <li style="float:right;"><span class="textclss">الدفعة الأخيرة : بقيمة : ( </span>
                <input type="text" class="form-control textclss req" name="lastpayment5" id="lastpayment5" value="<?php echo $rowss->lastpayment5; ?>" placeholder="الدفعة الأخيرة" style="width:80px;"  />
                <span class="textclss">ر.ع) </span>
                <input type="text" class="form-control textclss req" name="lastpaymenttxt5" id="lastpaymenttxt5" value="<?php echo $rowss->lastpaymenttxt5; ?>" placeholder="بالحروف" style="width:80px;"  />
                <span class="textclss"> تـدفـع بـعـد الانتهـاء مـن جميـع الأعمال وتسليـم المـوقـع</span></li>
            </ol>
            <br style="clear:both;"/>
            <div class="col-md-12 form-group"> <a class="add_multifields" href="#_" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> </div>
            <?php if(!empty($percent_amounts)):?>
            	<?php foreach($percent_amounts	as $amount):?>
                	<div class="multiple-block">
                  
                  <div class="col-md-4 form-group">
                    <label class="text-warning">تدفع بعد إنجاز نصــف الأعـمـــــال المطلوبة :</label>
                    <input name="percentage[<?php echo $amount->index_num;?>]"  placeholder="تدفع بعد إنجاز نصــف الأعـمـــــال المطلوبة" id="percentage<?php echo $amount->index_num;?>" type="text" class="form-control red" value="<?php echo $amount->percentage;?>" onKeyUp="only_numeric(this);">
                  </div>
                  <div class="col-md-4 form-group">
                    <label class="text-w
                    arning">كمية  :</label>
                    <input name="amount[<?php echo $amount->index_num;?>]"  placeholder="كمية" id="amount<?php echo $amount->index_num;?>" type="text" class="form-control red" onKeyUp="only_numeric(this);" value="<?php echo $amount->amount;?>" readonly >
                  </div>
                  <div class="col-md-4 form-group">
                    <label class="text-warning">ملاحظات  :</label>
                    <input name="notes[<?php echo $amount->index_num;?>]"  placeholder="ملاحظات" id="notes" type="text" class="form-control req" value="<?php echo $amount->notes;?>">
                  </div>
                </div>
                <br clear="all"/>
                <script>
                $( "#percentage<?php echo $amount->index_num;?>" ).blur(function() {
				var field_val			=	(<?php echo $total_divs;?>	-	1)
				var percentage			=	$(this).val();
				//var total_amount		=	$("#amount"+field_val).val();
				var total_amount		=	$("#prjtotal5").val();

				var percentage_amount	=	(Number(percentage) / 100) * Number(total_amount);
			
				$("#amount<?php echo $amount->index_num;?>").val(percentage_amount);
			});
                </script>
                <?php endforeach;?>
            <?php else:?>
            <div class="multiple-block">
                  
                  <div class="col-md-4 form-group">
                    <label class="text-warning">تدفع بعد إنجاز نصــف الأعـمـــــال المطلوبة :</label>
                    <input name="percentage[1]"  placeholder="تدفع بعد إنجاز نصــف الأعـمـــــال المطلوبة" id="percentage1" type="text" class="form-control red" onKeyUp="only_numeric(this);">
                  </div>
                  <div class="col-md-4 form-group">
                    <label class="text-warning">كمية  :</label>
                    <input name="amount[1]"  placeholder="كمية" id="amount1" type="text" class="form-control red" onKeyUp="only_numeric(this);" readonly>
                  </div>
                  <div class="col-md-4 form-group">
                    <label class="text-warning">ملاحظات  :</label>
                    <input name="notes[1]"  placeholder="ملاحظات" id="notes" type="text" class="form-control req">
                  </div>
                </div>
            <?php endif;?>    
                
                <div class="col-md-12 form-group" id="multifields"></div>
                
            <br style="clear:both;"/>
            <span class="textclss">يتم تسديد الدفعات حسبما هو مبين في (البند رقم 10)</span> </div>
        </div>
        <div class="col-md-11"  style="margin:20px; padding:10px; border:1px dashed #FFFFFF; border-radius:15px; float:right;">
          <div style=" text-align:center; margin-bottom:10px;"><b  >(6) التفاصيل الزمنية لإنجاز المشروع:</b></div>
          <div class="col-md-12"> <span class="textclss">يتم تنفيذ المشروع في مدة زمنية قدرها (</span>
            <input type="text" class="form-control textclss req" name="duration6" id="duration6" value="<?php echo $rowss->duration6; ?>" placeholder="أشهر" style="width:120px;"  />
            <span class="textclss">) أشهر، ابتداءً من تاريخ أمر التشغيل . </span> <br style="clear:both;"/>
            <span class="textclss">تعتبر المدة الزمنية المحددة في (البند رقم 6)  بمثابة تعهد من الطرف الثاني (المقاول) بالانتهاء من جميع الأعمال المحددة خلال المدة المتفق عليها وتسليم العمل بالكامل، وفي حالة عدم بدء التنفيذ خلال المدة المذكورة تلغى هذه الاتفاقية تلقائياً ولا تتحمل الهيئة أية مسؤولية أو التزام بقيمة مساهمتها.</span> </div>
        </div>
        <div class="col-md-11"  style="margin:20px; padding:10px; border:1px dashed #FFFFFF; border-radius:15px; float:right;">
          <div style=" text-align:center; margin-bottom:10px;"><b  >(7) تفاصيل الأعمال المطلوب تنفيذها:</b></div>
          <div class="col-md-12">
            <table class="table table-bordered table-striped" style="padding:10px !important;" >
              <thead>
                <tr role="row" style="background-color: #029625; color:#fff !important;">
                  <th>أولا:</th>
                  <th> الأعمال المتفق عليها بين الطرفين هي:</th>
                  <th>المبلغ (ر.ع)</th>
                </tr>
              </thead>
              <tbody>
                <tr style="background:none;" >
                  <td>1</td>
                  <td><span class="textclss">النـــــــــوع:</span>
                    <input type="text" class="form-control textclss req" name="type61" id="type61" value="<?php echo $rowss->type61 ?>" placeholder="النـــــــــوع" style="width:120px;"  />
                    <br style="clear:both;"/>
                    <span class="textclss">المواصفات:
                    <ul>
                      <li>تركيب بلاط لغرفة النوم + متر واحد على الجدار الداخلي وعدد 3 ابواب + نوافذ للصاله وصبغ الغرفة وتركيب فور سيلنج للسقف الداخلي والأناره الخاصة بالسقف الداخلي . </li>
                      <li>تركيب بلاط للصالة + متر واحد على الجدار الداخلي وعدد 3 ابواب + نوافذ وصبغ الصالة وتركيب فور سيلنج للسقف الداخلي والأناره الخاصة بالسقف الداخلي .</li>
                      <li>عمل سيراميك لألرضية المطبخ والجدران وصبغ سقف الغرفة الرئيسية والصالة والمطبخ والصبغ من الخارج للسور المنزل </li>
                    </ul>
                    </span></td>
                  <td><input type="text" class="form-control textclss price req" name="amount61" id="amount61" value="<?php echo $rowss->amount61; ?>" placeholder="المبلغ" style="width:120px;"  /></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td><span class="textclss">النـــــــــوع</span>
                    <input type="text" class="form-control textclss req" name="type62" id="type62" value="<?php echo $rowss->type62; ?>" placeholder="النـــــــــوع" style="width:120px;"  />
                    <br style="clear:both;"/>
                    <span class="textclss">المواصفات:</span>
                    <textarea class="form-control textclss req" name="specification62" id="specification62" value="<?php echo $rowss->specification62; ?>" placeholder="المواصفات:"   rows="10" cols="20" ></textarea></td>
                  <td></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td><span class="textclss">النـــــــــوع:</span>
                    <input type="text" class="form-control textclss req" name="type63" id="type63" value="<?php echo $rowss->type63; ?>" placeholder="النـــــــــوع" style="width:120px;"  />
                    <br style="clear:both;"/>
                    <span class="textclss">المقــــــاس :</span>
                    <input type="text" class="form-control textclss req" name="size63" id="size63" value="<?php echo $rowss->size63; ?>" placeholder="المقــــــاس " style="width:120px;"  />
                    <span class="textclss"> × </span>
                    <input type="text" class="form-control textclss req" name="metre63" id="metre63" value="<?php echo $rowss->metre63; ?>" placeholder="متر" style="width:120px;"  />
                    <span class="textclss">متر :</span> <br style="clear:both;"/>
                    <span class="textclss">المواصفات :</span>
                    <textarea class="form-control textclss req" name="specification63" id="specification63" value="<?php echo $rowss->specification63; ?>" placeholder="المواصفات"   rows="10" cols="20" ></textarea></td>
                  <td></td>
                </tr>
                <tr style="background-color: #029625; color:#fff !important;">
                  <td>ثانيا:</td>
                  <td>أعمال مضافة للاتفاقية (لا تدخل ضمن مسؤولية الطرف الثالث) هي:</td>
                  <td>المبلغ (ر.ع)</td>
                </tr>
                <tr>
                  <td>1</td>
                  <td><textarea class="form-control textclss req" name="specification64" id="specification64"  placeholder="المواصفات"   rows="3" cols="20" ><?php echo $rowss->specification64; ?></textarea></td>
                  <td><input type="text" class="form-control textclss price req" name="amount64" id="amount64" value="<?php echo $rowss->amount64; ?>" placeholder="المبلغ" style="width:120px;"  /></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td><textarea class="form-control textclss req" name="specification65" id="specification65"  placeholder="المواصفات"   rows="3" cols="20" ><?php echo $rowss->specification65 ?></textarea></td>
                  <td><input type="text" class="form-control textclss price req" name="amount65" id="amount65" value="<?php echo $rowss->amount65; ?>" placeholder="المبلغ" style="width:120px;"  /></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td><textarea class="form-control textclss price req" name="specification66" id="specification66"   placeholder="المواصفات"   rows="3" cols="20" ><?php echo $rowss->specification66; ?></textarea></td>
                  <td><input type="text" class="form-control textclss price req" name="amount66" id="amount66" value="<?php echo $rowss->amount66; ?>" placeholder="المبلغ" style="width:120px;"  /></td>
                </tr>
                <tr style="background-color:#FAC7F9;">
                  <td colspan="2">إجمالي قيمة الأعمال التي تنفذ بالريال العماني:</td>
                  <td><input type="text" class="form-control textclss total_price req" name="amount67" id="amount67" value="<?php echo $rowss->amount67; ?>" placeholder="المبلغ" style="width:120px;"  />
                    <span class="textclss">ر.ع</span></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-md-11"  style="margin:20px; padding:10px; border:1px dashed #FFFFFF; border-radius:15px; float:right;">
          <div style=" text-align:center; margin-bottom:10px;"><b  >(8) وفي حالة التأخير عن المدة المحددة (<font style="color:#060;" >بالبند رقم 6</font>) تحسب غرامة تأخير على المقاول بنسبة 5% من اجمالي مساهمة الهيئة، واما في حالة عدم البدء في العمل خلال ثلاثة أشهر يجمد مفعول الاتفاقية ويستبعد اسم المستفيد من القائمة بقرار من لجنة المشاريع بالهيئة دون تحمل الهيئة أية مسؤولية عن ذلك . </b></div>
          <br style="clear:both;"/>
        </div>
        <div class="col-md-11"  style="margin:20px; padding:10px; border:1px dashed #FFFFFF; border-radius:15px; float:right;">
          <div style=" text-align:center; margin-bottom:10px;"><b>اتفق الطرفان على الشروط المذكورة وتم التوقيع على هذه الاتفاقية، وتعتبر سارية المفعول اعتباراً من تاريخه.</b></div>
          <br style="clear:both;"/>
          <div class="col-md-6"> <span class="textclss">توقيع وختم الطرف الثاني ( المقاول )</span>
            <input type="text" class="form-control textclss req" name="sign2party8" id="sign2party8" value="<?php echo $rowss->sign2party8; ?>" placeholder=" توقيع وختم"  />
          </div>
          <div class="col-md-6"> <span class="textclss">توقيع الطرف الأول ( المستفيد )</span>
            <input type="text" class="form-control textclss req" name="sign1party8" id="sign1party8" value="<?php echo $rowss->sign1party8; ?>" placeholder="توقيع وختم"   />
          </div>
          <div class="col-md-6"> <span class="textclss">توقيع وختـــم سعادة الوالـــــي</span>
            <input type="text" class="form-control textclss req" name="Signature38" id="Signature38" value="<?php echo $rowss->Signature38; ?>" placeholder=" توقيع وختم"  />
          </div>
          <div class="col-md-6"> <span class="textclss">توقيع وختم شيخ المنطــــقة</span>
            <input type="text" class="form-control textclss req" name="Signature48" id="Signature48" value="<?php echo $rowss->Signature48 ?>" placeholder="توقيع وختم"   />
          </div>
        </div>
        <div class="col-md-11"  style="margin:20px; padding:10px; border:1px dashed #FFFFFF; border-radius:15px; float:right;">
          <div style=" text-align:center; margin-bottom:10px;"><b>(9) للاستعمال الرسمي فقط:</b></div>
          <br style="clear:both;"/>
          <div style=" text-align:center; margin-bottom:10px;"><b>اعتماد الهيئة العمانية للأعمال الخيرية ( الطرف الثالث ) </b></div>
          <br style="clear:both;"/>
          <div class="col-md-6"> <span class="textclss">الـــخـــتـــــــم</span>
            <input type="text" class="form-control textclss req" name="seal9" id="seal9" value="<?php echo $rowss->seal9; ?>" placeholder="الـــخـــتـــــــم"  />
            <span class="textclss">الدفعة رقم (30) تسلسل (50)تحريراً في : <?php echo date('Y/m/d');?> م </span> </div>
          <div class="col-md-6"> <span class="textclss">التــــــــوقـــيع </span>
            <input type="text" class="form-control textclss req" name="signature9" id="signature9" value="<?php echo $rowss->signature9; ?>" placeholder="التــــــــوقـــيع "  />
          </div>
        </div>
        <div class="col-md-11"  style="margin:20px; padding:10px; border:1px dashed #FFFFFF; border-radius:15px; float:right;">
          <div style=" text-align:center; margin-bottom:10px;"><b>أولا: نظام الدفعات:</b></div>
          <br style="clear:both;"/>
          <ol>
            <li style="float:right;"> يتم تسديد الدفعات لحساب المقاول من الممول بعد تقديم فاتورة الدفعة مرفقة بصور فوتوغرافية للموقع واعتماد مكتب والي المنطقة باستحقاق الدفعة.</li>
          </ol>
        </div>
        <div class="col-md-11"  style="margin:20px; padding:10px; border:1px dashed #FFFFFF; border-radius:15px; float:right;">
          <div style=" text-align:center; margin-bottom:10px;"><b>ثانيا: ملاحظات عامة: </b></div>
          <br style="clear:both;"/>
          <ol>
            <li style="float:right;">يتم تزويد الطرف الثالث (الهيئة) بصور من المستندات الرسمية لكل من (البطاقة الشخصية/ جواز السفر، ملكية الارض، شهادة المقاول، موافقة مكتب والي المنطقة على اسناد العمل على المقاول).</li>
            <li>يلتزم الطرفان الأول والثاني بجميع البنود الواردة بالاتفاقية والمكونة من (4) صفحات. </li>
            <li>مستندات وشروط عرض السعر تعتبر جزءًا من هذه الاتفاقية وملزمة للطرفين.</li>
            <li>يتحمل الطرف الثاني (المقاول) جميع المسؤوليات عن سير العمل والموقع والعمال خلال فترة التنفيذ.</li>
            <li>الممول (الهيئة) لا يتحمل مسؤولية وجود أية أخطاء في البناء ، وتكون الأمور الفنية وتأمين الموقع مــن مسؤولية الطرف الثاني (المقاول)، والمسؤولية الإدارية والاشراف مسؤوليــة الطرف الاول (المستفيد) وله ان يعين من يشاء.</li>
            <li>في حالة تأخر المستفيد أو المقاول عن إتمام الإجراءات واستكمال المستندات أو دفع قيمة المساهمة أو البدء في تنفيذ العمل وبعد مرور مدة ثلاثة أشهر من تاريخ التوقيع على العقد سيتم إلغاء هذا العقد تلقائياً دون تحمل الممول أية مسؤولية (بند رقم 7).</li>
            <li>يعتبر اعتماد وتصديق الهيئة على هذه الاتفاقية بمثابة موافقة رسمية وأمر تشغيل في بدء العمـــل.</li>
          </ol>
          <br style="clear:both;"/>
          <div class="col-md-6"> <span class="textclss"> امر تشغيل لبدء عمل الصيانة </span>
            <input type="text" class="form-control textclss req" name="maintenance10" id="maintenance10" value="<?php echo $rowss->maintenance10; ?>" placeholder="امر تشغيل لبدء عمل الصيانة"  />
          </div>
          <div class="col-md-6"> <br style="clear:both;"/>
            <br style="clear:both;"/>
            <span class="textclss"> التاريخ: <?php echo date('d/m/Y');?> م</span> </div>
          <br style="clear:both;"/>
          <span class="textclss"> الفاضــل مدير شركـــة / نوخذة المحيط للتجارة والمقاولات 	            المحترم</span> <br style="clear:both;"/>
          <span class="textclss"> ولاية: المصنعة             هاتف: 69167
          تحية طيبة وبعد،،، </span> <br style="clear:both;"/>
          <span class="textclss"> بناءا على عرض السعر المقدم من قبلكم بتاريخ    /    /2017 م للقيام باعمال الصيانة المذكوره ادناه ، يسرنا أن نفيدكم بموافقة الهيئة العمانية للاعمال الخيرية المساهمة في تكاليف تنفيذ الاعمال المبنية وبالقيمة المذكوره .</span> <br style="clear:both;"/>
          <div class="col-md-12">
            <table class="table table-bordered table-striped" style="padding:10px !important;" >
              <tbody>
                <tr style="background:none;" >
                  <td>اسـم المشــــروع:</td>
                  <td>برنامج الصيانة</td>
                  <td>رقم الدفعة:</td>
                  <td><input type="text" class="form-control textclss req" name="sequence11" id="sequence11" value="<?php echo $rowss->sequence11; ?>" placeholder="الواحد والعشريـن   تسلسل 42"   /></td>
                </tr>
                <tr style="background:none;" >
                  <td>مساهمة الهيئة :</td>
                  <td colspan="3"><input type="text" class="form-control textclss req" name="Contribution11" id="Contribution11" value="<?php echo $rowss->Contribution11; ?>" placeholder=" 3000  ثلاثة الاف   ريال عماني"    /></td>
                </tr>
                <tr style="background:none;" >
                  <td>اسـم المستفيــد :</td>
                  <td colspan="3"><input type="text" class="form-control textclss req" name="Beneficiary11" id="Beneficiary11" value="<?php echo $rowss->Beneficiary11; ?>" placeholder=" اسـم المستفيــد"    /></td>
                </tr>
                <tr style="background:none;" >
                  <td>الـولايــــــــــــة:</td>
                  <td><input type="text" class="form-control textclss req" name="state11" id="state11" value="<?php echo $rowss->state11; ?>" placeholder="الـولايــــــــــــة"  /></td>
                  <td>البلــــــدة:</td>
                  <td><input type="text" class="form-control textclss req" name="town11" id="town11" value="<?php echo $rowss->town11; ?>" placeholder="البلــــــدة" /></td>
                </tr>
                <tr style="background:none;" >
                  <td rowspan="2" >مــــدة تنفيــذ الاعمال المعتمدة:</td>
                  <td rowspan="2"><input type="text" class="form-control textclss req" name="duration11" id="duration11" value="<?php echo $rowss->duration11; ?>" placeholder="مــــدة تنفيــذ الاعمال المعتمدة"   /></td>
                  <td>تبــدا مـن:</td>
                  <td><input type="text" class="form-control textclss req" name="town211" id="town211" value="<?php echo $rowss->town211; ?>" placeholder="البلــــــدة"  /></td>
                </tr>
                <tr style="background:none;" >
                  <td>تنتهي في :</td>
                  <td><input type="text" class="form-control textclss req" name="Endin11" id="Endin11" value="<?php echo $rowss->Endin11; ?>" placeholder="تنتهي في"   /></td>
                </tr>
              </tbody>
            </table>
          </div>
          <br style="clear:both;"/>
          <span class="textclss">الاعمال المعتمدة من لجنة المشاريع بالهيئة:</span>
          <input type="text" class="form-control textclss req" name="projectcommittee11" id="projectcommittee11" value="<?php echo $rowss->projectcommittee11; ?>" placeholder="الاعمال المعتمدة من لجنة المشاريع بالهيئة"   />
          <div style=" text-align:center; margin-bottom:10px;"><b>يرجى البدء في تنفيذ العمل وفقا للبيانات المذكوره أعلاه</b></div>
          <span class="textclss">ملاحظة:</span> <br style="clear:both;"/>
          <ol>
            <li style="float:right;">في حالة وجود اي غموض اثناء التنفيذ يرجى مراجعة قسم المشاريع بالهيئة لاخذ الموافقة الخطية      على ذلك .</li>
            <li style="float:right;">في حالة عدم تسليم الموقع حسب المدة المحددة في الاتفاقية ، سيتم وقف صرف المستحقات لحين عرض الطلب على اللجنة الداخلية المختصة لاتخاذ الاجراء اللازم .</li>
          </ol>
          <br style="clear:both;"/>
          <div class="col-md-4"> <span class="textclss">توقيع وختم المقاول</span>
            <input type="text" class="form-control textclss req" name="signing11" id="signing11" value="<?php echo $rowss->signing11; ?>" placeholder="توقيع وختم المقاول"  />
          </div>
          <div class="col-md-4"> <span class="textclss"> رئيس قسم المشاريع</span>
            <input type="text" class="form-control textclss req" name="head11" id="head11" value="<?php echo $rowss->head11; ?>" placeholder=" رئيس قسم المشاريع"  />
          </div>
          <div class="col-md-4"> <span class="textclss"> اعتماد الرئيس التنفيذي</span>
            <input type="text" class="form-control textclss req" name="chairman11" id="chairman11" value="<?php echo $rowss->chairman11; ?>" placeholder=" اعتماد الرئيس التنفيذي"  />
          </div>
          <div class="form-group col-md-4">
       <label class="text-warning">تحميل</label>
       <input style="border: 0px !important; color: #d09c0d;" type="file" name="document" id="document" title='تحميل'>
      </div>
          <br style="clear:both;"/>
          <span class="textclss">نسخة الى: -  قسم الارشيف بالهيئة
          الملف الخاص </span> </div>
        <div class="form-group col-md-6">
          <button type="button" id="save_service" name="save_service" class="btn btn-success">حفظ</button>
        </div>
      </div>
    </form>
  </div>
</section>

<?php $this->load->view('common/footer'); ?>
</body>
</html><script>
function quotations_maintenance(applicantid){
	$.ajax({
			url: config.BASE_URL+'inquiries/quotations_maintenance/',
			type: "POST",
			data:{'applicantid':applicantid },
			dataType: "html",
			success: function(response)
			{	
				$('#quotations_maintenance').html(response);
			 }
		});	
}
quotations_maintenance('<?php echo $applicantid;?>');
 $(document).ready(function(){
	 
	 /*$(document).on("click", ".arabic-amount", function() {
	
	//var amount	=	parseInt($(".arabic-amount").val());
	var amount	=	500;
	var number_format	=	1;
	
	number_format	=	<?php //echo $number_format->format(200); ?>;
	console.log(<?php //echo $number_format->format(200); ?>);return;
    $("#totalintext5").val(number_format);

});*/

	$('#save_service').click(function () {
		check_my_session();
        $('#finaldecission .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#finaldecission .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#finaldecission .parsley-error').length;

        ht += '</ul>';
		
        //if (redline <= 0) {  
		  if (redline = true) {        
                $("#finaldecission").submit();
            
        }
        else 
		{
            show_notification(ht);
        }
    });
	
	/****************************************/
		$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
		});
<?php if(empty($percent_amounts)):?>
/*********************************************************************/
$( "#percentage1" ).blur(function() {
	var percentage			=	$(this).val();
	var total_amount		=	'<?php echo $decission->decission_amount?>';
	var percentage_amount	=	(Number(percentage) / 100) * Number(total_amount);

	$("#amount1").val(percentage_amount);
});		
/*********************************************************************/
<?php endif;?>
$(".add_multifields").click(function(){
			
		var total_divs	=	$(".multiple-block").length+1;
		
		$('#finaldecission .red').removeClass('parsley-error');
		var form_action = $('#finaldecission').attr('action');
		var ht = '<ul>';
		$('#finaldecission .red').each(function(index, element)
			{
				if($(this).val()=='')
				{
					$(this).addClass('parsley-error');
					ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
				}
			});
			var redline = $('#finaldecission .parsley-error').length;
			ht += '</ul>';
			//var redline	=	5;
			//alert(redline);
			//return;
			if (redline <= 0) {
				
								$.ajax({
					url: config.BASE_URL+'inquiries/add_multifields',
					type:"POST",
					data:{total_divs:total_divs},
					dataType:"html",				
					success: function(msg)
					{
						$("#multifields").before(msg);
					}
				  });
			} else {
				
				show_notification_error_end(ht);
			}
	});
});
function delete_data_system()
{
	check_my_session();
	
    var data_id 	=	$('#delete_id').val();
    var action_url 	=	$('#action_url').val();
    var rowid 		=	'#' + data_id + '_durar_lm';
	
	
    var delete_data_from_system = $.ajax({
        url: action_url,
        dataType: "html",
        success: function (msg) 
		{
            show_notification('لقد تم حذف سجل');
            $('#deleteDiag').modal('hide');
			quotations_maintenance('<?php echo $applicantid;?>');
        }
    });
}
</script>
<script>
$(document).on("keyup", ".price", function() {

    //loop and add up every value from $(".price").val()
	var sum = 0;
    $(".price").each(function(){
        sum += +$(this).val();
    });
    $(".total_price").val(sum);

});
</script>
<style>
.textclss {
	float: right;
	margin-left: 5px;
	margin-bottom: 10px;
	padding-top: 5px;
}
</style>
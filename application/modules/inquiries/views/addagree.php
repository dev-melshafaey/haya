  <form action="" method="POST" id="frm_expence" name="frm_expence" action="<?php echo base_url()?>aid/addagreeSubmit" enctype="multipart/form-data" onsubmit="return uploadFiles();">

<div class="row col-md-12">
   <div class="col-md-2 form-group">
                <label class="text-warning">موافق على المنحة</label>
                <input type="radio" checked name="approval" data-id="approval" id="approval_Accepted" class="apstatus" value="Accepted" tabindex="2" onclick="validate(this.value);">
    </div>
     <div class="col-md-2 form-group">
                <label class="text-warning">رفض المنحة</label>
                <input type="radio"  name="approval" data-id="approval" id="approval_Rejected" class="apstatus" value="Rejected" tabindex="2" onclick="validate(this.value);">
    </div>
   
    <div class="row" id="accepted">
        <div class="form-group col-md-12">
          <label for="basic-input">المرفق</label>
          <input type="file" class="form-control " value="<?php echo $rows->attachment;?>" placeholder="المرفق" name="attachment[]" id="attachment"  multiple="multiple"/>
           <input type="hidden"  name="attachment_old" id="attachment_old" value="<?php echo $rows->attachment;?>"  />
       
        </div>
    </div>
     <div class="row" id="rejected" style="display:none;">
        <div class="form-group col-md-12">
         <label for="basic-input">المرفق</label>
          <input type="file" class="form-control " value="<?php echo $rows->attachment;?>" placeholder="المرفق" name="attachment[]" id="attachment"  multiple="multiple"/>
           <input type="hidden"  name="attachment_old" id="attachment_old" value="<?php echo $rows->attachment;?>"  />
      <label for="basic-input">السبب</label>
      <textarea  class="form-control"  placeholder="السبب" name="reason" id="reason" rows="10"  ><?php echo $rows->reason;?></textarea>
    </div>
    </div>
    
    <input type="hidden" name="apstatusId" id="apstatusId" value="<?php echo $rows->apstatusId;?>"/>
    <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $applicantid?>"/>
 
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
     <input type="submit" class="btn btn-success btn-lrg" name="submit"  id="save_expence" value="حفظ"  />
  </div>
</div>
 </form>
<script>
function validate(vals){
	if(vals =="Accepted"){
		$('#rejected').hide("slow");
		$('#accepted').show("slow");
	}
	else{
		$('#rejected').show("slow");
		$('#accepted').hide("slow");
	}
}

function uploadFiles()
{
	check_my_session();
        $('#frm_meeting .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_meeting .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_meeting .parsley-error').length;

        ht += '</ul>';
		if(document.frm_expence.approval.value =="Accepted"){
			if($('#attachment').val() ==""){
				$('#attachment').css('border','1px solid rgba(247,32,35,1.00)');
				redline++;
			}
			else{
				$('#attachment').css('border','1px solid #029625');
			}
		}
		else{
			if($('#reason').val() ==""){
				$('#reason').css('border','1px solid rgba(247,32,35,1.00)');
				redline++;
			}
			else{
				$('#reason').css('border','1px solid #029625');
			}
			
		}
        if (redline <= 0) {
	var form_data = new FormData(this); 
 $.ajax({ //ajax form submit
            url :  config.BASE_URL+'inquiries/addagreeSubmit',
            type: 'POST',
            data : form_data,
            dataType : "html",
            contentType: false,
            cache: false,
            processData:false,
			beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(msg)
		  {		
			  $('#ajax_action').hide();
			 $('#addingDiag').modal('hide');	  
			 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
			
				window.location.reload(); 
			 
		  }
        });
		}
		 else 
		{
            show_notification_error_end(ht);
        }
		return false;
		
}
 $(document).ready(function(){

$('form').on('submit', uploadFiles);
});

</script>
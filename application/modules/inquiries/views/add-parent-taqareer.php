<?php $segment	=	 $this->uri->segment(3);?>

<div class="row col-md-12">
  <form action="" method="POST" id="save_data_form2" name="save_data_form2">
    <div class="form-group col-md-6">
      <label for="basic-input">إسم</label>
      <input type="text" class="form-control req" name="list_name"  id="list_name"  placeholder="اسم القائمة"/>
    </div>

        <?php if($segment == ''):?>
    <div class="form-group col-md-3">
    <label for="basic-input">‎</label>
      <select name="list_type" id="list_type" class="form-control">
                        <option value="rules" <?php if($single_list->list_type	==	'rules'):?> selected="selected" <?php endif;?>> قواعد</option>
                        <option value="qualification" <?php if($single_list->list_type	==	'qualification'):?> selected="selected" <?php endif;?>>المؤهل</option>
                        <option value="nature_project_site" <?php if($single_list->list_type	==	'nature_project_site'):?> selected="selected" <?php endif;?>>‫طبيعة موقع المشروع‬‎</option>
                        <option value="nature_project" <?php if($single_list->list_type	==	'nature_project'):?> selected="selected" <?php endif;?>>‫طبيعة محل المشروع‬‎</option>
                        <option value="business_type" <?php if($single_list->list_type	==	'business_type'):?> selected="selected" <?php endif;?>>‫القطاع الإقتصادي‬‎</option>
                        <option value="activity_project" <?php if($single_list->list_type	==	'activity_project'):?> selected="selected" <?php endif;?>>‫نشاط المشروع‬‎</option>
                        <option value="project_employment" <?php if($single_list->list_type	==	'project_employment'):?> selected="selected" <?php endif;?>>‫التعمين‬‎</option>
                        <option value="project_type" <?php if($single_list->list_type	==	'qualification'):?> selected="selected" <?php endif;?>>‫نوع المشروع‬‎</option>
                       </select>
    </div>
    <?php else:?>
    <div class="form-group col-md-3">
      <label for="basic-input">‎</label>
      <select name="list_type" id="list_type" class="form-control">
						  <?php if($segment == 'rules'):?>
                             <option value="rules" <?php if($single_list->list_type	==	'rules'):?> selected="selected" <?php endif;?>>قواعد</option>
                          <?php endif;?>
                          <?php if($segment == 'qualification'):?>
                             <option value="qualification" <?php if($single_list->list_type	==	'qualification'):?> selected="selected" <?php endif;?>>المؤهل</option>
                          <?php endif;?>
                          <?php if($segment == 'nature_project_site'):?>
                             <option value="nature_project_site" <?php if($single_list->list_type	==	'nature_project_site'):?> selected="selected" <?php endif;?>>‫طبيعة موقع المشروع</option>
                          <?php endif;?>
                          <?php if($segment == 'nature_project'):?>
                             <option value="nature_project" <?php if($single_list->list_type	==	'nature_project'):?> selected="selected" <?php endif;?>>‫طبيعة محل المشروع‬‎</option>
                          <?php endif;?>
                          <?php if($segment == 'business_type'):?>
                             <option value="business_type" <?php if($single_list->list_type	==	'business_type'):?> selected="selected" <?php endif;?>>القطاع الإقتصادي‬‎</option>
                          <?php endif;?>
                          <?php if($segment == 'activity_project'):?>
                             <option value="activity_project" <?php if($single_list->list_type	==	'activity_project'):?> selected="selected" <?php endif;?>>‫نشاط المشروع</option>
                          <?php endif;?>
                          <?php if($segment == 'project_employment'):?>
                             <option value="project_employment" <?php if($single_list->list_type	==	'project_employment'):?> selected="selected" <?php endif;?>>التعمين‬‎</option>
                          <?php endif;?>
                          <?php if($segment == 'project_type'):?>
                             <option value="project_type" <?php if($single_list->list_type	==	'project_type'):?> selected="selected" <?php endif;?>>نوع المشروع</option>
                          <?php endif;?>
                      </select>
    </div>
    <?php endif;?>
    <div class="form-group col-md-3">
      <label for="basic-input">الوضع&lrm;‎</label>
      <select name="list_status" id="list_status" class="form-control">
        <option value="1">نشط</option>
        <option value="0">غير نشط</option>
      </select>
    </div>
    <input type="hidden" name="list_id" id="list_id" value="<?php echo $list_id;?>"/>
  </form>
</div>
<div class="row col-md-12">
      <div class="form-group  col-md-12">
        
        <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="submit_form();" value="حفظ" />
      </div>
    </div>

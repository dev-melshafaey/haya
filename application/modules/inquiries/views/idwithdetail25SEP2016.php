<?PHP
	$ah_applicant 						=	$_applicant_data['ah_applicant'];
	$ah_applicant_wife 					= 	$_applicant_data['ah_applicant_wife'];
	$ah_applicant_survayresult 			= 	$_applicant_data['ah_applicant_survayresult'];
	$ah_applicant_relation 				= 	$_applicant_data['ah_applicant_relation'];
	$ah_applicant_family 				= 	$_applicant_data['ah_applicant_family'];
	$ah_applicant_economic_situation 	= 	$_applicant_data['ah_applicant_economic_situation'];
	$ah_applicant_documents 			= 	$_applicant_data['ah_applicant_documents'];
	
?>
<?php $charity_type	=	 $this->haya_model->get_name_from_list($ah_applicant->charity_type_id); ?>
<?php //echo $charity_type;?>
<div class="col-md-12" id="myprint"  style="direction:rtl;">
  <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tr>
      <td colspan="4" class="customhr">بيانات طلب مساعدة</td>
    </tr>
    <tr>
      <td class="w25 right"><label class="text-warning">رقم الاستمارة: </label>
        <br />
        <strong><?PHP echo a_date(applicant_number($ah_applicant->applicantcode)); ?> </strong></td>
      <td class="w25 right"><label class="text-warning">جنسية: </label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->country_listmanagement); ?> </strong></td>
      <td class="w25 right"><label class="text-warning">نوع الطلب: </label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->charity_type_id); ?> </strong></td>
      <td class="w25 right"><label class="text-warning">الاسم الرباعي والقبيلة (عمر): </label>
        <br />
        <strong><?PHP echo $ah_applicant->fullname; ?> (<?PHP echo a_date($ah_applicant->applicant_age); ?>)</strong></td>
    </tr>
    <tr>
      <td class="w25 right"><label class="text-warning">التاريخ: </label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->registrationdate,5); ?></strong></td>
      <td class="w25 right"><label class="text-warning">رقم جواز سفر: </label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->passport_number); ?> </strong></td>
      <td class="w25 right"><label class="text-warning">رقم البطاقة الشخصة: </label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->idcard_number); ?> </strong></td>
      <td class="w25 right"><label class="text-warning">الحالة الجتماعية: </label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->marital_status); ?></strong></td>
    </tr>
    <tr>
      <td class="w25 right"><label class="text-warning">المحافظة \ المنطقة:</label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->province); ?></strong></td>
      <td class="w25 right"><label class="text-warning">ولاية:</label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->wilaya); ?> </strong></td>
      <td class="w25 right"><label class="text-warning">البلدة \ المحلة:</label>
        <br />
        <strong><?PHP echo $ah_applicant->address; ?> </strong></td>
      <td class="w25 right"><label class="text-warning">هاتف المنزل: </label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->hometelephone); ?>, <?PHP echo a_date(implode(', ',json_decode($ah_applicant->extratelephone,TRUE))); ?></strong></td>
    </tr>
    <?PHP 
		$wifeCount = 0;
		foreach($ah_applicant_wife as $wife) {
			$wifeCount++;	
	?>
    <tr>
      <td class="right"><label class="text-warning">اسم الزوج \ الزوجة <?PHP echo a_date($wifeCount); ?>:</label></td>
      <td class="right"><?PHP echo $wife->relationname; ?></td>
      <td class="right"><?PHP echo a_date($wife->relationdoc); ?></td>
      <td class="right"><?PHP echo $this->haya_model->get_name_from_list($wife->relationnationality); ?></td>
    </tr>
    <?PHP } ?>
    <tr>
      <td class="right"><label class="text-warning">اسم الام:</label></td>
      <td class="right"><?PHP echo $ah_applicant->mother_name; ?></td>
      <td class="right"><?PHP echo a_date($ah_applicant->mother_id_card); ?></td>
      <td class="right"></td>
    </tr>
    <tr>
      <td colspan="4" class="customhr">اترفق الوثائق والمستندات الازمة لطلب</td>
    </tr>
    <?PHP 
		$doccount = 0;					
		foreach($this->inq->allRequiredDocument($ah_applicant->charity_type_id) as $ctid) { $doccount++; 
			$doc = $ah_applicant_documents[$ctid->documentid];						
			$url = base_url().'resources/applicants/'.$ah_applicant->applicantcode.'/'.$doc->document;	?>
    <tr>
      <td colspan="3" class="right"><?PHP echo a_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></td>
      <td class="center"><i class="myicon <?PHP if(file_exists($url)) { ?>alert-green<?PHP } ?> icon-ok"></i></td>
    </tr>
<!--    <tr>
    	<td colspan="4"><?PHP echo filePreview($url,$ctid->documenttype); ?></td>
    </tr>-->
    <?PHP } ?>
    <tr>
      <td colspan="4" class="customhr">افراد الأسرة: (بما فهيم العاملين و غيرالعاملين)</td>
    </tr>
    <tr>
      <td class="right y_head">الاسم</td>
      <td class="right y_head">القرابة</td>
      <td class="right y_head">المهنة</td>
      <td class="right y_head">الدخل الشهري</td>
    </tr>
    <?PHP 
		if($ah_applicant_relation)
		{	foreach($ah_applicant_relation as $apr) { ?>
    <tr>
      <td class="right"><?PHP echo $apr->relation_fullname; ?> (<?PHP echo a_date($apr->age); ?>)</td>
      <td class="right"><?PHP echo $this->haya_model->get_name_from_list($apr->relationtype); ?></td>
      <!--<td class="right"><?PHP echo $this->haya_model->get_name_from_list($apr->professionid); ?></td>-->
      <td class="right"><?PHP echo $apr->profession; ?></td>
      <td class="right"><?PHP echo a_date($apr->monthly_income); ?> ر. ع</td>
    </tr>
    <?PHP }
		} ?>
    <tr>
      <td colspan="4" class="customhr">االحالة الاقتصادية:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">مصدر الدخل:</label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->income); ?></strong></td>
      <td class="right"><label class="text-warning">الدخل:</label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->salary); ?></strong></td>
      <td class="right"><label class="text-warning">يوجد:</label>
        <br />
        <?PHP if($ah_applicant->source=='يوجد') { echo('يوضح كالاتي'); } elseif($ah_applicant->source=='لايوجد') { echo('ويوضح مصدر المعيشة'); } ?></td>
      <td class="right"><?PHP if($ah_applicant->source=='يوجد') { ?>
        <label class="text-warning">قيمتة / مصدره:</label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->qyama); ?> / <?PHP echo a_date($ah_applicant->musadra); ?></strong>
        <?PHP } ?>
        <?PHP if($ah_applicant->source=='لايوجد') { ?>
        <label class="text-warning">مصدر المعيشة:</label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->lamusadra); ?></strong>
        <?PHP } ?></td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">وضف الحالة السكنية:</label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->current_situation); ?></strong></td>
      <td class="right"><label class="text-warning"><?PHP echo $ah_applicant->ownershiptype; ?>:</label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->ownershiptype_amount); ?></strong></td>
      <td></td>
      <td></td>
    </tr>
    <?php if($ah_applicant->charity_type_id	!=	'81'):?>
    <tr>
      <td colspan="4" class="customhr">مكونات المنزل:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">نوع البناء:</label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->building_type); ?></strong></td>
      <td class="right"><label class="text-warning">عدد الغرف:</label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->number_of_rooms); ?></strong></td>
      <td class="right"><label class="text-warning">المرافق:</label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->utilities); ?></strong></td>
      <td class="right"><label class="text-warning">حالة الأثاث والموجودات:</label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->furniture); ?><strong></td>
    </tr>
    <?php endif;?>
    <?php if($ah_applicant->charity_type_id	==	'81'):?>
    <tr>
      <td colspan="4" class="customhr">البيانات الدراسية:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">شهادة ثنوية:</label>
        <br />
        <strong><?PHP echo $ah_applicant->certificate_dualism; ?></strong></td>
      <td class="right"><label class="text-warning">سنة التخرج:</label>
        <br />
        <strong><?PHP echo $ah_applicant->graduation_year; ?></strong></td>
      <td class="right"><label class="text-warning">النسبة:</label>
        <br />
        <strong><?PHP echo $ah_applicant->school_percentage; ?></strong></td>
    </tr>
        <tr>
      <td colspan="4" class="customhr">البيانات الجامعية:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">اسم الكلية أو الجامعة:</label>
        <br />
        <strong><?PHP echo $ah_applicant->college_name; ?></strong></td>
      <td class="right"><label class="text-warning">التخصص:</label>
        <br />
        <strong><?PHP echo $ah_applicant->specialization; ?></strong></td>
      <td class="right"><label class="text-warning">السنة لدراسية:</label>
        <br />
        <strong><?PHP echo $ah_applicant->year_of_study; ?></strong></td>
      <td class="right"><label class="text-warning">اخر معدل تراكمي:</label>
        <br />
        <strong><?PHP echo $ah_applicant->last_grade_average; ?><strong></td>
    </tr>
    <?php endif;?>
    <?php if($ah_applicant->charity_type_id	!=	'81'):?>
    <tr>
      <td colspan="4" class="customhr">تفاصيل البنك:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">اسم البنك:</label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->bankid); ?></strong></td>
      <td class="right"><label class="text-warning">الفرع:</label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->branchid); ?></strong></td>
      <td class="right"><label class="text-warning">رقم الحساب:</label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->accountnumber); ?></strong></td>
      <td class="right"><label class="text-warning">نوع الحساب: جاري \ توفير:</label>
        <br />
        <strong><?PHP echo $ah_applicant->accounttype; ?><strong></td>
    </tr>
    <?php endif;?>
    <tr>
      <td colspan="4" class="customhr">الملاحظات:</td>
    </tr>
    <?PHP foreach($_applicant_data['notes'] as $note) { ?>
    <tr>
      <td class="right" colspan="3"><?PHP echo a_date($note->notes); ?> <strong>(كتبت في <?PHP echo a_date($note->notesdate); ?>)</strong></td>      
      <td class="right"><?PHP echo $note->fullname; ?><br /><?PHP echo $note->list_name; ?></td>
    </tr>
    <?PHP } ?>
  </table>
</div>
<div class="col-md-12 center"><button type="button" class="btn" id="print" onclick="printthepage('myprint');"><i class="icon-print"></i> طباعة</button></div>

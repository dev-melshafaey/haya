<div class="row">
	  <form action="" method="POST" id="frm_expence" name="frm_expence" action="<?php echo base_url()?>inquiries/addrenewdata" enctype="multipart/form-data" onsubmit="return uploadFiles();">

		<input type="hidden" name="applicantid" id="applicantid" value="<?PHP echo $applicantid; ?>">
		<input type="hidden" name="userid" id="userid" value="<?PHP echo $user_detail['profile']->userid; ?>">
        <input type="hidden" name="appRewMid" id="appRewMid" value="<?PHP echo $appRewMid; ?>">
	 <div class="col-md-12 form-group">
    <div class="form-group col-md-12">
              <label for="basic-input">نوع التحديث</label> <br> 
               <input type="radio" name="gender" value="term"> فصلي    
               <input type="radio" name="gender" value="yearly"> سنوي 

            </div>
     		<div class="form-group col-md-4">
              <label for="basic-input">تاريخ التجديد</label>
              <input type="text" class="form-control datepicker req " value="" placeholder="تاريخ التجديد" name="currentdate" id="currentdate"/>
            </div>
            <div class="form-group col-md-4">
              <label for="basic-input">السنة الدراسية</label>
              <input type="text" class="form-control req " value="" placeholder="السنة الدراسية" name="years" id="years"  maxlength="9"   />
            </div>
             <div class="form-group col-md-4">
              <label for="basic-input">اخر معدل تراكمي</label>
              <input type="text" class="form-control  req" value="" placeholder="اخر معدل تراكمي" name="cumulative" id="cumulative"  />
            </div>
            <div class="form-group col-md-8">
              <label for="basic-input">ملاحظات</label>
              <textarea type="text" class="form-control req" value="" placeholder="ملاحظات" name="notes" id="notes" rows="6" ></textarea>
            </div>
             <div class="form-group col-md-4">
              <label for="basic-input">توقع التخرج</label>
              <input type="text" class="form-control req " value="" placeholder="توقع التخرج" name="degree" id="degree"  /> <br/>
              <label for="basic-input">الدرجة العلمية</label>
              <input type="text" class="form-control req " value="" placeholder="الدرجة العلمية" name="degree_num" id="degree_num" onkeyup="only_numeric(this);" /> 
              
            </div>
            
     </div>
     <div class="form-group col-md-12">
      <label for="basic-input">الانذارات</label>
      <textarea  class="form-control req"  placeholder="الانذارات" name="notes2" id="notes2"  rows="5" />
    </div>
     <div class="form-group col-md-6">
          <label for="basic-input">ارفاق كشف الدرجات</label>
          <input type="file" class="form-control " value="" placeholder="المرفق" name="attachment1[]" id="attachment1" multiple="multiple"  />
    </div>
     <div class="form-group col-md-6">
          <label for="basic-input">ارفاق الخطة الدراسية</label>
          <input type="file" class="form-control " value="" placeholder="المرفق" name="attachment2[]" id="attachment2" multiple="multiple"  />
    </div>
    <div class="col-md-12 form-group">
      <input type="submit" class="btn btn-success btn-lrg" name="submit"  id="save_expence" value="حفظ"  />
	</div>
  </form>
</div>

<script>
function uploadFiles()
{
	check_my_session();
        $('#frm_expence .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_expence .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_expence .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {
	var form_data = new FormData(this); 
 $.ajax({ //ajax form submit
            url :  config.BASE_URL+'inquiries/addrenewdata',
            type: 'POST',
            data : form_data,
            dataType : "json",
            contentType: false,
            cache: false,
            processData:false,
			beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(response)
		  {		
			  $('#ajax_action').hide();
			 $('#addingDiag').modal('hide');	  
			 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
			var appRewMid = response.appRewMid;
				window.location.href ="<?php echo base_url()?>inquiries/afteraddagreement/<?PHP echo $applicantid; ?>/"+appRewMid; 
			 
		  }
        });
		}
		 else 
		{
            show_notification_error_end(ht);
        }
		return false;
		
}
 $(document).ready(function(){

$('form').on('submit', uploadFiles);
});
$(function(){
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});

</script>

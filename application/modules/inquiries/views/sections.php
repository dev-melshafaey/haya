<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
    
         <?php
		 foreach($sections as $section) 
		 { 
		 	//$tharki = $this->haya_model->get_permission_ids($moduleparent,$section['list_id'],$permission);
			$tharki	=	1;
			if($tharki	==	1)
			{
		 	?>
            <?php $module 		 =	$this->haya_model->get_module_name_icon(0,$section['list_id']); ?>
            <div class="col-md-2 main_body" id="<?php echo $section['list_id'];?>">
              <div id="bingo<?php echo $section['list_id'];?>" class="col-md-12 center main_icon"><i  class="<?php echo $module->module_icon;?>"></i></div>
              <div class="col-md-12 center main_heading"><a href="<?php echo base_url();?>inquiries/<?php echo $method_name;?>/<?php echo $branchid;?>/<?php echo $section['list_id'];?>"> <?php echo $section['name'];?> </a></div>
            </div>
            <?php } 
		 		} ?>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script language="javascript">
$(function(){
	$('.main_body').mouseover(function()
	{
		var objid = $(this).attr("id");
		$('#bingo'+objid).animo( { animation: ['tada', 'bounce','spin'], duration:2 });
	}).mouseout(function()
	{
		$('.spin').animo("cleanse");
	});
})
</script>
</div>
</body>
</html>
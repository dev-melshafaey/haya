<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <style>
.panel.panel-block .panel-heading>div>img, .panel.panel-block .panel-heading>div>i
{
	height:65px !important;
}
.text-bold
{
	/*padding: 20px 20px;*/
	font-size:16px !important;
}
  </style>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block panel-title-block">
          <div class="panel-heading clearfix">
          <?php //echo '<pre>'; print_r($single_user);?>

            <div class="avatar">
                   <span style="float: left;">
                   <a href="<?php echo base_url();?>inquiries/add_user_registeration/<?php echo $this->session->userdata('userid');?>">
                   <input type="button" class="btn btn-success btn-lrg" value="تحرير">
                   </a></span>
             <img src="<?php echo base_url();?>images/logo.jpg" alt="">
             </div>

            <br /> <br /> 
            <span class="title"><b class="text-bold">اسم :</b> <?php echo $single_user->firstname.' '.$single_user->lastname;?></span>
            <br /> 
            <br />
            <small> <b class="text-bold">اسم المستخدم :</b> <?php echo $single_user->user_name;?> </small> 
            <br />
            <br /> 
            <small> <b class="text-bold">البريد الإلكتروني :</b> <?php echo $single_user->email;?> </small> 
            <br />
            <br /> 
            <small> <b class="text-bold">رقم الهاتف :</b> <?php echo $single_user->number;?> </small>
            <br />
            <br /> 
            <small> <b class="text-bold">عن مستخدم :</b> <?php echo $single_user->about_user;?> </small>
            <br />
            <br /> 
            <small> <b class="text-bold">فرع :</b> <?php echo $this->inq->get_branch_name($single_user->branch_id);?> </small>
            <br />
            <br /> 
            <small> <b class="text-bold">القسم :</b> <?php echo $this->inq->get_user_role($single_user->user_parent_role);?> </small>
            <br />
            <br /> 
            <small> <b class="text-bold">الوظيفة :</b> <?php echo $this->inq->get_user_child_role($single_user->user_role_id);?> </small>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php //echo $single_user->user_role_id;?>
<?php //echo '<pre>'; print_r($this->inq->get_user_child_role($single_user->user_parent_role));?>
<script>
    $(function(){        
      var branchid = '<?PHP echo $branchid; ?>';
	
    } );
</script>
<?php $this->load->view('common/footer'); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
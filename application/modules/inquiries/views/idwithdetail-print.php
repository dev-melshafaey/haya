<?PHP
	$ah_applicant = $_applicant_data['ah_applicant'];
	$ah_applicant_wife = $_applicant_data['ah_applicant_wife'];
	$ah_applicant_survayresult = $_applicant_data['ah_applicant_survayresult'];
	$ah_applicant_relation = $_applicant_data['ah_applicant_relation'];
	$ah_applicant_family = $_applicant_data['ah_applicant_family'];
	$ah_applicant_economic_situation = $_applicant_data['ah_applicant_economic_situation'];
	$ah_applicant_documents = $_applicant_data['ah_applicant_documents'];	
?>
<style>
	.print_td {
		border-bottom:1px solid #ccc !important;
		width: 25% !important;
		text-align: right!important;
    padding: 3px 10px!important;
	font-size:14px !important;
	}
	
	.print_footer {
		
	padding: 8px 10px!important;
    font-size: 12px;
    font-weight: bold;
	
	}
	
</style>
<?PHP
	$currentUser = $user_detail['profile']->fullname.' ('.$user_detail['profile']->branchname.')';

?>
<div class="row">
  <div class="col-md-12" id="printmydiv">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #000 !important; direction: rtl;">
      
	  <tr>
        <td colspan="4" style="text-align:center !important;"><img style="width: 400px; margin-top: 2px;" src="<?php echo base_url();?>assets/images/logox.png"></td>
      </tr>	  
	   <tr>
        <td colspan="4" style="text-align:center !important; font-size:15px; border-bottom:2px solid #000; padding-bottom:3px !important;"><strong>إيصال إستلام طلب مساعدة (<?php echo $this->haya_model->get_name_from_list($ah_applicant->charity_type_id); ?>)</strong></td>
      </tr>
      <tr>
        <td class="print_td"><strong>رقم الحاسب الآلي:</strong></td>
        <td class="print_td"><?php echo arabic_date($ah_applicant->applicantcode); ?></td>
        <td class="print_td"><strong>تاريخ إستلام الطلب:</strong></td>
        <td class="print_td"><?PHP echo show_date($ah_applicant->registrationdate,5); ?></td>
      </tr>
      <tr>
        <td class="print_td"><strong>أسم مقدم الطلب:</strong></td>
		<td class="print_td"><?php echo $ah_applicant->fullname; ?></td>
		<td class="print_td"><strong>الرقم المدني:</strong></td>
		<td class="print_td"><?PHP echo arabic_date($ah_applicant->idcard_number); ?></td>
      </tr>
      <tr>
        <td class="print_td"><strong>الــولايــة:</strong></td>
        <td class="print_td"><?php echo $this->haya_model->get_name_from_list($ah_applicant->wilaya); ?></td>
		<td class="print_td"><strong>القرية / المحلة:</strong></td>
		<td class="print_td"><?php echo $ah_applicant->address; ?></td>
      </tr>    
      <tr style="display: none !important;">
        <td class="print_td"><strong>رقم رسالة الطلب:</strong></td>
		<td class="print_td"></td>
        <td class="print_td"><strong>تاريخ الرسالة:</strong></td>
		<td class="print_td"><?php echo show_date($ah_applicant->registrationdate,5); ?></td>
      </tr>
      <tr style="display: none !important;">
        <td class="print_td"><strong>الجهة الواردة منها:</strong></td>
		<td class="print_td" colspan="3"></td>
      </tr>
	 
      <tr style="display: none !important;">
        <td class="print_td"><strong>توقيع المستلم:</strong></td>
		<td class="print_td"></td>
        <td class="print_td"><strong>التاريخ:</strong></td>
		<td class="print_td"></td>
      </tr>
      <tr>
        <td class="print_td" colspan="1"><strong>الملاحظات:</strong></td>
	    <td class="print_td" colspan="3"><input type="text" class="form-control" id="notes_text"></td>
      </tr>

      <tr>
        <td colspan="2" class="print_footer" style="text-align:right !important; font-size:11px !important;"><?PHP echo arabic_date(' الهاتف : 24297993 – 24297993 – 24297994   فاكس : 24487997 '); ?></td>
		<td colspan="2" class="print_footer" style="text-align:left !important;"><?PHP echo $currentUser; ?></td>
      </tr>      
    </table>
  </div>
  <div class="col-md-12 from-group" style="text-align:center !important;">
	<button type="button" class="btn btn-success" onclick="printwithsave('printmydiv','<?PHP echo $ah_applicant->applicantid; ?>','<?PHP echo($user_detail['profile']->userid); ?>','<?PHP echo($user_detail['profile']->branchid); ?>');"><i class="icon-print"></i> طباعة</button>
  </div>
</div>
<style>
	.removeBorder { border: 0px !important; }
</style>
<script>
	$(function(){
		$('#notes_text').blur(function(){
			$(this).addClass('removeBorder');
		});
	});
	
	
</script>
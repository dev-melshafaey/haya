<?PHP
	
	$ckList = $checkList[0];
	
	//echo '<pre>'; print_r($ckList);
	//echo 'check = '.$ckList->sealed_company;
?>

<div class="row">
  <div class="col-md-12">
    <form id="validate_form_six" name="validate_form_six" method="post" action="<?PHP echo base_url(); ?>inquiries/add_evolution_data" autocomplete="off">
      <input type="hidden" id="applicant_id" value="<?PHP echo $appId; ?>" name="applicant_id" />
      <script>
	$(document).ready(function(e) {
        $('.checkoption').click(function(e) {
            var status = $(this).is(':checked');
			var message = $(this).attr('placeholder');
			
			if(status)
			{	
				var status_val = 1;	
			}
			else
			{	
				var status_val = 0;	
			}

			var id = $(this).attr('id');
			var appId = $("#applicant_id").val();
			var checkedCount = $('.checkoption:checked').length;		
			$.ajax({
				url: config.BASE_URL+'inquiries/updatecheckList/',
				type: "POST",
				data:{'id':id,'val':status_val,'appId':appId,'count':checkedCount},
				dataType: "html",
				success: function(msg)
				{	show_notification(message); }
			});
		});
    });
	</script>
      <div class="panel panel-default panel-block">
        <div class="tab-pane list-group">
          <div class="list-group-item">
            <h4 class="section-title text-warning">كشف بالمستندات المطلوبة بعد الموافقة</h4>
            <table class="table table-bordered table-striped dataTable">
              <tbody>
                <tr>
                  <td><input placeholder="أصل عروض الأسعار (مزود بأرقام حساب الموردين و مختومة بختم الشركة الموردة و تكون باسم المؤسسة /الشركة). " type="checkbox"  name="sealed_company" id="sealed_company" <?php if(!empty($ckList) && $ckList->sealed_company !="0") { ?> checked="checked" <?php  } ?> class="checkoption"/></td>
                  <td>أصل عروض الأسعار (مزود بأرقام حساب الموردين و مختومة بختم الشركة الموردة و تكون باسم المؤسسة /الشركة).</td>
                </tr>
                <tr>
                  <td><input placeholder="أصل أوراق السجل التجاري+ ختم الشركة أو المؤسسة" type="checkbox"  name="commercial_papers" id="commercial_papers" <?php if(!empty($ckList) && $ckList->commercial_papers !="0") { ?> checked="checked" <?php  } ?> class="checkoption"/></td>
                  <td>أصل أوراق السجل التجاري+ ختم الشركة أو المؤسسة </td>
                </tr>
                <tr>
                  <td><input type="checkbox" placeholder="أصل الترخيص البلدي و عقد الايجار (لا يشمل نشاط النقل)"  name="municipal_contractrent" id="municipal_contractrent" <?php if(!empty($ckList) && $ckList->municipal_contractrent !="0") { ?> checked="checked" <?php  } ?>  class="checkoption"/></td>
                  <td>أصل الترخيص البلدي و عقد الايجار (لا يشمل نشاط النقل). </td>
                </tr>
                <tr>
                  <td><input placeholder="أصل شهادة الانتساب" type="checkbox"  name="membership_certificate" id="membership_certificate" <?php if(!empty($ckList) && $ckList->membership_certificate !="0") { ?> checked="checked" <?php  } ?> class="checkoption"/></td>
                  <td>أصل شهادة الانتساب . </td>
                </tr>
                <tr>
                <tr>
                  <td><input placeholder="تسجيل المؤسسة /الشركة بالهيئة العامة للمؤسسات الصغيرة والمتوسطة " type="checkbox"  name="company_general_authority" id="company_general_authority" <?php if(!empty($ckList) && $ckList->company_general_authority !="0") { ?> checked="checked" <?php  } ?> class="checkoption"/></td>
                  <td>تسجيل المؤسسة /الشركة بالهيئة العامة للمؤسسات الصغيرة والمتوسطة . </td>
                </tr>
                <tr>
                <tr>
                  <td><input placeholder="فتح حساب باسم (المؤسسة/ الشركة) بعد استخراج (السجل التجاري+ شهادة الانتساب)" type="checkbox"  name="open_account" id="open_account" <?php if(!empty($ckList) && $ckList->open_account !="0") { ?> checked="checked" <?php  } ?> class="checkoption"/></td>
                  <td>فتح حساب باسم (المؤسسة/ الشركة) بعد استخراج (السجل التجاري+ شهادة الانتساب) </td>
                </tr>
                <tr>
                  <td><input placeholder="دفتر الشيكات (.........................شيك)" type="checkbox"  name="check_book" id="check_book" <?php if(!empty($ckList) && $ckList->check_book !="0") { ?> checked="checked" <?php  } ?> class="checkoption"/></td>
                  <td>دفتر الشيكات (.........................شيك). </td>
                </tr>
                <tr>
                  <td><input placeholder="تسجيل الرمز البريدي و صندوق البريد" type="checkbox"  name="registration_zip" id="registration_zip" <?php if(!empty($ckList) && $ckList->check_book !="0") { ?> checked="checked" <?php  } ?> class="checkoption"/></td>
                  <td>تسجيل الرمز البريدي و صندوق البريد. </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

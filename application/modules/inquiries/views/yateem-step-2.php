<!doctype html>
<?php
	$ah_applicant 						=	$_applicant_data['ah_applicant'];
	$ah_applicant_wife 					=	$_applicant_data['ah_applicant_wife'];
	$ah_applicant_survayresult			=	$_applicant_data['ah_applicant_survayresult'];
	$ah_applicant_relation				=	$_applicant_data['ah_applicant_relation'];
	$ah_applicant_family				=	$_applicant_data['ah_applicant_family'];
	$ah_applicant_economic_situation	=	$_applicant_data['ah_applicant_economic_situation'];
	$ah_applicant_documents 			=	$_applicant_data['ah_applicant_documents'];
?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
       <?php $msg	=	$this->session->flashdata('msg');?>
      <?php if($msg):?>
      <div class="col-md-12">
      	<div style="padding: 22px 20px !important; background:#c1dfc9;">
        	<h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
        </div>
      </div>
      <?php endif;?>
  <div class="row">
    <form id="add_yateem_servay" name="add_yateem_servay" method="post" action="<?PHP echo base_url().'inquiries/add_yateem_servay' ?>" autocomplete="off">
      <input type="hidden" name="step" id="step" value="2" />
       <input type="hidden" name="orphan_id" id="orphan_id" value="<?php echo $orphan_id;?>" />
      <div class="col-md-12">
        <?php $this->load->view('common/panel_block', array('module' => $module, 'headxxx' => $heading)); ?>
        <div class="col-md-12">
          <div class="col-md-5 fox leftborder">
            <h4 class="panel-title customhr">نموذج بيانات الأيتام والرعاية المطلوبة</h4>
            <div class="col-md-6 form-group">
              <label class="text-warning">اسم اليتيم: </label>
              <strong><?php echo $yateem_data->orphan_name; ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الجنسية: </label>
              <strong><?php echo $this->haya_model->get_name_from_list($yateem_data->orphan_nationalty); ?></strong> </div>
            <div class="form-group col-md-6">
              <label class="text-warning">تاريخ الميلاد</label>
              <strong><?php echo $yateem_data->date_birth; ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">عمر:</label>
              <strong><?php echo $yateem_data->orphan_age; ?></strong></div>
            <br clear="all"/>
            <h4 class="panel-title customhr">باالكامل العنوان</h4>
            <div class="form-group col-md-3">
              <label class="text-warning">الدولة</label>
              <?php echo $this->haya_model->get_name_from_list($yateem_data->country_id); ?> </div>
            <div class="form-group col-md-3">
              <label class="text-warning">المدينة</label>
              <?php echo $this->haya_model->get_name_from_list($yateem_data->city_id); ?> </div>
            <div class="form-group col-md-3">
              <label class="text-warning">ص.ب</label>
              <?php echo $yateem_data->po_adress; ?> </div>
            <div class="form-group col-md-3">
              <label class="text-warning">الرمز البريدي</label>
              <?php echo $this->haya_model->get_name_from_list($yateem_data->country_id); ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">اليتيم أخوته ترتيب بين: </label>
              <strong><?PHP echo $yateem_data->orphan_arrangement_brothers; ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">عدد أفراد الأسرة: </label>
              <strong><?PHP echo $yateem_data->total_family_memebers; ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الهاتف/ منزل: </label>
              <strong><?PHP echo arabic_date($yateem_data->phone_number); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">هاتف ولي الامر: مع رمز الدولة: </label>
              <strong><?PHP echo arabic_date($yateem_data->home_number); ?></strong> </div>
<!--          <div class="col-md-6 form-group">
              <label class="text-warning">الأعمار الإخواة الأكبر: </label>
              <strong>
              <?php if(!empty($yateem_brothers)){
		  		foreach($yateem_brothers as $brothers){
					echo "<br/>".$brothers->br_sis_name."<br/>";
				}
		  
		  } ?>
              </strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الإخواة الأصغر: </label>
              <strong>
              <?php if(!empty($yateem_sisters)){
		  		foreach($yateem_sisters as $sisters){
					if($sisters->br_sis_name !="")
					echo "<p>".$sisters->br_sis_name."</p>";
				}
		  } ?>
              </strong> </div>-->
              
             <div class="col-md-12 form-group">
            <h4 class="panel-title customhr">الاخوة الذكور / الاخوة الاناث</h4>
                <table class="table table-bordered table-striped dataTable" id="females">
              <thead>
                <tr role="row">
                  <th class="my_td right" style="width:50% !important;">الاسم</th>
                  <th class="my_td right" style="width:20% !important;">جنس</th>
                  <th class="my_td center" style="width:15% !important;">سنة الميلاد</th>
                  <th class="my_td center" style="width:10% !important;">العمر</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
              <?php if(!empty($yateem_sisters_brothers)):?>
				  <?php foreach($yateem_sisters_brothers as $sisters):?>
                  <?php 
                  if($sisters->br_sis_type	==	'brother')
                  {
                      $gender	=	'بنين';
                  }
                  else
                  {
                      $gender	=	'بنات';
                  }
                  ?>
                   <tr role="row">
                      <td class="right" style="width:50% !important;"><?php echo $sisters->br_sis_name;?></td>
                      <td class="center" style="width:20% !important;"><?php echo $gender;?></td>
                      <td class="center" style="width:15% !important;"><?php echo $sisters->br_birthyear;?></td>
                      <td class="center" style="width:10% !important;"><?php echo $sisters->br_age;?></td>
                    </tr>
                    <?php endforeach;?>
                <?php endif;?>
              </tbody>
              </table>
    </div>
              <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr">بيانات عن ولي الأمر</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">الاسم بالكامل:</label>
      <strong><?php echo $yateem_father_data->father_name; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">صلة القرابة: </label>
      <strong><?php echo $yateem_father_data->parent_relationship; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">ص.ب: </label>
      <strong><?php echo $yateem_father_data->parent_po_address; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الرمز البريدي: </label>
      <strong><?php echo $yateem_father_data->parent_pc_address; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning"> رقم الهاتف: </label>
      <strong><?php echo arabic_date($yateem_father_data->father_phone_number); ?></strong> </div>
     <div class="col-md-6 form-group">
      <label class="text-warning">دولة: </label>
      <strong><?php echo $this->haya_model->get_name_from_list($yateem_father_data->parent_country_id); ?></strong> </div>
      <!--<div class="col-md-6 form-group">
      <label class="text-warning">منطقة: </label>
      <strong><?php echo $this->haya_model->get_name_from_list($yateem_father_data->parent_region_id); ?></strong> </div>-->
      <div class="col-md-6 form-group">
      <label class="text-warning">ولاية: </label>
      <strong><?php echo $this->haya_model->get_name_from_list($yateem_father_data->parent_wilaya_id); ?></strong> </div>
        </div>
      </div>
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr">بيانات الأم</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم الأم:</label>
      <strong><?php echo $yateem_mother_data->mother_name; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الحالة المادية: </label>
      <strong><?php echo $this->haya_model->get_name_from_list($yateem_mother_data->marital_status); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الهاتف: </label>
      <strong><?php echo $yateem_mother_data->mother_phone_number; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الرقم الدولي: </label>
      <strong><?php echo $yateem_mother_data->parent_country_code; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الهوية: </label>
      <strong><?php echo $yateem_mother_data->id_no; ?></strong> </div>
     <div class="col-md-6 form-group">
      <label class="text-warning">عنوان: </label>
      <strong><?php echo $yateem_mother_data->parent_address; ?></strong> </div>
        </div>
      </div>
    </div>
              <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr">البيانات بنكية</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم البنك:</label>
      <strong><?php echo $this->haya_model->get_name_from_list($yateem_banks_data->bankid); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الفرع: </label>
      <strong><?php echo $this->haya_model->get_name_from_list($yateem_banks_data->bankbranchid); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الحساب: </label>
      <strong><?php echo $yateem_banks_data->account_number; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">إسم الحساب: </label>
      <strong><?php echo $yateem_banks_data->account_title; ?></strong> </div>
       </div>
      </div>
    </div>
              <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr">بيانات أخرى</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">تاريخ الد الموت:</label>
      <strong><?php echo arabic_date($yateem_others_data->date_father_death); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">سبب الوفاة: </label>
      <strong><?php echo $yateem_others_data->reason_father_death; ?></strong> </div>
    <!--<div class="col-md-6 form-group">
      <label class="text-warning">عدد أفراد الأسرة: </label>
      <strong><?php echo $yateem_others_data->family_members; ?></strong> </div>-->
    <!--<div class="col-md-6 form-group">
      <label class="text-warning">الاخوة: </label>
      <strong><?php echo $yateem_others_data->brothers; ?></strong> </div>-->
      <!--<div class="col-md-6 form-group">
      <label class="text-warning">الأخوات: </label>
      <strong><?php echo $yateem_others_data->sisters; ?></strong> </div>-->
      <div class="col-md-6 form-group">
      <label class="text-warning">الظروف المعيششة للأسرة: </label>
      <strong><?php echo $yateem_others_data->living_condition_family; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الظروف السكينة: </label>
      <strong><?php echo $yateem_others_data->housing_condition; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الظروف الاقتصادية: </label>
      <strong><?php echo $yateem_others_data->economic_condition; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">مقدار الدخل الشهري: </label>
      <strong><?php echo $yateem_others_data->monthly_income; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">مصدر الدخل: </label>
      <strong><?php echo $yateem_others_data->source_income; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">عدد  الأخوة المكفين: </label>
      <strong><?php echo $yateem_others_data->number_brothers; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الجهات الكافلة: </label>
      <strong><?php echo $this->haya_model->get_name_from_list($yateem_others_data->foster_agencies); ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">اجمال قيمة الكفالة: </label>
      <strong><?php echo $yateem_others_data->income_value; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">مكان عمل المتوفي: </label>
      <strong><?php echo $this->haya_model->get_name_from_list($yateem_others_data->sponser_work_place); ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">جهة العمل: </label>
      <strong><?php echo $this->haya_model->get_name_from_list($yateem_others_data->place_name); ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">مقدار راتب التقاعد: </label>
      <strong><?php echo $yateem_others_data->retirement_period; ?></strong> </div>
       </div>
      </div>
    </div>
            <div class="col-md-12 form-group">
              <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <h4 class="panel-title customhr">بيانات المرحلة الدراسية</h4>
                <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                  <div class="col-md-6 form-group">
                    <label class="text-warning">اسم المدرسة:</label>
                    <strong><?php echo $yateem_edu_data->school_name; ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">المرحلة الدراسية: </label>
                    <strong><?php echo $yateem_edu_data->grade; ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">النتيجة آخر: </label>
                    <strong><?php echo $yateem_edu_data->last_result; ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">الصف: </label>
                    <strong><?php echo $yateem_edu_data->classs; ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">الترتيب: </label>
                    <strong><?php echo $yateem_edu_data->order; ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">الحالة الصحية: </label>
                    <strong><?php echo $yateem_edu_data->health_status; ?></strong> </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 form-group">
              <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <h4 class="panel-title customhr">الجهة المرشحة بيانات</h4>
                <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                  <div class="col-md-6 form-group">
                    <label class="text-warning">اسم الجهة:</label>
                    <strong><?php echo $yateem_can_data->candidate_name; ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">الدولة: </label>
                    <strong><?php echo $this->haya_model->get_name_from_list($yateem_can_data->candidate_country_id); ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">المحافظة: </label>
                    <strong><?php echo $this->haya_model->get_name_from_list($yateem_can_data->candidate_region_id); ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">المدينة: </label>
                    <strong><?php echo $this->haya_model->get_name_from_list($yateem_can_data->candidate_city_id); ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">الترتيب: </label>
                    <strong><?php echo $yateem_can_data->candidate_city_id; ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">العنوان: </label>
                    <strong><?php echo $yateem_can_data->candidate_address; ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">صندوق البريد: </label>
                    <strong><?php echo $yateem_can_data->candidate_pobox; ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">بلدة: </label>
                    <strong><?php echo $yateem_can_data->candidate_town; ?></strong> </div>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">رقم الهاتف: </label>
                    <strong><?php echo $yateem_can_data->candidate_phone_number; ?></strong> </div>
				<?php if($yateem_can_data->candidate_pincode):?>
                <div class="col-md-6 form-group">
                    <label class="text-warning">الرمز: </label>
                    <strong><?PHP echo $yateem_can_data->candidate_pincode; ?></strong>
                </div>
                <?php endif;?>
                </div>
              </div>
            </div>
            <div class="col-md-5 form-group"> <strong><?PHP echo arabic_date($yateem_data->ownershiptype_amount); ?></strong> </div>
            <div class="form-group col-md-12">
              <div class="tab-pane list-group active" id="tabsdemo-1">
                <div class="list-group-item">
                  <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                    <h4>اترفق الوثائق والمستندات الازمة لطلب</h4>
                    <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                <?PHP 
					$doccount = 0;					
					foreach($this->inq->allRequiredDocument(205) as $ctid) { $doccount++; 
						$doc = $yateem_docs[$ctid->documentid];						
						$url = 'resources/yateem/'.$doc->orphan_id.'/'.$doc->document_name;?>
                <div class="panel panel-default" style="border-bottom: 1px solid #ddd;">
                  <div class="panel-heading" style="padding:7px 3px !important;" id="head<?PHP echo $ctid->documentid;?>">
                    <h4 class="panel-title" style="font-size:12px; font-weight:normal !important;">
                      <?PHP if($doc->orphan_id!='') { ?>
                      <span class="icons" id="removeicons<?PHP echo $doc->document_id; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> </span>
                      <?PHP } ?>
                      <a class="accordion-toggle collapsed fixpopup" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $ctid->documentid;?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                  </div>
                </div>
                <?PHP } ?>
              </div>
                  </div>
                </div>
                <table style="width: 100% !important">
		   <?php
           $kafeel_list_count = sizeof($kafeel_list);
            if($kafeel_list_count > 0)
            {	  
           ?>
              <tr>
                <td class="customhr panel-title" style="border-bottom:1px solid #CCC;"> الكفيل</td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <?php foreach($kafeel_list as $kafeel) { ?>
                    <tr>
                      <td class="Y_value"><?php echo is_set($kafeel->sponser_name); ?> (<?PHP echo is_set(arabic_date($kafeel->sponser_id_number)); ?>)</td>
                      <td class="Y_value"><?php echo is_set($this->haya_model->get_name_from_list($kafeel->sponser_nationalty)); ?></td>
                      <td class="Y_value"><?php echo is_set($kafeel->sponser_designation); ?></td>
                      <td class="Y_value"><?php echo is_set($kafeel->sponser_town_id); ?></td>
                      <td class="Y_value"><?php echo is_set($this->haya_model->get_name_from_list($kafeel->city_id)); ?></td>
                    </tr>
                    <?php } ?>
                  </table></td>
              </tr>
			  <?php 
              }
              ?>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-7 fox">
            <h4 class="panel-title customhr">بعد إجراء البحث الاجتماعي والاطلاع على المستندات المؤيدة اتضح مايلي :-</h4>
            <div class="col-md-6 form-group">
              <label class="text-warning">الحالة الصحية:</label>
              <?PHP echo $this->haya_model->create_dropbox_list('health_condition','health_condition',$ah_applicant_survayresult->health_condition,0,'req'); ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الحالة السكنية:</label>
              <?PHP echo $this->haya_model->create_dropbox_list('housing_condition','housing_condition',$ah_applicant_survayresult->housing_condition,0,'req'); ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">عدد أفراد الأسرة:</label>
              <?PHP number_drop_box('numberofpeople',$ah_applicant_survayresult->numberofpeople,'عدد أفراد الأسرة'); ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">ترتيب مقدم الطلب بالأسرة:</label>
              <?PHP number_drop_box('positioninfamily',$ah_applicant_survayresult->positioninfamily,'ترتيب مقدم الطلب بالأسرة'); ?>
            </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">الحالة الاقتصادية:</label>
              <?php echo $this->haya_model->create_dropbox_list('economic_condition','economic_condition',$ah_applicant_survayresult->economic_condition,0,'req'); ?> </div>
            
            <!-- ---------------------------------------------- --> 
            <!-- Start New Modifications --> 
            <!-- ---------------------------------------------- -->
            <?php if($ah_applicant->charity_type_id	==	'78'){?>
            <div class="col-md-12 form-group">
              <h4 class="panel-title customhr">حالة البيت :-</h4>
              <label class="text-warning">نوع الصيانة:</label>
              <?php echo $this->haya_model->create_dropbox_list('maintenance_type','maintenance_type',$ah_applicant_survayresult->maintenance_type,0,'req'); ?>
              </select>
            </div>
            <div class="col-md-12 form-group" id="show-date" style="display:none;">
              <label class="text-warning">تاريخ :</label>
              <input name="certificate_date" value="<?php echo $ah_applicant_survayresult->certificate_date; ?>" placeholder="تاريخ" id="certificate_date" type="text" class="form-control dp" />
            </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">تقرير الزيارة:</label>
              <textarea name="report_visit" id="report_visit" style="height:150px; resize:none;" placeholder="رأي الباحث الاجتماعي و مبرراته"  class="form-control req"><?PHP echo $ah_applicant_survayresult->review; ?></textarea>
            </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">اقتراح القسم:</label>
              <textarea name="proposal_section" id="proposal_section" style="height:150px; resize:none;" placeholder="رأي الباحث الاجتماعي و مبرراته"  class="form-control req"><?PHP echo $ah_applicant_survayresult->review; ?></textarea>
            </div>
            <div class="container demo-wrapper">
              <div class="row demo-columns">
                <div class="col-md-12"> 
                  <!-- D&D Zone-->
                  <div id="drag-and-drop-zone" class="uploader">
                    <div>سحب وإسقاط الملفات هنا</div>
                    <div class="or">-او-</div>
                    <div class="browser">
                      <label> <span>اضغط لاستعراض الملف</span>
                        <input type="file" name="muzaffar" />
                      </label>
                    </div>
                  </div>
                </div>
                <!-- / Left column -->
                
                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">تحميل</h3>
                    </div>
                    <div class="panel-body demo-panel-files" id='demo-files'> <span class="demo-note">لم يتم اختيار الملفات / دروبيد بعد ...</span> </div>
                  </div>
                </div>
                <!-- / Right column --> 
              </div>
            </div>
            <?php }?>
            <!-- ---------------------------------------------- --> 
            <!-- End New Modifications --> 
            <!-- ---------------------------------------------- -->
            
            <div class="col-md-12 form-group">
              <h4 class="panel-title customhr">نوع الحالة :-</h4>
              <label class="text-warning">للحالات الضمانية:</label>
              &nbsp;&nbsp;&nbsp;
              <input type="radio" name="casetype" data-id="zamanya" <?php if($ah_applicant_survayresult->casetype=='للحالات الضمانية') { ?> checked <?PHP } ?> class="casetypeinput" value="للحالات الضمانية">
              <label class="text-warning">للحالات غير الضمانية:</label>
              &nbsp;&nbsp;&nbsp;
              <input type="radio" name="casetype" data-id="ghairzamanya" <?PHP if($ah_applicant_survayresult->casetype=='للحالات غير الضمانية') { ?> checked <?PHP } ?> class="casetypeinput" value="للحالات غير الضمانية">
            </div>
            <div class="col-md-12 form-group casetype" id="zamanya" <?PHP if($ah_applicant_survayresult->casetype=='للحالات الضمانية') { ?> style="display:block !important;" <?PHP } ?>>
              <div class="col-md-12 form-group">
                <label class="text-warning">الحالة الضمانية باسم:</label>
                <input name="aps_name" value="<?PHP echo $ah_applicant_survayresult->aps_name; ?>" placeholder="الحالة الاقتصادية" id="aps_name" type="text" class="form-control">
              </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">رقم الحاسب:</label>
                <input name="aps_account" value="<?PHP echo $ah_applicant_survayresult->aps_account; ?>" placeholder="الحالة الاقتصادية" id="aps_account" type="text" class="form-control NumberInput">
              </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">رقم الملف:</label>
                <input name="aps_filename" value="<?PHP echo $ah_applicant_survayresult->aps_filename; ?>" placeholder="الحالة الاقتصادية" id="aps_filename" type="text" class="form-control  NumberInput">
              </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">الفئة:</label>
                <?PHP echo $this->haya_model->create_dropbox_list('aps_category','aps_category',$ah_applicant_survayresult->aps_category,0,''); ?> </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">قيمة المعاش:</label>
                <input name="aps_salaryamount" value="<?PHP echo $ah_applicant_survayresult->aps_salaryamount; ?>" placeholder="الحالة الاقتصادية" id="aps_salaryamount" type="text" class="form-control  NumberInput">
              </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">تاريخ الربط:</label>
                <input name="aps_date" value="<?PHP echo $ah_applicant_survayresult->aps_date; ?>" placeholder="الحالة الاقتصادية" id="aps_date" type="text" class="form-control">
              </div>
              <div class="col-md-12 form-group">
                <label class="text-warning">مصادر دخل أخرى:</label>
                <?PHP $json = json_decode($ah_applicant_survayresult->aps_another_income,TRUE);
			  		$inx = 0;
			  		for($mdi=1; $mdi<=4; $mdi++) { ?>
                <input style="margin-bottom:2px;" name="aps_another_income[]" value="<?PHP echo $json[$inx]; ?>" placeholder="مصادر دخل أخرى <?PHP echo arabic_date($mdi); ?>." id="aps_another_income" type="text" class="form-control NumberInput">
                <?PHP $inx++; } ?>
              </div>
            </div>
            <div class="col-md-12 form-group casetype" id="ghairzamanya"  <?PHP if($ah_applicant_survayresult->casetype=='للحالات غير الضمانية') { ?> style="display:block !important;" <?PHP } ?>>
              <div class="col-md-12 form-group">
                <label class="text-warning">مصادر دخل أخرى:</label>
                <?php 
			  		$inxp = 0;
			  		for($mdi=1; $mdi<=4; $mdi++)
					{	
				?>
                <input style="margin-bottom:2px; width:50% !important;" name="aps_another_income[]" value="<?PHP echo $json[$inxp]; ?>" placeholder="مصادر دخل أخرى <?PHP echo arabic_date($mdi); ?>." id="aps_another_income" type="text" class="form-control">
                <?PHP $inxp++; } ?>
              </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">اجمالي الدخل الشهري:</label>
                <input name="aps_month" value="<?PHP echo $ah_applicant_survayresult->aps_month; ?>" placeholder="اجمالي الدخل الشهري للفرد" id="aps_month" type="text" class="form-control  NumberInput">
              </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">صافي الدخل الشهري للاسرة:</label>
                <input name="aps_year" value="<?PHP echo $ah_applicant_survayresult->aps_year; ?>" placeholder="اجمالي الدخل الشهري للاسرة" id="aps_year" type="text" class="form-control NumberInput">
              </div>
            </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">الغرض من المساعدة:</label>
              <input name="whyyouwant" value="<?PHP echo $ah_applicant_survayresult->whyyouwant; ?>" placeholder="الغرض من المساعدة" id="whyyouwant" type="text" class="form-control">
            </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">ملخص الحالة:</label>
              <textarea name="summary" style="height:150px; resize:none;" id="summary" placeholder="ملخص الحالة"  class="form-control req"><?PHP echo $ah_applicant_survayresult->summary; ?></textarea>
            </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">رأي الباحث الاجتماعي و مبرراته:</label>
              <textarea name="review" id="review" style="height:150px; resize:none;" placeholder="رأي الباحث الاجتماعي و مبرراته"  class="form-control req"><?PHP echo $ah_applicant_survayresult->review; ?></textarea>
            </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">عاجل:</label>
              <input  name="ajel" type="checkbox" value="Yes">
            </div>
            <div class="col-md-12 form-group"> <button type="button" id="save_yateem_servay" class="btn btn-success"><i class="icon-hdd"></i> حفظ</button></div>
          </div>
        </div>
      </div>
    </form>
  </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script src="<?PHP echo base_url(); ?>assets/js/tasgeel_js.js"></script> 
<script type="text/javascript">
	$(document).ready(function (){
	//alert('ready');
	$('.dp').datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat:'yy-mm-dd',
		minDate: "-2M -28D", maxDate: 1
    });
 });
	  var	applicant_id	=	'<?php echo $ah_applicant->applicantid;?>';
	  
      $('#drag-and-drop-zone').dmUploader({
        url: '<?php echo base_url();?>inquiries/uploads_sakaniya_visits/'+applicant_id,
        dataType: 'json',
        allowedTypes: 'image/*',
        /*extFilter: 'jpg;png;gif',*/
        onInit: function(){
          $.danidemo.addLog('#demo-debug', 'default', 'Plugin initialized correctly');
        },
        onBeforeUpload: function(id){
          $.danidemo.addLog('#demo-debug', 'default', 'Starting the upload of #' + id);

          $.danidemo.updateFileStatus(id, 'default', 'Uploading...');
        },
        onNewFile: function(id, file){
          $.danidemo.addFile('#demo-files', id, file);
        },
        onComplete: function(){
			
          $.danidemo.addLog('#demo-debug', 'default', 'All pending tranfers completed');
        },
        onUploadProgress: function(id, percent){
          var percentStr = percent + '%';

          $.danidemo.updateFileProgress(id, percentStr);
        },
        onUploadSuccess: function(id, data){
          $.danidemo.addLog('#demo-debug', 'success', 'Upload of file #' + id + ' completed');

          $.danidemo.addLog('#demo-debug', 'info', 'Server Response for file #' + id + ': ' + JSON.stringify(data));

          $.danidemo.updateFileStatus(id, 'success', 'Upload Complete');

          $.danidemo.updateFileProgress(id, '100%');
        },
        onUploadError: function(id, message){
          $.danidemo.updateFileStatus(id, 'error', message);

          $.danidemo.addLog('#demo-debug', 'error', 'Failed to Upload file #' + id + ': ' + message);
        },
        onFileTypeError: function(file){
          $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: must be an image');
        },
        onFileSizeError: function(file){
          $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: size excess limit');
        },
        /*onFileExtError: function(file){
          $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' has a Not Allowed Extension');
        },*/
        onFallbackMode: function(message){
          $.danidemo.addLog('#demo-debug', 'info', 'Browser not supported(do something else here!): ' + message);
        }
      }); 
    </script>
</body>
</html>
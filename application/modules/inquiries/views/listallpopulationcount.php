<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<?php $segment = $this->uri->segment(3); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <p>
                <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid"> 
                  
                  <!----------------------------> 
                  <!-- Reeja Soni 22-5-17 start-->
                  
                  <div class="row table-header-row"> <a class="btn btn-success" style="float:right; margin-left:10px;" href="<?php echo base_url();?>inquiries/listallpopulationcount/<?php echo $branchid;?>/<?php echo $charity_type;?>/1">موافق</a> <a class="btn btn-success" style="float:right;  margin-left:10px;" href="<?php echo base_url();?>inquiries/listallpopulationcount/<?php echo $branchid;?>/<?php echo $charity_type;?>/2">رفض</a> <a class="btn btn-success" style="float:right;  margin-left:10px;" href="<?php echo base_url();?>inquiries/listallpopulationcount/<?php echo $branchid;?>/<?php echo $charity_type;?>/3">تحويل</a> </div>
                  
                  <!----------------------------> 
                  <!-- Reeja Soni 22-5-17 end-->
                  
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center;">اسم القائمة</th>
                        <th style="text-align:center;">عدد</th>
                        <th style="text-align:center;">تاريخ</th>
                        <!--<th class="sorting" style="text-align:center;">الإجراءات</th>--> 
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!----------------------------> 
<!-- Reeja Soni 22-5-17 start-->

<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'inquiries/listallpopulationcount/'.$branchid.'/'.$charity_type.'/'.$application_status.'/list','columns_array'=>'
{ "data": "اسم القائمة" },
{"data":"عدد"},
{"data":"تاريخ"}')); ?>

<!----------------------------> 
<!-- Reeja Soni 22-5-17 end--> 

<!-- /.modal-dialog -->

</div>
</body>
</html>
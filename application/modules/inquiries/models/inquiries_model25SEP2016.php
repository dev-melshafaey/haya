<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inquiries_model extends CI_Model 
{
	/*
	* Properties
	*/
	private $_table_users;	
	private $_table_donate;	
	private $_table_applied_donation;
	private $_table_sms_management;
//----------------------------------------------------------------------    
	/*
	* Constructor
	*/	
	function __construct()
    {
        parent::__construct();
				
		$this->load->helper("file");
				
		// Load Table Names from Config
		$this->_table_users 			=  $this->config->item('table_users');
		$this->_table_donate 			=  $this->config->item('table_donate');
        $this->_table_user_profile 		=  $this->config->item('table_user_profile');
		$this->_table_applied_donation 	=  $this->config->item('table_applied_donation');
		$this->_table_sms_management 	=  $this->config->item('table_sms_management');
		$this->_login_userid			=	$this->session->userdata('userid');
    }
//----------------------------------------------------------------------    
	/*
	* 
	*/		
	function allRequiredDocument($charity_type_id_value)
	{
		$this->db->select('documentid,documenttype,isrequired');
        $this->db->from('ah_document_required');
        $this->db->where('charity_type_id',$charity_type_id_value);
		$this->db->where('documentstatus',1);
        $this->db->order_by("documentorder", "ASC");
		
        $query = $this->db->get();
		return $query->result();
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/		
	function submitfor()
	{
		$form_data = $this->input->post();
		
		$refil_data 						= 	$form_data['refil_data'];
		
		$applicantid 						= 	$form_data['applicantid'];
		$logged_in_user_detail 				= 	$this->haya_model->get_user_detail($this->_login_userid);
		$ah_applicant['userid'] 			= 	$this->_login_userid;
		$ah_applicant['branchid']			= 	$logged_in_user_detail['profile']->branchid;
		$ah_applicant['charity_type_id'] 	= 	$form_data['charity_type_id'];
		$ah_applicant['province'] 			= 	$form_data['province'];
		$ah_applicant['wilaya'] 			= 	$form_data['wilaya'];
		$ah_applicant['address'] 			= 	$form_data['address'];
		$ah_applicant['fullname'] 			= 	$form_data['fullname'];
		
		$ah_applicant['country_listmanagement']	= 	$form_data['country_listmanagement'];
		$ah_applicant['applicant_age']			= 	$form_data['applicant_age'];
		$ah_applicant['date_of_birth']			= 	$form_data['date_of_birth'];
			
		if($applicantid	==	'')
		{
			$ah_applicant['passport_number']	= $form_data['passport_number'];
			$ah_applicant['idcard_number'] 		= $form_data['idcard_number']; 
		}
		if($refil_data	!=	'')
		{
			$ah_applicant['passport_number']	= $form_data['passport_number'];
			$ah_applicant['idcard_number'] 		= $form_data['idcard_number'];
		}
		
		$ah_applicant['passport_number']		= 	$form_data['passport_number'];	
		$ah_applicant['hometelephone'] 			=	$form_data['hometelephone'];
		$ah_applicant['extratelephone'] 		=	json_encode($form_data['extratelephone']);
		$ah_applicant['marital_status'] 		=	$form_data['marital_status'];
		$ah_applicant['income'] 				=	$form_data['income'];
		$ah_applicant['mother_name'] 			=	$form_data['mother_name'];
		$ah_applicant['mother_id_card'] 		= 	$form_data['mother_id_card'];

		$ah_applicant['father_name'] 			= 	isset($form_data['father_name'])	?	$form_data['father_name']	:	NULL;
		$ah_applicant['father_id_card'] 		= 	isset($form_data['father_id_card'])	?	$form_data['father_id_card']	:	NULL;
		$ah_applicant['certificate_dualism'] 	= 	isset($form_data['certificate_dualism'])	?	$form_data['certificate_dualism']	:	NULL;
		$ah_applicant['graduation_year'] 		= 	isset($form_data['graduation_year'])	?	$form_data['graduation_year']	:	NULL;
		$ah_applicant['school_percentage'] 		= 	isset($form_data['school_percentage'])	?	$form_data['school_percentage']	:	NULL;
		
		$ah_applicant['college_name'] 			= 	isset($form_data['college_name'])	?	$form_data['college_name']	:	NULL;
		$ah_applicant['specialization'] 		= 	isset($form_data['specialization'])	?	$form_data['specialization']	:	NULL;
		$ah_applicant['year_of_study'] 			= 	isset($form_data['year_of_study'])	?	$form_data['year_of_study']	:	NULL;
		$ah_applicant['last_grade_average'] 	= 	isset($form_data['last_grade_average'])	?	$form_data['last_grade_average']	:	NULL;
		
		$ah_applicant['salary'] 				= 	$form_data['salary'];
		$ah_applicant['income'] 				= 	$form_data['income'];
		$ah_applicant['source'] 				= 	$form_data['source'];
		
		$ah_applicant['musadra'] 				= 	$form_data['musadra'];
		$ah_applicant['qyama'] 					= 	$form_data['qyama'];
		$ah_applicant['lamusadra'] 				= 	$form_data['lamusadra'];
		
		$ah_applicant['current_situation'] 		= 	$form_data['current_situation'];
		$ah_applicant['ownershiptype'] 			= 	$form_data['ownershiptype'];
		$ah_applicant['ownershiptype_amount'] 	= 	$form_data['ownershiptype_amount'];
		$ah_applicant['building_type'] 			= 	$form_data['building_type'];
		
		$ah_applicant['number_of_rooms'] 		= 	$form_data['number_of_rooms'];
		$ah_applicant['utilities'] 				= 	$form_data['utilities'];
		$ah_applicant['furniture'] 				= 	$form_data['furniture'];
		$ah_applicant['bankid'] 				= 	$form_data['bankid'];
		
		$ah_applicant['bankbranchid'] 			= 	$form_data['bankbranchid'];
		$ah_applicant['accountnumber'] 			= 	$form_data['accountnumber'];
		$ah_applicant['accounttype'] 			= 	$form_data['accounttype'];
		
		if($applicantid!='')
		{
			if($applicantid!='' AND $refil_data!='')
			{
				if($form_data['record_type']	==	'new')
				{
					$this->db->insert('ah_applicant',$ah_applicant,json_encode($ah_applicant),$this->_login_userid);	
					$applicantid = $this->db->insert_id();
						
					$this->haya_model->save_steps($applicantid,1);
					
					$this->haya_model->update_steps($applicantid,1);
				}
				else
				{
					$this->db->where('applicantid',$applicantid);
					$this->db->update('ah_applicant',json_encode($ah_applicant),$this->_login_userid,$ah_applicant);
				}
			}
			else
			{
				$this->db->where('applicantid',$applicantid);
				$this->db->update('ah_applicant',json_encode($ah_applicant),$this->_login_userid,$ah_applicant);
			}
		}
		else
		{
			$this->db->insert('ah_applicant',$ah_applicant,json_encode($ah_applicant),$this->_login_userid);	
			$applicantid = $this->db->insert_id();
				
			$this->haya_model->save_steps($applicantid,1);
			$this->haya_model->update_steps($applicantid,1);
		}

			$applicantcode = $applicantid.$logged_in_user_detail['profile']->branchid.$this->_login_userid.$form_data['charity_type_id'];
			
			//Uploading Path
			$fullPath = './resources/applicants/'.$applicantcode.'/';
			
			//Deleting Data
			$this->db->query("DELETE FROM ah_applicant_wife WHERE applicantid='".$applicantid."'");
			$this->db->query("DELETE FROM ah_applicant_relation WHERE applicantid='".$applicantid."'");
			$this->db->query("UPDATE ah_applicant SET `applicantcode`='".$applicantcode."' WHERE applicantid='".$applicantid."'");
			
		foreach($form_data['relationname'] as $relationkey => $relationvalue)
		{
			if($relationvalue!='')
			{
				$ah_applicant_wife['applicantid']			=	$applicantid;
				$ah_applicant_wife['relationname']			=	$relationvalue;
				$ah_applicant_wife['relationdoc'] 			=	$form_data['relationdoc'][$relationkey];
				$ah_applicant_wife['relationnationality']	=	$form_data['relationnationality'][$relationkey];
				
				$this->db->insert('ah_applicant_wife',$ah_applicant_wife,json_encode($ah_applicant_wife),$this->_login_userid);
			}
		}
		
		foreach($form_data['relation_fullname'] as $rfkey => $rfvalue)
		{
			if($rfvalue!='')
			{
				$ah_applicant_relation = array(
					'applicantid'		=>	$applicantid,
					'relationtype'		=>	$form_data['relationtype'][$rfkey],
					'relationtype_text'	=>	$form_data['relationtype_text'][$rfkey],
					'relation_fullname'	=>	$rfvalue,
					'age'				=>	$form_data['age'][$rfkey],
					'professionid'		=>	$form_data['professionid'][$rfkey],
					'professionid_text'	=>	$form_data['professionid_text'][$rfkey],
					'monthly_income'	=>	$form_data['monthly_income'][$rfkey],
					'profession'		=>	$form_data['profession'][$rfkey],
					'sort_order'		=>	$rfkey);
					
				$this->db->insert('ah_applicant_relation',$ah_applicant_relation,json_encode($ah_applicant_relation),$this->_login_userid);
			}
		}
		
		
		if($_FILES['bankstatement']['name']!='')
		{
			$ah_applicant_bankstatement['bankstatement'] = $this->haya_model->upload_file('bankstatement',$fullPath);
			
			$this->db->where('applicantid',$applicantid);
			$this->db->update('ah_applicant',json_encode($ah_applicant_bankstatement),$this->_login_userid,$ah_applicant_bankstatement);
		}
		if($_FILES['school_certificate']['name']!='')
		{
			$school_certificate['school_certificate'] = $this->haya_model->upload_file('school_certificate',$fullPath);
			
			$this->db->where('applicantid',$applicantid);
			$this->db->update('ah_applicant',json_encode($school_certificate),$this->_login_userid,$school_certificate);
		}
		if($_FILES['college_certificate']['name']!='')
		{
			$college_certificate['college_certificate'] = $this->haya_model->upload_file('college_certificate',$fullPath);
			
			$this->db->where('applicantid',$applicantid);
			$this->db->update('ah_applicant',json_encode($college_certificate),$this->_login_userid,$college_certificate);
		}
		
		foreach($this->inq->allRequiredDocument($form_data['charity_type_id']) as $ctid) 
		{
			if($_FILES['doclist'.$ctid->documentid]['name']!='')
			{
				$this->db->query("DELETE FROM ah_applicant_documents WHERE applicantid='".$applicantid."' AND documentid='".$ctid->documentid."'");
				$ah_applicant_documents['applicantid'] = $applicantid;
				$ah_applicant_documents['userid'] = $this->_login_userid;
				$ah_applicant_documents['documentid'] = $ctid->documentid;
				$ah_applicant_documents['document'] = $this->haya_model->upload_file('doclist'.$ctid->documentid,$fullPath);
				$this->db->insert('ah_applicant_documents',$ah_applicant_documents,json_encode($ah_applicant_documents),$this->_login_userid);				
			}
			
		}
		
		if($this->input->post("notes")!='')
		{
			$ah_applicant_notes['applicantid'] = $applicantid;
			$ah_applicant_notes['userid'] = $this->_login_userid;
			$ah_applicant_notes['branchid'] = $logged_in_user_detail['profile']->branchid;
			$ah_applicant_notes['notes'] = $this->input->post("notes");
			$ah_applicant_notes['notestype'] = 'from_receipt';
			$this->db->insert('ah_applicant_notes',$ah_applicant_notes,json_encode($ah_applicant_notes),$this->_login_userid);
		}
		
		redirect(base_url().'inquiries/transactions');
		exit();
}
//----------------------------------------------------------------------
	/*
	* Insert User Record
	* @param array $data
	* return True
	*/
	function insert_user_detail($data)
	{
		$this->db->insert($this->_table_users,$data);
		
		return TRUE;
	}
/***********************************************/
	
//29/12/2014-------------------------------------START
    public function users_role()
    {
        $this->db->select('id,user_name,firstname,lastname,user_role_id');
        $this->db->from('admin_users');
        $this->db->where('status',1);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();

        $dropdown = '<select name="userid" id="userid" class="form-control">';
        $dropdown .= '<option value="">حدد المستخدم</option>';

        foreach($query->result() as $row)
        {
            $dropdown .= '<option value="'.$row->id.'" ';
            if($value==$row->id)
            {
                $dropdown .= 'selected="selected"';
            }
            $dropdown .= '>'.$row->firstname.' '.$row->lastname.' ('.$row->user_name.')</option>';
        }
        $dropdown .= '</select>';
        echo($dropdown);
    }
//29/12/2014-------------------------------------END
//10/01/2015-------------------------------------START
    function user_perm_list()
    {
        $this->db->select('id,user_name,firstname,lastname,user_role_id');
        $this->db->from('admin_users');
        $this->db->where('status',1);
        $this->db->order_by("firstname", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
//----------------------------------------------------------------------    
	/*
	* 
	*/
    function parent_module()
    {
        $this->db->select('moduleid,module_name,module_icon');
        $this->db->from('mh_modules');
        $this->db->where('module_parent',0);
        $this->db->where('module_status','A');
        $this->db->order_by("module_order", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
//----------------------------------------------------------------------    
	/*
	* 
	*/	
    function childe_module($parentid)
    {
        $this->db->select('moduleid,module_name,module_icon');
        $this->db->from('mh_modules');
        $this->db->where('module_parent',$parentid);
        //$this->db->where('module_status','A');
        $this->db->order_by("module_order", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
//10/01/2015	-------------------------------------	END
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function getAprovalList($applicant_id){
		$sql = "SELECT 
			  SUM(
				sealed_company + commercial_papers + municipal_contractrent + membership_certificate +company_general_authority+open_account+check_book+registration_zip
			  ) AS total 
			FROM
			  `check_list` AS cl 
			WHERE cl.`applicant_id` = '".$applicant_id."' ";

			$q = $this->db->query($sql);
			return $r =  $q->result();
	}
//------------------------------------------------------------------------

	/*
	*
	*
	*/
	function deleteTempDelete()
	{
		// Login Query
		$userid = $this->session->userdata('userid');
		$this->db->where('userid',$userid);
		$query = $this->db->get('applicant_temp_document');	
			
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $temp_data)
			{
				$path = APPPATH.'../upload_files/'.$userid.'/'.$temp_data->documentname;
				if(file_exists($path))
				{
					unlink($path); //Delete temporary uploaded files
				}
				$this->db->where("documentid",$temp_data->documentid);
				$this->db->delete('applicant_temp_document');
			}
		}
	}
	
//------------------------------------------------------------------------

	/*
	*
	*
	*/	
	function checkApplicaitonProject($id)
	{
		$this->db->where('applicant_id',$id);
		$query = $this->db->get('applicant_project');
		
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

//----------------------------------------------------------------------
	/*
	* Insert User Record
	* @param array $data
	* return True
	*/
	function insert_donation_product($data)
	{
		$this->db->insert($this->_table_donate,$data);
		
		return TRUE;
	}
//----------------------------------------------------------------------
    /*
    * insert user_profile method
    */
    function insert_user_profile($data)
    {
        $this->db->insert($this->_table_user_profile,$data);

        return TRUE;
    }
//----------------------------------------------------------------------
	/*
	* User login
	* @param string $user_email
	* @param string $password
	* return Login User Data
	*/
	function login_user($user_email,$password)
	{
		//Login Query
		$this->db->where('email',$user_email);
		$this->db->where('password',md5($password));
		$query = $this->db->get($this->_table_users);
		
		//Check if Result is Greater Than Zero
		
		if($query->num_rows() > 0)
		{
			$this->session->set_userdata('user_id',$query->row()->user_id);
			return $query->row();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/		
	function loan_data($id)
	{
		//Login Query
		$this->db->where('loan_caculate_id',$id);
		$query = $this->db->get('loan_calculate');
		
		//Check if Result is Greater Than Zero
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/		
	function get_childLoan_data($id)
	{
		//Login Query
		$this->db->where('parent_id',$id);
		$query = $this->db->get('loan_calculate');
		
		//Check if Result is Greater Than Zero
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function get_app_ids($user_id)
	{
		$query = $this->db->query(
		"SELECT
		GROUP_CONCAT(applicantid) AS applicants
		FROM applicant_process_log WHERE userid='$user_id' AND stepsid = '1'");
		if($query->num_rows() > 0)
		{
		return $query->row();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function get_all_applicatnts_data($data)
	{
		//Login Query
		$this->db->where_in('applicant_id',$data);
		$query = $this->db->get('applicants');
		 
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
		return $query->result();
		}
	}
//----------------------------------------------------------------------
	
	function getChild($parentId){
			
			$this->db->where('parent_id',$parentId);
		$query = $this->db->get('loan_calculate');
		
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//------------------------------------------------------------------------

    /**
     * 
     * Insert User Data for Registration
     * @param array $data
     * return integer
     */
	function update_user($userid,$data)
	{
		$update['firstname'] = $data['firstname'];
		$update['lastname'] = $data['lastname'];
		$update['password'] = ($data['password']);	
		$update['email'] = $data['email'];
		$update['mobile_number'] = $data['number'];
		$update['number'] = $data['number'];
		$update['about_user'] = $data['about_user'];
		$update['branch_id'] = $data['branch_id'];
		$update['user_role_id'] = $data['user_role_id'];
		$update['user_parent_role'] = $data['user_parent_role'];
		$update['zyarapermission'] = $data['zyarapermission'];
		$this->db->where('id',$userid);
		$this->db->update('admin_users', $update);		
		return TRUE;
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/		
	public function getLastDetail($tempid='',$format='array')
	{
		$this->session->unset_userdata('inq_id');		
		$this->db->select('*');
		$this->db->from('main');		
		$this->db->where('tempid',$tempid);
		$this->db->order_by("applicantdate", "DESC"); 
		$this->db->limit(1);
		$tempMain = $this->db->get();
		if($tempMain->num_rows() > 0)
		{
			foreach($tempMain->result() as $data)
			{				
				$arr['main'] = $data;
				$tempid = $data->tempid;							
				$tempMain->free_result(); //Clean Result Set
				//Getting Applicant Data
				$this->db->select('*');
				$this->db->from('main_applicant');		
				$this->db->where('tempid',$tempid);
				$this->db->order_by("applicantid", "ASC");
				$tempApplicant = $this->db->get();
				if($tempApplicant->num_rows() > 0)
				{
					foreach($tempApplicant->result() as $app)
					{
                        $arr['main']->applicant[] = $app;
						//Getting Phone Data
						$this->db->select('*');
						$this->db->from('main_phone');		
						$this->db->where('tempid',$tempid);
						$this->db->where('applicantid',$app->applicantid);
						$this->db->order_by("phoneid", "ASC");
						$tempPhone = $this->db->get();
						if($tempPhone->num_rows() > 0)
						{
							foreach($tempPhone->result() as $phones)
							{
								$arr['main']->phones[$app->applicantid][] = $phones;						
							}
							$tempPhone->free_result(); //Clean Result Set
						}					
					}
					$tempApplicant->free_result(); //Clean Result Set
				}				
				//Getting Inquiry Type Data
				$this->db->select('*');
				$this->db->from('main_inquirytype');		
				$this->db->where('tempid',$data->tempid);
				$this->db->order_by("inqid", "ASC");
				$tempIType = $this->db->get();
				if($tempIType->num_rows() > 0)
				{
					foreach($tempIType->result() as $itype)
					{
						$arr['main']->inquery[] = $itype;
					}
					$tempIType->free_result(); //Clean Result Set
				}				
				//Getting Notes Data
				$this->db->select('*');
				$this->db->from('main_notes');		
				$this->db->where('tempid',$data->tempid);
				$this->db->order_by("notesid", "ASC");
				$tempNotes = $this->db->get();
				if($tempNotes->num_rows() > 0)
				{
					foreach($tempNotes->result() as $notes)
					{
						$arr['main']->notes[] = $notes;
						//$arr['notes'][] = $notes;
					}
					$tempNotes->free_result(); //Clean Result Set
				}				
								
			}
		}
		if($format=='json')
		{
			echo json_encode($arr);
		}
		else
		{
		return $arr;
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_data_for_store($limit,$start){
		//Login Query
		$this->db->where('permission','1');
		$this->db->limit($limit,$start);	
		$query = $this->db->get($this->_table_donate);
		
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function getsave_document(){
		//Login Query
		$userid = $this->session->userdata('userid');
		$query = $this->db->query("SELECT * FROM applicant_temp_document WHERE userid='".$userid."'");		
		if($query->num_rows() > 0)
		{
			foreach($query->result_array() as $ddx)
			{
				$ar[] = $ddx;
			}
			return $ar;
		}
	}
//----------------------------------------------------------------------
	/*
	* Get All Records From Giving Table
	*/
	public function get_rows($table_name)
	{
		$count_all = $this->db->get($table_name);
		if($count_all->num_rows() > 0)
		{
			return $count_all->num_rows();
		}
	}
//----------------------------------------------------------------------
	/*
	* Insert Applied Product Record into Database
	* Return True 
	*/
	public function applied_donation($data)
	{
		$this->db->insert($this->_table_applied_donation,$data);
		return TRUE;
	}
//----------------------------------------------------------------------
	/*
	* Insert Applied Product Record into Database
	* Return True 
	*/
	public function add_user_registration($data)
	{
		$this->db->insert('admin_users',$data);
		return TRUE;
	}

//----------------------------------------------------------------------
	/*
	* Get Applied Product Record into Database
	* Return True 
	*/
	public function get_applied_detail($user_id,$donate_id)
	{
		$this->db->where('user_id',$user_id);
		$this->db->where('donation_id',$donate_id);

		$query = $this->db->get($this->_table_applied_donation);
		
		if ($query->num_rows() > 0)
		{
			return $query->row()->donation_id;
		}
	}
//----------------------------------------------------------------------
	/*
	* Search Applied Products if 
	* Return True 
	*/
	public function donation_id_exists()
	{
		$this->db->where('user_id',$this->session->userdata('user_id'));
		$this->db->select('donation_id');
		$query = $this->db->get($this->_table_applied_donation);
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	public function update_db($table,$data,$sms_id)
	{
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('sms_id',$sms_id);
		$this->db->update($table,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}

//29/12/2014-------------------------------------START

//29/12/2014-------------------------------------END

	/*
	* Search Applied Products if 
	* Return True 
	*/
	public function getInquiriesSms($type)
	{
		$this->db->where('type',$type);
		$this->db->where('status',1);
		$this->db->select('*');
		$this->db->order_by('sms_order','ASC');
		$query = $this->db->get($this->_table_sms_management);
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function getSms($module_id,$type)
	{
		$this->db->where('sms_module_id',$module_id);
		$this->db->where('type',$type);
		
		if($module_id  == 1)
		{
			$this->db->join('register_auditors',"auditor_id=sms_receiver_id");
		}
		
		$this->db->order_by("sms_id", "DESC"); 
		$this->db->select('*');
		$query = $this->db->get('sms_history');
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
		
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_sms_history($module_id,$userid)
	{
		if($module_id	==	'1')
		{
			$this->db->select('sms_history.*,admin_users.firstname,admin_users.lastname,main_applicant.first_name,main_applicant.middle_name,main_applicant.last_name,main_applicant.family_name');
		}
		else
		{
			$this->db->select('sms_history.*,admin_users.firstname,admin_users.lastname,applicants.applicant_first_name,applicants.applicant_middle_name,applicants.applicant_last_name,applicants.applicant_sur_name');
		}
		
		$this->db->from('sms_history');
		
		if($module_id	==	'1')
		{
			$this->db->join('main_applicant',"main_applicant.tempid=sms_history.sms_receiver_id");
		}
		else
		{
			$this->db->join('applicants',"applicants.applicant_id=sms_history.sms_receiver_id");
		}
		
		$this->db->join('admin_users',"admin_users.id=sms_history.sms_sender_id");
		$this->db->where('sms_module_id',$module_id);
		$this->db->where('sms_receiver_id',$userid);
		$query = $this->db->get();
		
		//echo $this->db->last_query();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//---------------------------------------------------------------------	
	/*
	* get info of applicant comitte_decision
	* @param int $applicant_id
	* Created by M.Ahmed
	*/
	function getInquiresOfRequestChangePhaseFive($applicant_id)
	{
		// select info
		$this->db->where('applicant_id',$applicant_id);
		$query = $this->db->get('comitte_decision');
		
		//Check if Result is Greater Than Zero
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function new_inquery()
	{	
		$userid = $this->session->userdata('userid');
		$getQuery = $this->db->query("SELECT tempid FROM main WHERE tempid_value=0 AND userid=".$userid." LIMIT 1;");
		if($getQuery->num_rows() > 0)
		{
			foreach($getQuery->result() as $trex)
			{
				return $this->getLastDetail($trex->tempid);
			}
		}
		else
		{
			$this->db->trans_start();
			$this->db->insert('main',array('userid'=>$userid,'branchid'=>get_branch_id()));			
			$tempid = $this->db->insert_id();
			for($j=0; $j<=3; $j++)
			{
				$this->db->insert('main_applicant',array('tempid'=>$tempid,'applicanttype'=>'ذكر'));
				$applicantid = $this->db->insert_id();
				$this->db->insert('main_phone',array('tempid'=>$tempid,'applicantid'=>$applicantid));
			}
			$this->db->trans_complete();
			return $this->getLastDetail($tempid);
		}	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function sms_delete($sms_id)
	{
		$this->db->where("sms_id",$sms_id);
		$this->db->delete('sms_history');
		
		return true; 
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function checkData($table,$returnKey,$whereKey,$value){
		$this->db->select('*');
		$this->db->from($table);		
		$this->db->where($whereKey,$value);
		$checkOut = $this->db->get(); 
		$row = $checkOut->num_rows();
		$temp_main_data	=	(array)$checkOut->row();
		if($row>0){
		
			return 	$temp_main_data[''.$returnKey.''];	;
		}
		else{
			false;
		}
        
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function addUpdate($table,$data){
		
		//echo $C->db->on_duplicate('finance_project',$insertData);
		echo $table;
		echo "<pre>";
		print_r($data);
		//return $this->db->on_duplicate($table,$data);	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_all_users()
	{
		$query = $this->db->query(
		"SELECT
		admin_users.id
		,admin_users.user_name
		,admin_users.firstname
		,admin_users.lastname
		,admin_users.email
		,admin_users.number
		,branches.branch_name,
		COUNT(ap.`applicantid`) AS total
		FROM admin_users
		INNER JOIN branches
		ON (admin_users.branch_id = branches.branch_id)
		LEFT JOIN `applicant_process_log` AS ap 
		ON ap.`userid` = admin_users.`id` AND ap.`stepsid` = '1'
		Where admin_users.`branch_id` !=''
		GROUP BY admin_users.`id`
		ORDER BY total DESC");
		if($query->num_rows() > 0)
		{
		return $query->result();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_all_banks()
	{
		$query = $this->db->query(
		"SELECT 
		  admin_users.id,
		  admin_users.user_name,
		  admin_users.firstname,
		  admin_users.lastname,
		  admin_users.email,
		  admin_users.number,
		  bb.branch_name   
		FROM
		  `admin_users` 
		  INNER JOIN `bank_branches` AS bb 
			ON bb.`branch_id` = admin_users.`bank_branch_id` ");
		if($query->num_rows() > 0)
		{
		return $query->result();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function add_data_into_main($tempid	=	'')
	{
		$userid = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->from('temp_main');		
		$this->db->where('tempid',$tempid);
		$checkOut = $this->db->get(); 
		$checkOut->num_rows();
		
		
		$this->db->select('*');
		$this->db->from('main');		
		$this->db->where('tempid',$tempid);
		$checkOut2 = $this->db->get(); 
		$checkOut2->num_rows();
		
		if($tempid)
		$temp_main_data	=	(array)$checkOut->row();

		$this->db->insert('main',$temp_main_data);			
		$tempid = $this->db->insert_id();
				
		$applicantQuery = $this->db->query("SELECT * FROM temp_main_applicant WHERE tempid='".$tempid."'");
		
		foreach($applicantQuery->result() as $applicant)
		{
			if($applicant->first_name !="" && $applicant->idcard !=""){
			$temp_main_applicant = array('tempid'=>$applicant->tempid,
			'first_name'=>$applicant->first_name,
			'middle_name'=>$applicant->middle_name,
			'last_name'=>$applicant->last_name,
			'family_name'=>$applicant->family_name,
			'applicanttype'=>$applicant->applicanttype,
			'idcard'=>$applicant->idcard);
			$this->db->insert('main_applicant',$temp_main_applicant);
			$applicantid = $this->db->insert_id();
			}
						$phoneQuery = $this->db->query("SELECT * FROM temp_main_phone WHERE tempid='".$tempid."' AND applicantid='".$applicant->applicantid."'");
						foreach($phoneQuery->result() as $phoneres)
						{
							$temp_main_phone = array('tempid'=>$tempid,'applicantid'=>$applicantid,'phonenumber'=>$phoneres->phonenumber);
							$this->db->insert('main_phone',$temp_main_phone);
						}
						/*$this->db->select('*');
						$this->db->from('temp_main_phone');		
						$this->db->where('tempid',$tempid);
						$temp_main_phone = $this->db->get();
						$temp_main_phone	=	(array)$temp_main_phone->row();
						$temp_main_phone['applicantid']	=	$id;*/
						
						
		}
		
/*		$this->db->select('*');
		$this->db->from('temp_main_applicant');		
		$this->db->where('tempid',$tempid);
		$temp_main_applicant = $this->db->get();
		$temp_main_applicant	=	(array)$temp_main_applicant->row();

		$this->db->insert('main_applicant',$temp_main_applicant);			
		$id = $this->db->insert_id();*/
		
		$this->db->select('*');
		$this->db->from('temp_main_inquirytype');		
		$this->db->where('tempid',$tempid);
		$temp_main_inquirytype = $this->db->get();
		$temp_main_inquirytype	=	(array)$temp_main_inquirytype->row();
		
		$this->db->insert('main_inquirytype',$temp_main_inquirytype);			
		
		//$tempid = $this->db->insert_id();
		$this->db->delete('temp_main_notes', array('tempid' =>$tempid,'userid'=>'0'));
		///
		$this->db->select('*');
		$this->db->from('temp_main_notes');		
		$this->db->where('tempid',$tempid);
		$temp_main_notes = $this->db->get();
		$temp_main_notes	=	(array)$temp_main_notes->row();
		
		$this->db->insert('main_notes',$temp_main_notes);			
		//$tempid = $this->db->insert_id();
		
		
		$this->session->unset_userdata('inq_id');
		$this->db->delete('temp_main', array('userid' => $userid));
			$mobileNumbers = get_mobilenumbers($tempid);
			
			$this->db->where('tempid',$tempid);
			$this->db->update('main',array('tempid_value'=>applicant_number($tempid)));
			$userNumber = applicant_number($tempid);
			send_sms(1,$mobileNumbers,$userid,$userNumber);
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function reset_inquery()
	{
		$this->session->unset_userdata('inq_id');
		$userid = $this->session->userdata('userid');
		$this->db->trans_start();
		$this->db->query("DELETE FROM main WHERE idcard='' AND userid='".$userid."'");
		$this->db->insert('main',array('userid'=>$userid,'branchid'=>get_branch_id()));			
		$tempid = $this->db->insert_id();
		$this->session->set_userdata('inq_id',$tempid);	
		for($a==0; $a<4; $a++)
		{	$this->db->insert('main_applicant',array('tempid'=>$tempid,'applicanttype'=>'ذكر'));
			$applicantid = $this->db->insert_id();
			$this->db->insert('main_phone',array('tempid'=>$tempid,'applicantid'=>$applicantid));
		}
		$this->db->trans_complete();
		redirect(base_url().'inquiries/newinquery');			
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function getInquiries(){
		$this->db->select('*');
		$this->db->from('register_auditors');
		//$this->db->join('register_auditors',"auditor_id=sms_receiver_id");
		$this->db->order_by("auditor_id", "DESC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result();
		} 
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function getNotes($applicant_id)
	{
		$this->db->select('apn.notesdate, apn.notestype, apn.notes, ah_userprofile.fullname, ah_branchs.branchname');
		$this->db->from('ah_applicant_notes AS apn');
		$this->db->join('ah_userprofile',"apn.userid=ah_userprofile.userid");
		$this->db->join('ah_branchs',"ah_branchs.branchid=apn.branchid");
		$this->db->where("apn.applicantid",$applicant_id);
		$this->db->order_by("apn.notesdate","desc");
		$notes = $this->db->get();
		return $notes->result();
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function getNotesCount($applicant_id)
	{
		$this->db->select('apn.notesdate, apn.notestype, apn.notes, ah_userprofile.fullname, ah_branchs.branchname');
		$this->db->from('ah_applicant_notes AS apn');
		$this->db->join('ah_userprofile',"apn.userid=ah_userprofile.userid");
		$this->db->join('ah_branchs',"ah_branchs.branchid=apn.branchid");
		$this->db->where("apn.applicantid",$applicant_id);
		$this->db->order_by("apn.notesdate","desc");
		$notes = $this->db->get();
		
		if($notes->num_rows()	>	0)
		{
			return $notes->num_rows();
		}
		else
		{
			return '0';
		}
		
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
    function muragain_branch_count()
    {
        $mbc = $this->db->query("SELECT branches.`branch_name`,COUNT(admin_users.`id`) AS cnt,branches.`branch_id` FROM `branches`
          INNER JOIN `admin_users` ON (`branches`.`branch_id` = `admin_users`.`branch_id`)
          INNER JOIN `main`  ON (`admin_users`.`id` = `main`.`userid`)
		  INNER JOIN `main_applicant` ON (`main`.`tempid` = `main_applicant`.`tempid`) WHERE `main_applicant`.`idcard`!='' AND `main_applicant`.`idcard`!='0'		  
		   GROUP BY branches.`branch_id` ORDER BY cnt DESC LIMIT 0,10;");
        $xz = 0;
        $mindex = 0;
        $arr[0]['b_name'] = 'عرض الكل ';
        $arr[0]['b_id'] = '0';
        $arr[0]['class'] = 'active';
        foreach($mbc->result() as $qdata)
        {
            $mindex++;
            $arr[$mindex] = array('b_name'=>$qdata->branch_name,'b_count'=>$qdata->cnt,'b_id'=>$qdata->branch_id,'class'=>'');
            $xz += $qdata->cnt;
        }
        $arr[0]['b_count'] = $xz;
        return $arr;
    }
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function getmaindata($branchid='')
	{
		$query = "SELECT `main`.`tempid`,`main`.`applicantdate` FROM `main` 
		INNER JOIN `admin_users` ON (`main`.`userid` = `admin_users`.`id`)
		INNER JOIN `main_applicant` ON (`main`.`tempid` = `main_applicant`.`tempid`) WHERE `main_applicant`.`idcard`!='' AND `main_applicant`.`idcard`!='0' AND ";
		
		if($branchid!='' && $branchid!=0)
		{
			$query .= " `admin_users`.`branch_id`='".$branchid."' AND ";
		}
		$query .= " `admin_users`.`branch_id` > '0' GROUP BY `main`.`tempid` ORDER BY `main`.`applicantdate` DESC ";	
		$qx = $this->db->query($query);
		
		if ($qx->num_rows() > 0)
		{
			return $qx->result();
		} 
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function getLastInquery($tempid='')
	{
		$userid = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->from('main');		
		$this->db->where('userid',$userid);
		$this->db->order_by("applicantdate", "DESC"); 
		$this->db->limit(1);
		$tempMain = $this->db->get();
		if($tempMain->num_rows() > 0)
		{
			foreach($tempMain->result() as $data)
			{				
				$arr['main'] = $data;
				$tempid = $data->tempid;							
				$tempMain->free_result(); //Clean Result Set
				//Getting Applicant Data
				$this->db->select('*');
				$this->db->from('main_applicant');		
				$this->db->where('tempid',$tempid);
				$this->db->order_by("applicantid", "ASC");
				$tempApplicant = $this->db->get();
				if($tempApplicant->num_rows() > 0)
				{
					foreach($tempApplicant->result() as $app)
					{
						$arr['main']->applicant[] = $app;
						//Getting Phone Data
						$this->db->select('*');
						$this->db->from('main_phone');		
						$this->db->where('tempid',$tempid);
						$this->db->where('applicantid',$app->applicantid);
						$this->db->order_by("phoneid", "ASC");
						$tempPhone = $this->db->get();
						if($tempPhone->num_rows() > 0)
						{
							foreach($tempPhone->result() as $phones)
							{
								$arr['main']->phones[$app->applicantid][] = $phones;						
							}
							$tempPhone->free_result(); //Clean Result Set
						}					
					}
					$tempApplicant->free_result(); //Clean Result Set
				}				
				//Getting Inquiry Type Data
				$this->db->select('*');
				$this->db->from('main_inquirytype');		
				$this->db->where('tempid',$data->tempid);
				$this->db->order_by("inqid", "ASC");
				$tempIType = $this->db->get();
				if($tempIType->num_rows() > 0)
				{
					foreach($tempIType->result() as $itype)
					{
						$arr['main']->inquery[] = $itype;
					}
					$tempIType->free_result(); //Clean Result Set
				}				
				//Getting Notes Data
				$this->db->select('*');
				$this->db->from('main_notes');		
				$this->db->where('tempid',$data->tempid);
				$this->db->order_by("notesid", "ASC");
				$tempNotes = $this->db->get();
				if($tempNotes->num_rows() > 0)
				{
					foreach($tempNotes->result() as $notes)
					{
						$arr['main']->notes[] = $notes;
						//$arr['notes'][] = $notes;
					}
					$tempNotes->free_result(); //Clean Result Set
				}				
								
			}
		}
		return $arr;
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function check_record($table,$id){
		$this->db->select('*');
		$this->db->from($table);			
		$this->db->where('applicant_id',$id);
		$tempMain = $this->db->get();	
		if($tempMain->num_rows() > 0)
		{
			return $tempMain->result();
		}
		else{
			return false;
		}	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function getSeniorPeople($applicantid){
		$sql = $this->db->query("SELECT peopleid,notes,notesdate FROM `applicant_touqeeyat` WHERE applicantid = '".$applicantid."'");
		foreach($sql->result() as $gsp)
		{
			$arr[$gsp->peopleid]['peopleid'] = $gsp->peopleid;
			$arr[$gsp->peopleid]['notes'] = $gsp->notes;
			$arr[$gsp->peopleid]['notesdate'] = $gsp->notesdate;
		}
		return $arr;
		
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_all_applicatnts($branchid='')
	{	
		$sql = "SELECT a.*, cd.`commitee_decision_type`, br.`is_reject`,br.`loan_id` FROM `applicants` AS a 
		LEFT JOIN `comitte_decision` AS cd ON cd.`applicant_id` = a.`applicant_id` 
		LEFT JOIN `bank_response` AS br ON br.`applicant_id` = a.`applicant_id` 
		LEFT JOIN `admin_users` AS au ON au.`id` = a.`addedby`  ";
		if($branchid!='')
		{	$sql .= " WHERE au.`branch_id`='".$branchid."' ";	};
			$sql .= " ORDER BY a.applicant_id DESC ";
							
		$query = $this->db->query($sql);	
						
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_all_reject_bank()
	{
		$sql = "SELECT *
				FROM
			  `applicants` AS a
			  INNER JOIN `bank_response` ON `bank_response`.`applicant_id` = a.`applicant_id`
			  WHERE `bank_response`.`is_reject` = '1'";
							
		$query = $this->db->query($sql);					
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_mukhair()
	{
		$query = "SELECT applicants.* FROM `applicant_loans` 
		  INNER JOIN `applicants` ON (`applicant_loans`.`applicant_id` = `applicants`.`applicant_id`) 
		  LEFT JOIN `study_analysis_demand` ON (`study_analysis_demand`.`applicant_id` = `applicants`.`applicant_id`) 
		  WHERE 
		  (study_analysis_demand.`credit_risk` = 'نعم' AND 
		  study_analysis_demand.`is_musanif` = 'classified') OR
		  `study_analysis_demand`.`credit_risk` IS NULL
		  
		  ORDER BY `applicants`.applicant_regitered_date DESC;";
		  
		$result = $this->db->query($query);
		return $r =  $result->result();

	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function getStudy_analyze($id){
		$sql = "SELECT * FROM `study_analysis_demand` AS sa WHERE sa.`applicant_id` = '".$id."'";
			$result = $this->db->query($sql);
			return $r =  $result->row();
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function getAprovalStepData()
	{
		$sql= "SELECT 
		  * 
		FROM
		  `applicants` AS a 
		  INNER JOIN `check_list` AS cl 
			ON cl.`applicant_id` = a.`applicant_id` 
		WHERE cl.`sealed_company` = '1' 
		  AND cl.`registration_zip` = '1' 
		  AND cl.`municipal_contractrent` = '1'  
		  AND cl.`open_account` = '1' 
		  AND cl.`membership_certificate` = '1'
		  AND cl.`company_general_authority` = '1'
		  AND cl.`commercial_papers` = '1'
		  AND cl.`check_book` = '1'";
		  $q = $this->db->query($sql);
		return $r =  $q->result();

	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function getSmsHistory($id)
	{
		$this->db->select('sh.*');
		$this->db->from('sms_history as sh');
		$this->db->join('admin_users AS au',"au.id=sh.id");
		$this->db->where('sh.sms_receiver_id',$id);
		$this->db->order_by("sh.sms_id", "DESC");
		$tempNotes = $this->db->get();
		if($tempNotes->num_rows() > 0)
		{
			return $tempNotes->result();
		}
	}

//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_single_applicatnt($id)
	{
		//Login Query
		$this->db->where("applicant_id",$id);
		$query = $this->db->get('applicants');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_applicant_evolution($id)
	{
		//Login Query
		$this->db->where("applicant_id",$id);
		$query = $this->db->get('project_evolution');		
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}

//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_single_auditor($id)
	{
		//Login Query
		$this->db->where("auditor_id",$id);
		$query = $this->db->get('register_auditors');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_main_info($id)
	{
		//Login Query
		$this->db->where("tempid",$id);
		$query = $this->db->get('main');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_phone_number($id)
	{
		//Login Query
		$this->db->where("tempid",$id);
		$query = $this->db->get('main_phone');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_notes($id)
	{
		//Login Query
		$query = $this->db->query("SELECT
						CONCAT(`admin_users`.`firstname`,' ',`admin_users`.`lastname`) AS adminname
						, `admin_users`.`user_name`
						, `main_notes`.`tempid`
						, `main_notes`.`userid`
						, `main_notes`.`notesdate`
						, `main_notes`.`notestext`
						, `main_notes`.`inquiry_text`
						, `main_notes`.`notesip`
						, `main_notes`.`inquerytype`
						, `main_notes`.`notesid`
					FROM
						`admin_users`
						INNER JOIN `main_notes` ON (`admin_users`.`id` = `main_notes`.`userid`)
						WHERE `main_notes`.`tempid`='".$id."' ORDER BY `main_notes`.`notesdate` DESC;");

			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_last_note($id)
	{
		//Login Query
		$this->db->where("tempid",$id);
		$this->db->order_by("notesdate","DESC");
		$this->db->limit("1");
		$query = $this->db->get('main_notes');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_type_name($typeid)
	{
		//Login Query
		$this->db->where("list_id",$typeid);

		$query = $this->db->get('list_management');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->list_name;
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_gender($typeid)
	{
		//Login Query
		$this->db->where("tempid",$typeid);

		$query = $this->db->get('main_applicant');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->applicanttype;
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_user_name_of_added($id)
	{
		//Login Query
		$this->db->select("firstname,lastname");
		
		$this->db->where("id",$id);

		$query = $this->db->get('admin_users');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_user_name($id)
	{
		//Login Query
		$this->db->where("tempid",$id);

		$query = $this->db->get('main_applicant');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_province_name($provinceid)
	{
		$this->db->where('ID',$provinceid);

		$query = $this->db->get('election_reigons');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->REIGONNAME;
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_wilayats_name($wilayatsid)
	{
		$this->db->where('WILAYATID',$wilayatsid);

		$query = $this->db->get('election_wilayats');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->WILAYATNAME;
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_tab_data($table_name,$app_id)
	{
		$this->db->where("applicant_id",$app_id); 
		$query = $this->db->get($table_name);
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}	
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_branch_name($branchid)
	{
		$this->db->where("branch_id",$branchid); 
		$query = $this->db->get('branches');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->branch_name;
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_user_role($role_id)
	{
		$this->db->select('role_name');
		$this->db->where("role_id",$role_id); 
		$this->db->where('role_parent_id','0');
		
		$query = $this->db->get('user_roles');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->role_name;
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_user_child_role($parent_role)
	{
		//$this->db->select('role_name');
		$this->db->where("role_id",$parent_role); 
		
		$query = $this->db->get('user_roles');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->role_name;
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function marajeen_phones()
	{
		$query = $this->db->get('main_phone');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function tasjeel_phones()
	{
		$query = $this->db->get('applicant_phones');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* Insert Banner Record
	* @param array $data
	* return True
	*/
	function add_banner($data)
	{
		//print_r($data);
		$this->db->insert('system_images',$data);
		
		
		
		return TRUE;
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_all_banners(){
		
		$query = $this->db->get('system_images');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function delete_banner($banner_id)
	{	
		$this->db->where("imageid",$banner_id);
		$this->db->delete('system_images');
		
		return true; 	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function delete_banner_ajax($image)
	{
		$this->db->where('banner_image',$image);
		$query 	= 	$this->db->get('system_images');
		$img_id	=	$query->row()->imageid;

		if($query->num_rows() > 0)
		{
			$this->db->where("imageid",$img_id);
			$this->db->delete('system_images');

		}
		
		if(file_exists(rootpath.'upload_files/banners/'.$image))
		{
			unlink(rootpath.'upload_files/banners/'.$image);	
		}
		
		return true; 	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function delete_image($image,$type ='')
	{
		
		if($type !=""){
				if($type == 'commetti'){
					

					$this->db->where('image_name',$image);
				$query = $this->db->get('comitte_decision_image');
				$guarantee_img_id	=	 $query->row()->id;
				
				if($query->num_rows() > 0){
				$this->db->where("id",$guarantee_img_id);
				$this->db->delete('comitte_decision_image');
				
				}
					
				}
		}
		else{
				$this->db->where('gurarntee_image',$image);
				$query = $this->db->get('guanttee_attachment');
				$guarantee_img_id	=	 $query->row()->guarantee_img_id;
				if($query->num_rows() > 0){
				$this->db->where("guarantee_img_id",$guarantee_img_id);
				$this->db->delete('guanttee_attachment');
				
				}
		
		}

		if(file_exists(rootpath.'upload_files/documents/'.$image)){
			unlink(rootpath.'upload_files/documents/'.$image);	
		}
		
		
		return true; 	
	}
//--------------------------------------------------------------------------	
	/*
	*
	* Delete Document File From Database asa well from Folder
	*
	*/
	function delete_document($image)
	{
		$this->db->where('documentname',$image);
		$query = $this->db->get('applicant_document');
		$documentid	=	 $query->row()->documentid;

		if($query->num_rows() > 0)
		{
			$this->db->where("documentid",$documentid);
			$this->db->delete('applicant_document');

		}
		
		if(file_exists(rootpath.'upload_files/documents/'.$image))
		{
			unlink(rootpath.'upload_files/documents/'.$image);	
		}
		
		return true; 	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	public function update_status($image_id,$data)
	{
		$this->db->where('imageid',$image_id);
		$this->db->update('system_images', $data);
		
		return TRUE;
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function get_banner_detail($image_id)
	{
		$this->db->where('imageid',$image_id);
		
		$query = $this->db->get('system_images');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function update_banner($image_id,$data)
	{
		$this->db->where('imageid',$image_id);
		$this->db->update('system_images', $data);
		
		return TRUE;
	}
//-------------------------------------------------------------

	/*
	* Delete APPLICANT Document
	* @param $docid	integer
	* RETURN TRUE
	*/
	public function delete_applicant_document($docid)
	{
		$this->db->select('doc.document,ap.applicantcode');
        $this->db->from('ah_applicant_documents AS doc');
		$this->db->join('ah_applicant AS ap','doc.applicantid=ap.applicantid');
        $this->db->where('doc.appli_doc_id',$docid);
		$query = $this->db->get();
				
		if($query->num_rows() > 0)
		{
			$document = $query->row();
			$fullPath = './resources/applicants/'.$document->applicantcode.'/'.$document->document;
			
			unlink($fullPath);
			$this->db->query("DELETE FROM ah_applicant_documents WHERE appli_doc_id='".$docid."' ");
		}
			
		return TRUE;
	}
//-------------------------------------------------------------

	/*
	* GET details according to parameters
	* @param $select_fields STRING
	* @param $condition ARRAY
	* @param $charity_type_id INTEGER
	* @param $type STRING
	* return JSON DATA
	*/
	public function get_detail_by_param($select_fields,$condition,$charity_type_id,$type)
	{
		$this->db->select($select_fields);
		$this->db->from('ah_applicant');
		$this->db->like($condition);
		$this->db->order_by('applicantid','DESC');
		$this->db->limit('1');
		$query = $this->db->get();
			
		if($query->num_rows() > 0)
		{
			$other_type	=	NULL;
			foreach($query->result() as $data)
			{
				if($type	==	'MOTHER CARD')
				{
					$refil_data_url	=	''.base_url().'inquiries/applicants_mother_help/'.$data->applicantid.'/'.$data->mother_id_card.'';
				}
				else if($type	==	'FATHER CARD')
				{
					$refil_data_url	=	''.base_url().'inquiries/applicants_father_help/'.$data->applicantid.'/'.$data->father_id_card.'';
				}
				else
				{
					$refil_data_url	=	charity_edit_url($charity_type_id).$data->applicantid.'/refilData';
				}
				
				if($type	==	'CARD')
				{
					$fullname = $data->fullname.' ('.$data->idcard_number.')';
				}
				else if($type	==	'MOTHER CARD')
				{
					$other_type	=	'mother';
					$fullname = $data->mother_name.' ('.$data->mother_id_card.')';
				}
				else if($type	==	'FATHER CARD')
				{
					$other_type	=	'father';
					$fullname = $data->father_name.' ('.$data->father_id_card.')';
				}
				else
				{
					$fullname = $data->fullname.' ('.$data->passport_number.')';
				}
				
				$arr[] = array('id'=>$data->applicantid,'label'=>$fullname,'value'=>$data->idcard_number,'refil_data_url'=>$refil_data_url,'other'	=>	$other_type);
			}
			
			return json_encode($arr);
		}
	}
//-------------------------------------------------------------

	/*
	* GET APPLICANT ID accordint to ID CARD NUMBER
	* $param $id integer
	* return applicantid
	*/
	public function get_applicant_id($id)
	{
		$this->db->select('applicantid');
		$this->db->from('ah_applicant');		
		$this->db->where('idcard_number',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->applicantid;
		}
		else
		{
			return 0;
		}
	}
//-------------------------------------------------------------

	/*
	* GET APPLICANT ID accordint to PASSPORT NUMBER
	* $param $id integer
	* return applicantid
	*/
	public function get_passport_number($id)
	{
		$this->db->select('applicantid');
		$this->db->from('ah_applicant');		
		$this->db->where('passport_number',$id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->applicantid;
		}
		else
		{
			return 0;
		}
	}
//-------------------------------------------------------------

	/*
	* GET APPLICANT ID accordint to WIFE PASSPORT NUMBER
	* $param $id integer
	* return applicantid
	*/
	public function get_passport_number_wife($id)
	{
		$this->db->select('applicantid');
		$this->db->from('ah_applicant_wife');		
		$this->db->where('relationdoc',$id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->applicantid;
		}
		else
		{
			return 0;
		}
	}
//-------------------------------------------------------------

	/*
	* GET APPLICANT ID accordint to WIFE PASSPORT NUMBER
	* $param $id integer
	* return applicantid
	*/
	public function get_passport_number_wife_info($term,$charity_type_id)
	{
		$this->db->select('applicantid,relationname,relationdoc');
		$this->db->from('ah_applicant_wife');
		$this->db->like('relationdoc',$term);
		$this->db->limit('1');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$html = '<div class="col-md-12">';
			foreach($query->result() as $data)
			{
				//{"id":"Upupa epops","label":"Eurasian Hoopoe","value":"Eurasian Hoopoe"}
				$refil_data_url	=	charity_edit_url($charity_type_id).$data->applicantid.'/refilData';
				
				$fullname	=	$data->relationname.' ('.$data->relationdoc.')';
				$arr[] 		=	array('id'=>$data->applicantid,'label'=>$fullname,'value'=>$data->relationdoc,'refil_data_url'=>$refil_data_url);
			}
			
			return json_encode($arr);
		}
	}
//-------------------------------------------------------------	
	/*
	*	UPDATE SURVAY
	*	@parm $applicantid integer
	*	@parm $ah_applicant_survayresult array
	*	return TRUE
	*/
	function update_survay($applicantid,$ah_applicant_survayresult)
	{
		$this->db->where('applicantid', $applicantid);
		$this->db->update('ah_applicant_survayresult',json_encode($ah_applicant_survayresult),$this->_login_userid,$ah_applicant_survayresult);
		
		return TRUE;	
	}
//-------------------------------------------------------------	
	/*
	*	ADD SURVAY
	*	@parm $ah_applicant_survayresult array
	*	return TRUE
	*/
	function add_survay($ah_applicant_survayresult)
	{
		$this->db->insert('ah_applicant_survayresult',$ah_applicant_survayresult,json_encode($ah_applicant_survayresult),$this->_login_userid);

		return TRUE;	
	}
//-------------------------------------------------------------	
	/*
	*	Get Exetra Telephone Number
	*	@parm $id integer
	*	return integer
	*/
	function get_extratelephone($id)
	{
		$query	=	$this->db->query("SELECT extratelephone FROM ah_applicant WHERE applicantid=".$id."");
		
		if($query->num_rows()	>	0)
		{
			return $query->row()->extratelephone;
		}
			
	}
//-------------------------------------------------------------	
	/*
	*	Get Template
	*	return ARRAY
	*/
	function get_template()
	{
		$query = $this->db->query("SELECT templatesubject, template FROM system_sms_template WHERE templatestatus='1' AND delete_record='0' ORDER BY templatesubject ASC");

		if($query->num_rows()	>	0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------	
	/*
	*	UPDATE SMS Template
	*	@parm $templateid integer
	*	@parm $data array
	*	return TRUE
	*/
	function update_sms_template($templateid,$data)
	{
		$this->db->where('templateid',$templateid);
		$this->db->update('system_sms_template', $data);
		
		return TRUE;	
	}
//-------------------------------------------------------------	
	/*
	*	ADD SMS Template
	*	@parm $data array
	*	return TRUE
	*/
	function add_sms_template($data)
	{
		$this->db->insert('system_sms_template',$data);
		
		return TRUE;	
	}
//-------------------------------------------------------------	
	/*
	*	Get all Templates
	*	return ARRAY
	*/
	function get_all_sms_templates($templateid)
	{
		$query = $this->db->query("SELECT * FROM system_sms_template WHERE templateid='".$templateid."'");

		if($query->num_rows()	>	0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------	
	/*
	* 
	* 
	*/
	function get_last_decission($applicant_id)
	{
		$decission_query = $this->db->query("SELECT decission_amount,decissiontime FROM ah_applicant_decission WHERE applicantid='".$applicant_id."' ORDER BY decissiontime DESC LIMIT 0,1 ");

		if($decission_query->num_rows()	>	0)
		{
			return $decission_query->row();
		}
	}
//-------------------------------------------------------------	
	/*
	*
	*
	*/
	function get_money_last($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		if($branchid!=0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		
		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.application_status != ",'رفض');
		$this->db->where("ah_applicant.application_status != ",'');
		$this->db->where("ah_applicant.step",'4');			
		$this->db->order_by('ah_applicant.registrationdate','DESC');
					
		$query = $this->db->get();
		
		return $query->result();
	}
//-------------------------------------------------------------	
	/*
	*
	*
	*/
	function rejected_list($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.meeting_number,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->join('ah_applicant_survayresult','ah_applicant_survayresult.applicantid = ah_applicant.applicantid');
		
		if($branchid!=0)
		{	
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		
		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.application_status",'رفض');
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		$query = $this->db->get();
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function update_final_decission($applicantid,$data)
	{
		$json_data	=	json_encode(array('record'=>'update','applicantid'	=>	$applicantid));
		
		$this->db->where('applicantid', $applicantid);

		$this->db->update('ah_applicant',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
		
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function add_final_decission($ah_applicant_decission)
	{
		$this->db->insert('ah_applicant_decission',$ah_applicant_decission,json_encode($ah_applicant_decission),$this->_login_userid);

		return TRUE;
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_socilservay_list_special($branchid,$charity_type,$step	=	NULL)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya,ah_applicant_survayresult.ajel');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->join('ah_applicant_survayresult','ah_applicant_survayresult.applicantid = ah_applicant.applicantid');
		
		if($branchid	!=	0)
		{	
			$this->db->where("ah_applicant.branchid",$branchid); 
		}
		
		if($charity_type	!=	0)
		{	
			$this->db->where("ah_applicant.charity_type_id",$charity_type); 
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		
		if($step	!=	NULL)
		{
			$this->db->where("ah_applicant.step >",$step);
			//$this->db->where("ah_applicant.step",$step);
		}
		else
		{
			$this->db->where("ah_applicant.step",'2');
		}
		
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		return $query->result();
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function get_socilservay_list_special_slt($branchid,$charity_type,$step	=	NULL)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya,ah_applicant_survayresult.ajel');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->join('ah_applicant_survayresult','ah_applicant_survayresult.applicantid = ah_applicant.applicantid');
		
		if($branchid	!=	0)
		{	
			$this->db->where("ah_applicant.branchid",$branchid); 
		}
		
		if($charity_type	!=	0)
		{	
			$this->db->where("ah_applicant.charity_type_id",$charity_type); 
		}	
		
		$this->db->where("ah_applicant.application_status",'إعادة نظر');
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.isOld",'1');
		$this->db->where("ah_applicant.step",$step);
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		return $query->result();
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function get_ssr_rejected($charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya,ah_applicant_survayresult.ajel');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->join('ah_applicant_survayresult','ah_applicant_survayresult.applicantid = ah_applicant.applicantid');
		
		if($branchid	!=	0)
		{	
			$this->db->where("ah_applicant.branchid",$branchid); 
		}
		
		if($charity_type	!=	0)
		{	
			$this->db->where("ah_applicant.charity_type_id",$charity_type); 
		}	
		
		$this->db->where("ah_applicant.application_status",'رفض');
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step",'2');
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		return $query->result();
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function getLast_decission($applicantid,$stepid)
	{
		$this->db->select("ah_applicant_decission.*, ah_userprofile.fullname");
		$this->db->from("ah_applicant_decission");
		$this->db->join("ah_userprofile","ah_applicant_decission.userid=ah_userprofile.userid");
		$this->db->where("ah_applicant_decission.applicantid",$applicantid);
		$this->db->where("ah_applicant_decission.step",$stepid);
		$this->db->order_by("ah_applicant_decission.decissiontime","desc");
		$this->db->limit(1);
		return $this->db->get()->row();
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_all_decission($applicantid,$stepid)
	{
		$this->db->select("ah_applicant_decission.*, ah_userprofile.fullname");
		$this->db->from("ah_applicant_decission");
		$this->db->join("ah_userprofile","ah_applicant_decission.userid=ah_userprofile.userid");
		$this->db->where("ah_applicant_decission.applicantid",$applicantid);
		$this->db->where("ah_applicant_decission.step",$stepid);
		$this->db->order_by("ah_applicant_decission.decissiontime","desc");
		
		return $this->db->get()->result();
	}	
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_total_family_members($totalfamilymember,$familysalary)
	{
		$query  		=	$this->db->query("SELECT * FROM ah_financial WHERE '".$totalfamilymember."' BETWEEN people_start AND people_end AND netincome >= '".$familysalary."' LIMIT 0,1");
		return $result 	=	$query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_finaldecission_list($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.meeting_number,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		if($branchid!=0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step",'3');
		$this->db->where("ah_applicant.application_status != ",'رفض');
		$this->db->where("ah_applicant.application_status != ",'');		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
					
		$query = $this->db->get();
		
		return	$query->result();	
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_socilservay_list($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		if($branchid	!=	0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		if($charity_type	!=	0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step",'1');
		$this->db->where("ah_applicant.isOld IS NULL");
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		$query = $this->db->get();
		
		return	$query->result();	
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_socilservay_list_completesteps($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		if($branchid	!=	0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		if($charity_type	!=	0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step >",'1');
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		$query = $this->db->get();
		
		return	$query->result();	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function get_socilservay_list_step($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');		
		
		if($branchid!=0)
		{	$this->db->where("ah_applicant.branchid",$branchid);	}
		
		if($charity_type!=0)
		{	$this->db->where("ah_applicant.charity_type_id",$charity_type);	}
		
		$this->db->where("ah_applicant.application_status","إعادة نظر");
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step",'1');
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		//echo $charity_type;
		return $this->db->get()->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_inquiries_list($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.charity_type_id,ah_applicant.step,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		
		if($branchid!=0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		
		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		$this->db->last_query();
		
		return	$query->result();	
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function save_my_notes($ah_applicant_notes)
	{
		$this->db->insert('ah_applicant_notes',$ah_applicant_notes,json_encode($ah_applicant_notes),$this->_login_userid);
		
		return TRUE;
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_all_helps($charity_type,$id_card_number)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.charity_type_id,ah_applicant.step,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');

		$this->db->where("ah_applicant.idcard_number",$id_card_number);
		$this->db->where("ah_applicant.charity_type_id",$charity_type);
		$this->db->where("ah_applicant.isdelete",'0');
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_all_mother_helps($charity_type,$imother_card_number)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.charity_type_id,ah_applicant.step,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya,ah_applicant.father_name,ah_applicant.father_id_card,ah_applicant.mother_name,ah_applicant.mother_id_card');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');

		$this->db->where("ah_applicant.mother_id_card",$imother_card_number);
		//$this->db->where("ah_applicant.charity_type_id",$charity_type);
		$this->db->where("ah_applicant.isdelete",'0');
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		//return $this->db->last_query();
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_all_father_helps($charity_type,$father_card_number)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.charity_type_id,ah_applicant.step,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya,ah_applicant.father_name,ah_applicant.father_id_card,ah_applicant.mother_name,ah_applicant.mother_id_card');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');

		$this->db->where("ah_applicant.father_id_card",$father_card_number);
		//$this->db->where("ah_applicant.charity_type_id",$charity_type);
		$this->db->where("ah_applicant.isdelete",'0');
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		//return $this->db->last_query();
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_list_taqareer_qaima($type)
	{
		$this->db->select('*');
		$this->db->where('list_type',$type);
		$query = $this->db->get('list_management');
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_tadeel_count($id)
	{
		$this->db->select('*');
		$this->db->where('list_type',$type);
		$query = $this->db->get('list_management');
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function add_decission($ah_applicant_decission)
	{
		$this->db->insert('ah_applicant_decission',$ah_applicant_decission,json_encode($ah_applicant_decission),$this->_login_userid);

		return TRUE;
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function update_decission($applicantid,$ah_applicant)
	{
		$this->db->where('applicantid',$applicantid);
		$this->db->update('ah_applicant',json_encode($ah_applicant),$this->_login_userid,$ah_applicant);
		
		return TRUE;
	}
//-------------------------------------------------------------
	/*
	* Get Total SONS
	* @param $charity_type_id int
	* @param $ah_applicant int
	*/
	function total_count_son($ah_applicant)
	{
		$this->db->where('applicantid',$ah_applicant);
		$this->db->where('relationtype','128');
		
		$query = $this->db->get('ah_applicant_relation');
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}	
//-------------------------------------------------------------
	/*
	* Get Total DAUGHTERS
	* @param $charity_type_id int
	* @param $ah_applicant int
	*/
	function total_count_daughter($ah_applicant)
	{
		$this->db->where('applicantid',$ah_applicant);
		$this->db->where('relationtype','129');
		
		$query = $this->db->get('ah_applicant_relation');
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}	
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_templates()
	{
		$query 	=	$this->db->query("SELECT `templateid`,`templatesubject`,`template`,`tempatedate`,`templatestatus` FROM `system_sms_template` ORDER BY templatesubject ASC;");
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_sms_history_list($module_id,$userid)
	{
		if($module_id	==	'1')
		{
			$this->db->select('sms_history.*,admin_users.firstname,admin_users.lastname,main_applicant.first_name,main_applicant.middle_name,main_applicant.last_name,main_applicant.family_name');
		}
		else
		{
			$this->db->select('sms_history.*,admin_users.firstname,admin_users.lastname,applicants.applicant_first_name,applicants.applicant_middle_name,applicants.applicant_last_name,applicants.applicant_sur_name');
		}
		
		$this->db->from('sms_history');
		
		if($module_id	==	'1')
		{
			$this->db->join('main_applicant',"main_applicant.tempid=sms_history.sms_receiver_id");
		}
		else
		{
			$this->db->join('applicants',"applicants.applicant_id=sms_history.sms_receiver_id");
		}
		
		$this->db->join('admin_users',"admin_users.id=sms_history.sms_sender_id");
		$this->db->where('sms_module_id',$module_id);
		$this->db->where('sms_receiver_id',$userid);
		$this->db->group_by('sms_history.`sms_id`');
		
		$query = $this->db->get();
				
		return $query->result();
	}
	
//-------------------------------------------------------------	
	/*
	*	UPDATE SMS Template
	*	@parm $templateid integer
	*	@parm $data array
	*	return TRUE
	*/
	function update_applicant_in_socialservay($applicantid,$data)
	{
		$this->db->where('applicantid',$applicantid);
		$this->db->update('ah_applicant',json_encode($data),$this->_login_userid,$data);
		
		return TRUE;	
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function bernamij_almasadaat_types($step,$branchid)
	{
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='charity_type' AND list_status='1' AND list_name!='اضافة اليتيم'  AND list_name!='اضافة الكفيل' AND list_name!='النقدية' AND delete_record='0'");
		$result_1	=	$query_1->result();
		
		$almasadaat	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(applicantid) as total FROM `ah_applicant` WHERE charity_type_id='".$type->list_id."' AND isdelete='0' AND step='".$step."' AND branchid='".$branchid."'");
				$result_2	=	$query_2->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		
		return $data;
	}
//-------------------------------------------------------------
	/*
	* Add First Ziyarah
	* @param $data ARRAY
	* return TRUE
	*/
	function save_first_ziyarah($data)
	{
		$this->db->insert('ah_applicant_first_zyarah',$data,json_encode($data),$this->_login_userid);

		return TRUE;
	}	

//-------------------------------------------------------------	
	/*
	*	ADD Yateem SURVAY
	*	@parm $ah_applicant_survayresult array
	*	return TRUE
	*/
	function add_yateem_servay($ah_applicant_survayresult)
	{
		$this->db->insert('ah_yateem_servay',$ah_applicant_survayresult,json_encode($ah_applicant_survayresult),$this->_login_userid);

		return TRUE;	
	}
//-------------------------------------------------------------	

	/*
	*
	*/
	public function update_yateem_step($orphan_id,$step)
	{
		$data = array('step'=>$step);

		$this->db->where('orphan_id', $orphan_id);

		$this->db->update('ah_yateem_info',json_encode($data),$this->session->userdata('userid'),$data);
		
		return TRUE;	
	}
//-------------------------------------------------------------
	/*
	* Get Branch ID and Type ID
	* @param $charity_type_id int
	* @param $ah_applicant int
	*/
	function get_branch_type($applicantid)
	{
		$this->db->select('branchid,charity_type_id');
		$this->db->where('applicantid',$applicantid);
		
		$query = $this->db->get('ah_applicant');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------	
}
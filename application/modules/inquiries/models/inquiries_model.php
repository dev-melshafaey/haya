<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inquiries_model extends CI_Model 
{
	/*
	* Properties
	*/
	
	private $_table_users;	
	private $_table_sms_management;
//----------------------------------------------------------------------    
	/*
	* Constructor
	*/	
	function __construct()
    {
        parent::__construct();
				
		$this->load->helper("file");
				
		// Load Table Names from Config
		$this->_table_users 			=  $this->config->item('table_users');
        $this->_table_user_profile 		=  $this->config->item('table_user_profile');
		$this->_table_sms_management 	=  $this->config->item('table_sms_management');
		$this->_login_userid			=	$this->session->userdata('userid');
    }
	
	function ocoReport()
	{
		$fieldArray = array(
			'FORMID'			=>	'رقم الحاسب',
			'FORMNO'			=>	'رقم الاستمارة',
			'NAME'				=>	'الاسم',
			'LABOUR'			=>	'الرقم المدني',
			'PASSPORT'			=>	'رقم جواز سفر',
			'GENDER'			=>	'جنس',
			'NATIONALITY_ID'	=>	'جنسية',
			'MARITAL_STATUS'	=>	'الحالة الجتماعية',
			'REGION'			=>	'المحافظة',
			'STATE'				=>	'ولاية',
			'TOWN'				=>	'البلدة\المحلة',
			'PHONE'				=>	'هاتف المنزل',
			'MOBILE'			=>	'رقم الهاتف',
			'SALARY'			=>	'مصدر الدخل',
			'SALSOURCE'			=>	'قيمة الدخل',
			'TYPEOFHELP'		=>	'نوع',
			'FORMCASE'			=>	'حالة'
		);
		
		foreach($fieldArray as $fieldKey => $fieldValue)
		{
			$CountQuery = $this->db->query("SELECT COUNT(FORMID) as cnt FROM oco_person WHERE `".$fieldKey."` IS NULL OR `".$fieldKey."`=0");
			$arr[$fieldValue] = $CountQuery->row()->cnt;
		}
		
		return $arr;
	}
	
	function oco_person_count()
	{		
		$this->db->select("oco_person.FORMNO");
		$this->db->from("oco_person");
		$this->db->join("oco_typeofassistance","oco_typeofassistance.TA_ASSISTANCEID=oco_person.TYPEOFHELP");
		//$this->db->join("ah_applicant","ah_applicant.idcard_number=oco_person.LABOUR");
		//$this->db->where("oco_person.FORMNO != ",'');
		$this->db->where("oco_person.FORMID != ",'');
		$this->db->where("oco_person.ourstatus",'0');		
		foreach($this->input->get() as $getkey => $getvalue)
		{
			if($getvalue!='')
			{
				if($getkey=='NAME')
				{	$this->db->like($getkey,$getvalue); 	}
				else if($getkey=='LABOUR')
				{	$this->db->where("REPLACE(oco_person.LABOUR,'/','')",$getvalue); }
				else
				{	$this->db->where($getkey,$getvalue);	}
			}
		}		
		return $this->db->get()->num_rows();
	}
	
	function oco_person($limit, $start, $FORMID=-1, $FORMNO=-1)
	{
		$this->db->from("oco_person");
		$this->db->join("oco_typeofassistance","oco_typeofassistance.TA_ASSISTANCEID=oco_person.TYPEOFHELP");
		//$this->db->join("ah_applicant","ah_applicant.idcard_number=oco_person.LABOUR");
		//$this->db->where("oco_person.FORMNO != ",'');
		$this->db->where("oco_person.FORMID != ",'');
		$this->db->where("oco_person.ourstatus",'0');
		foreach($this->input->get() as $getkey => $getvalue)
		{
			if($getvalue!='')
			{
				if($getkey=='NAME')
				{	$this->db->like($getkey,$getvalue); 	}
				else if($getkey=='LABOUR')
				{	$this->db->where("REPLACE(oco_person.LABOUR,'/','')",$getvalue); }
				else
				{	$this->db->where($getkey,$getvalue);	}
			}
		}
		
		if($FORMID!=-1 && $FORMNO!=-1)
		{	
			$this->db->where("oco_person.FORMID",$FORMID);
			$this->db->where("oco_person.FORMNO",$FORMNO);	
		}
			
		$this->db->order_by("oco_person.FORMID","DESC");
		$this->db->limit($limit, $start);
		
        foreach($this->db->get()->result() as $oco)
		{
			$arrp[] = array(
				'FORMID' 			=>	$oco->FORMID,
				'FORMNO' 			=>	$oco->FORMNO,
				'HELPID' 			=>	$oco->TYPEOFHELP,
				'FORMDATE' 			=>	check_null_oco($oco->FORMDATE),
				'NAME' 				=>	check_null_oco($oco->NAME),
				'LABOUR' 			=>	check_null_oco($oco->LABOUR),
				'PASSPORT' 			=>	check_null_oco($oco->PASSPORT),
				'GENDER' 			=>	$this->oco_data('G',$oco->GENDER),
				'NATIONALITY_ID' 	=>	$this->oco_data('N',check_null_oco($oco->NATIONALITY_ID)),
				'MARITAL_STATUS' 	=>	$this->oco_data('M',check_null_oco($oco->MARITAL_STATUS)),
				'RELATION' 			=>	check_null_oco($oco->RELATION),
				'FAMILYMEMBERS' 	=>	check_null_oco($oco->FAMILYMEMBERS),
				'REGION' 			=>	$this->oco_data('R',check_null_oco($oco->REGION)),
				'STATE' 			=>	$this->oco_data('W',check_null_oco($oco->STATE)),
				'TOWN' 				=>	check_null_oco($oco->TOWN),
				'PHONE' 			=>	check_null_oco($oco->PHONE),
				'MOBILE' 			=>	check_null_oco($oco->MOBILE),
				'SALARY' 			=>	check_null_oco($oco->SALARY),
				'SALSOURCE' 		=>	check_null_oco($oco->SALSOURCE),
				'SECURITYNO' 		=>	check_null_oco($oco->SECURITYNO),
				'TYPEOFHELP' 		=>	$this->oco_data('T',check_null_oco($oco->TYPEOFHELP)),
				'FORMCASE' 			=>	$this->oco_data('F',check_null_oco($oco->FORMCASE)),
				'CREATEDBY' 		=>	check_null_oco($oco->CREATEDBY),
				'CREATEDDATE' 		=>	check_null_oco($oco->CREATEDDATE),
				'UPDATEDBY' 		=> 	$this->oco_data('U',check_null_oco($oco->UPDATEDBY)),
				'UPDATEDDATE' 		=>	check_null_oco($oco->UPDATEDDATE)
				);
		}
		return $arrp;
	}
	
	function countfromtheTable($table,$formid,$column)
	{
		$this->db->select($column);
		$this->db->from($table);
		$this->db->where($column,$formid);
		return $this->db->get()->num_rows();
	}
	
	function oco_person_data($personid)
	{
		$this->db->select("oco_username.UN_USERNAME_AR,oco_userrole.RO_ROLENAME_AR");
		$this->db->from("oco_username");
		$this->db->join("oco_userrole","oco_userrole.RO_ROLEID=oco_username.UN_USERROLEID");
		$this->db->where("oco_username.UN_USERID",$personid);
		return $this->db->get()->row();
	}
	
	function oco_data_detail($FORMNO,$FORMID,$CASE)
	{		
		switch($CASE)
		{
			case 'F';
				$this->db->from('oco_financial');
				$this->db->where('FIN_FORMID',$FORMID);
				return $this->db->get()->result();
			break;
			case 'P';
				$this->db->from('oco_person');
				$this->db->where('FORMID',$FORMID);
				foreach($this->db->get()->result() as $oco)
				{
					$arrp[] = array(
						'FORMID' => $oco->FORMID,
						'FORMNO' => $oco->FORMNO,
						'HELPID' => $oco->TYPEOFHELP,
						'FORMDATE' => show_date(check_null_oco($oco->FORMDATE),9),
						'NAME' => check_null_oco($oco->NAME),
						'LABOUR' => check_null_oco($oco->LABOUR),
						'PASSPORT' => check_null_oco($oco->PASSPORT),
						'GENDER' => $this->oco_data('G',$oco->GENDER),
						'NATIONALITY_ID' => $this->oco_data('N',check_null_oco($oco->NATIONALITY_ID)),
						'MARITAL_STATUS' => $this->oco_data('M',check_null_oco($oco->MARITAL_STATUS)),
						'RELATION' => check_null_oco($oco->RELATION),
						'FAMILYMEMBERS' => check_null_oco($oco->FAMILYMEMBERS),
						'REGION' => $this->oco_data('R',check_null_oco($oco->REGION)),
						'STATE' => $this->oco_data('W',check_null_oco($oco->STATE)),
						'TOWN' => check_null_oco($oco->TOWN),
						'PHONE' => check_null_oco($oco->PHONE),
						'MOBILE' => check_null_oco($oco->MOBILE),
						'SALARY' => check_null_oco($oco->SALARY),
						'SALSOURCE' => check_null_oco($oco->SALSOURCE),
						'SECURITYNO' => check_null_oco($oco->SECURITYNO),
						'TYPEOFHELP' => $this->oco_data('T',check_null_oco($oco->TYPEOFHELP)),
						'FORMCASE' => $this->oco_data('F',check_null_oco($oco->FORMCASE)),
						'CREATEDBY' => $this->oco_data('U',check_null_oco($oco->CREATEDBY)),
						'CREATEDDATE' => show_date(check_null_oco($oco->CREATEDDATE),9),
						'UPDATEDBY' => $this->oco_data('U',check_null_oco($oco->UPDATEDBY)),
						'UPDATEDDATE' => show_date(check_null_oco($oco->UPDATEDDATE),9)
						);
				}
				return $arrp;
			break;	
			case 'PD';
				$query = $this->db->query("
					SELECT
						`NAME` as fullname,
						`LABOUR` as idcard_number,
						`PASSPORT` as passport_number,
						`MOBILE` as extratelephone,
						`PHONE` as hometelephone,
						`TOWN` as address,
						 IF(GENDER='M', 'ذكر', 'أنثى') gender,
						 IF(NATIONALITY_ID='1', '200', '254') country_listmanagement,
						 SALSOURCE as income,
						 SALARY as salary,
						 region_filter(REGION) as province,
						 marital_filter(MARITAL_STATUS) as marital_status,
						 RELATION as relation
						 		
					FROM oco_person
					WHERE FORMID='".$FORMID."'
				
				");
				return $query->row();
			break;			
			case 'FD';
				$this->db->from('oco_financial_detail');
				$this->db->where('FORMID',$FORMID);
				return $this->db->get()->result();			
			break;
			
			case 'H';
				$this->db->from('oco_house');
				$this->db->where('HOU_FORMID',$FORMID);
				return $this->db->get()->result();				
			break;
			
			case 'HD';
				$this->db->from('oco_house_detail');
				$this->db->where('FORMID',$FORMID);
				return $this->db->get()->result();				
			break;						
		}
	}
	
	
	function oco_data($case,$id,$list='NO')
	{
		switch($case)
		{
			case 'R';
				$this->db->select('REG_DESC,REG_ID');
				$this->db->from('oco_regions');				
				if($list=='YES')
				{
					$this->db->where('REG_DESC != ',"");
					return $this->db->get()->result();
				}
				else
				{	$this->db->where('REG_ID',$id);
					return $this->db->get()->row()->REG_DESC;
				}
			break;
			case 'W';
				$this->db->select('WIL_DESC,WIL_ID');
				$this->db->from('oco_wilayats');
				
				if($list=='YES')
				{
					$this->db->where('WIL_DESC != ',"");
					return $this->db->get()->result();
				}
				else
				{	$this->db->where('WIL_ID',$id);
					return $this->db->get()->row()->WIL_DESC;
				}
			break;
			case 'N';
				$this->db->select('NA_NATIONALITY_AR,NA_NATIONALITYID');
				$this->db->from('oco_nationality');				
				if($list=='YES')
				{
					$this->db->where('NA_NATIONALITY_AR != ',"");
					return $this->db->get()->result();
				}
				else
				{
					$this->db->where('NA_NATIONALITYID',$id);
					return $this->db->get()->row()->NA_NATIONALITY_AR;
				}
			break;
			case 'M';
				$this->db->select('MS_MARITAL_AR,MS_MARITALID');
				$this->db->from('oco_maritalstatus');				
				if($list=='YES')
				{
					$this->db->where('MS_MARITAL_AR != ',"");
					return $this->db->get()->result();
				}
				else
				{	$this->db->where('MS_MARITALID',$id);
					return $this->db->get()->row()->MS_MARITAL_AR;
				}
			break;
			case 'T';
				$this->db->select('TA_ASSISTANCE_AR,TA_ASSISTANCEID');
				$this->db->from('oco_typeofassistance');
				
				if($list=='YES')
				{
					$this->db->where('TA_ASSISTANCE_AR != ',"");
					return $this->db->get()->result();
				}
				else
				{	$this->db->where('TA_ASSISTANCEID',$id);
					return $this->db->get()->row()->TA_ASSISTANCE_AR;
				}
			break;
			case 'F';
				$this->db->select('CO_CASE_AR,CO_CASEID');
				$this->db->from('oco_caseform');
				
				if($list=='YES')
				{
					$this->db->where('CO_CASE_AR != ',"");
					return $this->db->get()->result();
				}
				else
				{	$this->db->where('CO_CASEID',$id);
					return $this->db->get()->row()->CO_CASE_AR;
				}
			break;
			case 'U';
				$this->db->select('UN_USERNAME_AR,UN_USERID');
				$this->db->from('oco_username');
				
				if($list=='YES')
				{
					$this->db->where('UN_USERNAME_AR != ',"");
					return $this->db->get()->result();
				}
				else
				{	$this->db->where('UN_USERID',$id);
					return $this->db->get()->row()->UN_USERNAME_AR;
				}
			break;
			case 'R';
				$this->db->select('RO_ROLENAME_AR,RO_ROLEID');
				$this->db->from('oco_userrole');				
				if($list=='YES')
				{
					$this->db->where('RO_ROLENAME_AR != ',"");
					return $this->db->get()->result();
				}
				else
				{	$this->db->where('RO_ROLEID',$id);
					return $this->db->get()->row()->RO_ROLENAME_AR;
				}
			break;
			case 'G';
				$array = array('M'=>'ذكر','F'=>'أنثى');
				if($list=='YES')
				{	return $array; }
				else
				{	return $array[$id];	}
			break;
		}
	}
	
//----------------------------------------------------------------------    
	/*
	* 
	*/		
	function allRequiredDocument($charity_type_id_value)
	{	
		$this->db->select('documentid,documenttype,isrequired');
        $this->db->from('ah_document_required');
        $this->db->where('charity_type_id',$charity_type_id_value);
		$this->db->where('documentstatus',1);
        $this->db->order_by("documentorder", "ASC");
		
        $query = $this->db->get();
		
		if($query->num_rows()	>	0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/		
	function submitfor()
	{
		$form_data = $this->input->post();
		
		//echo '<pre>';print_r($form_data);
		//exit();

		$refil_data 							= 	$form_data['refil_data'];
		$applicantid 							= 	$form_data['applicantid'];
		$formid 								= 	$form_data['formid'];
		$logged_in_user_detail 					= 	$this->haya_model->get_user_detail($this->_login_userid);
		$ah_applicant['userid'] 				= 	$this->_login_userid;
		$ah_applicant['branchid']				= 	$logged_in_user_detail['profile']->branchid;
		$ah_applicant['charity_type_id'] 		= 	$form_data['charity_type_id'];
		$ah_applicant['province'] 				= 	$form_data['province'];
		$ah_applicant['wilaya'] 				= 	$form_data['wilaya'];
		$ah_applicant['country'] 				= 	$form_data['country'];
		$ah_applicant['address'] 				= 	$form_data['address'];
		$ah_applicant['fullname'] 				= 	$form_data['fullname'];
		$ah_applicant['extra_detail'] 			=	$form_data['extra_detail'];
		$ah_applicant['country_listmanagement']	= 	$form_data['country_listmanagement'];
		$ah_applicant['applicant_age']			= 	$form_data['applicant_age'];
		$ah_applicant['date_of_birth']			= 	$form_data['date_of_birth'];
			
		//if($applicantid	==	'')
		//{
			$ah_applicant['passport_number']	= $form_data['passport_number'];
			$ah_applicant['idcard_number'] 		= $form_data['idcard_number']; 
		//}
		// Managment told me to remove unlock this condition the want to change idcard_number/passport_number fields
		if($refil_data	!=	'')
		{
			$ah_applicant['passport_number']	= $form_data['passport_number'];
			$ah_applicant['idcard_number'] 		= $form_data['idcard_number'];
		}
		
		$ah_applicant['passport_number']		= 	$form_data['passport_number'];	
		$ah_applicant['hometelephone'] 			=	$form_data['hometelephone'];
		$ah_applicant['extratelephone'] 		=	json_encode($form_data['extratelephone']);
		$ah_applicant['marital_status'] 		=	$form_data['marital_status'];
		$ah_applicant['income'] 				=	$form_data['income'];
		$ah_applicant['mother_name'] 			=	$form_data['mother_name'];
		$ah_applicant['mother_id_card'] 		= 	$form_data['mother_id_card'];
		$ah_applicant['father_name'] 			= 	isset($form_data['father_name'])	?	$form_data['father_name']	:	NULL;
		$ah_applicant['father_id_card'] 		= 	isset($form_data['father_id_card'])	?	$form_data['father_id_card']	:	NULL;
		$ah_applicant['certificate_dualism'] 	= 	isset($form_data['certificate_dualism'])	?	$form_data['certificate_dualism']	:	NULL;
		$ah_applicant['graduation_year'] 		= 	isset($form_data['graduation_year'])	?	$form_data['graduation_year']	:	NULL;
		$ah_applicant['school_percentage'] 		= 	isset($form_data['school_percentage'])	?	$form_data['school_percentage']	:	NULL;		
		$ah_applicant['college_name'] 			= 	isset($form_data['college_name'])	?	$form_data['college_name']	:	NULL;
		$ah_applicant['specialization'] 		= 	isset($form_data['specialization'])	?	$form_data['specialization']	:	NULL;
		$ah_applicant['year_of_study'] 			= 	isset($form_data['year_of_study'])	?	$form_data['year_of_study']	:	NULL;
		$ah_applicant['last_grade_average'] 	= 	isset($form_data['last_grade_average'])	?	$form_data['last_grade_average']	:	NULL;		
		$ah_applicant['salary'] 				= 	$form_data['salary'];
		$ah_applicant['income'] 				= 	$form_data['income'];		
		$ah_applicant['source'] 				= 	$form_data['source'];
		$ah_applicant['q_source'] 				= 	$form_data['q_source'];
		$ah_applicant['gender'] 				= 	$form_data['gender'];
		$ah_applicant['musadra'] 				= 	$form_data['musadra'];
		$ah_applicant['qyama'] 					= 	$form_data['qyama'];
		$ah_applicant['musadra_2'] 				= 	$form_data['musadra_2'];
		$ah_applicant['qyama_2'] 				= 	$form_data['qyama_2'];
		$ah_applicant['musadra_3'] 				= 	$form_data['musadra_3'];
		$ah_applicant['qyama_3'] 				= 	$form_data['qyama_3'];
		$ah_applicant['musadra_4'] 				= 	$form_data['musadra_4'];
		$ah_applicant['qyama_4'] 				= 	$form_data['qyama_4'];						
		$ah_applicant['lamusadra'] 				= 	$form_data['lamusadra'];		
		$ah_applicant['current_situation'] 		= 	$form_data['current_situation'];
		$ah_applicant['ownershiptype'] 			= 	$form_data['ownershiptype'];
		$ah_applicant['ownershiptype_amount'] 	= 	$form_data['ownershiptype_amount'];
		$ah_applicant['building_type'] 			= 	$form_data['building_type'];
		$ah_applicant['number_of_rooms'] 		= 	$form_data['number_of_rooms'];
		$ah_applicant['utilities'] 				= 	$form_data['utilities'];
		$ah_applicant['furniture'] 				= 	$form_data['furniture'];
		$ah_applicant['bankid'] 				= 	$form_data['bankid'];		
		$ah_applicant['bankbranchid'] 			= 	$form_data['bankbranchid'];
		$ah_applicant['accountnumber'] 			= 	$form_data['accountnumber'];
		$ah_applicant['accounttype'] 			= 	$form_data['accounttype'];
		$ah_applicant['swiftcode'] 				= 	$form_data['swiftcode'];
		
				
		if(!isset($form_data['urgent_applicant']))
		{
			$ah_applicant['urgent_applicant'] 			= 	'0';
		}
		else
		{
			$ah_applicant['urgent_applicant'] 			= 	'1';
		}
		
		if($applicantid!='')
		{
			if($applicantid!='' AND $refil_data!='')
			{
				if($form_data['record_type']	==	'new')
				{
					$this->db->insert('ah_applicant',$ah_applicant,json_encode($ah_applicant),$this->_login_userid);	
					$applicantid = $this->db->insert_id();
						
					$this->haya_model->save_steps($applicantid,1);
					
					$this->haya_model->update_steps($applicantid,1);
				}
				else
				{
					$this->db->where('applicantid',$applicantid);
					$this->db->update('ah_applicant',json_encode($ah_applicant),$this->_login_userid,$ah_applicant);
				}
			}
			else
			{
				$this->db->where('applicantid',$applicantid);
				$this->db->update('ah_applicant',json_encode($ah_applicant),$this->_login_userid,$ah_applicant);
				
				//echo $this->db->last_query();exit();
			}
			
			/*Save history for applicant*/
			$update_history	=	array(
				'applicantid'	=>	$applicantid,
				'userid'		=>	$this->_login_userid,
				'action'		=>	'update'
				);
			
			$this->inq->update_history($update_history); // Update the Record for History
			
		}
		else
		{	
			$this->db->insert('ah_applicant',$ah_applicant,json_encode($ah_applicant),$this->_login_userid);	
			$applicantid = $this->db->insert_id();
				
			$this->haya_model->save_steps($applicantid,1);
			$this->haya_model->update_steps($applicantid,1);
			
			/*Save history for applicant*/
			$update_history	=	array(
				'applicantid'	=>	$applicantid,
				'userid'		=>	$this->_login_userid,
				'action'		=>	'add'
				);
			
			$this->inq->update_history($update_history); // Update the Record for History
		}

			$applicantcode = $applicantid.$logged_in_user_detail['profile']->branchid.$this->_login_userid.$form_data['charity_type_id'];
			
			//Uploading Path
			$fullPath = './resources/applicants/'.$applicantcode.'/';
			
			//Deleting Data
			$this->db->query("DELETE FROM ah_applicant_wife WHERE applicantid='".$applicantid."'");
			$this->db->query("DELETE FROM ah_applicant_relation WHERE applicantid='".$applicantid."'");
			$this->db->query("DELETE FROM ah_applicants_loans WHERE applicantid='".$applicantid."'");
			$this->db->query("UPDATE ah_applicant SET `applicantcode`='".$applicantcode."' WHERE applicantid='".$applicantid."'");
			
		
		foreach($form_data['loan_type'] as $loanKey => $loanValue)
		{
			$loan_type = $loanValue;
			$loan_amount = $form_data['loan_amount'][$loanKey];
			$loan_limit = $form_data['loan_limit'][$loanKey];
			
			if($loan_type!='' && $loan_amount!='' && $loan_limit!='')
			{
				$ah_applicants_loans['applicantid']			=	$applicantid;
				$ah_applicants_loans['loan_type']			=	$loan_type;
				$ah_applicants_loans['loan_amount'] 		=	$loan_amount;
				$ah_applicants_loans['loan_limit']			=	$loan_limit;
				$ah_applicants_loans['adding_date']			=	date("Y-m-d H:i:s");
				$this->db->insert('ah_applicants_loans',$ah_applicants_loans,json_encode($ah_applicants_loans),$this->_login_userid);
			}
		}		
		
		
			
		foreach($form_data['relationname'] as $relationkey => $relationvalue)
		{
			if($relationvalue!='')
			{
				$ah_applicant_wife['applicantid']			=	$applicantid;
				$ah_applicant_wife['relationname']			=	$relationvalue;
				$ah_applicant_wife['relationdoc'] 			=	$form_data['relationdoc'][$relationkey];
				$ah_applicant_wife['relationnationality']	=	$form_data['relationnationality'][$relationkey];
				
				$this->db->insert('ah_applicant_wife',$ah_applicant_wife,json_encode($ah_applicant_wife),$this->_login_userid);
			}
		}
		
		foreach($form_data['relation_fullname'] as $rfkey => $rfvalue)
		{
			if($rfvalue!='')
			{
				$ah_applicant_relation = array(
					'applicantid'		=>	$applicantid,
					'relationtype'		=>	$form_data['relationtype'][$rfkey],
					'relationtype_text'	=>	$form_data['relationtype_text'][$rfkey],
					'relation_fullname'	=>	$rfvalue,
					'age'				=>	$form_data['age'][$rfkey],
					'birthyear'			=>	$form_data['birthyear'][$rfkey],
					'professionid'		=>	$form_data['professionid'][$rfkey],
					'professionid_text'	=>	$form_data['professionid_text'][$rfkey],
					'monthly_income'	=>	$form_data['monthly_income'][$rfkey],
					'profession'		=>	$form_data['profession'][$rfkey],
					'sort_order'		=>	$rfkey);
					
				$this->db->insert('ah_applicant_relation',$ah_applicant_relation,json_encode($ah_applicant_relation),$this->_login_userid);
			}
		}
		
		if($_FILES['bankstatement']['name']!='')
		{
			$ah_applicant_bankstatement['bankstatement'] = $this->haya_model->upload_file('bankstatement',$fullPath);
			
			$this->db->where('applicantid',$applicantid);
			$this->db->update('ah_applicant',json_encode($ah_applicant_bankstatement),$this->_login_userid,$ah_applicant_bankstatement);
		}
		if($_FILES['school_certificate']['name']!='')
		{
			$school_certificate['school_certificate'] = $this->haya_model->upload_file('school_certificate',$fullPath);
			
			$this->db->where('applicantid',$applicantid);
			$this->db->update('ah_applicant',json_encode($school_certificate),$this->_login_userid,$school_certificate);
		}
		if($_FILES['college_certificate']['name']!='')
		{
			$college_certificate['college_certificate'] = $this->haya_model->upload_file('college_certificate',$fullPath);
			
			$this->db->where('applicantid',$applicantid);
			$this->db->update('ah_applicant',json_encode($college_certificate),$this->_login_userid,$college_certificate);
		}
		
		foreach($this->inq->allRequiredDocument($form_data['charity_type_id']) as $ctid) 
		{
			if($_FILES['doclist'.$ctid->documentid]['name']!='')
			{
				$this->db->query("DELETE FROM ah_applicant_documents WHERE applicantid='".$applicantid."' AND documentid='".$ctid->documentid."'");
				$ah_applicant_documents['applicantid'] = $applicantid;
				$ah_applicant_documents['userid'] = $this->_login_userid;
				$ah_applicant_documents['documentid'] = $ctid->documentid;
				$ah_applicant_documents['document'] = $this->haya_model->upload_file('doclist'.$ctid->documentid,$fullPath);
				$this->db->insert('ah_applicant_documents',$ah_applicant_documents,json_encode($ah_applicant_documents),$this->_login_userid);				
			}
			
		}
		
		if($this->input->post("notes")!='')
		{
			$ah_applicant_notes['applicantid'] 	=	$applicantid;
			$ah_applicant_notes['userid'] 		=	$this->_login_userid;
			$ah_applicant_notes['branchid'] 	=	$logged_in_user_detail['profile']->branchid;
			$ah_applicant_notes['notes'] 		=	$this->input->post("notes");
			$ah_applicant_notes['notestype'] 	=	'from_receipt';
			$this->db->insert('ah_applicant_notes',$ah_applicant_notes,json_encode($ah_applicant_notes),$this->_login_userid);
		}
		
		//Updated By Noor Khan @ 12OCT2016
		//Updating Old Records
		if($formid!='')
		{	$this->deleteOldData($formid);	}
		else
		{	$this->db->query("UPDATE `oco_person` SET `ourstatus`='1' WHERE REPLACE(LABOUR,'/','')='".$form_data['idcard_number']."'");	}
		
		/*redirect(base_url().'inquiries/transactions');
		exit();*/
			$this->load->library('user_agent');
			if ($this->agent->is_referral())
			{
				$url	=	 $this->agent->referrer();
			}
			
			$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
			redirect($url);
			exit();
		}
		
		function deleteOldData($formid)
		{
			$this->db->query("UPDATE oco_person SET ourstatus='1' WHERE FORMID='".$formid."'");
			$this->db->query("UPDATE oco_person_log SET ourstatus='1' WHERE FORMID='".$formid."'");
			$this->db->query("UPDATE oco_financial SET ourstatus='1' WHERE FIN_FORMID='".$formid."'");
			$this->db->query("UPDATE oco_financial_detail SET ourstatus='1' WHERE FORMID='".$formid."'");
			$this->db->query("UPDATE oco_financial_log SET ourstatus='1' WHERE FIN_FORMID='".$formid."'");
			$this->db->query("UPDATE oco_house SET ourstatus='1' WHERE HOU_FORMID='".$formid."'");
			$this->db->query("UPDATE oco_house_detail SET ourstatus='1' WHERE FORMID='".$formid."'");
			$this->db->query("UPDATE oco_house_log SET ourstatus='1' WHERE HOU_FORMID='".$formid."'");			 
		}
		
		function LabourUpdate($labour)
		{
			$query = $this->db->query("SELECT FORMID FROM oco_person WHERE LABOUR='".$labour."'");
			if($query->num_rows() > 0)
			{
				echo $query->row()->FORMID;
				//$this->db->query("UPDATE oco_person SET ourstatus='1' WHERE FORMID='".$query->row()->FORMID."'");
				
			}
		}
//----------------------------------------------------------------------
	/*
	* Insert User Record
	* @param array $data
	* return True
	*/
	function insert_user_detail($data)
	{
		$this->db->insert($this->_table_users,$data);
		
		return TRUE;
	}
/***********************************************/
	
//29/12/2014-------------------------------------START
    public function users_role()
    {
        $this->db->select('id,user_name,firstname,lastname,user_role_id');
        $this->db->from('admin_users');
        $this->db->where('status',1);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();

        $dropdown = '<select name="userid" id="userid" class="form-control">';
        $dropdown .= '<option value="">حدد المستخدم</option>';

        foreach($query->result() as $row)
        {
            $dropdown .= '<option value="'.$row->id.'" ';
            if($value==$row->id)
            {
                $dropdown .= 'selected="selected"';
            }
            $dropdown .= '>'.$row->firstname.' '.$row->lastname.' ('.$row->user_name.')</option>';
        }
        $dropdown .= '</select>';
        echo($dropdown);
    }
//29/12/2014-------------------------------------END
//10/01/2015-------------------------------------START
    function user_perm_list()
    {
        $this->db->select('id,user_name,firstname,lastname,user_role_id');
        $this->db->from('admin_users');
        $this->db->where('status',1);
        $this->db->order_by("firstname", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
//----------------------------------------------------------------------    
	/*
	* 
	*/
    function parent_module()
    {
        $this->db->select('moduleid,module_name,module_icon');
        $this->db->from('mh_modules');
        $this->db->where('module_parent',0);
        $this->db->where('module_status','A');
        $this->db->order_by("module_order", "ASC");
        $query = $this->db->get();
        
		return $query->result();
    }
//----------------------------------------------------------------------    
	/*
	* 
	*/	
    function childe_module($parentid)
    {
        $this->db->select('moduleid,module_name,module_icon');
        $this->db->from('mh_modules');
        $this->db->where('module_parent',$parentid);
        //$this->db->where('module_status','A');
        $this->db->order_by("module_order", "ASC");
        $query = $this->db->get();
        
		return $query->result();
    }
//10/01/2015	-------------------------------------	END
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function getAprovalList($applicant_id){
		$sql = "SELECT 
			  SUM(
				sealed_company + commercial_papers + municipal_contractrent + membership_certificate +company_general_authority+open_account+check_book+registration_zip
			  ) AS total 
			FROM
			  `check_list` AS cl 
			WHERE cl.`applicant_id` = '".$applicant_id."' ";

			$q = $this->db->query($sql);
			return $r =  $q->result();
	}
//------------------------------------------------------------------------

	/*
	*
	*
	*/
	function deleteTempDelete()
	{
		// Login Query
		$userid = $this->session->userdata('userid');
		$this->db->where('userid',$userid);
		$query = $this->db->get('applicant_temp_document');	
			
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $temp_data)
			{
				$path = APPPATH.'../upload_files/'.$userid.'/'.$temp_data->documentname;
				if(file_exists($path))
				{
					unlink($path); //Delete temporary uploaded files
				}
				
				$this->db->where("documentid",$temp_data->documentid);
				$this->db->delete('applicant_temp_document');
			}
		}
	}
	
//------------------------------------------------------------------------

	/*
	*
	*
	*/	
	function checkApplicaitonProject($id)
	{
		$this->db->where('applicant_id',$id);
		$query = $this->db->get('applicant_project');
		
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
//----------------------------------------------------------------------
    /*
    * insert user_profile method
    */
    function insert_user_profile($data)
    {
        $this->db->insert($this->_table_user_profile,$data);

        return TRUE;
    }
//----------------------------------------------------------------------
	/*
	* User login
	* @param string $user_email
	* @param string $password
	* return Login User Data
	*/
	function login_user($user_email,$password)
	{
		//Login Query
		$this->db->where('email',$user_email);
		$this->db->where('password',md5($password));
		$query = $this->db->get($this->_table_users);
		
		//Check if Result is Greater Than Zero
		
		if($query->num_rows() > 0)
		{
			$this->session->set_userdata('user_id',$query->row()->user_id);
			return $query->row();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/		
	function loan_data($id)
	{
		//Login Query
		$this->db->where('loan_caculate_id',$id);
		$query = $this->db->get('loan_calculate');
		
		//Check if Result is Greater Than Zero
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/		
	function get_childLoan_data($id)
	{
		//Login Query
		$this->db->where('parent_id',$id);
		$query = $this->db->get('loan_calculate');
		
		//Check if Result is Greater Than Zero
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function get_app_ids($user_id)
	{
		$query = $this->db->query(
		"SELECT
		GROUP_CONCAT(applicantid) AS applicants
		FROM applicant_process_log WHERE userid='$user_id' AND stepsid = '1'");
		if($query->num_rows() > 0)
		{
		return $query->row();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function get_all_applicatnts_data($data)
	{
		//Login Query
		$this->db->where_in('applicant_id',$data);
		$query = $this->db->get('applicants');
		 
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
		return $query->result();
		}
	}
//----------------------------------------------------------------------
	
	function getChild($parentId){
			
		$this->db->where('parent_id',$parentId);
		$query = $this->db->get('loan_calculate');
		
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//------------------------------------------------------------------------

    /**
     * 
     * Insert User Data for Registration
     * @param array $data
     * return integer
     */
	function update_user($userid,$data)
	{
		$update['firstname'] 		=	$data['firstname'];
		$update['lastname'] 		=	$data['lastname'];
		$update['password'] 		=	($data['password']);	
		$update['email'] 			=	$data['email'];
		$update['mobile_number'] 	=	$data['number'];
		$update['number'] 			=	$data['number'];
		$update['about_user'] 		=	$data['about_user'];
		$update['branch_id'] 		=	$data['branch_id'];
		$update['user_role_id'] 	=	$data['user_role_id'];
		$update['user_parent_role'] =	$data['user_parent_role'];
		$update['zyarapermission'] 	=	$data['zyarapermission'];
		
		$this->db->where('id',$userid);
		$this->db->update('admin_users', $update);		
		
		return TRUE;
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/		
	public function getLastDetail($tempid='',$format='array')
	{
		$this->session->unset_userdata('inq_id');		
		$this->db->select('*');
		$this->db->from('main');		
		$this->db->where('tempid',$tempid);
		$this->db->order_by("applicantdate", "DESC"); 
		$this->db->limit(1);
		$tempMain = $this->db->get();
		if($tempMain->num_rows() > 0)
		{
			foreach($tempMain->result() as $data)
			{				
				$arr['main'] = $data;
				$tempid = $data->tempid;							
				$tempMain->free_result(); //Clean Result Set
				//Getting Applicant Data
				$this->db->select('*');
				$this->db->from('main_applicant');		
				$this->db->where('tempid',$tempid);
				$this->db->order_by("applicantid", "ASC");
				$tempApplicant = $this->db->get();
				if($tempApplicant->num_rows() > 0)
				{
					foreach($tempApplicant->result() as $app)
					{
                        $arr['main']->applicant[] = $app;
						//Getting Phone Data
						$this->db->select('*');
						$this->db->from('main_phone');		
						$this->db->where('tempid',$tempid);
						$this->db->where('applicantid',$app->applicantid);
						$this->db->order_by("phoneid", "ASC");
						$tempPhone = $this->db->get();
						if($tempPhone->num_rows() > 0)
						{
							foreach($tempPhone->result() as $phones)
							{
								$arr['main']->phones[$app->applicantid][] = $phones;						
							}
							$tempPhone->free_result(); //Clean Result Set
						}					
					}
					$tempApplicant->free_result(); //Clean Result Set
				}				
				//Getting Inquiry Type Data
				$this->db->select('*');
				$this->db->from('main_inquirytype');		
				$this->db->where('tempid',$data->tempid);
				$this->db->order_by("inqid", "ASC");
				$tempIType = $this->db->get();
				if($tempIType->num_rows() > 0)
				{
					foreach($tempIType->result() as $itype)
					{
						$arr['main']->inquery[] = $itype;
					}
					$tempIType->free_result(); //Clean Result Set
				}				
				//Getting Notes Data
				$this->db->select('*');
				$this->db->from('main_notes');		
				$this->db->where('tempid',$data->tempid);
				$this->db->order_by("notesid", "ASC");
				$tempNotes = $this->db->get();
				if($tempNotes->num_rows() > 0)
				{
					foreach($tempNotes->result() as $notes)
					{
						$arr['main']->notes[] = $notes;
						//$arr['notes'][] = $notes;
					}
					$tempNotes->free_result(); //Clean Result Set
				}				
								
			}
		}
		if($format=='json')
		{
			echo json_encode($arr);
		}
		else
		{
		return $arr;
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function getsave_document(){
		//Login Query
		$userid = $this->session->userdata('userid');
		$query = $this->db->query("SELECT * FROM applicant_temp_document WHERE userid='".$userid."'");		
		if($query->num_rows() > 0)
		{
			foreach($query->result_array() as $ddx)
			{
				$ar[] = $ddx;
			}
			return $ar;
		}
	}
//----------------------------------------------------------------------
	/*
	* Get All Records From Giving Table
	*/
	public function get_rows($table_name)
	{
		$count_all = $this->db->get($table_name);
		
		if($count_all->num_rows() > 0)
		{
			return $count_all->num_rows();
		}
	}

//----------------------------------------------------------------------
	/*
	* Insert Applied Product Record into Database
	* Return True 
	*/
	public function add_user_registration($data)
	{
		$this->db->insert('admin_users',$data);
		return TRUE;
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	public function update_db($table,$data,$sms_id)
	{
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('sms_id',$sms_id);
		$this->db->update($table,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}

//29/12/2014-------------------------------------START

//29/12/2014-------------------------------------END

	/*
	* Search Applied Products if 
	* Return True 
	*/
	public function getInquiriesSms($type)
	{
		$this->db->where('type',$type);
		$this->db->where('status',1);
		$this->db->select('*');
		$this->db->order_by('sms_order','ASC');
		$query = $this->db->get($this->_table_sms_management);
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function sms_delete($sms_id)
	{
		$this->db->where("sms_id",$sms_id);
		$this->db->delete('sms_history');
		
		return true; 
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function checkData($table,$returnKey,$whereKey,$value){
		$this->db->select('*');
		$this->db->from($table);		
		$this->db->where($whereKey,$value);
		$checkOut = $this->db->get(); 
		$row = $checkOut->num_rows();
		$temp_main_data	=	(array)$checkOut->row();
		if($row>0){
		
			return 	$temp_main_data[''.$returnKey.''];	;
		}
		else{
			false;
		}
        
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_all_users()
	{
		$query = $this->db->query(
		"SELECT
		admin_users.id
		,admin_users.user_name
		,admin_users.firstname
		,admin_users.lastname
		,admin_users.email
		,admin_users.number
		,branches.branch_name,
		COUNT(ap.`applicantid`) AS total
		FROM admin_users
		INNER JOIN branches
		ON (admin_users.branch_id = branches.branch_id)
		LEFT JOIN `applicant_process_log` AS ap 
		ON ap.`userid` = admin_users.`id` AND ap.`stepsid` = '1'
		Where admin_users.`branch_id` !=''
		GROUP BY admin_users.`id`
		ORDER BY total DESC");
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_all_banks()
	{
		$query = $this->db->query(
		"SELECT 
		  admin_users.id,
		  admin_users.user_name,
		  admin_users.firstname,
		  admin_users.lastname,
		  admin_users.email,
		  admin_users.number,
		  bb.branch_name   
		FROM
		  `admin_users` 
		  INNER JOIN `bank_branches` AS bb 
			ON bb.`branch_id` = admin_users.`bank_branch_id` ");
		if($query->num_rows() > 0)
		{
		return $query->result();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function getNotes($applicant_id)
	{
		$this->db->select('apn.notesdate, apn.notestype, apn.notes, ah_userprofile.fullname, ah_branchs.branchname');
		$this->db->from('ah_applicant_notes AS apn');
		$this->db->join('ah_userprofile',"apn.userid=ah_userprofile.userid");
		$this->db->join('ah_branchs',"ah_branchs.branchid=apn.branchid");
		$this->db->where("apn.applicantid",$applicant_id);
		$this->db->order_by("apn.notesdate","desc");
		
		$notes = $this->db->get();
		return $notes->result();
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function getNotesCount($applicant_id)
	{
		$this->db->select('apn.notesdate, apn.notestype, apn.notes, ah_userprofile.fullname, ah_branchs.branchname');
		$this->db->from('ah_applicant_notes AS apn');
		$this->db->join('ah_userprofile',"apn.userid=ah_userprofile.userid");
		$this->db->join('ah_branchs',"ah_branchs.branchid=apn.branchid");
		$this->db->where("apn.applicantid",$applicant_id);
		$this->db->order_by("apn.notesdate","desc");
		$notes = $this->db->get();
		
		if($notes->num_rows()	>	0)
		{
			return $notes->num_rows();
		}
		else
		{
			return '0';
		}
		
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function check_record($table,$id){
		$this->db->select('*');
		$this->db->from($table);			
		$this->db->where('applicant_id',$id);
		$tempMain = $this->db->get();	
		if($tempMain->num_rows() > 0)
		{
			return $tempMain->result();
		}
		else{
			return false;
		}	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function getSmsHistory($id)
	{
		$this->db->select('sh.*');
		$this->db->from('sms_history as sh');
		$this->db->join('admin_users AS au',"au.id=sh.id");
		$this->db->where('sh.sms_receiver_id',$id);
		$this->db->order_by("sh.sms_id", "DESC");
		$tempNotes = $this->db->get();
		if($tempNotes->num_rows() > 0)
		{
			return $tempNotes->result();
		}
	}

//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_single_applicatnt($id)
	{
		//Login Query
		$this->db->where("applicant_id",$id);
		$query = $this->db->get('applicants');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_type_name($typeid)
	{
		//Login Query
		$this->db->where("list_id",$typeid);

		$query = $this->db->get('list_management');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->list_name;
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	*/
	function get_user_name_of_added($id)
	{
		//Login Query
		$this->db->select("firstname,lastname");
		
		$this->db->where("id",$id);

		$query = $this->db->get('admin_users');
			
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_tab_data($table_name,$app_id)
	{
		$this->db->where("applicant_id",$app_id); 
		$query = $this->db->get($table_name);
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}	
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_branch_name($branchid)
	{
		$this->db->where("branch_id",$branchid); 
		$query = $this->db->get('branches');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->branch_name;
		}
	}
//----------------------------------------------------------------------
	/*
	* Insert Banner Record
	* @param array $data
	* return True
	*/
	function add_banner($data)
	{
		//print_r($data);
		$this->db->insert('system_images',$data);
		
		
		
		return TRUE;
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_all_banners(){
		
		$query = $this->db->get('system_images');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function delete_banner($banner_id)
	{	
		$this->db->where("imageid",$banner_id);
		$this->db->delete('system_images');
		
		return true; 	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function delete_banner_ajax($image)
	{
		$this->db->where('banner_image',$image);
		$query 	= 	$this->db->get('system_images');
		$img_id	=	$query->row()->imageid;

		if($query->num_rows() > 0)
		{
			$this->db->where("imageid",$img_id);
			$this->db->delete('system_images');

		}
		
		if(file_exists(rootpath.'upload_files/banners/'.$image))
		{
			unlink(rootpath.'upload_files/banners/'.$image);	
		}
		
		return true; 	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function delete_image($image,$type ='')
	{
		
		if($type !=""){
				if($type == 'commetti'){
					

					$this->db->where('image_name',$image);
				$query = $this->db->get('comitte_decision_image');
				$guarantee_img_id	=	 $query->row()->id;
				
				if($query->num_rows() > 0){
				$this->db->where("id",$guarantee_img_id);
				$this->db->delete('comitte_decision_image');
				
				}
					
				}
		}
		else{
				$this->db->where('gurarntee_image',$image);
				$query = $this->db->get('guanttee_attachment');
				$guarantee_img_id	=	 $query->row()->guarantee_img_id;
				if($query->num_rows() > 0){
				$this->db->where("guarantee_img_id",$guarantee_img_id);
				$this->db->delete('guanttee_attachment');
				
				}
		
		}

		if(file_exists(rootpath.'upload_files/documents/'.$image)){
			unlink(rootpath.'upload_files/documents/'.$image);	
		}
		
		
		return true; 	
	}
//--------------------------------------------------------------------------	
	/*
	*
	* Delete Document File From Database asa well from Folder
	*
	*/
	function delete_document($image)
	{
		$this->db->where('documentname',$image);
		$query = $this->db->get('applicant_document');
		$documentid	=	 $query->row()->documentid;

		if($query->num_rows() > 0)
		{
			$this->db->where("documentid",$documentid);
			$this->db->delete('applicant_document');

		}
		
		if(file_exists(rootpath.'upload_files/documents/'.$image))
		{
			unlink(rootpath.'upload_files/documents/'.$image);	
		}
		
		return true; 	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	public function update_status($image_id,$data)
	{
		$this->db->where('imageid',$image_id);
		$this->db->update('system_images', $data);
		
		return TRUE;
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function get_banner_detail($image_id)
	{
		$this->db->where('imageid',$image_id);
		
		$query = $this->db->get('system_images');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	public function update_banner($image_id,$data)
	{
		$this->db->where('imageid',$image_id);
		$this->db->update('system_images', $data);
		
		return TRUE;
	}
//-------------------------------------------------------------

	/*
	* Delete APPLICANT Document
	* @param $docid	integer
	* RETURN TRUE
	*/
	public function delete_applicant_document($docid)
	{
		$this->db->select('doc.document,ap.applicantcode');
        $this->db->from('ah_applicant_documents AS doc');
		$this->db->join('ah_applicant AS ap','doc.applicantid=ap.applicantid');
        $this->db->where('doc.appli_doc_id',$docid);
		$query = $this->db->get();
				
		if($query->num_rows() > 0)
		{
			$document = $query->row();
			$fullPath = './resources/applicants/'.$document->applicantcode.'/'.$document->document;
			
			unlink($fullPath);
			$this->db->query("DELETE FROM ah_applicant_documents WHERE appli_doc_id='".$docid."' ");
		}
			
		return TRUE;
	}
//-------------------------------------------------------------

	/*
	* GET details according to parameters
	* @param $select_fields STRING
	* @param $condition ARRAY
	* @param $charity_type_id INTEGER
	* @param $type STRING
	* return JSON DATA
	*/
	public function get_detail_by_param($select_fields,$condition,$charity_type_id,$type)
	{
		$this->db->select($select_fields);
		$this->db->from('ah_applicant');
		$this->db->like($condition);
		$this->db->order_by('applicantid','DESC');
		$this->db->limit('1');
		$query = $this->db->get();
			
		if($query->num_rows() > 0)
		{
			$other_type	=	NULL;
			foreach($query->result() as $data)
			{
				if($type	==	'MOTHER CARD')
				{
					$refil_data_url	=	''.base_url().'inquiries/applicants_mother_help/'.$data->applicantid.'/'.$data->mother_id_card.'';
				}
				else if($type	==	'FATHER CARD')
				{
					$refil_data_url	=	''.base_url().'inquiries/applicants_father_help/'.$data->applicantid.'/'.$data->father_id_card.'';
				}
				else
				{
					$refil_data_url	=	charity_edit_url($charity_type_id).$data->applicantid.'/refilData';
				}
				
				if($type	==	'CARD')
				{
					$fullname = $data->fullname.' ('.$data->idcard_number.')';
				}
				else if($type	==	'MOTHER CARD')
				{
					$other_type	=	'mother';
					$fullname = $data->mother_name.' ('.$data->mother_id_card.')';
				}
				else if($type	==	'FATHER CARD')
				{
					$other_type	=	'father';
					$fullname = $data->father_name.' ('.$data->father_id_card.')';
				}
				else
				{
					$fullname = $data->fullname.' ('.$data->passport_number.')';
				}
				
				$arr[] = array('id'=>$data->applicantid,'label'=>$fullname,'value'=>$data->idcard_number,'refil_data_url'=>$refil_data_url,'other'	=>	$other_type);
			}
			
			return json_encode($arr);
		}
	}
//-------------------------------------------------------------

	/*
	* GET APPLICANT ID accordint to ID CARD NUMBER
	* $param $id integer
	* return applicantid
	*/

	public function get_applicant_id($id)
	{
		$this->db->select('applicantid');
		$this->db->from('ah_applicant');		
		$this->db->where('idcard_number',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->applicantid;
		}
		else
		{
			return 0;
		}
	}
//-------------------------------------------------------------

	/*
	* GET APPLICANT ID accordint to PASSPORT NUMBER
	* $param $id integer
	* return applicantid
	*/
	public function get_passport_number($id)
	{
		$this->db->select('applicantid');
		$this->db->from('ah_applicant');		
		$this->db->where('passport_number',$id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->applicantid;
		}
		else
		{
			return 0;
		}
	}
//-------------------------------------------------------------

	/*
	* GET APPLICANT ID accordint to WIFE PASSPORT NUMBER
	* $param $id integer
	* return applicantid
	*/
	public function get_passport_number_wife($id)
	{
		$this->db->select('applicantid');
		$this->db->from('ah_applicant_wife');		
		$this->db->where('relationdoc',$id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->applicantid;
		}
		else
		{
			return 0;
		}
	}
//-------------------------------------------------------------

	/*
	* GET APPLICANT ID accordint to WIFE PASSPORT NUMBER
	* $param $id integer
	* return applicantid
	*/
	public function get_passport_number_wife_info($term,$charity_type_id)
	{
		$this->db->select('applicantid,relationname,relationdoc');
		$this->db->from('ah_applicant_wife');
		$this->db->like('relationdoc',$term);
		$this->db->limit('1');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$html = '<div class="col-md-12">';
			foreach($query->result() as $data)
			{
				//{"id":"Upupa epops","label":"Eurasian Hoopoe","value":"Eurasian Hoopoe"}
				$refil_data_url	=	charity_edit_url($charity_type_id).$data->applicantid.'/refilData';
				
				$fullname	=	$data->relationname.' ('.$data->relationdoc.')';
				$arr[] 		=	array('id'=>$data->applicantid,'label'=>$fullname,'value'=>$data->relationdoc,'refil_data_url'=>$refil_data_url);
			}
			
			return json_encode($arr);
		}
	}
//-------------------------------------------------------------	
	/*
	*	UPDATE SURVAY
	*	@parm $applicantid integer
	*	@parm $ah_applicant_survayresult array
	*	return TRUE
	*/
	function update_survay($applicantid,$ah_applicant_survayresult)
	{
		$this->db->where('applicantid', $applicantid);
		$this->db->update('ah_applicant_survayresult',json_encode($ah_applicant_survayresult),$this->_login_userid,$ah_applicant_survayresult);
		
		return TRUE;	
	}
//-------------------------------------------------------------	
	/*
	*	ADD SURVAY
	*	@parm $ah_applicant_survayresult array
	*	return TRUE
	*/
	function add_survay($ah_applicant_survayresult)
	{
		$this->db->insert('ah_applicant_survayresult',$ah_applicant_survayresult,json_encode($ah_applicant_survayresult),$this->_login_userid);

		return TRUE;	
	}
//-------------------------------------------------------------	
	/*
	*	Get Exetra Telephone Number
	*	@parm $id integer
	*	return integer
	*/
	function get_extratelephone($id)
	{
		$query	=	$this->db->query("SELECT extratelephone FROM ah_applicant WHERE applicantid=".$id."");
		
		if($query->num_rows()	>	0)
		{
			return $query->row()->extratelephone;
		}
			
	}
//-------------------------------------------------------------	
	/*
	*	Get Template
	*	return ARRAY
	*/
	function get_template()
	{
		$query = $this->db->query("SELECT templatesubject, template FROM system_sms_template WHERE templatestatus='1' AND delete_record='0' ORDER BY templatesubject ASC");

		if($query->num_rows()	>	0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------	
	/*
	*	UPDATE SMS Template
	*	@parm $templateid integer
	*	@parm $data array
	*	return TRUE
	*/
	function update_sms_template($templateid,$data)
	{
		$this->db->where('templateid',$templateid);
		$this->db->update('system_sms_template', $data);
		
		return TRUE;	
	}
//-------------------------------------------------------------	
	/*
	*	ADD SMS Template
	*	@parm $data array
	*	return TRUE
	*/
	function add_sms_template($data)
	{
		$this->db->insert('system_sms_template',$data);
		
		return TRUE;	
	}
//-------------------------------------------------------------	
	/*
	*	Get all Templates
	*	return ARRAY
	*/
	function get_all_sms_templates($templateid)
	{
		$query = $this->db->query("SELECT * FROM system_sms_template WHERE templateid='".$templateid."'");

		if($query->num_rows()	>	0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------	
	/*
	* 
	* 
	*/
	function get_last_decission($applicant_id)
	{
		$decission_query = $this->db->query("SELECT decissionid,decission_amount,decissiontime,applicant_name,medical_accessories,applicant_bank,applicant_account_number FROM ah_applicant_decission WHERE applicantid='".$applicant_id."' ORDER BY decissiontime DESC LIMIT 0,1 ");

		if($decission_query->num_rows()	>	0)
		{
			return $decission_query->row();
		}
	}
//-------------------------------------------------------------	
	/*
	*
	*
	*/
	function get_money_last($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		if($branchid!=0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		
		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.application_status != ",'رفض');
		$this->db->where("ah_applicant.application_status != ",'');
		$this->db->where("ah_applicant.step",'4');			
		$this->db->order_by('ah_applicant.registrationdate','DESC');
					
		$query = $this->db->get();
		
		return $query->result();
	}
//-------------------------------------------------------------	
	/*
	*
	*
	*/
	function rejected_list($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.meeting_number,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->join('ah_applicant_survayresult','ah_applicant_survayresult.applicantid = ah_applicant.applicantid');
		
		if($branchid!=0)
		{	
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		
		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.application_status",'رفض');
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		$query = $this->db->get();
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function update_final_decission($applicantid,$data)
	{
		$json_data	=	json_encode(array('record'=>'update','applicantid'	=>	$applicantid));
		
		$this->db->where('applicantid', $applicantid);

		$this->db->update('ah_applicant',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
		
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function add_final_decission($ah_applicant_decission)
	{
		$this->db->insert('ah_applicant_decission',$ah_applicant_decission,json_encode($ah_applicant_decission),$this->_login_userid);

		return TRUE;
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_socilservay_list_special($branchid,$charity_type,$step	=	NULL)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya,ah_applicant_survayresult.ajel');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->join('ah_applicant_survayresult','ah_applicant_survayresult.applicantid = ah_applicant.applicantid');
		
		if($branchid	!=	0)
		{	
			$this->db->where("ah_applicant.branchid",$branchid); 
		}
		
		if($charity_type	!=	0)
		{	
			$this->db->where("ah_applicant.charity_type_id",$charity_type); 
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		
		if($step	!=	NULL)
		{
			$this->db->where("ah_applicant.step >",$step);
			//$this->db->where("ah_applicant.step",$step);
		}
		else
		{
			$this->db->where("ah_applicant.step",'2');
		}
		
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		return $query->result();
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function get_socilservay_list_special_slt($branchid,$charity_type,$step	=	NULL)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya,ah_applicant_survayresult.ajel');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->join('ah_applicant_survayresult','ah_applicant_survayresult.applicantid = ah_applicant.applicantid');
		
		if($branchid	!=	0)
		{	
			$this->db->where("ah_applicant.branchid",$branchid); 
		}
		
		if($charity_type	!=	0)
		{	
			$this->db->where("ah_applicant.charity_type_id",$charity_type); 
		}	
		
		$this->db->where("ah_applicant.application_status",'إعادة نظر');
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.isOld",'1');
		$this->db->where("ah_applicant.step",$step);
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();

		return $query->result();
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function get_ssr_rejected($charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya,ah_applicant_survayresult.ajel');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->join('ah_applicant_survayresult','ah_applicant_survayresult.applicantid = ah_applicant.applicantid');
		
		if($branchid	!=	0)
		{	
			$this->db->where("ah_applicant.branchid",$branchid); 
		}
		
		if($charity_type	!=	0)
		{	
			$this->db->where("ah_applicant.charity_type_id",$charity_type); 
		}	
		
		$this->db->where("ah_applicant.application_status",'رفض');
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step",'2');
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		return $query->result();
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function getLast_decission($applicantid,$stepid)
	{
		$this->db->select("ah_applicant_decission.*, ah_userprofile.fullname");
		$this->db->from("ah_applicant_decission");
		$this->db->join("ah_userprofile","ah_applicant_decission.userid=ah_userprofile.userid");
		$this->db->where("ah_applicant_decission.applicantid",$applicantid);
		$this->db->where("ah_applicant_decission.step",$stepid);
		$this->db->order_by("ah_applicant_decission.decissiontime","desc");
		$this->db->limit(1);
		
		return $this->db->get()->row();
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/
	function get_all_decission($applicantid,$stepid)
	{
		$this->db->select("ah_applicant_decission.*, ah_userprofile.fullname");
		$this->db->from("ah_applicant_decission");
		$this->db->join("ah_userprofile","ah_applicant_decission.userid=ah_userprofile.userid");
		$this->db->where("ah_applicant_decission.applicantid",$applicantid);
		$this->db->where("ah_applicant_decission.step",$stepid);
		$this->db->order_by("ah_applicant_decission.decissiontime","desc");
		
		return $this->db->get()->result();
	}	
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_total_family_members($totalfamilymember,$familysalary)
	{
		$query  		=	$this->db->query("SELECT * FROM ah_financial WHERE '".$totalfamilymember."' BETWEEN people_start AND people_end AND netincome >= '".$familysalary."' LIMIT 0,1");
		
		//echo $this->db->last_query();
		return $result 	=	$query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_finaldecission_list($branchid,$charity_type,$listid)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.meeting_number,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->join('ah_applicant_sections','ah_applicant_sections.applicantid = ah_applicant.applicantid');
		
		if($branchid!=0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}
		if($listid)
		{
			
			$this->db->where("ah_applicant.section_listid",$listid);
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step",'3');
		$this->db->where("ah_applicant.application_status != ",'رفض');
		$this->db->where("ah_applicant.application_status != ",'');		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
					
		$query = $this->db->get();

		return	$query->result();	
	}
//-------------------------------------------------------------
	/*
	* CALL Stored Procedure
	* @param $branchid integer
	* @param $charity_type integer
	* return OBJECT
	*/
	function socilservay_list_storedprocedure($branchid,$charity_type)
	{
		 $call_procedure ="CALL step_1('$branchid', '$charity_type')";
		 
		 $query	=	$this->db->query($call_procedure);
	
		 if($query->num_rows()	>	0)
		 {
			 return $query->result();
		 } 
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_socilservay_list($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id', 'left');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province', 'left');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya', 'left');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid', 'left');
		
		if($branchid	!=	0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		if($charity_type	!=	0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step",'1');
		$this->db->where("ah_applicant.isOld IS NULL");
		$this->db->order_by('ah_applicant.registrationdate','DESC');

		$query = $this->db->get();
		
		if($query->num_rows()	>	0)
		{
			return	$query->result();
		}
		
			
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_socilservay_list_completesteps($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		if($branchid	!=	0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		if($charity_type	!=	0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step >",'1');
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		$query = $this->db->get();
		
		return	$query->result();	
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	function get_socilservay_list_step($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');		
		
		if($branchid!=0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		
		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}
		
		$this->db->where("ah_applicant.application_status","إعادة نظر");
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step",'1');
		$this->db->order_by('ah_applicant.registrationdate','DESC');

		return $this->db->get()->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_inquiries_list($branchid,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.userid,ah_applicant.gender,ah_applicant.charity_type_id,ah_applicant.step,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		
		if($branchid!=0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		
		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		return	$query->result();	
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function save_my_notes($ah_applicant_notes)
	{
		$this->db->insert('ah_applicant_notes',$ah_applicant_notes,json_encode($ah_applicant_notes),$this->_login_userid);
		
		return TRUE;
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_all_helps($charity_type,$id_card_number)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.charity_type_id,ah_applicant.step,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');

		$this->db->where("ah_applicant.idcard_number",$id_card_number);
		$this->db->where("ah_applicant.charity_type_id",$charity_type);
		$this->db->where("ah_applicant.isdelete",'0');
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_all_mother_helps($charity_type,$imother_card_number)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.charity_type_id,ah_applicant.step,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya,ah_applicant.father_name,ah_applicant.father_id_card,ah_applicant.mother_name,ah_applicant.mother_id_card');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');

		$this->db->where("ah_applicant.mother_id_card",$imother_card_number);
		//$this->db->where("ah_applicant.charity_type_id",$charity_type);
		$this->db->where("ah_applicant.isdelete",'0');
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_all_father_helps($charity_type,$father_card_number)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.charity_type_id,ah_applicant.step,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya,ah_applicant.father_name,ah_applicant.father_id_card,ah_applicant.mother_name,ah_applicant.mother_id_card');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');

		$this->db->where("ah_applicant.father_id_card",$father_card_number);
		//$this->db->where("ah_applicant.charity_type_id",$charity_type);
		$this->db->where("ah_applicant.isdelete",'0');
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		//return $this->db->last_query();
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_list_taqareer_qaima($type)
	{
		$this->db->select('*');
		$this->db->where('list_type',$type);
		$query = $this->db->get('list_management');
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_tadeel_count($id)
	{
		$this->db->select('*');
		$this->db->where('list_type',$type);
		$query = $this->db->get('list_management');
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function add_decission($ah_applicant_decission)
	{
		$this->db->insert('ah_applicant_decission',$ah_applicant_decission,json_encode($ah_applicant_decission),$this->_login_userid);

		return TRUE;
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function update_decission($applicantid,$ah_applicant)
	{
		$this->db->where('applicantid',$applicantid);
		$this->db->update('ah_applicant',json_encode($ah_applicant),$this->_login_userid,$ah_applicant);
		
		return TRUE;
	}
//-------------------------------------------------------------
	/*
	* Get Total SONS
	* @param $charity_type_id int
	* @param $ah_applicant int
	*/
	function total_count_son($ah_applicant)
	{
		$this->db->where('applicantid',$ah_applicant);
		$this->db->where('relationtype','128');
		
		$query = $this->db->get('ah_applicant_relation');
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}	
//-------------------------------------------------------------
	/*
	* Get Total DAUGHTERS
	* @param $charity_type_id int
	* @param $ah_applicant int
	*/
	function total_count_daughter($ah_applicant)
	{
		$this->db->where('applicantid',$ah_applicant);
		$this->db->where('relationtype','129');
		
		$query = $this->db->get('ah_applicant_relation');
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}	
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_templates()
	{
		$query 	=	$this->db->query("SELECT `templateid`,`templatesubject`,`template`,`tempatedate`,`templatestatus` FROM `system_sms_template` ORDER BY templatesubject ASC;");
		
		return $query->result();
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function get_sms_history_list($module_id,$userid)
	{
		if($module_id	==	'1')
		{
			$this->db->select('sms_history.*,admin_users.firstname,admin_users.lastname,main_applicant.first_name,main_applicant.middle_name,main_applicant.last_name,main_applicant.family_name');
		}
		else
		{
			$this->db->select('sms_history.*,admin_users.firstname,admin_users.lastname,applicants.applicant_first_name,applicants.applicant_middle_name,applicants.applicant_last_name,applicants.applicant_sur_name');
		}
		
		$this->db->from('sms_history');
		
		if($module_id	==	'1')
		{
			$this->db->join('main_applicant',"main_applicant.tempid=sms_history.sms_receiver_id");
		}
		else
		{
			$this->db->join('applicants',"applicants.applicant_id=sms_history.sms_receiver_id");
		}
		
		$this->db->join('admin_users',"admin_users.id=sms_history.sms_sender_id");
		$this->db->where('sms_module_id',$module_id);
		$this->db->where('sms_receiver_id',$userid);
		$this->db->group_by('sms_history.`sms_id`');
		
		$query = $this->db->get();
				
		return $query->result();
	}
	
//-------------------------------------------------------------	
	/*
	*	UPDATE SMS Template
	*	@parm $templateid integer
	*	@parm $data array
	*	return TRUE
	*/
	function update_applicant_in_socialservay($applicantid,$data)
	{
		$this->db->where('applicantid',$applicantid);
		$this->db->update('ah_applicant',json_encode($data),$this->_login_userid,$data);
		
		return TRUE;	
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function bernamij_almasadaat_types($step,$branchid)
	{
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='charity_type' AND list_status='1' AND list_name!='اضافة اليتيم'  AND list_name!='اضافة الكفيل' AND list_name!='النقدية' AND delete_record='0'");
		$result_1	=	$query_1->result();
		
		$almasadaat	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(applicantid) as total FROM `ah_applicant` WHERE charity_type_id='".$type->list_id."' AND isdelete='0' AND step='".$step."' AND branchid='".$branchid."'	AND isOld	IS NULL");
				$result_2	=	$query_2->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		
		return $data;
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function section_types($step,$branchid,$parent_id)
	{
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='section_types' AND list_parent_id='".$parent_id."' AND list_status='1' AND delete_record='0' ORDER BY list_order ASC");
		$result_1	=	$query_1->result();
		
		$almasadaat	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(applicantid) as total FROM `ah_applicant` WHERE charity_type_id='".$type->list_id."' AND isdelete='0' AND step='".$step."' AND branchid='".$branchid."'");
				$result_2	=	$query_2->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		
		return $data;
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function section_types_countries($step,$branchid,$type,$list_parent_id,$section_status,$charity_type_id)
	{
		if($list_parent_id)
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"";
		}
		
		if($section_status	!= '1708' && $reagionid	!=	'1709' && $reagionid	!=	'1722' && $section_status	!='1704')
		{
			$section_status	=	"AND section_status='".$section_status."'";
		}
		else
		{
			$section_status	=	"AND section_status='0'";
		}
		
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND list_parent_id='0' AND delete_record='0' ".$query_string." ");
		$result_1	=	$query_1->result();
		//echo $this->db->last_query();exit();
		//echo '<pre>'; print_r($result_1);exit();
		
		$almasadaat	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(applicantid) as total FROM `ah_applicant` WHERE country='".$type->list_id."' ".$section_status." AND isdelete='0' AND step='".$step."' AND branchid='".$branchid."' AND charity_type_id='".$charity_type_id."'");
				$result_2	=	$query_2->row()->total;
				//echo $this->db->last_query().'<br>';
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		//exit();
		return $data;
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function section_types_regions($step,$branchid,$type,$list_parent_id,$section_status,$charity_type_id)
	{
		if($list_parent_id)
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"";
		}
		
		if($section_status	!= '1708' && $reagionid	!=	'1709' && $reagionid	!=	'1722' && $section_status	!='1704')
		{
			$section_status	=	"AND section_status='".$section_status."'";
		}
		else
		{
			$section_status	=	"AND section_status='0'";
		}
		
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");
		$result_1	=	$query_1->result();
		
		//echo '<pre>'; print_r($result_1);exit();
		
		$almasadaat	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(applicantid) as total FROM `ah_applicant` WHERE province='".$type->list_id."' ".$section_status." AND isdelete='0' AND step='".$step."' AND branchid='".$branchid."' AND charity_type_id='".$charity_type_id."'");
				$result_2	=	$query_2->row()->total;
				//echo $this->db->last_query();
				//exit();
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		
		return $data;
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function section_types_sates($step,$branchid,$type,$list_parent_id,$section_status,$charity_type_id)
	{
		if($list_parent_id)
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"";
		}
		
		if($section_status	!= '1708' && $reagionid	!=	'1709' && $reagionid	!=	'1722' && $section_status	!='1704')
		{
			$section_status	=	"AND section_status='".$section_status."'";
		}
		else
		{
			$section_status	=	"AND section_status='0'";
		}
		
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");
		$result_1	=	$query_1->result();
		
		$almasadaat	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(applicantid) as total FROM `ah_applicant` WHERE wilaya='".$type->list_id."' ".$section_status." AND isdelete='0' AND step='".$step."' AND branchid='".$branchid."' AND charity_type_id='".$charity_type_id."'");
				$result_2	=	$query_2->row()->total;

				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}

		return $data;
	}
//-------------------------------------------------------------
	/*
	* Add First Ziyarah
	* @param $data ARRAY
	* return TRUE
	*/
	function save_first_ziyarah($data)
	{
		$this->db->insert('ah_applicant_first_zyarah',$data,json_encode($data),$this->_login_userid);

		return TRUE;
	}

//-------------------------------------------------------------	
	/*
	*	ADD Yateem SURVAY
	*	@parm $ah_applicant_survayresult array
	*	return TRUE
	*/
	function add_yateem_servay($ah_applicant_survayresult)
	{
		$this->db->insert('ah_yateem_servay',$ah_applicant_survayresult,json_encode($ah_applicant_survayresult),$this->_login_userid);

		return TRUE;	
	}
//-------------------------------------------------------------	

	/*
	*
	*/
	public function update_yateem_step($orphan_id,$step)
	{
		$data = array('step'=>$step);

		$this->db->where('orphan_id', $orphan_id);

		$this->db->update('ah_yateem_info',json_encode($data),$this->session->userdata('userid'),$data);
		
		return TRUE;	
	}
//-------------------------------------------------------------
	/*
	* Get Branch ID and Type ID
	* @param $charity_type_id int
	* @param $ah_applicant int
	*/
	function get_branch_type($applicantid)
	{
		$this->db->select('branchid,charity_type_id');
		$this->db->where('applicantid',$applicantid);
		
		$query = $this->db->get('ah_applicant');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	
//-------------------------------------------------------------

	public function update_gender($gender,$applicantid)
	{
		$query = $this->db->query("UPDATE ah_applicant SET gender='".$gender."' WHERE applicantid='".$applicantid."'");
	}
//----------------------------------------------------------------------
    /*
    * Update Applicant user history
    */
    function update_history($data)
    {
        $this->db->insert('ah_applicant_users_history',$data);

        return TRUE;
    }
//----------------------------------------------------------------------
    /*
    * Update Applicant user history
    */
	function get_users_history($applicantid)
	{
		$this->db->select('userid,action_date,action');
		$this->db->where('applicantid',$applicantid);
		$this->db->order_by('history_id','DESC');
		
		$query = $this->db->get('ah_applicant_users_history');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* Get Transaction search result
	*/
	public function get_transaction_Search_result($data)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.date_of_birth,ah_applicant.userid,ah_applicant.gender,ah_applicant.charity_type_id,ah_applicant.step,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.marital_status,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		
		if($data['branchid']!=0)
		{
			$this->db->where("ah_applicant.branchid",$data['branchid']);
		}
		else if($data['idcard_number']!=0)
		{
			$this->db->like("ah_applicant.idcard_number",$data['idcard_number']);
		}
		else if($data['fullname'])
		{
			$this->db->like("ah_applicant.fullname",$data['fullname']);
		}
		else if($data['marital_status']!=0)
		{
			$this->db->where("ah_applicant.marital_status",$data['marital_status']);
		}
		else if($data['passport_number']!=0)
		{
			$this->db->where("ah_applicant.passport_number",$data['passport_number']);
		}
		else if($data['country_listmanagement']!=0)
		{
			$this->db->like("ah_applicant.country_listmanagement",$data['country_listmanagement']);
		}
		else if($data['date_of_birth'])
		{
			$this->db->like("ah_applicant.date_of_birth",$data['date_of_birth']);
		}
		else if($data['province'])
		{
			$this->db->where("ah_applicant.province",$data['province']);
		}
		else if($data['extratelephone'])
		{
			$this->db->like("ah_applicant.extratelephone",$data['extratelephone']);
		}
		
		if($data['wilaya'])
		{
			$this->db->where("ah_applicant.wilaya",$data['wilaya']);
		}
		if($data['charity_type'])
		{
			$this->db->where("ah_applicant.charity_type_id",$data['charity_type']);
		}
		
		if(array_filter($data)) 
		{
			$this->db->where("ah_applicant.isdelete",'0');
			$this->db->order_by('ah_applicant.registrationdate','DESC');
			
		}
		else
		{
			$this->db->limit(50);
			$this->db->order_by('ah_applicant.registrationdate','DESC');
		}
		
		
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//---------------------------------------------------------------------
	/*
	*
	*
	*/
	function section_applicants_list($wilaya,$branchid,$step,$section_status,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.meeting_number,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		if($branchid!=0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		if($wilaya!=0)
		{
			$this->db->where("ah_applicant.wilaya",$wilaya);
		}
		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}
		if($section_status	!=	'1708' && $section_status	!=	'1709' && $section_status	!=	'1717' && $section_status	!=	'1722'	&& $section_status	!='1704')
		{
			$this->db->where("ah_applicant.section_status",$section_status);
		}
		else
		{
			$this->db->where("ah_applicant.section_status",'0');
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step",$step);
		//$this->db->where(array('ah_applicant.section_status' => NULL));
		$this->db->where("ah_applicant.application_status != ",'رفض');
		$this->db->where("ah_applicant.application_status != ",'');		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
					
		$query = $this->db->get();
		
		//echo $this->db->last_query();
		if($query->num_rows()	>	0)
		{
			return	$query->result();
		}
			
	}
//----------------------------------------------------------------------
	/*
	* Get No OF List 
	* 
	*/	
	function get_no_list($listid = 0)
    {		
		$this->db->select('listid,listname,listdate,listno');

		if($listid	!=	0 && $listid	!=	'')
		{
			$this->db->where('listid',$listid);
		}
		
		$this->db->order_by("listid", "ASC");
		$query =	$this->db->get('no_list_managment');	
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//----------------------------------------------------------------------
	/*
	* Get No OF List 
	* 
	*/	
	function get_total($listid,$charity_type_id,$step)
    {
		$query	=	$this->db->query("
					SELECT 
					COUNT(
					`ah_applicant`.`section_listid`
					) AS total 
					FROM
						`no_list_managment` 
					LEFT JOIN `ah_applicant` 
					ON (
					`no_list_managment`.`listid` = `ah_applicant`.`section_listid`
					) 
					WHERE `ah_applicant`.`section_listid` = '".$listid."'
					AND ah_applicant.`charity_type_id` = '".$charity_type_id."' 
					AND ah_applicant.step = '".$step."'");
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return '0';
		}
    }
//----------------------------------------------------------------------
	/*
	* Submit No List Data
	* 
	*/	
	function save_no_list_management()
	{
		$listid		=	$this->input->post("listid");
		$listname	=	$this->input->post("listname");
		$listdate	=	$this->input->post("listdate");
		
		if($listid	!=	'')
		{
			$no_list 	= array(
				'listid'	=>	$listid,
				'listdate'	=>	$listdate);
						
			$this->db->where('listid', $listid);
			$this->db->update('no_list_managment',json_encode($no_list),$this->session->userdata('userid'),$no_list);
		}
		else
		{
			// Check list Name EXIST OR NOT
			$result		=	$this->listno_exist($listname);
			
			if(!$result)
			{
				$no_list 	= array(
					'listid'	=>	$listid,
					'listname'	=>	$listname,
					'listno'	=>	$listno,
					'listdate'	=>	$listdate);
				
				$this->db->insert('no_list_managment',$no_list,json_encode($no_list),$this->session->userdata('userid'));
			}
			else
			{
				echo 'هذه القائمة لا هو موجود بالفعل';
			}
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/	
	function get_single_no_list($listid)
    {		
		$this->db->select('listid,listname,listdate,listno');;

		$this->db->where('listid',$listid);
		$this->db->order_by("listid", "ASC");
		$query	=	$this->db->get('no_list_managment');	
		
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
    }
//----------------------------------------------------------------------
	/*
	* Delete list No Record
	* 
	*/	
	function delete_no_list_management($listid)
    {
		$json_data	=	json_encode(array('record'=>'delete','listid'=>$listid));		
		$data 		=	array('liststatus'	=>	'1');
					
		$this->db->where('listid', $listid);
		$this->db->update('no_list_managment',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//----------------------------------------------------------------------
	/*
	*  Check List NO Exist Or Not
	* 
	*/	
	function listno_exist($listno)
    {	
		$this->db->select('listname');
		$this->db->where('listname',$listno);

		$query	=	$this->db->get('no_list_managment');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->listname;
		}
    }
//-------------------------------------------------------------
	/*
	*
	* UPDATE SECTION STATUS
	*/
	function update_section_status($applicantid,$data)
	{
		$json_data	=	json_encode(array('record'=>'update','applicantid'	=>	$applicantid));
		
		$this->db->where('applicantid', $applicantid);

		$this->db->update('ah_applicant',$json_data,$this->session->userdata('userid'),$data);

		return TRUE;
	}
//-------------------------------------------------------------
	/*
	*
	*
	*/
	function add_applicant_section($ah_applicant_section)
	{
		$this->db->insert('ah_applicant_sections',$ah_applicant_section,json_encode($ah_applicant_section),$this->_login_userid);

		return TRUE;
	}
//----------------------------------------------------------------------
	/*
	*  Get Detail
	* 
	*/	
	function get_section_notes($applicantid,$section_status)
    {
		$query	=	$this->db->query("SELECT
		`APS`.`section_id`
		, `APS`.`userid`
		, `APS`.`applicantid`
		, `APS`.`listid`
		, `APS`.`section_status`
		, `APS`.`notes`
		, `APS`.`doctor_notes`
		, `APS`.`proposal_section`
		, `APS`.`maintenance_type`
		, `APS`.`pictures`
		, `APS`.`certificate`
		, `APS`.`report_visit`
		, `APS`.`list_type_amount`
		, `APS`.`list_type_notes`
		, `APS`.`submited_date`
		, `NL`.`listname`
		FROM
			`ah_applicant_sections` AS APS
		INNER JOIN `no_list_managment` AS NL 
			ON (`APS`.`listid` = `NL`.`listid`) WHERE APS.`applicantid`=".$applicantid." AND APS.`section_status`=".$section_status.";");

		if($query->num_rows() > 0)
		{
			return $query->row();
		}
    }
//------------------------------------------------------
	/*
	*
	*
	*/
	public function section_types_parents($step,$branchid,$parent_id	=	NULL)
	{
		
		if($parent_id)
		{
			$query_string	=	"AND list_parent_id='".$parent_id."'";
		}
		else
		{
			$query_string	=	"AND list_parent_id='0'";
		}
		
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='section_types' AND list_status='1' ".$query_string." AND delete_record='0'");
		$result_1	=	$query_1->result();

		$almasadaat	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(applicantid) as total FROM `ah_applicant` WHERE charity_type_id='".$type->list_id."' AND isdelete='0' AND step='".$step."' AND branchid='".$branchid."'");
				$result_2	=	$query_2->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}

		return $data;
		/*
		SELECT 
		l.`list_id`,
		l.`list_name`,
		IFNULL(total,0) AS total
		FROM
		ah_listmanagement AS l 
		LEFT JOIN 
		(SELECT 
		COUNT(applicantid)AS total,
		charity_type_id  
		FROM
		`ah_applicant` 
		WHERE isdelete = '0' 
		AND step = '3') AS lnew 
		ON lnew.charity_type_id = l.list_id 
		WHERE l.`list_type` = 'section_types' 
		AND l.`delete_record` = '0' 
		AND l.`list_parent_id` = '1699' 
		GROUP BY l.list_id;
		*/
	}
//---------------------------------------------------------------------
/////////////////////reejs///////////
	function getdmessagedata($id,$table,$field,$orderby = NULL){
		$this->db->select('*');
		$this->db->from($table);						
		$this->db->where($field,$id);
		if($orderby !="" )$this->db->order_by($orderby,'DESC');
		$query = $this->db->get(); 
		return $query->result();
	}
	function getdata($id,$table,$column){
		$this->db->select('*');
		$this->db->from($table);						
		$this->db->where($column,$id);
		$query = $this->db->get(); 
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	function savestore($data,$table,$feild)
	{
		$vals= $data[$feild];
		if($vals	!=	'')
		{
			$this->db->where($feild,$vals);
			$this->db->update($table,json_encode($data),$this->_login_userid,$data);
			return $vals;
		}
		else
		{
			$this->db->insert($table,$data,json_encode($data),$this->_login_userid);
			return $this->db->insert_id();
		}
	}
	
	/*------------------------*/
	 /*Reeja Soni 22-5-17 start*/
	/*------------------------*/

	function get_total_users($listname,$charity_type_id,$branchid,$application_status='0')
    {		
		$sql	="
				SELECT 
				COUNT(
				`ah_applicant_sections`.`listid`
				) AS total 
				FROM
				`ah_applicant_sections` 
			
				INNER JOIN `ah_applicant`
				ON (
				  `ah_applicant_sections`.`applicantid` = `ah_applicant`.`applicantid`
				) 
				WHERE `ah_applicant_sections`.`listid` = '".$listname."' AND ah_applicant.`charity_type_id`='".$charity_type_id."' ";
				
				if($application_status != '0'){
					$sql	.=" AND ah_applicant.application_status LIKE '".$application_status."'  ";
				}
				
				$sql	.= " and ah_applicant.branchid='".$branchid."' and ah_applicant.step='4'";
		$query	=	$this->db->query($sql);		
				
		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return '0';
		}
    }
	
/*------------------------
Reeja Soni 22-5-17 end*/
	
	function get_total_userswithStatus($listname,$approval)
    {		
		$query	=	$this->db->query("
				SELECT 
				COUNT(
				`ah_applicant_sections`.`listid`
				) AS total 
				FROM
				`ah_applicant_sections` 
				
				INNER JOIN `ah_applicant`
				ON (
				  `ah_applicant_sections`.`applicantid` = `ah_applicant`.`applicantid`
				) 
				INNER JOIN `ah_applicant_status`
				ON (
				  `ah_applicant_status`.`applicantid` = `ah_applicant`.`applicantid`
				) 
				WHERE `ah_applicant_sections`.`listid` = '".$listname."'   and ah_applicant.step='4' and  `ah_applicant`.`applicantid` IN( select ah_applicant_status.applicantid FROM ah_applicant_status where ah_applicant_status.approval='".$approval."')");

		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return '0';
		}
    }
	
	function get_userdetailsbyStatus_list($approval,$listid)
    {		
		$query	=	$this->db->query("
		SELECT ah_applicant.applicantid,ah_applicant.meeting_number,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,a.list_name AS charity_type ,b.list_name AS province ,c.list_name AS wilaya 
		FROM ah_applicant 
		INNER JOIN ah_applicant_sections ON (ah_applicant_sections.applicantid = ah_applicant.applicantid) 
		INNER JOIN ah_applicant_status ON (ah_applicant_status.applicantid = ah_applicant.applicantid) 
		INNER JOIN ah_listmanagement  as a ON (a.list_id = ah_applicant.charity_type_id) 
		INNER JOIN ah_listmanagement  as b ON (b.list_id = ah_applicant.province) 
		INNER JOIN ah_listmanagement as c ON (c.list_id = ah_applicant.wilaya) 
		INNER JOIN ah_branchs ON (ah_branchs.branchid = ah_applicant.branchid)
		WHERE ah_applicant_sections.listid='".$listid."'  AND ah_applicant_status.approval ='".$approval."'");
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return '0';
		}
    }
//-------------------------------------------------------------	
	/*
	*	Get All zyarat Visits Images
	*	@parm $applicantid integer
	*	return integer
	*/
	function all_visits_images($applicantid)
	{
		$query	=	$this->db->query("SELECT imagename FROM ah_applicant_first_zyarah WHERE applicantid=".$applicantid."");
		
		if($query->num_rows()	>	0)
		{
			return $query->result();
		}
			
	}
//////////////////////////////////////////////reeja 2-4-17//////////////////////////////////
	function listallpopulation($branchid,$charity_type,$listid)
    {		
		$query	=	$this->db->query("SELECT * 
				FROM ah_applicant 
				INNER JOIN ah_applicant_sections  as s ON (s.applicantid = ah_applicant.applicantid)
				INNER JOIN ah_applicant_decission  as d ON (d.applicantid = ah_applicant.applicantid)
				INNER JOIN no_list_managment  as a ON (a.listid = s.listid) 
				WHERE ah_applicant.branchid='".$branchid."' AND  ah_applicant.charity_type_id ='".$charity_type."' AND s.listid='".$listid."' AND  ah_applicant.step='4' GROUP BY s.applicantid");
		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return '0';
		}
    }
	function delete($id,$field,$table)
	{
		$this->db->where($field,$id);
		$this->db->delete($table);
		return true;
			
	}
/**************************************************************************************/
	function get_inquiries_record()
	{
		$this->db->select('applicantid,userid,country,province,wilaya');
		$this->db->from('ah_applicant');
		
		$query	=	$this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* Delete list No Record
	* 
	*/	
	function update_provinces($updated_id,$province_id)
    {		
		$this->db->query("UPDATE ah_applicant SET province='".$updated_id."' WHERE province='".$province_id."'");
		return TRUE;
	}
//----------------------------------------------------------------------
	/*
	* Delete list No Record
	* 
	*/	
	function update_wilaya($updated_id,$wilaya_id)
    {		
		$this->db->query("UPDATE ah_applicant SET wilaya='".$updated_id."' WHERE wilaya='".$wilaya_id."'");
		return TRUE;
	}
//-----------------------------------------------------------------------

	/*
	*
	* Check Applicant ID Exist
	*/
	public function check_appid_exist($appid)
	{
		$this->db->select('applicantid');
		$this->db->from('ah_applicant_sections');
		$this->db->where('applicantid',$appid);
		
		$query	=	$this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row()->applicantid;
		}
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	public function update_applicant_section($applicantid,$data)
	{
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('applicantid',$applicantid);
		$this->db->update("ah_applicant_sections",$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-----------------------------------------------------------------------

	/*
	*
	* Check Applicant ID Exist
	*/
	public function get_sakaniya_detail($applicantid)
	{
		$this->db->select('applicantid,maintenance_type,report_visit,pictures,certificate,proposal_section,lat,lng');
		$this->db->where("applicantid",$applicantid);
		$this->db->from('ah_applicant_sections');
		
		$query	=	$this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//---------------------------------------------------------------------
	/*
	*
	*
	*/
	function get_all_applicants_by_status($branchid,$step,$section_status,$charity_type)
	{
		$this->db->select('ah_applicant.applicantid,ah_applicant.meeting_number,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		if($branchid!=0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}

		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}
		if($section_status	!=	'1708' && $section_status	!=	'1709' && $section_status	!=	'1717' && $section_status	!=	'1722'	&& $section_status	!='1704')
		{
			$this->db->where("ah_applicant.section_status",$section_status);
		}
		else
		{
			$this->db->where("ah_applicant.section_status",'0');
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.country !=",'');
		$this->db->where("ah_applicant.step",$step);
		$this->db->where("ah_applicant.application_status != ",'رفض');
		$this->db->where("ah_applicant.application_status != ",'');		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		
					
		$query = $this->db->get();

		if($query->num_rows()	>	0)
		{
			return	$query->result();
		}	
	}
//-------------------------------------------------------------	
	/*
	*	Tranfer Detail
	*	@parm $data array
	*	return TRUE
	*/
	function tranfer_detail($data)
	{
		$this->db->insert('transfer_type_n_nolist',$data,json_encode($data),$this->_login_userid);

		return TRUE;	
	}	
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	public function update_type_nolist($applicantid,$data)
	{
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('applicantid',$applicantid);
		$this->db->update('ah_applicant',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//----------------------------------------------------------------------    
	/*
	* 
	*/	
	public function update_sectin_list($applicantid,$data)
	{
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('applicantid',$applicantid);
		$this->db->update('ah_applicant_sections',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-----------------------------------------------------------------------

	/*
	*
	* GET Quatation Record
	*/
	public function get_quatation_detail($quotationsId)
	{
		$query	=	$this->db->query("SELECT quotationsId,company,address,phone,mobile,fax,total FROM `ah_applicant_quotations` WHERE quotationsId='".$quotationsId."'");
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-----------------------------------------------------------------------

	/*
	*
	* GET CONTRACT detail
	* @param @quotationsId integer
	* return OBJECT
	*/
	public function get_contract_detail($applicantid)
	{
		$query	=	$this->db->query("
		SELECT * 
		FROM `ah_applicant_createcontract` 
		WHERE applicantid='".$applicantid."';");

		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	* Add Bank Detail according to there Countries
	* @param $data ARRAY
	* return TRUE
	*/

	function add_multifields($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_applicant_percentage_amounts',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Add Bank Detail according to there Countries
	* @param $data ARRAY
	* return TRUE
	*/

	function delete_percentage_amount($applicantid)
    {

		$this->db->query("DELETE FROM `ah_applicant_percentage_amounts` WHERE applicantid='".$applicantid."'");
		
		return TRUE;
    }
//-----------------------------------------------------------------------

	/*
	*
	* GET CONTRACT detail
	* @param @quotationsId integer
	* return OBJECT
	*/
	public function get_all_percentage_amount($applicantid)
	{
		$query	=	$this->db->query("SELECT id,index_num,applicantid,percentage,amount,notes,submit_date,status,paid,contract_notes,contract_document FROM `ah_applicant_percentage_amounts` WHERE applicantid='".$applicantid."'");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
/**************************************************************************************/
//------------------------------------------------------------------------

    /**
     * 
     * Insert User Data for Registration
     * @param array $data
     * return integer
     */
	function update_contract_status($applicantid,$data)
	{
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('applicantid',$applicantid);
		$this->db->update('ah_applicant',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-----------------------------------------------------------------------

	/*
	*
	* GET CONTRACT detail
	* @param @quotationsId integer
	* return OBJECT
	*/
	public function get_applicant_data($applicantid)
	{
		$query	=	$this->db->query("SELECT applicantid,contract_ready,contract_notes,contract_document,approve_amount FROM ah_applicant WHERE applicantid='".$applicantid."'");

		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
/**************************************************************************************/
	function get_quotation_info($id,$table,$field,$orderby = NULL){
		$this->db->select('*');
		$this->db->from($table);						
		$this->db->where($field,$id);
		if($orderby !="" )$this->db->order_by($orderby,'DESC');
		$query = $this->db->get(); 
		return $query->row();
	}
//-----------------------------------------------------------------------

	/*
	*
	* GET Quatation Record
	*/
	public function get_quatation_detail_muzi($quotationsId,$applicationid)
	{
		$query	=	$this->db->query("SELECT
    `AQD`.`quotationsId` AS  quotationsId
FROM
   `ah_applicant_quotations` AS AQ
    INNER JOIN `ah_applicant_quotations_data` AS AQD
        ON (`AQ`.`quotationsId` = `AQD`.`quotationsId`) WHERE AQD.quotationsDataId =  '".$quotationsId."' AND AQ.applicantid='".$applicationid."';");
		
		if($query->num_rows() > 0)
		{
			return $query->row()->quotationsId;
		}
	}
//------------------------------------------------------------------------

    /**
     * 
     * Insert User Data for Registration
     * @param array $data
     * return integer
     */
	function update_payment_detail($id,$data)
	{
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('id',$id);
		$this->db->update('ah_applicant_percentage_amounts',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
/*
SELECT 
  CB.`bds_id` ,MB.bd_totalamount,
  MB.bd_year,
  CB.`bds_title` 
FROM
  `ah_budget_main` AS MB,
  `ah_budget_sub` AS CB 
WHERE MB.delete_record = '0' 
  AND MB.`bd_id` = CB.`bd_id` AND CB.`bds_categoryId`='1'
*/
}
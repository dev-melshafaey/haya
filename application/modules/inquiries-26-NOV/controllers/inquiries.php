<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inquiries extends CI_Controller 
{
//-------------------------------------------------------------------------------	
	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;
//-------------------------------------------------------------------------------
	/*
	* Costructor 
	*/
	public function __construct()
	{
		parent::__construct();
		
		// Load Models
		$this->load->model('inquiries_model','inq');

		$this->_data['controller']	=	$this; 

		$this->load->library("pagination");
		
		$this->_data['module']			=	$this->haya_model->get_module(); // Get all Module Detail
		$this->_login_userid			=	$this->session->userdata('userid'); // Set userid into SESSION
		$this->_data['login_userid']	=	$this->_login_userid;
		
		// GET all detail about LOGIN User
		$this->_data['user_detail'] 	= 	$this->haya_model->get_user_detail($this->_login_userid);
		$this->_data['users_muhafizas']	=	unserialize($this->_data['user_detail']['profile']->muhafiza_ids);
		
		$this->_data['number_format']	= new NumberFormatter("ar", NumberFormatter::SPELLOUT);
		
		//use this calss for words in any language
		//echo 'Amount in Arabic = '.$this->_data['number_format']->format(560);
		
		//echo '<pre>'; print_r($this->_data['users_muhafizas']);exit();
	}
//------------------------------------------------------------------------------
	/*
	* To Update Applicant History
	* 
	*/
	public function update_history()
	{
		$all_applicatns	=	$this->inq->get_inquiries_list(NULL,NULL);
		
		foreach($all_applicatns	as $applicants)
		{
			$update_history	=	array(
			'applicantid'	=>	$applicants->applicantid,
			'userid'		=>	$applicants->userid,
			'action'		=>	'add',
			'action_date'	=>	$applicants->registrationdate
			);
			
			$this->inq->update_history($update_history); // Update the Record for History
		}
	}
//------------------------------------------------------------------------------
	/*
	* To Update Applicant History
	* 
	*/
	public function update_countries_provinces()
	{
		$all_applicatns			=	$this->inq->get_inquiries_record(NULL,NULL);
		$update_destinations	=	array();
		
		foreach($all_applicatns	as $applicants)
		{
			if($applicants->province	==	'1')
			{
				$this->inq->update_provinces('1125',$applicants->province);
			}
			else if($applicants->province	==	'2')
			{
				$this->inq->update_provinces('1126',$applicants->province);
			}
			else if($applicants->province	==	'3')
			{
				$this->inq->update_provinces('1127',$applicants->province);
			}
			else if($applicants->province	==	'4')
			{
				$this->inq->update_provinces('1128',$applicants->province);
			}
			else if($applicants->province	==	'5')
			{
				$this->inq->update_provinces('1129',$applicants->province);
			}
			else if($applicants->province	==	'6')
			{
				$this->inq->update_provinces('1130',$applicants->province);
			}
			else if($applicants->province	==	'7')
			{
				$this->inq->update_provinces('1131',$applicants->province);
			}
			else if($applicants->province	==	'8')
			{
				$this->inq->update_provinces('1132',$applicants->province);
			}
			else if($applicants->province	==	'9')
			{
				$this->inq->update_provinces('1133',$applicants->province);
			}
			else if($applicants->province	==	'10')
			{
				$this->inq->update_provinces('1134',$applicants->province);
			}
			else if($applicants->province	==	'11')
			{
				$this->inq->update_provinces('1135',$applicants->province);
			}
			/********************************************/
			if($applicants->wilaya	==	'12')
			{
				$update_destinations['wilaya']	=	'1136';
				$this->inq->update_wilaya('1136',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'13')
			{
				$update_destinations['wilaya']	=	'1137';
				$this->inq->update_wilaya('1137',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'14')
			{
				$update_destinations['wilaya']	=	'1138';
				$this->inq->update_wilaya('1138',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'15')
			{
				$update_destinations['wilaya']	=	'1139';
				$this->inq->update_wilaya('1139',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'16')
			{
				$update_destinations['wilaya']	=	'1140';
				$this->inq->update_wilaya('1140',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'17')
			{
				$update_destinations['wilaya']	=	'1141';
				$this->inq->update_wilaya('1141',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'30')
			{
				$update_destinations['wilaya']	=	'1142';
				$this->inq->update_wilaya('1142',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'31')
			{
				$update_destinations['wilaya']	=	'1143';
				$this->inq->update_wilaya('1143',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'32')
			{
				$update_destinations['wilaya']	=	'1144';
				$this->inq->update_wilaya('1144',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'33')
			{
				$update_destinations['wilaya']	=	'1145';
				$this->inq->update_wilaya('1145',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'34')
			{
				$update_destinations['wilaya']	=	'1146';
				$this->inq->update_wilaya('1146',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'35')
			{
				$update_destinations['wilaya']	=	'1147';
				$this->inq->update_wilaya('1147',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'36')
			{
				$update_destinations['wilaya']	=	'1148';
				$this->inq->update_wilaya('1148',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'37')
			{
				$update_destinations['wilaya']	=	'1149';
				$this->inq->update_wilaya('1149',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'39')
			{
				$update_destinations['wilaya']	=	'1150';
				$this->inq->update_wilaya('1150',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'40')
			{
				$update_destinations['wilaya']	=	'1151';
				$this->inq->update_wilaya('1151',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'41')
			{
				$update_destinations['wilaya']	=	'1152';
				$this->inq->update_wilaya('1152',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'42')
			{
				$update_destinations['wilaya']	=	'1153';
				$this->inq->update_wilaya('1153',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'43')
			{
				$update_destinations['wilaya']	=	'1154';
				$this->inq->update_wilaya('1154',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'44')
			{
				$update_destinations['wilaya']	=	'1155';
				$this->inq->update_wilaya('1155',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'45')
			{
				$update_destinations['wilaya']	=	'1156';
				$this->inq->update_wilaya('1156',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'46')
			{
				$update_destinations['wilaya']	=	'1157';
				$this->inq->update_wilaya('1157',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'47')
			{
				$update_destinations['wilaya']	=	'1158';
				$this->inq->update_wilaya('1158',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'48')
			{
				$update_destinations['wilaya']	=	'1159';
				$this->inq->update_wilaya('1159',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'49')
			{
				$update_destinations['wilaya']	=	'1160';
				$this->inq->update_wilaya('1160',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'50')
			{
				$update_destinations['wilaya']	=	'1161';
				$this->inq->update_wilaya('1161',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'51')
			{
				$update_destinations['wilaya']	=	'1162';
				$this->inq->update_wilaya('1162',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'52')
			{
				$update_destinations['wilaya']	=	'1163';
				$this->inq->update_wilaya('1163',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'53')
			{
				$update_destinations['wilaya']	=	'1164';
				$this->inq->update_wilaya('1164',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'54')
			{
				$update_destinations['wilaya']	=	'1165';
				$this->inq->update_wilaya('1165',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'55')
			{
				$update_destinations['wilaya']	=	'1166';
				$this->inq->update_wilaya('1166',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'56')
			{
				$update_destinations['wilaya']	=	'1167';
				$this->inq->update_wilaya('1167',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'57')
			{
				$update_destinations['wilaya']	=	'1168';
				$this->inq->update_wilaya('1168',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'58')
			{
				$update_destinations['wilaya']	=	'1169';
				$this->inq->update_wilaya('1169',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'59')
			{
				$update_destinations['wilaya']	=	'1170';
				$this->inq->update_wilaya('1170',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'60')
			{
				$update_destinations['wilaya']	=	'1171';
				$this->inq->update_wilaya('1171',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'61')
			{
				$update_destinations['wilaya']	=	'1172';
				$this->inq->update_wilaya('1172',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'62')
			{
				$update_destinations['wilaya']	=	'1173';
				$this->inq->update_wilaya('1173',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'63')
			{
				$update_destinations['wilaya']	=	'1174';
				$this->inq->update_wilaya('1174',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'64')
			{
				$update_destinations['wilaya']	=	'1175';
				$this->inq->update_wilaya('1175',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'65')
			{
				$update_destinations['wilaya']	=	'1176';
				$this->inq->update_wilaya('1176',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'66')
			{
				$update_destinations['wilaya']	=	'1177';
				$this->inq->update_wilaya('1177',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'68')
			{
				$update_destinations['wilaya']	=	'1178';
				$this->inq->update_wilaya('1178',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'69')
			{
				$update_destinations['wilaya']	=	'1179';
				$this->inq->update_wilaya('1179',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'70')
			{
				$update_destinations['wilaya']	=	'1180';
				$this->inq->update_wilaya('1180',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'71')
			{
				$update_destinations['wilaya']	=	'1181';
				$this->inq->update_wilaya('1181',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'72')
			{
				$update_destinations['wilaya']	=	'1182';
				$this->inq->update_wilaya('1182',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'73')
			{
				$update_destinations['wilaya']	=	'1183';
				$this->inq->update_wilaya('1183',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'74')
			{
				$update_destinations['wilaya']	=	'1184';
				$this->inq->update_wilaya('1184',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'75')
			{
				$update_destinations['wilaya']	=	'1185';
				$this->inq->update_wilaya('1185',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'18')
			{
				$update_destinations['1186']	=	'1186';
				$this->inq->update_wilaya('1135',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'20')
			{
				$update_destinations['wilaya']	=	'1187';
				$this->inq->update_wilaya('1187',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'21')
			{
				$update_destinations['wilaya']	=	'1188';
				$this->inq->update_wilaya('1188',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'22')
			{
				$update_destinations['wilaya']	=	'1189';
				$this->inq->update_wilaya('1189',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'23')
			{
				$update_destinations['wilaya']	=	'1190';
				$this->inq->update_wilaya('1190',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'24')
			{
				$update_destinations['wilaya']	=	'1191';
				$this->inq->update_wilaya('1191',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'25')
			{
				$update_destinations['wilaya']	=	'1192';
				$this->inq->update_wilaya('1192',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'26')
			{
				$update_destinations['wilaya']	=	'1193';
				$this->inq->update_wilaya('1193',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'27')
			{
				$update_destinations['wilaya']	=	'1194';
				$this->inq->update_wilaya('1194',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'28')
			{
				$update_destinations['wilaya']	=	'1190';
				$this->inq->update_wilaya('1190',$applicants->wilaya);
			}
			else if($applicants->wilaya	==	'29')
			{
				$update_destinations['wilaya']	=	'1196';
				$this->inq->update_wilaya('1196',$applicants->wilaya);
			}
		}
	}
//------------------------------------------------------------------------------
	/*
	* To Update Gender for all applicants
	* 
	*/
	public function update_gender()
	{
		$all_applicatns	=	$this->inq->get_inquiries_list(NULL,NULL);
		
		foreach($all_applicatns	as $applicants)
		{
			$arr = explode(' ',trim($applicants->fullname)); // Explode String
			
			if ($arr[1]	==	'بن')
			{
				$this->inq->update_gender('ذكر',$applicants->applicantid); // Update the Record
			}
			elseif($arr[1]	==	'بنت')
			{
				$this->inq->update_gender('أنثى',$applicants->applicantid); // Update the Record
			}
			else
			{
				$this->inq->update_gender('ذكر',$applicants->applicantid); // Update the Record
			}
		}
	}
//-------------------------------------------------------------------------------
	/*
	* Show all ICONS 
	*/	
    public function index()
    {
		$this->load->view('haya/allicons', $this->_data);
    }
	
//-------------------------------------------------------------------------------

	/*
	*
	*/
	function test_send_sms()
	{
		$numbers	=	array('96898824404');
		
		send_general_sms('سب بيل س',$numbers);
	}
//-------------------------------------------------------------------------------

	/*
	* @functon newrequest
	* @param $applicantid integer
	* @param $step integer
	*/
	public function newrequest($applicantid=NULL, $step=NULL)
	{
		$this->_data['a_id']	=	$tempid;
		$this->_data['a_step']	=	$step;
		
		// New Request Form View
		$this->load->view('newrequest', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Add Helps data into Database 
	*/	
	public function submit_tasgeel()
	{
		// GET POST DATA and inserted into DATABASE
		$this->inq->submitfor();
	}
//-------------------------------------------------------------------------------
 
	/*
	* Delete APPLICANT DOCUMENT
	*/
	public function delete_document()
	{
		$docid = $this->input->post('docid'); // GET docid from POST
		
		$this->inq->delete_applicant_document(); // DELETE Document
	}
//-------------------------------------------------------------------------------
 
	/*
	* Get Old Records
	*/	
	public function oldrecords()
	{
		$config	=	array();
		$page 	=	($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		if($this->input->get())
		{
			$config["uri_segment"] 	=	3;
			$config["base_url"] 	=	base_url() . "inquiries/oldrecords/".http_build_query($this->input->get());
		}
		else
		{
			$config["uri_segment"] 	=	3;
        	$config["base_url"] 	=	base_url() . "inquiries/oldrecords/";
		}
		
        $config["total_rows"] 	=	$this->inq->oco_person_count();
        $config["per_page"] 	=	200;
			
        $this->pagination->initialize($config);
		
		$this->_data['results'] =	$this->inq->oco_person($config["per_page"], $page);		
        $this->_data["links"] 	=	$this->pagination->create_links();
		
		$this->load->view("old-listing", $this->_data);
	}
//-------------------------------------------------------------------------------
 
	/*
	* Get Old Records
	*/	
	public function transaction_search_view()
	{
		$data	=	$this->input->post();
		
		if(!empty($data))
		{
			$this->_data['return_result']	=	$this->inq->get_transaction_Search_result($data);
			
			$this->load->view("transaction-search-view", $this->_data);
		}
		else
		{
			$this->load->view("transaction-search-view", $this->_data);
		}
	}
		
	public function oldviewdata($FORMNO, $FORMID)
	{
		$this->_data['oco_person'] = $this->inq->oco_data_detail($FORMNO,$FORMID,'P');		
		$this->_data['oco_financial'] = $this->inq->oco_data_detail($FORMNO,$FORMID,'F');		
		//$this->_data['oco_financial_detail'] = $this->inq->oco_data_detail($FORMNO,$FORMID,'FD');
		$this->_data['oco_house'] = $this->inq->oco_data_detail($FORMNO,$FORMID,'H');
		//$this->_data['oco_house_detail'] = $this->inq->oco_data_detail($FORMNO,$FORMID,'HD');
		$this->load->view("view_old_data", $this->_data);
	}
	
	public function old_missing_data()
	{
		$this->_data['reports'] = $this->inq->ocoReport();
		$this->load->view("report_old_data", $this->_data);
	}
	
	public function newformwitholddata($charity_type, $formid, $formnumber)
	{
       	$oldType = array('1'=>'79','2'=>'78','4'=>'81','5'=>'80');
		$oldName = array('1'=>'cashform','2'=>'housingform','4'=>'educationform','5'=>'medicalform');	
		$newName = array('79'=>'cashform','78'=>'housingform','81'=>'educationform','80'=>'medicalform');
		
		if($oldType[$charity_type]!='')
		{	$this->_data['charity_type_id'] 	=	$oldType[$charity_type];	}
		else
		{	$this->_data['charity_type_id'] 	=	$charity_type;	}
		
		if($oldType[$charity_type]!='')
		{	$this->_data['form_name'] 	=	$oldName[$charity_type];	}
		else
		{	$this->_data['form_name'] 	=	$newName[$charity_type];	}
		
		$this->_data['formid'] 				=	$formid;
		$this->_data['formnumber'] 			=	$formnumber;
		
		$this->_data['_applicant_data']['ah_applicant'] 	= 	$this->inq->oco_data_detail(0,$formid,'PD');
		$this->_data['_applicant_data']['ah_applicant']->extratelephone = json_encode(array($this->_data['_applicant_data']['ah_applicant']->extratelephone));
	
		$html = '';
		
		foreach($this->inq->oco_data_detail($formnumber,$formid,'F') as $fin)
		{
			//$fData = $this->inq->oco_person_data($fin->CREATEDBY);
			$html .= 'الرمز:'.check_null_oco($fin->FIN_CODE).' / المبلغ:'.check_null_oco($fin->FIN_AMOUNT).' / الفئة:'.check_null_oco($fin->FIN_CATEGORY).' / رقم الاجتماع:'.check_null_oco($fin->FIN_NOOFMEETING).' / <br><br>';
			$html .= $fin->FIN_NOTES.'<br>';
			$html .= '<hr>';			
		}

		foreach($this->inq->oco_data_detail($formnumber,$formid,'H') as $hou)
		{
			//$hData = $this->inq->oco_person_data($hou->CREATEDBY);
			$html .= 'التاريخ:'.show_date(check_null_oco($hou->HOU_DATE),9).' / المبلغ:'.check_null_oco($hou->HOU_AMOUNT).' / نوع وحجم العمل:'.check_null_oco($hou->HOU_SIZEOFWORK).' / الدفعة:'.check_null_oco($hou->HOU_BATCH).' / <br><br>';
			$html .= 'الجهة المقدمة:'.check_null_oco($hou->HOU_PROVIDER).'<br><br>';
			$html .= $hou->HOU_NOTES.'<br>';
			$html .= '<hr>';	
		}		
		
		$this->_data['notes'] = $html;

		$this->load->view('newrequestwitholddata', $this->_data);	// New Request Form
	}	
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function medicalform($applicantid=NULL, $refil=NULL, $step=NULL)
	{
        $this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['_applicantid']	=	$applicantid;			
		$this->_data['charity_type_id'] =	80;
		$this->_data['refil_data'] 		=	$refil;
		$this->_data['form_name'] 		=	'medicalform';
		$this->_data['_applicant_data'] =	$this->haya_model->getRequestInfo($applicantid);
		
		$this->load->view('newrequest', $this->_data);	// New Request Form
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function housingform($applicantid=NULL, $refil=NULL, $step=NULL)
	{
        $this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['_applicantid']	= $applicantid;		
		$this->_data['charity_type_id'] = 78;
		$this->_data['refil_data'] 		= $refil;
		$this->_data['form_name'] 		= 'housingform';
		
		$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($applicantid);
		
		//echo '<pre>';print_r($this->_data['_applicant_data']);
		//exit();
		
		$this->load->view('newrequest', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function cashform($applicantid=NULL, $refil=NULL, $step=NULL)
	{
        $this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['a_id']			= $applicantid;	
		$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($applicantid);
		$this->_data['charity_type_id'] = 79;
		$this->_data['refil_data'] 		= $refil;
		$this->_data['form_name'] 		= 'cashform';
	
				
		$this->load->view('newrequest', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function educationform($applicantid=NULL, $refil=NULL)
	{
        $this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['a_id']			= $applicantid;	
		$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($applicantid);		
		$this->_data['charity_type_id'] = 81;
		$this->_data['refil_data'] 		= $refil;
		$this->_data['form_name'] 		= 'educationform';
					
		$this->load->view('newrequest', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	function get_document_list()
	{
		$charity_type_id_value	=	$this->input->post('charity_type_id_value');
		$document_list			=	$this->inq->allRequiredDocument($charity_type_id_value);
		
		echo json_encode($document_list);
	}
//-------------------------------------------------------------------------------

	/*
	*
	*
	*/
	public function get_charge_in_one()
	{
		$this->_data['get_detail_charges']	=	$this->inq->get_all_detail();
	}
//-------------------------------------------------------------------------------
	/*
	* functon Get All Document List
	*/
	public function socilservaylist($branchid=0,$charity_type=0)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		
		$this->load->view('socialservay-listing', $this->_data);	
	}
//-------------------------------------------------------------------------------
	/*
	* functon Get All Document List
	*/
	public function api($branchid=0,$charity_type=0)
	{
		$result			=	$this->inq->get_socilservay_list($branchid,$charity_type);
		
		return json_encode($result);	
	}	
//-------------------------------------------------------------------------------
	/*
	* Function Get All Document List
	*/
	public function socilservaylist_completesteps($branchid=0,$charity_type=0)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		
		$this->load->view('socialservay-completesteps-listing', $this->_data);	
	}
	
	public function socilservaylistbystatus($branchid,$charity_type_id,$step)
	{
		$this->_data['branchid'] 		=	$branchid;
		$this->_data['charity_type'] 	=	$charity_type_id;
		$this->_data['step'] 			=	$step;
		
		$this->load->view("socialsurvay_list_status",$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get All Document List
	*/
	public function socilservaylist_by_types($branchid=0,$charity_type=0)
	{
		$branchid	=	$this->_data['user_detail']['profile']->branchid;
		
		$this->_data['moduleparent'] 	=	212;
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;		
		$this->_data["step"] 			=	1;
		$this->_data["done_step"]		=	2;
		$this->_data["method_step"] 	=	"socilservaylistbystatus";
		$this->_data['method_name']		=	'socilservaylist';
		$this->_data['complete_step']	=	'socilservaylist_completesteps';
		$this->_data['total_yateem']	=	$this->haya_model->get_yateem_counts();
		$this->_data['permission'] 		= 	json_decode($this->_data['user_detail']['profile']->permissionjson,true);
		
		$this->_data["bernamij_masadaat_types"]  = $this->inq->bernamij_almasadaat_types('1',$branchid); // Get all types for 1st step

		$this->load->view('socialservay-listing-by-types', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get All Document List
	*/
	public function socilservayresultfilledlist_by_types($branchid=0,$charity_type=0)
	{
		$branchid	=	$this->_data['user_detail']['profile']->branchid;
		
		$this->_data['moduleparent'] 	=	218;
		$this->_data['permission'] 		= 	json_decode($this->_data['user_detail']['profile']->permissionjson,true);

		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data["step"] 			=	2;
		$this->_data["done_step"]		=	3;
		/*$this->_data['method_step'] 	=	"social_servay_result_return";*/
		$this->_data['method_step'] 	=	"socilservaylistbystatus";
		$this->_data['method_reject'] 	=	"social_servay_result_rejected";
		$this->_data['method_name']		=	'socilservayresultfilledlist';
		$this->_data['complete_step']	=	'socilservayresultfilledlist_completesteps';
		
		$this->_data["bernamij_masadaat_types"]  = $this->inq->bernamij_almasadaat_types('2',$branchid);// Get all types for 2nd step
		
		$this->load->view('socialservay-listing-by-types', $this->_data);	// Load View Page
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function finaldecission($branchid=0,$charity_type=0,$islist	=	NULL)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		
		// Old Work
		//$this->load->view('finaldecission-listing', $this->_data); 
		
		// Updating New Changes from here
		if($islist	!=	'')
		{			
			foreach($this->inq->get_no_list() as $lc)
			{
				////////////////////////////////////////////reeja//////////////////////////
				$count	=	$this->inq->get_total($lc->listid,$charity_type,$step	=	3);
				
				//////////////////////////////////////////////////////////
				$actions =' <a  id="'.$lc->listid.'" href="'.base_url().'inquiries/finaldecission_no_list/'.$branchid.'/'.$charity_type.'/'.$lc->listid.'" > ( '.$count->total.' ) </a>&nbsp;&nbsp;';

				$arr[] = array(
					"DT_RowId"		=>	$lc->listid.'_durar_lm',
					"اسم القائمة" 	=>	$lc->listname,
					"عدد" 			=>	$actions,
					"تاريخ" 		=>	arabic_date($lc->listdate));
				
				unset($actions,$text);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
			exit();
		}
		
		$this->load->view('lagna-section-number-list',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function finaldecission_no_list($branchid=0,$charity_type=0,$listid	=	NULL)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['listid']			=	$listid;
		$this->_data['charity_type']	=	$charity_type;
		
		// Old Work
		$this->load->view('finaldecission-listing', $this->_data); 
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get All Document List
	*/
	public function finaldecission_by_types($branchid=0,$charity_type=0)
	{
		$branchid						=	$this->_data['user_detail']['profile']->branchid;
		$this->_data['moduleparent'] 	= 	224;
		$this->_data['permission'] 		= 	json_decode($this->_data['user_detail']['profile']->permissionjson,true);
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data['method_name']		=	'finaldecission';
		$this->_data["step"] 			=	3;
		$this->_data["done_step"]		=	4;
		$this->_data['complete_step']	=	'socilservayresultfilledlist_completesteps';
		
		$this->_data["bernamij_masadaat_types"]  =	$this->inq->bernamij_almasadaat_types('3',$branchid);
		
		$this->load->view('socialservay-listing-by-types', $this->_data);	
	}

//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socilservayresultfilledlist($branchid=0,$charity_type=0)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		
		$this->load->view('socialservay-listingspecial', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Compelted Steps
	*/	
	public function socilservayresultfilledlist_completesteps($branchid=0,$charity_type=0,$step)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data['step']			=	$step;
		
		$this->load->view('socialservay-listingspecial-completesteps', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function social_servay_result_return($charity_type=0)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['charity_type']	=	$charity_type;
			
		$this->load->view('socialservay-listingspecial-type', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function social_servay_result_rejected($charity_type=0)
	{
		//$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['charity_type']	=	$charity_type;
		
		$this->load->view('ssr_rejected', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get Information from ID Card
	*/

	public function getIDCardNumber()
	{	
		$term 				=	$this->input->get('term');
		$query = $this->db->query("SELECT ah_applicant.`applicantid`,ah_applicant.`charity_type_id`,ah_applicant.`date_of_birth`, ah_listmanagement.`list_name`,ah_applicant.`fullname`,ah_applicant.`idcard_number`,ah_branchs.`branchname`,province.`list_name` AS pro, wilaya.`list_name` AS wil FROM ah_applicant
									JOIN ah_listmanagement ON (ah_listmanagement.`list_id`=ah_applicant.`charity_type_id`)
									JOIN ah_listmanagement AS province ON (province.`list_id`=ah_applicant.`province`)
									JOIN ah_listmanagement AS wilaya ON (wilaya.`list_id`=ah_applicant.`wilaya`)
									JOIN ah_branchs ON (ah_applicant.`branchid`=ah_branchs.`branchid`)
									WHERE ah_applicant.`idcard_number` LIKE '%".$term."%' ORDER BY ah_applicant.`applicantid` ASC ");
		
		foreach($query->result() as $res) 
		{
			$refil_data_url = charity_edit_url($res->charity_type_id).$res->applicantid.'/refilData';
			$fullname = $res->fullname.' ('.$res->idcard_number.') فرع '.$res->branchname.' '.$res->pro.' ولاية '.$res->wil;
			$arr[] = array('id'=>$res->applicantid,'label'=>$fullname,'value'=>$res->idcard_number,'dob'=>$res->date_of_birth,'refil_data_url'=>$refil_data_url);
		}
		
		
		$oldQuery = $this->db->query("SELECT oco_person.`FORMID`,oco_person.`TYPEOFHELP`, oco_person.`FORMNO`, oco_person.`NAME`, oco_person.`LABOUR`, oco_regions.`REG_DESC`, oco_wilayats.`WIL_DESC` FROM oco_person
								JOIN oco_typeofassistance ON (oco_typeofassistance.`TA_ASSISTANCEID`=oco_person.`TYPEOFHELP`)
								JOIN oco_regions ON (oco_regions.`REG_ID`=oco_person.`REGION`)
								JOIN oco_wilayats ON (oco_wilayats.`WIL_ID`=oco_person.`STATE`)
								WHERE REPLACE(oco_person.`LABOUR`,'/','') LIKE '%".$term."%' AND oco_person.`ourstatus`='0' ORDER BY oco_person.`CREATEDDATE` DESC;");
		
		foreach($oldQuery->result() as $ores) 
		{
			$refil_data_url = base_url().'inquiries/newformwitholddata/'.$ores->TYPEOFHELP.'/'.$ores->FORMID.'/'.$ores->FORMNO;
			$fullname = $ores->NAME.' ('.$ores->LABOUR.') '.$ores->REG_DESC.' ولاية '.$ores->WIL_DESC.' (قديم)';
			$arr[] = array('id'=>$ores->FORMID,'label'=>$fullname,'value'=>$ores->LABOUR,'dob'=>date('Y-m-d'),'datatype'=>'old','refil_data_url'=>$refil_data_url);
		}
		
		echo json_encode($arr);
		// GET DATA according to ID CARD NUMMBER
		// echo $result		=	$this->inq->get_detail_by_param($select_fields,$condition,$charity_type_id,'CARD');
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get Information from ID Card
	*/

	public function get_mother_id_card_number()
	{	
		$term 				=	$this->input->get('term');
		$charity_type_id 	=	$this->input->get('charity_type_id');
		$select_fields		=	'applicantid,fullname,mother_name,mother_id_card,charity_type_id';
		$condition 			=	array('mother_id_card'	=>	$term);
		
		// GET DATA according to ID CARD NUMMBER
		echo $result		=	$this->inq->get_detail_by_param($select_fields,$condition,$charity_type_id,'MOTHER CARD');
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get Information from ID Card
	*/

	public function get_father_id_card_number()
	{	
		$term 				=	$this->input->get('term');
		$charity_type_id 	=	$this->input->get('charity_type_id');
		$select_fields		=	'applicantid,fullname,father_name,father_id_card,charity_type_id';
		$condition 			=	array('father_id_card'	=>	$term);
		
		// GET DATA according to ID CARD NUMMBER
		echo $result		=	$this->inq->get_detail_by_param($select_fields,$condition,$charity_type_id,'FATHER CARD');
	}	
//-------------------------------------------------------------------------------

	/*
	* functon Get Information from ID Card
	*/
	public function getPassportNumber()
	{
		$term 				=	$this->input->get('term');
		$charity_type_id 	=	$this->input->get('charity_type_id');
		$select_fields		=	'applicantid,fullname,passport_number,charity_type_id';
		$condition 			=	array('passport_number'	=>	$term);
		
		// GET DATA according to PASSPORT NUMMBER
		echo $result		=	$this->inq->get_detail_by_param($select_fields,$condition,$charity_type_id,'PASSPORT');
	}
//-------------------------------------------------------------------------------

	/*
	* functon Get Information from ID Card
	*/
	public function getPassportNumberWife()
	{
		$term = $this->input->get('term');
		$charity_type_id = $this->input->get('charity_type_id');
		
		echo $this->inq->get_passport_number_wife_info($term,$charity_type_id); // Only removes query from here an make function in Model.
	}			
//-------------------------------------------------------------------------------

	/*
	* functon Adding Social Servay Result
	*/	
	public function add_socilservayresult()
	{
		$sarvayid 					=	$this->input->post('sarvayid');
		$applicantid 				=	$this->input->post('applicantid');
		$userid 					=	$this->_login_userid;
		$health_condition 			=	$this->input->post('health_condition');
		$health_condition_text 		=	$this->input->post('health_condition_text');
		$housing_condition 			=	$this->input->post('housing_condition');
		$housing_condition_text 	=	$this->input->post('housing_condition_text');
		$numberofpeople 			=	$this->input->post('numberofpeople');
		$positioninfamily 			=	$this->input->post('positioninfamily');
		$economic_condition 		=	$this->input->post('economic_condition');
		$economic_condition_text 	=	$this->input->post('economic_condition_text');
		$casetype 					=	$this->input->post('casetype');
		$aps_name 					=	$this->input->post('aps_name');
		$aps_account 				=	$this->input->post('aps_account');
		$aps_filename 				=	$this->input->post('aps_filename');
		$aps_category 				=	$this->input->post('aps_category');
		$aps_category_text 			=	$this->input->post('aps_category_text');
		$aps_salaryamount 			=	$this->input->post('aps_salaryamount');
		$aps_date 					=	$this->input->post('aps_date');
		$first_time_date 			=	$this->input->post('first_time_date');
		$aps_another_income 		=	json_encode($this->input->post('aps_another_income'));
		$whyyouwant 				=	$this->input->post('whyyouwant');
		$summary 					=	$this->input->post('summary');
		$review 					=	$this->input->post('review');
		$ajel 						=	$this->input->post('ajel');
		$maintenance_type 			=	$this->input->post('maintenance_type');
		$report_visit 				=	$this->input->post('report_visit');
		$certificate_date 			=	$this->input->post('certificate_date');
		$proposal_section 			=	$this->input->post('proposal_section');
		$aps_month 					=	$this->input->post('aps_month');
		$aps_year 					=	$this->input->post('aps_year');
		$education_level 			=	$this->input->post('education_level');
		
		
		$ah_applicant_survayresult = array(
			'applicantid'				=>	$applicantid,
			'userid'					=>	$userid,
			'health_condition'			=>	$health_condition,
			'health_condition_text'		=>	$health_condition_text,
			'housing_condition'			=>	$housing_condition,
			'housing_condition_text'	=>	$housing_condition_text,
			'numberofpeople'			=>	$numberofpeople,
			'positioninfamily'			=>	$positioninfamily,
			'economic_condition'		=>	$economic_condition,
			'economic_condition_text'	=>	$economic_condition_text,
			'casetype'					=>	$casetype,
			'aps_name'					=>	$aps_name,
			'aps_account'				=>	$aps_account,
			'aps_filename'				=>	$aps_filename,
			'aps_category'				=>	$aps_category,
			'aps_category_text'			=>	$aps_category_text,
			'aps_salaryamount'			=>	$aps_salaryamount,
			'aps_date'					=>	$aps_date,
			'aps_another_income'		=>	$aps_another_income,
			'aps_month'					=>	$aps_month,
			'aps_year'					=>	$aps_year,
			'whyyouwant'				=>	$whyyouwant,
			'summary'					=>	$summary,
			'ajel'						=>	$ajel,
			'maintenance_type'			=>	$maintenance_type,
			'report_visit'				=>	$report_visit,
			'proposal_section'			=>	$proposal_section,
			'certificate_date'			=>	$certificate_date,
			'education_level'			=>	$education_level,
			'review'					=>	$review);
			
		$this->db->query("UPDATE `ah_applicant` SET `application_status`='' WHERE `applicantid`='".$applicantid."'");
		//$this->haya_model->save_steps($applicantid,3);
		
		$data	=	array(
		'bankid'		=>	$this->input->post('bankid'),
		'bankbranchid'	=>	$this->input->post('bankbranchid'),
		'banksource'	=>	$this->input->post('banksource'),
		'accountnumber'	=>	$this->input->post('accountnumber'),
		'swiftcode'		=>	$this->input->post('swiftcode'),
		'holder_name'	=>	$this->input->post('holder_name'));
		
		$this->inq->update_applicant_in_socialservay($applicantid,$data);
			
		if($sarvayid!='')
		{
			$this->inq->update_survay($applicantid,$ah_applicant_survayresult); // UPDATE SURVAY
			
			$this->haya_model->save_steps($applicantid,2);	// SAVE STEP
			$this->haya_model->update_steps($applicantid,2);
		}
		else
		{
			$this->haya_model->save_steps($applicantid,2);	// SAVE STEP
			$this->haya_model->update_steps($applicantid,2);
			$this->inq->add_survay($ah_applicant_survayresult); // ADD SURVAY
		}
		
		$query_type		=	$this->db->query("SELECT charity_type_id FROM `ah_applicant` WHERE `applicantid`='".$applicantid."'");
		$type			=	$query_type->charity_type_id;
		
		/*if($type)
		{
			redirect(base_url().'inquiries/socilservaylist/'.$this->_login_userid.'/'.$type);
			exit();
		}*/
		
		// Change this redirection
		/*redirect(base_url().'inquiries/socilservaylist_by_types/');
		exit();*/
		
		$this->load->library('user_agent');
		if ($this->agent->is_referral())
		{
			$url	=	 $this->agent->referrer();
		}
						
		$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
		redirect($url);
		exit();

	}
//-------------------------------------------------------------------------------

	/*
	* functon Checking Information
	*/	
	public function checkFurture()
	{
		$id 	=	$this->input->post('id');
		$type 	=	$this->input->post('type');
		
		switch($type)
		{
			case 'idcard_number';
				echo $this->inq->get_applicant_id($id); // RETURN applicationid 
			break;
			case 'passport_number';
				echo $this->inq->get_passport_number($id); // RETURN applicationid 
			break;
			case 'passport_number_wife';
				echo $this->inq->get_passport_number_wife($id); // RETURN applicationid 
			break;
		}
	}
//-------------------------------------------------------------------------------

	/*
	* functon Showing informtion according to the resule
	*/
	function checkbefore($id, $type)
	{		
		switch($type)
		{
			case 'idcard_number';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail', $this->_data);
			break;
			case 'passport_number';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail', $this->_data);
			break;
			case 'passport_number_wife';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail', $this->_data);
			break;			
		}
	}
//-------------------------------------------------------------------------------

	/*
	* functon Showing informtion according to the resule
	*/
	function checkbefore_print($id, $type)
	{		
		switch($type)
		{
			case 'idcard_number';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail-print', $this->_data);
			break;
			case 'passport_number';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail-print', $this->_data);
			break;
			case 'passport_number_wife';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail-print', $this->_data);
			break;	
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function send_sms_to_user()
	{
		$phone_numbers 	= implode(',',$this->input->post('phoneNumbers'));
		
		/*$dTime 			=  $this->input->post('date_time');
		if($dTime!='')
		{
			$dateTime = $dTime;	
		}
		else
		{
			$dateTime = date('Y-m-d');	
		}
		
		$id 	=	$this->input->post('hd_id');
		$msx 	=	$this->input->post('expiry_msg');	
		
		//$replace	=	array(arabic_date(applicant_number($id)),arabic_date($dateTime));
		//$message	=	str_replace(sms_lagend(),$replace,$msx);
		$sms_time	=	$this->input->post('sms_time');
		$type 		=	$this->input->post('type');*/
		$message	=	$this->input->post('expiry_msg');
		
		
		
		$return	=	send_general_sms($message,$phone_numbers); // Send SMS
		
		/*$numbers_arr = explode(',',$phone_numbers);
		if(!empty($numbers_arr)){
				foreach($numbers_arr as $new){

					$myData['sms_receiver_id'] = $id;
					$myData['sms_sender_id'] = $this->session->userdata('userid');
					$myData['sms_receiver_number'] = $new;
					$myData['sms_module_id'] = $sms_module_id;
					$myData['sms_sent_date'] = $dateTime;
					$myData['sms_message'] = $message;
					$myData['sms_sent_type_time'] = 'Now';
					$myData['type'] = 'sms';
					$myData['sms_list'] = $sms_list;
					$newData[] = $myData;
				}
		}*/
				
		//$this->db->insert_batch('ah_sms_history',$newData);
		
		if($return)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function bulksms()
	{
		$this->load->view('bulksms',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function smsmodal($id,$type)
	{
		$response	=	$this->inq->get_extratelephone($id); // GET EXETRA telephone number
		
		$this->_data['phone_numbers']	=	json_decode($response);
		$this->_data['applicantid']		=	$id;
		$this->_data['type'] 			=	$type;
		$this->_data['template'] 		=	$this->inq->get_template(); // GET template
		
		$this->load->view('smsmodal',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* SMS Template
	*/	
	public function smstemplate()
	{
		$this->haya_model->check_permission($this->_data['module'],'v'); //Checking View Permission
		$this->load->view('smstemplate', $this->_data); //Loading Template View
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function addupdate_sms_template()
	{
		$templateid = $this->input->post('template_id');
		
		if($this->input->post('from_form')	==	'1')
		{			
			$data = array(
				'addedby'			=>	$this->session->userdata('userid'),
				'templatesubject'	=>	$this->input->post('templatesubject'),
				'template'			=>	$this->input->post('template'));
			
			if($templateid!='')
			{
				$this->inq->update_sms_template($templateid,$data);	// Update SMS Template
			}
			else
			{
				$this->inq->add_sms_template($data); // Add SMS Template
			}
		}

		$this->_data['sms'] = $this->inq->get_all_sms_templates($templateid); // GET all SMS templates
		
		$this->load->view("addupdate_sms_template",$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function templatedelete($tempid)
	{
		if($tempid!='') { $this->db->query("DELETE FROM system_sms_template WHERE templateid='".$tempid."' "); }
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function smstemplate_ajax()
	{
		$permissions	=	$this->haya_model->check_other_permission(array('99'));
		
		$conut	=	0;
		
		$result	=	$this->inq->get_templates();	// GET SMS Templates
				
		foreach($result as $smsresult)
		{
			if($permissions[99]['u']	==	1)
			{
				$actions .='<a href="#1" id="'.$smsresult->templateid.'" onClick="add_update_sms_template(this);"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
            
			if($permissions[99]['d']	==	1)
			{
				$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$smsresult->templateid.'" data-url="'.base_url().'inquiries/templatedelete/'.$smsresult->templateid.'"><i style="color:#F00;" class="icon-remove-circle"></i></a>';
			}
                
			$arr[] = array(
				"DT_RowId"		=>	$smsresult->templateid.'_durar_lm',				
				"عنوان الرسئل" 	=>	$smsresult->templatesubject,              
				"تاريخ" 		=>	arabic_date($smsresult->tempatedate),
				"عدد الرسالة" 	=>	arabic_date(strlen($smsresult->template)),
				"الإجراءات" 		=>	$actions
				);
				unset($actions);
		}
		
		echo json_encode($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function inquiries_filter($userid='',$branchid='')
	{
		//check_permission($this->_data['module'],'v');		
		$branchid = $_GET['branch_code'];		
        //$this->_data['inquiries'] = $this->inq->getmaindata($branchid);
		$this->_data['branchid'] = $branchid;
        $this->_data['blist'] = $this->inq->muragain_branch_count();
		
        $this->load->view('inquiries_list', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function systempeople()
	{
		 check_permission($this->_data['module'],'v');
		 $this->load->view('systempeople', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function allappointments()
	{
		$this->load->view('allappointments', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function testevent()
	{
		send_sms_steps(1,'96898818663,96892463374,96897890223,96898824404,96892324717',35);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function findkey()
	{
		$keyvalue = (int)$this->input->post('keyvalue');
		$keytype = (int)$this->input->post('keytype');
		
		switch($keytype)
		{
			case '4';				
				$this->db->select('tempid');
				$this->db->from('main');		
				$this->db->where('tempid',$keyvalue);
				$query = $this->db->get();
				if($query->num_rows() > 0)
				{	$ar['url'] = base_url().'inquiries/newinquery/'.$keyvalue;	}
				else
				{	$ar['error'] = 'لا تتوافر بيانات';	}
				echo json_encode($ar);
			break;
			case '17';				
				$this->db->select('applicant_id');
				$this->db->from('applicants');		
				$this->db->where('applicant_id',$keyvalue);
				$query = $this->db->get();
				if($query->num_rows() > 0)
				{	$ar['url'] = base_url().'inquiries/newrequest/'.$keyvalue;	}
				else
				{	$ar['error'] = 'لا تتوافر بيانات';	}
				echo json_encode($ar);
			break;
		}
	}
	
//-------------------------------------------------------------------------------


// 29/12/2014	-------------------------------------	START
	

	public function userhistory()
	{
        check_permission($this->_data['module'],'v');
		$this->load->view('userhistory', $this->_data);
	}
	
// 29/12/2014	-------------------------------------	END
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function applicantsList(){
        //check_permission($this->_data['module'],'v');
        $this->_data['all_applicatns']	=	$this->inq->getAprovalStepData();
		$this->load->view('banklist', $this->_data);
	
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function getsavedocument()
	{
		echo json_encode($this->inq->getsave_document());
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function get_sms_history($module_id,$userid)
	{
		$this->_data['module_id']	=	$module_id;
		$this->_data['userid']		=	$userid;
		
		$this->_data['sms_history']	=	$this->inq->get_sms_history($module_id,$userid);
		
		$this->load->view('history-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function list_sms_history($module_id,$userid)
	{
		$result	=	$this->inq->get_sms_history_list($module_id,$userid); // GET all SMS history List
		
		foreach($result as $lc)
		{
			if($module_id	==	'1')
			{
               $name	=	   $lc->first_name.' '.$lc->middle_name.' '.$lc->last_name.' '.$history->family_name;
			}
			else
			{
               $name	=   $lc->applicant_first_name.' '.$lc->applicant_middle_name.' '.$lc->applicant_last_name.' '.$lc->applicant_sur_name ;
			}
                  
			 $arr[] = array(
			"DT_RowId"		=>	$lc->sms_id.'_durar_lm',
			"اسم المتلقي" 	=>	$name,              
			"رقم المتلقي" 	=>	$lc->sms_receiver_number,
			"رسالة" 		=>	$lc->sms_message,
			"اسم المرسل" 	=>	$lc->firstname.' '.$lc->lastname,
			"التاريخ" 		=>	date("Y-m-d",strtotime($lc->sms_sent_date))
			);
		}
		
		echo json_encode($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function bank_users()
	{
        check_permission($this->_data['module'],'v');
		$this->_data['all_users']	=	$this->inq->get_all_banks();
		
		// Load Users Listing Page
		$this->load->view('bank-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function get_calc_data($applicantid	=	NULL)
	{
		$postData = $this->input->post();
		
		$this->_data['post_data']	= $postData;
		echo $HTML	=	$this->load->view('ajax-calc-html', $this->_data,TRUE);
	}

//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function newcompany($tempid='')
	{
        check_permission($this->_data['module'],'v');
		$this->_data['user_info'] = userinfo();
		if($tempid!='')
		{
			$this->_data['m'] = $this->inq->getLastDetail($tempid);
			$this->_data['t'] = 'review';
		}
		else
		{
			$this->_data['m'] = $this->inq->new_inquery();
		}
		
		$this->_data['page'] = 'addinq';
		
		$this->load->view('newinquery', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function requestProjectIlust(){
        check_permission($this->_data['module'],'v');
		$this->load->view('requestprojectIllus_view', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function loanEvolution(){
        check_permission($this->_data['module'],'v');
		$this->load->view('request_loan_view', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function user_applicants($user_id)
	{
    	//check_permission($this->_data['module'],'v');	
		$this->_data['user_id'] = $user_id;	 
		$this->load->view('applicant-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function socilservayresult($applicantid='',$temp2='',$temp3='')
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
			
		$this->_data['_applicantid']		=	$applicantid;			
		$this->_data['_applicant_data']		=	$this->haya_model->getRequestInfo($applicantid);
		
		$this->load->view('requestphasefive', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socialnoneditable($applicantid='',$type	=	NULL,$temp2='',$temp3='')
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
			
		$this->_data['_applicantid']	=	$applicantid;			
		$this->_data['_applicant_data'] =	$this->haya_model->getRequestInfo($applicantid);	
		$this->_data['current_status'] 	=	$this->inq->getLast_decission($applicantid,2);
		$this->_data['all_status'] 		=	$this->inq->get_all_decission($applicantid,2);
		$this->_data['type']			=	$type;
		
		$this->load->view('requestphasefivenoneditable', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function add_decission()
	{
		$applicantid 		= $this->input->post('applicantid');
		$application_status = $this->input->post('application_status');
		$steptype 			= $this->input->post('steptype');
		
		
		if($application_status)
		{
			if($applicantid!='')
			{
				if($application_status=='إعادة نظر')
				{	
					$app_status 			= '1';	
					$ah_applicant['isOld'] 	= 1;
				}
				else if($application_status	==	'إعتذار')
				{
					$application_status	=	'رفض';	
				
					$app_status = '2';
				}
				else
				{
					$app_status = '3';
				}
				
				$ah_applicant['application_status'] = $application_status;
				$ah_applicant['applicant_notes'] 	= $this->input->post('notes');
				
				$this->inq->update_decission($applicantid,$ah_applicant); // Update Record
				
				//Adding data into dicission table
				$ah_applicant_decission['applicantid'] 	=	$applicantid;
				$ah_applicant_decission['userid'] 		=	$this->_login_userid;
				$ah_applicant_decission['decission'] 	=	$application_status;
				$ah_applicant_decission['step'] 		=	2;
				$ah_applicant_decission['notes'] 		=	$this->input->post('notes');
				$ah_applicant_decission['decissionip'] 	=	$_SERVER['REMOTE_ADDR'];
				$ah_applicant_decission['steptype'] 	=	$steptype;
				
				$this->haya_model->update_steps($applicantid,$app_status); 	//	Update Record
				$this->inq->add_decission($ah_applicant_decission); 		//	Add Record				
				$this->haya_model->save_steps($applicantid,$app_status);	//	SAVE Steps
				
				$get_branch_type	=	$this->inq->get_branch_type($applicantid); // Get Brnch ID and Charity Type ID
				
				/*redirect(base_url().'inquiries/socilservayresultfilledlist/'.$get_branch_type->branchid.'/'.$get_branch_type->charity_type_id);
				exit();*/
				
				$this->load->library('user_agent');
				if ($this->agent->is_referral())
				{
					$url	=	 $this->agent->referrer();
				}
								
				$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
				redirect($url);
				exit();
			
			}			
		}
	}
	
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function requestPhase()
	{
        check_permission($this->_data['module'],'v');
		$this->load->view('requestphasefive');
	}
	
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function uploadDocument()
	{
		$postData 			=	$this->input->post();
		$fileSesionName 	=	$this->session->userdata('fileSesionName');
		$fileSesionName;
		$session 			= 	explode('_',$fileSesionName);
		$f					=	$session[2];
		
		/*$ext 				=	pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
		$name 				= 	generateCode();
		$newname  			=  	$name.'.'.$ext;
		
		$targetPath = 	"./upload_files/documents/".$newname;
		
		if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetPath))
		{
			echo json_encode(array('status' => 'ok','name' => $newname,'type' => $f));
		}
		else
		{
			echo json_encode(array('status' => 'error'));
		}*/
		
		/*****************************************************/		
		// Muzaffar Updated this Function
		// Date : 7-Feb-2016
		/*---------------------------------------------------*/
		
		$config['upload_path'] 		=	'./upload_files/documents/';
		$config['allowed_types'] 	=	'gif|jpg|png';
		$config['max_size']			=	'500000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode(array('status' => 'error'));
		}
		else
		{
			$image_data		= 	$this->upload->data();
			$file_name 		= 	$image_data['file_name'];
			
			echo json_encode(array('status' => 'ok','name' => $file_name ,'type' => $f));
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function attachment($id)
	{
		$this->_data['mustarik']	=	$id;
		$this->load->view('view_attachment', $this->_data);
	}
//-------------------------------------------------------------------------------	    
	/*
	* update info of applicant comitte_decision
	* @param int $applicant_id
	* Created by M.Ahmed
	*/
	public function generateRandomString($length = 10) 
	{
		$characters 		=	'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength 	=	strlen($characters);
		$randomString 		=	'';
		
		for ($i = 0; $i < $length; $i++) 
		{
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		
		return $randomString;
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
    public function findExtension($filename)
	{
	   $filename 	=	strtolower($filename) ;
	   $exts 		=	explode(".", $filename) ;
	   $n 			=	count($exts)-1;
	   $exts 		=	$exts[$n];
	   
	   return $exts;
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function uploadFile(){
		
		if (!file_exists(FCPATH.'upload_files/documents/')) 
		{
			mkdir(FCPATH.'upload_files/documents/', 0777, true);
		}
		
		/*$file 	=	$_FILES['file']['name'];
		$name 		=	$this->generateRandomString(5);
		$ext 		=	$this->findExtension($file);
		$filename 	=	$name.".".$ext;
		
		if(move_uploaded_file($_FILES['file']['tmp_name'], FCPATH.'upload_files/documents/'.$filename))
		{
			$image	=	'<i class="delete-icon icon-remove-sign doc8remove0" style="color:#CC0000;cursor:pointer" onclick="deleteDoc(this,\''.$filename.'\');"></i>';
			echo json_encode(array('status' => 'ok','filename'=>$filename,'delicon'=>$image));
		}
		else
		{
			echo json_encode(array('status' => 'error'));
		}*/
		
		// Muzaffar Updated this Function
		// Date : 7-Feb-2016
		/*---------------------------------------------------*/
		$config['upload_path'] = './upload_files/documents/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '500000';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode(array('status' => 'error'));
		}
		else
		{
			$image_data		= 	$this->upload->data();
			$file_name 		= 	$image_data['file_name'];
			
			$image	=	'<i class="delete-icon icon-remove-sign doc8remove0" style="color:#CC0000;cursor:pointer" onclick="deleteDoc(this,\''.$file_name.'\');"></i>';
			echo json_encode(array('status' => 'ok','filename'=>$file_name,'delicon'=>$image));
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	function uploadGuranteeAttachment()
    {
		if (!file_exists(FCPATH.'upload_files/documents/')) 
		{
			mkdir(FCPATH.'upload_files/banners/', 0777, true);
		}
		
		//---------------------------------------------------
		$config['upload_path'] = './upload_files/documents/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '500000';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode(array('status' => $error,'errorfilename'=>$image_name));
		}
		else
		{
			$image_data		= 	$this->upload->data();
			$image_name 	= 	$image_data['file_name'];
			echo json_encode(array(
			'status'	=>	'ok',
			'filename'	=>	$image_name)
			);
		}
	}
	
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function uploadBanner(){
		
		if (!file_exists(FCPATH.'upload_files/banners/')) 
		{
			mkdir(FCPATH.'upload_files/banners/', 0777, true);
		}
		
		//---------------------------------------------------
		$config['upload_path'] = './upload_files/banners/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '500000';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo json_encode(array('status' => $error,'errorfilename'=>$image_name));
		}
		else
		{
			$image_data		= 	$this->upload->data();
			$image_name 	= 	$image_data['file_name'];
			
			$path			=	base_url().'upload_files/banners/'.$image_name;
			
			$image			=	'<i class="icon-remove-sign doc8remove0" style="color:#CC0000;cursor:pointer" onclick="deleteFile(\''.$image_name.'\');"></i>';
			$image		   .= 	'<img src="'.$path.'" width="400"/>';
			
			echo json_encode(array(
				'status'	=>	'ok',
				'filename'	=>	$image_name,
				'image'		=>	$image,
				'dataaaa'	=>	$id)
				);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function add_banner_detail()
	{		
		if($_POST['imageid'])
		{
			if($_POST['attachmentbanner'])
			{
				$image_name	=	$_POST['attachmentbanner'];
				
				if(file_exists(rootpath.'upload_files/banners/'.$_POST['db_banner_image']))
				{
					unlink(rootpath.'upload_files/banners/'.$_POST['db_banner_image']);
				}
			}
			else
			{
				$image_name	=	$_POST['db_banner_image'];
			}
			
			$update_data	=	array(
				'userid'		=>	$_POST['userid'],
				'branch_id'		=>	$_POST['branch_id'],
				'image_title'	=>	$_POST['image_title'],
				'start_date'	=>	$_POST['start_date'],
				'end_date'		=>	$_POST['end_date'],
				'image_order'	=>	$_POST['image_order'],
				'banner_image'	=>	$image_name	
				);
		
			$this->haya_model->update_banner($_POST['imageid'],$update_data);
		}
		else
		{
			$add_data	=	array(
			'userid'		=>	$_POST['userid'],
			'branch_id'		=>	$_POST['branch_id'],
			'image_title'	=>	$_POST['image_title'],
			'start_date'	=>	$_POST['start_date'],
			'end_date'		=>	$_POST['end_date'],
			'image_order'	=>	$_POST['image_order'],
			'banner_image'	=>	$_POST['attachmentbanner']
			);
			
			$this->haya_model->add_banner($add_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function update_status($status,$image_id)
	{	
		if($status	==	'0')
		{
			$data	=	array('isactive'	=>	'1');
		}
		else
		{
			$data	=	array('isactive'	=>	'0');
		}
			
		$this->haya_model->update_status($image_id,$data);
		
		redirect(base_url().'inquiries/bannerslisting');
		exit();
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function bannerslisting()
	{
		$this->load->view('banners-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function requestphasesix(){
        check_permission($this->_data['module'],'v');
		$this->load->view('requesphasesix', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function listingprelimanryAproval(){
        check_permission($this->_data['module'],'v');
		$this->load->view('listingaproval_view', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function listingaproval(){

		$this->load->view('listingaprovali_view', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function resetinq()
	{
		$this->inq->reset_inquery();
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function getInquirysms($type = 'sms')
	{

		$this->_data['type']	=	$type;
		
		$inqSms = $this->inq->getInquiriesSms($type);
		
		// For Saving
		if($this->input->post('submit'))
		{
			//-----------------------------------------------------------------
			$form_data		=	$this->input->post();
			unset($form_data['submit']);
			
			$firstData	=	array();
			$count		=	count($this->inq->getInquiriesSms($type));
			
			for($i=1;$i<=$count;$i++)
			{
				$sms_reminder	=	'sms_reminder_type_'.$i;
				$reminder_count	=	'reminder_count_'.$i;
				$sms_value		=	'sms_value_'.$i;
				$sms_id			=	'sms_id_'.$i;
				$sms_heading	=	'sms_heading_'.$i;
				
				$firstData['sms_heading']			= $form_data[$sms_heading];
				$firstData['sms_remider'] 			= $form_data[$sms_reminder];
				$firstData['sms_reminder_counter'] 	= $form_data[$reminder_count];
				$firstData['sms_value'] 			= $form_data[$sms_value];
				$firstData['type'] 					= $form_data['type'];
				
				$condition = array('sms_id'=>$form_data[$sms_id]);
				
				
				$return	=	$this->inq->update_db('sms_management',$firstData,$form_data[$sms_id]);

			}
			if($return)
			{
				$this->session->set_flashdata('msg', 'S');	
			}
			else
			{
				$this->session->set_flashdata('msg', 'E');	
			}
			
			// UNSET ARRAY key
			$this->_data['page'] = 'addinqsms';
			redirect('inquiries/getInquirysms/'.$type);
		}
		
		$this->_data['inq_info_sms']	=	$inqSms;
		
		// View Inq Type Messages		
		$this->load->view('inqType', $this->_data);
	
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function sendms()
	{
		$numbers = array('96898824404','96893338241');
		echo send_sms('1',$numbers,'mesage');
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function add($listid	= NULL)
	{
		if($listid)
		{
			$this->_data['single_list']	=	$this->listing->get_single_record($listid);	
		}
		
		if($this->input->post())
		{
			$data =	$this->input->post();
			// UNSET ARRAY key
			unset($data['submit']);
			if($this->input->post('list_id'))
			{
				$this->listing->update_list($this->input->post('list_id'),$data);
				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				}
				else
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
					redirect(base_url()."inquiries/listing");
					exit();
				}
				
			}
			else
			{

				$this->listing->add_list($data);
				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				}
				else
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
					redirect(base_url()."inquiries/listing");
					exit();
				}

			}
		}
		else
		{
			if($listid)
			{
				$this->_data['list_id']	=	$listid;
			}
			else
			{
				$this->_data['list_id']	=	'';
			}
			
            check_permission($this->_data['module'],'v');
			$this->load->view('inquiries/add', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function addrules1($listid	= NULL)
	{
		if($listid)
		{
			$this->_data['single_list']	=	$this->listing->get_single_record($listid);	
		}
		
		if($this->input->post())
		{
			$data		=	$this->input->post();
			
			// UNSET ARRAY key
			unset($data['submit']);
			
			if($this->input->post('list_id'))
			{
				$this->listing->update_list($this->input->post('list_id'),$data);
				
				$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				redirect(base_url()."inquiries/listing");
				exit();
			}
			else
			{
				$this->listing->add_list($data);
				
				$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				redirect(base_url()."inquiries/listing");
				exit();
			}
		}
		else
		{
			if($listid)
			{
				$this->_data['list_id']	=	$listid;
			}
			else
			{
				$this->_data['list_id']	=	'';
			}
            check_permission($this->_data['module'],'v');
			$this->load->view('inquiries/rules-listing', $this->_data);
		}
		
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function listing($type	=	NULL)
	{
		if($type)
		{
			$this->_data['type']	=	$type;

			// Get List Name By Type
			$this->_data['listing']	=	$this->listing->get_all_by_type($type);

			$this->_data['list_type_name']	=	$type;

			$this->load->view('type-listing', $this->_data);
		}
		else
		{
			$this->_data['marital_count']	=	$this->listing->total_count('maritalstatus');
			$this->_data['situation_count']	=	$this->listing->total_count('current_situation');
			$this->_data['inquiry_type']	=	$this->listing->total_count('inquiry_type');

			$this->load->view('listing', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function types_listing($type	=	NULL)
	{
        check_permission($this->_data['module'],'v');
		if($type)
		{
			// Get List Name By Type
			$this->_data['listing']	=	$this->listing->by_type($type);
			
			$this->_data['list_type_name']	=	$type;
			
			$this->load->view('types-listing', $this->_data);
		}
		else
		{
			$this->load->view('typeslisting', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function tadeel_qaima()
	{
		$segment = $this->uri->segment(3);
		
		foreach($this->listing->get_list_type() as $listdata) 
		{
			$typename = list_types($listdata->list_type);
			
			$action =' <a href="'.base_url().'inquiries/listing/'.$listdata->list_type.'">عرض الكل</a> '; 
			
			$action .= ' <button onClick="gototype(this);" type="button" data-url="'.base_url().'inquiries/listing/'.$listdata->list_type.'" class="btn btn-success">'.$this->listing->total_count($listdata->list_type).'</button>';
			
			
			$arr[] = array(
				"DT_RowId"		=>	$listdata->list_id.'_durar_lm',
                "‫نوع القائمة" 	=>	$typename['ar'],
				"إجمالي عدد" 	=>	$action);
				unset($actions);
		}
		
		echo json_encode($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function list_tadeel_qaima($type)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('6'));
		$result			=	$this->inq->get_list_taqareer_qaima($type);
		
		$checkbox	=	'---';
		
		foreach($result as $lc)
		{
			if($permissions[6]['a']	==	1)
			{
				if($lc->list_type != 'inquiry_type' AND $lc->list_type != 'rules' AND $lc->list_type != 'qualification' AND $lc->list_type != 'business_type' AND $lc->list_type != 'activity_project' AND $lc->list_type != 'project_employment' AND $lc->list_type != 'project_type')
				{
					if($lc->other)
					{
						$checked	=	'checked="checked"';
					}
					else
					{
						$checked	=	'';
					}
					
					$checkbox ='<div class="other1"> <input type="checkbox" onClick="other(this);" id="'.$lc->list_id.'" name="other'.$lc->list_id.'" '.$checked.'>
	
				  <div id="show'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;">&#10004;</div>
	
				  <div id="hide'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;"><i style="color:#CC0000;" class="icon-remove-sign"></i></div>
	
				  </div>';
				}
			}

			if($permissions[6]['a']	==	1)
			{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inquiries/add_child_qaima/'.$lc->list_id.'/" id="'.$lc->list_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> ';	}
							
			if($permissions[6]['u']	==	1)
			{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inquiries/add_data_for_qaima/'.$lc->list_id.'/parent" id="'.$lc->list_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';	}
			
			if($permissions[6]['d']	==	1)
			{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->list_id.'" data-url="'.base_url().'inquiries/delete/'.$lc->list_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
			
			
			$count = $this->tadeel_count($lc->list_id);
			$actions .=' <a href="'.base_url().'inquiries/child_listing/'.$lc->list_id.'">عرض الكل ('.$count.')</a>';
			 
			$arr[] = array(
				"DT_RowId"		=>	$lc->list_id.'_durar_lm',
                "قائمة البرامج" =>	$lc->list_name,              
				"أخرى" 			=>	$checkbox,
				"الإجراءات" 		=>	$actions);
				unset($actions);
		}
		
		echo json_encode($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
    public function tadeel_count($id)
	{	
		return $result	=	$this->inq->get_tadeel_count($id); // Get Total Count From Tadeel
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	    
	public function add_child_qaima($parentid)
	{
		
		$this->_data['type'] = 'child';
		$this->_data['parent_id'] = $parentid;
		
		$this->load->view('dialog/add-child-dialog',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function sub_child_listing($listid	=	NULL)
	{
		$this->_data['parent_id']	=	$listid;

		$this->_data['listing']	=	$this->listing->get_subchild_listing($listid);

		$this->load->view('subchilds-type-listing', $this->_data);

	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function add_data_for_qaima($parentid)
	{
		if($parentid!='' && $parentid!=0)
		{
			$this->_data['data'] = $this->listing->get_list_data($parentid);

		}
		
		$this->_data['type'] = 'parent';
		
		$this->load->view('dialog/add-child-dialog',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function rule_qualification_listing($type	=	NULL)
	{
		if($type)
		{
			$this->_data['type']	=	$type;

			// Get List Name By Type
			$this->_data['listing']	=	$this->listing->rules_qualification_by_type($type);

			$this->_data['list_type_name']	=	$type;

			$this->load->view('rule-qualification-type-listing', $this->_data);
		}
		else
		{
			$this->_data['marital_count']	=	$this->listing->total_count('maritalstatus');
			$this->_data['situation_count']	=	$this->listing->total_count('current_situation');
			$this->_data['inquiry_type']	=	$this->listing->total_count('inquiry_type');

			$this->load->view('rule-qualification-listing', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function taqareer_qaima()
	{
		$segment = $this->uri->segment(3);

		foreach($this->listing->get_list_type_rule_qualification() as $listdata) 
		{
			$typename = list_types($listdata->list_type);
			
			$action =' <a href="'.base_url().'inquiries/rule_qualification_listing/'.$listdata->list_type.'">عرض الكل</a> '; 
			
			$action .= ' <button onClick="gototype(this);" type="button" data-url="'.base_url().'inquiries/rule_qualification_listing/'.$listdata->list_type.'" class="btn btn-success">'.$this->listing->total_count($listdata->list_type).'</button>';
			
			$arr[] = array(
				"DT_RowId"=>$listdata->list_id.'_durar_lm',
                "‫نوع القائمة" =>$typename['ar'],
				/*"عرض الكل" =>$listdata->list_type,*/
				"إجمالي عدد" =>$action);
				unset($actions);
		}
		echo json_encode($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function list_taqareer_qaima($type)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('37'));
		$result			=	$this->inq->get_list_taqareer_qaima($type);
		
		$checkbox		=	'---';
		
		foreach($result	 as $lc)
		{
			if($permissions[37]['a']	==	1)
			{
				if($lc->list_type != 'inquiry_type' AND $lc->list_type != 'rules' AND $lc->list_type != 'business_type' AND $lc->list_type != 'activity_project' AND $lc->list_type != 'project_employment' AND $lc->list_type != 'project_type')
				{
					if($lc->other)
					{
						$checked	=	'checked="checked"';
					}
					else
					{
						$checked	=	'';
					}
				
					$checkbox ='<div class="other1"> <input type="checkbox" onClick="other(this);" id="'.$lc->list_id.'" name="other'.$lc->list_id.'" '.$checked.'>

				  <div id="show'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;">&#10004;</div>
	
				  <div id="hide'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;"><i style="color:#CC0000;" class="icon-remove-sign"></i></div>
	
				  </div>';
				}

			}
			
/*				if(check_other_permission(37,'a')==1)
			{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inquiries/add_child_qaima/'.$lc->list_id.'/" id="'.$lc->list_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> ';	}*/
							
			if($permissions[37]['u']	==	1)
			{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inquiries/add_data_for_qaima/'.$lc->list_id.'/parent" id="'.$lc->list_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';	}
			
			if($permissions[37]['d']	==	1)
			{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->list_id.'" data-url="'.base_url().'inquiries/delete/'.$lc->list_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
			
			if(function_exists('tadeel_count'))
			{
				$count = $this->tadeel_count($lc->list_id);
			}
			else
			{
				$count	=	'0';
			}
			
			 /*$actions .=' <a href="'.base_url().'inquiries/child_listing/'.$lc->list_id.'">عرض الكل ('.$count.')</a>'; */
			 $arr[] = array(
			"DT_RowId"=>$lc->list_id.'_durar_lm',
			"قائمة البرامج" =>$lc->list_name,              
			"أخرى" =>$checkbox,
			"الإجراءات" =>$actions);
			unset($actions);
		}
		
		$ex['data'] = $arr;
		//print_r($arr);
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function add_parent_taqareer($type	=	NULL)
	{
		if($type)
		{
			$this->_data['type'] = $this->listing->get_type_id($type);
			
			//$this->_data['single_list']	=	$this->listing->get_single_record($listid);	
		}
		else
		{
			//$this->_data['parent_id']	=	NULL;
		}
		
		$this->load->view('add-parent-taqareer',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function getList($type = 'sms')
	{
		$this->_data['type']		=	$type;
		$this->_data['module_id']	=	1;

		$this->_data['sms']	= $this->inq->getSms(1,$type);

		$this->load->view('listingsms', $this->_data);
		
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function getList_ajax($module_id,$type)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('89'));
		
		$this->db->where('sms_module_id',$module_id);
		$this->db->where('type',$type);
		
		if($module_id  == 1)
		{
			$this->db->join('register_auditors',"auditor_id=sms_receiver_id");
		}
		
		$this->db->order_by("sms_id", "DESC"); 
		$this->db->select('*');
		$query = $this->db->get('sms_history');
		
		$value	=	NULL;
		
		foreach($query->result() as $lc)
		{
			if($sm->sms_status == 1)
			{
				$class = "green_main_right_icon";
			}
			else
			{
				$class = "gray_main_right_icon";	
			}
					
			$concate_values	=	$lc->auditor_name .'|'. $lc->sms_receiver_number . '|'. $lc->sms_sent_date;
			
			if($lc->auditor_name)
			{
				$value	.=	'<li class="liinline">'.$lc->auditor_name.'</li>';
			}
			if($lc->sms_receiver_number)
			{
				$value	.=	'<li class="liinline">'.$lc->sms_receiver_number.'</li>';
			}
			if($lc->sms_sent_date)
			{
				$value	.=	'<li class="liinline">'.$lc->sms_sent_date.'</li>';
			}
							
			if($permissions[89]['d']	==	1)
			{	
				$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->sms_id.'" data-url="'.base_url().'inquiries/sms_delete/'.$lc->sms_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';
			}
			
			 $arr[] = array(
			"DT_RowId"=>$lc->sms_id.'_durar_lm',
			"DT_RowClass"=>'durar_right',
			"القائمة القصيرة‬‎" =>$value,
			"الإجراءات" =>$actions);
			unset($actions);
			unset($value);
		}
		
		echo json_encode($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function child_listing($listid	=	NULL)
	{
        //check_permission($this->_data['module'],'v');
		$this->_data['parent_id']	=	$listid;
		
		$this->_data['listing']	=	$this->listing->get_child_listing($listid);

		
		$this->load->view('child-type-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function add_parent($type	=	NULL)
	{
		if($type)
		{
			$this->_data['type'] = $this->listing->get_type_id($type);

		}
		else
		{
			//$this->_data['parent_id']	=	NULL;
		}
		
		$this->load->view('add-parent',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function add_child($parent_id)
	{
		if($parent_id)
		{
			$this->_data['parent_id']	=	$parent_id;
		}
		else
		{
			$this->_data['parent_id']	=	NULL;
		}
		
		$this->load->view('add-child',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function types_child_listing($listid	=	NULL)
	{
        check_permission($this->_data['module'],'v');
		$this->_data['listing']	=	$this->listing->get_child_listing($listid);
		$this->load->view('types-child-type-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function sub_child_listing1($listid	=	NULL)
	{
        check_permission($this->_data['module'],'v');
		$this->_data['listing']	=	$this->listing->get_subchild_listing($listid);
		$this->load->view('subchilds-type-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function types_sub_child_listing($listid	=	NULL)
	{
		$this->_data['listing']	=	$this->listing->get_subchild_listing($listid);
		$this->load->view('subchilds-type-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/		
	public function add_new()
	{
		$parent_id	=	$this->input->post("parent_id");
		$add_sub	=	$this->input->post("add_sub");
		
		$data	=	array("list_parent_id"=>$parent_id,"list_name"=>$add_sub);
		
		$this->listing->add_list_child($data);		
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function other()
	{
		$list_id	=	$this->input->post("id");
		$entry		=	$this->input->post("entry");
		
		$data		=	array('other' => $entry);

		$this->listing->update_record($list_id, $data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function addrules($listid	=	NULL)
	{

		if($listid)
		{
			$this->_data['single_list']	=	$this->listing->get_single_record($listid);	
		}
		
		if($this->input->post())
		{
			$data		=	$this->input->post();
			
			// UNSET ARRAY key
			unset($data['submit']);
			
			if($this->input->post('list_id'))
			{
				$this->listing->update_list($this->input->post('list_id'),$data);
				
				/*$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				redirect(base_url()."inquiries/rules");
				exit();*/
				
				$this->load->library('user_agent');
				if ($this->agent->is_referral())
				{
					$url	=	 $this->agent->referrer();
				}
				
				$this->session->set_flashdata('msg', '* ملاحظة : تم تحديث المعاملة بنجاح');
				redirect($url);
				exit();
			}
			else
			{
				$this->listing->add_list($data);
				
				/*$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				redirect(base_url()."inquiries/rules");
				exit();*/
				
				$this->load->library('user_agent');
				if ($this->agent->is_referral())
				{
					$url	=	 $this->agent->referrer();
				}
				
				$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
				redirect($url);
				exit();
			}
		}
		else
		{
			if($listid)
			{
				$this->_data['list_id']	=	$listid;
			}
			else
			{
				$this->_data['list_id']	=	'';
			}
            check_permission($this->_data['module'],'v');
			$this->load->view('inquiries/add-rule', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function rules()
	{
        check_permission($this->_data['module'],'v');
		$this->_data['listing']	=	$this->listing->get_rules();
		$this->load->view('rules-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function add_qualification($listid	=	NULL)
	{
		if($listid)
		{
			$this->_data['single_list']	=	$this->listing->get_single_record($listid);	
		}
		
		if($this->input->post())
		{
			$data		=	$this->input->post();
			
			// UNSET ARRAY key
			unset($data['submit']);
			
			if($this->input->post('list_id'))
			{
				$this->listing->update_list($this->input->post('list_id'),$data);
				
				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				}
				else
				{
					/*$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
					redirect(base_url()."inquiries/qualification_listing");
					exit();*/
					
					$this->load->library('user_agent');
					if ($this->agent->is_referral())
					{
						$url	=	 $this->agent->referrer();
					}
					
					$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
					redirect($url);
					exit();
				}
			}
			else
			{
				$this->listing->add_list($data);
				
				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				}
				else
				{
					/*$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
					redirect(base_url()."inquiries/qualification_listing");
					exit();*/
					
					$this->load->library('user_agent');
					if ($this->agent->is_referral())
					{
						$url	=	 $this->agent->referrer();
					}
					
					$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
					redirect($url);
					exit();
				}
			}
		}
		else
		{
			if($listid)
			{
				$this->_data['list_id']	=	$listid;
			}
			else
			{
				$this->_data['list_id']	=	'';
			}
            check_permission($this->_data['module'],'v');
			$this->load->view('inquiries/add-qualification', $this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Add Rule Form
	*/
	public function qualification_listing()
	{
		$this->_data['listing']	=	$this->listing->get_qualification();
		
		$this->load->view('qualification-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Add Rule Form
	*/
	public function add_rules_qualification($listid	=	NULL)
	{

		if($listid)
		{
			$this->_data['single_list']	=	$this->listing->get_single_record($listid);	
		}
		
		if($this->input->post())
		{
			$data		=	$this->input->post();
			
			// UNSET ARRAY key
			unset($data['submit']);
			
			if($this->input->post('list_id'))
			{
				$this->listing->update_list($this->input->post('list_id'),$data);
				
				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				}
				else
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
					redirect(base_url()."inquiries/rule_qualification_listing");
					exit();
				}
				

			}
			else
			{
				$this->listing->add_list($data);
				
				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				}
				else
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
					redirect(base_url()."inquiries/rule_qualification_listing");
					exit();
				}
			}
		}
		else
		{
			if($listid)
			{
				$this->_data['list_id']	=	$listid;
			}
			else
			{
				$this->_data['list_id']	=	'';
			}
			
			$this->load->view('inquiries/add-rules-qualification', $this->_data);
		}
	}	
//-------------------------------------------------------------------------------

	/*
	*
	* Add Rule Form
	*/
	public function rule_qualification_listing1($type	=	NULL)
	{
		if($type)
		{	
			// Get List Name By Type
			$this->_data['listing']			=	$this->listing->rules_qualification_by_type($type);
			$this->_data['list_type_name']	=	$type;

			$this->load->view('rule-qualification-type-listing', $this->_data);
		}
		else
		{
			$this->_data['listing']	=	$this->listing->get_rule_qualification();
			
			$this->load->view('rule-qualification-listing', $this->_data);
		}
			
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	
	public function delete_child($childlistid)
	{
		$this->listing->delete_child($childlistid);
		
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
		redirect(base_url().'listing_managment/listing/');
		exit();
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function delete_image()
	{
		$image = $this->input->post('image');
		$type = $this->input->post('type');
		
		if(isset($type) && $type !="")
		{
			echo $ret= $this->inq->delete_image($image,$type);
		}
		else
		{
			echo $ret= $this->inq->delete_image($image,'');
		}
  }
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	function deleteDoc()
	{
		$image = $this->input->post('image');
		
		echo $ret= $this->inq->delete_document($image);
	}
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	
	public function delete_banner($bannerid)
	{
		$this->inq->delete_banner($bannerid);
	}
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	public function delete_banner_ajax()
	{
		$image = $this->input->post('image');
		echo $this->inq->delete_banner_ajax($image);
	}	
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	public function delete($listid,$type)
	{
		$this->listing->delete($listid);
		
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
		redirect(base_url().'inquiries/listing/'.$type);
		exit();
	}
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	public function sms_delete($sms_id)
	{
		$this->listing->sms_delete($sms_id);
		
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
	}
	
//-------------------------------------------------------------------------------	
	public function get_list_data()
	{
		$list_id	=	$this->input->post('id');
		$data		=	$this->listing->get_list_data($list_id);
		echo  $data	=	json_encode(array('list_id'	=>	$data->list_id,'list_name'	=>	$data->list_name,'list_type'	=>	$data->list_type,'list_status'	=>	$data->list_status));
	}
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	public function transactionsprint()
	{
		$this->_data['all_applicatns']	=	$this->inq->get_all_applicatnts();
		
		$this->load->view('transactions-printlisting', $this->_data);
	}

//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function transactions($branchid=0,$charity_type=0)
	{
		$this->_data['branchid'] 		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		
		//phpinfo();exit();	
		$this->load->view('transactions-listing', $this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function all_helps($charity_type,$id_card_number)
	{
		$this->_data['all_helps']	=	$this->inq->get_all_helps($charity_type,$id_card_number);
				
		$this->load->view('all-helps-by-card-number', $this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function applicants_mother_help($charity_type,$mother_id_number)
	{
		$this->_data['all_helps']	=	$this->inq->get_all_mother_helps($charity_type,$mother_id_number);
				
		$this->load->view('all-helps-by-card-number-for-mother', $this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function applicants_father_help($charity_type,$father_id_number)
	{
		$this->_data['all_helps']	=	$this->inq->get_all_father_helps($charity_type,$father_id_number);
				
		$this->load->view('all-helps-by-card-number-for-father', $this->_data);
	}	
	public function inquiry_notes($applicant_id)
	{
		$this->_data['applicant_id'] = $applicant_id;
		$this->load->view("inquiry_notes",$this->_data);
	}
	
	public function savemyprintdata()
	{
		$ah_applicant_notes['applicantid']	=	$this->input->post("applicantid");
		$ah_applicant_notes['userid']		=	$this->input->post("userid");
		$ah_applicant_notes['branchid']		=	$this->input->post("branchid");
		$ah_applicant_notes['notes'] 		=	$this->input->post("notes");
		$ah_applicant_notes['notestype']	=	'from_receipt';
		
		$this->inq->save_my_notes($ah_applicant_notes); // Add Notes into Database
	}
	
	public function savemynotes()
	{
		$ah_applicant_notes['applicantid']	=	$this->input->post("applicantid");
		$ah_applicant_notes['userid']		=	$this->input->post("userid");
		$ah_applicant_notes['branchid']		=	$this->input->post("branchid");
		$ah_applicant_notes['notes'] 		=	$this->input->post("notes");
		$ah_applicant_notes['notestype']	=	'from_notes';
		
		$this->inq->save_my_notes($ah_applicant_notes); // Add Notes into Database
				
		$myNotes	=	$this->inq->getNotes($this->input->post("applicantid"));
		$html		=	"";
		
		foreach($myNotes as $mn)
		{
			$html .= '<table width="100%" style="border:1px solid #000 !important; direction: rtl; margin-bottom:8px;" >';
			$html .= '<tr>';
			$html .= '<td style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>الموظف:</strong> '.$mn->fullname.'</td>';
			$html .= '<td  style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>فرع:</strong> '.$mn->branchname.'</td>';
			$html .= '<td  style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>تاريخ:</strong> '.arabic_date($mn->notesdate).'</td>';
			$html .= '</tr>';	
			$html .= '<tr>';
			$html .= '<td colspan="3" class="print_td">'.$mn->notes.'</td>';
			$html .= '</tr>';
			$html .= '</table>';
		}
		
		echo $html;
	}
	
	public function showallnotes()
	{
		$applicantid = $this->input->post("applicantid");
		$myNotes = $this->inq->getNotes($applicantid);
		$html = "";
		
		foreach($myNotes as $mn)
		{
			$html .= '<table width="100%" style="border:1px solid #000 !important; direction: rtl; margin-bottom:8px;" >';
			$html .= '<tr>';
			$html .= '<td style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>الموظف:</strong> '.$mn->fullname.'</td>';
			$html .= '<td  style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>فرع:</strong> '.$mn->branchname.'</td>';
			$html .= '<td  style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>تاريخ:</strong> '.arabic_date($mn->notesdate).'</td>';
			$html .= '</tr>';	
			$html .= '<tr>';
			$html .= '<td colspan="3" class="print_td">'.$mn->notes.'</td>';
			$html .= '</tr>';
			$html .= '</table>';
		}
		
		echo $html;
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function inquiries_list($branchid=0,$charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array(17));
		
		if($branchid!=0 && $branchid!='')
		{
			$branchid = $branchid;
		}
		else
		{
			$branchid = $this->haya_model->get_branch_id($branchid,$charity_type);
		}
		
		$result	=	$this->inq->get_inquiries_list($branchid,$charity_type);
		
		foreach($result as $lc)
		{
			//$action   = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			//$action  .= ' <a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore_print/'.$lc->applicantid.'/idcard_number"><i class="icon-print"></i></a>';
			
			//$action  .= ' <a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/inquiry_notes/'.$lc->applicantid.'"><i class="icon-book"></i></a>';
			
			//$action  .= ' <a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			
			if($permissions[17]['u']	==	1) 
			{	
				//$action .= ' <a class="iconspace" href="'.charity_edit_url($lc->charity_type_id).$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			if($permissions[17]['d']	==	1) 
			{
				//$action .= ' <a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			
			$steps	=	 config_item('steps');
			
			
			$arr[] = array(					
				//'رقم المعاملة'		=>	arabic_date($lc->applicantcode),
				'الإسم'				=>	'<div class="right"><a class="iconspace" href="'.charity_edit_url($lc->charity_type_id).$lc->applicantid.'">'.$lc->fullname.'</a></div>',
				'نوع المساعدات'		=>	$lc->charity_type,
				'البطاقة الشخصة'	=>	arabic_date($lc->idcard_number),
				'المحافظة'			=>	$lc->province,
				'ولاية'				=>	$lc->wilaya,
				'جنس'				=>	$lc->gender,
				'تاريخ التسجيل'		=>	date('Y-m-d',strtotime($lc->registrationdate)),  
				'المرحلة'			=>	$steps[$lc->step], 
				'كل الملاحظات'		=>	'<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/inquiry_notes/'.$lc->applicantid.'"><i class="icon-book"></i></a>'.' ('.$this->inq->getNotesCount($lc->applicantid).')');
							
				unset($action);
		}
			
		checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socilservay_list($branchid=0,$charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('17'));
		//$result			=	$this->inq->get_socilservay_list($branchid,$charity_type);
		$result			=	$this->inq->socilservay_list_storedprocedure($branchid,$charity_type);

		foreach($result as $lc)
		{			
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			if($permissions[17]['u']	==	1) 
			{	$action .= '<a class="iconspace" href="'.base_url().'inquiries/socilservayresult/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
			
			if($permissions[17]['d']	==	1) 
			{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم"				=>	$lc->applicantcode,
				"الإسم"				=>	'<a class="iconspace" href="'.base_url().'inquiries/socilservayresult/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
				"نوع المساعدات"		=>	$lc->charity_type,
				"فرع"				=>	$lc->branchname,
				"البطاقة الشخصة"	=>	$lc->idcard_number,
				"المحافظة"			=>	$lc->province,
				"ولاية"				=>	$lc->wilaya,
				"تاريخ التسجيل"		=>	show_date($lc->registrationdate,9),              
				"الإجرائات"			=>	$action);
				unset($action);
		}
		
		/*for($i	=	1;$i	<=	20000;	$i++)
		{
			$arr[] = array(
				"DT_RowId"			=>	"",
				"رقم"				=>	$i,
				"الإسم"				=>	"",
				"نوع المساعدات"		=>	"",
				"فرع"				=>	"",
				"البطاقة الشخصة"	=>	$i,
				"المحافظة"			=>	"",
				"ولاية"				=>	"",
				"تاريخ التسجيل"		=>	"12-06-2017",              
				"الإجرائات"			=>	"");
				unset($action);
		}*/
		
		checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socilservay_list_completesteps($branchid=0,$charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('17'));
		$result			=	$this->inq->get_socilservay_list_completesteps($branchid,$charity_type);
		
		foreach($result as $lc)
		{			
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			//$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			if($permissions[17]['u']	==	1) 
			{
				//$action .= '<a class="iconspace" href="'.base_url().'inquiries/socilservayresult/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			if($permissions[17]['d']	==	1) 
			{
				//$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم"				=>	$lc->applicantcode,
				"الإسم"				=>	$lc->fullname,
				"نوع المساعدات"		=>	$lc->charity_type,
				"فرع"				=>	$lc->branchname,
				"البطاقة الشخصة"	=>	$lc->idcard_number,
				"المحافظة"			=>	$lc->province,
				"ولاية"				=>	$lc->wilaya,
				"تاريخ التسجيل"		=>	$lc->registrationdate,              
				"الإجرائات"			=>	$action);
				unset($action);
		}
		
		checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socilservay_list_step($branchid,$charity_type=0,$step)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('206'));
		
		/*$result	=	$this->inq->get_socilservay_list_step($branchid,$charity_type);*/ // Before
		$result	=	$this->inq->get_socilservay_list_special_slt($branchid,$charity_type,$step); // After changing the function for second step
		/*print_r($result);
		exit();*/
		foreach($result as $lc)
		{
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';

			if(!isset($step) OR $lc->application_status	!= 'إعادة نظر')
			{
				$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			}	
			if($permissions[17]['u']	==	1) 
			{
				if(!isset($step) OR $lc->application_status	!= 'إعادة نظر')
				{
					$action .= '<a class="iconspace" href="'.base_url().'inquiries/socilservayresult/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				}
			}
			
			//if($permissions[17]['d']	==	1) 
			//{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
			
			if(!isset($step) OR $lc->application_status	!= 'إعادة نظر')
			{
				$name	=	'<a class="iconspace" href="'.base_url().'inquiries/socilservayresult/'.$lc->applicantid.'">'.$lc->fullname.'</a>';
			}
			else
			{
				$name	=	$lc->fullname;
			}
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم" 				=>	$lc->applicantcode,
				"الإسم" 				=>	$name,
				"نوع المساعدات" 	=>	$lc->charity_type,
				"فرع" 				=>	$lc->branchname,
				"البطاقة الشخصة" 	=>	$lc->idcard_number,
				"المحافظة" 			=>	$lc->province,
				"ولاية" 				=>	$lc->wilaya,
				"تاريخ التسجيل" 	=>	$lc->registrationdate,              
				"الإجرائات" 			=>	$action);
				unset($action);
		}
		
		checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function finaldecission_list($branchid=0,$charity_type=0,$listid	=	NULL)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('145'));
			
		$result	=	$this->inq->get_finaldecission_list($branchid,$charity_type,$listid); // Get finaldecession List 
		
		foreach($result as $lc)
		{				
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			
			if($permissions[145]['u']==1) 
			{	$action .= '<a class="iconspace" href="'.base_url().'inquiries/finaldecission_edit/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
			
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم"				=>	arabic_date($lc->applicantcode),
				"الإسم"				=>	'<a class="iconspace" href="'.base_url().'inquiries/finaldecission_edit/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
				"نوع المساعدات"		=>	$lc->charity_type,
				"فرع"				=>	$lc->branchname,
				"البطاقة الشخصة"	=>	arabic_date($lc->idcard_number),
				"المحافظة"			=>	$lc->province,
				"ولاية"				=>	$lc->wilaya,
				/*"الاجتماع رقم"		=>	$lc->meeting_number,*/
				"تاريخ التسجيل"		=>	arabic_date($lc->registrationdate),              
				"الإجرائات"			=>	$action);
				
				unset($action);
		}
		
		checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socilservay_list_special($branchid=0,$charity_type=0,$step	=	NULL)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('66'));
		
		$socilservay	=	$this->inq->get_socilservay_list_special($branchid,$charity_type,$step);
				
		foreach($socilservay as $lc)
		{
			
			$action 	= '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			//$action 	.= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			if($permissions[66]['u']	==	1) 
			{
				if(!isset($step))
				{
					$action .= '<a class="iconspace" href="'.base_url().'inquiries/socialnoneditable/'.$lc->applicantid.'/'.$charity_type.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	
				}
			}
			
			if($permissions[66]['d']==1) 
			{
				if(!isset($step))
				{
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
			}
			
			if(!isset($step))
			{
				$name	=	'<a class="iconspace" href="'.base_url().'inquiries/socialnoneditable/'.$lc->applicantid.'/'.$charity_type.'">'.$lc->fullname.'</a>';
			}
			else
			{
				$name	=	$lc->fullname;
			}
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم" 				=>	arabic_date($lc->applicantcode),
				"الإسم" 				=>	$name,
				"نوع المساعدات" 	=>	$lc->charity_type,
				"فرع" 				=>	$lc->branchname,
				"البطاقة الشخصة" 	=>	arabic_date($lc->idcard_number),
				"المحافظة" 			=>	$lc->province,
				"ولاية" 				=>	$lc->wilaya,
				"عاجل" 				=>	$lc->ajel,
				"تاريخ التسجيل"		=>	arabic_date($lc->registrationdate),              
				"الإجرائات" 			=>	$action);
				
				unset($action);
		}
			
		checkArraySize($arr);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function socilservay_list_special_slt($charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('66'));
		$socilservay	=	$this->inq->get_socilservay_list_special_slt($charity_type);
								
			foreach($socilservay as $lc)
			{
				
				$action 	= '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
				$action 	.= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
				
				if($permissions[66]['u']	==	1) 
				{
					$action .= '<a class="iconspace" href="'.base_url().'inquiries/socialnoneditable/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
				
				//if($permissions[66]['d']==1) 
				//{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
				
				$arr[] = array(
					"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
					"رقم" 				=>	arabic_date($lc->applicantcode),
					"الإسم" 				=>	'<a class="iconspace" href="'.base_url().'inquiries/socialnoneditable/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
					"نوع المساعدات" 	=>	$lc->charity_type,
					"فرع" 				=>	$lc->branchname,
					"البطاقة الشخصة" 	=>	arabic_date($lc->idcard_number),
					"المحافظة" 			=>	$lc->province,
					"ولاية" 				=>	$lc->wilaya,
					"عاجل" 				=>	$lc->ajel,
					"تاريخ التسجيل"		=>	arabic_date($lc->registrationdate),              
					"الإجرائات" 			=>	$action);
					
					unset($action);
			}
			
			checkArraySize($arr);
	}	
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function ssr_rejected($charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('66'));
		$socilservay	=	$this->inq->get_ssr_rejected($charity_type);
						
		foreach($socilservay as $lc)
		{
			$action 	= '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			$action 	.= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			if($permissions[66]['u']	==	1) 
			{
				$action .= '<a class="iconspace" href="'.base_url().'inquiries/socialnoneditable/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
			
			//if($permissions[66]['d']==1) 
			//{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم" 				=>	arabic_date($lc->applicantcode),
				"الإسم" 				=>	'<a class="iconspace" href="'.base_url().'inquiries/socialnoneditable/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
				"نوع المساعدات" 	=>	$lc->charity_type,
				"فرع" 				=>	$lc->branchname,
				"البطاقة الشخصة" 	=>	arabic_date($lc->idcard_number),
				"المحافظة" 			=>	$lc->province,
				"ولاية" 				=>	$lc->wilaya,
				"عاجل" 				=>	$lc->ajel,
				"تاريخ التسجيل"		=>	arabic_date($lc->registrationdate),              
				"الإجرائات" 			=>	$action);
				
				unset($action);
		}
		
		checkArraySize($arr);
	}	
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function rejected_applicants()
	{
        check_permission($this->_data['module'],'v');
		
		$this->_data['all_applicatns']	=	$this->inq->get_all_reject_bank();
		
		$this->load->view('rejected-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function comitte_descions()
	{
        check_permission($this->_data['module'],'v');
		
		$this->_data['all_descisions']	=	$this->inq->get_all_decisions();
		
		$this->load->view('transactions-listing', $this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function get_applicant_data($applicantid	=	NULL)
	{
		
		$this->_data['applicatn_info']	=	$this->inq->get_single_applicatnt($applicantid);
		$this->_data['udetail'] 		=	$this->inq->getRequestInfo($applicantid);
		
		echo $html = $this->load->view('ajax-response-html', $this->_data,TRUE);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	function getHistory($applicantid	=	NULL){
				
		$this->load->model('ajax/ajax_model', 'ajax');
			
	    $this->_data['history'] =	$this->ajax->getHistory($applicantid);
		$this->_data['tempid']	=	$applicantid;
		
		echo $HTML	=	$this->load->view('ajax-history-response-html', $this->_data,TRUE);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function get_auditor_data($auditorid	=	NULL)
	{
		$this->_data['auditor_info']	=	$this->inq->get_single_auditor($auditorid);
		$this->_data['main_info']	=	$this->inq->get_main_info($auditorid);
		
		echo $HTML	=	$this->load->view('ajax-inquiries-response-html', $this->_data,TRUE);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function inqprint()
	{
		$inqdata = $this->input->post('inquery_id');
		$this->_data['auditor_info']	=	$this->inq->get_single_auditor($inqdata);
		$this->_data['main_info'] 		=	$this->inq->get_main_info($inqdata);
		$this->load->view('print_inquery',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function addbanners($banner_id	=	NULL)
	{
		if($banner_id)
		{
			$this->_data['banner_detail']	=	$this->haya_model->get_banner_detail($banner_id);
		}

		$this->load->view('add-banner',$this->_data);	// Add Banner Form
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function finaldecission_edit($applicantid)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		
		if($this->input->post())
		{
			$applicantid 		= $this->input->post('applicantid');
			$application_status = $this->input->post('application_status');
			
			
			
			if($application_status	==	"إعادة نظر")
			{
				$apStatus 				=	2;
				$case_close 			=	0;
				$ah_applicant['isOld'] 	=	1;
			}
			else
			{
				$apStatus 		=	4;
				$case_close 	=	1;
			}
			
			if($application_status)
			{
				if($applicantid	!=	'')
				{
					if($application_status	==	'إعتذار')
					{
						$ah_applicant['application_status']	=	'رفض';	
					}
					if($application_status	==	'تحويل الى مالية')
					{
						$ah_applicant['application_status']	=	'إحالة';	
					}
					if($application_status	==	'تأجيل')
					{
						$ah_applicant['application_status']	=	'إحالة';	
					}
					
					//$ah_applicant['application_status'] =	$application_status;
					$ah_applicant['applicant_notes'] 	=	$this->input->post('notes');
					$ah_applicant['case_close'] 		=	$case_close;
                    $ah_applicant['meeting_number'] 	=	$this->input->post('meeting_number');
                    $ah_applicant['meeting_notes'] 		=	$this->input->post('meeting_notes');
					

					$this->inq->update_final_decission($applicantid,$ah_applicant); // UPDATE Final dession
					
					//Adding data into dicission table
					$ah_applicant_decission['applicantid'] 				=	$applicantid;
					$ah_applicant_decission['userid'] 					=	$this->_login_userid;
					$ah_applicant_decission['decission'] 				=	$ah_applicant['application_status'];
					$ah_applicant_decission['decission_amount'] 		=	$this->input->post('decission_amount');
					$ah_applicant_decission['meeting_number'] 			=	$this->input->post('meeting_number');
					$ah_applicant_decission['notes'] 					=	$this->input->post('notes');
					$ah_applicant_decission['decissionip']				=	$_SERVER['REMOTE_ADDR'];
					$ah_applicant_decission['meeting_notes']			=	$this->input->post('meeting_notes');
					$ah_applicant_decission['refuse_notes']				=	$this->input->post('refuse_notes');
					
					if($this->input->post('charity_type_id')	==	'80')
					{
						$ah_applicant_decission['applicant_name']			=	$this->input->post('applicant_name');
						$ah_applicant_decission['medical_accessories']		=	$this->input->post('medical_accessories');
						$ah_applicant_decission['applicant_bank']			=	$this->input->post('applicant_bank');
						$ah_applicant_decission['applicant_account_number']	=	$this->input->post('applicant_account_number');
					}
					if($_FILES["decission_doc"]["tmp_name"])
					{
						// Uplaod Decision Document
						$document			=	$this->upload_file($this->_login_userid,'decission_doc','resources/applicants'); 
						$ah_applicant_decission['decission_doc']	=	$document;
					}
					
					$this->inq->add_final_decission($ah_applicant_decission); // ADD Final dession
					
					$this->haya_model->save_steps($applicantid,$apStatus); // ADD step
					$this->haya_model->update_steps($applicantid,$apStatus); // Update Step
					
					// Transfer applicant from SAKANIYA/ELAJIA TO MALIA NAQDIYA
					if($application_status	==	'تحويل الى مالية')
					{
						$transfer['charity_type_id'] 		=	$this->input->post('charity_type_id');
                    	$transfer['section_listid'] 		=	$this->input->post('section_listid');
						$transfer['old_charity_type_id'] 	=	$this->input->post('old_charity_type_id');
                    	$transfer['old_section_listid'] 	=	$this->input->post('old_section_listid');
						$transfer['applicantid'] 			=	$this->input->post('applicantid');
						$transfer['login_userid'] 			=	$this->_login_userid;
						
						// INSERT Transfer Detail into DATABASE
						$this->inq->tranfer_detail($transfer);
						
						$update_type_nolist['charity_type_id'] 		=	$this->input->post('charity_type_id');
                    	$update_type_nolist['section_listid'] 		=	$this->input->post('section_listid');
						
						// UPDATE charity_type_id and section_list
						$this->inq->update_type_nolist($this->input->post('applicantid'),$update_type_nolist);
						
						$sectin_list['listid'] 			=	$this->input->post('section_listid');
						
						// UPDATE charity_type_id and section_list
						$this->inq->update_sectin_list($this->input->post('applicantid'),$sectin_list);
					}

					
					$referred_from = $this->session->userdata('referred_from');
					redirect($referred_from, 'refresh');
					exit();
				}
			}
		}
		else
		{
			$this->session->set_userdata('referred_from', $_SERVER['HTTP_REFERER']);
		}
		
				
		$this->_data['_applicantid']	= $applicantid;			
		$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($applicantid);
		
		$applicantid	=	$this->_data['_applicant_data']['ah_applicant']->applicantid;
		$section_status	=	$this->_data['_applicant_data']['ah_applicant']->section_status;
				
		if(isset($applicantid) AND isset($section_status))
		{
			$notes								=	$this->inq->get_section_notes($applicantid,$section_status);

			$this->_data['maintenance_type']	=	$notes->maintenance_type;
			$this->_data['proposal_section']	=	$notes->proposal_section;
			$this->_data['pictures']			=	$notes->pictures;
			$this->_data['certificate']			=	$notes->certificate;
			$this->_data['report_visit']		=	$notes->report_visit;
			$this->_data['list_type_amount']	=	$notes->list_type_amount;
			$this->_data['list_type_notes']		=	$notes->list_type_notes;
			$this->_data['section_status']		=	$notes->section_status;
			$this->_data['notes']				=	$notes->notes;
			$this->_data['doctor_notes']		=	$notes->doctor_notes;
			$this->_data['listname']				=	$notes->listname;
			
			// Get No List
			$this->_data['all_list']		=	$this->inq->get_no_list();
			
		}
		
		// 		
		$this->load->view('finaldecission_edit', $this->_data);
	}	
//-------------------------------------------------------------------------------

	/*
	* 
	*/    
    public function checkAmount()
    {
       $application_status 	=	$this->input->post('application_status');
       $totalfamilymember 	=	$this->input->post('totalfamilymember');
       $loanamount 			=	$this->input->post('loanamount');
       $familysalary 		=	$this->input->post('familysalary');
       
	   $result 				=	$this->inq->get_total_family_members($totalfamilymember,$familysalary); // Get Data 
        
       $html  = '<table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">';
       $html .= '<thead>';
       $html .= '<tr role="row">';
       $html .= '<th style="text-align:center;">عدد افراد الاسرة</th>';
       $html .= '<th style="text-align:center;">الفئة المستحقة</th>';
       $html .= '<th style="text-align:center;">المبلغ</th>';
       $html .= '<th style="text-align:center;">صافي الدخل</th>';
      
       $html .= '</tr>';
       $html .= '</thead>';
       $html .= '<tbody role="alert" aria-live="polite" aria-relevant="all">';
	   
       foreach($result as $lc)
       {
        	if($lc->people_start >= 0 && $lc->people_end<=1)
			{	
				$text = "فرد واحد";	
			}
			else if($lc->people_start >= 2 && $lc->people_end<=3)
			{
				$text = "فردين او ".arabic_date(3)." افراد";
			}
			else
			{	
				$text  = arabic_date($lc->people_start)." او ".arabic_date($lc->people_end)." افراد";
			}
				$html .= '<tr role="row">';
				$html .= '<td style="text-align:center;">'.$text.'</td>';
				$html .= '<td style="text-align:center;">'.number_drop_box_arabic('',$lc->priority,1).'</td>';
				$html .= '<td style="text-align:center;"><strong style="font-size: 14px; color: #F00;">'.arabic_date($lc->amount).'</strong></td>';
				$html .= '<td style="text-align:center;">'.arabic_date($lc->netincome).' ريال فما دون'.'</td>';
				$html .= '</tr>';
       }
	   
       $html .= '</tbody>';
       $html .= '</table>';
	   
	   $userTable['html_table'] =	$html;
	   $userTable['amount'] 	=	$lc->amount;
	   
       echo json_encode($userTable);	//	RETURN as JSON encode
    }
//-------------------------------------------------------------------------------

	/*
	* 
	*/    
    public function rejected($branchid=0,$charity_type=0)
    {
        $this->_data['branchid'] 		=	$branchid;
        $this->_data['charity_type'] 	=	$charity_type;
		
        $this->load->view('reject_list', $this->_data); 
    }
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
 	public function rejected_list($branchid=0,$charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('149'));
		
		$rejected_list	=	$this->inq->rejected_list($branchid,$charity_type); // Get Rejected List
	
		foreach($rejected_list as $lc)
		{
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			
			if($permissions[149]['u']	==	1) 
			{
				$action .= '<a class="iconspace" href="'.base_url().'inquiries/finaldecission_edit/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
			
			//if($permissions[149]['d']	==	1) 
			//{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->applicantid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->applicantid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
			
			$arr[] = array(
				"DT_RowId"				=>	$lc->applicantid.'_durar_lm',
				"رقم" 					=>	arabic_date($lc->applicantcode),
				"الإسم" 					=>	'<a class="iconspace" href="'.base_url().'inquiries/finaldecission_edit/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
				"نوع المساعدات" 		=>	$lc->charity_type,
				"فرع" 					=>	$lc->branchname,
				"عرض في الاجتماع رقم" 	=>	'<a href="#" onclick="alatadad(this);" data-url="'.base_url().'inquiries/get_meeting_number/'.$lc->applicantid.'">'.arabic_date($lc->meeting_number).'</a>',
				"المحافظة" 				=>	$lc->province,
				"ولاية" 					=>	$lc->wilaya,
				"تاريخ التسجيل" 		=>	arabic_date($lc->registrationdate),              
				"الإجرائات" 				=>	$action);
				unset($action);
		}
				
		checkArraySize($arr);
	}
//-------------------------------------------------------------------------------
	/*
	* 
	*/    
    public function get_meeting_number($applicantid)
    {
        $this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($applicantid);
        $this->load->view('meetingdata',$this->_data); 
    }
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function givemoney()
	{
		$this->load->view('givemoney_view',$this->_data);
	}
// -------------------------------------------------------------------------------

	/*
	* 
	*/
	public function givemoney_list($branchid=0,$charity_type=0)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('145'));
		$result			=	$this->inq->get_money_last($branchid,$charity_type); // Give Money list
		
		foreach($result as $lc)
		{				
			$action  = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			
			if($permissions[145]['u']==1) 
			{
				$action .= '<a class="iconspace" href="'.base_url().'inquiries/finaldecission_edit/'.$lc->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
			
				$decission = $this->getLastDecission($lc->applicantid);
			
				$arr[] = array(
					"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
					"رقم" 				=>	arabic_date($lc->applicantcode),
					"الإسم" 				=>	'<a class="iconspace" href="'.base_url().'inquiries/finaldecission_edit/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
					"نوع المساعدات" 	=>	$lc->charity_type,
					"فرع" 				=>	$lc->branchname,
					"البطاقة الشخصة" 	=>	arabic_date($lc->idcard_number),					
					"القيمة" 			=>	arabic_date($decission->decission_amount),
					"تاريخ اللجنة" 		=>	arabic_date($decission->decissiontime),              
					"الإجرائات" 			=>	$action);
					
				unset($action);
		}
		
		checkArraySize($arr);
	}

// -------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function getLastDecission($applicant_id)
	{
		return $this->inq->get_last_decission($applicant_id);
	}
// --------------------------------------------------------------------------------

	/*
	* Upload Files First Visit
	* @param $applicant_id 
	*
	*/
	public function uploads_sakaniya_visits($applicant_id)
	{
		$path	=	'resources/users'; // Define Path where the files Upload
		
		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data =	$this->upload->data();
			$name		=	$image_data['file_name'];
			$data		=	array(
								'applicantid'	=>	$applicant_id,
								'userid'		=>	$this->_login_userid,
								'imagename'		=>	$name);
			
			$this->inq->save_first_ziyarah($data);	// Insert Data of First Visit	
			echo json_encode(array('status' => 'ok'));	// Response
		}
	}

// -----------------------------------------------------------------------

	/*
	*	File Uploading
	*	@param $userid integer
	*	@param $filefield integer Input File name
	*	@param $folder integer older name where Image Upload
	*	@param $width integer
	*	@param $height integer
	*/
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
				
		if(!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();		
			return $image_data['file_name'];
		}
	}
// ------------------------------------------------------------------------

  	/**
   	*	Dynamic Forms Listing Page
   	*	@param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]	=	$this->haya_model->get_all_custom_form($moduleid);
		$this->_data["userid"]	=	$this->_login_userid;
		
		$this->_data['formid']	=	$moduleid;
		 
		// Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);
	 }
//-----------------------------------------------------------------------
	/*
	* This Method copied Yateem Controller
	* Yateem Listing Page
	*/
	function yateem_list()
	{
		$this->load->view('yatem-listing',$this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	* This Method copied Yateem Controller
	* Yateem Listing Page
	*/	
	public function all_yateem_list($id	=	NULL,$type	=		NULL,$status	=	NULL)
	{
		$this->load->model('yateem/yateem_model','yateem'); // Load Yateem Model
		
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		
		if($id)
		{
			$all_users	=	$this->yateem->get_yateeem_list($id,$type,'1');
		}
		else
		{
			$all_users	=	$this->yateem->get_yateeem_list_new($status,'1');
		}

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				$action  = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				$action .= '<a class="iconspace" href="'.base_url().'yateem/add_yateem/'.$lc->orphan_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
/*				$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_yateeem/'.$lc->orphan_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
*/				$url 	= '<a class="iconspace" href="'.base_url().'inquiries/yateem_servay/'.$lc->orphan_id.'">'.is_set($lc->orphan_name).'</a>';

				$img = '<img src="'.base_url().'">';
				$arr[] = array(
					"DT_RowId"			=>	$lc->orphan_id.'_durar_lm',
					"رقم المدني لليتيم"	=>	is_set(arabic_date($lc->file_number)),
					"اسم اليتيم" 		=>	$url,
					"الجنسية" 			=>	is_set($this->haya_model->get_name_from_list($lc->orphan_nationalty)),
					"تاريخ الميلاد" 		=>	is_set(arabic_date($lc->date_birth)),
					"الدولة" 			=>	is_set($this->haya_model->get_name_from_list($lc->country_id)),
					"المدينة" 			=>	is_set($this->haya_model->get_name_from_list($lc->city_id)),
					"تاريخ تقديم الطلب" =>	date('Y-m-d',strtotime($lc->created)),
					"الإجرائات" 			=>	$action);
					
					unset($action);
			}
			
			$ex['data'] = $arr;
						
			echo json_encode($ex);			
		}
	}
//--------------------------------------------------------------------------------//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function yateem_servay($orphan_id)
	{
		$this->load->model('yateem/yateem_model','yateem'); // Load Yateem Model
		
		//$this->haya_model->check_permission($this->_data['module'],'v');
		if(isset($orphan_id) && $orphan_id!="")
		{
			$this->_data['yateem_data'] 		=	$this->yateem->getYateemDataById($orphan_id);
			$this->_data['yateem_sisters'] 		=	$this->yateem->getBroSisNames($orphan_id,'sister');
			$this->_data['yateem_brothers'] 	=	$this->yateem->getBroSisNames($orphan_id,'brother');
			$this->_data['yateem_father_data'] 	=	$this->yateem->get_father_data($orphan_id);
			$this->_data['yateem_mother_data'] 	=	$this->yateem->get_mother_data($orphan_id);
			$this->_data['yateem_banks_data'] 	=	$this->yateem->getBanksData($orphan_id);
			$this->_data['yateem_others_data'] 	=	$this->yateem->getOthersData($orphan_id);
			$this->_data['yateem_docs'] 		=	$this->yateem->getYateemDocument($orphan_id);
			
			$this->_data['yateem_edu_data'] 	=	$this->yateem->get_yateem_others_data('ah_yateem_education',$orphan_id);
			$this->_data['yateem_can_data'] 	=	$this->yateem->get_yateem_others_data('ah_yateem_candidate',$orphan_id);
			
			$this->_data['orphan_id']			=	$orphan_id;
			$this->_data['yateem_sisters_brothers'] 	=	$this->yateem->getBroSisNamesNew($orphan_id,'sister');
			
		}
		
		$this->load->view('yateem-step-2', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* functon Adding Social Servay Result
	*/	
	public function add_yateem_servay()
	{
		$orphan_id 					=	$this->input->post('orphan_id');
		$userid 					=	$this->_login_userid;
		$health_condition 			=	$this->input->post('health_condition');
		$health_condition_text 		=	$this->input->post('health_condition_text');
		$housing_condition 			=	$this->input->post('housing_condition');
		$housing_condition_text 	=	$this->input->post('housing_condition_text');
		$numberofpeople 			=	$this->input->post('numberofpeople');
		$positioninfamily 			=	$this->input->post('positioninfamily');
		$economic_condition 		=	$this->input->post('economic_condition');
		$economic_condition_text 	=	$this->input->post('economic_condition_text');
		$casetype 					=	$this->input->post('casetype');
		$aps_name 					=	$this->input->post('aps_name');
		$aps_account 				=	$this->input->post('aps_account');
		$aps_filename 				=	$this->input->post('aps_filename');
		$aps_category 				=	$this->input->post('aps_category');
		$aps_category_text 			=	$this->input->post('aps_category_text');
		$aps_salaryamount 			=	$this->input->post('aps_salaryamount');
		$aps_date 					=	$this->input->post('aps_date');
		$aps_another_income 		=	json_encode($this->input->post('aps_another_income'));
		$whyyouwant 				=	$this->input->post('whyyouwant');
		$summary 					=	$this->input->post('summary');
		$review 					=	$this->input->post('review');
		$ajel 						=	$this->input->post('ajel');
		$maintenance_type 			=	$this->input->post('maintenance_type');
		$report_visit 				=	$this->input->post('report_visit');
		$certificate_date 			=	$this->input->post('certificate_date');
		$aps_month 					=	$this->input->post('aps_month');
		$aps_year 					=	$this->input->post('aps_year');
		$step 						=	$this->input->post('step');
		
		
		$ah_applicant_survayresult = array(
			'orphan_id'					=>	$orphan_id,
			'userid'					=>	$userid,
			'health_condition'			=>	$health_condition,
			'health_condition_text'		=>	$health_condition_text,
			'housing_condition'			=>	$housing_condition,
			'housing_condition_text'	=>	$housing_condition_text,
			'numberofpeople'			=>	$numberofpeople,
			'positioninfamily'			=>	$positioninfamily,
			'economic_condition'		=>	$economic_condition,
			'economic_condition_text'	=>	$economic_condition_text,
			'casetype'					=>	$casetype,
			'aps_name'					=>	$aps_name,
			'aps_account'				=>	$aps_account,
			'aps_filename'				=>	$aps_filename,
			'aps_category'				=>	$aps_category,
			'aps_category_text'			=>	$aps_category_text,
			'aps_salaryamount'			=>	$aps_salaryamount,
			'aps_date'					=>	$aps_date,
			'aps_month'					=>	$aps_month,
			'aps_year'					=>	$aps_year,
			'aps_another_income'		=>	$aps_another_income,
			'whyyouwant'				=>	$whyyouwant,
			'summary'					=>	$summary,
			'ajel'						=>	$ajel,
			'maintenance_type'			=>	$maintenance_type,
			'report_visit'				=>	$report_visit,
			'proposal_section'			=>	$proposal_section,
			'certificate_date'			=>	$certificate_date,
			'review'					=>	$review);
			
		$this->inq->add_yateem_servay($ah_applicant_survayresult); // SAVE ALL the record into database
		$this->inq->update_yateem_step($orphan_id,$step); // UPDATE Orphan Step
		
		// For Notification
		$notification_data	=	array();
		
		$notification_data['step']				=	$step;
		$notification_data['notification_to']	=	'ORPHAN';
		$notification_data['reffrence_id']		=	$orphan_id;
		$notification_data['status']			=	'2';
		
		$this->haya_model->add_action_for_notification($notification_data); // UPDATE Orphan Step
		
		$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
		redirect(base_url().'yateem/orphan_sponsorship');
		exit();
		
		/*$this->load->library('user_agent');
		if ($this->agent->is_referral())
		{
			$url	=	 $this->agent->referrer();
		}
		
		$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
		redirect($url);
		exit();*/
	}
	
		/***************************************************/
				/* Add New Section dated 03-01-2017*/
				/* Muhammad Muzaffar*/
		/***************************************************/
//-------------------------------------------------------------------------------

	/*
	* Get all types for SECTIONS
	*/
	public function sections($branchid=0,$charity_type=0)
	{
		$branchid						=	$this->_data['user_detail']['profile']->branchid;
		$this->_data['moduleparent'] 	= 	224;
		$this->_data['permission'] 		= 	json_decode($this->_data['user_detail']['profile']->permissionjson,true);
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data['method_name']		=	'sections_categories';
		$this->_data["step"] 			=	3;
		$this->_data["done_step"]		=	4;
		$this->_data['complete_step']	=	'socilservayresultfilledlist_completesteps';
		
		$this->_data["sections"]  		=	$this->inq->section_types_parents('3',$branchid,NULL);
		

		$this->load->view('sections', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES
	*/
	public function sections_categories($branchid,$charity_type)
	{
		$branchid						=	$this->_data['user_detail']['profile']->branchid;
		$this->_data['moduleparent'] 	= 	17;
		$this->_data['permission'] 		= 	json_decode($this->_data['user_detail']['profile']->permissionjson,true);
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data['method_name']		=	'sections_countries';
		$this->_data["step"] 			=	3;
		$this->_data["done_step"]		=	4;
		$this->_data['complete_step']	=	'socilservayresultfilledlist_completesteps';
		
		$this->_data["section_types"]   =	$this->inq->section_types('3',$branchid,$charity_type);
		
		$this->load->view('section-categories', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Countries related to their Region
	*/
	public function sections_countries($branchid,$reagionid,$charity_type,$country_id)
	{
		if($charity_type	==	'1699')
		{
			$charity_type_id	=	'78';
		}
		else if($charity_type	==	'1700')
		{
			$charity_type_id	=	'79';
		}
		else if($charity_type	==	'1701')
		{
			$charity_type_id	=	'80';
		}
		else
		{
			$charity_type_id	=	'81';
		}
		
		if($reagionid	==	'1708' || $reagionid	==	'1709' || $reagionid	==	'1717' || $reagionid	==	'1722' || $reagionid	==	'1704')
		{
			$this->_data["section_regions"]  	= 	$this->inq->section_types_countries('3',$branchid,'issuecountry','',"",$charity_type_id);
		}
		else
		{
			$this->_data["section_regions"]  	= 	$this->inq->section_types_countries('3',$branchid,'issuecountry','',$reagionid,$charity_type_id);
		}
		
		$this->_data['branchid']			=	$branchid;
		$this->_data['reagionid']			=	$reagionid;
		$this->_data['charity_type']		=	$charity_type;

		$this->load->view('section-countries', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES LIST
	*/
	public function sections_regions($branchid,$reagionid,$charity_type,$country_id)
	{
		if($charity_type	==	'1699')
		{
			$charity_type_id	=	'78';
		}
		else if($charity_type	==	'1700')
		{
			$charity_type_id	=	'79';
		}
		else if($charity_type	==	'1701')
		{
			$charity_type_id	=	'80';
		}
		else
		{
			$charity_type_id	=	'81';
		}
		
		
		if($reagionid	==	'1708' || $reagionid	==	'1709' || $reagionid	==	'1717' || $reagionid	==	'1722' || $reagionid	==	'1704')
		{
			$this->_data["section_regions"]  	= 	$this->inq->section_types_regions('3',$branchid,'issuecountry',$country_id,"",$charity_type_id);
		}
		else
		{
			$this->_data["section_regions"]  	= 	$this->inq->section_types_regions('3',$branchid,'issuecountry',$country_id,$reagionid,$charity_type_id);
		}
		
		
				
		$this->_data['branchid']			=	$branchid;
		$this->_data['reagionid']			=	$reagionid;
		$this->_data['charity_type']		=	$charity_type;

		$this->load->view('section-regions', $this->_data);	
	}

//-------------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES LIST
	*/
	public function section_states($list_parent_id,$branchid,$parent_id	=	NULL,$child_id)
	{
		if($parent_id	==	'1699')
		{
			$charity_type_id	=	'78';
		}
		else if($parent_id	==	'1700')
		{
			$charity_type_id	=	'79';
		}
		else if($parent_id	==	'1701')
		{
			$charity_type_id	=	'80';
		}
		else
		{
			$charity_type_id	=	'81';
		}
		
		if($child_id	==	'1708' || $child_id	==	'1709' || $child_id	==	'1717' || $child_id	==	'1722' || $child_id	==	'1704')
		{
			$this->_data["section_states"]	=	$this->inq->section_types_sates('3',$branchid,'issuecountry',$list_parent_id,"",$charity_type_id);
		}
		else
		{
			$this->_data["section_states"]	=	$this->inq->section_types_sates('3',$branchid,'issuecountry',$list_parent_id,$child_id,$charity_type_id);
		}
		
		//echo '<pre>'; print_r($this->_data["section_states"]);exit();
		
		
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	'3';
		$this->_data['charity_type']	=	$parent_id;
		$this->_data['reagionid']		=	$child_id;
		
		$this->load->view('section-states', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES LIST
	*/
	public function section_applicants_list($wilaya,$branchid,$step,$parent_id,$child_id	=	NULL)
	{
		//$this->_data["section_states"]	=	$this->inq->section_types_sates('3',$branchid,'wilaya',$list_parent_id,$charity_type);
		
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['wilaya']			=	$wilaya;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		
		$this->load->view('section-applicants-list', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/
	public function section_applicants_list_ajax($wilaya,$branchid,$step,$parent_id,$child_id	=	NULL)
	{
		if($parent_id	==	'1699')
		{
			$charity_type_id	=	'78';
		}
		else if($parent_id	==	'1700')
		{
			$charity_type_id	=	'79';
		}
		else if($parent_id	==	'1701')
		{
			$charity_type_id	=	'80';
		}
		else
		{
			$charity_type_id	=	'81';
		}
		
		$permissions	=	$this->haya_model->check_other_permission(array('145'));
			
		//$result			=	$this->inq->section_applicants_list($wilaya,$branchid,$step,$reagionid); // Get finaldecession List 
		$result			=	$this->inq->section_applicants_list($wilaya,$branchid,$step,$child_id,$charity_type_id);
		foreach($result as $lc)
		{
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/all_detail/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			
			if($permissions[145]['u']==1) 
			{
				if($child_id	==	'1708' || $child_id	==	'1709' || $child_id	==	'1717' || $child_id	==	'1722' || $child_id	==	'1704')
				{
					$action .= '<a class="iconspace" href="'.base_url().'inquiries/edit_for_section/'.$lc->applicantid.'/'.$parent_id.'/'.$child_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				}
			}
			
			if($child_id ==	'1708' || $child_id	==	'1709' || $child_id	==	'1717' || $child_id	==	'1722' || $child_id	==	'1704')
			{
				$name	=	'<a class="iconspace" href="'.base_url().'inquiries/edit_for_section/'.$lc->applicantid.'/'.$parent_id.'/'.$child_id.'">'.$lc->fullname.'</a>';
			}
			else
			{
				$name	=	$lc->fullname;
			}
			
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم"				=>	arabic_date($lc->applicantcode),
				"الإسم"				=>	$name,
				"نوع المساعدات"		=>	$lc->charity_type,
				"فرع"				=>	$lc->branchname,
				"البطاقة الشخصة"	=>	arabic_date($lc->idcard_number),
				"المحافظة"			=>	$lc->province,
				"ولاية"				=>	$lc->wilaya,
				"الاجتماع رقم"		=>	$lc->meeting_number,
				"تاريخ التسجيل"		=>	arabic_date($lc->registrationdate),              
				"الإجرائات"			=>	$action);
				
				unset($action);
		}
		
		checkArraySize($arr);
	}
//----------------------------------------------------------------------
	/*
	* Show all records
	* @param islist string
	*/
	public function no_list_management($islist='')
	{
		$permissions	=	$this->haya_model->check_other_permission(array('122'));
		
		if($islist	!=	'')
		{			
			foreach($this->inq->get_no_list() as $lc)
			{
				if($permissions[122]['u']	==	1)
				{
					$actions =' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inquiries/add_no_list_management/'.$lc->listid.'" id="'.$lc->listid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a>&nbsp;&nbsp;';
				}
				if($permissions[122]['d']	==	1)
				{
					//$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->listid.'" data-url="'.base_url().'inquiries/delete_no_list_management/'.$lc->listid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>&nbsp;&nbsp;';
				}
			
			$arr[] = array(
				"DT_RowId"		=>	$lc->listid.'_durar_lm',
                "اسم القائمة" 	=>	$lc->listname,
				"تاريخ" 		=>	arabic_date($lc->listdate),			
				"الإجراءات" 		=>	$actions);
				
				unset($actions,$text);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
			exit();
		}
		
		$this->load->view('section-number-list',$this->_data);
	}
//-------------------------------------------------------------------------------	
	/*
	* Add No List Form
	* @param financeid integer
	*/
	public function add_no_list_management($listid=0)
	{
		if($this->input->post())
		{
			return $return	=	$this->inq->save_no_list_management();
		}
		
		$this->_data['list'] = $this->inq->get_single_no_list($listid);
		
		$this->load->view('add-no-list-form',$this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	* DELETE No List Form
	* @param financeid integer
	*/
	public function delete_no_list_management($listid)
	{	
		$this->inq->delete_no_list_management();
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function edit_for_Section($applicantid,$list_parent_id	=	NULL,$child_id	=	NULL)
	{
		if($this->input->post())
		{
			$applicantid	= $this->input->post('applicantid');
			$section_status = $this->input->post('section_status');
			$listid 		= $this->input->post('listid');
			
			if($applicantid	!=	'')
			{
				$ah_applicant['section_status']	=	$this->input->post('section_status');
				$ah_applicant['section_listid']	=	$this->input->post('listid');
				
				$this->inq->update_section_status($applicantid,$ah_applicant); // UPDATE Final dession

				// Adding data into dicission table
				$ah_applicant_section['userid'] 		=	$this->_login_userid;
				$ah_applicant_section['applicantid']	=	$this->input->post('applicantid');
				$ah_applicant_section['section_status']	=	$this->input->post('section_status');
				$ah_applicant_section['listid'] 		=	$this->input->post('listid');
				$ah_applicant_section['notes'] 			=	$this->input->post('notes');
				$ah_applicant_section['doctor_notes'] 	=	$this->input->post('doctor_notes');
				
				if($this->input->post('child_id')	==	'1708' || $this->input->post('child_id')	==	'1704')
				{
					$ah_applicant_section['list_type_amount']	=	$this->input->post('list_type_amount');
					$ah_applicant_section['list_type_notes']	=	$this->input->post('list_type_notes');
				}
				
				/*if($this->input->post('child_id')	==	'1717')
				{
					$ah_applicant_section['maintenance_type'] 	=	$this->input->post('maintenance_type');
					$ah_applicant_section['report_visit']		=	$this->input->post('report_visit');
					$ah_applicant_section['proposal_section']	=	$this->input->post('proposal_section');
					
					$fullPath = 'resources/nolist/'.$this->input->post('child_id').'/';
					
					$pictures		=	NULL;
					$certificate	=	NULL;
					
					if($_FILES["pictures"]["tmp_name"])
					{
						$ah_applicant_section['pictures']	=	$this->haya_model->upload_file('pictures',$fullPath);
					}
					
					if($_FILES["certificate"]["tmp_name"])
					{
						$ah_applicant_section['certificate']	=	$this->haya_model->upload_file('certificate',$fullPath);
					}
				}*/
				
				// ADD Final dession
				//$this->inq->add_applicant_section($ah_applicant_section); 
				
				/***************** START Reeja Code 5-4-2017 START*************************/
				$sections = array();
				$sections =	$this->inq->getdata($applicantid,'ah_applicant_sections','applicantid');
				
				
				$applicant_id	=	$this->inq->check_appid_exist($applicantid);
				
				if(isset($applicant_id))
				{
					//echo 'inside Update';
					$this->inq->update_applicant_section($applicant_id,$ah_applicant_section); // ADD Final dession
				}
				else
				{
					//echo 'inside add';
					$this->inq->add_applicant_section($ah_applicant_section); // ADD Final dession
				}
				
				/*if(count($sections)>0)
				{
					$section_id = $sections->section_id;
					$ah_applicant_section['section_id'] = $section_id;
					$this->inq->savestore($ah_applicant_section,'ah_applicant_sections','section_id');
					
				}
				else
				{
					$applicant_id	=	$this->inq->check_appid_exist($applicantid);
					
					if(isset($applicant_id))
					{
						//echo 'inside Update';
						$this->inq->update_applicant_section($ah_applicant_section); // ADD Final dession
					}
					else
					{
						//echo 'inside add';
						$this->inq->add_applicant_section($ah_applicant_section); // ADD Final dession
					}
				}*/
				
				/***************** END Reeja Code 5-4-2017 START*************************/
				
				/*$this->load->library('user_agent');
				
				if ($this->agent->is_referral())
				{
					$url	=	 $this->agent->referrer();
				}
								
				$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
				redirect($url);
				exit();*/
				
				/* New Redirect Code For Back to Previous Page*/
				$referred_from = $this->session->userdata('referred_from');
				redirect($referred_from, 'refresh');
				exit();
			}
		}
		else
		{
			$this->session->set_userdata('referred_from', $_SERVER['HTTP_REFERER']);
		}
				
		$this->_data['_applicantid']	=	$applicantid;
		$this->_data['list_parent_id']	=	$list_parent_id;
		$this->_data['child_id']		=	$child_id;	
		$this->_data['_applicant_data']	=	$this->haya_model->getRequestInfo($applicantid);
		$this->_data['all_list']		=	$this->inq->get_no_list();
		$this->_data['sakaniya_detail']	=	$this->inq->get_sakaniya_detail($applicantid);
		$this->_data['list_parent_id']	=	$list_parent_id;
		
		$this->load->view('edit_for_section', $this->_data);
	}
	
//-------------------------------------------------------------------------------
	/*
	*
	*/
	public function add_sakaniya_additional()
	{
		$ah_applicant_section['applicantid']		= 	$this->input->post('applicantid');
		$ah_applicant_section['maintenance_type'] 	=	$this->input->post('maintenance_type');
		$ah_applicant_section['report_visit']		=	$this->input->post('report_visit');
		$ah_applicant_section['proposal_section']	=	$this->input->post('proposal_section');
		$ah_applicant_section['lat']				=	$this->input->post('lat');
		$ah_applicant_section['lng']				=	$this->input->post('lng');
		
		$fullPath = 'resources/nolist/'.$this->input->post('child_id').'/';
		
		$pictures		=	NULL;
		$certificate	=	NULL;
		
		if($_FILES["pictures"]["tmp_name"])
		{
			$ah_applicant_section['pictures']	=	$this->haya_model->upload_file('pictures',$fullPath);
		}
		
		if($_FILES["certificate"]["tmp_name"])
		{
			$ah_applicant_section['certificate']	=	$this->haya_model->upload_file('certificate',$fullPath);
		}
		
		$this->inq->add_applicant_section($ah_applicant_section); // ADD Servay Detail
		
		$this->load->library('user_agent');
		
		if ($this->agent->is_referral())
		{
			$url	=	 $this->agent->referrer();
		}
						
		$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
		redirect($url);
		exit();
	}
//-------------------------------------------------------------------------------

	/*
	* functon Showing informtion according to the resule
	*/
	function all_detail($id, $type)
	{		
		switch($type)
		{
			case 'idcard_number';
				$this->_data['_applicant_data'] =	$this->haya_model->getRequestInfo($id);

				
				$applicantid	=	$this->_data['_applicant_data']['ah_applicant']->applicantid;
				$section_status	=	$this->_data['_applicant_data']['ah_applicant']->section_status;
				
				if(isset($applicantid) AND isset($section_status))
				{
					$notes								=	$this->inq->get_section_notes($applicantid,$section_status);

					$user_detail						=	$this->haya_model->get_user_detail($notes->userid);
					$this->_data['user_name']			=	$user_detail['profile']->fullname;
					$this->_data['notes']				=	$notes->notes;
					$this->_data['doctor_notes']		=	$notes->doctor_notes;
					$this->_data['submit_date']			=	$notes->submited_date;
					$this->_data['maintenance_type']	=	$notes->maintenance_type;
					$this->_data['proposal_section']	=	$notes->proposal_section;
					$this->_data['pictures']			=	$notes->pictures;
					$this->_data['certificate']			=	$notes->certificate;
					$this->_data['report_visit']		=	$notes->report_visit;
					$this->_data['list_type_amount']	=	$notes->list_type_amount;
					$this->_data['list_type_notes']		=	$notes->list_type_notes;
					
				}
					
				echo $this->load->view('idwithdetail-section', $this->_data);
			break;			
		}
	}
//---------------------------------------------------------------------

/////////////////added by reeja /////////////////////////////////////////////
	public function decission($branchid=0,$charity_type=0,$islist	=	NULL){
		
		$branchid						=	$this->_data['user_detail']['profile']->branchid;
		$this->_data['moduleparent'] 	= 224;
		$this->_data['permission'] 		= 	json_decode($this->_data['user_detail']['profile']->permissionjson,true);
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data['method_name']		=	'listalltotalusers';
		$this->_data["step"] 			=	3;
		$this->_data["done_step"]		=	4;
		$this->_data['complete_step']	=	'socilservayresultfilledlist_completesteps';
		
		$this->_data["bernamij_masadaat_types"]  =	$this->inq->bernamij_almasadaat_types('3',$branchid);
		$this->load->view('decission', $this->_data);	
	}
	public function listalltotalusers($branchid=0,$charity_type=0,$islist	=	NULL){
		
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		
		// Old Work
		//$this->load->view('finaldecission-listing', $this->_data); 
		
		// Updating New Changes from here
		if($islist	!=	'')
		{			
			foreach($this->inq->get_no_list() as $lc)
			{
				$count	=	$this->inq->get_total_users($lc->listid,$charity_type,$branchid);
				$actions =' <a  id="'.$lc->listid.'" href="'.base_url().'inquiries/listallusers/'.$branchid.'/'.$charity_type.'/'.$lc->listid.'" > ( '.$count->total.' ) </a>&nbsp;&nbsp;';

			
			$arr[] = array(
				"DT_RowId"		=>	$lc->listid.'_durar_lm',
                "اسم القائمة" 	=>	$lc->listname,
				"عدد" 			=>	$actions,
				"تاريخ" 		=>	arabic_date($lc->listdate));
				
				unset($actions,$text);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
			exit();
		}
		
		$this->load->view('listalltotalusers',$this->_data);
	
	}
	
	
	public function listallusers($branchid=0,$charity_type=0,$listid=0,$islist	=	NULL){
		
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data['listid']	=	$listid;
		if($islist	!=	'')
		{			
			
			$this->db->select('ah_applicant.*');
			$this->db->from('ah_applicant');	
			$this->db->join('ah_applicant_sections',"ah_applicant_sections.applicantid = ah_applicant.applicantid",'inner');					
			$this->db->where('charity_type_id',$charity_type);
			$this->db->where('branchid',$branchid);
			$this->db->where('ah_applicant_sections.listid',$listid);
			$this->db->where('step','4');
			$this->db->order_by('applicantid','DESC');
			$query = $this->db->get();
			//echo $this->db->last_query();
			//$result =  $this->inq->getdmessagedata($branchid,'ah_applicant','branchid','applicantid');
			foreach($query->result() as $lc)
			{
				$total =0;
				$result2 =  $this->inq->getdmessagedata($lc->applicantid,'ah_applicant_decission','applicantid');
				foreach($result2 as $lc1)
				{
					$total +=$lc1->decission_amount;
				}
				
				$meeting_number	=	$lc->meeting_number;
				$actions =' <a  id="'.$lc->applicantid.'" href="'.base_url().'inquiries/applicant_details_listing/'.$lc->applicantid.'" >'.$lc->fullname.'</a>';

			
			$arr[] = array(
				"DT_RowId"		=>	$lc->applicantid.'_durar_lm',
                "رقم الاجتماع" 	=>	$lc->meeting_number,
				"الإسم" 		=>	$actions,
				"المبلغ الإجمالي" =>	$total,
				);
				
				unset($actions,$text);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
			exit();
		}
		
		$this->load->view('listallusers', $this->_data);	
	}
	public function applicant_details_listing($applicantid,$islist	=	NULL){
		$this->_data['applicantid']	=	$applicantid;
		if($islist	!=	'')
		{			
			$result =  $this->inq->getdmessagedata($applicantid,'ah_applicant','applicantid');
			$arr = array();
			
			foreach($result as $lc)
			{
				$decision_no =0;
				$listid =0;
				$result2 =  $this->inq->getdmessagedata($lc->applicantid,'ah_applicant_sections','applicantid');
				
				if(count($result2)>0){
					foreach($result2 as $lc1)
					{
						$listid =$lc1->listid;
						$rows = $this->inq->getdata($listid,'no_list_managment','listid');
						$decision_no =$rows->listname;
					}
				}
				$notes = '';
				$rows1 = $this->inq->getdata($lc->applicantid,'ah_applicant_notes','applicantid');
				$notes = $rows1->notes;
				$notesid =$rows1->noteid;
				$province = $this->haya_model->get_name_from_list($lc-province);
				$state = $this->haya_model->get_name_from_list($lc-wilaya);
				$total =0;
				
				$result2 =  $this->inq->getdmessagedata($lc->applicantid,'ah_applicant_decission','applicantid','applicantid');
				if(count($result2)>0){
					foreach($result2 as $lc1)
					{
						$total +=$lc1->decission_amount;
					}
				}
				$notess='<a href="#" onclick="alatadad(this);" data-url="'.base_url().'inquiries/editnotes/'.$lc->applicantid.'">';
				$notess .='إضافة ملاحظات'; $notess .='</a>';
				$status ="";
				$statusnote='';
				$rows2 = $this->inq->getdata($lc->applicantid,'ah_applicant_status','applicantid');
				$status =$rows2->approval;
				
				
				if($status ==""){
					$fullname ='<a href="#" onclick="alatadad(this);" data-url="'.base_url().'inquiries/addagree/'.$lc->applicantid.'">'.htmlentities($lc->fullname).'</a>';
				}
				else{
					$fullname =htmlentities($lc->fullname);
				}
				
				if($status ==""){
					$statusnote='<img src="'.base_url().'assets/images/pending.png" style="width: 24px;" alt="Pending" title="Pending">';
				}
				else if($status =="Accepted"){
					$statusnote='<img src="'.base_url().'assets/images/completed.png" style="width: 24px;" alt="Accepted" title="Accepted">';
				}
				else if($status =="Rejected"){
					$statusnote='<img src="'.base_url().'assets/images/rejected.png" style="width: 24px;" alt="Rejected" title="Rejected">';
				}
				
			$arr[] = array(
				"DT_RowId"		=>	$lc->applicantid.'_durar_lm',
				"الإسم" 	=>	$fullname,
                "رقم الاجتماع" 	=>	htmlentities($lc->meeting_number),
				 "اسم القائمة" 	=>	htmlentities($decision_no),
				 "المنطقة" 	=>	htmlentities($province) ,
				 "ولاية" 	=>	htmlentities($state),
				 "التخصص" 	=>	htmlentities($lc->specialization),
				 "السنة لدراسية" 	=>	htmlentities($lc->year_of_study),
				 "اخر معدل تراكمي" 	=>	htmlentities($lc->last_grade_average),
				  "اسم الكلية أو الجامعة" 	=>	htmlentities($lc->college_name),
				   "مبلغ القرار" 	=>	htmlentities($total),
				    "ملاحظات" 	=>	$notess,
					"الحالة" 	=>	$statusnote
				);
				
				unset($actions,$text);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
			exit();
		}
		
		$this->load->view('applicant_details_listing', $this->_data);
	}
	public function listalluserswithstatus($approval=0,$islist	=	NULL){
		$this->_data['approval']	=$approval;
		if($approval == 1)
		 $appatatus	=	"Accepted";
		else
		$appatatus	=	"Rejected";
		
		// Old Work
		//$this->load->view('finaldecission-listing', $this->_data); 
		
		// Updating New Changes from here
		if($islist	!=	'')
		{			
			foreach($this->inq->get_no_list() as $lc)
			{
				$count	=	$this->inq->get_total_userswithStatus($lc->listid,$appatatus);
				$actions =' <a  id="'.$lc->listid.'" href="'.base_url().'inquiries/listallusersbystatus/'.$approval.'/'.$lc->listid.'" > ( '.$count->total.' ) </a>&nbsp;&nbsp;';

			
			$arr[] = array(
				"DT_RowId"		=>	$lc->listid.'_durar_lm',
                "اسم القائمة" 	=>	$lc->listname,
				"عدد" 			=>	$actions,
				"تاريخ" 		=>	arabic_date($lc->listdate));
				
				unset($actions,$text);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
			exit();
		}
		
		$this->load->view('listalluserswithstatus',$this->_data);
	
	}
	function editnotes($applicantid){
	
		$this->_data["rows"] = $this->inq->getdata($applicantid,'ah_applicant_step4_notes','applicantid');
		$this->_data["applicantid"] =$applicantid;
		$this->load->view('editnotes', $this->_data);	
	}
	function updatenotes(){
		$data["applicantid"]= $applicantid = $_POST["applicantid"];
		$data["apStepnotesId"]='';
		$data["notes"]= $notes = $_POST["notes"];
		$data["userid"]= $_POST["userid"];
		$data["curdate"]=date('Y-m-d');
		$this->inq->savestore($data,'ah_applicant_step4_notes','apStepnotesId');
	}
	public function showallstep4notes()
	{
		$applicantid = $this->input->post("applicantid");
		$myNotes = $this->inq->getdmessagedata($applicantid,'ah_applicant_step4_notes','applicantid');
		$html = "";
		
		foreach($myNotes as $mn)
		{
			$row = $this->inq->getdata($mn->userid,'ah_userprofile','userid');
			$html .= '<table width="100%" style="border:1px solid #000 !important; direction: rtl; margin-bottom:8px;" >';
			$html .= '<tr>';
			$html .= '<td style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>الموظف:</strong> '.$row->fullname.'</td>';
			/*$html .= '<td  style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>فرع:</strong> '.$mn->branchname.'</td>';*/
			$html .= '<td  style="color:#000; border-bottom: 1px solid #ccc !important; width: 33% !important; text-align: right!important; padding: 3px 10px!important; font-size: 13px !important; background-color: #CCC !important;"><strong>تاريخ:</strong> '.arabic_date($mn->curdate).'</td>';
			$html .= '</tr>';	
			$html .= '<tr>';
			$html .= '<td colspan="3" class="print_td">'.$mn->notes.'</td>';
			$html .= '</tr>';
			$html .= '</table>';
		}
		
		echo $html;
	}
	
	function addagree($applicantid){
		$this->_data["rows"] = $this->inq->getdata($applicantid,'ah_applicant_status','applicantid');
		$this->_data["applicantid"] =$applicantid;
		$this->load->view('addagree', $this->_data);
	}
	function addagreeSubmit(){
		$data["apstatusId"]=  $_POST["apstatusId"];
		$data["approval"]=  $_POST["approval"];
		$data["applicantid"]=  $_POST["applicantid"];
		$data["userid"]=$this->_login_userid;
		if(isset($_FILES["attachment"]) && count($_FILES["attachment"]) > 0)
		{
					$names = array();
					$files = $_FILES["attachment"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/inquiries');
						}
						
					}
					$data['attachment']	=	@implode(',',$names);
				}
		$data['reason']	=$_POST["reason"];
		$this->inq->savestore($data,'ah_applicant_status','apstatusId');
	}
	
	function listallusersbystatus($approval,$listid){
		
		$this->_data["approval"]=$approval;
		$this->_data["listid"]=$listid;
		$this->load->view('listallusersbystatus', $this->_data);
	}
	public function listallusersbystatus_list($approval,$listid)
	{
		$this->_data["approval"]=$approval;
		$this->_data["listid"]=$listid;
		$app ='';
		if($approval ==1){$app ='Accepted';}else{$app ='Rejected';}
		
		$permissions	=	$this->haya_model->check_other_permission(array('145'));
			
		$result	=	$this->inq->get_userdetailsbyStatus_list($app,$listid); // Get finaldecession List 
		//print_r($result);exit();
		foreach($result as $lc)
		{				
			
			//$action .= '<a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$lc->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
			
			
			
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم"				=>	arabic_date($lc->applicantcode),
				"الإسم"				=>	'<a class="iconspace" href="'.base_url().'inquiries/step4edit/'.$lc->applicantid.'">'.$lc->fullname.'</a>',
				"نوع المساعدات"		=>	$lc->charity_type,
				"فرع"				=>	$lc->branchname,
				"البطاقة الشخصة"	=>	arabic_date($lc->idcard_number),
				"المحافظة"			=>	$lc->province,
				"ولاية"				=>	$lc->wilaya,
				"الاجتماع رقم"		=>	$lc->meeting_number,
				"تاريخ التسجيل"		=>	arabic_date($lc->registrationdate));
				//, "الإجرائات"			=>	$action
				unset($action);
		}
		
		checkArraySize($arr);
	}
	function step4edit($applicantid){
		$this->_data["applicantid"]= $applicantid;
		
		//$this->haya_model->check_permission($this->_data['module'],'v');
		
		
				
		$this->_data['_applicantid']	= $applicantid;			
		$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($applicantid);
	
		
		
		$this->load->view('step4edit', $this->_data);
	}
	
	
	function afteraddagreement($applicantid,$appRewMid,$labels=NULL){
		
		$this->_data['labels']	=$labels;
		$this->_data['applicantid']	=	$applicantid;
		$this->_data['appRewMid']	=	$appRewMid;
		
		$this->_data["notes"] =  $this->inq->getdmessagedata($appRewMid,'ah_applicant_renewal_sub','appRewMid');
		$this->_data["main"] =  $this->inq->getdata($appRewMid,'ah_applicant_renewal_main','appRewMid');
		if(isset($_POST["submit"])){
			if($_POST["submit"] == "Renew"){
				$data= array();
				
				$chkoption =$_POST["chkoption"];
				$data["applicantid"]= $applicantid;
				$data["userid"]= $this->_login_userid;
				$data["conditions"]= $chkoption;
				$data["condition_text"]= $_POST["reason".$chkoption];
				if($appRewMid == 0){$appRewMid ="";}
				$data["appRewMid"]=$appRewMid;
				$data["curdate"]=date('Y-m-d');
				if(isset($_FILES["attachment".$chkoption]) && count($_FILES["attachment".$chkoption]) > 0)
				{
						$names = array();
					$files = $_FILES["attachment".$chkoption];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/inquiries');
						}
						
					}
					$data['condition_attachment']	=	@implode(',',$names);
				}
				$appRewMid =$this->inq->savestore($data,'ah_applicant_renewal_main','appRewMid');	
			}
			else if($_POST["submit"] == "Transfer"){
				$data= array();
				
				$data["conditon_transfer"]= $_POST["conditon_transfer"];
				$data["applicantid"]= $applicantid;
				$data["userid"]= $this->_login_userid;
				$data["conditions"]= $chkoption;
				$data["condition_text"]= $_POST["reason".$chkoption];
				if($appRewMid == 0){$appRewMid ="";}
				$data["appRewMid"]=$appRewMid;
				$data["curdate"]=date('Y-m-d');
				$appRewMid =$this->inq->savestore($data,'ah_applicant_renewal_main','appRewMid');
				$dt["applicantid"] =$applicantid;	
				$dt["step"] =3;
					$this->inq->savestore($dt,'ah_applicant','applicantid');
			}
		}//if(isset($_POST["submit"])){
		
			
			
		
		$this->load->view('afteraddagreement', $this->_data);
	
	}
	function addrenewdata($applicantid,$appRewMid = NULL){
		$this->_data["applicantid"] = $applicantid;
		$this->_data["appRewMid"] = $appRewMid;
		if(isset($_POST["currentdate"])){
			
			if($_POST["appRewMid"] <=0 or $_POST["appRewMid"] == ""){
				$data1= array();
				$data1["applicantid"]=$_POST["applicantid"];
				$data1["userid"]=$_POST["userid"];
				$appRewMid = $this->inq->savestore($data1,'ah_applicant_renewal_main','appRewMid');	
			}
			else{$appRewMid = $_POST["appRewMid"];}
			$data = array();
			$data["currentdate"]=$_POST["currentdate"];
			$data["years"]= $_POST["years"];
			$data["notes"]= $_POST["notes"];
			$data["degree"]= $_POST["degree"];
			$data["cumulative"]= $_POST["cumulative"];
			$data["degree_num"]= $_POST["degree_num"];
			$data["notes2"]= $_POST["notes2"];
			$data["appRewMid"]= $appRewMid;
			$data["userid"]=$_POST["userid"];
			$data["applicantid"]= $_POST["applicantid"];
				if(isset($_FILES["attachment1"]["name"]) )
				{
						$names = array();
					$files = $_FILES["attachment1"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/inquiries');
						}
						
					}
					$data['attachment1']	=	@implode(',',$names);
				}	
				//print_r($_FILES);exit();
				if(isset($_FILES["attachment2"]["name"]))
				{
						$names = array();
					$files = $_FILES["attachment2"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/inquiries');
						}
						
					}
					$data['attachment2']	=	@implode(',',$names);
				}
				$data["appRewSubid"] ='';
				$this->inq->savestore($data,'ah_applicant_renewal_sub','appRewSubid');
				$ex["appRewMid"]=$appRewMid;
				echo json_encode($ex);	
				exit();
		}
		
		$this->load->view('addrenewdata', $this->_data);
	}
	
	
	
	
	function getfileicon($filename,$path,$title= NULL)
	{
		$filename = strtolower($filename);
		$file_extension = strrchr($filename, ".");	
	
		$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_nd.png" style="width:30px; height:30px" alt="image" title="image" oldtitle="'.$title.'" /></a>';
	
		if($file_extension=='.doc' || $file_extension=='.docx')
	
			 $rettype =' <a class="fancybox-button" rel="gallery1" href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_doc.png" style="width:30px; height:30px" alt="'.$title.'" title="'.$title.'" oldtitle="'.$title.'"/></a>';
	
		if($file_extension=='.xls' || $file_extension=='.xlsx' || $file_extension=='.csv' || $file_extension=='.xlt' || $file_extension=='.xltx')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_xls.png" style="width:30px; height:30px" alt="'.$title.'" title="'.$title.'" oldtitle="'.$title.'"/></a>';
	
		if($file_extension=='.pdf')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_pdf.png" style="width:30px; height:30px" alt="'.$title.'" title="'.$title.'" oldtitle="'.$title.'"/></a>';
	
		if($file_extension=='.jpg' || $file_extension=='.jpeg'  || 	$file_extension=='.gif')
	
			$rettype=' <a class="fancybox-button" rel="gallery1" href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_jpg.png" style="width:30px; height:30px" alt="'.$title.'" title="'.$title.'" oldtitle="'.$title.'"/></a>';
	
		if($file_extension=='.png' )
	
			$rettype=' <a class="fancybox-button" rel="gallery1" href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_png.png" style="width:30px; height:30px" alt="'.$title.'" title="'.$title.'" oldtitle="'.$title.'"/></a>';
	
		if($file_extension=='.ppt' || $file_extension=='.pptx')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_ppt.png" style="width:30px; height:30px" alt="'.$title.'" title="'.$title.'" oldtitle="'.$title.'"/></a>';		
	
		if($file_extension=='.zip' || $file_extension=='.tz' || $file_extension=='.rar' || $file_extension=='.gzip')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_zip.png" style="width:30px; height:30px" alt="'.$title.'" title="'.$title.'" oldtitle="'.$title.'"/></a>';		
	
		if($file_extension=='.txt' || $file_extension=='.rtf')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_txt.png" style="width:30px; height:30px" alt="'.$title.'" title="'.$title.'" oldtitle="'.$title.'"/></a>';
	
		return $rettype;
	}

//////////////////////////////////////////////////////////////////////////////

	function listpopulationview($applicantid)
	{
		//$this->haya_model->check_permission($this->_data['module'],'v');		
		$this->_data['_applicantid']	= $applicantid;			
		$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($applicantid);
		$this->_data['quotation'] 		=	$this->inq->getdmessagedata($applicantid,'ah_applicant_quotations','applicantid');	
		if($_POST["applicantid"] !="")
		{
			echo '<pre>'; print_r($data = $this->input->post());

			$data = $this->input->post();
			
			$dbdata["quotationsId"]='';
			
			if(count($data["ids"])>0)
			{
				$ids						=	$data["ids"];
				$dbdata["applicantid"]		=	$applicantid =  $data["applicantid"];
				$dbdata["quotationsDataId"]	=	'';
				
				/*    Reeja Work Bfore*/
/*				foreach($ids as $id)
				{
					if(isset($data["approved"][$id]))
					{
						if($dbdata["quotationsId"]	==	'')
						{
							$dbdata["quotationsId"]	= $id;
						}
						else
						{
							$dbdata["quotationsId"] .= ','.$id;
						}
					}
				}*/
				
				// Muhammad Muzaffar Changed
				foreach($ids as $id)
				{
					if(isset($data["approved"]) AND $data["approved"]	==	$id)
					{
						if($dbdata["quotationsId"]	==	'')
						{
							$dbdata["quotationsId"]	= $id;
						}
						else
						{
							$dbdata["quotationsId"] .= ','.$id;
						}
					}
				}
				
				$rows = $this->inq->getdata($applicantid,'ah_applicant_quotations_data',"applicantid");
				
				if(count($rows)>0)
				{
					$dbdata["quotationsDataId"]= $rows->quotationsDataId;	
				}
				
				$dbdata["userid"] = $this->_login_userid;
				$quotationsDataId = $this->inq->savestore($dbdata,'ah_applicant_quotations_data','quotationsDataId');
				
				$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
				redirect(base_url().'inquiries/createcontract/'.$applicantid.'/'.$quotationsDataId);
				exit();
			}
		}
		
		$this->load->view('listpopulationview',$this->_data);
	}
	function addquotation($applicantid,$quotationsId = NULL){
		
		$this->_data['applicantid']	= $applicantid;	
		$this->_data['quotationsId']	= $quotationsId;
		//print_r($_POST);
		if($_POST["contractName"] !="")
		{
			//print_r($_POST);exit();
			$data = $this->input->post();
			$services["userid"] = $this->_login_userid;
			$services["applicantid"] = $data["applicantid"];
			$services["contractName"] = $data["contractName"];
			$services["company"] = $data["company"];
			$services["phone"] = $data["phone"];
			$services["address"] = $data["address"];
			$services["mobile"] = $data["mobile"];
			$services["fax"] = $data["fax"];
			$services["email"] = $data["email"];
			$services["notes"] = $data["notes"];
			
			
			
			
			
			/* ------------------------*/
  			  /*Reeja Soni 22-5-17 start*/
			
			$services["total"] = $data["total"];
			
			/* ------------------------*/
  			  /*Reeja Soni 22-5-17 end*/
			
			
			
			
			
			
			
			if($data["quotationsId"] >0){
				$services["quotationsId"] = $data["quotationsId"];
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
					$names = array();
					$files = $_FILES["attachment"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/inquiries');
						}
					}
					
					$services['attachment']	=	@implode(',',$names);
				}
			}
			else{
				$services["quotationsId"] = '';
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
				$names = array();
				$files = $_FILES["attachment"];
				 foreach ($files['name'] as $key => $image) {
					if(!empty($files['name'][$key]))
					{
						$_FILES['images']['name']= $files['name'][$key];
						$_FILES['images']['type']= $files['type'][$key];
						$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['images']['error']= $files['error'][$key];
						$_FILES['images']['size']= $files['size'][$key];
						$names[] =	$this->upload_file($this->_login_userid,'images','resources/inquiries');
					}
					
				}
				$services['attachment']	=	@implode(',',$names);
			}
			}
			
			 $this->inq->savestore($services,'ah_applicant_quotations','quotationsId');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			//echo json_encode($ex);
			//exit();
		}
		if($quotationsId >0){
			$this->_data["rows"] = $this->inq->getdata($quotationsId,'ah_applicant_quotations',"quotationsId");
		}
			
		$this->load->view('addquotation',$this->_data);
	}
//----------------------------------------------------------------
	/*
	*
	*
	*/
	function listquotation()
	{
		$applicantid = $_POST["applicantid"];
		
		$quotation =	$this->inq->getdmessagedata($applicantid,'ah_applicant_quotations','applicantid');	
		
			echo '<table class="table table-bordered table-striped dataTable">
              <thead>
                <tr role="row">
                  <th width="10%">اسم المقاول</th>
                  <th width="10%">اسم الشركة</th>
                  <th width="10%">العنوان</th>
                  <th width="10%">الهاتف</th>
                  <th width="10%">النقال</th>
                  <th width="10%">الفاكس</th>
                  <th width="10%">البريد الاكتروني</th>
                  <th width="10%">الملاحظات</th>
                   <th width="10%">أرفاق عروض الاسعار</th>
				   <th width="10%">	الإجراءات</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">';
			  if(count($quotation)>0){
			  	foreach($quotation as $apr) { 
               echo '<tr class="familycount">
                    <td>'.$apr->contractName.'</td>
                    <td>'.$apr->company.'</td>
                    <td>'.$apr->address.'</td>
                    <td>'.$apr->phone.'</td>
                    <td>'.$apr->mobile.'</td>
                    <td>'.$apr->fax.'</td>
                    <td>'.$apr->email.'</td>
                    <td>'.$apr->notes.'</td>
                    <td>';
					 if( $apr->attachment !=""){
					$images=@explode(",",$apr->attachment);	
						foreach($images as $img){
							echo $this->getfileicon($img,base_url().'resources/inquiries/'.$this->_login_userid);
						}
					}
					echo '</td>
					<td><a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'inquiries/addquotation/'.$applicantid.'/'.$apr->quotationsId.'" style="margin-left:5px;"><i class="icon-pencil"></i></a>
					<a class="iconspace" href="#deleteDiag" onclick="show_delete_diag(this);" id="" data-url="'.base_url().'inquiries/deletequotation/'.$apr->quotationsId
					.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>
					<a onclick="alatadad(this);" data-url="'.base_url().'inquiries/viewquotation/'.$apr->quotationsId.'" href="#"><i class="my icon icon-eye-open" data-hasqtip="31" aria-describedby="qtip-31"></i></a>
					</td>
                  
                  
                </tr>';
                }
			  }
             echo ' </tbody>
            </table>';
		
	}
//------------------------------------------------------------
	/*
	*
	*
	*/
	function viewquotation($quotationsId)
	{
		$this->_data["rows"] = $this->inq->getdata($quotationsId,'ah_applicant_quotations',"quotationsId");
		
		$this->load->view('viewquotation',$this->_data);	
	}
//------------------------------------------------------------
	/*
	*
	*
	*/
	function deletequotation($quotationsId)
	{
		$quotation =	$this->inq->getdata($quotationsId,'ah_applicant_quotations','quotationsId');	
		 if( $quotation->attachment !="")
		 {
			$images=@explode(",",$quotation->attachment);	
			foreach($images as $img)
			{
				@unlink('resources/inquiries/'.$img);
			}
		 }
		 
		$this->inq->delete($quotationsId,'quotationsId','ah_applicant_quotations');
		exit();
	}
	
	/*function createcontract($applicantid,$quotationsDataId)
	{
		$this->_data['applicantid']			=	$applicantid;	
		$this->_data['quotationsDataId']	=	$quotationsDataId;
		
		if($_POST["geographical"] !="")
		{
			$data = $this->input->post();
			
			$services["userid"] = $this->_login_userid;
			$services["applicantid"] = $data["applicantid"];
			$services["geographical"] = $data["geographical"];
			$services["quotationsDataId"] = $data["quotationsDataId"];
			$services["financialdetails"] = $data["financialdetails"];
			$services["timelydetails"] = $data["timelydetails"];
			$services["detailsoftheactions"] = $data["detailsoftheactions"];
			$services["additionalWorks"] = $data["additionalWorks"];
			
			if($data["quotationsDataId"] >0)
			{
				$services["quotationsDataId"] = $data["quotationsDataId"];
				
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
					$names = array();
					$files = $_FILES["attachment"];
					 foreach ($files['name'] as $key => $image)
					 {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/inquiries');
						}
					}
					
					$services['attachment']	=	@implode(',',$names);
				}
			}
			
			
			$this->inq->savestore($services,'ah_applicant_quotations_data','quotationsDataId');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			redirect(base_url().'inquiries/createcontract/'.$applicantid.'/'.$quotationsDataId);
			exit();
		}
		
		$this->_data['rows'] =	$this->inq->getdata($quotationsDataId,'ah_applicant_quotations_data','quotationsDataId');		
		$this->_data['quotation'] =	$this->inq->getdmessagedata($applicantid,'ah_applicant_quotations','applicantid');
		$this->_data['application'] =	$this->inq->getdata($applicantid,'ah_applicant','applicantid');
		
		$this->load->view('createcontract',$this->_data);

	}*/
	
	/***************************************/
	    /*******29-05-2017********/
	/***************************************/
	function createcontract($applicantid,$quotationsDataId)
	{
		$this->_data['applicantid']			=	$applicantid;	
		$this->_data['quotationsDataId']	=	$quotationsDataId;
		
		if($_POST["beneficiary1"] !="")
		{
			$data = $this->input->post();
			
			$data["userid"] =	$this->_login_userid;
			$data["date"]	=	date('Y-m-d');
			
			if($data["id"] <=0 || $data["id"] == "")
			{ 
				$data["id"]	=	'';
			}
			
			if($_FILES["document"]["tmp_name"])
			{
				// Uplaod  Document
				$document			=	$this->upload_file($this->_login_userid,'document','resources/applicants'); 
				$data['document']	=	$document;
			}
			
			$this->inq->delete_percentage_amount($this->_data['applicantid']);
			
				$total_divs	=	count($data['percentage']);
				for($i	=	1;$i	<=	$total_divs;	$i++)
				{
					$multifields	=	array(
										'applicantid'	=>	$this->_data['applicantid'],
										'percentage'	=>	$data['percentage'][$i],
										'amount'		=>	$data['amount'][$i],
										'notes'			=>	$data['notes'][$i],
										'index_num'		=>	$i
										);
			
					$this->inq->add_multifields($multifields); // Add Multi Fields Detail
					
				}
				
			unset($data['percentage'],$data['amount'],$data['notes']);
			
			
			$data['quotationsId']	=	$this->inq->get_quatation_detail_muzi($data['quotationsDataId'],$this->_data['applicantid']);
			
			$this->inq->savestore($data,'ah_applicant_createcontract','id');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			redirect(base_url().'inquiries/createcontract/'.$applicantid.'/'.$quotationsDataId);
			exit();
		}
		
		
		$this->_data['rowss'] 			=	$this->inq->getdata($applicantid,'ah_applicant_createcontract','applicantid');

		$this->_data['rows'] 			=	$this->inq->getdata($quotationsDataId,'ah_applicant_quotations_data','quotationsDataId');
		$this->_data['quotation'] 		=	$this->inq->getdmessagedata($applicantid,'ah_applicant_quotations','applicantid');
		$this->_data['application'] 	=	$this->inq->getdata($applicantid,'ah_applicant','applicantid');
		
		$this->_data['percent_amounts'] =	$this->inq->get_all_percentage_amount($applicantid);
		
		$this->_data['quotation_info'] 		=	$this->inq->get_quotation_info($applicantid,'ah_applicant_quotations','applicantid');

		$this->load->view('createcontract',$this->_data);

	}
	
	function quotations_maintenance(){
		$applicantid= $_POST["applicantid"];
		$quotation =	$this->inq->getdmessagedata($applicantid,'ah_applicant_quotations_maintenance','applicantid');
		
		echo '<table class="table table-bordered table-striped ">
                <thead style="background-color: #029625;" >
                  <tr role="row">
                    <th>أسم شركة المقاولات</th>
                     <th>هاتف</th>
                    <th>العنوان</th>
                    <th>ملاحظات</th>
                    <th>مدة تنفيذ الاعمال المعتمدة</th>
                    <th>تبدا من تاريخ</th>
                    <th>قيمة مساهمة الهيئة</th>
                    <th>تنتهي بتاريخ</th>
                    <th>أرفاق أمر تشغيل لبدء عمل الصيانة </th>
					<th >	الإجراءات</th>
                  </tr>
                </thead>
                <tbody role="alert" aria-live="polite" aria-relevant="all">';
				if(count($quotation)>0){
					foreach($quotation as $qt)
					{
						echo '<tr>';
						echo '<td>'.$qt->company.'</td>';
						echo '<td>'.$qt->phone.'</td>';
						echo '<td>'.$qt->address.'</td>';
						echo '<td>'.$qt->notes.'</td>';
						echo '<td>'.$qt->duration.'</td>';
						echo '<td>'.$qt->startdate.'</td>';
						echo '<td>'.$qt->contribution.'</td>';
						echo '<td>'.$qt->expires.'</td>';
						echo '<td>';
						 if( $qt->attachment !=""){
					$images	=	@explode(",",$qt->attachment);	
						foreach($images as $img){
							echo $this->getfileicon($img,base_url().'resources/inquiries/'.$this->_login_userid);
						}
					}
						echo'</td>';
						echo '</td>
					<td><a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'inquiries/addquotations_maintenance/'.$applicantid.'/'.$qt->quotationsDataId.'/'.$qt->maintananceId.'" style="margin-left:5px;"><i class="icon-pencil"></i></a>
					<a class="iconspace" href="#deleteDiag" onclick="show_delete_diag(this);" id="" data-url="'.base_url().'inquiries/delete_maintenance/'.$qt->maintananceId
					.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>
					<a onclick="alatadad(this);" data-url="'.base_url().'inquiries/viewmaintenance/'.$qt->maintananceId.'" href="#"><i class="my icon icon-eye-open" data-hasqtip="31" aria-describedby="qtip-31"></i></a>
					</td>';
						echo '</tr>';
					}
				}
                 echo '  </tbody>
              </table>';
		
	}
	function viewmaintenance($maintananceId){
		$this->_data["rows"] = $this->inq->getdata($maintananceId,'ah_applicant_quotations_maintenance',"maintananceId");
		$this->load->view('viewmaintenance',$this->_data);	
	}
	function addquotations_maintenance($applicantid,$quotationsDataId, $maintananceId=NULL){
		$this->_data['applicantid']	= $applicantid;	
		$this->_data['quotationsDataId']	= $quotationsDataId;
		$this->_data['maintananceId']	= $maintananceId;	
		$this->_data['quotation'] =	$this->inq->getdmessagedata($applicantid,'ah_applicant_quotations','applicantid');	
		if($_POST["company"] !="")
		{
			$data = $this->input->post();
			$services["userid"] 			=	$this->_login_userid;
			$services["applicantid"] 		=	$data["applicantid"];
			$services["quotationsDataId"] 	=	$data["quotationsDataId"];
			$services["quotationsId"] 		=	$data["quotationsId"];
			$services["company"] 			=	$data["company"];
			$services["phone"] 				=	$data["phone"];
			$services["address"] 			=	$data["address"];
			$services["duration"] 			=	$data["duration"];
			$services["startdate"] 			=	$data["startdate"];
			$services["contribution"] 		=	$data["contribution"];
			$services["expires"] 			=	$data["expires"];
			$services["notes"] 				=	$data["notes"];
			
			if($data["maintananceId"] >0){
				$services["maintananceId"] = $data["maintananceId"];
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
					$names = array();
					$files = $_FILES["attachment"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/inquiries');
						}
						
					}
					$services['attachment']	=	@implode(',',$names);
				}
			}
			else{
				$services["maintananceId"] = '';
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
				$names = array();
				$files = $_FILES["attachment"];
				 foreach ($files['name'] as $key => $image) {
					if(!empty($files['name'][$key]))
					{
						$_FILES['images']['name']= $files['name'][$key];
						$_FILES['images']['type']= $files['type'][$key];
						$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['images']['error']= $files['error'][$key];
						$_FILES['images']['size']= $files['size'][$key];
						$names[] =	$this->upload_file($this->_login_userid,'images','resources/inquiries');
					}
					
				}
				$services['attachment']	=	@implode(',',$names);
			}
			}
			
			 $this->inq->savestore($services,'ah_applicant_quotations_maintenance','maintananceId');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			//echo json_encode($ex);
			//exit();
		}
		
		
		if($maintananceId >0){
			$this->_data["rows"] = $this->inq->getdata($maintananceId,'ah_applicant_quotations_maintenance',"maintananceId");
		}	
		$this->load->view('addquotations_maintenance',$this->_data);
	}
	function delete_maintenance($maintananceId){
		$quotation =	$this->inq->getdata($maintananceId,'ah_applicant_quotations_maintenance','maintananceId');	
		 if( $quotation->attachment !=""){
			$images=@explode(",",$quotation->attachment);	
				foreach($images as $img){
					@unlink('resources/inquiries/'.$img);
				}
			}
		$this->inq->delete($maintananceId,'maintananceId','ah_applicant_quotations_maintenance');
		exit();
	}

/*------------------------
Reeja Soni 22-5-17 start*/
	public function listallpopulationcount($branchid=0,$charity_type=0,$application_status='0',$islist	=	NULL){
		
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data['application_status']	=	$application_status;
		// Old Work
		//$this->load->view('finaldecission-listing', $this->_data); 
		
		// Updating New Changes from here
		if($application_status ==1){$application_status ="موافق";}
		else if($application_status ==2){$application_status ="رفض";}
		else if($application_status ==3){$application_status ="تحويل";}
		if($islist	!=	'')
		{			
			foreach($this->inq->get_no_list() as $lc)
			{
				$count	=	$this->inq->get_total_users($lc->listid,$charity_type,$branchid,$application_status);
				$actions =' <a  id="'.$lc->listid.'" href="'.base_url().'inquiries/listallpopulation/'.$branchid.'/'.$charity_type.'/'.$lc->listid.'" > ( '.$count->total.' ) </a>&nbsp;&nbsp;';

			
			$arr[] = array(
				"DT_RowId"		=>	$lc->listid.'_durar_lm',
                "اسم القائمة" 	=>	$lc->listname,
				"عدد" 			=>	$actions,
				"تاريخ" 		=>	arabic_date($lc->listdate));
				
				unset($actions,$text);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
			exit();
		}
		
		$this->load->view('listallpopulationcount',$this->_data);
	}
/*------------------------
Reeja Soni 22-5-17 end*/

/* ------------------------*/
/*Reeja Soni 22-5-17 start*/
	
	/*public function update_thirdparty()
	{
		echo "1"; exit();
		$data["quotationsId"] = $_POST["quotationsId"];
		$data["id"] = $_POST["id"];
		$data["funded"] = $_POST["funded"];
		$data["address"] = $_POST["address"];
		$data["ContactData"] = $_POST["ContactData"];
		 $this->inq->savestore($data,'ah_applicant_thirdparty','id');
		echo "1";
		
	}*/
/*Reeja Soni 25-5-17 start*/
	public function update_thirdparty(){
		//echo "1"; exit();
		$data["quotationsId"] = $_POST["quotationsId"];
		if($_POST["id"] ==0){$data["id"] ='';}else{$data["id"] = $_POST["id"];}
		$data["funded"] = $_POST["funded"];
		$data["address"] = $_POST["address"];
		$data["contactData"] = $_POST["ContactData"];
		$id = $this->inq->savestore($data,'ah_applicant_thirdparty','id');
		 
		echo $id;
		
	}
/* ------------------------*/
/*Reeja Soni 22-5-17 end*/
	
/*************************reeja 2-4-17************************************************/
/*function listallpopulation($branchid=0,$charity_type=0,$listid=0,$islist	=	NULL){
		
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
			$this->_data['listid']	=	$listid;
		
		//print_r($result);exit();
		if($islist	!=	'')
		{	
			$result =$this->inq->listallpopulation($branchid,$charity_type,$listid);	
			foreach($result as $lc)
			{
				$actions =' <a  id="'.$lc->applicantid.'" href="'.base_url().'inquiries/listpopulationview/'.$lc->applicantid.'" > '.$lc->fullname.'</a>&nbsp;&nbsp;';
				$province = $this->haya_model->get_name_from_list($lc-province);
				$maintenance_type = $this->haya_model->get_name_from_list($lc-maintenance_type);
			
			$arr[] = array(
				"DT_RowId"		=>	$lc->applicantid.'_durar_lm',
                "الاسم" 			=>	$actions,
				"رقم الاجتماع" 			=>	$lc->listname,
				"الولاية" 		=>	$province,
				"الرقم المدني" 		=>	$lc->idcard_number,
				"نوع الصيانة" 		=>	$maintenance_type,
				"قرار اللجنة" 		=>	$lc->meeting_notes,
				"قيمة المساعدة" 		=>	$lc->decission_amount,
				"رقم الهاتف" 		=>	arabic_date(implode('<br>',json_decode($lc->extratelephone,TRUE)))
				
				);
				
				unset($actions,$text);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
			exit();
		}
		
		$this->load->view('listallpopulation',$this->_data);
	
	}*/
/*************************STRAT REEJA 05-04-2017 START************************************************/
	function viewfinalsection($applicantid)
	{
		$this->_data['_applicantid']			= $applicantid;			
		$this->_data['_applicant_data'] 		= $this->haya_model->getRequestInfo($applicantid);
		$this->_data['quotation'] 				=	$this->inq->getdmessagedata($applicantid,'ah_applicant_quotations','applicantid');
		$this->_data['ah_maintananceapplicant']	=	$this->inq->getdata($applicantid,'ah_applicant_quotations_data','applicantid');	
		$this->_data['tabledata'] 				=	$this->inq->getdmessagedata($applicantid,'ah_applicant_quotations_maintenance','applicantid');
		
		$this->load->view('viewfinalsection',$this->_data);
	}

//--------------------------------------------------------------------------------
	function listallpopulation($branchid=0,$charity_type=0,$listid=0,$islist	=	NULL)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data['listid']			=	$listid;

		if($islist	!=	'')
		{	
			$result =$this->inq->listallpopulation($branchid,$charity_type,$listid);
				
			foreach($result as $lc)
			{
				$contract_detail	=	count($this->inq->getdata($lc->applicantid,'ah_applicant_createcontract','applicantid'));
				$actions 			=	' <a  id="'.$lc->applicantid.'" href="'.base_url().'inquiries/listpopulationview/'.$lc->applicantid.'" > '.$lc->fullname.'</a>&nbsp;&nbsp;';
				$province 			=	$this->haya_model->get_name_from_list($lc-province);
				$maintenance_type 	=	$this->haya_model->get_name_from_list($lc-maintenance_type);
				$view				=	'<a href="#" onclick="alatadad(this);" data-url="'.base_url().'inquiries/viewfinalsection/'.$lc->applicantid.'"><i class="icon-eye-open" data-hasqtip="33" aria-describedby="qtip-33"></i></a> ';
				if($contract_detail	>	0)
				{
				$view	.=	' <a href="#" onclick="alatadad(this);" data-url="'.base_url().'inquiries/proceed_contract/'.$lc->applicantid.'"><i class="myicon icon-random"></i></a>';
				}
				$arr[] = array(
					"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
					"الاسم" 				=>	$actions,
					"رقم الاجتماع" 		=>	$lc->listname,
					"الولاية" 			=>	$province,
					"الرقم المدني" 		=>	$lc->idcard_number,
					"نوع الصيانة" 		=>	$maintenance_type,
					"قرار اللجنة" 		=>	$lc->meeting_notes,
					"قيمة المساعدة" 	=>	$lc->decission_amount,
					"رقم الهاتف" 		=>	arabic_date(implode('<br>',json_decode($lc->extratelephone,TRUE))),
					"الإجرائات" 			=>	$view
					);
				
				unset($actions,$text);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
			exit();
		}
		
		$this->load->view('listallpopulation',$this->_data);
	}
//------------------------------------------------------------------------------
	/*
	*
	*
	*/
	public function modify_datatable($index,$value)
	{
		$this->_data['id']		=	$index;
		$this->_data['value']	=	$value;
		
		$json_response	=	$this->getColoumnsForDataTable($tableName	=	'ah_applicants');
		
		// JSON Decode 
		$data	=	json_decode($json_response);
		$main	=	$data->info->first_Record;
	}
//------------------------------------------------------------------------------	
/*************************END REEJA 05-04-2017 END************************************************/


//-----------------------------------------------------------------------
	/*
	* This Function For see all applicants directly
	*
	*/
	public function get_all_applicants_by_status($branchid,$list_id,$charity_type)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['list_id']			=	$list_id;
		$this->_data['charity_type']	=	$charity_type;
		
		$this->load->view('applicants-listing-by-status',$this->_data);	
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function ajax_get_all_applicants_by_status($branchid,$list_id,$charity_type)
	{
		$step	=	'3';
		
		if($charity_type	==	'1699')
		{
			$charity_type_id	=	'79';
		}
		else if($charity_type	==	'1700')
		{
			$charity_type_id	=	'79';
		}
		else if($charity_type	==	'1701')
		{
			$charity_type_id	=	'80';
		}
		else
		{
			$charity_type_id	=	'81';
		}
		
		$all_applicants	=	$this->inq->get_all_applicants_by_status($branchid,$step,$list_id,$charity_type_id);
		

		foreach($all_applicants as $lc)
		{
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/all_detail/'.$lc->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
			
			if($permissions[145]['u']==1) 
			{
				if($child_id	==	'1708' || $child_id	==	'1709' || $child_id	==	'1717' || $child_id	==	'1722' || $child_id	==	'1704')
				{
					$action .= '<a class="iconspace" href="'.base_url().'inquiries/edit_for_section/'.$lc->applicantid.'/'.$parent_id.'/'.$child_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				}
			}
			
			if($child_id ==	'1708' || $child_id	==	'1709' || $child_id	==	'1717' || $child_id	==	'1722' || $child_id	==	'1704')
			{
				$name	=	'<a class="iconspace" href="'.base_url().'inquiries/edit_for_section/'.$lc->applicantid.'/'.$parent_id.'/'.$child_id.'">'.$lc->fullname.'</a>';
			}
			else
			{
				$name	=	$lc->fullname;
			}
			
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم"				=>	arabic_date($lc->applicantcode),
				"الإسم"				=>	$name,
				"نوع المساعدات"		=>	$lc->charity_type,
				"فرع"				=>	$lc->branchname,
				"البطاقة الشخصة"	=>	arabic_date($lc->idcard_number),
				"المحافظة"			=>	$lc->province,
				"ولاية"				=>	$lc->wilaya,
				"الاجتماع رقم"		=>	$lc->meeting_number,
				"تاريخ التسجيل"		=>	arabic_date($lc->registrationdate),              
				"الإجرائات"			=>	$action);
				
				unset($action);
		}
		
		checkArraySize($arr);
	}
//--------------------------------------------------------------------------------
	/*
	* Get all detail about CONTRACT
	* @param $applicationid integer
	*/
	public function get_contract_detail($applicantid	=	NULL)
	{
		$this->_data['contract_detail']	=	$this->inq->get_contract_detail($applicantid);
		
		$this->load->view('full-contract-detail',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	* Add Multifields into Country Registration Form
	*
	*/
	
	function add_multifields()
	{
		$this->_data['total_divs']	=	$this->input->post('total_divs'); // Total Counts of Multi fields
		
		echo $html	=	$this->load->view('add_multifields',$this->_data,TRUE); // Multifields form
	}
//-----------------------------------------------------------------------

	/*
	*
	* GET CONTRACT detail
	* @param @quotationsId integer
	* return OBJECT
	*/
	public function proceed_contract($applicantid)
	{
		if($this->input->post())
		{
			$data['contract_notes']	=	$this->input->post('contract_notes');
			$data['contract_ready']	=	1;
			$data['approve_amount']	=	$this->input->post('approve_amount');
			
			if($_FILES["contract_document"]["tmp_name"])
			{
				// Uplaod  Document
				$document			=	$this->upload_file($this->_login_userid,'contract_document','resources/applicants'); 
				$data['contract_document']	=	$document;
			}
			
			$this->inq->update_contract_status($this->input->post('applicantid'),$data);
			
			$this->load->library('user_agent');
			if ($this->agent->is_referral())
			{
				$url	=	 $this->agent->referrer();
			}
							
			$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
			redirect($url);
			exit();
		}
		
		$this->_data['applicantid']		=	$applicantid;
		
		$this->_data['percent_amounts']	=	$this->inq->get_all_percentage_amount($applicantid);
		$this->_data['applicant_data']	=	$this->inq->get_applicant_data($applicantid);
		
		$this->load->view('proceed-contract-detail',$this->_data); // Multifields form
	}

//------------------------------------------------------------------------------
}
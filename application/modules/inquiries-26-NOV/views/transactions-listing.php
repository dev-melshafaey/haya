<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <?php $this->load->view('common/globalfilter', array('type'=>'help','b'=>$branchid,'c'=>$charity_type));?>

        <div class="nav nav-tabs panel panel-default panel-block">
          <div class="tab-pane list-group active">           
            <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
              <thead>
                <tr>
                  <!--<th style="display:none !important;">رقم المعاملة</th>-->
                  <th style="width:300px !important; text-align:right !important;" class="right">الإسم</th>
                  <th>نوع المساعدات</th>                
                  <th style="width:100px;">البطاقة الشخصة</th>
                  <th>المحافظة</th>
                  <th>ولاية</th> 
                  <th>جنس</th>                 
                  <th style="width:90px;">تاريخ التسجيل</th>
                  <th>المرحلة</th>
                  <th style="width:85px !important;">كل الملاحظات</th>
                  <!--<th>الإجرائات</th>-->
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'inquiries/inquiries_list/'.$branchid.'/'.$charity_type,'columns_array'=>'
				
                { "data": "الإسم" },
                { "data": "نوع المساعدات" },               
                { "data": "البطاقة الشخصة" },
				{ "data": "المحافظة" },
				{ "data": "ولاية" },
				{ "data": "جنس" },
				{ "data": "تاريخ التسجيل" },
				{ "data": "المرحلة" },
				{ "data": "كل الملاحظات" }')); ?>
</div>
</body>
</html>
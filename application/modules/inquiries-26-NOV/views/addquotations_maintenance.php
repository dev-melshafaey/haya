  <form  method="POST" id="frm_meeting" name="frm_meeting" action="<?php echo base_url()?>inquiries/addquotations_maintenance/<?php echo $applicantid;?>/<?php echo $quotationsDataId;?>" enctype="multipart/form-data" onsubmit="return uploadFiles();" >
  <input type="hidden" value="<?php echo $applicantid;?>"   name="applicantid" id="applicantid" />
  <input type="hidden" value="<?php echo $quotationsDataId;?>"   name="quotationsDataId" id="quotationsDataId" />
   <input type="hidden" value="<?php echo $rows->maintananceId;?>"   name="maintananceId" id="maintananceId" />
      <input type="hidden" name="quotationsId" id="quotationsId" value=""/>
<div class="row col-md-12">
    <div class="form-group col-md-4">
      <label for="basic-input">أسم شركة المقاولات</label>
      <select class="form-control req"  placeholder="أسم شركة المقاولات" name="company" id="company"   >
      <option value="">--تحديد--</option>
     <?php if(count($quotation)>0){
		 foreach($quotation as $qts){
		 ?>
     <option value="<?php echo $qts->company ?>" data-title="<?php echo $qts->phone ?>" data-id="<?php echo $qts->address ?>" data-role="<?php echo $qts->quotationsId ?>"  ><?php echo $qts->company ?></option>
     
     <?php } } ?>
     </select>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">هاتف</label>
      <input type="text" class="form-control req" value="<?php echo $rows->phone;?>" onkeyup="only_numeric(this);" placeholder="هاتف" name="phone" id="phone" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">العنوان</label>
      <input type="text" class="form-control req" value="<?php echo $rows->address;?>" placeholder="العنوان" name="address" id="address" />
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">مدة تنفيذ الاعمال المعتمدة</label>
      <input type="text" class="form-control req" value="<?php echo $rows->duration;?>" placeholder="مدة تنفيذ الاعمال المعتمدة" name="duration" id="duration" />
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">تبدا من تاريخ</label>
      <input type="text" class="form-control req datepicker " value="<?php echo $rows->startdate;?>" placeholder="تبدا من تاريخ" name="startdate" id="startdate" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">قيمة مساهمة الهيئة</label>
      <input type="text" class="form-control req" value="<?php echo $rows->contribution;?>" placeholder="قيمة مساهمة الهيئة" name="contribution" id="contribution" />
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">تنتهي بتاريخ</label>
      <input type="text" class="form-control req datepicker " value="<?php echo $rows->expires;?>" placeholder="تنتهي بتاريخ" name="expires" id="expires" />
    </div>
    
   
     <div class="form-group col-md-4">
      <label for="basic-input">أرفاق أمر تشغيل لبدء عمل الصيانة</label>
      <input type="file" class="form-control"  placeholder="أرفاق أمر تشغيل لبدء عمل الصيانة" name="attachment[]" id="attachment" multiple="multiple" />
      <input type="hidden"  name="attachment_old" id="attachment_old" value="<?php echo $rows->attachment;?>"  />
   	<?php if($rows->attachment !=""){
				   $i=0;
				   $attachment = @explode(',',$rows->attachment);
				   foreach($attachment as $att){
					   if($att !=""){
						   $i++;
				   ?>
                   <div id="att_<?php echo $i;?>" style="float:right;">
              <?php echo $controller->getfileicon($att,base_url().'resources/inquiries/'.$login_userid);?>
            
              </div>
              <?php } } }?>
    </div>
      <div class="form-group col-md-12">
      <label for="basic-input">ملاحظات</label>
      <textarea   class="form-control req"  placeholder="ملاحظات" name="notes" id="notes" ><?php echo $rows->notes;?></textarea>
    </div>
    
  
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="submit" class="btn btn-success btn-lrg" name="submit"  id="save_meeting" value="حفظ"  />
  </div>
</div>
</form>
<script>
function uploadFiles()
{
	//check_my_session();
        $('#frm_meeting .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_meeting .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_meeting .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {
	var form_data = new FormData(this); 
 $.ajax({ //ajax form submit
            url :  config.BASE_URL+'inquiries/addquotations_maintenance',
            type: 'POST',
            data : form_data,
            dataType : "html",
            contentType: false,
            cache: false,
            processData:false,
			beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(msg)
		  {		
			  $('#ajax_action').hide();
			 $('#addingDiag').modal('hide');	  
			 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
			
			quotations_maintenance('<?php echo $applicantid ?>');
			 return false;
		  }
        });
		}
		 else 
		{
            show_notification_error_end(ht);
			return false;
        }
		return false;
		
}
 $(document).ready(function(){

$('form').on('submit', uploadFiles);
});
 $("#company").change(function(e){
	 $('#phone').val($(this).find(':selected').data('title'));
	$('#address').val($(this).find(':selected').data('id'));
	$('#quotationsId').val($(this).find(':selected').data('role'));
 });
$(function(){
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});
</script>
<!doctype html>
<?PHP
	$main = $m['main'];	
	$tab_heading = array('مشترك ١','مشترك ٢','مشترك ٣','مشترك ٤');
	
?>
<?PHP $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?PHP $this->load->view('common/bodyscript'); ?>
<?PHP $this->load->view('common/menu'); ?>
<?PHP if($main->tempid!='' && !empty($h)) { 
	$this->load->view('common/leftpanel',array('history'=>$h)); 
} ?>
<section class="wrapper scrollable">
<?PHP $this->load->view('common/logo'); ?>
<?PHP $this->load->view('common/usermenu'); ?>
<?PHP $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
<?PHP $this->load->view('common/quicklunchbar'); ?>

<div class="row">
  <div class="col-md-12">
    <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
    <!----------------->
    <form id="form2" name="form2" method="post" action="<?PHP echo md5(date('Ymdhisf')); ?>" autocomplete="off">
      <input type="hidden" name="tempid" id="tempid" value="<?PHP echo $main->tempid; ?>" />
      <input type="hidden" name="utype" id="utype" value="<?PHP echo $main->user_type; ?>" />
      <?php if($t=='review') { ?>
      <input type="hidden" name="review" id="review" value="1" />
      <?PHP } ?>
      
      <div class="col-md-8">
        <div class="panel-default panel-block">
          <div class="nav nav-tabs panel panel-default panel-block">
            <div class="col-md-2">
              <label class="text-warning font_size">فردي </label>
              <label>
                <input type="radio" onClick="setx_user_type(this);" id="user_type" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $main->tempid; ?>"  class="temptypemain" <?PHP if($main->user_type=='فردي') { ?>checked="checked"<?PHP } ?> name="user_type" value="فردي" data-title="personal"  required />
              </label>
            </div>
            <div class="col-md-2">
              <label class="text-warning font_size">مشترك </label>
              <label>
                <input type="radio" onClick="setx_user_type(this);" id="user_type" class="temptypemain" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $main->tempid; ?>" name="user_type" <?PHP if($main->user_type=='مشترك') { ?>checked="checked"<?PHP } ?> value="مشترك" data-title="partner" />
              </label>
            </div>
          </div>
        </div>
        <!------------------------------------->
        <div class="panel panel-default panel-block">
          <div id="nimbus">
            <ul class="nav nav-tabs panel panel-default panel-block">
              <?PHP for($k=0; $k<=3; $k++) {?>
              <li><?PHP if($k!=0) { ?><img style="margen-top:5px;" src="<?php echo base_url();?>images/sep.png" width="20" height="44"><?PHP } ?></li>
              <li class="tabsdemo-<?PHP echo $k; ?> <?PHP if($k==0) { ?>active<?PHP } ?>"><a href="#tabsdemo-<?PHP echo $k; ?>"  data-toggle="tab"><?PHP echo $tab_heading[$k]; ?></a></li>
              <?PHP } ?>
            </ul>
          </div>
          <div class="tab-content panel panel-default panel-block">
            <?PHP 
					for($j=0; $j<=3; $j++) { 
					$index = $j;
					$appli = $main->applicant[$j];
					$appid = $appli->applicantid;							
			?>
            <div class="tab-pane list-group <?PHP if($j==0) { ?>active<?PHP } ?>" id="tabsdemo-<?PHP echo $j; ?>">
              <div class="row setMargin">
                <div class="col-md-12">
                  <input type="hidden" name="applicantid[]" id="applicantid<?php echo $in ?>" value="<?php echo $appli->applicantid; ?>" />
                  <div class="form-group col-md-6">
                    <label class="text-warning">الاسم الأول</label>
                    <input name="first_name_<?PHP echo $appid;?>" value="<?PHP echo $appli->first_name; ?>" placeholder="الاسم الأول" id="first_name" type="text" class="<?PHP echo is_required($j);?> form-control">
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">الاسم الثاني</label>
                    <input name="middle_name_<?PHP echo $appid;?>" value="<?PHP echo $appli->middle_name; ?>" placeholder="الاسم الثاني" id="middle_name" type="text" class="<?PHP echo is_required($j);?> form-control">
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">الاسم الثالث</label>
                    <input name="last_name_<?PHP echo $appid;?>" value="<?PHP echo $appli->last_name; ?>" placeholder="الاسم الثالث" id="last_name" type="text" class="<?PHP echo is_required($j);?> form-control">
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">القبيلة / العائلة</label>
                    <input name="sur_name_<?PHP echo $appid;?>" value="<?PHP echo $appli->family_name; ?>" placeholder="القبيلة / العائلة" id="family_name" type="text" class="<?PHP echo is_required($j);?> form-control">
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">النوع</label>
                    <br>
                    <input type="radio" id="applicanttype" name="applicanttype_<?PHP echo $appid;?>" <?PHP if($appli->applicanttype=='ذكر') { ?>checked="checked"<?PHP } ?> value="ذكر" data-title="personal"  required />
                    ذكر
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" id="applicanttype" name="applicanttype_<?PHP echo $appid;?>" <?PHP if($appli->applicanttype=='أنثى') { ?>checked="checked"<?PHP } ?> value="أنثى" data-title="partner" />
                    أنثى </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">رقم الرقم المدني</label>
                    <input name="idcard_<?PHP echo $appid;?>"  value="<?PHP echo $appli->idcard; ?>" id="idcard" placeholder="رقم الرقم المدني" type="text" class="<?PHP echo is_required($j);?> form-control NumberInput autocomplete">
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">رقم بطاقة سجل القوى العاملة</label>
                    <input name="cr_number_<?PHP echo $appid;?>"  value="<?PHP echo $appli->cr_number; ?>" id="cr_number" placeholder="رقم سجل القوى العاملة" type="text" class="<?PHP echo is_required($j);?> form-control NumberInput">
                  </div>
                  <div class="form-group col-md-4">
                    <label class="text-warning">تاريخ الميلاد</label>
                    <input name="datepicker_<?PHP echo $appid;?>" type="text"  value="<?PHP echo $appli->datepicker; ?>" class="<?PHP echo is_required($j);?> form-control age_datepicker p_age" data-durar="age_datepicker_<?PHP echo $appli->applicantid; ?>"  id="age_datepicker_<?PHP echo $index; ?>" placeholder="تاريخ الميلاد" size="15" maxlength="10">
                  </div>
                  <div class="form-group col-md-2">
                    <label class="text-warning">العمر</label>
                    <input name="age_<?PHP echo $appid;?>" type="text" class="form-control age" id="age_datepicker_<?PHP echo $appli->applicantid; ?>" value="<?PHP if($appli->datepicker) { echo calcualteAge($appli->datepicker); } ?>"  placeholder="العمر" size="5" maxlength="8" readonly>
                  </div>
                  <div class="form-group col-md-6" style="margin-bottom: 0px !important;">
                    <label class="text-warning">رقم الهاتف</label>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning"></label>
                  </div>
                  <?PHP 
				  		$px = $main->phones[$appli->applicantid];						
						for($p=0; $p<=3; $p++) 
						{	$phones = $px[$p];
					?>
                  <div class="form-group col-md-3">
                    <input name="phone_numbers_<?PHP echo $appli->applicantid; ?>[]" value="<?PHP echo $phones->phonenumber; ?>"  type="text" class=" <?PHP if($p==0 && is_required($j)=='req') { ?>req<?PHP } ?> form-control applicantphone checkphonenumber NumberInput phoneautocomplete" id="phonenumber<?PHP echo $appli->applicantid.$p; ?>" placeholder="رقم الهاتف" maxlength="8">
                  </div>
                  <?PHP }  ?>
                  <div class="form-group col-md-6">
                    <label class="text-warning">الحالة الاجتماعية</label>
                    <?PHP multi_action_dropbox('','marital_status',$appli->applicantid,$main->tempid,$appli->marital_status,'اختر الحالة الاجتماعية','maritalstatus','',$appli->marital_status_text,'كم عدد الأطفال لديك','marital_status_text'); ?>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">الوضع الحالي</label>
                    <?PHP multi_action_dropbox('','job_status',$appli->applicantid,$main->tempid,$appli->job_status,'اختر الوضع الحالي','current_situation','',$appli->job_status_text,'الوضع الحالي','job_status_text'); ?>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">العنوان الشخصي</label>
                    <?PHP inq_reigons('','province',$appli->province,'',$main->tempid,$appli->applicantid); ?>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">الولاية</label>
                    <?PHP inq_wilayats('walaya',$appli->walaya,$appli->province,'',$main->tempid,$appli->applicantid); ?>
                  </div>
                  <div class="form-group col-md-6" style="display:none;">
                    <label class="text-warning">رقم بطاقة سجل القوى العاملة</label>
                    <input name="mr_number_<?PHP echo $appid;?>" id="mr_number" value="<?PHP echo $appli->mr_number; ?>" placeholder="رقم بطاقة سجل القوى العاملة" type="text" class=" form-control NumberInput">
                  </div>
                  <div class="form-group col-md-6">
                    <div class="form-group col-md-8">
                      <label class="text-warning">هل أنت مسجل في التأمينات الإجتماعية؟</label><br>
                      
                      <input id="is_insurance" onclick="pass_id(this,'insinfo','<?PHP echo $index;?>')" type="radio" <?PHP if($appli->is_insurance=='Y') { ?>checked="checked"<?PHP } ?> name="is_insurance_<?PHP echo $appid;?>" class="ins" value="Y" />
                      نعم
                      <input id="is_insurance"  onclick="pass_id(this,'insinfo','<?PHP echo $index;?>')"  <?PHP if($appli->is_insurance=='N') { ?>checked="checked"<?PHP } ?> type="radio" class="ins" name="is_insurance_<?PHP echo $appid;?>" value="N" />
                      لا </div>
                    <div class="form-group col-md-4 arrange_field" id="insinfo<?PHP echo $index;?>" <?PHP if($appli->is_insurance=='Y') { ?>style="display:block !Important;"<?PHP } else{?> style="display:none !Important;"<?php } ?>>
                      <label class="text-warning">رقم التسجيل</label>
                      <input name="insurance_number_<?PHP echo $appid;?>" data-handler="<?PHP echo $main->tempid; ?>_<?PHP echo $main->tempid; ?>" value="<?PHP echo $appli->insurance_number; ?>" id="insurance_number" placeholder="رقم التسجيل" type="text" class="form-control">
                    </div>
                  </div>
                  <div class="form-group col-md-11">
                    <label class="text-warning">هل لديك مشروع؟</label>
                    <input id="confirmation"  onclick="pass_id(this,'extrainfo','<?PHP echo $index;?>')" type="radio" <?PHP if($appli->confirmation=='Y') { ?>checked="checked"<?PHP } ?> name="confirm_<?PHP echo $appid;?>" value="Y" />
                    نعم
                    <input id="confirmation" onclick="pass_id(this,'extrainfo','<?PHP echo $index;?>')" <?PHP if($appli->confirmation=='N') { ?>checked="checked"<?PHP } ?> type="radio" name="confirm_<?PHP echo $appid;?>" value="N" />
                    لا </div>
                  <?PHP
    	$display = 'none';
		if($appli->confirmation == 'Y')	{	$display = 'block';	}
	?>
                  <div class="form-group col-md-12" id="extrainfo<?PHP echo $index;?>" style="display:<?php echo $display; ?>">
                    <div class="form-group col-md-6">
                      <label class="text-warning">اسم المشروع</label>
                      <input name="project_name_<?PHP echo $appid;?>" value="<?PHP echo $appli->project_name; ?>" id="project_name" placeholder="اسم المشروع" type="text" class=" form-control">
                    </div>
                    <div class="form-group col-md-6">
                      <label class="text-warning">موقع المشروع</label>
                      <input name="project_location_<?PHP echo $appid;?>" value="<?PHP echo $appli->project_location; ?>" id="project_location" placeholder="المكان" type="text" class=" form-control">
                    </div>
                    <div class="form-group col-md-6">
                      <label class="text-warning">نشاط المشروع</label>
                      <input name="project_activities_<?PHP echo $appid;?>" value="<?PHP echo $appli->project_activities; ?>" id="project_activities" placeholder="نشاط المشروع" type="text" class=" form-control">
                    </div>
                    <div class="form-group col-md-6">
                      <label class="text-warning">الاسم التجاري</label>
                      <input name="project_cr_name_<?PHP echo $appid;?>" value="<?PHP echo $appli->project_cr_name; ?>" id="project_cr_name" placeholder="الاسم التجاري" type="text" class=" form-control">
                    </div>
                    
                  </div>
                  <br>
                  <div class="form-group col-md-11">
                      <label class="text-warning">هل سبق لك الحصول على قرض للمشروع؟</label>
                      <input id="is_loan" onclick="pass_id(this,'question_details','<?PHP echo $index;?>')" type="radio" <?PHP if($appli->is_loan=='Y') { ?>checked="checked"<?PHP } ?> name="is_loan_<?PHP echo $appid;?>" value="Y" />
                      نعم
                      <input id="is_loan" onclick="pass_id(this,'question_details','<?PHP echo $index;?>')" <?PHP if($appli->is_loan=='N') { ?>checked="checked"<?PHP } ?> type="radio" name="is_loan_<?PHP echo $appid;?>" value="N" />
                      لا </div>
                    <div class="form-group question_details col-md-6" id="question_details<?PHP echo $index;?>" <?PHP if($main->is_loan=='Y') { ?>style="display:block;"<?PHP } else { ?>style="display:none;"<?PHP } ?>>
                      <li class="data_list_grid">
                        <input id="is_bank_loan"  type="checkbox" <?PHP if($main->is_bank_loan=='1') { ?>checked="checked"<?PHP } ?> name="is_bank_loan_<?PHP echo $appid;?>" value="1" />
                        بنك التنمية العماني </li>
                      <li class="data_list_grid">
                        <input id="is_rafd_loan"  type="checkbox" <?PHP if($main->is_rafd_loan=='1') { ?>checked="checked"<?PHP } ?> name="is_rafd_loan_<?PHP echo $appid;?>" value="1" />
                        صندوق شراكة
                        </liv>
                      <li class="data_list_grid">
                        <input id="is_commercial_loan"  type="checkbox" <?PHP if($main->is_commercial_loan=='1') { ?>checked="checked"<?PHP } ?> name="is_commercial_loan_<?PHP echo $appid;?>" class="" value="1" />
                        بنك تجاري </li>
                      <li class="data_list_grid">
                        <input id="is_other_loan"  onclick="pass_id(this,'other_value','<?PHP echo $index;?>')"  type="checkbox" <?PHP if($main->is_other_loan =='1') { ?>checked="checked"<?PHP } ?> name="is_other_loan_<?PHP echo $appid;?>" class="" value="1" />
                        اخرى </li>
                      <li class="data_list_grid">
                        <?PHP if($main->is_other_loan =='1') { $dislay = "Block"; } else { $dislay = "None"; } ?>
                        <input id="other_value" name="other_value_<?PHP echo $appid;?>" value="<?PHP echo $main->project_location; ?>"  placeholder="اخرى" type="text" class=" form-control" style="display:<?php echo $dislay; ?>">
                      </li>
                    </div>
                </div>
              </div>
            </div>
            <?PHP unset($appli); } ?>
          </div>
        </div>
      </div>
      <!------------------------>
     <div class="col-md-4">
        <div class="panel panel-default panel-block">
       		<div class="form-group">
            <label for="text-area-auto-resize"></label>
            	<input id="searchcurrent" type="text" class="form-control label-default pull-left searchfieldforpan" style="" value="بحث رقم المراجعين" tabindex="1">
            </div>
        </div>
        <div class="panel panel-default panel-block">
          <div class="list-group">
            <div class="list-group-item" id="checkboxes-and-radios">
              <h4 class="section-title">نوع الاستفسار</h4>
              <div class="form-group">
                <div class="multibox">
                  <?PHP inquiry_type_tree($main->tempid); ?>
                </div>
              </div>
            </div>
            <div class="list-group-item" id="checkboxes-and-radios">
              <h4 class="section-title">تفاصيل الإستفسار</h4>
              <div class="form-group">
                <label for="text-area-auto-resize"></label>
                <div>
                  <textarea class="form-control auto-resize txt_textarea" name="inquiry_text" id="inquiry_text"><?PHP echo $main->inquiry_text; ?></textarea>
                </div>
              </div>
            </div>
            <div class="list-group-item" id="checkboxes-and-radios">
              <h4 class="section-title">ملاحظات الموظف</h4>
              <div class="form-group">
                <label for="text-area-auto-resize"></label>
                <div>
                  <textarea class="form-control auto-resize txt_textarea" data-handler="<?PHP echo $main->tempid; ?>" name="notestext" id="notestext"></textarea>
                </div>
              </div>
            </div>
            <div class="list-group-item" id="checkboxes-and-radios">
              <div class="form-group">
                <div>
                <?php if($main->tempid=='') { ?>
                  <button type="button" id="save_data_inquery" class="btn btn-success">حفظ</button>
                <?PHP } else { ?>
                <button type="button" id="update_data_inquery" class="btn btn-success">حفظ</button>
                <?PHP } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </form>
    <!-----------------> 
  </div>
</div>
<?php $this->load->view('common/footer');?>
<script src="<?PHP echo base_url(); ?>js/muragain_js.js"></script>
</body>
</html>
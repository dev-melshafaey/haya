<!doctype html>
<?php
	$ah_applicant 						=	$_applicant_data['ah_applicant'];
	$ah_applicant_wife 					=	$_applicant_data['ah_applicant_wife'];
	$ah_applicant_survayresult 			=	$_applicant_data['ah_applicant_survayresult'];
	$ah_applicant_relation 				=	$_applicant_data['ah_applicant_relation'];
	$ah_applicant_family 				=	$_applicant_data['ah_applicant_family'];
	$ah_applicant_economic_situation 	=	$_applicant_data['ah_applicant_economic_situation'];
	$ah_applicant_documents 			=	$_applicant_data['ah_applicant_documents'];
	$ah_applicant_decission 			=	$_applicant_data['ah_applicant_decission'];
	
?>
<?php $section_type	=	 $this->haya_model->get_name_from_list($list_parent_id); ?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <?php $msg	=	$this->session->flashdata('msg');?>
  <?php if($msg):?>
  <div class="col-md-12">
    <div style="padding: 22px 20px !important; background:#c1dfc9;">
      <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
    </div>
  </div>
  <?php endif;?>
  <div class="row">
    <form id="edit_for_section" name="edit_for_section" method="post" action="<?PHP echo base_url().'inquiries/edit_for_Section' ?>" autocomplete="off" enctype="multipart/form-data">
      <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $ah_applicant->applicantid;?>" />
      <div class="col-md-12">
        <?php $this->load->view('common/panel_block', array('module' => $module, 'headxxx' => $heading)); ?>
        <div class="col-md-12">
          <div class="col-md-5 fox leftborder">
            <h4 class="panel-title customhr">بيانات طلب مساعدة</h4>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الاستمارة: </label>
              <strong><?PHP echo arabic_date($ah_applicant->applicantcode); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">التاريخ: </label>
              <strong><?PHP echo show_date($ah_applicant->registrationdate,5); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">نوع الطلب: </label>
              <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->charity_type_id); ?> </strong> </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">الاسم الرباعي والقبيلة: </label>
              <strong><?PHP echo $ah_applicant->fullname; ?> </strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم جواز سفر: </label>
              <strong><?PHP echo arabic_date($ah_applicant->passport_number); ?> </strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم البطاقة الشخصة: </label>
              <strong><?PHP echo arabic_date($ah_applicant->idcard_number); ?> </strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">المحافظة \ المنطقة: </label>
              <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->province); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">ولاية: </label>
              <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->wilaya); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">البلدة \ المحلة: </label>
              <strong><?PHP echo $ah_applicant->address; ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">هاتف المنزل: </label>
              <strong><?PHP echo arabic_date($ah_applicant->hometelephone); ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الهاتف :</label>
              <strong><?PHP echo arabic_date(implode('<br>',json_decode($ah_applicant->extratelephone,TRUE))); ?></strong></div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الحالة الجتماعية:</label>
              <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->marital_status); ?></strong> </div>
            <?PHP foreach($ah_applicant_wife as $wife) { ?>
            <div class="col-md-6 form-group">
              <label class="text-warning">اسم الزوج\الزوجة: </label>
              <strong><?PHP echo $wife->relationname; ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الجواز\البطاقة: </label>
              <strong><?PHP echo arabic_date($wife->relationdoc); ?></strong> </div>
            <?PHP } ?>
            <div class="col-md-6 form-group">
              <label class="text-warning">اسم الام: </label>
              <strong><?PHP echo $ah_applicant->mother_name; ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الجواز\البطاقة: </label>
              <strong><?PHP echo arabic_date($ah_applicant->mother_id_card); ?></strong> </div>
            <!-----------------Documents--------------------------> 
            
            <!---------------------------------------------------->
            <div class="col-md-12 form-group">
              <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <h4 class="panel-title customhr">اترفق الوثائق والمستندات الازمة لطلب</h4>
                <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                  <?PHP 
					$doccount = 0;					
					foreach($this->inq->allRequiredDocument($ah_applicant->charity_type_id) as $ctid) { $doccount++; 
						$doc = $ah_applicant_documents[$ctid->documentid];						
						$url = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$doc->document;
					?>
                  <div class="panel panel-default" style="border-bottom: 1px solid #ddd;">
                    <div class="panel-heading" style="padding:7px 3px !important;" id="head<?PHP echo $ctid->documentid;?>">
                      <h4 class="panel-title" style="font-size:12px; font-weight:normal !important;">
                        <?PHP if($doc->appli_doc_id!='') { ?>
                        <span class="icons" id="removeicons<?PHP echo $doc->appli_doc_id; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> </span>
                        <?PHP } ?>
                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $ctid->documentid;?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                    </div>
                  </div>
                  <?PHP } ?>
                </div>
              </div>
            </div>
            <h4 class="panel-title customhr">افراد الأسرة: (بما فهيم العاملين و غيرالعاملين) </h4>
            <div class="col-md-12 form-group">
              <table class="table table-bordered table-striped dataTable">
                <thead>
                  <tr role="row">
                    <th>الاسم</th>
                    <th style="width: 50px;">السن</th>
                    <th>القرابة</th>
                    <th>المهنة</th>
                    <th style="width: 70px;">الدخل الشهري</th>
                  </tr>
                </thead>
                <tbody role="alert" aria-live="polite" aria-relevant="all">
                  <?PHP 
			  
			  if($ah_applicant_relation)
			  {	
			  	foreach($ah_applicant_relation as $apr) { ?>
                  <tr class="familycount">
                    <td><?php echo $apr->relation_fullname; ?></td>
                    <td><?php echo arabic_date($apr->age); ?></td>
                    <td><?php echo $this->haya_model->get_name_from_list($apr->relationtype); ?></td>
                    <td><?php echo $this->haya_model->get_name_from_list($apr->professionid); ?></td>
                    <td><?php echo arabic_date($apr->monthly_income); ?></td>
                  </tr>
                  <?php 
                    $salarys += $apr->monthly_income;
                }
			  }
			 ?>
                <input type="hidden" id="familysalary" value="<?php echo $ah_applicant->salary; ?>" />
                  </tbody>
              </table>
            </div>
            <h4 class="panel-title customhr">االحالة الاقتصادية: </h4>
            <div class="col-md-6 form-group">
              <label class="text-warning">الدخل: </label>
              <strong><?php echo arabic_date($ah_applicant->salary); ?></strong> </div>
            <div class="col-md-6 form-group">
              <?php if($ah_applicant->source=='يوجد') { echo('يوجد: يوضح كالاتي'); } elseif($ah_applicant->source=='لايوجد') { echo('لايوجد: ويوضح مصدر المعيشة'); } ?>
            </div>
            <?php if($ah_applicant->source=='يوجد') { ?>
            <div class="col-md-6 form-group">
              <label class="text-warning">مصدره: </label>
              <strong><?php echo $ah_applicant->musadra; ?></strong> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">قيمتة: </label>
              <strong><?php echo $ah_applicant->qyama; ?></strong> </div>
            <?php } ?>
            <?php if($ah_applicant->source=='لايوجد') { ?>
            <div class="col-md-12 form-group">
              <label class="text-warning">مصدر المعيشة: </label>
              <strong><?php echo $ah_applicant->lamusadra; ?></strong> </div>
            <?php } ?>
            <div class="col-md-12 form-group">
              <label class="text-warning">وضف الحالة السكنية: </label>
              <?php echo $ah_applicant->current_situation; ?> </div>
            <div class="col-md-7 form-group">
              <label class="text-warning"><strong><?php echo $ah_applicant->ownershiptype; ?></strong> </label>
            </div>
            <div class="col-md-5 form-group"> <strong><?php echo arabic_date($ah_applicant->ownershiptype_amount); ?></strong> </div>
            <div class="col-md-12">
              <h4 class="panel-title customhr">مكونات المنزل:</h4>
            </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">نوع البناء:</label>
              <strong><?php echo $this->haya_model->get_name_from_list($ah_applicant->building_type); ?></strong> </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">عدد الغرف:</label>
              <strong><?php echo arabic_date($ah_applicant->number_of_rooms); ?></strong> </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">المرافق:</label>
              <strong><?php echo arabic_date($ah_applicant->utilities); ?></strong> </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">حالة الأثاث والموجودات:</label>
              <strong><?php echo $ah_applicant->furniture; ?></strong> </div>
            <div class="col-md-12">
              <h4 class="panel-title customhr">البيانات الدراسية:</h4>
            </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">شهادة ثنوية:</label>
              <strong><?php echo $ah_applicant->certificate_dualism; ?></strong> </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">سنة التخرج:</label>
              <strong><?php echo $ah_applicant->graduation_year; ?></strong> </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">النسبة :</label>
              <strong><?php echo $ah_applicant->school_percentage; ?></strong> </div>
            <div class="col-md-12">
              <h4 class="panel-title customhr">البيانات الجامعية :</h4>
            </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">اسم الكلية أو الجامعة:</label>
              <strong><?php echo $ah_applicant->college_name; ?></strong> </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">التخصص :</label>
              <strong><?php echo $ah_applicant->specialization; ?></strong> </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">السنة لدراسية :</label>
              <strong><?php echo $ah_applicant->year_of_study; ?></strong> </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">اخر معدل تراكمي :</label>
              <strong><?php echo $ah_applicant->last_grade_average; ?></strong> </div>
            <div class="col-md-12">
              <h4 class="panel-title customhr">تفاصيل البنك:</h4>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">اسم البنك:</label>
              <?php echo $this->haya_model->get_name_from_list($ah_applicant->bankid); ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الفرع:</label>
              <?php echo $this->haya_model->get_name_from_list($ah_applicant->branchid); ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الحساب:</label>
              <?php echo $ah_applicant->accountnumber; ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">نوع الحساب: جاري \ توفير : </label>
              <?php echo($ah_applicant->accounttype); ?> </div>
            <?php if($ah_applicant->bankstatement) { ?>
            <div class="col-md-12 form-group">
              <label class="text-warning">يرفق صورة من كشف الحسابات (حديث الاصدار) :</label>
              <?php
          	$statmentURL = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->bankstatement;
			echo getFileResult($statmentURL,'يرفق صورة من كشف الحسابات (حديث الاصدار)',$statmentURL);
		  	?>
            </div>
            <?php } ?>
          </div>
          <div class="col-md-7 fox">
            <h4 class="panel-title customhr">بعد إجراء البحث الاجتماعي والاطلاع على المستندات المؤيدة اتضح مايلي :-</h4>
            <div class="col-md-6 form-group">
              <label class="text-warning">الحالة الصحية :</label>
              <?php echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->health_condition); ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الحالة السكنية :</label>
              <?php echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->housing_condition); ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">عدد أفراد الأسرة :</label>
              <?php echo arabic_date($ah_applicant_survayresult->numberofpeople); ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">ترتيب مقدم الطلب بالأسرة :</label>
              <?php echo arabic_date($ah_applicant_survayresult->positioninfamily); ?> </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">الحالة الاقتصادية :</label>
              <?php echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->economic_condition); ?> </div>
            <div class="col-md-12 form-group">
              <h4 class="panel-title customhr">نوع الحالة (<?php echo $ah_applicant_survayresult->casetype; ?>) :-</h4>
            </div>
            <div class="col-md-12 form-group casetype" id="zamanya" <?php if($ah_applicant_survayresult->casetype=='للحالات الضمانية') { ?> style="display:block !important;" <?php } ?>>
              <div class="col-md-6 form-group">
                <label class="text-warning">الحالة الضمانية باسم :</label>
                <?php echo $ah_applicant_survayresult->aps_name; ?> </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">رقم الحاسب :</label>
                <?php echo $ah_applicant_survayresult->aps_account; ?> </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">رقم الملف :</label>
                <?php echo $ah_applicant_survayresult->aps_filename; ?> </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">الفئة :</label>
                <?php echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->aps_category); ?> </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">قيمة المعاش :</label>
                <?php echo arabic_date($ah_applicant_survayresult->aps_salaryamount); ?> </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">تاريخ الربط :</label>
                <?php echo arabic_date(date('d/m/Y',strtotime($ah_applicant_survayresult->aps_date))); ?> </div>
              <div class="col-md-12 form-group">
                <label class="text-warning">مصادر دخل أخرى :</label>
                <?php $json = json_decode($ah_applicant_survayresult->aps_another_income,TRUE);
			  		$inx = 0;
			  		for($mdi=1; $mdi<=4; $mdi++) 
					{ 
						if($json[$inx]) 
						{ 
							echo('<li>'.$json[$inx].'<li>');
							$inx++; 
						} 
					} 
				?>
              </div>
            </div>
            <div class="col-md-12 form-group casetype" id="ghairzamanya"  <?php if($ah_applicant_survayresult->casetype=='للحالات غير الضمانية') { ?> style="display:block !important;" <?php } ?>>
              <div class="col-md-12 form-group">
                <label class="text-warning">مصادر دخل أخرى :</label>
                <?php $inxp = 0;
			  		for($mdix=1; $mdix<=4; $mdix++) 
					{	if($json[$inxp])
						{
							echo('<li>'.$json[$inxp].'<li>');
							$inxp++; 
						}
					}?>
              </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">اجمالي الدخل الشهري للفرد :</label>
                <?php echo $ah_applicant_survayresult->aps_month; ?> </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">اجمالي الدخل الشهري للاسرة :</label>
                <?php echo $ah_applicant_survayresult->aps_year; ?> </div>
            </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">الغرض من المساعدة :</label>
              <?php echo $ah_applicant_survayresult->whyyouwant; ?> </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">ملخص الحالة :</label>
              <?php echo $ah_applicant_survayresult->summary; ?> </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">رأي الباحث الاجتماعي و مبرراته :</label>
              <?php echo $ah_applicant_survayresult->review; ?> </div>
          <?php if($child_id	==	'1717'):?>
          <div class="col-md-12 form-group" style="background-color: #CDEB8B; padding-bottom: 9px; padding-top: 9px;">
           <div class="col-md-12 form-group">
          	تقرير الزيارة الميدانية<br />
            <label class="text-warning">نوع الصيانة:</label>
            <?php echo $this->haya_model->create_dropbox_list('maintenance_type','maintenance_type','',0,'req'); ?>
          </select>
          </div>
          	<div class="col-md-6 form-group">
            <label class="text-warning">تقرير الزيارة:</label>
            <textarea name="report_visit" id="report_visit" style="height:150px; resize:none;" placeholder="رأي الباحث الاجتماعي و مبرراته"  class="form-control req"></textarea>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">اقتراح القسم:</label>
            <textarea name="proposal_section" id="proposal_section" style="height:150px; resize:none;" placeholder="رأي الباحث الاجتماعي و مبرراته"  class="form-control req"></textarea>
          </div>
          <div class="form-group col-md-6" >
             <label class="text-warning">أرفق استمارات الزيارة الميدانية</label>
             <input style="border: 0px !important; color: #d09c0d;" type="file" name="certificate" title="تحميل" tabindex="7">
            </div>
          <div class="form-group col-md-6" >
            <label class="text-warning">أرفق الصور الفوتوغرافية للموقع</label>
            <input style="border: 0px !important; color: #d09c0d;" type="file" name="pictures" title="تحميل" tabindex="7">
          	</div>
            <br clear="all">
            <div class="container demo-wrapper">
              <div class="row demo-columns">
                <div class="col-md-12"> 
                  <!-- D&D Zone-->
                  <div id="drag-and-drop-zone" class="uploader">
                    <div>سحب وإسقاط الملفات هنا</div>
                    <div class="or">-او-</div>
                    <div class="browser">
                      <label> <span>اضغط لاستعراض الملف</span>
                        <input type="file" name="muzaffar" />
                      </label>
                    </div>
                  </div>
                </div>
                <!-- / Left column -->
                
                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">تحميل</h3>
                    </div>
                    <div class="panel-body demo-panel-files" id='demo-files'> <span class="demo-note">لم يتم اختيار الملفات / دروبيد بعد ...</span> </div>
                  </div>
                </div>
                <!-- / Right column --> 
              </div>
            </div>
          </div>
          <?php endif;?>
          <div class="col-md-12 form-group" style="background-color: #CDEB8B; padding-bottom: 9px; padding-top: 9px;">
            <h4 class="panel-title customhr">رابعا: قسم المساعدات الاسكانية ( عروض الاسعار ):</h4>
           
            <a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>inquiries/addquotation/<?php echo $_applicantid;?>" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">تفاصيل الطرد الغذائي</a>
            <br style="clear:both;"/>
            <div id="listquotation">
            
          	</div>
         
           </div>
          <div class="col-md-12 form-group" style="background-color: #CDEB8B; padding-bottom: 9px; padding-top: 9px;">
          
		  <?php
		  if($list_parent_id	==	'1701')
		  {
			  $display	=	'style="display:block;"';
			  $req		=	"req";
		  }
		  else
		  {
			  $display	=	'style="display:none;"';
			  $req		=	"";
		  }
		  ?>
          <div class="col-md-12 form-group" <?php echo $display;?>>
            <label class="text-warning">بيان الاخصائي العلاجي عن الحاله المرضيه</label>
            <textarea style="height:141px !important;" class="form-control <?php echo $req;?>" id="doctor_notes" name="doctor_notes" tabindex="85"></textarea>
          </div>
              <div class="col-md-12 form-group"> 
                <!-- <label class="text-warning">توصية اللجنة :</label>-->
                <label class="text-warning">أنواع القسم ( <?php echo $section_type;?> ) :</label>
                <br>
                <?php foreach($this->inq->section_types_parents('3',$branchid,$list_parent_id) as $key	=> $value) { ?>
                <div class="col-md-8 form-group">
                  <?php if($value['list_id']	!=	'1674'):?>
                  <label class="text-warning"><?php echo $value['name']; ?></label>
                  <input type="radio" <?php if($key==0) { echo('checked'); } ?> name="section_status" id="section_status" on value="<?php echo $value['list_id']; ?>">
                  <?php endif;?>
                </div>
                <?php } ?>
              </div>
              <div class="col-md-12 form-group" id="list-box">
                <label class="text-warning">اسم القائمة</label>
                <select name="listid" id="listid" class="form-control req" placeholder="اسم القائمة">
                  <option value="">اسم القائمة</option>
                  <?php if(!empty($all_list)):?>
                  <?php foreach($all_list as $list):?>
                  <option value="<?php echo $list->listid;?>"><?php echo $list->listname;?></option>
                  <?php endforeach;?>
                  <?php endif;?>
                </select>
              </div>
             	<?php if($list_parent_id	==	'1700'):?>
                 <div class="col-md-6 form-group">
                    <label class="text-warning">القيمة</label>
                    <input type="text" name="list_type_amount" id="list_type_amount" class="form-control" />
                 </div>
                 <div class="col-md-12 form-group">
                    <label class="text-warning">ملاحظات القسم</label>
                    <textarea style="height:141px !important;" class="form-control" id="list_type_notes" name="list_type_notes" tabindex="85"></textarea>
                  </div>
             	<?php endif;?>
              <div class="col-md-12 form-group" id="notes-box" style="display:none;">
                <label class="text-warning">الملاحظات</label>
                <textarea style="height:141px !important;" class="form-control" id="notes" name="notes" tabindex="85"></textarea>
              </div>
              <div class="col-md-12 form-group">
              <input type="hidden" name="child_id" id="child_id" value="<?php echo $child_id;?>"/>
			  <?php echo($this->haya_model->module_button(0,$ah_applicant->applicantid,'save_edit_section',$ah_applicant->case_close)); ?>
              </div>
            </div>
            <br clear="all">
          </div>
        </div>
      </div>
    </form>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/tasgeel_js.js"></script> 
<script>
$(document).ready(function(){ 
    $("input[name$='section_status']").click(function() {
        var listid = $(this).val();
		
		if(listid	==	'1706' || listid	==	'1712' || listid	==	'1721' || listid	==	'1726')
		{
			/* This is for No list Section*/
			$("#listid").removeClass("req");
			$("#listid").removeClass("parsley-error");
			$("#list-box").hide();
			
			/* This is for Notes Section*/
			$("#notes").addClass("req");
			$("#notes").addClass("parsley-error");
			$("#notes-box").show();
			
		}
		else
		{
			/* This is for No list Section*/
			$("#listid").addClass("req");
			$("#list-box").show();
			
			/* This is for Notes Section*/
			$("#notes").removeClass("req");
			$("#notes").removeClass("parsley-error");
			$("#notes-box").hide();
			
		}
    }); 
});
	  var	applicant_id	=	'<?php echo $ah_applicant->applicantid;?>';
	  
      $('#drag-and-drop-zone').dmUploader({
        url: '<?php echo base_url();?>inquiries/uploads_sakaniya_visits/'+applicant_id,
        dataType: 'json',
        allowedTypes: 'image/*',
        /*extFilter: 'jpg;png;gif',*/
        onInit: function(){
          $.danidemo.addLog('#demo-debug', 'default', 'Plugin initialized correctly');
        },
        onBeforeUpload: function(id){
          $.danidemo.addLog('#demo-debug', 'default', 'Starting the upload of #' + id);

          $.danidemo.updateFileStatus(id, 'default', 'Uploading...');
        },
        onNewFile: function(id, file){
          $.danidemo.addFile('#demo-files', id, file);
        },
        onComplete: function(){
			
          $.danidemo.addLog('#demo-debug', 'default', 'All pending tranfers completed');
        },
        onUploadProgress: function(id, percent){
          var percentStr = percent + '%';

          $.danidemo.updateFileProgress(id, percentStr);
        },
        onUploadSuccess: function(id, data){
          $.danidemo.addLog('#demo-debug', 'success', 'Upload of file #' + id + ' completed');

          $.danidemo.addLog('#demo-debug', 'info', 'Server Response for file #' + id + ': ' + JSON.stringify(data));

          $.danidemo.updateFileStatus(id, 'success', 'Upload Complete');

          $.danidemo.updateFileProgress(id, '100%');
        },
        onUploadError: function(id, message){
          $.danidemo.updateFileStatus(id, 'error', message);

          $.danidemo.addLog('#demo-debug', 'error', 'Failed to Upload file #' + id + ': ' + message);
        },
        onFileTypeError: function(file){
          $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: must be an image');
        },
        onFileSizeError: function(file){
          $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: size excess limit');
        },
        /*onFileExtError: function(file){
          $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' has a Not Allowed Extension');
        },*/
        onFallbackMode: function(message){
          $.danidemo.addLog('#demo-debug', 'info', 'Browser not supported(do something else here!): ' + message);
        }
      }); 
	  function listquotation(applicantid){
	$.ajax({
			url: config.BASE_URL+'inquiries/listquotation/',
			type: "POST",
			data:{'applicantid':applicantid },
			dataType: "html",
			success: function(response)
			{	
				$('#listquotation').html(response);
			 }
		});	
}
listquotation('<?php echo $_applicantid;?>');
function delete_data_system()
{
	check_my_session();
	
    var data_id 	=	$('#delete_id').val();
    var action_url 	=	$('#action_url').val();
    var rowid 		=	'#' + data_id + '_durar_lm';
	
	
    var delete_data_from_system = $.ajax({
        url: action_url,
        dataType: "html",
        success: function (msg) 
		{
            //$('#tableSortable').dataTable().fnDestroy();
            show_notification('لقد تم حذف سجل');
            $('#deleteDiag').modal('hide');
			listquotation('<?php echo $_applicantid;?>');
            //$(rowid).slideUp('slow');
        }
    });
}
</script>
</body>
</html>
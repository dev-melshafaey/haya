<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
       
        <div class="nav nav-tabs panel panel-default panel-block">
          <div class="tab-pane list-group active">           
            <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th>رقم</th>
                  <th>الإسم</th>
                  <th>نوع المساعدات</th>
                  <th>فرع</th>
                  <th>البطاقة الشخصة</th>
                  <th>المحافظة</th>
                  <th>ولاية</th>
                  <th>عاجل</th>                 
                  <th>تاريخ التسجيل</th>
                  <th>الإجرائات</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'inquiries/socilservay_list_special_slt/'.$charity_type,'columns_array'=>'
				{ "data": "رقم" },
                { "data": "الإسم" },
                { "data": "نوع المساعدات" },
                { "data": "فرع" },
                { "data": "البطاقة الشخصة" },
				{ "data": "المحافظة" },
				{ "data": "ولاية" },
				{ "data": "عاجل" },
				{ "data": "تاريخ التسجيل" },
                { "data": "الإجرائات" }')); ?>
</div>
</body>
</html>
<!doctype html>
<?PHP
  	$applicant = $applicant_data['applicants'];
	$project = $applicant_data['applicant_project'][0];
	$loan = $applicant_data['applicant_loans'][0];
	$phones = $applicant_data['applicant_phones'];
	$comitte = $applicant_data['comitte_decision'][0];
	$evolution['applicants'] = $applicant; 
	$evolution['loan'] = $loan; 	
	$fullname = $applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name;
	foreach($phones as $p)
	{	$ar[] = '986'.$p->applicant_phone;	}
		$applicantphone = implode('<br>',$ar);

?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
  <form id="validate_form_data" name="validate_form_data" method="post" action="<?PHP echo base_url().'inquiries/add_zyarat' ?>" autocomplete="off">
    <input type="hidden" name="form_step" id="form_step" value="5" />
    <input type="hidden" name="applicant_id" id="applicant_id" value="<?php echo $applicant_id;?>" />
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="list-group">
            <div class="list-group-item" id="input-fields">
              <div class="row">
                <div class="form-group">
                  <h4 class="section-title preface-title text-warning"> <?PHP echo $fullname; ?> <span class="label label-default">رقم التسجيل <?php echo arabic_date(applicant_number($applicant_id)); ?></span></h4>
                </div>
                <div class="col-md-2">
                  <label class="text-warning">لنشاط:</label>
                  <label><strong><?php echo is_set($project->activity_project_text); ?></strong></label>
                </div>
                <div class="col-md-2">
                  <label class="text-warning">مبلغ القرض:</label>
                  <label><strong><?php echo arabic_date(number_format($loan->loan_amount,0)); ?></strong></label>
                </div>
                <div class="col-md-2">
                  <label class="text-warning">الولاية:</label>
                  <label><strong><?php echo is_set(show_data('Walaya',$applicant->walaya)); ?></strong></label>
                </div>
                <div class="col-md-2">
                  <label class="text-warning">رقم الهاتف:</label>
                  <label><strong><?php echo $applicantphone; ?></strong></label>
                </div>
                <div class="col-md-2">
                  <label class="text-warning">نوع البرنامج:</label>
                  <label><strong><?php echo is_set(show_data('LoanLimit',$loan->loan_limit)); ?></strong></label>
                </div>
              </div>
              <?PHP if($sad->is_musanif=='مصنف') { ?>
              <div class="row setMargin">
                <div class="col-md-11">
                  <input class="addpartnerphone btn btn-lg btn-success" id="addnewmusanif" value="إضافة" type="button"  onclick="addMusanif()">
                </div>
              </div>
              <?PHP } ?>
              <div class="row" id="technical">
                <?php
				//echo "<pre>";
				//print_r($study_data);
				
              if(!empty($zyarat_data))
			  {
				  $lastIndex = count($zyarat_data)-1;
				  $complete_data = $zyarat_data[$lastIndex]->is_complete;
				  $visit_counter = 1;
				  $i = 0;
				  foreach($zyarat_data as $i => $zyarat )
				  {
			?>
                <div class="row setMargin" id="zyara_detail<?php echo $i; ?>">
                  <input type="hidden" value="<?PHP echo rQuote($zyarat->zyarat_id); ?>" name="zyarat_id[]" id="zyarat_id[]"  class="txt_field"/>
                  <div class="col-md-12">
                    <div class="form-group">
                      <h4 class="section-title preface-title text-warning"> الزيارة <?php echo arabic_date($visit_counter); ?></h4>
                    </div>
                    <div class="col-md-4">
                      <label class="text-warning">قيمة الإجار الشهري:</label>
                      <label>
                        <input type="text" value="<?PHP echo rQuote($zyarat->monthly_rent); ?>" name="monthly_rent[]" id="monthly_rent[]"  class="form-control"/>
                      </label>
                    </div>
                    <div class="col-md-3">
                      <label class="text-warning">اخرى:</label>
                      <label>
                        <input type="text" value="<?PHP echo rQuote($zyarat->monthly_other_rent); ?>" name="monthly_other_rent[]" id="monthly_other_rent[]" class="form-control"/>
                      </label>
                    </div>
                    <div class="col-md-2">
                      <label class="text-warning">الكهرباء: </label>
                      <label>
                      <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-success <?PHP if(rQuote($zyarat->is_electricity)=='نعم') { ?>active<?PHP } ?>">
                          <input <?PHP if(rQuote($zyarat->is_electricity)=='نعم') { ?> checked="checked"<?PHP } ?> type="radio" name="is_electricity[<?php echo $i ?>]"  class="sForm" value="نعم">
                          نعم </label>
                        <label class="btn btn-success <?PHP if(rQuote($zyarat->is_electricity)=='لا') { ?>active<?PHP } ?>">
                          <input <?PHP if(rQuote($zyarat->is_electricity)=='لا') { ?> checked="checked"<?PHP } ?> type="radio" name="is_electricity[<?php echo $i ?>]" class="sForm"  value="لا">
                          لا </label>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <label class="text-warning">الماء: </label>
                      <label>
                      <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-info <?PHP if(rQuote($zyarat->is_water)=='نعم') { ?>active<?PHP } ?>">
                          <input <?PHP if(rQuote($zyarat->is_water)=='نعم') { ?> checked="checked"<?PHP } ?> type="radio" name="is_water[<?php echo $i ?>]"  class="sForm" value="نعم">
                          نعم </label>
                        <label class="btn btn-info <?PHP if(rQuote($zyarat->is_water)=='لا') { ?>active<?PHP } ?>">
                          <input <?PHP if(rQuote($zyarat->is_water)=='لا') { ?> checked="checked"<?PHP } ?> type="radio" name="is_water[<?php echo $i ?>]" class="sForm"  value="لا">
                          لا </label>
                      </div>
                      </label>
                    </div>
                  </div>
                
                <div class="row setMargin">
                  <div class="col-md-12">
                    <div class="col-md-4">
                      <label class="text-warning">هل المقر مناسب للمشروع؟: </label>
                      <label>
                      <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-warning <?PHP if(rQuote($zyarat->is_suitable)=='مناسب') { ?>active<?PHP } ?>">
                          <input <?PHP if(rQuote($zyarat->is_suitable)=='مناسب') { ?> checked="checked"<?PHP } ?> type="radio" name="is_suitable[<?php echo $i ?>]"  class="sForm" value="مناسب">
                          مناسب </label>
                        <label class="btn btn-warning <?PHP if(rQuote($zyarat->is_suitable)=='غيرمناسب') { ?>active<?PHP } ?>">
                          <input <?PHP if(rQuote($zyarat->is_suitable)=='غيرمناسب') { ?> checked="checked"<?PHP } ?> type="radio" name="is_suitable[<?php echo $i ?>]" class="sForm"  value="غيرمناسب">
                          غيرمناسب </label>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <label class="text-warning">المساحة الجملية للمقر (م2):</label>
                      <label>
                        <input type="text" value="<?PHP echo rQuote($zyarat->fine_headquarter); ?>" name="fine_headquarter[]" id="fine_headquarter"  class="form-control"/>
                      </label>
                    </div>
                    <div class="col-md-4">
                      <label class="text-warning">منها مغطاة (م2):</label>
                      <label>
                        <input type="text" value="<?PHP echo rQuote($zyarat->which_covered); ?>" name="which_covered[]" id="which_covered"  class="form-control"/>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row setMargin">
                  <div class="col-md-12">
                    <div class="col-md-9">
                      <label class="text-warning">الرأي الفني: </label>
                      <textarea name="technical_notes[]" id="technical_notes"  class="form-control"><?PHP echo rQuote($zyarat->technical_notes); ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row setMargin">
                  <div class="col-md-12">
                    <div class="col-md-9">
                      <label class="text-warning">ملاحظات الزيارة: </label>
                      <textarea name="visit_notes[]" id="visit_notes"  class="form-control"><?PHP echo rQuote($zyarat->visit_notes); ?></textarea>
                    </div>
                    <div class="col-md-2">
                      <label class="text-warning">&nbsp;&nbsp;</label>
                      <button type="button" style="margin-top: 25px;" onClick="removezyara('<?php echo $i ?>','<?php echo $zyarat->zyarat_id; ?>');" id="remove" class="btn btn-danger"><i class="icon-remove-sign"></i></button>
                    </div>
                  </div>
                </div>
                <div class="row setMargin">
                  <div class="col-md-12">
                    <div class="col-md-9">
                      <label class="text-warning">التوصيات: </label>
                      <textarea name="notes[]" id="problem_notes"  class="form-control"><?PHP echo rQuote($zyarat->notes); ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row setMargin" id="type_value2">
                  <div class="col-md-12">
                    <div class="col-md-9">
                      <label class="text-warning">تحميل: <?php echo $zyarat->attachment;?></label>
                      <label>
                      <div id="drag<?php echo $i; ?>" class="uploader">
                        <div class="browser">
                          <input type="file" name="files[]" multiple title='تحميل'>
                        </div>
                      </div>
                      </label>
                      <div id="show-image" class="show-image">
						<?PHP if($zyarat->attachment) { ?>
        
                                <!--<input type="hidden" name="document_name_8" id="document_name_8" value="<?php echo $zyarat->attachment; ?>" class="doc8">-->
                                <a  title="موافقة صورة " href="<?php echo base_url() ?>/upload_files/documents/<?php echo $zyarat->attachment; ?>" rel="gallery1" class="fancybox-button doclink">
                                    <img style="width:200px;" src="<?php echo base_url(); ?>upload_files/documents/<?php echo $zyarat->attachment; ?>">
                                </a>
                                <i class="delete-icon icon-remove-sign doc8remove0" style="color:#CC0000;cursor:pointer" onclick="deleteDoc(this,'<?php echo $zyarat->attachment; ?>');"></i>
                               <!--<i class="icon-remove-sign doc8remove" style="color:#CC0000;cursor:pointer"  onclick="deleteFile('<?php echo $zyarat->attachment; ?>')"></i>-->
        
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row setMargin">
                  <div class="col-md-12">
                    <div class="col-md-9">
                      <label class="text-warning">الصرف: </label>
                      <label>
                        <input type="checkbox"  id="is_surf" name="is_surf" value="1" <?php if($zyarat->is_surf) { ?> checked="checked" <?php } ?>/>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row setMargin uploader surf" id="drag1" <?php if(!$zyarat->is_surf) { ?>style="display:none;"<?php } ?>>
                  <div class="col-md-12">
                    <div class="col-md-9">
                      <label class="text-warning">الصرف تحميل:</label>
                      <label>
                      <div id="drag<?php echo $i; ?>" class="uploader">
                        <div class="browser">
                          <input type="file" name="files[]" multiple title='تحميل'>
                        </div>
                      </div>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row setMargin">
                  <div class="col-md-12">
                    <div class="col-md-9 panel-body demo-panel-files" id="demo-files<?php echo $i; ?>">
                      <span class="demo-note"></span>
                    </div>
                  </div>
                </div>
                
              </div>
              <?PHP 
			  $visit_counter++;
			  	} 
			  }
			  else
			  { 
			  ?>
              <div class="row setMargin">
                  
                  <div class="col-md-12">
                    <div class="form-group">
                      <h4 class="section-title preface-title text-warning"> الزيارة <?php echo arabic_date($zyarat_count); ?></h4>
                    </div>
                    <div class="col-md-4">
                      <label class="text-warning">قيمة الإجار الشهري:</label>
                      <label>
                        <input type="text" value="<?PHP echo rQuote($zyarat->monthly_rent); ?>" name="monthly_rent[]" id="monthly_rent[]"  class="form-control"/>
                      </label>
                    </div>
                    <div class="col-md-3">
                      <label class="text-warning">اخرى:</label>
                      <label>
                        <input type="text" value="<?PHP echo rQuote($zyarat->monthly_other_rent); ?>" name="monthly_other_rent[]" id="monthly_other_rent[]" class="form-control"/>
                      </label>
                    </div>
                    <div class="col-md-2">
                      <label class="text-warning">الكهرباء: </label>
                      <label>
                      <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-success <?PHP if(rQuote($zyarat->is_electricity)=='نعم') { ?>active<?PHP } ?>">
                          <input <?PHP if(rQuote($zyarat->is_electricity)=='نعم') { ?> checked="checked"<?PHP } ?> type="radio" name="is_electricity[]"  class="sForm" value="نعم">
                          نعم </label>
                        <label class="btn btn-success <?PHP if(rQuote($zyarat->is_electricity)=='لا') { ?>active<?PHP } ?>">
                          <input <?PHP if(rQuote($zyarat->is_electricity)=='لا') { ?> checked="checked"<?PHP } ?> type="radio" name="is_electricity[]" class="sForm"  value="لا">
                          لا </label>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <label class="text-warning">الماء: </label>
                      <label>
                      <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-info <?PHP if(rQuote($zyarat->is_water)=='نعم') { ?>active<?PHP } ?>">
                          <input <?PHP if(rQuote($zyarat->is_water)=='نعم') { ?> checked="checked"<?PHP } ?> type="radio" name="is_water[<?php echo $i ?>]"  class="sForm" value="نعم">
                          نعم </label>
                        <label class="btn btn-info <?PHP if(rQuote($zyarat->is_water)=='لا') { ?>active<?PHP } ?>">
                          <input <?PHP if(rQuote($zyarat->is_water)=='لا') { ?> checked="checked"<?PHP } ?> type="radio" name="is_water[<?php echo $i ?>]" class="sForm"  value="لا">
                          لا </label>
                      </div>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row setMargin">
                  <div class="col-md-12">
                    <div class="col-md-4">
                      <label class="text-warning">هل المقر مناسب للمشروع؟: </label>
                      <label>
                      <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-warning <?PHP if(rQuote($zyarat->is_suitable)=='مناسب') { ?>active<?PHP } ?>">
                          <input <?PHP if(rQuote($zyarat->is_suitable)=='مناسب') { ?> checked="checked"<?PHP } ?> type="radio" name="is_suitable[<?php echo $i ?>]"  class="sForm" value="مناسب">
                          مناسب </label>
                        <label class="btn btn-warning <?PHP if(rQuote($zyarat->is_suitable)=='غيرمناسب') { ?>active<?PHP } ?>">
                          <input <?PHP if(rQuote($zyarat->is_suitable)=='غيرمناسب') { ?> checked="checked"<?PHP } ?> type="radio" name="is_suitable[<?php echo $i ?>]" class="sForm"  value="غيرمناسب">
                          غيرمناسب </label>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <label class="text-warning">المساحة الجملية للمقر (م2):</label>
                      <label>
                        <input type="text" value="<?PHP echo rQuote($zyarat->fine_headquarter); ?>" name="fine_headquarter[]" id="fine_headquarter"  class="form-control"/>
                      </label>
                    </div>
                    <div class="col-md-4">
                      <label class="text-warning">منها مغطاة (م2):</label>
                      <label>
                        <input type="text" value="<?PHP echo rQuote($zyarat->which_covered); ?>" name="which_covered[]" id="which_covered"  class="form-control"/>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row setMargin">
                  <div class="col-md-12">
                    <div class="col-md-9">
                      <label class="text-warning">الرأي الفني: </label>
                      <input type="text" value="<?PHP echo rQuote($zyarat->technical_notes); ?>" name="technical_notes[]" id="technical_notes"  class="form-control"/>
                    </div>
                  </div>
                </div>
                <div class="row setMargin">
                  <div class="col-md-12">
                    <div class="col-md-9">
                      <label class="text-warning">ملاحظات الزيارة: </label>
                      <input type="text" value="<?PHP echo rQuote($zyarat->visit_notes); ?>" name="visit_notes[]" id="visit_notes"  class="form-control"/>
                    </div>
                    <div class="col-md-2">
                      <label class="text-warning">&nbsp;&nbsp;</label>
                      <button type="button" style="margin-top: 25px;" onClick="removezyara('<?php echo $i ?>','<?php echo $zyarat->zyarat_id; ?>');" id="remove" class="btn btn-danger"><i class="icon-remove-sign"></i></button>
                    </div>
                  </div>
                </div>
                <div class="row setMargin">
                  <div class="col-md-12">
                    <div class="col-md-9">
                      <label class="text-warning">التوصيات: </label>
                      <input type="text" value="<?PHP echo rQuote($zyarat->notes); ?>" name="notes[]" id="problem_notes"  class="form-control"/>
                    </div>
                  </div>
                </div>
                <div class="row setMargin" id="type_value2">
                  <div class="col-md-12">
                    <div class="col-md-9">
                      <label class="text-warning">تحميل: </label>
                      <label>
                      <div id="drag<?php echo $i; ?>" class="uploader">
                        <div class="browser">
                          <input type="file" name="files[]" multiple title='تحميل'>
                        </div>
                      </div>
                      </label>
                    </div>
                  </div>
                </div>
                <?PHP if($zyarat_count>0) { ?>
                <div class="row setMargin">
                  <div class="col-md-12">
                    <div class="col-md-9">
                      <label class="text-warning">الصرف: </label>
                      <label>
                        <input type="checkbox"  id="is_surf" name="is_surf" value="1" <?php if($zyarat->is_surf) { ?> checked="checked" <?php } ?>/>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row setMargin uploader surf" id="drag1" <?php if(!$zyarat->is_surf) { ?>style="display:none;"<?php } ?>>
                  <div class="col-md-12">
                    <div class="col-md-9">
                      <label class="text-warning">الصرف تحميل:</label>
                      <label>
                      <div id="drag<?php echo $i; ?>" class="uploader">
                        <div class="browser">
                          <input type="file" name="files[]" multiple title='تحميل'>
                        </div>
                      </div>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row setMargin" style="display:none;">
                  <div class="col-md-12">
                    <div class="col-md-9 panel-body demo-panel-files" id="demo-files<?php echo $i; ?>">
                      <span class="demo-note"></span>
                    </div>
                  </div>
                </div>
              <?PHP } 
			  } ?>
              </div>
              <div class="row setMargin">
              <div class="col-md-3">
                      <label class="text-warning">انقر هنا لتحميل: </label>
                      <label>
                      <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-info <?PHP if(rQuote($study_data->is_complete)=='1') { ?>active<?PHP } ?>">
                          <input type="radio" <?PHP if(rQuote($study_data->is_complete)=='1') { ?> checked="checked"<?PHP } ?>  name="is_complete"  class="sForm" value="1" onClick="checkComplete(this.value)"/>
                          مكتمل </label>
                        <label class="btn btn-info <?PHP if(rQuote($study_data->is_complete)=='0') { ?>active<?PHP } ?>">
                          <input type="radio" <?PHP if(rQuote($study_data->is_complete)=='0') { ?> checked="checked"<?PHP } ?>  name="is_complete" class="sForm"  value="0"  onclick="checkComplete(this.value)"/>
                          غيرمكتمل</label>
                      </div>
                      </label>
                    </div>
               
              </div>
            </div>
          </div>
          <br />
          <div class="row">
            <div class="col-md-12"  style="padding: 19px 50px;">
              <input type="hidden" id="attachment" name="attachment" />
              <button type="button" id="save_data_form_five" name="save_data_form_five" class="btn btn-success btn-lg">حفظ</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    </div>
  </form>
  </div>
</section>
<?php //echo '<pre>'; print_r($zyarat);?>
<?php $this->load->view('common/footer'); ?>
</body>
</html>
<?php $steps	=	 config_item('steps');?>
<div class="row col-md-12">
  <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
    <thead>
      <tr role="row">
        <th>رقم</th>
        <th>الإسم</th>
        <th>Step #</th>
        <th>البطاقة الشخصة</th>
        <th>تاريخ التسجيل</th>
        <th>الإجرائات</th>
      </tr>
    </thead>
    <tbody role="alert" aria-live="polite" aria-relevant="all">
      <?php if(!empty($all_helps)):?>
      <?php foreach($all_helps as $help):?>
      <?php $actions	.=	' <a class="iconspace" href="'.charity_edit_url($help->charity_type_id).$help->applicantid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>'; ?>
      <tr role="row">
        <td  style="text-align:center;"><?php echo arabic_date($help->applicantcode);?></td>
        <td  style="text-align:center;"><?php echo '<a class="iconspace" href="'.charity_edit_url($help->charity_type_id).$help->applicantid.'">'.$help->fullname.'</a>';?></td>
        <td  style="text-align:center;"><?php echo $steps[$help->step];?></td>
        <td  style="text-align:center;"><?php echo arabic_date($help->idcard_number);?></td>
        <td  style="text-align:center;"><?php echo arabic_date($help->registrationdate);?></td>
        <td><?php echo $actions;?></td>
      </tr>
      <?php unset($actions); endforeach;?>
      <?php endif;?>
    </tbody>
  </table>
</div>
<!doctype html>
<?PHP
	$ah_applicant 						=	$_applicant_data['ah_applicant'];
	$ah_applicant_wife 					=	$_applicant_data['ah_applicant_wife'];
	$ah_applicant_survayresult 			=	$_applicant_data['ah_applicant_survayresult'];
	$ah_applicant_relation 				=	$_applicant_data['ah_applicant_relation'];
	$ah_applicant_family 				=	$_applicant_data['ah_applicant_family'];
	$ah_applicant_economic_situation	=	$_applicant_data['ah_applicant_economic_situation'];
	$ah_applicant_documents				=	$_applicant_data['ah_applicant_documents'];	
	$ah_applicants_loans 				=	$_applicant_data['ah_applicants_loans'];
	//echo '<pre>'; print_r();exit($ah_applicant);
	
?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <?php $msg	=	$this->session->flashdata('msg');?>
  <?php if($msg):?>
      <div class="col-md-12">
      	<div style="padding: 22px 20px !important; background:#c1dfc9;">
        	<h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
        </div>
      </div>
  <?php endif;?>
  <div class="row">
  <form id="add_decission" name="add_decission" method="post" action="<?PHP echo base_url().'inquiries/add_decission' ?>" autocomplete="off">   
    <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $ah_applicant->applicantid;?>" />
    <input type="hidden" name="sarvayid" id="sarvayid" value="<?php echo $ah_applicant_survayresult->sarvayid;?>" />
    <input type="hidden" name="steptype" id="steptype" value="<?php echo $type;?>" />
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module, 'headxxx' => $heading)); ?>
      <div class="col-md-12">
        <div class="col-md-5 fox leftborder">
          <h4 class="panel-title customhr">بيانات مقدم الطلب</h4>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الاستمارة: </label>
            <strong><?PHP echo arabic_date($ah_applicant->applicantcode); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">التاريخ: </label>
            <strong><?PHP echo show_date($ah_applicant->registrationdate,5); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">نوع الطلب: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->charity_type_id); ?> </strong> </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الاسم الرباعي والقبيلة: </label>
            <strong><?PHP echo $ah_applicant->fullname; ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم جواز سفر: </label>
            <strong><?PHP echo arabic_date($ah_applicant->passport_number); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الرقم المدني: </label>
            <strong><?PHP echo arabic_date($ah_applicant->idcard_number); ?> </strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الدولة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->country); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">المحافظة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->province); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">ولاية: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->wilaya); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">البلدة / المحلة: </label>
            <strong><?PHP echo $ah_applicant->address; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">هاتف المنزل: </label>
            <strong><?PHP echo arabic_date($ah_applicant->hometelephone); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الهاتف :</label>
            <strong><?PHP echo arabic_date(implode('<br>',json_decode($ah_applicant->extratelephone,TRUE))); ?></strong></div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة الجتماعية:</label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->marital_status); ?></strong> </div>
          <?PHP foreach($ah_applicant_wife as $wife) { ?>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم الزوج/الزوجة: </label>
            <strong><?PHP echo $wife->relationname; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الجواز/الرقم المدني: </label>
            <strong><?PHP echo arabic_date($wife->relationdoc); ?></strong> </div>
          <?PHP } ?>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم الام: </label>
            <strong><?PHP echo $ah_applicant->mother_name; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الجواز\البطاقة: </label>
            <strong><?PHP echo arabic_date($ah_applicant->mother_id_card); ?></strong> </div>
          <!-----------------Documents-------------------------->
          <!---------------------------------------------------->
          <div class="col-md-12 form-group">
            <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
              <h4 class="panel-title customhr">الوثائق والمستندات المرفقة مع طلب المساعدة</h4>
              <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                <?PHP 
					$doccount = 0;					
					foreach($this->inq->allRequiredDocument($ah_applicant->charity_type_id) as $ctid) { $doccount++; 
						$doc = $ah_applicant_documents[$ctid->documentid];						
						$url = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$doc->document;
					?>
                <div class="panel panel-default" style="border-bottom: 1px solid #ddd;">
                  <div class="panel-heading" style="padding:7px 3px !important;" id="head<?PHP echo $ctid->documentid;?>">
                    <h4 class="panel-title" style="font-size:12px; font-weight:normal !important;">
                      <?PHP if($doc->appli_doc_id!='') { ?>
                      <span class="icons" id="removeicons<?PHP echo $doc->appli_doc_id; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> </span>
                      <?PHP } ?>
                      <a class="accordion-toggle fixpopup collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $ctid->documentid;?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                  </div>
                </div>
                <?PHP } ?> 
              </div>
            </div>
          </div> 
          <h4 class="panel-title customhr">افراد الأسرة: (بمن فيهم العاملين و غيرالعاملين) </h4>
          <div class="col-md-12 form-group">
            <table class="table table-bordered table-striped dataTable">
              <thead>
                <tr role="row">
                  <th class="right">الاسم</th>
                  <th class="center">السن</th>
                  <th class="center">سنة الميلاد</th>
                  <th class="right">القرابة</th>
                  <th class="right">المهنة</th>
                  <th class="center">الدخل الشهري</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?PHP 
			  
			  if($ah_applicant_relation)
			  {	
			  	foreach($ah_applicant_relation as $apr) { ?>
                <tr>
                  <td class="right"><?PHP echo $apr->relation_fullname; ?></td>
                  <td class="center"><?PHP echo a_date($apr->age); ?></td>
                  <td class="center"><?PHP echo a_date($apr->birthyear); ?></td>
                  <td class="right"><?PHP echo $this->haya_model->get_name_from_list($apr->relationtype); ?></td>
                  <td class="right"><?PHP echo $this->haya_model->get_name_from_list($apr->professionid); ?></td>
                  <td class="center"><?PHP echo number_format($apr->monthly_income,3); ?></td>
                </tr>
                <?php }
			  }
			 ?>
              </tbody>
            </table>
          </div>
          <h4 class="panel-title customhr">الحالة الاقتصادية: </h4>
          <div class="col-md-12 form-group">
            <label class="text-warning">مصدر الدخل:</label>
            <?PHP echo $ah_applicant->income; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">قيمة الدخل:</label>
            <?PHP echo number_format($ah_applicant->salary,3); ?></strong> </div>
          <div class="col-md-6 form-group">
            <?PHP if($ah_applicant->source=='يوجد') { 
				echo('<label class="text-warning">يوجد</label> : يوجد مصدر دخل آخر</strong>'); 
			} elseif($ah_applicant->source=='لايوجد') { 
				echo('<label class="text-warning">لايوجد</label> : لايوجد مصدر دخل آخر</strong>'); 
			} ?>
          </div>
          <?PHP if($ah_applicant->source=='يوجد') { 
		  			$totalQyama = $ah_applicant->salary+$ah_applicant->qyama+$ah_applicant->qyama_2+$ah_applicant->qyama_3+$ah_applicant->qyama_4;
		  
		  ?>
          <div class="col-md-12 form-group">
            <table width="100%" class="table" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>&nbsp;</td>
            <td class="right"><label class="text-warning">مصدره</label></td>
            <td class="center"><label class="text-warning">قيمته</label></td>
          </tr>
          <tr>
            <td class="center">1.</td>
            <td class="right"><?PHP echo $ah_applicant->musadra; ?></td>
            <td class="center"><?php echo number_format($ah_applicant->qyama,3); ?></td>
          </tr>
          <tr>
            <td class="center">2.</td>
            <td class="right"><?php echo $ah_applicant->musadra_2; ?></td>
            <td class="center"><?php echo number_format($ah_applicant->qyama_2,3); ?></td>
          </tr>
          <tr>
            <td class="center">3.</td>
            <td class="right"><?php echo $ah_applicant->musadra_3; ?></td>
            <td class="center"><?php echo number_format($ah_applicant->qyama_3,3); ?></td>
          </tr>
          <tr>
            <td class="center">4.</td>
            <td class="right"><?PHP echo $ah_applicant->musadra_4; ?></td>
            <td class="center"><?php echo number_format($ah_applicant->qyama_4,3); ?></td>
          </tr>
          <tr>
            <td class="center"></td>
            <td class="left"><label class="text-warning">مجموع الدخل:</label></td>
            <td class="center"><?PHP echo number_format($totalQyama,3); ?></td>
          </tr>
      </table> </div>
          <?PHP } ?>
		  <?PHP if($ah_applicant->q_source=='يوجد') { ?>    
            <h4 class="panel-title customhr">القروض: (<?PHP echo $ah_applicant->q_source; ?>)</h4>
              <div class="col-md-12 form-group">
          <table class="table">
                  <thead>
                    <tr role="row">
                      <th class="right">نوع القرض</th>
                      <th class="center">قبمة القرض</th>
                      <th class="center">القسط الشهري</th>                
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?PHP 
                  
                  if($ah_applicants_loans)
                  {
                        $apr_req = 0;
                        foreach($ah_applicants_loans as $loans) 
                        {
                            //$lmaount += $loans->loan_amount; // Before
                            $loan_limit += $loans->loan_limit; // Now i changed
                  ?>
                    <tr>
                      <td class="right"><?PHP echo $loans->loan_type;  ?></td>
                      <td class="center"><?PHP echo number_format($loans->loan_amount,3);  ?></td>
                      <td class="center"><?PHP echo number_format($loans->loan_limit,3);  ?></td>                 
                    </tr>
                    <?PHP $apr_req++; }
                    }
                  
                            //$totalSafi = $totalQyama-$lmaount; // Before
                            $totalSafi = $totalQyama-$loan_limit; // Now i changed
                    
                    ?>
                    <tr>
                      <td class="right"></td>
                      <td class="center" style="background:#090;"><label class="panel-title"><strong>صافي الدخل</strong></label></td>
                      <td class="center" style="background:#090;"><?PHP echo number_format($totalSafi,3); ?></td>
                    </tr>
                  </tbody>
                </table>
          </div>
          <?PHP } ?>
         
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة السكنية:</label>
            <?PHP echo $ah_applicant->current_situation; ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning"><?PHP echo $ah_applicant->ownershiptype; ?></strong> </label>
            <?PHP echo number_format($ah_applicant->ownershiptype_amount,3); ?>
          </div>
          
        </div>
        
        <div class="col-md-7 fox">  
          
          <h4 class="panel-title customhr">مكونات المنزل:</h4>          
          <div class="col-md-4 form-group">
            <label class="text-warning">نوع البناء:</label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->building_type); ?></strong> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">عدد الغرف:</label>
            <strong><?PHP echo arabic_date($ah_applicant->number_of_rooms); ?></strong> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">المرافق:</label>
            <strong><?PHP echo arabic_date($ah_applicant->utilities); ?></strong> </div>
          <div class="col-md-12 form-group"> 
            <label class="text-warning">حالة الأثاث والموجودات:</label> 
            <strong><?PHP echo $ah_applicant->furniture; ?></strong> </div>
            
          <?PHP if($ah_applicant->charity_type_id==81) { ?> 
          <h4 class="panel-title customhr">البيانات الدراسية:</h4>          
          	<div class="col-md-4 form-group">
            <label class="text-warning">شهادة ثنوية:</label>
            <strong><?PHP echo $ah_applicant->certificate_dualism; ?></strong> </div>
           	<div class="col-md-4 form-group">
            <label class="text-warning">سنة التخرج:</label>
            <strong><?PHP echo $ah_applicant->graduation_year; ?></strong> </div>
            <div class="col-md-4 form-group">
            <label class="text-warning">النسبة :</label>
            <strong><?PHP echo $ah_applicant->school_percentage; ?></strong> </div>
          	<br clear="all">
            <h4 class="panel-title customhr">البيانات الجامعية :</h4>
          
          	<div class="col-md-4 form-group">
            <label class="text-warning">اسم الكلية أو الجامعة:</label>
            <strong><?PHP echo $ah_applicant->college_name; ?></strong> </div>
           	<div class="col-md-4 form-group">
            <label class="text-warning">التخصص :</label>
            <strong><?PHP echo $ah_applicant->specialization; ?></strong> </div>
            <div class="col-md-4 form-group">
            <label class="text-warning">السنة لدراسية :</label>
            <strong><?PHP echo $ah_applicant->year_of_study; ?></strong> </div>
            <div class="col-md-4 form-group">
            <label class="text-warning">اخر معدل تراكمي :</label>
            <strong><?PHP echo $ah_applicant->last_grade_average; ?></strong> </div>
            <br clear="all">
         <?PHP } ?>   
            
         <?PHP if($ah_applicant->charity_type_id!=78) { ?>
         <h4 class="panel-title customhr">تفاصيل البنك:</h4> 
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم صاحب الحساب بالانجليزي:</label>
            <?PHP echo (isset($ah_applicant->holder_name) ? $ah_applicant->holder_name : '-- --'); ?>
          </div>       
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم البنك:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant->bankid); ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الفرع:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant->branchid); ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الحساب:</label>
            <?PHP echo $ah_applicant->accountnumber; ?>
          </div>
          <div class="col-md-6 form-group">
           <?PHP if($ah_applicant->charity_type_id==81) { ?>
            <label class="text-warning"><?PHP echo $ah_applicant->banksource; ?></label>            
            <?PHP } ?>
          </div>
          <br clear="all">
          <div class="col-md-6 form-group">
            <label class="text-warning">نوع الحساب: جاري / توفير: </label>
            <?PHP echo($ah_applicant->accounttype); ?>           
          </div>
          <?PHP if($ah_applicant->bankstatement) { ?>
          <div class="col-md-12 form-group">
            <label class="text-warning">يرفق صورة من كشف الحسابات (حديث الاصدار):</label>           
            <?PHP
          	$statmentURL	=	'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->bankstatement;
			echo getFileResult($statmentURL,'يرفق صورة من كشف الحسابات (حديث الاصدار)',$statmentURL);
		  ?>
          </div>
          <br clear="all">
          <?PHP }  } ?>
             
            <br clear="all"> 
          <h4 class="panel-title customhr">بعد إجراء البحث الاجتماعي والاطلاع على المستندات المؤيدة اتضح مايلي :-</h4>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة الصحية:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->health_condition); ?>
			
            
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة السكنية:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->housing_condition); ?>
            
            
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">عدد أفراد الأسرة:</label>
            <?PHP echo arabic_date($ah_applicant_survayresult->numberofpeople); ?>
            
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">ترتيب مقدم الطلب بالأسرة:</label>
            <?PHP echo arabic_date($ah_applicant_survayresult->positioninfamily); ?>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الحالة الاقتصادية:</label>
            <?PHP echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->economic_condition); ?>            
            
          </div>
          <div class="col-md-12 form-group">
            <h4 class="panel-title customhr">نوع الحالة (<?PHP echo $ah_applicant_survayresult->casetype; ?>):-</h4>           
         </div>
          <div class="col-md-12 form-group casetype" id="zamanya" <?PHP if($ah_applicant_survayresult->casetype=='للحالات الضمانية') { ?> style="display:block !important;" <?PHP } ?>>
            <div class="col-md-6 form-group">
              <label class="text-warning">الحالة الضمانية باسم:</label>
              <?PHP echo $ah_applicant_survayresult->aps_name; ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الحاسب:</label>
              <?PHP echo $ah_applicant_survayresult->aps_account; ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">رقم الملف:</label>
              <?PHP echo $ah_applicant_survayresult->aps_filename; ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">الفئة:</label>
              <?PHP echo $this->haya_model->get_name_from_list($ah_applicant_survayresult->aps_category); ?> </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">قيمة المعاش:</label>
              <?PHP echo number_format($ah_applicant_survayresult->aps_salaryamount,3); ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">تاريخ الربط:</label>
              <?PHP echo arabic_date(date('d/m/Y',strtotime($ah_applicant_survayresult->aps_date))); ?>
            </div>
            <div class="col-md-12 form-group">
              <label class="text-warning">مصادر دخل أخرى:</label>
              <?php $json = json_decode($ah_applicant_survayresult->aps_another_income,TRUE);
			  		$inx = 0;
			  		for($mdi=1; $mdi<=4; $mdi++) 
					{ 
						if($json[$inx]) 
						{ 
							echo('<li>'.number_format($json[$inx],3).'<li>');
							$inx++; 
						} 
					} 
				?>
            </div>
          </div>
          <div class="col-md-12 form-group casetype" id="ghairzamanya"  <?PHP if($ah_applicant_survayresult->casetype=='للحالات غير الضمانية') { ?> style="display:block !important;" <?PHP } ?>>
            <div class="col-md-12 form-group">
              <label class="text-warning">مصادر دخل أخرى:</label> <br clear="all" />
              <?PHP 
			  $json = json_decode($ah_applicant_survayresult->aps_another_income,TRUE);
			  
			  //echo '<pre>'; print_r( $json);
			  		$inxp = 0;
			  		for($mdix=1; $mdix<=4; $mdix++) 
					{	if($json[$inxp])
						{
							//echo('<li>'.$json[$inxp].'<li>');
							echo('<strong>. </strong>'.number_format($json[$inxp],3).'<br />');
							$inxp++; 
						}
					}?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">اجمالي الدخل الشهري للفرد:</label>
              <?PHP echo $ah_applicant_survayresult->aps_month; ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">اجمالي الدخل الشهري للاسرة:</label>
              <?PHP echo $ah_applicant_survayresult->aps_year; ?>
            </div>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الغرض من المساعدة:</label>
            <?PHP echo $ah_applicant_survayresult->whyyouwant; ?>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">ملخص الحالة:</label>
            <?PHP echo $ah_applicant_survayresult->summary; ?>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">رأي الباحث الاجتماعي و مبرراته:</label>
            <?PHP echo $ah_applicant_survayresult->review; ?>
          </div>
          <?PHP foreach($all_status as $asx) {?>
          <div class="col-md-12 form-group dicissionbox">
          		<div class="col-md-4"><label class="text-warning strong">الموظف : </label> <?PHP echo $asx->fullname; ?></div>
                <div class="col-md-4"><label class="text-warning strong">قرار : </label> <?PHP echo $asx->decission; ?></div>
                <div class="col-md-4"><label class="text-warning strong">التاريخ : </label> <?PHP echo $asx->decissiontime; ?></div>
                <div class="col-md-12"><label class="text-warning strong">الملاحظات : </label> <?PHP echo $asx->notes; ?></div>          	
          </div>
          <?PHP } ?>
          <div class="col-md-12 form-group">
			<?PHP foreach(applicant_status() as $askey => $as) { ?>
            <?php if($as	!=	'إحالة' && $as	!=	'رفض'):?>
              <div class="col-md-3 m_application_status">  
                <label class="text-warning"><?PHP echo $as; ?></label>
                <input type="radio" name="application_status" <?PHP if($current_status->decission==$as) { ?> checked <?PHP } ?> data-id="x<?PHP echo $askey; ?>" id="application_status" class="apstatus" value="<?PHP echo $as; ?>">
                </div>
                <?php endif;?>
            <?PHP } ?>
          </div>
          <div class="col-md-12 form-group" id="notess" <?PHP if($current_status->decission=="رفض") { ?>style="display:block;"<?PHP } ?>>
          	<textarea id="notes" name="notes"><?PHP echo $current_status->notes; ?></textarea>
          </div>
          <div class="col-md-12 form-group">
          <?PHP echo $this->haya_model->module_button(0,$ah_applicant->applicantid,'save_decission',$ah_applicant->case_close); ?>
         
          </div>
        </div>
        
      </div>
    </div>
     </form>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script src="<?PHP echo base_url(); ?>assets/js/tasgeel_js.js"></script>
</body>
</html>
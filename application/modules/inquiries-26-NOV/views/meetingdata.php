<?PHP
$ah_applicant = $_applicant_data['ah_applicant'];
$ah_applicant_wife = $_applicant_data['ah_applicant_wife'];
$ah_applicant_survayresult = $_applicant_data['ah_applicant_survayresult'];
$ah_applicant_relation = $_applicant_data['ah_applicant_relation'];
$ah_applicant_family = $_applicant_data['ah_applicant_family'];
$ah_applicant_economic_situation = $_applicant_data['ah_applicant_economic_situation'];
$ah_applicant_documents = $_applicant_data['ah_applicant_documents'];
$ah_applicant_decission = $_applicant_data['ah_applicant_decission'];

?>

  <div class="row" style="background: #FFF !important;">
 <div class="col-md-12 form-group">
          	<fieldset><legend>مقرر القائمة</legend>
            <?PHP
$cnt = sizeof($ah_applicant_decission);
$nmt = 0;
foreach ($ah_applicant_decission as $aad) {
    $nmt++; ?>
            	<div class="col-md-12 form-group" <?PHP if ($nmt != $cnt) { ?>style="border-bottom: 1px solid #C0C0C0; <?PHP if ($nmt ==
1) { ?>background-color: #EFE;<?PHP } ?>"<?PHP } ?>>
                	<div class="col-md-6 form-group" style="margin-bottom: 2px !important;"><label class="text-warning">مدير القرار :</label> <span class="smallfont"><?PHP echo
$aad->decission; ?></span></div>
                    <div class="col-md-6 form-group" style="margin-bottom: 2px !important;"><label class="text-warning">مدير الباحث :</label> <span class="smallfont"><a onclick="alatadad(this);" data-url="<?PHP echo
base_url(); ?>users/getUsersDetails/<?PHP echo
$aad->userid; ?>" href="#"><?PHP echo
$aad->fullname; ?></span></a></div>
                    <div class="col-md-6 form-group" style="margin-bottom: 2px !important;"><label class="text-warning">التاريخ :</label> <span class="smallfont"><?PHP echo
arabic_date($aad->decissiontime); ?></span></div>
                    <div class="col-md-6 form-group" style="margin-bottom: 2px !important;"><label class="text-warning">آى بى :</label> <span class="smallfont"><?PHP echo
arabic_date($aad->decissionip); ?></span></div>
                    <div class="col-md-12 form-group" style="margin-bottom: 2px !important;"><?PHP if ($aad->
notes) { ?><label class="text-warning">ملاحظات :</label> <span class="smallfont"><?PHP echo
$aad->notes; ?></span><?PHP } ?></div>
                </div>              
            <?PHP } ?>
            </fieldset>
          </div>
          <?PHP if ($ah_applicant->case_close == 0) { ?>
            <div class="col-md-12 form-group">
                <div class="col-md-3 form-group"><label class="text-warning">عرض في الاجتماع رقم :</label></div>
                <div class="col-md-9 form-group"><input name="meeting_number" placeholder="عرض في الاجتماع رقم" id="meeting_number" type="text" class="form-control NumberInput"></div>
            </div>
           <div class="col-md-12 form-group">
                <div class="col-md-3 form-group"><label class="text-warning">توصية اللجنة :</label></div>
			<?PHP foreach (applicant_status() as $askey => $as) { ?>
            <div class="col-md-2 form-group">
                <label class="text-warning"><?PHP echo $as; ?></label>
                <input type="radio" <?PHP if ($askey == 0) {
            echo ('checked');
        } ?> name="application_status" data-id="x<?PHP echo
$askey; ?>" id="application_status" class="apstatus" value="<?PHP echo
$as; ?>">
            </div>
            <?PHP } ?>
            <div class="col-md-3 form-group"></div>
          </div>
         
          <div class="col-md-12 form-group" id="notess">
          	<input name="notes" id="notes" type="text" class="form-control getnotesrelateddata">
          </div>
           <div class="col-md-12 form-group" id="moneydiv">
            <div class="col-md-3 form-group">الفئة المستحقة : </div>
            <div class="col-md-9 form-group" id="moneylist"></div>
          </div>
          <div class="col-md-12 form-group">
                <div class="col-md-3 form-group"><label class="text-warning">الملاحظات والقرار :</label></div>
                <div class="col-md-9 form-group"><textarea class="form-control" id="meeting_notes" name="meeting_notes"></textarea></div>
            </div>
       	  <?PHP } else { ?>
          <div class="col-md-12 form-group">
                <div class="col-md-3 form-group"><label class="text-warning">الملاحظات والقرار :</label></div>
                <div class="col-md-9 form-group"><?PHP echo $ah_applicant->
meeting_notes; ?></div>
            </div>
            
            </div>
          <?PHP } ?>

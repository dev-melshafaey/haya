<style>
.my_td {
	text-align: center!important;
}
</style>
<div class="col-md-12 form-group">
  <form action="<?php echo base_url(); ?>inquiries/proceed_contract" method="POST" id="step-2-form" name="committee_meeting" autocomplete="off" enctype="multipart/form-data">
  <table class="table table-bordered table-striped dataTable" id="females">
    <thead>
      <tr role="row">
        <th class="my_td" >تدفع بعد إنجاز نصــف الأعـمـــــال المطلوبة</th>
        <th class="my_td" align="center">كمية</th>
        <th class="my_td">ملاحظات</th>
        <th class="my_td">تحديد</th>
      </tr>
    </thead>
    <tbody role="alert" aria-live="polite" aria-relevant="all">
      <?php if(!empty($percent_amounts)):?>
      <?php foreach($percent_amounts	as $amount):?>
      <tr role="row">
        <td class="center"><?php echo $amount->percentage;?></td>
        <td class="center"><?php echo $amount->amount;?></td>
        <td class="right"><?php echo $amount->notes;?></td>
        <td>
        	<input value="<?php echo $amount->amount;?>" name="approve_amount" class="subject-list" type="checkbox" 
            
            <?php if($applicant_data->approve_amount	==	$amount->amount):?> checked="checked" <?php endif;?>>
        </td>
      </tr>
      <?php endforeach;?>
      <?php endif;?>
    </tbody>
  </table>
  <br clear="all" />
  <br clear="all" />

    <div class="form-group col-md-12">
      <label for="basic-input">الملاحظات</label>
      <textarea class="form-control" value="" placeholder="الملاحظات" name="contract_notes" id="contract_notes" rows="5" ><?php echo (isset($applicant_data->contract_notes)	?	$applicant_data->contract_notes : NULL);?></textarea>
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">المرفقات</label>
      <input type="file" class="form-control " name="contract_document" id="contract_document" placeholder="المرفقات :" />
    </div>
    <br clear="all" />
    <div class="form-group col-md-6">
      <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $applicantid;?>" />
      <button type="submit" id="gggg" class="btn btn-sm btn-success">تحديث</button>
    </div>
  </form>
  <br clear="all" />
</div>
<script type="text/javascript">
	    $('.subject-list').on('change', function() {
		    $('.subject-list').not(this).prop('checked', false);  
		});
    </script>
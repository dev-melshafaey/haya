<?php
	$ah_applicant 						=	$_applicant_data['ah_applicant'];
	$ah_applicant_wife 					= 	$_applicant_data['ah_applicant_wife'];
	$ah_applicant_survayresult 			= 	$_applicant_data['ah_applicant_survayresult'];
	$ah_applicant_relation 				= 	$_applicant_data['ah_applicant_relation'];
	$ah_applicant_family 				= 	$_applicant_data['ah_applicant_family'];
	$ah_applicant_economic_situation 	= 	$_applicant_data['ah_applicant_economic_situation'];
	$ah_applicant_documents 			= 	$_applicant_data['ah_applicant_documents'];
	$ah_applicants_loans 				=	$_applicant_data['ah_applicants_loans'];
?>
<?php $charity_type	=	 $this->haya_model->get_name_from_list($ah_applicant->charity_type_id); ?>

<div class="col-md-12" id="myprint"  style="direction:rtl;">
  <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tr>
      <td colspan="4" class="customhr">بيانات طلب مساعدة</td>
    </tr>
    <tr>
      <td class="w25 right"><label class="text-warning">رقم الاستمارة: </label>
        <br />
        <strong><?PHP echo a_date(applicant_number($ah_applicant->applicantcode)); ?> </strong></td>
      <td class="w25 right"><label class="text-warning">جنسية: </label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->country_listmanagement); ?> </strong></td>
      <td class="w25 right"><label class="text-warning">نوع الطلب: </label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->charity_type_id); ?> </strong></td>
      <td class="w25 right"><label class="text-warning">الاسم الرباعي والقبيلة (عمر): </label>
        <br />
        <strong><?PHP echo $ah_applicant->fullname; ?> (<?PHP echo a_date($ah_applicant->applicant_age); ?>)</strong></td>
    </tr>
    <tr>
      <td class="w25 right"><label class="text-warning">التاريخ: </label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->registrationdate,5); ?></strong></td>
      <td class="w25 right"><label class="text-warning">رقم جواز سفر: </label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->passport_number); ?> </strong></td>
      <td class="w25 right"><label class="text-warning">رقم البطاقة الشخصة: </label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->idcard_number); ?> </strong></td>
      <td class="w25 right"><label class="text-warning">الحالة الجتماعية: </label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->marital_status); ?></strong></td>
    </tr>
    <tr>
      <td class="w25 right"><label class="text-warning">المحافظة \ المنطقة:</label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->province); ?></strong></td>
      <td class="w25 right"><label class="text-warning">ولاية:</label>
        <br />
        <strong><?PHP echo $this->haya_model->get_name_from_list($ah_applicant->wilaya); ?> </strong></td>
      <td class="w25 right"><label class="text-warning">البلدة \ المحلة:</label>
        <br />
        <strong><?PHP echo $ah_applicant->address; ?> </strong></td>
      <td class="w25 right"><label class="text-warning">هاتف المنزل: </label>
        <br />
        <strong><?PHP echo a_date($ah_applicant->hometelephone); ?>, <?PHP echo a_date(implode(', ',json_decode($ah_applicant->extratelephone,TRUE))); ?></strong></td>
    </tr>
    <?php 
		$wifeCount = 0;
		
		foreach($ah_applicant_wife as $wife) 
		{
			$wifeCount++;	
	?>
    <tr>
      <td class="right"><label class="text-warning">اسم الزوج \ الزوجة <?PHP echo a_date($wifeCount); ?>:</label></td>
      <td class="right"><?php echo $wife->relationname; ?></td>
      <td class="right"><?php echo a_date($wife->relationdoc); ?></td>
      <td class="right"><?php echo $this->haya_model->get_name_from_list($wife->relationnationality); ?></td>
    </tr>
    <?php } ?>
    <tr>
      <td class="right"><label class="text-warning">اسم الام:</label></td>
      <td class="right"><?php echo $ah_applicant->mother_name; ?></td>
      <td class="right"><?php echo a_date($ah_applicant->mother_id_card); ?></td>
      <td class="right"></td>
    </tr>
    <tr>
      <td colspan="4" class="customhr">اترفق الوثائق والمستندات الازمة لطلب</td>
    </tr>
    <?php 
		$doccount = 0;					
		foreach($this->inq->allRequiredDocument($ah_applicant->charity_type_id) as $ctid) { $doccount++; 
			$doc = $ah_applicant_documents[$ctid->documentid];						
			$url = base_url().'resources/applicants/'.$ah_applicant->applicantcode.'/'.$doc->document;	?>
    <tr>
      <td colspan="3" class="right"><?php echo a_date($doccount); ?>. <?php echo $ctid->documenttype;?></td>
      <td class="center"><i class="myicon <?php if(file_exists($url)) { ?>alert-green<?php } ?> icon-ok"></i></td>
    </tr>
    <!--    <tr>
    	<td colspan="4"><?php echo filePreview($url,$ctid->documenttype); ?></td>
    </tr>-->
    <?php } ?>
    <tr>
      <td colspan="4" class="customhr">افراد الأسرة: (بما فهيم العاملين و غيرالعاملين)</td>
    </tr>
    <tr>
      <td class="right y_head">الاسم</td>
      <td class="right y_head">القرابة</td>
      <td class="right y_head">المهنة</td>
      <td class="right y_head">الدخل الشهري</td>
    </tr>
    <?php 
		if($ah_applicant_relation)
		{	foreach($ah_applicant_relation as $apr) { ?>
    <tr>
      <td class="right"><?php echo $apr->relation_fullname; ?> (<?php echo a_date($apr->age); ?>)</td>
      <td class="right"><?php echo $this->haya_model->get_name_from_list($apr->relationtype); ?></td>
      <td class="right"><?php echo $apr->profession; ?></td>
      <td class="right"><?php echo number_format($apr->monthly_income,3); ?> ر. ع</td>
    </tr>
    <?php }
		} 
	?>
    <tr>
      <td colspan="4" class="customhr">االحالة الاقتصادية:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">مصدر الدخل:</label>
        <br />
        <strong><?php echo number_format($ah_applicant->income,3); ?></strong></td>
      <td class="right"><label class="text-warning">قيمة الدخل:</label>
        <br />
        <strong><?php echo number_format($ah_applicant->salary,0); ?></strong></td>
      <td class="right"><label class="text-warning">يوجد:</label>
        <br />
        <?php if($ah_applicant->source=='يوجد') { echo('يوجد مصدر دخل آخر'); } elseif($ah_applicant->source=='لايوجد') { echo('لايوجد مصدر دخل آخر'); } ?></td>
      <td class="right"><?php if($ah_applicant->source=='يوجد') { ?>
        <label class="text-warning">قيمتة / مصدره:</label>
        <br />
        <strong><?php echo a_date($ah_applicant->qyama); ?> / <?php echo a_date($ah_applicant->musadra); ?></strong>
        <?php } ?>
        <?php if($ah_applicant->source=='لايوجد') { ?>
        <label class="text-warning">مصدر المعيشة:</label>
        <br />
        <strong><?php echo a_date($ah_applicant->lamusadra); ?></strong>
        <?php } ?></td>
    </tr>
    <?php $totalQyama = $ah_applicant->salary+$ah_applicant->qyama+$ah_applicant->qyama_2+$ah_applicant->qyama_3+$ah_applicant->qyama_4;?>
    <tr>
      <td colspan="4" class="right"><table width="100%" class="table" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>&nbsp;</td>
            <td class="right"><label class="text-warning">مصدره</label></td>
            <td class="center"><label class="text-warning">قيمته</label></td>
          </tr>
          <tr>
            <td class="center">1.</td>
            <td class="right"><?php echo $ah_applicant->musadra; ?></td>
            <td class="center"><?php echo number_format($ah_applicant->qyama,3); ?></td>
          </tr>
          <tr>
            <td class="center">2.</td>
            <td class="right"><?php echo $ah_applicant->musadra_2; ?></td>
            <td class="center"><?php echo number_format($ah_applicant->qyama_2,3); ?></td>
          </tr>
          <tr>
            <td class="center">3.</td>
            <td class="right"><?php echo $ah_applicant->musadra_3; ?></td>
            <td class="center"><?php echo number_format($ah_applicant->qyama_3,3); ?></td>
          </tr>
          <tr>
            <td class="center">4.</td>
            <td class="right"><?php echo $ah_applicant->musadra_4; ?></td>
            <td class="center"><?php echo number_format($ah_applicant->qyama_4,3); ?></td>
          </tr>
          <tr>
            <td class="center"></td>
            <td class="left"><label class="text-warning">مجموع الدخل:</label></td>
            <td class="center"><?php echo number_format($totalQyama,3); ?></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4" class="customhr">القروض: (<?php echo $ah_applicant->q_source; ?>)</td>
    </tr>
    <?php if($ah_applicant->q_source=='يوجد') { ?>
    <tr>
      <td colspan="4"><table class="table">
          <thead>
            <tr role="row">
              <th class="right">نوع القرض</th>
              <th class="center">قبمة القرض</th>
              <th class="center">القسط الشهري</th>
            </tr>
          </thead>
          <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php 
			  
			  if($ah_applicants_loans)
			  {
				  	$apr_req = 0;
			  		foreach($ah_applicants_loans as $loans) 
					{	$lmaount += $loans->loan_limit;
			  ?>
            <tr>
              <td class="right"><?php echo $loans->loan_type;  ?></td>
              <td class="center"><?php echo number_format($loans->loan_amount,3);  ?></td>
              <td class="center"><?php echo number_format($loans->loan_limit,3);  ?></td>
            </tr>
            <?php $apr_req++; }
			  	}
			  
						$totalSafi = $totalQyama-$lmaount;
				
				?>
            <tr>
              <td class="right"></td>
              <td class="left"><label class="text-warning">صافي الدخل:</label></td>
              <td class="center"><?php echo number_format($totalSafi-$ah_applicant->ownershiptype_amount,3); ?></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <?php } ?>
    <tr>
      <td class="right"><label class="text-warning">وضف الحالة السكنية:</label>
        <br />
        <strong><?php echo a_date($ah_applicant->current_situation); ?></strong></td>
      <td class="right"><label class="text-warning"><?php echo $ah_applicant->ownershiptype; ?>:</label>
        <br />
        <strong><?php echo number_format($ah_applicant->ownershiptype_amount,3); ?></strong></td>
      <td></td>
      <td></td>
    </tr>
    <?php if($ah_applicant->charity_type_id	!=	'81'):?>
    <tr>
      <td colspan="4" class="customhr">مكونات المنزل:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">نوع البناء:</label>
        <br />
        <strong><?php echo $this->haya_model->get_name_from_list($ah_applicant->building_type); ?></strong></td>
      <td class="right"><label class="text-warning">عدد الغرف:</label>
        <br />
        <strong><?php echo a_date($ah_applicant->number_of_rooms); ?></strong></td>
      <td class="right"><label class="text-warning">المرافق:</label>
        <br />
        <strong><?php echo a_date($ah_applicant->utilities); ?></strong></td>
      <td class="right"><label class="text-warning">حالة الأثاث والموجودات:</label>
        <br />
        <strong><?php echo a_date($ah_applicant->furniture); ?></strong></td>
    </tr>
    <?php endif;?>
    <?php if($ah_applicant->charity_type_id	==	'81'):?>
    <tr>
      <td colspan="4" class="customhr">البيانات الدراسية:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">شهادة ثنوية:</label>
        <br />
        <strong><?php echo $ah_applicant->certificate_dualism; ?></strong></td>
      <td class="right"><label class="text-warning">سنة التخرج:</label>
        <br />
        <strong><?php echo $ah_applicant->graduation_year; ?></strong></td>
      <td class="right"><label class="text-warning">النسبة:</label>
        <br />
        <strong><?php echo $ah_applicant->school_percentage; ?></strong></td>
    </tr>
    <tr>
      <td colspan="4" class="customhr">البيانات الجامعية:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">اسم الكلية أو الجامعة:</label>
        <br />
        <strong><?php echo $ah_applicant->college_name; ?></strong></td>
      <td class="right"><label class="text-warning">التخصص:</label>
        <br />
        <strong><?php echo $ah_applicant->specialization; ?></strong></td>
      <td class="right"><label class="text-warning">السنة لدراسية:</label>
        <br />
        <strong><?php echo $ah_applicant->year_of_study; ?></strong></td>
      <td class="right"><label class="text-warning">اخر معدل تراكمي:</label>
        <br />
        <strong><?php echo $ah_applicant->last_grade_average; ?></strong></td>
    </tr>
    <?php endif;?>
    <?php if($ah_applicant->charity_type_id	!=	'81'):?>
    <tr>
      <td colspan="4" class="customhr">تفاصيل البنك:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">اسم البنك:</label>
        <br />
        <strong><?php echo $this->haya_model->get_name_from_list($ah_applicant->bankid); ?></strong></td>
      <td class="right"><label class="text-warning">الفرع:</label>
        <br />
        <strong><?php echo $this->haya_model->get_name_from_list($ah_applicant->branchid); ?></strong></td>
      <td class="right"><label class="text-warning">رقم الحساب:</label>
        <br />
        <strong><?php echo a_date($ah_applicant->accountnumber); ?></strong></td>
      <td class="right"><label class="text-warning">نوع الحساب: جاري / توفير:</label>
        <br />
        <strong><?php echo $ah_applicant->accounttype; ?></strong></td>
    </tr>
    <?php endif;?>
    
      <td class="right"><label class="text-warning">نواقص الطلب:</label>
        <br />
        <strong><?php echo $ah_applicant->extra_detail; ?></strong></td>
      <?php if($doctor_notes):?>
    <tr>
      <td colspan="4" class="customhr">بيان الاخصائي العلاجي عن الحاله المرضيه:</td>
    </tr>
    <tr>
      <td colspan="4" class="right"><?php echo $doctor_notes;?></td>
    </tr>
    <?php endif;?>
    <?php if(isset($maintenance_type)):?>
    <tr>
      <td colspan="4" class="customhr">تقرير الزيارة الميدانية:</td>
    </tr>
    <tr>
      <td colspan="4" class="right">نوع الصيانة: <?php echo $this->haya_model->get_name_from_list($maintenance_type);?></td>
    </tr>
    <tr>
      <td colspan="4" class="right">اقتراح القسم: <?php echo $proposal_section;?></td>
    </tr>
    <tr>
      <td colspan="4" class="right">تقرير الزيارة: <?php echo $report_visit;?></td>
    </tr>
    <tr>
      <td colspan="4" class="right"><img src="<?php echo base_url();?>resources/nolist/1717/<?php echo $pictures;?>" width="500"/></td>
    </tr>
    <tr>
      <td colspan="4" class="right"><img src="<?php echo base_url();?>resources/nolist/1717/<?php echo $certificate;?>" width="500"/></td>
    </tr>
    <?php endif;?>
    <?php if($notes):?>
    <tr>
      <td colspan="4" class="customhr">أسباب الاعتذار:</td>
    </tr>
    <?php endif;?>
    <tr>
      <td colspan="4" class="right">موظف القسم <strong><?PHP echo $user_name; ?></strong> التاريخ : <strong> <?php echo a_date($submit_date,5);?> </strong>
        <?php if(isset($notes)):?>
        <br />
        <strong><?PHP echo $notes; ?></strong>
        <?php endif;?></td>
    </tr>
  </table>
</div>
<div class="col-md-12 center">
  <button type="button" class="btn" id="print" onclick="printthepage('myprint');"><i class="icon-print"></i> طباعة</button>
</div>

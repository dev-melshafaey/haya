<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php
		  	$msg = $this->session->flashdata('msg');
			
			if($this->session->flashdata('msg') !="")
			{ 
			?>
      <center style="color:#060;">
        تم تحديث البيانات بنجاح
      </center>
      <?php } ?>
      <div class="col-md-12">
        <?php
		  	$msg = $this->session->flashdata('msg');
			
			if($this->session->flashdata('msg') !="")
			{
				?>
			<script type="text/javascript">
                     show_notification_error_end('تم تحديث البيانات بنجاح');
            </script>
        <?php
			}
		  ?>
        <form action="<?php echo current_url();?>" method="POST" autocomplete="off">
          <div class="col-md-12">
            <div class="panel panel-default panel-block">
              <div class="list-group">
                <div class="list-group-item" id="input-fields"> 
                  <!-- -------------------------------------------- -->
                  
                  <?php if(!empty($inq_info_sms)):?>
					  <?php $counter	=	1;?>
                      <?php foreach($inq_info_sms as $sms):?>
						  <?php //$lable_name	=	get_lable($sms->sms_type);?>
                          <div class="opxbox">
                            <div class="form-group col-md-8" >
                              <label class="text-warning"><input class="text-warning" type="text" name="sms_heading_<?php echo $counter;?>" id="sms_heading_<?php echo $counter;?>" value="<?php echo $sms->sms_heading; ?>" /> <span class="smslagend">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?PHP echo implode(',',sms_lagend() ); ?></span></label>
                              <input type="hidden" name="sms_id_<?php echo $counter;?>" id="sms_id_<?php echo $counter;?>" value="<?php echo (isset($sms->sms_id) ? $sms->sms_id : NULL);  ?>" />
                              <textarea id="sms_value_<?php echo $counter;?>" class="form-control exTextarea" name="sms_value_<?php echo $counter;?>" onKeyUp="CharacterCount(this.id,'count_'+<?php echo $counter;?>)"><?php echo (isset($sms->sms_value) ? $sms->sms_value : NULL);?></textarea><br />
                             <label for="text-warning">عدد الأحرف المكتوبة</label>
                              <?php
                                    if(strlen(utf8_decode($sms->sms_value))	>=	70)
                                    {
                                        $color = "red";
                                    }
                                    else
                                    {
                                        $color = "green";
                                    }
                                ?>
                              <span id="count_<?php echo $counter;?>" class="smscounterbox" style="color:<?php echo $color; ?>"><?php echo strlen(utf8_decode($sms->sms_value)); ?></span> 70 حرفا باللغة العربية تساوي رسالة واحدة 
                            </div>
                            <div class="form-group col-md-2" style="display:none;">
                              <label class=" text-warning">إرسال رسالة تذكير بعد</label>
                              <input type="text" class="form-control NumberInput" name="reminder_count_<?php echo $counter;?>" size="2" width="2" id="reminder_count_<?php echo $counter;?>" value="<?php echo (isset($sms->sms_reminder_counter) ? $sms->sms_reminder_counter : NULL);?>">
                              <br />
                            </div>
                            <div class="form-group col-md-2" style="display:none;">
                              <label class=" text-warning">تذكير</label>
                              <select id="sms_reminder_type_<?php echo $counter;?>" name="sms_reminder_type_<?php echo $counter;?>" class="form-control">
                                <option value="day" <?php if($sms->sms_remider	==	'day'):?> selected="selected" <?php endif;?>>يوم</option>
                                <option value="week" <?php if($sms->sms_remider	==	'week'):?> selected="selected" <?php endif;?>>أسبوع</option>
                                <option value="month" <?php if($sms->sms_remider	==	'month'):?> selected="selected" <?php endif;?>>شهر</option>
                                <option value="year" <?php if($sms->sms_remider	==	'year'):?> selected="selected" <?php endif;?>>سنة</option>
                              </select>
                            </div>
                            <div class="form-group col-md-4" style="font-size:11px;"></div>
                            <br clear="all" />
                          </div>
                          <?php $counter++;?>
                      <?php endforeach;?>
                  <?php endif;?>
                  
                  <!-- -------------------------------------- --> 
                  <br clear="all" />
                  <div class="form-group">
                    <input type="hidden" name="type" id="type" value="<?php echo $type;?>"/>
                    <input type="submit" class="btn btn-success btn-lrg" name="submit"  value="حفظ" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<script>
    $(function(){        
      var branchid = '<?PHP echo $branchid; ?>';
	
    } );
</script> 
<script type="text/javascript">
CharacterCount = function(TextArea,FieldToCount){
	
	var myField = document.getElementById(TextArea);
	var myLabel = document.getElementById(FieldToCount); 

	if(myField.value.length>=70){
		
		$("#"+FieldToCount).css('color','red');
	}
	else{
		$("#"+FieldToCount).css('color','green');	
	}
	myLabel.innerHTML = myField.value.length;
}
</script>
<?php $this->load->view('common/footer'); ?>
<!-- /.modal-dialog -->
</div>
</body>
</html>
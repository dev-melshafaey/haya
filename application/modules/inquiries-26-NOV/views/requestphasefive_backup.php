<!doctype html>
<?PHP
	$applicant = $applicant_data['applicants'];
	$project = $applicant_data['applicant_project'][0];
	$loan = $applicant_data['applicant_loans'][0];
	$partners = $applicant_data['applicant_partners'];
	$phones = $applicant_data['applicant_phones'];
	$comitte = $applicant_data['comitte_decision'][0];
	$evolution['applicants'] = $applicant; 
	$evolution['loan'] = $loan; 
	$fullname = $applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name;
	foreach($phones as $p)
	{	$ar[] = '986'.$p->applicant_phone;	}
		$applicantphone = implode('<br>',$ar);
		$tabs = array('1'=>'الثاني','2'=>'الثالث','3'=>'الرابع');
	$evo = project_evolution($applicant->applicant_id);	
?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
  <form id="validate_form_data" name="validate_form_data" method="post" action="<?PHP echo base_url().'inquiries/comittie_decision' ?>" autocomplete="off">
    <input type="hidden" name="form_step" id="form_step" value="5" />
    <input type="hidden" name="applicant_id" id="applicant_id" value="<?php echo $applicant->applicant_id;?>" />    
    <input type="hidden" id="loan_start" value="<?PHP echo $loan_data->loan_start_amount; ?>" name="loan_start" />
    <input type="hidden" id="loan_limit" value="<?PHP echo $loan_data->loan_end_amount; ?>" name="loan_limit" />
    <input type="hidden" id="loan_amount" name="loan_amount"  value="<?php echo $loan->loan_amount; ?>"/>
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        
        <div class="col-md-4">
          <div class="panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item" style="padding-bottom: 8px;" id="input-fields">
                <h4 class="section-title text-warning">البيانات الشخصية <a onClick="show_get_set_data(this);" href="#globalDiag" data-url="<?PHP echo base_url(); ?>inquiries/get_applicant_data/<?PHP echo $applicant->applicant_id; ?>"><i class="icon-folder-close pull-left"><strong> مشاهدة الملف </strong></i></a></h4>
                <div class="panel panel-default panel-block">
                  <?PHP if($applicant->applicant_type!='فردي') { ?>
                  <ul class="nav nav-tabs panel panel-default panel-block">
                    <li class="active"><a href="#tabsdemo-0" data-toggle="tab">الأول</a></li>
                    <?PHP 
					$xx = 1;
					foreach($partners as $tabindex => $tabvalue) { ?>
                    <li><a href="#tabsdemo-<?PHP echo $tabvalue->parnter_id; ?>" data-toggle="tab"><?PHP echo $tabs[$xx]; ?></a></li>
                    <?PHP $xx++; } ?>
                  </ul>
                  <?PHP } ?>
                  <div class="tab-content panel panel-default panel-block">
                    <div class="tab-pane list-group active" id="tabsdemo-0">
                      <div class="list-group-item form-horizontal">
                        <h4 align="center" class="section-title preface-title text-warning" style="margin-bottom: 0px !important;"><?php echo $fullname; ?></h4>
                        <input type="text" class="form-control label label-default pull-left" style="margin-bottom: 20px; width: 100%; padding: 6px 4px; font-size: 16px;" value="رقم التسجيل <?PHP echo arabic_date(applicant_number($applicant->applicant_id)); ?>">
                        <!--<span class="label label-default pull-left" style="margin-bottom: 20px; width: 100%; padding: 6px 4px; font-size: 16px;">رقم التسجيل <?PHP echo arabic_date(applicant_number($applicant->applicant_id)); ?></span>-->
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning"><strong>نوع القرض</strong></label>
                            <br />
                            <label class="text-success"><strong><?PHP echo show_data('calc',$loan->loan_limit); ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning"><strong>المبلغ المطلوب</strong></label>
                            <br />
                            <label class="text-success"><strong><?PHP echo number_format($loan->loan_amount,0); ?> ر.ع</strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning" for="basic-input">النوع</label>
                            <br />
                            <label><strong><?PHP echo $applicant->applicant_gender; ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning" for="basic-input">تاريخ الميلاد</label>
                            <br />
                            <label><strong><?PHP echo date('Y-m-d',strtotime($applicant->applicant_date_birth)); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning" for="basic-input">القبيلة</label>
                            <br />
                            <label><strong><?PHP echo $applicant->applicant_sur_name; ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning" for="basic-input">الحالة الإجتماعية</label>
                            <br />
                            <label><strong><?PHP echo getlistType('maritalstatus',$applicant->applicant_marital_status); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning" for="basic-input">رقم البطاقة</label>
                            <br />
                            <label><strong><?PHP echo $applicant->appliant_id_number; ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning" for="basic-input">رقم سجل القوى العاملة</label>
                            <br />
                            <label><strong><?PHP echo $applicant->applicant_cr_number; ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning">رقم الهاتف</label>
                            <br />
                            <label><strong><?PHP echo $applicantphone; ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning">البريد الإلكتروني</label>
                            <br />
                            <label><strong><?PHP echo is_set($applicant->email); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-10">
                            <label class="text-warning" for="basic-input">الوضع الحالي</label>
                            <label><strong><?PHP echo getlistType('current_situation',$applicant->applicant_job_staus); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning">فئة الضمان الإجتماعي</label>
                            <label><strong><?PHP echo rander_html($applicant->option1,'x'); ?></strong></label>
                          </div>
                          <?PHP if($applicant->option1=='Y') { ?>
                          <div class="form-group col-md-6">
                            <label class="text-warning">رقم</label>
                            <label><strong><?PHP echo is_set($applicant->option_txt); ?></strong></label>
                          </div>
                          <?PHP } ?>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning">فئة ذوي الإعاقة</label>
                            <label><strong><?PHP echo rander_html($applicant->option2,'x'); ?></strong></label>
                          </div>
                          <?PHP if($applicant->option2=='Y') { ?>
                          <div class="form-group col-md-6">
                            <label class="text-warning">نوعها</label>
                            <label><strong><?PHP echo is_set(getlistType('disable_type',$applicant->disable_type)); ?></strong></label>
                          </div>
                          <?PHP } ?>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12">
                            <label class="text-warning">العنوان</label>
                            <label><strong><?PHP echo $applicant->village; ?> <?PHP echo $applicant->way; ?> <?PHP echo $applicant->home; ?> - مكتب رقم <?PHP echo $applicant->zipcode; ?> - <?PHP echo show_data('Walaya',$applicant->walaya); ?> - <?PHP echo show_data('Region',$applicant->province); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning">الهاتف النقال</label>
                            <br />
                            <label><strong><?PHP echo is_set($applicant->mobile_number); ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning">الهاتف الثابت</label>
                            <br />
                            <label><strong><?PHP echo is_set($applicant->linephone); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning">هاتف أحد الأقارب</label>
                            <br />
                            <label><strong><?PHP echo is_set($applicant->option_txt); ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning">الفاكس</label>
                            <br />
                            <label><strong><?PHP echo is_set($applicant->fax); ?></strong></label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?PHP
						$xxp = 1; 
						foreach($partners as $partner_index => $partner) { 
							$partner_name = $partner->partner_first_name.' '.$partner->partner_middle_name.' '.$partner->partner_last_name.' '.$partner->partner_sur_name;
						
						?>
                    <div class="tab-pane list-group" id="tabsdemo-<?PHP echo $partner->parnter_id; ?>">
                      <div class="list-group-item form-horizontal">
                        <h4 align="center" class="section-title preface-title text-warning"><?php echo $fullname; ?><span class="label label-default pull-left">رقم التسجيل <?PHP echo arabic_date(applicant_number($applicant->applicant_id)); ?></span></h4>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning" for="basic-input">الإسم</label>
                            <br />
                            <label><strong><?PHP echo $partner_name; ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning" for="basic-input">النوع</label>
                            <br />
                            <label><strong><?PHP echo $partner->partner_gender; ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning" for="basic-input">القبيلة</label>
                            <br />
                            <label><strong><?PHP echo $partner->partner_sur_name; ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning" for="basic-input">الحالة الإجتماعية</label>
                            <br />
                            <label><strong><?PHP echo getlistType('maritalstatus',$partner->partner_marital_status); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning" for="basic-input">رقم البطاقة</label>
                            <br />
                            <label><strong><?PHP echo $partner->partner_id_number; ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning" for="basic-input">رقم سجل القوى العاملة</label>
                            <br />
                            <label><strong><?PHP echo $partner->partner_cr_number; ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning">رقم الهاتف</label>
                            <br />
                            <label><strong><?PHP echo $applicantphone; ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning">البريد الإلكتروني</label>
                            <br />
                            <label><strong><?PHP echo is_set($partner->email); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-10">
                            <label class="text-warning" for="basic-input">الوضع الحالي</label>
                            <label><strong><?PHP echo getlistType('current_situation',$partner->partner_job_staus); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning">فئة الضمان الإجتماعي</label>
                            <label><strong><?PHP echo rander_html($partner->option1,'x'); ?></strong></label>
                          </div>
                          <?PHP if($applicant->option1=='Y') { ?>
                          <div class="form-group col-md-6">
                            <label class="text-warning">رقم</label>
                            <label><strong><?PHP echo is_set($partner->option_txt); ?></strong></label>
                          </div>
                          <?PHP } ?>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning">فئة ذوي الإعاقة</label>
                            <label><strong><?PHP echo rander_html($partner->option2,'x'); ?></strong></label>
                          </div>
                          <?PHP if($applicant->option2=='Y') { ?>
                          <div class="form-group col-md-6">
                            <label class="text-warning">نوعها</label>
                            <label><strong><?PHP echo is_set(getlistType('disable_type',$partner->disable_type)); ?></strong></label>
                          </div>
                          <?PHP } ?>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12">
                            <label class="text-warning">العنوان</label>
                            <label><strong><?PHP echo $partner->village; ?> <?PHP echo $partner->way; ?> <?PHP echo $partner->home; ?> - مكتب رقم <?PHP echo $partner->zipcode; ?> - <?PHP echo show_data('Walaya',$partner->walaya); ?> - <?PHP echo show_data('Region',$partner->province); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning">الهاتف النقال</label>
                            <br />
                            <label><strong><?PHP echo is_set($partner->mobile_number); ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning">الهاتف الثابت</label>
                            <br />
                            <label><strong><?PHP echo is_set($partner->linephone); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label class="text-warning">هاتف أحد الأقارب</label>
                            <br />
                            <label><strong><?PHP echo is_set($partner->option_txt); ?></strong></label>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="text-warning">الفاكس</label>
                            <br />
                            <label><strong><?PHP echo is_set($partner->fax); ?></strong></label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?PHP $xxp++; } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item" id="الخبرة-في-نفس-نشاط-المشروع">
                <div class="list-group-item" id="الخبرة-في-نفس-نشاط-المشروع">
                  <h4 class="section-title text-warning">تقييم طلب القرض - (لجنة سقف القروض)</h4>
                  <div class="row">
                    <div class="form-group col-md-6 text-center text-warning"> البند </div>
                    <div class="form-group col-md-6 text-center text-warning"> المبلغ بالريال العماني </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>مصاريف ما قبل التشغيل</label>
                    </div>
                    <div class="form-group col-md-6">
                      <input type="text" placeholder="مصاريف ما قبل التشغيل" id="evolution_pre_expenses" class="charges form-control xx NumberInput req" value="<?PHP echo($evo->evolution_pre_expenses); ?>" name="evolution_pre_expenses">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>أثاث و تركيبات</label>
                    </div>
                    <div class="form-group col-md-6">
                      <input type="text" maxlength="10" size="15" placeholder="اثاث وتركيبات" id="furniture_fixture" class="charges form-control xx NumberInput req" value="<?PHP echo($evo->furniture_fixture); ?>" name="furniture_fixture">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>الألت و المعدات</label>
                    </div>
                    <div class="form-group col-md-6">
                      <input type="text" maxlength="10" size="15" placeholder="الآلات والمعدات" id="machinery_equipment" class="charges form-control xx NumberInput req" value="<?PHP echo($evo->machinery_equipment); ?>" name="machinery_equipment">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>المركبات</label>
                    </div>
                    <div class="form-group col-md-6">
                      <input type="text" maxlength="10" size="15" placeholder="المركبات" id="vehicles" class="charges form-control xx NumberInput req" value="<?PHP echo($evo->vehicles); ?>" name="vehicles">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>رأس المال العام</label>
                    </div>
                    <div class="form-group col-md-6">
                      <input type="text" maxlength="10" size="15" placeholder="رأس المال العامل" id="working_capital" class="charges form-control xx NumberInput req" value="<?PHP echo($evo->working_capital); ?>" name="working_capital">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>المبلغ المخصص للبائع (بالنسبة لشراء المشاريع)</label>
                    </div>
                    <div class="form-group col-md-6">
                      <input type="text" maxlength="10" size="15" placeholder="رأس المال العامل" id="seller_amount" class="charges form-control xx NumberInput req" value="<?PHP echo($evo->seller_amount); ?>" name="seller_amount">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label><strong>الاجمالي</strong></label>
                    </div>
                    <div class="form-group col-md-6">
                      <input type="text" maxlength="10" size="15" placeholder="الاجمالي" id="total_cost" class="form-control xx NumberInput req" value="<?PHP echo($evo->total_cost); ?>" name="total_cost" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>المساهمة <?php echo $loan_data->loan_percentage."%"; ?></label>
                    </div>
                    <div class="form-group col-md-6">
                    <?PHP
                    	$p = $loan_data->loan_percentage*0.01;
				$percentage = $evo->total_cost*$p;
					?>
                    <input type="hidden" maxlength="10" size="15" placeholder="المساهمة" id="percentage" class="form-control xx NumberInput req" value="<?PHP echo $loan->loan_percentage; ?>" name="percentage">
                      <input type="text" maxlength="10" size="15" placeholder="المساهمة" id="contribute" class="form-control xx NumberInput req" value="<?PHP echo $percentage; ?>" name="contribute">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default panel-block">
            <div class="tab-pane list-group">
              <div class="list-group-item">
                <h4 class="section-title text-warning">التوقيعات</h4>
                <?PHP foreach(people() as $people_key => $people_value) { ?>
                <div class="row">
                  <div class="col-lg-1 pull-right">
                    <input type="checkbox" class="people"  value="<?PHP echo $people_key; ?>">
                  </div>
                  <div class="form-group col-md-7">
                    <label><?PHP echo $people_value; ?></label>
                  </div>
                  <div class="form-group col-md-4">
                    <input id="notes<?PHP echo $people_key; ?>" name="notes[]" class="form-control xx" placeholder="ملاحظات">
                  </div>
                </div>               
               <?PHP } ?>
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="panel-group" id="demo-accordion">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseOne">بطاقة سجل القوى العاملة <i class="icon-upload-alt pull-left"></i></a> </h4>
              </div>
              <div id="demo-collapseOne" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
                <div class="panel-body">
                  <li><img style="text-align:center;" src="<?PHP echo base_url(); ?>images/spinners/22.gif" alt="loader"></li>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseTwo">الرقم المدني<i class="icon-user pull-left"></i></a> </h4>
              </div>
              <div id="demo-collapseTwo" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
                <div class="panel-body">
                  <li><img style="text-align:center;" src="<?PHP echo base_url(); ?>images/spinners/22.gif" alt="loader"></li>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseThree">شهادة عدم محكومية<i class="icon-certificate pull-left"></i></a> </h4>
              </div>
              <div id="demo-collapseThree" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
                <div class="panel-body">
                  <li><img style="text-align:center;" src="<?PHP echo base_url(); ?>images/spinners/22.gif" alt="loader"></li>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseFour">دراسة الجدوى الإقتصادية للمشروع<i class="icon-briefcase pull-left"></i><i class="icon-question-sign text-info pull-left"></i></a> </h4>
              </div>
              <div id="demo-collapseFour" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
                <div class="panel-body">
                  <li><img style="text-align:center;" src="<?PHP echo base_url(); ?>images/spinners/22.gif" alt="loader"></li>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseFive">صورة شمسية<i class="icon-camera-retro pull-left"></i></a> </h4>
              </div>
              <div id="demo-collapseFive" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
                <div class="panel-body"> <img style="text-align:center; margin-bottom:10px;" src="<?PHP echo base_url(); ?>images/pic" width="250" height="225"><br />
                  <button type="button" class="btn btn-success"><i class="icon-edit"></i> تعديل</button>
                  <button type="button" class="btn btn-danger"><i class="icon-remove-circle"></i> حذف </button>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseSix">شهادات الخبرة / التدريب<i class="icon-wrench pull-left"></i> <i class="icon-ok text-success  pull-left"></i> </a> </h4>
              </div>
              <div id="demo-collapseSix" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
                <div class="panel-body">
                  <li><img style="text-align:center;" src="images/spinners/22.gif" alt="loader"></li>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseSeven">بطاقة الضمان الاجتماعي<i class="icon-umbrella pull-left"></i> <i class="icon-ok text-success  pull-left"></i> </a> </h4>
              </div>
              <div id="demo-collapseSeven" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
                <div class="panel-body">
                  <li><img style="text-align:center;" src="images/spinners/22.gif" alt="loader"></li>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseEight">ملفات أخرى<i class="icon-tasks pull-left"></i></a> </h4>
              </div>
              <div id="demo-collapseEight" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
                <div class="panel-body">
                  <li><img style="text-align:center;" src="images/spinners/22.gif" alt="loader"></li>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
            <h4 class="section-title text-warning" style="margin-bottom: 0px !important;"></h4>
              <div class="panel-heading"><button type="button" id="save_data_form_five" class="btn btn-lg btn-success">حفظ</button></div>
              
            </div>
          </div>
        </div>
      
        <div class="col-md-4">
          <div class="panel panel-default panel-block">
            <div class="tab-pane list-group">
              <div class="list-group-item">
                <h4 class="section-title text-warning">نموذج قرار اللجنة</h4>
                <textarea name="comission_decision" id="comission_decision" rows="2" class="form-control auto-resize"></textarea>
              </div>
              <div class="list-group-item">
                <h4 class="section-title text-warning">الملاحظات</h4>
                <textarea id="astamaarah_value" name="astamaarah_value" placeholder="الملاحظات" rows="2" class="form-control auto-resize req"></textarea>
              </div>
            </div>
          </div>
        </div>
        <!------------------------------------------------------> 
        <!------------------------------------------------------> 
        <!------------------------------------------------------> 
        <!------------------------------------------------------>
        <div class="col-md-8">
          <div class="panel panel-default panel-block">
            <div class="tab-pane list-group">
              <div class="list-group-item">
                <h4 class="section-title text-warning">قرار اللجنة</h4>
                <!-----------------------btn-warning-------------------------->
                <div class="row">
                <div class="form-group col-md-12">
                <div class="btn-group" data-toggle="buttons">
                


<label class="btn btn-success <?PHP if($comitte->commitee_decision_type=='rejected') { ?>active<?PHP } ?>">
<input <?PHP if($comitte->commitee_decision_type=='rejected') { ?> checked="checked"<?PHP } ?> type="radio" value="rejected" onchange="check_comitee(this.value)" name="commitee_decision_type" id="is_project2"> الرفض
</label>
<label class="btn btn-success <?PHP if($comitte->commitee_decision_type=='postponed') { ?>active<?PHP } ?>">
<input <?PHP if($comitte->commitee_decision_type=='postponed') { ?> checked="checked"<?PHP } ?> type="radio" value="postponed" onchange="check_comitee(this.value)" name="commitee_decision_type" id="is_project2"> تأجيل
</label>
<label class="btn btn-success <?PHP if($comitte->commitee_decision_type=='approved') { ?>active<?PHP } ?>">
<input <?PHP if($comitte->commitee_decision_type=='approved') { ?> checked="checked"<?PHP } ?> type="radio" value="approved" onchange="check_comitee(this.value)" name="commitee_decision_type" id="is_project2"> الموافق
</label>
</div>
</div>
                </div>
                <div class="row">
                  <div class="form-group col-md-12">
                    <div class="form-group col-md-2">
                      <label class="text-warning" for="basic-input">الموافق</label>
                      <label>
                        <input type="radio" <?PHP if($comitte->commitee_decision_type=='approved') { ?> checked="checked"<?PHP } ?>  name="commitee_decision_type" id="is_project2" value="approved" onchange="check_comitee(this.value)"  class="req comitee" />
                      </label>
                    </div>
                    <div class="form-group col-md-2">
                      <label class="text-warning" for="basic-input">تأجيل</label>
                      <label>
                        <input type="radio" <?PHP if($comitte->commitee_decision_type=='postponed') { ?> checked="checked"<?PHP } ?>  name="commitee_decision_type" id="is_project2" value="postponed" onchange="check_comitee(this.value)"  class="req comitee"/>
                      </label>
                    </div>
                    <div class="form-group col-md-2">
                      <label class="text-warning" for="basic-input">الرفض</label>
                      <label>
                        <input type="radio" <?PHP if($comitte->commitee_decision_type=='rejected') { ?> checked="checked"<?PHP } ?>  name="commitee_decision_type" id="is_project2" value="rejected" onchange="check_comitee(this.value)"  class="req comitee"/>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row rowBorder onClickShow approvedclass" id="is_aproved" <?PHP if($comitte->commitee_decision_type=='approved') { ?>style="display:block;"<?PHP } ?>>
                  <div class="form-group col-md-12">
                    <div class="form-group col-md-2">
                      <label class="text-warning" for="basic-input">‫مشروطة</label>
                      <label>
                        <input type="radio"  name="committee_decision_is_aproved" id="committee_decision_is_query" value="queries"  class="" onchange="check_quez(this.value)" placeholder='الموافق' <?php if($comitte->committee_decision_is_aproved == "queries"){ ?> checked="checked" <?php }?> />
                      </label>
                    </div>
                    <div class="form-group col-md-2">
                      <label class="text-warning" for="basic-input">موافقة اولية</label>
                      <label>
                        <input type="radio"  name="committee_decision_is_aproved" id="committee_decision_is_aproved" value="approval"  class="" onchange="check_quez(this.value)" placeholder='الموافق' <?php if($comitte->committee_decision_is_aproved == "approval"){ ?> checked="checked" <?php }?>/>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row rowBorder onClickShow approvedclass" id="is_query" <?PHP if($comitte->committee_decision_is_aproved=='queries') { ?>style="display:block;"<?PHP } ?>>
                  <div class="form-group col-md-12">
                    <div class="form-group col-md-10">
                      <label class="text-warning" for="basic-input">‫مشروطة</label>
                      <label>
                        <input name="query_text" id="query_text" value="<?PHP echo $comitte->query_text; ?>"  placeholder="موافقة اولية" type="text" class="form-control">
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row rowBorder onClickShow approvedclass" id="is_aprovedamount"  <?PHP if($comitte->committee_decision_is_aproved=='approval') { ?>style="display:block;"<?PHP } ?>>
                  <div class="form-group col-md-10">
                    <label class="text-warning" for="basic-input">قيمة التمويل (ريال عماني)</label>
                    <label>
                      <input name="approv_text" id="approv_text" value="<?PHP echo $comitte->approv_text; ?>" placeholder="قيمة التمويل" type="text" class="form-control" readonly>
                    </label>
                  </div>
                </div>
                <div class="row rowBorder onClickShow postponed" id="is_postponed"  <?PHP if($comitte->commitee_decision_type=='postponed') { ?>style="display:block;"<?PHP } ?>>
                  <div class="form-group col-md-10">
                    <?PHP exdrobpx('postponed',$comitte->postponed,'سبب التأجيل ','postponed','form-control'); ?>
                  </div>
                </div>
                <div class="row rowBorder onClickShow" id="is_forward" style="display:none;">
                  <div class="form-group col-md-10">
                    <?PHP exdrobpx('project_type',$comitte->project_type,'سبب التأجيل ','project_type','form-control'); ?>
                  </div>
                </div>
                <div class="row rowBorder onClickShow rejected" id="is_rejected" <?PHP if($comitte->commitee_decision_type=='rejected') { ?>style="display:block;"<?PHP } ?>>
                  <div class="form-group col-md-10">
                    <?PHP exdrobpx('rejected',$comitte->rejected,'سبب الرفض','rejected','form-control'); ?>
                  </div>
                </div>
                <!----------------------------------------------------------> 
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </form>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script src="<?PHP echo base_url(); ?>js/request_loan_view.js"></script>
</body>
</html>
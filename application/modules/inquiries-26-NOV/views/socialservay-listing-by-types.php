<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
    
         <?php
		 foreach($bernamij_masadaat_types as $muhasaba) 
		 { 
		 	$tharki = $this->haya_model->get_permission_ids($moduleparent,$muhasaba['list_id'],$permission);
			if($tharki	==	1)
			{
		 	?>
            <?php $module 		 =	$this->haya_model->get_module_name_icon(0,$muhasaba['list_id']); ?>
            <div class="col-md-2 main_body" id="<?php echo $muhasaba['list_id'];?>">
              <div id="bingo<?php echo $muhasaba['list_id'];?>" class="col-md-12 center main_icon"><i  class="<?php echo $module->module_icon;?>"></i></div>
              <div class="col-md-12 center main_heading"><a href="<?php echo base_url();?>inquiries/<?php echo $method_name;?>/<?php echo $branchid;?>/<?php echo $muhasaba['list_id'];?>"> <?php echo $muhasaba['name'];?> </a></div>
              <div class="col-md-12 center">
              	<?php if($step!=3) { ?><a class="main_count" href="<?php echo base_url();?>inquiries/<?php echo $method_name;?>/<?php echo $branchid;?>/<?php echo $muhasaba['list_id'];?>">مجموع (<?php echo $muhasaba['y'];?>)</a> - <?php } ?>
              	<?php if($step!=1 AND $step!=3) { ?><a class="main_count" href="<?php echo base_url();?>inquiries/<?php echo $method_reject;?>/<?php echo $muhasaba['list_id'];?>">رفض (<?php echo $this->haya_model->get_all_list_value($muhasaba['list_id'],$step,'رفض');?>)</a> <?php if($step	!=	3){?>-<?php }?><?php } ?>
              	<?php if($step!=3) { ?><a class="main_count" href="<?php echo base_url();?>inquiries/<?php echo $method_step;?>/<?php echo $branchid;?>/<?php echo $muhasaba['list_id'];?>/<?php echo $step;?>">إعادة نظر (<?php echo $this->haya_model->get_all_list_value($muhasaba['list_id'],$step,'إعادة نظر');?>)</a> <?php } ?>
                <?php if($step!=3) { ?> <!--- <a class="main_count" href="<?php echo base_url();?>inquiries/<?php echo $complete_step;?>/<?php echo $branchid;?>/<?php echo $muhasaba['list_id'];?>/<?php echo $done_step;?>">معاملات سابقة (<?php echo $this->haya_model->get_all_old_value($muhasaba['list_id'],$step);?>)</a>--><?php } ?>
              </div>
            </div>
            <?php } 
		 		} ?>
            <div class="col-md-2 main_body" id="<?php echo $muhasaba['list_id'];?>">
              <div id="bingo-static" class="col-md-12 center main_icon"><i class="myicon icon-sitemap"></i></div>
              <div class="col-md-12 center main_heading"><a href="<?php //echo base_url();?>inquiries/<?php echo $method_name;?>/<?php echo $branchid;?>/<?php echo $muhasaba['list_id'];?>">قائمة الأيتام</a></div>
              <div class="col-md-12 center">
              	<a class="main_count" href="<?php echo base_url();?>inquiries/yateem_list">مجموع (<?php echo (isset($total_yateem) ? $total_yateem : '0');?>)</a>
              </div>
            </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script language="javascript">
$(function(){
	$('.main_body').mouseover(function()
	{
		var objid = $(this).attr("id");
		$('#bingo'+objid).animo( { animation: ['tada', 'bounce','spin'], duration:2 });
	}).mouseout(function()
	{
		$('.spin').animo("cleanse");
	});
})
</script>
</div>
</body>
</html>
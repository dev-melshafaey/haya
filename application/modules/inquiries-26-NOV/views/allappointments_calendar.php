<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <br clear="all">
          <div id="loading" class="col-md-11">loading...</div>
          <div id="calendar" class="col-md-12"></div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>

	$(document).ready(function() {
	
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			lang:'ar-sa',
			defaultDate: '<?PHP echo date('Y-m-d');?>',
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: {
				url: config.BASE_URL+'ajax/getevents',
				error: function(msg) {
					show_notification_error_end(msg);
					//$('#script-warning').show();
				}
			},
			loading: function(bool) {
				$('#loading').toggle(bool);
			}
		});
		
	});

</script>
</body>
</html>
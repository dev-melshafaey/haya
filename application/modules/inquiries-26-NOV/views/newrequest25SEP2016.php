<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
<?php $this->load->view('common/logo'); ?>
<?php $this->load->view('common/usermenu'); ?>
<?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
<?php $this->load->view('common/quicklunchbar'); ?>
<?php
	$ah_applicant 						=	$_applicant_data['ah_applicant'];
	$ah_applicant_wife 					=	$_applicant_data['ah_applicant_wife'];
	$ah_applicant_survayresult 			=	$_applicant_data['ah_applicant_survayresult'];
	$ah_applicant_relation 				=	$_applicant_data['ah_applicant_relation'];
	$ah_applicant_family 				=	$_applicant_data['ah_applicant_family'];
	$ah_applicant_economic_situation 	=	$_applicant_data['ah_applicant_economic_situation'];
	$ah_applicant_documents 			=	$_applicant_data['ah_applicant_documents'];
?>
<div class="row">
  <div class="col-md-12">
    <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
    <?php if($ah_applicant->applicantid):?>
    <?php //$this->load->view('common/globalfilter', array('type'=>'help','b'=>$branchid,'c'=>$charity_type));?>
    <div class="nav nav-tabs panel panel-default panel-block">
    	<?php echo $html	=	$this->haya_model->get_helps_by_appid($charity_type_id,$ah_applicant->idcard_number);?>
    </div>
    <?php endif;?>
    <div class="panel panel-default panel-block">
      <form name="frm_tasgeel_form" id="frm_tasgeel_form" action="<?PHP echo base_url(); ?>inquiries/submit_tasgeel" method="post" enctype="multipart/form-data" autocomplete="off">
        <input type="hidden" name="charity_type_id" id="charity_type_id" value="<?PHP echo $charity_type_id; ?>">
        <input type="hidden" name="refil_data" id="refil_data" value="<?PHP echo $refil_data; ?>">
        <input type="hidden" name="applicantid" id="applicantid" value="<?PHP echo $ah_applicant->applicantid; ?>">
        <div class="col-md-6 panel panel-default panel-block">
        <h4 style="border-bottom: 2px solid #EEE;">البيانات الأساسية عن الحالة:</h4>
        <div class="col-md-6 form-group">
            <label class="text-warning">الرقم المدني:</label>
            <input name="idcard_number" value="<?php echo $ah_applicant->idcard_number; ?>" placeholder="رقم البطاقة الشخصة" id="idcard_number" type="text" class="form-control req NumberInput <?PHP if($ah_applicant->idcard_number) { if(!$refil_data){?>disable<?php }} ?>" maxlength="9">
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الاسم الرباعي والقبيلة:</label>
            <input name="fullname" value="<?PHP echo $ah_applicant->fullname; ?>" placeholder="االاسم الرباعي والقبيلة" id="fullname" type="text" class="form-control req">
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة الجتماعية:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('marital_status','marital_status',$ah_applicant->marital_status,0,'req'); ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم جواز سفر:</label>
            <input name="passport_number" value="<?PHP echo $ah_applicant->passport_number; ?>" placeholder="رقم جواز سفر" id="passport_number" type="text" class="form-control Passport <?PHP if($ah_applicant->passport_number) { if(!$refil_data){?>disable<?PHP } } ?>">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">جنسية:</label>
            <?php echo $this->haya_model->create_dropbox_list('country_listmanagement','issuecountry',$ah_applicant->country_listmanagement,0,''); ?> </div>
           <div class="col-md-4 form-group">
            <label class="text-warning">تاريخ الميلاد:</label>
            <input name="date_of_birth" value="<?PHP echo $ah_applicant->date_of_birth; ?>" placeholder="تاريخ الميلاد" id="date_of_birth" type="text" class="form-control req age_datepicker" set-age="applicant_age" /></div>
          <div class="col-md-4 form-group">
            <label class="text-warning">عمر:</label>
            <input name="applicant_age" value="<?PHP echo $ah_applicant->applicant_age; ?>" placeholder="عمر" id="applicant_age" type="text" class="form-control req"  /></div>
          <div class="col-md-4 form-group">
            <label class="text-warning">المحافظة \ المنطقة:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('province','regions',$ah_applicant->province,0,'req'); ?> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">ولاية:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('wilaya','wilaya',$ah_applicant->wilaya,$ah_applicant->province,'req'); ?> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">البلدة \ المحلة:</label>
            <input name="address" value="<?PHP echo $ah_applicant->address; ?>" placeholder="البلدة \ المحلة" id="address" type="text" class="form-control req">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">هاتف المنزل:</label>
            <input name="hometelephone" onBlur="checkPhoneNumber(this);" value="<?PHP echo $ah_applicant->hometelephone; ?>" placeholder="هاتف المنزل" id="hometelephone" type="text" class="form-control NumberInput">
          </div>
          <?PHP
          	$extraphone = json_decode($ah_applicant->extratelephone);
		  ?>
          <div class="col-md-4 form-group">
            <label class="text-warning">رقم الهاتف :</label>
            <input name="extratelephone[]" onBlur="checkPhoneNumber(this);" value="<?PHP echo $extraphone[0]; ?>" maxlength="8" placeholder="رقم الهاتف" id="extratelephone" type="text" class="form-control req NumberInput">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">رقم الهاتف :</label>
            <input name="extratelephone[]" onBlur="checkPhoneNumber(this);" value="<?PHP echo $extraphone[1]; ?>" maxlength="8" placeholder="رقم الهاتف" id="extratelephone" type="text" class="form-control NumberInput">
          </div>
          
          
          <!-----------Wife or Husband------------------->
          
          <div class="col-md-12 form-group">
          <?php 
		  if($form_name	==	'educationform')
		  {
			  $required	=	'req';
		  }
		  else
		  {
			  $required	=	'';
		  }
		  ?>
          <table width="100%" class="table" style="border: 1px solid #EFEFEF;" border="0" cellspacing="0" cellpadding="0">
           	 <?php if($form_name	==	'educationform'):?>
              <tr>
              	<td colspan="2" class="my_td">اسم الاب</td>
                <td class="my_td">رقم الجواز \ الرقم المدني</td>
              </tr>
              <tr>
              	<td colspan="2"><input name="father_name" value="<?PHP echo $ah_applicant->father_name; ?>" placeholder="اسم الاب" id="father_name" type="text" class="form-control <?php echo $required;?>"></td>
                <td><input name="father_id_card" value="<?PHP echo $ah_applicant->father_id_card; ?>" placeholder="رقم الجواز\الرقم المدني" id="father_id_card" type="text" class="form-control <?php echo $required;?> <?PHP if($ah_applicant->father_id_card) { ?>disable<?PHP } ?>"></td>
              </tr>
              <?php endif;?>
             <tr>
              	<td colspan="2" class="my_td">اسم الام</td>
                <td class="my_td">رقم الجواز \ الرقم المدني</td>
              </tr>
              <tr>
              	<td colspan="2"><input name="mother_name" value="<?PHP echo $ah_applicant->mother_name; ?>" placeholder="اسم الام" id="mother_name" type="text" class="form-control <?php echo $required;?>"></td>
                <td><input name="mother_id_card" value="<?PHP echo $ah_applicant->mother_id_card; ?>" placeholder="رقم الجواز\الرقم المدني" id="mother_id_card" type="text" class="form-control <?php echo $required;?> <?PHP if($ah_applicant->mother_id_card) { ?>disable<?PHP } ?>"></td>
              </tr>
              <?php if($form_name	!=	'educationform'):?>
              <tr>
                <td class="my_td" style="width:34% !important;">اسم الزوج \ الزوجة</td>
                <td class="my_td" style="width:33% !important;">رقم الجواز \ الرقم المدني</td>
                <td class="my_td" style="width:33% !important;">جنسية</td>
              </tr>
              <?PHP for($i=0; $i<=3; $i++) { 
		  		if($i==0)
				{	$req = ''; }
				else
				{	$req = ''; }
		  ?>
              <tr>
                <td><input name="relationname[]" value="<?PHP echo $ah_applicant_wife[$i]->relationname; ?>" placeholder="اسم الزوج\الزوجة" id="relationname" type="text" class="form-control marital_status<?PHP echo $i;?>"></td>
                <td><?PHP if($ah_applicant_wife[$i]->relationdoc=='') { ?>
            <input name="relationdoc[]" value="<?PHP echo $ah_applicant_wife[$i]->relationdoc; ?>" placeholder="رقم الجواز\الرقم المدني" id="passport_number_wife" type="text" class="form-control passport_number_wife marital_status<?PHP echo $i;?> <?PHP if($ah_applicant_wife[$i]->relationdoc) { ?>disable<?PHP } ?>">
          	<?PHP } else { ?><br>
            	<strong class="display"><?PHP echo $ah_applicant_wife[$i]->relationdoc; ?></strong>
            	<input type="hidden" name="relationdoc[]" value="<?PHP echo $ah_applicant_wife[$i]->relationdoc; ?>">
            <?PHP } ?></td>
                <td><?PHP echo $this->haya_model->create_dropbox_list('relationnationality','nationality',$ah_applicant_wife[$i]->relationnationality,0,$req,0,1); ?></td>
              </tr>
              <?PHP } ?>
             <?php endif;?>
             
            </table>

          
          
         
          </div>
          
          <!-----------Wife or Husband End------------------->
        
          <div class="col-md-12 form-group">
             <h4>ترفق الوثائق والمستندات اللازمة للطلب</h4>
                  <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                    <?PHP 
					$doccount = 0;
					
					foreach($this->inq->allRequiredDocument($charity_type_id) as $ctid) { $doccount++; 
						$doc = $ah_applicant_documents[$ctid->documentid];						
						$url = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$doc->document;	?>
                    <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                      <div class="panel-heading" style="padding:10px 3px;" id="head<?PHP echo $ctid->documentid;?>">
                        <h4 class="panel-title" style="font-size:15px;">
                          <?PHP if($doc->appli_doc_id!='') { ?>
                          <span class="icons" id="removeicons<?PHP echo $doc->appli_doc_id; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> <i onClick="removeDocument(this);" data-id="<?PHP echo $doc->appli_doc_id; ?>" data-remove="<?PHP echo $ctid->documentid; ?>" class="icon-remove-sign" style="color:#FF0000; cursor:pointer;"></i> </span>
                          <?PHP } ?>
                          <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $ctid->documentid;?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                      </div>
                      <div id="demo-collapse<?PHP echo $ctid->documentid;?>" class="panel-collapse collapse">
                        <div class="panel-body" style="text-align:right;">
                          <input type="file" name="doclist<?PHP echo $ctid->documentid;?>" placeholder="<?PHP echo $ctid->documenttype;?>" class="form-control <?PHP if($ctid->isrequired==1 && $doc->document=='') { echo 'req'; } ;?>">
                        </div>
                      </div>
                    </div>
                    <?PHP } ?>
                  </div>
          </div>
          <div class="col-md-12 form-group">
          <div class="col-md-4 form-group">
            <?PHP echo $this->haya_model->module_button(66,$ah_applicant->applicantid,'save_malya_naqdia',$ah_applicant->case_close); ?>
         </div>
		 <?php if(isset($refil_data) AND $ah_applicant->case_close	==	'0'):?>
              <div class="col-md-8 form-group">
                <input type="radio" name="record_type" checked id="record_type" checked value="update">
                &nbsp;&nbsp;&nbsp;
                <label class="text-warning">تحديث هذا السجل</label>
                <input type="radio" name="record_type"  id="record_type" value="new">
                &nbsp;&nbsp;&nbsp;
                <label class="text-warning">إضافة قياسيا جديدا</label>
              </div>
          <?php endif;?>
          </div>
        </div>
        <div class="col-md-6  panel panel-default panel-block" style="border-right: 2px solid #eee;">
          <div class="col-md-12 form-group">
            <h4>افراد الأسرة: (بما فهيم العاملين و غيرالعاملين)
              <button type="button" id="azafa_afrad_al_sarah" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px;">اضافة افراد الأسرة</button>
            </h4>
            <table class="table table-bordered table-striped dataTable" id="afrad_al_sarah">
              <thead>
                <tr role="row">
                  <th class="my_td">الاسم</th>
                  <th class="my_td" style="width: 50px;">السن</th>
                  <th class="my_td">القرابة</th>
                  <th class="my_td">المهنة</th>
                  <th class="my_td" style="width: 70px;">الدخل الشهري</th>
                  <th class="my_td">الإجراءات</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?PHP 
			  
			  if($ah_applicant_relation)
			  {
				  $apr_req = 0;
			  foreach($ah_applicant_relation as $apr) { ?>
                <tr>
                  <td><input name="relation_fullname[]" value="<?PHP echo $apr->relation_fullname; ?>" placeholder="الاسم" id="relation_fullname" type="text" class="form-control <?PHP is_required($apr_req); ?>"></td>
                  <td><input name="age[]" type="text" class="form-control NumberInput <?PHP is_required($apr_req); ?>" id="age" placeholder="السن" value="<?PHP echo $apr->age; ?>" maxlength="2"></td>
                  <td><?PHP echo $this->haya_model->create_dropbox_list('relationtype','user_relation',$apr->relationtype,0,'',0,1); ?></td>
                  <!--<td><?PHP echo $this->haya_model->create_dropbox_list('professionid','profession',$apr->professionid,0,'',0,1); ?></td>-->
                  <td><input name="profession[]" value="<?PHP echo $apr->profession; ?>" placeholder="المهنة" id="profession" type="text" class="form-control <?PHP is_required($apr_req); ?>"></td>
                  <td><input name="monthly_income[]" value="<?PHP echo $apr->monthly_income; ?>" placeholder="الدخل الشهري" id="monthly_income" type="text" class="form-control NumberInput <?PHP is_required($apr_req); ?>"></td>
                  <td></td>
                </tr>
                <?PHP $apr_req++; }
			  }
			  else
			  { ?>
                <tr>
                  <td><input name="relation_fullname[]" value="<?PHP echo $apr->relation_fullname; ?>" placeholder="الاسم" id="relation_fullname" type="text" class="form-control <?PHP is_required($apr_req); ?>"></td>
                  <td><input name="age[]" type="text" class="form-control NumberInput <?PHP is_required($apr_req); ?>" id="age" placeholder="السن" value="<?PHP echo $apr->age; ?>" maxlength="2"></td>
                  <td><?PHP echo $this->haya_model->create_dropbox_list('relationtype','user_relation',$apr->relationtype,0,'',0,1); ?></td>
                  <!--<td><?PHP echo $this->haya_model->create_dropbox_list('professionid','profession',$apr->professionid,0,'',0,1); ?></td>-->
                  <td><input name="profession[]" value="<?PHP echo $apr->profession; ?>" placeholder="المهنة" id="profession" type="text" class="form-control <?PHP is_required($apr_req); ?>"></td>
                  <td><input name="monthly_income[]" value="<?PHP echo $apr->monthly_income; ?>" placeholder="الدخل الشهري" id="monthly_income" type="text" class="form-control NumberInput <?PHP is_required($apr_req); ?>"></td>
                  <td></td>
                </tr>
                <?PHP  }?>
              </tbody>
            </table>
          </div>
          <div class="col-md-12">
            <h4>الحالة الاقتصادية:</h4>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">مصدر الدخل:</label>
            <input name="income" value="<?PHP echo $ah_applicant->income; ?>" maxlength="50" placeholder="مصدر الدخل" id="income" type="text" class="form-control">
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">قيمة الدخل:</label>
            <input name="salary" value="<?PHP echo $ah_applicant->salary; ?>" maxlength="8" placeholder="قيمة الدخل" id="salary" type="text" class="form-control NumberInput">
          </div>
          <div class="col-md-6 form-group">
            <input type="radio" name="source" <?PHP if($ah_applicant->source=='يوجد') { ?> checked <?PHP } ?> class="source" value="يوجد">
            &nbsp;&nbsp;&nbsp;
            <label class="text-warning">يوجد مصدر دخل آخر</label>
            <br>
            <input type="radio" name="source" <?PHP if($ah_applicant->source=='لايوجد') { ?> checked <?PHP } ?> class="source" value="لايوجد">
            &nbsp;&nbsp;&nbsp;
            <label class="text-warning">لايوجد مصدر دخل آخر</label>
          </div>
          <br clear="all">
          <div class="col-md-6 form-group yogid"<?PHP if($ah_applicant->source=='يوجد') { ?> style="display:block !important;" <?PHP } ?>>
            <label class="text-warning">مصدره:</label>
            <input name="musadra" value="<?PHP echo $ah_applicant->musadra; ?>" maxlength="50" placeholder="مصدره" id="musadra" type="text" class="form-control">
          </div>
          <div class="col-md-6 form-group yogid"<?PHP if($ah_applicant->source=='يوجد') { ?> style="display:block !important;" <?PHP } ?>>
            <label class="text-warning">قيمته:</label>
            <input name="qyama" value="<?PHP echo $ah_applicant->qyama; ?>" maxlength="8" placeholder="قيمته" id="qyama" type="text" class="form-control NumberInput">
          </div>
          <div class="col-md-12 form-group layogid"<?PHP if($ah_applicant->source=='لايوجد') { ?> style="display:block !important;" <?PHP } ?>>
            <label class="text-warning">مصدر المعيشة:</label>
            <input name="lamusadra" value="<?PHP echo $ah_applicant->lamusadra; ?>" maxlength="8" placeholder="مصدر المعيشة" id="lamusadra" type="text" class="form-control">
          </div>
          <?php if($form_name	==	'educationform')
		  {
			  $display	=	'display:none;';
		  }
		  else
		  {
			   $display	=	'';
		  }
		  ?>
          <div style=" <?php echo $display;?> ">
          
          <div class="col-md-12 form-group">
            <label class="text-warning">الحالة السكنية:</label>
            <input name="current_situation" value="<?PHP echo $ah_applicant->current_situation; ?>" placeholder="الحالة السكنية" id="current_situation" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <input type="radio" name="ownershiptype" <?PHP if($ah_applicant->ownershiptype=='ملك') { ?> checked <?PHP } ?> class="ownershiptype" value="ملك">
            <label class="text-warning">ملك</label><br>
            <input name="ownershiptype_amount" placeholder="ملك" value="<?PHP echo $ah_applicant->ownershiptype_amount; ?>" id="ownershiptype_amount_a1" type="text" class="form-control NumberInput axClass">
          </div>  
          <div class="col-md-4 form-group">  
            <input type="radio" name="ownershiptype" <?PHP if($ah_applicant->ownershiptype=='ايجار و قيمته') { ?> checked <?PHP } ?> class="ownershiptype" value="ايجار و قيمته">
            <label class="text-warning">ايجار و قيمته</label><br>
            <input name="ownershiptype_amount" placeholder="ايجار و قيمته" value="<?PHP echo $ah_applicant->ownershiptype_amount; ?>" id="ownershiptype_amount_a2" type="text" class="form-control NumberInput axClass">
           </div>
           <div class="col-md-4 form-group">
            <input type="radio" name="ownershiptype" <?PHP if($ah_applicant->ownershiptype=='مساعدة و مصدرها') { ?> checked <?PHP } ?> class="ownershiptype" value="مساعدتة و مصدرها">
            <label class="text-warning">مساعدة</label><br>
            <input name="ownershiptype_amount" placeholder="مساعدة" value="<?PHP echo $ah_applicant->ownershiptype_amount; ?>" id="ownershiptype_amount_a3" type="text" class="form-control axClass">
          </div>
          
         
          
          <div class="col-md-12">
            <h4>مكونات المنزل:</h4>
          </div>
          <div class="col-md-2 form-group">
            <label class="text-warning">نوع البناء:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('building_type','buildingtype',$ah_applicant->building_type,0,''); ?> </div>
          <div class="col-md-2 form-group">
            <label class="text-warning">عدد الغرف:</label>
            <input name="number_of_rooms" value="<?PHP echo $ah_applicant->number_of_rooms; ?>" placeholder="عدد الغرف" id="number_of_rooms" type="text" class="form-control NumberInput">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">المرافق:</label>
            <input name="utilities" value="<?PHP echo $ah_applicant->utilities; ?>" placeholder="المرافق" id="utilities" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">حالة الأثاث والموجودات:</label>
            <input name="furniture" value="<?PHP echo $ah_applicant->furniture; ?>" placeholder="حالة الأثاث والموجودات" id="furniture" type="text" class="form-control">
          </div>
          <div class="col-md-12">
            <h4>تفاصيل البنك:</h4>
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">اسم البنك:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('bankid','bank',$ah_applicant->bankid,0,''); ?> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">الفرع:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('bankbranchid','bank_branch',$ah_applicant->bankbranchid,$ah_applicant->bankid,''); ?> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">رقم الحساب:</label>
            <input name="accountnumber" value="<?PHP echo $ah_applicant->accountnumber; ?>" placeholder="رقم الحساب" id="accountnumber" type="text" class="form-control">
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">نوع الحساب: جاري \ توفير:</label>
            <br>
            <input type="radio" name="accounttype" <?PHP if($ah_applicant->accounttype=='جاري') { ?> checked <?PHP } ?> id="accounttype" checked value="جاري">
            &nbsp;&nbsp;&nbsp;
            <label class="text-warning">جاري</label>
            <input type="radio" name="accounttype" <?PHP if($ah_applicant->accounttype=='توفير') { ?> checked <?PHP } ?> id="accounttype" value="توفير">
            &nbsp;&nbsp;&nbsp;
            <label class="text-warning">توفير</label>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">يرفق صورة من كشف الحسابات (حديث الاصدار):</label>
            <input type="file" name="bankstatement" id="bankstatement" class="form-control">
            <?PHP
          	$statmentURL = base_url().'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->bankstatement;
			echo getFileResult($statmentURL,'يرفق صورة من كشف الحسابات (حديث الاصدار)',$statmentURL);
		  ?>
          </div>
          
          </div>
         <?php if($form_name	==	'educationform'):?>
        <div class="col-md-12">
            <h4>البيانات الدراسية :</h4>
         </div>
         <div class="col-md-4 form-group">
            <label class="text-warning">شهادة ثنوية :</label>
            <input name="certificate_dualism" value="<?PHP echo $ah_applicant->certificate_dualism; ?>" placeholder="شهادة ثنوية" id="certificate_dualism" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">سنة التخرج :</label>
            <input name="graduation_year" value="<?PHP echo $ah_applicant->graduation_year; ?>" placeholder="سنة التخرج" id="graduation_year" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">النسبة :</label>
            <input name="school_percentage" value="<?PHP echo $ah_applicant->school_percentage; ?>" placeholder="النسبة" id="school_percentage" type="text" class="form-control">
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">المرفقات</label>
            <input type="file" name="school_certificate" id="school_certificate" class="form-control">
            <?PHP
          	$statmentURL = base_url().'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->school_certificate;
			echo getFileResult($statmentURL,'المرفقات ',$statmentURL);
		  ?>
          </div>
          <br clear="all" />
          <!-- ---------------------------------------------------- -->
         <div class="col-md-12">
            <h4>البيانات الجامعية :</h4>
         </div>
          
         <div class="col-md-4 form-group">
            <label class="text-warning">اسم الكلية أو الجامعة :</label>
            <input name="college_name" value="<?PHP echo $ah_applicant->college_name; ?>" placeholder="اسم الكلية أو الجامعة" id="college_name" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">التخصص :</label>
            <input name="specialization" value="<?PHP echo $ah_applicant->specialization; ?>" placeholder="التخصص" id="specialization" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">السنة لدراسية :</label>
            <input name="year_of_study" value="<?PHP echo $ah_applicant->year_of_study; ?>" placeholder="السنة لدراسية" id="year_of_study" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">اخر معدل تراكمي :</label>
            <input name="last_grade_average" value="<?PHP echo $ah_applicant->last_grade_average; ?>" placeholder="اخر معدل تراكمي" id="last_grade_average" type="text" class="form-control">
          </div>
          <div class="col-md-8 form-group">
            <label class="text-warning">المرفقات</label>
            <input type="file" name="college_certificate" id="college_certificate" class="form-control">
            <?PHP
          	$statmentURL = base_url().'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->college_certificate;
			echo getFileResult($statmentURL,'المرفقات ',$statmentURL);
		  ?>
          </div>
          <?php endif;?>
		  <div class="col-md-12 form-group">
			<label class="text-warning">الملاحظات:</label>
			<textarea style="height:141px !important;" class="form-control" id="notes" name="notes"></textarea>
			<br class="clear:both;">
		  </div>

        </div>
      </form>
    </div>
  </div>
</div>
<?php $this->load->view('common/footer');?>
<script src="<?PHP echo base_url(); ?>assets/js/tasgeel_js.js"></script>
</body>
</html>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            
              <div class="list-group-item">               
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row" style="display:none;">
                  		<div class="col-md-12">
                        	<div class="col-md-2 text-warning" style="padding-top:8px;">البحث تصفية (فلترة):</div>
                            <div class="col-md-2"><?PHP $this->inq->users_role($user_info['user_role_id']); ?></div>
                            <div class="col-md-2"><input name="first_date"  placeholder="أولا التاريخ" id="first_date" type="text" class="form-control"></div>
                            <div class="col-md-2"><input name="second_date"  placeholder="الثاني تاريخ" id="second_date" type="text" class="form-control"></div>
                            <div class="col-md-2"><button type="button" id="search_filter" class="btn btn-success">بحث</button></div>
                        </div>
                  </div>
                  <style>
				  	#tableSortable tr td:first-child{ text-align:right; }
				  </style>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable">
                    <thead>
                      <tr role="row">
                        <th style="text-align:right !important;">المستخدم</th>
                        <th style="text-align:center !important; width: 50px;">نوع</th>
                        <th style="text-align:center !important; width: 50px;">الأصل</th>                        
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
               
              </div>
          
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'ajax/user_history','columns_array'=>'
{ "data": "المستخدم" },
{ "data": "نوع" },
{ "data": "الأصل" }
')); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
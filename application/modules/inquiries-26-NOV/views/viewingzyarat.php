<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div class="row">
                 <?PHP echo add_button(base_url().'inquiries/updatezyarat/'.$app_id,'اضافه الزيارة الأولية'); ?>
                </div>
                <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"></div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable"
                                           aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center; width: 70px !important;">الزيارة رقم</th>
                        <th style="text-align:center; width: 110px !important;">قيمة الإجار الشهري</th>
                        <th style="text-align:center; width: 165px !important;">هل المقر مناسب للمشروع؟</th>
                        <th style="text-align:center; width: 60px !important;">الكهرباء</th>                       
                        <th style="text-align:center;">تاريخ الزيارة</th>                     
                        <th style="text-align:center; width: 80px !important;">أضيفت من قبل</th>                                                                                                                       
                        <th style="text-align:center; width: 45px !important;">الإجراءات</th>
                        
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'inquiries/viewzyaratlist_ajax/'.$app_id,'columns_array'=>'{ "data": "الزيارة رقم" },
					{ "data": "قيمة الإجار الشهري" },
					{ "data": "هل المقر مناسب للمشروع؟" },
					{ "data": "الكهرباء" },					
					{ "data": "تاريخ الزيارة" },					
					{ "data": "أضيفت من قبل" },			
					{ "data": "الإجراءات"}
					')); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
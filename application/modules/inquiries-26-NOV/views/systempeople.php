<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
               
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"><button type="button" style="margin-top:5px; float:right" data-url="<?PHP echo base_url(); ?>inquiries/newsystempeople" onclick="gototype(this);" class="btn btn-success"><i class="icon-plus-sign"></i> اضافه االتوقيعات </button></div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable"
                                           aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center;">اسم</th>
                        <th style="text-align:center;">اسم المستخدم</th>
                        <th style="text-align:center;">البريد الإلكتروني</th>
                        <th style="text-align:center;">ارقم الهاتف</th>
                        <th style="text-align:center;">فرع</th>
                        <th style="text-align:center;">الإجراءات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                     
                    </tbody>
                  </table>
                </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'ajax/all_users_list/0/19','columns_array'=>' { "data": "اسم" },
                { "data": "اسم المستخدم" },
                { "data": "البريد الإلكتروني" },
                { "data": "رقم الهاتف" },
                { "data": "فرع" },
                { "data": "الإجراءات"}')); ?>

</body>
</html>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
<?php $this->load->view('common/logo'); ?>
<?php $this->load->view('common/usermenu'); ?>
<?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
<?php $this->load->view('common/quicklunchbar'); ?>
<style>
select#relationtype1.form-control
{
	width:98px !important;
}
</style>
<?php
	$ah_applicant 						=	$_applicant_data['ah_applicant'];
	$ah_applicant_wife 					=	$_applicant_data['ah_applicant_wife'];
	$ah_applicant_survayresult 			=	$_applicant_data['ah_applicant_survayresult'];
	$ah_applicant_relation 				=	$_applicant_data['ah_applicant_relation'];
	$ah_applicant_family 				=	$_applicant_data['ah_applicant_family'];
	$ah_applicant_economic_situation 	=	$_applicant_data['ah_applicant_economic_situation'];
	$ah_applicant_documents 			=	$_applicant_data['ah_applicant_documents'];
	$ah_applicants_loans 				=	$_applicant_data['ah_applicants_loans'];
	$view   = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$ah_applicant->applicantid.'/idcard_number"><i class="icon-eye-open"></i></a>';
	$print  .= ' <a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore_print/'.$ah_applicant->applicantid.'/idcard_number"><i class="icon-print"></i></a>';
	$sms  .= ' <a class="iconspace" href="#" data-url="'.base_url().'inquiries/smsmodal/'.$ah_applicant->applicantid.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
?>
<div class="row">
  <div class="col-md-12">
    <?php $this->load->view('common/panel_block', array('module' => $module)); ?> 
    
    <?php if($ah_applicant->applicantid):?>
    <?php // $this->load->view('common/globalfilter', array('type'=>'help','b'=>$branchid,'c'=>$charity_type));?>
    <div class="nav nav-tabs panel panel-default panel-block">
    	<?php echo $html	=	$this->haya_model->get_helps_by_appid($charity_type_id,$ah_applicant->idcard_number);?>
    </div>
    <div class="nav nav-tabs panel panel-default panel-block left">
      <div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">
        <li class="haya_branch_list"><?php echo $view;?></li>
        <li class="haya_branch_list"><?php echo $print;?></li>
        <li class="haya_branch_list"><?php echo $sms;?></li>
      </div>
    </div>
    <?php endif;?>
    <div class="panel panel-default panel-block">
      <form name="frm_tasgeel_form" id="frm_tasgeel_form" action="<?PHP echo base_url(); ?>inquiries/submit_tasgeel" method="post" enctype="multipart/form-data" autocomplete="off">
        
        <input type="hidden" name="refil_data" id="refil_data" value="<?PHP echo $refil_data; ?>">
        <input type="hidden" name="applicantid" id="applicantid" value="<?PHP echo $ah_applicant->applicantid; ?>">
        <input type="hidden" name="formid" id="formid" value="<?PHP echo $formid; ?>">
        <div class="col-md-6 panel panel-default panel-block">
        <h4 style="border-bottom: 2px solid #EEE;">البيانات الأساسية عن الحالة:</h4>
        <div class="col-md-6 form-group">
        	<label class="text-warning">نوع المساعدات:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('charity_type_id','charity_type',$charity_type_id,0,'req'); ?>
        </div>
        <div class="col-md-6 form-group">
            <label class="text-warning">الرقم المدني:</label>
            <input name="idcard_number" value="<?php echo str_replace("/","",$ah_applicant->idcard_number); ?>" placeholder="رقم البطاقة الشخصة" id="idcard_number" type="text" class="form-control req NumberInput <?PHP if($ah_applicant->idcard_number) { if(!$refil_data){?>readonly<?php }} ?>" maxlength="9">
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الاسم الرباعي والقبيلة:</label>
            <input name="fullname" value="<?PHP echo $ah_applicant->fullname; ?>" placeholder="االاسم الرباعي والقبيلة" id="fullname" type="text" class="form-control req">
          </div>
          <div class="col-md-3 form-group">
            <label class="text-warning">الحالة الجتماعية:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('marital_status','marital_status',$ah_applicant->marital_status,0,'req'); ?> </div>
          <div class="col-md-3 form-group">
            <label class="text-warning">رقم جواز سفر:</label>
            <input name="passport_number" value="<?PHP echo $ah_applicant->passport_number; ?>" placeholder="رقم جواز سفر" id="passport_number" type="text" class="form-control Passport <?PHP if($ah_applicant->passport_number) { if(!$refil_data){?>readonly<?PHP } } ?>">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">جنسية:</label>
            <?php echo $this->haya_model->create_dropbox_list('country_listmanagement','nationality',$ah_applicant->country_listmanagement,0,''); ?> </div>
           <div class="col-md-2 form-group">
            <label class="text-warning">تاريخ الميلاد:</label>
            <input name="date_of_birth" value="<?PHP echo $ah_applicant->date_of_birth; ?>" placeholder="تاريخ الميلاد" id="date_of_birth" type="text" class="form-control req age_datepicker" set-age="applicant_age" /></div>
          <div class="col-md-2 form-group">
            <label class="text-warning">عمر:</label>
            <input name="applicant_age" value="<?PHP echo $ah_applicant->applicant_age; ?>" placeholder="عمر" id="applicant_age" type="text" class="form-control req"  /></div>
          <div class="col-md-4 form-group">
            <label class="text-warning">جنس:</label>
            <select class="form-control req" name="gender" id="gender">
            	<option value="">جنس</option>
            	<option	value="ذكر" <?php if($ah_applicant->gender	==	'ذكر'){?> selected="selected" <?php }?>>ذكر</option>
                <option value="أنثى" <?php if($ah_applicant->gender	==	'أنثى'){?> selected="selected" <?php }?>>أنثى</option>
            </select></div>
          <div class="col-md-4 form-group">
            <label class="text-warning">المحافظة \ المنطقة:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('province','regions',$ah_applicant->province,0,'req'); ?> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">ولاية:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('wilaya','wilaya',$ah_applicant->wilaya,$ah_applicant->province,'req'); ?> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">البلدة \ المحلة:</label>
            <input name="address" value="<?PHP echo $ah_applicant->address; ?>" placeholder="البلدة \ المحلة" id="address" type="text" class="form-control req">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">هاتف المنزل:</label>
            <input name="hometelephone" onBlur="checkPhoneNumber(this);" value="<?PHP echo $ah_applicant->hometelephone; ?>" placeholder="هاتف المنزل" id="hometelephone" type="text" class="form-control NumberInput">
          </div>
          <?PHP
          	$extraphone = json_decode($ah_applicant->extratelephone);
		  ?>
          <div class="col-md-4 form-group">
            <label class="text-warning">رقم الهاتف :</label>
            <input name="extratelephone[]" onBlur="checkPhoneNumber(this);" value="<?PHP echo $extraphone[0]; ?>" maxlength="8" placeholder="رقم الهاتف" id="extratelephone" type="text" class="form-control req NumberInput">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">رقم الهاتف :</label>
            <input name="extratelephone[]" onBlur="checkPhoneNumber(this);" value="<?PHP echo $extraphone[1]; ?>" maxlength="8" placeholder="رقم الهاتف" id="extratelephone" type="text" class="form-control NumberInput">
          </div>
          
          
          <!-----------Wife or Husband------------------->
          
          <div class="col-md-12 form-group">
          <?php 
		  if($form_name	==	'educationform')
		  {
			  $required	=	'req';
		  }
		  else
		  {
			  $required	=	'';
		  }
		  ?>
          <table width="100%" class="table" style="border: 1px solid #EFEFEF;" border="0" cellspacing="0" cellpadding="0">
           	 <?php if($form_name	==	'educationform'):?>
              <tr>
              	<td colspan="2" class="my_td">اسم الاب</td>
                <td class="my_td">رقم الجواز \ الرقم المدني</td>
              </tr>
              <tr>
              	<td colspan="2"><input name="father_name" value="<?PHP echo $ah_applicant->father_name; ?>" placeholder="اسم الاب" id="father_name" type="text" class="form-control <?php echo $required;?>"></td>
                <td><input name="father_id_card" value="<?PHP echo $ah_applicant->father_id_card; ?>" placeholder="رقم الجواز\الرقم المدني" id="father_id_card" type="text" class="form-control <?php echo $required;?> <?PHP if($ah_applicant->father_id_card) { ?>disable<?PHP } ?>"></td>
              </tr>
              <?php endif;?>
             <tr>
              	<td colspan="2" class="my_td">اسم الام</td>
                <td class="my_td">رقم الجواز \ الرقم المدني</td>
              </tr>
              <tr>
              	<td colspan="2"><input name="mother_name" value="<?PHP echo $ah_applicant->mother_name; ?>" placeholder="اسم الام" id="mother_name" type="text" class="form-control <?php echo $required;?>"></td>
                <td><input name="mother_id_card" value="<?PHP echo $ah_applicant->mother_id_card; ?>" placeholder="رقم الجواز\الرقم المدني" id="mother_id_card" type="text" class="form-control <?php echo $required;?> <?PHP if($ah_applicant->mother_id_card) { ?>disable<?PHP } ?>"></td>
              </tr>
              <?php if($form_name	!=	'educationform'):?>
              <tr class="fixfor92" <?PHP if($ah_applicant->charity_type_id==92) { echo(' style="display:block" '); } ?>>
                <td class="my_td" style="width:34% !important;">اسم الزوج \ الزوجة</td>
                <td class="my_td" style="width:33% !important;">رقم الجواز \ الرقم المدني</td>
                <td class="my_td" style="width:33% !important;">جنسية</td>
              </tr>
              <?PHP for($i=0; $i<=3; $i++) { 
		  		if($i==0)
				{	$req = ''; }
				else
				{	$req = ''; }
		  ?>
              <tr class="fixfor92" <?PHP if($ah_applicant->charity_type_id==92) { echo(' style="display:block" '); } ?>>
                <td><input name="relationname[]" value="<?PHP echo $ah_applicant_wife[$i]->relationname; ?>" placeholder="اسم الزوج\الزوجة" id="relationname" type="text" class="form-control marital_status<?PHP echo $i;?> name<?PHP echo $i;?> <?PHP //if($ah_applicant->charity_type_id==92) { echo $req; }?>"></td>
                <td><?PHP if($ah_applicant_wife[$i]->relationdoc=='') { ?>
            <input name="relationdoc[]" value="<?PHP echo $ah_applicant_wife[$i]->relationdoc; ?>" placeholder="رقم الجواز\الرقم المدني" id="passport_number_wife" type="text" class="form-control passport_number_wife marital_status<?PHP echo $i;?> <?PHP if($ah_applicant_wife[$i]->relationdoc) { ?>disable<?PHP } ?> <?PHP //if($ah_applicant->charity_type_id==92) { echo $req; }?>">
          	<?PHP } else { ?><br>
            	<strong class="display"><?PHP echo $ah_applicant_wife[$i]->relationdoc; ?></strong>
            	<input type="hidden" name="relationdoc[]" value="<?PHP echo $ah_applicant_wife[$i]->relationdoc; ?>">
            <?PHP } ?></td>
                <td><?PHP echo $this->haya_model->create_dropbox_list('relationnationality','nationality',$ah_applicant_wife[$i]->relationnationality,0,$req,0,1); ?></td>
              </tr>
              <?PHP } ?>
             <?php endif;?>
             
            </table>

          
          
         
          </div>
          
          <!-----------Wife or Husband End------------------->
        
          <div class="col-md-12 form-group">
             <h4>ترفق الوثائق والمستندات اللازمة للطلب</h4>
                  <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                    <?PHP 
					$doccount = 0;
					
					foreach($this->inq->allRequiredDocument($charity_type_id) as $ctid) { $doccount++; 
						$doc = $ah_applicant_documents[$ctid->documentid];						
						$url = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$doc->document;	?>
                    <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                      <div class="panel-heading" style="padding:10px 3px;" id="head<?PHP echo $ctid->documentid;?>">
                        <h4 class="panel-title" style="font-size:15px;">
                          <?PHP if($doc->appli_doc_id!='') { ?>
                          <span class="icons" id="removeicons<?PHP echo $doc->appli_doc_id; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> <i onClick="removeDocument(this);" data-id="<?PHP echo $doc->appli_doc_id; ?>" data-remove="<?PHP echo $ctid->documentid; ?>" class="icon-remove-sign" style="color:#FF0000; cursor:pointer;"></i> </span>
                          <?PHP } ?>
                          <a style="width:95% !important;" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $ctid->documentid;?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                      </div>
                      <div id="demo-collapse<?PHP echo $ctid->documentid;?>" class="panel-collapse collapse">
                        <div class="panel-body" style="text-align:right;">
                          <input type="file" name="doclist<?PHP echo $ctid->documentid;?>" placeholder="<?PHP echo $ctid->documenttype;?>" class="form-control <?PHP if($ctid->isrequired==1 && $doc->document=='') { echo 'req'; } ;?>">
                        </div>
                      </div>
                    </div>
                    <?PHP } ?>
                  </div>
          </div>
          <div class="col-md-12 form-group">
          <div class="col-md-4 form-group">
            <?PHP echo $this->haya_model->module_button(66,$ah_applicant->applicantid,'save_malya_naqdia',$ah_applicant->case_close); ?>
         </div>
		 <?php if(isset($refil_data) AND $ah_applicant->case_close	==	'0'):?>
              <div class="col-md-8 form-group">
                <input type="radio" name="record_type" checked id="record_type" checked value="update">
                &nbsp;&nbsp;&nbsp;
                <label class="text-warning">تحديث هذا السجل</label>
                <input type="radio" name="record_type"  id="record_type" value="new">
                &nbsp;&nbsp;&nbsp;
                <label class="text-warning">إضافة قياسيا جديدا</label>
              </div>
          <?php endif;?>
          </div>
        </div>
        <div class="col-md-6  panel panel-default panel-block" style="border-right: 2px solid #eee;">
          <div class="col-md-12 form-group">
            <h4>افراد الأسرة: (بما فهيم العاملين و غيرالعاملين)
              <button type="button" id="azafa_afrad_al_sarah" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px;">اضافة افراد الأسرة</button>
            </h4>
            <table class="table table-bordered table-striped dataTable" id="afrad_al_sarah">
              <thead>
                <tr role="row">
                  <th class="my_td">الاسم</th>
                  
                  <th class="my_td" style="width: 80px;">سنة الميلاد</th>
                  <th class="my_td" style="width: 50px;">العمر</th>
                  <th class="my_td">القرابة</th>
                  <th class="my_td">المهنة</th>
                  <th class="my_td" style="width: 70px;">الدخل الشهري</th>
                  <th class="my_td">الإجراءات</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?PHP 
			  
			  if($ah_applicant_relation)
			  {
				  $apr_req = 0;
			  foreach($ah_applicant_relation as $apr) { 
			  			$relationBox = 'yearbox'.$apr_req;
			  ?>
                <tr>
                  <td><input name="relation_fullname[]" value="<?PHP echo $apr->relation_fullname; ?>" placeholder="الاسم" id="relation_fullname" type="text" class="form-control <?PHP is_required($apr_req); ?>"></td>
                  <td><input type="text"  value="<?PHP echo $apr->birthyear; ?>" name="birthyear[]" data-id="<?PHP echo $relationBox; ?>" onBlur="populateAge(this);" class="form-control NumberInput <?PHP is_required($apr_req); ?>" placeholder="سنة الميلاد" maxlength="4"></td>
                  <td><input readonly name="age[]" type="text" class="form-control <?PHP echo $relationBox; ?> NumberInput <?PHP is_required($apr_req); ?>" id="age" placeholder="السن" value="<?PHP echo $apr->age; ?>" maxlength="2"></td>
                  <td><?PHP echo $this->haya_model->create_dropbox_list('relationtype','user_relation',$apr->relationtype,0,'',0,1); ?></td>
                  <!--<td><?PHP echo $this->haya_model->create_dropbox_list('professionid','profession',$apr->professionid,0,'',0,1); ?></td>-->
                  <td><input name="profession[]" value="<?PHP echo $apr->profession; ?>" placeholder="المهنة" id="profession" type="text" class="form-control <?PHP is_required($apr_req); ?>"></td>
                  <td><input name="monthly_income[]" value="<?PHP echo $apr->monthly_income; ?>" placeholder="الدخل الشهري" id="monthly_income" type="text" class="form-control NumberInput <?PHP is_required($apr_req); ?>" style="width:60px !important;"></td>
                  <td></td>
                </tr>
                <?PHP $apr_req++; }
			  }
			  else
			  { ?>
                <tr>
                  <td><input name="relation_fullname[]" value="<?PHP echo $apr->relation_fullname; ?>" placeholder="الاسم" id="relation_fullname" type="text" class="form-control <?PHP is_required($apr_req); ?>"></td>
                  <td><input type="text" name="birthyear[]" data-id="yearbox0" onBlur="populateAge(this);" class="form-control NumberInput <?PHP is_required($apr_req); ?>" placeholder="سنة الميلاد" maxlength="4"></td>
                  <td><input name="age[]" readonly type="text" class="form-control NumberInput yearbox0 <?PHP is_required($apr_req); ?>" id="age" placeholder="السن" value="<?PHP echo $apr->age; ?>" maxlength="2"></td>
                  <td><?PHP echo $this->haya_model->create_dropbox_list('relationtype','user_relation',$apr->relationtype,0,'',0,1); ?></td>
                  <!--<td><?PHP echo $this->haya_model->create_dropbox_list('professionid','profession',$apr->professionid,0,'',0,1); ?></td>-->
                  <td><input name="profession[]" value="<?PHP echo $apr->profession; ?>" placeholder="المهنة" id="profession" type="text" class="form-control <?PHP is_required($apr_req); ?>"></td>
                  <td><input name="monthly_income[]" value="<?PHP echo $apr->monthly_income; ?>" placeholder="الدخل الشهري" id="monthly_income" type="text" class="form-control NumberInput <?PHP is_required($apr_req); ?>" style="width:60px !important;"></td>
                  <td></td>
                </tr>
                <?PHP  }?>
              </tbody>
            </table>
          </div>
          <div class="col-md-12">
            <h4>الحالة الاقتصادية:</h4>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">مصدر الدخل:</label>
            <input name="income" value="<?PHP echo $ah_applicant->income; ?>" maxlength="50" placeholder="مصدر الدخل" id="income" type="text" class="form-control req">
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">قيمة الدخل:</label>
            <input name="salary" value="<?PHP echo $ah_applicant->salary; ?>" maxlength="8" placeholder="قيمة الدخل" id="salary" type="text" class="form-control NumberInput TotalControl req">
          </div>
          <div class="col-md-6 form-group">
            <input type="radio" name="source" <?PHP if($ah_applicant->source=='يوجد') { ?> checked <?PHP } ?> class="source" value="يوجد">
            &nbsp;&nbsp;&nbsp;
            <label class="text-warning">يوجد مصدر دخل آخر</label>
            <br>
            <input type="radio" name="source" <?PHP if($ah_applicant->source=='لايوجد') { ?> checked <?PHP } ?> class="source" value="لايوجد">
            &nbsp;&nbsp;&nbsp;
            <label class="text-warning">لايوجد مصدر دخل آخر</label>
          </div>
          <?PHP
          		$totalQyama = $ah_applicant->salary+$ah_applicant->qyama+$ah_applicant->qyama_2+$ah_applicant->qyama_3+$ah_applicant->qyama_4;
		  ?>
          <br clear="all">
          <div class="col-md-12 form-group yogid qrozBox" style="<?PHP if($ah_applicant->source=='يوجد') { echo('display:block !important;'); } ?>">
          	<div class="col-md-1 form-group center" style="padding:0px !important;"><label class="text-warning">&nbsp;</label></div>
            <div class="col-md-5 form-group center" style="padding:0px !important;"><label class="text-warning">مصدره:</label></div>
            <div class="col-md-6 form-group center" style="padding:0px !important;"><label class="text-warning">قيمته:</label></div>
          
          	<div class="col-md-1 form-group center">1.</div>
         	<div class="col-md-5 form-group "><input name="musadra" value="<?PHP echo($ah_applicant->musadra);  ?>" maxlength="50" placeholder="مصدره" type="text" class="form-control"></div>
          	<div class="col-md-6 form-group"><input name="qyama" value="<?PHP echo($ah_applicant->qyama); ?>" maxlength="8" placeholder="قيمته" type="text" class="form-control LaYogid NumberInput TotalControl"></div>
 
           	<div class="col-md-1 form-group center">2.</div>
         	<div class="col-md-5 form-group "><input name="musadra_2" value="<?PHP echo($ah_applicant->musadra_2);  ?>" maxlength="50" placeholder="مصدره"  type="text" class="form-control"></div>
          	<div class="col-md-6 form-group"><input name="qyama_2" value="<?PHP echo($ah_applicant->qyama_2); ?>" maxlength="8" placeholder="قيمته" type="text" class="form-control LaYogid NumberInput TotalControl"></div>
          
          	<div class="col-md-1 form-group center">3.</div>
         	<div class="col-md-5 form-group "><input name="musadra_3" value="<?PHP echo($ah_applicant->musadra_3);  ?>" maxlength="50" placeholder="مصدره" type="text" class="form-control"></div>
          	<div class="col-md-6 form-group"><input name="qyama_3" value="<?PHP echo($ah_applicant->qyama_3); ?>" maxlength="8" placeholder="قيمته" type="text" class="form-control LaYogid NumberInput TotalControl"></div>
 
          	<div class="col-md-1 form-group center">4.</div>
         	<div class="col-md-5 form-group "><input name="musadra_4" value="<?PHP echo($ah_applicant->musadra_4);  ?>" maxlength="50" placeholder="مصدره" type="text" class="form-control"></div>
          	<div class="col-md-6 form-group"><input name="qyama_4" value="<?PHP echo($ah_applicant->qyama_4); ?>" maxlength="8" placeholder="قيمته" type="text" class="form-control LaYogid NumberInput TotalControl"></div>
                                       
          	
          <br clear="all">
          </div>
          <div class="col-md-6 form-group left"><label class="text-warning" style="padding-top:7px;">مجموع الدخل:</label></div>
          <div class="col-md-6 form-group"><input readonly placeholder="مجموع الدخل" value="<?PHP echo $totalQyama; ?>" id="totalQyama" type="text" class="form-control NumberInput"></div>
		  <div class="col-md-12 form-group qrozBox">
            <h4>القروض:
              <button type="button" id="ijafa_qurad_button" class="btn btn-sm btn-success qAction" style="float: left; margin-bottom: 8px; <?PHP if(sizeof($ah_applicants_loans) > 0) { ?> display:block; <?PHP } ?>">اضافة قرض</button>
            </h4>
            <div class="col-md-12 form-group" style="padding:0px !important;">
            <input type="radio" name="q_source" <?PHP if($ah_applicant->q_source=='يوجد') { ?> checked <?PHP } ?> class="q_source" value="يوجد">          
            <label class="text-warning">يوجد</label>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="q_source" <?PHP if($ah_applicant->q_source=='لايوجد') { ?> checked <?PHP } ?> class="q_source" value="لايوجد">
            
            <label class="text-warning">لايوجد</label>
            </div>
            <table class="table table-bordered table-striped dataTable qAction" <?PHP if($ah_applicant->q_source=='يوجد') { ?> style="display:inline-table;" <?PHP } ?> id="ijafa_qurad">
              <thead>
                <tr role="row">
                  <th class="my_td">نوع القرض</th>
                  <th class="my_td">قبمة القرض</th>
                  <th class="my_td">القسط الشهري</th>                 
                  <th class="my_td">الإجراءات</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?PHP 
			  
			  if($ah_applicants_loans)
			  {
				  $apr_req = 0;
			  foreach($ah_applicants_loans as $loans) { 
			  	$lmaount += $loans->loan_amount;
			  ?>
                <tr>
                  <td><input type="text" value="<?PHP echo $loans->loan_type;  ?>" name="loan_type[]" class="form-control" placeholder="نوع القرض"></td>
                  <td><input type="text" value="<?PHP echo $loans->loan_amount;  ?>" name="loan_amount[]" class="form-control NumberInput " placeholder="قبمة القرض"></td>
                  <td><input type="text" value="<?PHP echo $loans->loan_limit;  ?>" onKeyUp="getSafiValue();" name="loan_limit[]" class="form-control SafiInupt NumberInput" placeholder="القسط الشهري"></td>
                 <td>&nbsp;</td>
                </tr>
                <?PHP $apr_req++; }
			  }
			  else
			  { ?>
                <tr>
                  <td><input type="text" name="loan_type[]" class="form-control" placeholder="نوع القرض"></td>
                  <td><input type="text" name="loan_amount[]"    class="form-control NumberInput " placeholder="قبمة القرض"></td>
                  <td><input type="text" name="loan_limit[]" onKeyUp="getSafiValue();" class="form-control SafiInupt NumberInput" placeholder="القسط الشهري"></td>
                 <td>&nbsp;</td>
                </tr>
                <?php  }
					$totalSafi = $totalQyama-$lmaount;
				?>
               
              </tbody>
            </table>
          </div>
		  <div class="col-md-6 form-group left"><label class="text-warning" style="padding-top:7px;">صافي الدخل:</label></div>
          <div class="col-md-6 form-group"><input readonly placeholder="صافي الدخل" value="<?PHP echo $totalSafi-$ah_applicant->ownershiptype_amount; ?>" id="totalSafi" type="text" class="form-control NumberInput"></div>
		  
          <!--<div class="col-md-12 form-group layogid"<?PHP if($ah_applicant->source=='لايوجد') { ?> style="display:block !important;" <?PHP } ?>>
            <label class="text-warning">مصدر المعيشة:</label>
            <input name="lamusadra" value="<?PHP echo $ah_applicant->lamusadra; ?>" maxlength="8" placeholder="مصدر المعيشة" id="lamusadra" type="text" class="form-control">
          </div>-->
          <?php if($form_name	==	'educationform')
		  {
			  $display	=	'display:none;';
		  }
		  else
		  {
			   $display	=	'';
		  }
		  ?>
          <div style=" <?php echo $display;?> ">
          
          <div class="col-md-12 form-group">
            <label class="text-warning">الحالة السكنية:</label>
            <input name="current_situation" value="<?PHP echo $ah_applicant->current_situation; ?>" placeholder="الحالة السكنية" id="current_situation" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <input type="radio" name="ownershiptype" <?PHP if($ah_applicant->ownershiptype=='ملك') { ?> checked <?PHP } ?> class="ownershiptype" value="ملك">
            <label class="text-warning">ملك</label><br>
            <input <?php if($ah_applicant->ownershiptype=='ملك') { ?> name="ownershiptype_amount" <?PHP } ?> placeholder="ملك" value="0" id="ownershiptype_amount_a1" type="text" class="form-control NumberInput axClass" <?PHP if($ah_applicant->ownershiptype=='ملك') { ?> style="display:none !important;" <?PHP } ?> />
          </div>  
          <div class="col-md-4 form-group">  
            <input type="radio" name="ownershiptype" <?PHP if($ah_applicant->ownershiptype=='ايجار و قيمته') { ?> checked <?PHP } ?> class="ownershiptype" value="ايجار و قيمته">
            <label class="text-warning">ايجار و قيمته</label><br>
            <input <?PHP if($ah_applicant->ownershiptype=='ايجار و قيمته') { ?>  name="ownershiptype_amount"  <?PHP } ?> placeholder="ايجار و قيمته" value="<?PHP echo $ah_applicant->ownershiptype_amount; ?>" id="ownershiptype_amount_a2" type="text" class="form-control NumberInput axClass" <?PHP if($ah_applicant->ownershiptype=='ايجار و قيمته') { ?> style="display:block !important;" <?PHP } ?> />
           </div>
           <div class="col-md-4 form-group">
            <input type="radio" name="ownershiptype" <?PHP if($ah_applicant->ownershiptype=='مساعدتة و مصدرها') { ?> checked <?PHP } ?> class="ownershiptype" value="مساعدتة و مصدرها">
            <label class="text-warning">مساعدة</label><br>
            <input <?PHP if($ah_applicant->ownershiptype=='مساعدتة و مصدرها') { ?>  name="ownershiptype_amount"  <?PHP } ?> placeholder="مساعدة" value="<?PHP echo $ah_applicant->ownershiptype_amount; ?>" id="ownershiptype_amount_a3" type="text" class="form-control axClass" <?PHP if($ah_applicant->ownershiptype=='مساعدتة و مصدرها') { ?> style="display:block !important;" <?PHP } ?> />
          </div>
         
          <div class="col-md-12">
            <h4>مكونات المنزل:</h4>
          </div>
          <div class="col-md-3 form-group">
            <label class="text-warning">نوع البناء:</label>
            <?php echo $this->haya_model->create_dropbox_list('building_type','buildingtype',$ah_applicant->building_type,0,''); ?> </div>
          <div class="col-md-3 form-group">
            <label class="text-warning">عدد الغرف:</label>
            <?php number_drop_box('number_of_rooms',$ah_applicant->number_of_rooms,'عدد الغرف',1,20,'عدد الغرف'); ?> 
            
          </div>
          <div class="col-md-3 form-group">
            <label class="text-warning">المرافق:</label>
            <input name="utilities" value="<?PHP echo $ah_applicant->utilities; ?>" placeholder="المرافق" id="utilities" type="text" class="form-control">
          </div>
          <div class="col-md-3 form-group">
            <label class="text-warning">حالة الأثاث والموجودات:</label>
            <input name="furniture" value="<?PHP echo $ah_applicant->furniture; ?>" placeholder="حالة الأثاث والموجودات" id="furniture" type="text" class="form-control">
          </div>
          <div class="col-md-12">
            <h4>تفاصيل البنك:</h4>
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">اسم البنك:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('bankid','bank',$ah_applicant->bankid,0,''); ?> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">الفرع:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('bankbranchid','bank_branch',$ah_applicant->bankbranchid,$ah_applicant->bankid,''); ?> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">رقم الحساب:</label>
            <input name="accountnumber" value="<?PHP echo $ah_applicant->accountnumber; ?>" placeholder="رقم الحساب" id="accountnumber" type="text" class="form-control">
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">نوع الحساب: جاري \ توفير:</label>
            <br>
            <input type="radio" name="accounttype" <?PHP if($ah_applicant->accounttype=='جاري') { ?> checked <?PHP } ?> id="accounttype" checked value="جاري">
            &nbsp;&nbsp;&nbsp;
            <label class="text-warning">جاري</label>
            <input type="radio" name="accounttype" <?PHP if($ah_applicant->accounttype=='توفير') { ?> checked <?PHP } ?> id="accounttype" value="توفير">
            &nbsp;&nbsp;&nbsp;
            <label class="text-warning">توفير</label>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">يرفق صورة من كشف الحسابات (حديث الاصدار):</label>
            <input type="file" name="bankstatement" id="bankstatement" class="form-control">
            <?PHP
          	$statmentURL = base_url().'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->bankstatement;
			echo getFileResult($statmentURL,'يرفق صورة من كشف الحسابات (حديث الاصدار)',$statmentURL);
		  ?>
          </div>
          
          </div>
         <?php if($form_name	==	'educationform'):?>
        <div class="col-md-12">
            <h4>البيانات الدراسية :</h4>
         </div>
         <div class="col-md-4 form-group">
            <label class="text-warning">شهادة ثنوية :</label>
            <input name="certificate_dualism" value="<?PHP echo $ah_applicant->certificate_dualism; ?>" placeholder="شهادة ثنوية" id="certificate_dualism" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">سنة التخرج :</label>
            <input name="graduation_year" value="<?PHP echo $ah_applicant->graduation_year; ?>" placeholder="سنة التخرج" id="graduation_year" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">النسبة :</label>
            <input name="school_percentage" value="<?PHP echo $ah_applicant->school_percentage; ?>" placeholder="النسبة" id="school_percentage" type="text" class="form-control">
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">المرفقات</label>
            <input type="file" name="school_certificate" id="school_certificate" class="form-control">
            <?PHP
          	$statmentURL = base_url().'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->school_certificate;
			echo getFileResult($statmentURL,'المرفقات ',$statmentURL);
		  ?>
          </div>
          <br clear="all" />
          <!-- ---------------------------------------------------- -->
         <div class="col-md-12">
            <h4>البيانات الجامعية :</h4>
         </div>
          
         <div class="col-md-4 form-group">
            <label class="text-warning">اسم الكلية أو الجامعة :</label>
            <input name="college_name" value="<?PHP echo $ah_applicant->college_name; ?>" placeholder="اسم الكلية أو الجامعة" id="college_name" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">التخصص :</label>
            <input name="specialization" value="<?PHP echo $ah_applicant->specialization; ?>" placeholder="التخصص" id="specialization" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">السنة لدراسية :</label>
            <input name="year_of_study" value="<?PHP echo $ah_applicant->year_of_study; ?>" placeholder="السنة لدراسية" id="year_of_study" type="text" class="form-control">
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">اخر معدل تراكمي :</label>
            <input name="last_grade_average" value="<?PHP echo $ah_applicant->last_grade_average; ?>" placeholder="اخر معدل تراكمي" id="last_grade_average" type="text" class="form-control">
          </div>
          <div class="col-md-8 form-group">
            <label class="text-warning">المرفقات</label>
            <input type="file" name="college_certificate" id="college_certificate" class="form-control">
            <?PHP
          	$statmentURL = base_url().'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->college_certificate;
			echo getFileResult($statmentURL,'المرفقات ',$statmentURL);
		  ?>
          </div>
          <?php endif;?>
          <div class="col-md-12 form-group">
			<label class="text-warning">نواقص الطلب:</label>
			<textarea style="height:141px !important;" class="form-control" id="extra_detail" name="extra_detail"><?PHP echo (isset($ah_applicant->extra_detail) ? $ah_applicant->extra_detail : NULL); ?></textarea>
			<br class="clear:both;">
		  </div>
		  <div class="col-md-12 form-group">
			<label class="text-warning">الملاحظات:</label>
			<textarea style="height:141px !important;" class="form-control" <?php if(isset($formid)){?>id="wysiwyg" <?php }?>name="notes"><?PHP echo $notes; ?></textarea>
			<br class="clear:both;">
		  </div>

        </div>
      </form>
    </div>
  </div>
</div>
<?php $this->load->view('common/footer');?>
<script>
$(function(){
		var marital_status = $('#marital_status').val();
		if(marital_status==92)
		{	$('.fixfor92').slideDown('slow');	
			$('#relationname').addClass('req');
			<?php if($formid){?>
				$('.name0').val('<?PHP echo $ah_applicant->relation; ?>');
			<?php }?>
			$('#passport_number_wife').addClass('req');
		}
		/*else
		{	$('.fixfor92').slideUp('slow');	
			$('#relationname').removeClass('req');
			$('#mother_name').val('<?PHP echo $ah_applicant->relation; ?>');
			$('#passport_number_wife').removeClass('req');
		
		}*/
		
	$('.TotalControl').keyup(function(){
		var tc = 0;
		$('.TotalControl').each(function(index, element) {
            if($(this).val()!='')
			{
				tc += parseInt($(this).val());
			}
        });
		$('#totalQyama').val(tc);
		$('#totalSafi').val(tc);
	});
	
	$('#marital_status').change(function(){
		var marital_status = $(this).val();
		if(marital_status==92)
		{	$('.fixfor92').slideDown('slow');	
			$('#relationname').addClass('req');
			$('.name0').val('<?PHP echo $ah_applicant->relation; ?>');
			$('#passport_number_wife').addClass('req');
		}
		else
		{	$('.fixfor92').slideUp('slow');	
			$('#relationname').removeClass('req');
			$('#passport_number_wife').removeClass('req');
		
		}
	});
	
	$('#charity_type_id').change(function(){
		var charity_type_id = $(this).val();
		var formid = $('#formid').val();
		location.href = config.BASE_URL+'inquiries/newformwitholddata/'+charity_type_id+'/'+formid+'/0';	
	});
		 
}); 
</script>
<script src="<?PHP echo base_url(); ?>assets/js/tasgeel_js.js"></script>
</body>
</html>
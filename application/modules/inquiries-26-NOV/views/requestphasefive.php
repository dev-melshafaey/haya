<!doctype html>
<?PHP
	$ah_applicant 						=	$_applicant_data['ah_applicant'];
	$ah_applicant_wife 					=	$_applicant_data['ah_applicant_wife'];
	$ah_applicant_survayresult			=	$_applicant_data['ah_applicant_survayresult'];
	$ah_applicant_relation				=	$_applicant_data['ah_applicant_relation'];
	$ah_applicant_family				=	$_applicant_data['ah_applicant_family'];
	$ah_applicant_economic_situation	=	$_applicant_data['ah_applicant_economic_situation'];
	$ah_applicant_documents 			=	$_applicant_data['ah_applicant_documents'];
	$ah_applicants_loans 				=	$_applicant_data['ah_applicants_loans'];
	
	$segment_2	=	$this->uri->segment(2);
	$segment_3	=	$this->uri->segment(3);
	
	if(isset($segment_2) && $segment_2	==	'socilservayresult')
	{
		if(isset($segment_3))
		{
			$module['module_name']	=	'الباحث الاجتماعي';
		}
	}
?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <?php $msg	=	$this->session->flashdata('msg');?>
  <?php if($msg):?>
  <div class="col-md-12">
    <div style="padding: 22px 20px !important; background:#c1dfc9;">
        <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
    </div>
  </div>
   <?php endif;?>
  <div class="row">
  <form id="add_socilservayresult" name="add_socilservayresult" method="post" action="<?PHP echo base_url().'inquiries/add_socilservayresult' ?>" autocomplete="off">
    <input type="hidden" name="step" id="step" value="2" />
    <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $ah_applicant->applicantid;?>" />
    <input type="hidden" name="sarvayid" id="sarvayid" value="<?php echo $ah_applicant_survayresult->sarvayid;?>" />
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module, 'headxxx' => $heading)); ?>
      <div class="col-md-12">
        <div class="col-md-5 fox leftborder">
          <h4 class="panel-title customhr">بيانات مقدم الطلب</h4>
          <div class="col-md-12 form-group">
            <label class="text-warning">الاسم الرباعي والقبيلة: </label> <strong style="color: #000 !important; font-size: 16px;"><?php echo $ah_applicant->fullname; ?> (<?PHP echo a_date($ah_applicant->applicant_age); ?>)</strong></div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الاستمارة: </label>
            <?php echo a_date($ah_applicant->applicantcode); ?></div>
          <div class="col-md-6 form-group">
            <label class="text-warning">التاريخ: </label>
            <?php echo a_date(show_date($ah_applicant->registrationdate,5)); ?></div>
          <div class="col-md-6 form-group">
            <label class="text-warning">نوع الطلب: </label>
            <?php echo $this->haya_model->get_name_from_list($ah_applicant->charity_type_id); ?></div>
          
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم جواز سفر: </label>
            <?php echo is_set(a_date($ah_applicant->passport_number)); ?></div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الرقم المدني: </label>
            <?php echo a_date($ah_applicant->idcard_number); ?></div>
            <div class="col-md-6 form-group">
            <label class="text-warning">الدولة: </label>
            <?php echo $this->haya_model->get_name_from_list($ah_applicant->country); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">المحافظة: </label>
            <?php echo $this->haya_model->get_name_from_list($ah_applicant->province); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">ولاية: </label>
            <?php echo $this->haya_model->get_name_from_list($ah_applicant->wilaya); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">البلدة / المحلة: </label>
            <?php echo $ah_applicant->address; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">هاتف المنزل: </label>
            <?php echo a_date($ah_applicant->hometelephone); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الهاتف :</label>
            <?php echo a_date(implode('<br>',json_decode($ah_applicant->extratelephone,TRUE))); ?></strong></div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الحالة الجتماعية:</label>
            <?php echo $this->haya_model->get_name_from_list($ah_applicant->marital_status); ?></strong> </div>
          <?php foreach($ah_applicant_wife as $wife) { ?>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم الزوج/الزوجة: </label>
            <?php echo $wife->relationname; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الجواز/الرقم المدني: </label>
            <?php echo a_date($wife->relationdoc); ?></strong> </div>
          <?php } ?>
          <?php if($ah_applicant->mother_name):?>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم الام: </label>
            <?php echo $ah_applicant->mother_name; ?></strong> </div>
          <?php endif;?>
          <?php if($ah_applicant->mother_id_card):?>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الجواز\البطاقة: </label>
            <?php echo a_date($ah_applicant->mother_id_card); ?></strong> </div>
          <?php endif;?>
          <!----------------- Documents ------------------------> 
          <!---------------------------------------------------->
          <div class="col-md-12 form-group" style="padding:0px !important;">
            <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
              <h4 class="panel-title customhr">الوثائق والمستندات المرفقة مع طلب المساعدة</h4>
              <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                <?PHP 
					//print_r($this->inq->allRequiredDocument($ah_applicant->charity_type_id));
					$doccount = 0;					
					foreach($this->inq->allRequiredDocument($ah_applicant->charity_type_id) as $ctid) { $doccount++; 
						$doc = $ah_applicant_documents[$ctid->documentid];						
						$url = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$doc->document;
					?>
                <div class="panel panel-default" style="border-bottom: 1px solid #ddd;">
                  <div class="panel-heading" style="padding:7px 3px !important;" id="head<?PHP echo $ctid->documentid;?>">
                    <h4 class="panel-title" style="font-size:12px; font-weight:normal !important;">
                      <?PHP if($doc->appli_doc_id!='') { ?>
                      <span class="icons" id="removeicons<?PHP echo $doc->appli_doc_id; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> </span>
                      <?PHP } ?>
                      <a class="accordion-toggle collapsed fixpopup" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $ctid->documentid;?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                  </div>
                </div>
                <?PHP } ?>
              </div>
            </div>
          </div>
          <h4 class="panel-title customhr">نواقص الطلب </h4>
          <div class="col-md-12 form-group">
            <?php echo $ah_applicant->extra_detail; ?></strong> </div>
          <h4 class="panel-title customhr">افراد الأسرة: (بمن فيهم العاملين و غيرالعاملين) </h4>
          <div class="col-md-12 form-group">
            <table class="table table-bordered table-striped dataTable">
              <thead>
                <tr role="row">
                  <th class="right">الاسم</th>
                  <th class="center" style="width: 50px;">السن</th>
                  <th class="right">القرابة</th>
                  <th class="right">المهنة</th>
                  <th class="right" style="width: 70px;">الدخل الشهري</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?php 
			  
			  if($ah_applicant_relation)
			  {	
			  	foreach($ah_applicant_relation as $apr) { ?>
                <tr>
                  <td class="right"><?php echo $apr->relation_fullname; ?></td>
                  <td class="center"><?php echo check_all($apr->age); ?></td>
                  <td class="right"><?php echo check_all($this->haya_model->get_name_from_list($apr->relationtype)); ?></td>
                  <td class="right"><?php echo check_all($this->haya_model->get_name_from_list($apr->professionid)); ?></td>
                  <td class="right"><?php echo number_format($apr->monthly_income,3); ?></td>
                </tr>
                <?php }
			  }
			 ?>
              </tbody>
            </table>
          </div>
          <h4 class="panel-title customhr">الحالة الاقتصادية: </h4>
          <div class="col-md-12 form-group">
            <label class="text-warning">مصدر الدخل:</label>
            <?PHP echo $ah_applicant->income; ?></strong> </div>
            <div class="col-md-6 form-group">
            <label class="text-warning">قيمة الدخل:</label>
            <?PHP echo number_format($ah_applicant->salary,3); ?></strong> </div>
          <div class="col-md-6 form-group">
            <?PHP if($ah_applicant->source=='يوجد') { 
				echo('<label class="text-warning">يوجد</label> : يوجد مصدر دخل آخر</strong>'); 
			} elseif($ah_applicant->source=='لايوجد') { 
				echo('<label class="text-warning">لايوجد</label> : لايوجد مصدر دخل آخر</strong>'); 
			} ?>
          </div>
          <?PHP if($ah_applicant->source=='يوجد') { 
		  			$totalQyama = $ah_applicant->salary+$ah_applicant->qyama+$ah_applicant->qyama_2+$ah_applicant->qyama_3+$ah_applicant->qyama_4;
		  
		  ?>
          <div class="col-md-12 form-group">
            <table width="100%" class="table" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>&nbsp;</td>
            <td class="right"><label class="text-warning">مصدره</label></td>
            <td class="center"><label class="text-warning">قيمته</label></td>
          </tr>
          <tr>
            <td class="center">1.</td>
            <td class="right"><?PHP echo $ah_applicant->musadra; ?></td>
            <td class="center"><?PHP echo number_format($ah_applicant->qyama,3); ?></td>
          </tr>
          <tr>
            <td class="center">2.</td>
            <td class="right"><?PHP echo $ah_applicant->musadra_2; ?></td>
            <td class="center"><?PHP echo $ah_applicant->qyama_2; ?></td>
          </tr>
          <tr>
            <td class="center">3.</td>
            <td class="right"><?PHP echo $ah_applicant->musadra_3; ?></td>
            <td class="center"><?PHP echo $ah_applicant->qyama_3; ?></td>
          </tr>
          <tr>
            <td class="center">4.</td>
            <td class="right"><?PHP echo $ah_applicant->musadra_4; ?></td>
            <td class="center"><?PHP echo $ah_applicant->qyama_4; ?></td>
          </tr>
          <tr>
            <td class="center"></td>
            <td class="left"><label class="text-warning">مجموع الدخل:</label></td>
            <td class="center"><?PHP echo number_format($totalQyama,3); ?></td>
          </tr>
      </table> </div>
          <?PHP } ?>
      <?PHP if($ah_applicant->q_source=='يوجد') { ?>    
      <h4 class="panel-title customhr">القروض: (<?PHP echo $ah_applicant->q_source; ?>)</h4>
      <div class="col-md-12 form-group">
      <table class="table">
              <thead>
                <tr role="row">
                  <th class="right">نوع القرض</th>
                  <th class="center">قبمة القرض</th>
                  <th class="center">القسط الشهري</th>                
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?PHP 
			  
			  if($ah_applicants_loans)
			  {
				  	$apr_req = 0;
			  		foreach($ah_applicants_loans as $loans) 
					{	
						//$lmaount += $loans->loan_amount; // Before
						$loan_limit += $loans->loan_limit; // Now i changed
			  ?>
                <tr>
                  <td class="right"><?PHP echo $loans->loan_type;  ?></td>
                  <td class="center"><?PHP echo number_format($loans->loan_amount,3);  ?></td>
                  <td class="center"><?PHP echo number_format($loans->loan_limit,3);  ?></td>                 
                </tr>
                <?PHP $apr_req++; }
			  	}
				
				 	//echo 'total qaima = '.$totalQyama.'<br>';
				  	//echo 'total qaima = '.$lmaount;
			  
						//$totalSafi = $totalQyama-$lmaount; // Before
						$totalSafi = $totalQyama-$loan_limit; // Now i changed
				
				?>
                <tr>
                  <td class="right"></td>
                      <td class="center" style="background:#090;"><label class="panel-title"><strong>صافي الدخل</strong></label></td>
                      <td class="center" style="background:#090;"><?PHP echo number_format($totalSafi,3); ?></td>
                </tr>
              </tbody>
            </table>
      </div>
      <?PHP } ?>
         
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة السكنية:</label>
            <?PHP echo $ah_applicant->current_situation; ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning"><?PHP echo $ah_applicant->ownershiptype; ?></strong> </label>
            <?PHP echo number_format($ah_applicant->ownershiptype_amount,3); ?>
          </div>
          
           <div class="col-md-12 form-group">
           <h4 class="panel-title customhr">الملاحظات:</h4>
           <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
           <?php $all_notes	=	$this->inq->getNotes($ah_applicant->applicantid);?>
			<?PHP foreach($all_notes as $note) { ?>
            <tr>
                <td class="right" colspan="3"><?PHP echo a_date($note->notes); ?> <strong>(كتبت في <?PHP echo a_date($note->notesdate); ?>)</strong></td>      
                <td class="right"><?PHP echo $note->fullname; ?><br /><?PHP echo $note->list_name; ?></td>
            </tr>
            <?PHP } ?>
            </table>
           </div>
            
          
        </div>
        <div class="col-md-7 fox">
        <!--Bank Information Starts Here -->
          <?PHP if($ah_applicant->charity_type_id!=78) 
		  { ?>
          	<?php if($ah_applicant->charity_type_id	==81):?>
            <h4 class="panel-title customhr">نوع المساعدة الدراسية:</h4>
            <?php else:?>
            <h4 class="panel-title customhr">تفاصيل البنك:</h4>
            <?php endif;?>
          
          <?PHP if($ah_applicant->charity_type_id==81) { ?>
          <div class="col-md-6 form-group"><label class="text-warning">منحة دراسية:</label>&nbsp;&nbsp;&nbsp;<input type="radio" name="banksource" <?PHP if($ah_applicant->banksource=='منحة دراسية') { ?> checked <?PHP } ?> class="banksource" value="منحة دراسية"></div>
          <div class="col-md-6 form-group"><label class="text-warning">مساعدة مصروف جيب:</label>&nbsp;&nbsp;&nbsp;<input type="radio" name="banksource" <?PHP if($ah_applicant->banksource=='مساعدة مصروف جيب') { ?> checked <?PHP } ?> class="banksource" value="مساعدة مصروف جيب"></div>         
          <?PHP } ?>
          <div class="col-md-6 form-group hideBank" <?PHP if($ah_applicant->banksource=='مساعدة مصروف جيب') { ?> style="display:block;" <?PHP } ?>>
            <label class="text-warning">اسم البنك:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('bankid','bank',$ah_applicant->bankid,0,'req'); ?> </div>
          <div class="col-md-6 form-group hideBank" <?PHP if($ah_applicant->banksource=='مساعدة مصروف جيب') { ?> style="display:block;" <?PHP } ?>>
            <label class="text-warning">الفرع:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('bankbranchid','bank_branch',$ah_applicant->bankbranchid,$ah_applicant->bankid,'req'); ?> </div>
          <div class="col-md-6 form-group hideBank" <?PHP if($ah_applicant->banksource=='مساعدة مصروف جيب') { ?> style="display:block;" <?PHP } ?>>
            <label class="text-warning">رقم الحساب:</label>
            <input name="accountnumber" value="<?PHP echo $ah_applicant->accountnumber; ?>" placeholder="رقم الحساب" id="accountnumber" type="text" class="form-control req">
          </div>
          <div class="col-md-6 form-group hideBank" style="display:none;">
                    <label class="text-warning">Swift Code  :</label>
                    <input name="swiftcode"  placeholder="Swift Code" value="<?PHP echo $ah_applicant->swiftcode; ?>" id="swiftcode" type="text" class="form-control" >
                  </div>
          <div class="col-md-6 form-group hideBank" <?PHP if($ah_applicant->banksource=='مساعدة مصروف جيب') { ?> style="display:block;" <?PHP } ?>>
               <label class="text-warning">اسم صاحب الحساب بالانجليزي</label>
              <input type="text" class="form-control percentage-3 req" name="holder_name" id="holder_name" placeholder="اسم صاحب الحساب" value="<?php echo $ah_applicant->holder_name; ?>"/>
           </div><br clear="all">
          <div class="col-md-6 form-group hideBank" <?PHP if($ah_applicant->banksource=='مساعدة مصروف جيب') { ?> style="display:block;" <?PHP } ?>>
          <br clear="all">
            <label class="text-warning">نوع الحساب: جاري \ توفير: </label>
            <?PHP echo($ah_applicant->accounttype); ?> </div>
          <div class="col-md-12 form-group hideBank" <?PHP if($ah_applicant->banksource=='مساعدة مصروف جيب') { ?> style="display:block;" <?PHP } ?>>
            <label class="text-warning">يرفق صورة من كشف الحسابات (حديث الاصدار):</label>
            <?PHP
          	$statmentURL = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$ah_applicant->bankstatement;
			echo getFileResult($statmentURL,'يرفق صورة من كشف الحسابات (حديث الاصدار)',$statmentURL);
		  ?>
          </div>
          <br clear="all">
          <?PHP } 
		  ?>
          <!--Bank Information Ends Here -->
           
            <h4 class="panel-title customhr">مكونات المنزل:</h4>
         
          <div class="col-md-4 form-group">
            <label class="text-warning">نوع البناء:</label>
            <?PHP echo check_all($this->haya_model->get_name_from_list($ah_applicant->building_type)); ?></strong> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">عدد الغرف:</label>
            <?PHP echo check_all($ah_applicant->number_of_rooms); ?></strong> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">المرافق:</label>
            <?PHP echo check_all($ah_applicant->utilities); ?></strong> </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">حالة الأثاث والموجودات:</label>
            <?PHP echo check_all($ah_applicant->furniture); ?> </div>
            
          
         <?PHP if($ah_applicant->charity_type_id==81) { ?>
         <h4 class="panel-title customhr">البيانات الدراسية:</h4>
          
          <div class="col-md-4 form-group">
            <label class="text-warning">شهادة ثنوية:</label>
            <?PHP echo check_all($ah_applicant->certificate_dualism); ?></strong> </div>
           <div class="col-md-4 form-group">
            <label class="text-warning">سنة التخرج:</label>
            <?PHP echo check_all($ah_applicant->graduation_year); ?></strong> </div>
            <div class="col-md-4 form-group">
            <label class="text-warning">النسبة :</label>
            <?PHP echo check_all($ah_applicant->school_percentage); ?></strong> </div>
          <br clear="all">
            <h4 class="panel-title customhr">البيانات الجامعية :</h4>
         
          <div class="col-md-4 form-group">
            <label class="text-warning">اسم الكلية أو الجامعة:</label>
            <?PHP echo check_all($ah_applicant->college_name); ?></strong> </div>
           <div class="col-md-4 form-group">
            <label class="text-warning">التخصص :</label>
            <?PHP echo check_all($ah_applicant->specialization); ?></strong> </div>
            <div class="col-md-4 form-group">
            <label class="text-warning">السنة لدراسية :</label>
            <?PHP echo check_all($ah_applicant->year_of_study); ?></strong> </div>
            <div class="col-md-4 form-group">
            <label class="text-warning">اخر معدل تراكمي :</label>
            <?PHP echo check_all($ah_applicant->last_grade_average); ?></strong> </div>
          <br clear="all">
        <?PHP } ?>  
          <h4 class="panel-title customhr">بعد إجراء البحث الاجتماعي والاطلاع على المستندات المؤيدة اتضح الآتي:-</h4>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة الصحية:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('health_condition','health_condition',$ah_applicant_survayresult->health_condition,0,'req'); ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة السكنية:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('housing_condition','housing_condition',$ah_applicant_survayresult->housing_condition,0,'req'); ?> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">عدد أفراد الأسرة:</label>
            <?PHP number_drop_box('numberofpeople',$ah_applicant_survayresult->numberofpeople,'عدد أفراد الأسرة'); ?>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">ترتيب مقدم الطلب بالأسرة:</label>
            <?PHP number_drop_box('positioninfamily',$ah_applicant_survayresult->positioninfamily,'ترتيب مقدم الطلب بالأسرة'); ?>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الحالة الاقتصادية:</label>
            <?php echo $this->haya_model->create_dropbox_list('economic_condition','economic_condition',$ah_applicant_survayresult->economic_condition,0,'req'); ?> </div>
          
          <!-- ---------------------------------------------- -->
          			<!-- Start New Modifications -->
          <!-- ---------------------------------------------- -->
          	<?php if($ah_applicant->charity_type_id	==	'78'){?>
          <div style="display:none;">
          	<div class="col-md-12 form-group">
          <h4 class="panel-title customhr">حالة البيت :-</h4>
            <label class="text-warning">نوع الصيانة:</label>
            <?php echo $this->haya_model->create_dropbox_list('maintenance_type','maintenance_type',$ah_applicant_survayresult->maintenance_type,0,''); ?>
          </select>
          </div>
          	<div class="col-md-12 form-group" id="show-date" style="display:none;">
            <label class="text-warning">تاريخ :</label>
            <input name="certificate_date" value="<?php echo $ah_applicant_survayresult->certificate_date; ?>" placeholder="تاريخ" id="certificate_date" type="text" class="form-control dp" /></div>
          	<div class="col-md-6 form-group">
            <label class="text-warning">تقرير الزيارة:</label>
            <textarea name="report_visit" id="report_visit" style="height:150px; resize:none;" placeholder="رأي الباحث الاجتماعي و مبرراته"  class="form-control"><?PHP echo $ah_applicant_survayresult->review; ?></textarea>
          </div>
          	<div class="col-md-6 form-group">
            <label class="text-warning">اقتراح القسم:</label>
            <textarea name="proposal_section" id="proposal_section" style="height:150px; resize:none;" placeholder="رأي الباحث الاجتماعي و مبرراته"  class="form-control"><?PHP echo $ah_applicant_survayresult->review; ?></textarea>
          </div>
          	<div class="container demo-wrapper">
              <div class="row demo-columns">
                <div class="col-md-12"> 
                  <!-- D&D Zone-->
                  <div id="drag-and-drop-zone" class="uploader">
                    <div>سحب وإسقاط الملفات هنا</div>
                    <div class="or">-او-</div>
                    <div class="browser">
                      <label> <span>اضغط لاستعراض الملف</span>
                        <input type="file" name="muzaffar" />
                      </label>
                    </div>
                  </div>
                </div>
                <!-- / Left column -->
                
                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">تحميل</h3>
                    </div>
                    <div class="panel-body demo-panel-files" id='demo-files'> <span class="demo-note">لم يتم اختيار الملفات / دروبيد بعد ...</span> </div>
                  </div>
                </div>
                <!-- / Right column --> 
              </div>
            </div>
            </div>
            <?php }?>
          <!-- ---------------------------------------------- -->
          			<!-- End New Modifications -->
          <!-- ---------------------------------------------- -->
          <h4 class="panel-title customhr">نوع الحالة :-</h4>
          <div class="col-md-6 form-group">            
            <label class="text-warning">للحالات الضمانية:</label>            
            <input type="radio" name="casetype" data-id="zamanya" <?php if($ah_applicant_survayresult->casetype=='للحالات الضمانية') { ?> checked <?PHP } ?> class="casetypeinput" value="للحالات الضمانية">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <label class="text-warning">للحالات غير الضمانية:</label>
            
            <input type="radio" name="casetype" data-id="ghairzamanya" <?PHP if($ah_applicant_survayresult->casetype=='للحالات غير الضمانية') { ?> checked <?PHP } ?> class="casetypeinput" value="للحالات غير الضمانية">
          </div>
           <div class="col-md-6 form-group"> 
           <?php if($ah_applicant->charity_type_id	==	'81'){?>           
            <label class="text-warning">شهادة الثانوية العامة:</label>
            <input type="radio" name="education_level" <?php if($ah_applicant_survayresult->education_level=='S') { ?> checked <?PHP } ?> value="S">
            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
            <label class="text-warning">الدراسة الجامعية:</label>           
            <input type="radio" name="education_level" <?PHP if($ah_applicant_survayresult->education_level=='C') { ?> checked <?PHP } ?> value="C">
           <?PHP } ?> 
          </div>
          <div class="col-md-12 form-group casetype" id="zamanya" <?PHP if($ah_applicant_survayresult->casetype=='للحالات الضمانية') { ?> style="display:block !important;" <?PHP } ?>>
            <div class="col-md-12 form-group">
              <label class="text-warning">الحالة الضمانية باسم:</label>
              <input name="aps_name" value="<?PHP echo $ah_applicant_survayresult->aps_name; ?>" placeholder="الحالة الاقتصادية" id="aps_name" type="text" class="form-control">
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">رقم الحساب:</label>
              <input name="aps_account" value="<?PHP echo $ah_applicant_survayresult->aps_account; ?>" placeholder="الحالة الاقتصادية" id="aps_account" type="text" class="form-control NumberInput">
            </div>
            <div class="col-md-2 form-group">
              <label class="text-warning">رقم الملف:</label>
              <input name="aps_filename" value="<?PHP echo $ah_applicant_survayresult->aps_filename; ?>" placeholder="الحالة الاقتصادية" id="aps_filename" type="text" class="form-control  NumberInput">
            </div>
            <div class="col-md-2 form-group">
              <label class="text-warning">الفئة:</label>
              <?PHP echo $this->haya_model->create_dropbox_list('aps_category','aps_category',$ah_applicant_survayresult->aps_category,0,''); ?> </div>
            <div class="col-md-2 form-group">
              <label class="text-warning">قيمة المعاش:</label>
              <input name="aps_salaryamount" value="<?PHP echo $ah_applicant_survayresult->aps_salaryamount; ?>" placeholder="الحالة الاقتصادية" id="aps_salaryamount" type="text" class="form-control  NumberInput">
            </div>
            
            <div class="col-md-3 form-group">
              <label class="text-warning">تاريخ الربط:</label>
              <input name="aps_date" value="<?PHP echo $ah_applicant_survayresult->aps_date; ?>" placeholder="الحالة الاقتصادية" id="aps_date" type="text" class="form-control">
            </div>
            <!--<div class="col-md-12 form-group">
              <label class="text-warning">مصادر دخل أخرى:</label>
              <?PHP $json = json_decode($ah_applicant_survayresult->aps_another_income,TRUE);
			  //echo $ah_applicant_survayresult->aps_another_income;'<pre>'; print_r($json);
			  		$inx = 0;
			  		for($mdi=1; $mdi<=4; $mdi++) { ?>
              <input style="margin-bottom:2px;" name="aps_another_income[]" value="<?PHP echo $json[$inx]; ?>" placeholder="مصادر دخل أخرى <?PHP echo arabic_date($mdi); ?>." id="aps_another_income" type="text" class="form-control NumberInput">
              <?PHP $inx++; } ?>
            </div>-->
          </div>
          <div class="col-md-12 form-group casetype" id="ghairzamanya"  <?PHP if($ah_applicant_survayresult->casetype=='للحالات غير الضمانية') { ?> style="display:block !important;" <?PHP } ?>>
            <div class="col-md-12 form-group" style="padding:0px !important;">
              <label class="text-warning">مصادر دخل أخرى:</label><br>
              <?php 
			  		$inxp = 0;
			  		for($mdi=1; $mdi<=4; $mdi++) {
					?>
              <div class="col-md-3 form-group"  style="padding:3px !important;"><input style="margin-bottom:2px;" name="aps_another_income[]" value="<?PHP echo $json[$inxp]; ?>" placeholder="مصادر دخل أخرى <?PHP echo arabic_date($mdi); ?>." id="aps_another_income" type="text" class="form-control"></div>
              <?PHP $inxp++; } ?>
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">اجمالي الدخل الشهري:</label>
              <input name="aps_month" value="<?PHP echo $ah_applicant_survayresult->aps_month; ?>" placeholder="اجمالي الدخل الشهري للفرد" id="aps_month" type="text" class="form-control  NumberInput">
            </div>
            <div class="col-md-6 form-group">
              <label class="text-warning">صافي الدخل الشهري للاسرة:</label>
              <input name="aps_year" value="<?PHP echo $ah_applicant_survayresult->aps_year; ?>" placeholder="اجمالي الدخل الشهري للاسرة" id="aps_year" type="text" class="form-control NumberInput">
            </div>
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">الغرض من المساعدة:</label>
            <input name="whyyouwant" value="<?PHP echo $ah_applicant_survayresult->whyyouwant; ?>" placeholder="الغرض من المساعدة" id="whyyouwant" type="text" class="form-control">
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">ملخص الحالة:</label>
            <textarea name="summary" style="height:100px; resize:none;" id="summary" placeholder="ملخص الحالة"  class="form-control req"><?PHP echo $ah_applicant_survayresult->summary; ?></textarea>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رأي الباحث الاجتماعي و مبرراته:</label>
            <textarea name="review" id="review" style="height:100px; resize:none;" placeholder="رأي الباحث الاجتماعي و مبرراته"  class="form-control req"><?PHP echo $ah_applicant_survayresult->review; ?></textarea>
          </div>
          <div class="col-md-6 form-group">
              <label class="text-warning">تاريخ تقديم طلب المساعدة :</label>
              <input name="first_time_date" value="<?PHP echo $ah_applicant_survayresult->first_time_date; ?>" placeholder="تاريخ تقديم طلب المساعدة" id="first_time_date" type="text" class="form-control datepicker">
            </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">عاجل:</label>
            <input  name="ajel" type="checkbox" value="Yes">
          </div>
          <div class="col-md-12 form-group"> <?PHP echo $this->haya_model->module_button(140,$ah_applicant->applicantid,'save_socilservayresult',$ah_applicant->case_close); ?> </div>
        	<br clear="all">
        </div>
      </div>
    </div>
    
  </form>
  </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script src="<?PHP echo base_url(); ?>assets/js/tasgeel_js.js"></script>

    <script type="text/javascript">
	$(document).ready(function (){
		
	var charity_type_id = '<?PHP echo $ah_applicant->charity_type_id; ?>';
	if(charity_type_id==81)
	{	$('.hideBank').hide(); 
		$('#bankid').removeClass('req');
		$('#bankbranchid').removeClass('req');
		$('#accountnumber').removeClass('req');
		$('#holder_name').removeClass('req');
	}
	
	
	
	$('.dp').datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat:'yy-mm-dd',
		minDate: "-2M -28D", maxDate: 1
    });
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
	
	
	
	
 });
	  var	applicant_id	=	'<?php echo $ah_applicant->applicantid;?>';
	  
      $('#drag-and-drop-zone').dmUploader({
        url: '<?php echo base_url();?>inquiries/uploads_sakaniya_visits/'+applicant_id,
        dataType: 'json',
        allowedTypes: 'image/*',
        /*extFilter: 'jpg;png;gif',*/
        onInit: function(){
          $.danidemo.addLog('#demo-debug', 'default', 'Plugin initialized correctly');
        },
        onBeforeUpload: function(id){
          $.danidemo.addLog('#demo-debug', 'default', 'Starting the upload of #' + id);

          $.danidemo.updateFileStatus(id, 'default', 'Uploading...');
        },
        onNewFile: function(id, file){
          $.danidemo.addFile('#demo-files', id, file);
        },
        onComplete: function(){
			
          $.danidemo.addLog('#demo-debug', 'default', 'All pending tranfers completed');
        },
        onUploadProgress: function(id, percent){
          var percentStr = percent + '%';

          $.danidemo.updateFileProgress(id, percentStr);
        },
        onUploadSuccess: function(id, data){
          $.danidemo.addLog('#demo-debug', 'success', 'Upload of file #' + id + ' completed');

          $.danidemo.addLog('#demo-debug', 'info', 'Server Response for file #' + id + ': ' + JSON.stringify(data));

          $.danidemo.updateFileStatus(id, 'success', 'Upload Complete');

          $.danidemo.updateFileProgress(id, '100%');
        },
        onUploadError: function(id, message){
          $.danidemo.updateFileStatus(id, 'error', message);

          $.danidemo.addLog('#demo-debug', 'error', 'Failed to Upload file #' + id + ': ' + message);
        },
        onFileTypeError: function(file){
          $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: must be an image');
        },
        onFileSizeError: function(file){
          $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: size excess limit');
        },
        /*onFileExtError: function(file){
          $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' has a Not Allowed Extension');
        },*/
        onFallbackMode: function(message){
          $.danidemo.addLog('#demo-debug', 'info', 'Browser not supported(do something else here!): ' + message);
        }
      }); 
    </script>
</body>
</html>
<?PHP
	$main = $m;
	
	if($type == 'register')
	{
		$applicant = $main['applicants'];
		$applicant_qualification = $main['applicant_qualification'][0];
		$applicant_project = $main['applicant_project'];
		$applicant_professional_experience = $main['applicant_professional_experience'];
		$applicant_phones = $main['applicant_phones'];
		$applicant_partners = $main['applicant_partners'];
		$applicant_numbers = $main['applicant_numbers'];
		$applicant_loans = $main['applicant_loans'];
		$applicant_document = $main['applicant_document'];
		$applicant_businessrecord = $main['applicant_businessrecord'];	
		$step['s'] = 1;
		$step['temp'] = $applicant->applicant_id;	
	}
	else
	{
		$applicant = $main['main']->applicant[0];
		$applicant_qualification = $main['main']->qualification;
		$applicant_project = $main['applicant_project'];
		$applicant_professional_experience = $main['applicant_professional_experience'];
		$applicant_phones = $main['applicant_phones'];
		$applicant_partners = $main['applicant_partners'];
		$applicant_numbers = $main['applicant_numbers'];
		$applicant_loans = $main['applicant_loans'];
		$applicant_document = $main['applicant_document'];
		$applicant_businessrecord = $main['applicant_businessrecord'];	
		$step['s'] = 1;
		$step['temp'] = $applicant->applicant_id;	
	}
	$applicant_qualification = $main['applicant_qualification'][0];
	$applicant_project = $main['applicant_project'];
	$applicant_professional_experience = $main['applicant_professional_experience'];
	$applicant_phones = $main['applicant_phones'];
	$applicant_partners = $main['applicant_partners'];
	$applicant_numbers = $main['applicant_numbers'];
	$applicant_loans = $main['applicant_loans'];
	$applicant_document = $main['applicant_document'];
	$applicant_businessrecord = $main['applicant_businessrecord'];	
	$step['s'] = 1;
	$step['temp'] = $applicant->applicant_id;
	
	$index = 1;
	$ind  = $mustarik.''.$index;
	$hiddenInd  =$mustarik.''.$index;
	$hiddenIndex = 1;
	$uploaderArray = array(
	'icon-upload-alt pull-left'=>'بطاقة سجل القوى العاملة',
	'icon-user pull-left'=>'الرقم المدني',
	'icon-certificate pull-left'=>'شهادة عدم محكومية',
	'icon-briefcase pull-left'=>'دراسة الجدوى الإقتصادية للمشروع',
	'icon-camera-retro pull-left'=>'صورة شمسية',
	'icon-wrench pull-left'=>'شهادات الخبرة / التدريب',
	'icon-umbrella pull-left'=>'بطاقة الضمان الاجتماعي');
	
	
?>

<div class="row">
  
  <div class="col-md-8" style="  margin-top:10px !important;">
    <div class="col-md-4 form-group">
      <label class="text-warning">الاسم الأول :</label>
      <input name="applicant_first_name" value="<?PHP  if(isset($type) && $type != "inquiry") { echo $applicant->applicant_first_name; } else { if(isset($type) && $type == "inquiry") { echo $applicant->first_name; } } ?>" placeholder="الاسم الأول" id="applicant_first_name" type="text" class="form-control req">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">الاسم الثاني :</label>
      <input name="applicant_middle_name" value="<?PHP if(isset($type) && $type != "inquiry") { echo $applicant->applicant_middle_name; } else{ if(isset($type) && $type == "inquiry") { echo  $applicant->middle_name; } } ?>" placeholder="الاسم الثاني" id="applicant_middle_name" type="text" class="form-control req">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">الاسم الثالث :</label>
      <input name="applicant_last_name" value="<?PHP  if(isset($type) && $type != "inquiry") { echo $applicant->applicant_last_name; } else{ if(isset($type) && $type == "inquiry") { echo  $applicant->last_name; } } ?>" placeholder="الاسم الثالث" id="applicant_last_name" type="text" class="form-control req">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">القبيلة / العائلة :</label>
      <input name="applicant_sur_name" value="<?PHP  if(isset($type) && $type != "inquiry") { echo $applicant->applicant_sur_name; } else{ if(isset($type) && $type == "inquiry") { echo  $applicant->family_name; } }  ?>" placeholder="القبيلة / العائلة" id="applicant_sur_name" type="text" class="form-control req">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">النوع :</label>
      <input type="radio" <?PHP if(isset($type) && $type != "inquiry") { if($applicant->applicant_gender=='ذكر') { ?> checked="checked" <?PHP } } else{ if(isset($type) && $type == "inquiry") { if($applicant->applicanttype=='ذكر') { ?> checked="checked" <?PHP } } }  ?> class="req" name="applicant_gender" value="ذكر" id="applicant_gender"/>
      ذكر
      <input type="radio" <?PHP if(isset($type) && $type != "inquiry") { if($applicant->applicant_gender=='أنثى') { ?> checked="checked" <?PHP } } else{ if(isset($type) && $type == "inquiry") { if($applicant->applicanttype=='أنثى') { ?> checked="checked" <?PHP } } } ?> class="req" name="applicant_gender" value="أنثى" id="applicant_gender"/>
      أنثى </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">رقم الرقم المدني :</label>
      <input name="appliant_id_number"  value="<?PHP  if(isset($type) && $type != "inquiry") { echo $applicant->appliant_id_number; } else{ if(isset($type) && $type == "inquiry") { echo  $applicant->idcard; } }  ?>" id="appliant_id_number" placeholder="رقم الرقم المدني" type="text" class="form-control NumberInput appliant_id_number">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">رقم سجل القوى العاملة :</label>
      <input name="applicant_cr_number" id="applicant_cr_number" value="<?PHP if(isset($type) && $type != "inquiry") { echo $applicant->applicant_cr_number; } else { echo $main['main']->mr_number; }  ?>" placeholder="رقم سجل القوى العاملة" type="text" class="form-control NumberInput">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">تاريخ الميلاد :</label>
      <input data-durar="age_1" name="applicant_date_birth" type="text"  value="<?PHP echo date('Y-m-d',strtotime($applicant->applicant_date_birth)); ?>" class="form-control age_datepicker req" id="applicant_date_birth" placeholder="تاريخ الميلاد" size="15" maxlength="10">
    </div>
    <div class="col-md-1 form-group">
      <label class="text-warning">&nbsp;</label>
      <input name="age_1" type="text" class="form-control NumberInput" id="age_1" value="<?PHP if($applicant->applicant_date_birth) { echo calcualteAge($applicant->applicant_date_birth); } ?>" placeholder="العمر" readonly>
    </div>
    <div class="col-md-11 form-group" style="  width: 100% !important;">
      <label class="text-warning">رقم الهاتف :</label>
      <br>
      <?PHP 
	  
	  for($p=0; $p<=3; $p++) { 
				echo(' <div class="form-group col-md-3" style="padding-right: 2px !important;   padding-left: 2px !important;">');
				if(isset($type) && $type != "inquiry") { ?>
      <input name="phone_numbers[]" value="<?PHP echo $applicant_phones[$p]->applicant_phone; ?>"  type="text" class="form-control applicantphone checkphonenumber phoneautocomplete NumberInput <?PHP if($p==0) { ?>req<?PHP } ?>" id="phonenumber" placeholder="رقم الهاتف" maxlength="8">
      <?PHP	} else { ?>
      <input name="phone_numbers[]" value="<?PHP echo $applicant_phones[$p]->applicant_phone; ?>"  type="text" class="form-control applicantphone checkphonenumber phoneautocomplete NumberInput <?PHP if($p==0) { ?>req<?PHP } ?>" id="phonenumber" placeholder="رقم الهاتف" maxlength="8">
      <?PHP	} echo('</div>');
				} ?>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الحالة الاجتماعية :</label>
      <?PHP hd_dropbox('applicant_marital_status',$applicant->applicant_marital_status,'اختر الحالة الاجتماعية','maritalstatus','req form-control',$applicant->applicant_marital_status_text,'كم عدد الأطفال لديك','applicant_marital_status_text'); ?>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الوضع الحالي :</label>
      <?PHP hd_dropbox('applicant_job_staus',$applicant->applicant_job_staus,'اختر الوضع الحالي','current_situation','req form-control',$applicant->applicant_job_staus_text,'','applicant_job_staus_text'); ?>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">فئة الضمان الإجتماعي :</label>
      <br>
      <input data-open="option_txt_id" id="option1" onClick="openOptions(this);" type="radio" <?PHP if($applicant->option1=='Y') { ?>checked="checked"<?PHP } ?> name="option1" value="Y" />
      نعم
      <input data-open="option_txt_id" id="option1" onClick="openOptions(this);" <?PHP if($applicant->option1=='N') { ?>checked="checked"<?PHP } ?> type="radio" name="option1" value="N" />
      لا
      <div class="row" id="option_txt_id" style=" <?PHP if($applicant->option1!='Y') { ?>display:none;<?PHP } ?>">
        <div class="col-md-11">
          <input type="text" class="form-control " value="<?PHP echo $applicant->option_txt; ?>" name="option_txt" id="option_txt" placeholder="رقم بطاقة الضمان الاجتماعي">
        </div>
      </div>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">فئة الاحتياجات الخاصة:</label>
      <br>
      <input  data-open="disable" id="option2" <?PHP if($applicant->option2=='Y') { ?>checked="checked"<?PHP } ?> type="radio" name="option2" value="Y"  onClick="openOptions(this);" />
      نعم
      <input  data-open="disable" id="option2" <?PHP if($applicant->option2=='N') { ?>checked="checked"<?PHP } ?> type="radio" name="option2" value="N"  onClick="openOptions(this);"/>
      لا
      <div class="row" style=" <?PHP if($applicant->option2!='Y') { ?>display:none;<?PHP } ?>" id="disable">
        <div class="col-md-11">
          <?PHP hd_dropbox('disable_type',$applicant->disable_type,'اختر نوع الإعاقة','disable_type','form-control',$applicant->applicant_disable_type_text,'','applicant_disable_type_text');  ?>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="panel-group" id="demo-accordion" style="border-right: 1px solid #ddd;">
      <?PHP 
	  $applicant_doc_index = 1;
	  
	  foreach($uploaderArray as $tabkey => $tabvalue) 
	  { 
	  		$data_url = base_url().'upload_files/documents/'.$applicant_document[$applicant_doc_index];
			if($applicant_document[$applicant_doc_index]!='')
			{	$is_uploaded = 1;	}
			else
			{	$is_uploaded = 0;	}
						
	  ?>
      <div class="panel panel-default doc<?PHP echo $applicant_doc_index; ?>" style="border-bottom:1px solid #ddd; <?PHP if($tabvalue=='بطاقة الضمان الاجتماعي' && $applicant->option1=='Y') { ?>display:block;<?PHP } ?>">
        <div class="panel-heading" id="head<?PHP echo $applicant_doc_index;?>">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $applicant_doc_index; ?>"><?PHP echo $tabvalue; ?> <i class="<?PHP echo $tabkey; ?>"></i><?PHP if($applicant_document[$applicant_doc_index]!='') { echo('<i style="color:#00A603; float:left;" class="icon-ok-circle"></i>'); } ?></a> </h4>
        </div>
        <div id="demo-collapse<?PHP echo $applicant_doc_index; ?>" class="panel-collapse collapse" style="height: 0px; list-style:none; text-align:center;">
          <div class="panel-body multiple_uploader" data-index="<?PHP echo $applicant_doc_index; ?>" data-heading="<?PHP echo $tabvalue; ?>" id="drag<?php echo $applicant_doc_index; ?>">
            <div class="browser">
              <input type="hidden" data-id="<?PHP echo $applicant_doc_index; ?>" id="document_uploaded_<?PHP echo $applicant_doc_index;?>" placeholder="<?PHP echo $tabvalue; ?>" class="<?PHP if($tabvalue!='بطاقة الضمان الاجتماعي') { ?>dreq<?PHP } ?>" value="<?PHP echo $is_uploaded; ?>">
              <input type="hidden" name="document_name_<?PHP echo $applicant_doc_index; ?>" id="document_name_<?PHP echo $applicant_doc_index; ?>" value="<?PHP if($applicant_document[$applicant_doc_index]!='') { echo($applicant_document[$applicant_doc_index]); } ?>">
              <input type="file" style="font-size: 11px;" name="document_id<?php echo $applicant_doc_index; ?>" id="document_id<?php echo $applicant_doc_index; ?>" title='<?PHP echo $tabvalue; ?>'>
            </div>
            <div class="data<?PHP echo $applicant_doc_index; ?>">
                
            	<?PHP if($applicant_document[$applicant_doc_index]!='') 
				{	echo getFileResult($applicant_document[$applicant_doc_index],$tabvalue);
					echo('<i class="delete-icon icon-remove-sign doc8remove0" style="color:#CC0000;cursor:pointer" onclick="deleteDoc(this,\''.$applicant_document[$applicant_doc_index].'\');"></i>'); 	} 
				
				?>
            </div>
          </div>
        </div>
      </div>
      <?PHP 
	  	$applicant_doc_index++;
	  } ?>
     
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12" style="margin-top:10px !important;">
    <h4 class="section-title preface-title text-warning" style="padding-right: 15px !important;">العنوان الشخصي</h4>
    <div class="col-md-3 form-group">
      <label class="text-warning">المحافظة:</label>
      <?PHP inq_reigons('','province',$applicant->province,' req ',$evo,$applicant->applicantid); ?>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning"> الولاية:</label>
      <?PHP inq_wilayats('walaya',$applicant->walaya,$applicant->province,' req ',$applicant->applicantid,$applicant->applicantid); ?>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">القرية:</label>
      <input type="text" value="<?PHP echo $applicant->village; ?>" class="form-control" name="village" placeholder="القرية">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">السكة:</label>
      <input type="text" value="<?PHP echo $applicant->way; ?>" class="form-control" name="way" placeholder="السكة">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">المنزل/المبني:</label>
      <input type="text" value="<?PHP echo $applicant->home; ?>" class="form-control" name="home" placeholder="المنزل/المبني">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الشقة:</label>
      <input type="text" value="<?PHP echo $applicant->deparment; ?>" class="form-control" name="deparment" placeholder="الشقة">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">ص.ب:</label>
      <input type="text" value="<?PHP echo $applicant->zipcode; ?>" class="form-control NumberInput" name="zipcode" placeholder="ص.ب">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">ر.ب:</label>
      <input type="text" value="<?PHP echo $applicant->postalcode; ?>" class="form-control NumberInput" name="postalcode" placeholder="ر.ب">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الهاتف النقال:</label>
      <input type="text" value="<?PHP echo $applicant->mobile_number; ?>" class="form-control NumberInput" name="mobile_number" placeholder="الهاتف النقال">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الهاتف الثابت:</label>
      <input type="text" value="<?PHP echo $applicant->linephone; ?>" class="form-control NumberInput" name="linephone" placeholder="الهاتف الثابت">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">الفاكس:</label>
      <input type="text" value="<?PHP echo $applicant->fax; ?>" class="form-control NumberInput" name="fax" placeholder="الفاكس">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">البريد الإلكتروني:</label>
      <input type="text" value="<?PHP echo $applicant->email; ?>" class="form-control" name="email" placeholder="البريد الإلكتروني">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">هاتف نقال أحد الأقارب:</label>
      <input type="text" value="<?PHP echo $applicant->refrence_number; ?>" class="form-control NumberInput" name="refrence_number" placeholder="هاتف نقال أحد الأقارب">
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12" style="margin-top:10px !important;">
    <div class="panel-group" id="demo-accordion">
      <div class="panel panel-default" style="border-bottom: 1px solid #ddd;">
        <div class="panel-heading">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseOne"><i class="icon-chevron-sign-left "></i> المؤهلات </a> </h4>
        </div>
        <div id="demo-collapseOne" class="panel-collapse collapse" style="height: auto;">
          <div class="panel-body">
            <h4 class="myheading" style="padding-right: 15px !important; margin-top: 15px;">١ / المستوى الدراسي</h4>
            <div class="col-md-3 form-group">
              <label class="text-warning">المؤهل:</label>
              <?PHP hd_dropbox('applicant_qualification',$applicant->applicant_qualification,'اختر المؤهل','qualification','form-control',$applicant->applicant_qualification_text,'','applicant_qualification_text'); ?>
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">التخصص:</label>
              <input name="applicant_specialization" type="text"  value="<?PHP echo $applicant->applicant_specialization; ?>" class="form-control " id="applicant_specialization" placeholder="التخصص">
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">الجهة:</label>
              <?PHP hd_dropbox('applicant_institute',$applicant->applicant_institute,'اختر الجهة','institute','form-control',$applicant->applicant_institute_text,'الجهة','applicant_institute_text'); ?>
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">المؤهل:</label>
              <input name="applicant_institute_year" id="applicant_institute_year" value="<?PHP echo $applicant->applicant_institute_year; ?>" placeholder="سنة التخرج" type="text" class="form-control NumberInput">
            </div>
            <h4 class="myheading" style="padding-right: 15px !important;">٢ / التدريب المهني</h4>
            <div class="col-md-3 form-group">
              <label class="text-warning">مركز التدريب:</label>
              <input name="applicant_trainningcenter" type="text"  value="<?PHP echo $applicant->applicant_trainningcenter; ?>" class="form-control " id="applicant_trainningcenter" placeholder="مركز التدريب">
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">التخصص:</label>
              <input name="applicant_specializations" type="text"  value="<?PHP echo $applicant->applicant_specializations; ?>" class="form-control " id="applicant_specializations" placeholder="التخصص">
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">مدة التدريب (بالأشهر):</label>
              <input name="applicant_training_month" type="text"  value="<?PHP echo $applicant->applicant_training_month; ?>" class="form-control NumberInput" id="applicant_training_month" placeholder="مدة التدريب (بالأشهر)">
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">شهادة التدريب المهني المتحصل عليها:</label>
              <input name="applicant_vtco" type="text"  value="<?PHP echo $applicant->applicant_vtco; ?>" class="form-control " id="applicant_vtco" placeholder="شهادة التدريب المهني المتحصل عليها">
            </div>
            <div class="col-md-3 form-group">
              <label class="text-warning">سنة الحصول على الشهادة:</label>
              <input name="applicant_ytotc" type="text"  value="<?PHP echo $applicant->applicant_ytotc; ?>" class="form-control " id="applicant_ytotc" placeholder="سنة الحصول على الشهادة">
            </div>
            <div class="col-md-9 form-group">
              <label class="text-warning">دورات تدريبية ميدانية أخرى (اختصاص التدريب, جهة التدريب, مدة التدريب (بالأشهر):</label>
              <textarea style="resize:none !important;" name="applicant_other_trainning" id="applicant_other_trainning" class="form-control" placeholder="دورات تدريبية ميدانية أخرى (اختصاص التدريب, جهة التدريب, مدة التدريب (بالأشهر) )"><?PHP echo $applicant->applicant_other_trainning; ?></textarea>
            </div>
            <div class="col-md-12 form-group" style="position:static !important;">
              <label class="text-warning">دورات التدريب المتخصصة قبل إقامة المشروع: (تنمية المبادرة-إدارة المؤسسات-مجالات فنية أخرى):</label>
              <textarea style="resize:none !important;" name="applicant_other_specializations" id="applicant_other_specializations" class="form-control" placeholder="دورات التدريب المتخصصة قبل إقامة المشروع: (تنمية المبادرة-إدارة المؤسسات-مجالات فنية أخرى)"><?PHP echo $applicant->applicant_other_specializations; ?></textarea>
            </div>
            <br clear="all">
          </div>
        </div>
      </div>
      <div class="panel panel-default"  style="border-bottom: 1px solid #ddd;">
        <div class="panel-heading">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseTwo"><i class="icon-chevron-sign-left"></i> الخبرة المهنية </a> </h4>
        </div>
        <div id="demo-collapseTwo" class="panel-collapse collapse" style="height: 0px;">
          <div class="panel-body">
            <h4 class="myheading" style="padding-right: 15px !important; margin-top: 15px;">الخبرة في نفس نشاط المشروع</h4>
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr>
                <th class="ex_th">تاريخ بداية المشروع</th>
                <th class="ex_th">اسم الجهة/المؤسسة/ المشروع الخاص</th>
                <th class="ex_th">نشاط الجهة/المؤسسة/ المشروع الخاص</th>
                <th class="ex_th">المهنة المزاولة بالجهة/المؤسسة/ المشروع الخاص</th>
                <th class="ex_th">عدد سنوات الخبرة</th>
              </tr>
              <?php for($i=0; $i<=2; $i++) 
			  { $xp = $applicant_professional_experience[$i];  ?>
              <input name="experienceid[]" type="hidden"  value="<?PHP echo $xp->experienceid; ?>" class="form-control xx dateinput" id="experienceid<?PHP echo $i; ?>" placeholder="">
              <tr>
                <td><input name="option_one[]" type="text"  value="<?PHP echo $xp->option_one; ?>" class="form-control xx dateinput" id="option_one<?PHP echo $i; ?>" placeholder="تاريخ"></td>
                <td><input name="option_two[]" type="text"  value="<?PHP echo $xp->option_two; ?>" class="form-control xx" id="option_two" placeholder="اسم الجهة"></td>
                <td><input name="option_three[]" type="text"  value="<?PHP echo $xp->option_three; ?>" class="form-control xx " id="option_three" placeholder="نشاط الجهة"></td>
                <td><input name="option_four[]" type="text"  value="<?PHP echo $xp->option_four; ?>" class="form-control xx" id="option_four" placeholder="المهنة المزاولة بالجهة"></td>
                <td><input name="option_five[]" type="text"  value="<?PHP echo $xp->option_five; ?>" class="form-control xx" id="option_five" placeholder="عدد سنوات الخبرة"></td>
              </tr>
              <?PHP } ?>
            </table>
            <h4 class="myheading" style="padding-right: 15px !important; margin-top: 15px;">الخبرة في أنشطة أخرى</h4>
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr>
                <th class="ex_th">تاريخ بداية المشروع</th>
                <th class="ex_th">اسم الجهة/المؤسسة/ المشروع الخاص</th>
                <th class="ex_th">نشاط الجهة/المؤسسة/ المشروع الخاص</th>
                <th class="ex_th">المهنة المزاولة بالجهة/المؤسسة/ المشروع الخاص</th>
                <th class="ex_th">عدد سنوات الخبرة</th>
              </tr>
              <?php for($j=0; $j<=2; $j++) 
						  { 
						  	$pq = $applicant_professional_experience[$j];
					?>
              <tr>
                <td><input name="activities_one[]" type="text"  value="<?PHP echo $pq->activities_one; ?>" class="form-control xx  dateinput" id="activities_one<?php echo $j; ?>" placeholder="تاريخ"></td>
                <td><input name="activities_two[]" type="text"  value="<?PHP echo $pq->activities_two; ?>" class="form-control xx" id="activities_two" placeholder="اسم الجهة"></td>
                <td><input name="activities_three[]" type="text"  value="<?PHP echo $pq->activities_three; ?>" class="form-control xx" id="activities_three" placeholder="نشاط الجهة"></td>
                <td><input name="activities_four[]" type="text"  value="<?PHP echo $pq->activities_four; ?>" class="form-control xx" id="activities_four" placeholder="المهنة المزاولة بالجهة"></td>
                <td><input name="activities_five[]" type="text"  value="<?PHP echo $pq->activities_five; ?>" class="form-control xx" id="activities_five" placeholder="عدد سنوات الخبرة"></td>
              </tr>
              <?PHP } ?>
            </table>
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title"> <a class="accordion-toggle collapsed " data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapseThree"><i class="icon-chevron-sign-left"></i> السجلات التجارية الأخرى </a> </h4>
        </div>
        <div id="demo-collapseThree" class="panel-collapse collapse" style="height: 0px;">
          <div class="panel-body">
            <input value="مالك" type="checkbox" name="applicant_activity" />
            مالك
            <input value="شريك" type="checkbox" name="applicant_activity" />
            شريك
            <input value="مفوض بالتوقيع" type="checkbox" name="applicant_activity" />
            مفوض بالتوقيع
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr>
                <th class="ex_th">اسم السجل</th>
                <th class="ex_th">رقم السجل</th>
                <th class="ex_th">عدد القوى العاملة الوطنية</th>
                <th class="ex_th">عدد القوى العاملة الوافدة</th>
              </tr>
              <?php 
			for($i=0; $i<=2; $i++)
			{	$act = $apb[$i];	?>
              <tr>
                <input name="bid[]" type="hidden"  value="<?PHP echo $act->bid; ?>" class="form-control " id="bid" placeholder="اسم الجهة">
                <td><input name="activity_name[]" type="text"  value="<?PHP echo $act->activity_name; ?>" class="form-control " id="activity_name" placeholder="اسم الجهة"></td>
                <td><input name="activity_registration_no[]" type="text"  value="<?PHP echo $act->activity_registration_no; ?>" class="form-control " id="activity_registration_no" placeholder="نشاط الجهة"></td>
                <td><input name="activity_nationalmanpower[]" type="text"  value="<?PHP echo $act->activity_nationalmanpower; ?>" class="form-control " id="activity_nationalmanpower" placeholder="المهنة المزاولة بالجهة"></td>
                <td><input name="activity_laborforce[]" type="text"  value="<?PHP echo $act->activity_laborforce; ?>" class="form-control " id="activity_laborforce" placeholder="عدد سنوات الخبرة"></td>
              </tr>
              <?PHP } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
    
    <!----------------------------------------------------------------------------------> 
    
  </div>
</div>

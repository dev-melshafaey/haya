<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <?php //$this->load->view('common/floatingmenu');
  
  
  	$applicant = $applicant_data['applicants'];
	$project = $applicant_data['applicant_project'][0];
	$loan = $applicant_data['applicant_loans'][0];
	$phones = $applicant_data['applicant_phones'];
	$comitte = $applicant_data['comitte_decision'][0];
	$evolution['applicants'] = $applicant; 
	$evolution['loan'] = $loan; 
	
	//echo "<pre>";
	//print_r($evolution);
	
	$fullname = $applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name;
	foreach($phones as $p)
	{	
		$ar[] = '986'.$p->applicant_phone;	
	}
		
	$applicantphone = implode('<br>',$ar);
	
  ?>
        <form id="validate_form_data" name="validate_form_data" method="post" action="<?PHP echo base_url().'inquiries/add_zyarat' ?>" autocomplete="off">
          <div class="col-md-12">
            <div class="panel panel-default panel-block">
              <div class="list-group">
                <div class="list-group-item" id="input-fields">                  
                  <div class="form-group col-md-6">
                    <label for="basic-input"><strong>الاسم الأول</strong> :</label>
                    <?php echo $fullname; ?>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="basic-input"><strong>النشاط</strong> :</label>
                   <?php echo $project->activity_project_text; ?>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="basic-input"><strong>مبلغ القرض</strong> :</label>
                    <?php echo arabic_date(number_format($loan->loan_amount,0)); ?>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="basic-input"><strong>الولاية</strong> :</label>
                    <?php echo show_data('Walaya',$applicant->walaya); ?>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="basic-input"><strong>رقم الهاتف</strong> :</label>
                    <?php echo $applicantphone; ?>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="basic-input"><strong>نوع البرنامج</strong> :</label>
                     <?php echo show_data('LoanLimit',$loan->loan_limit); ?>
                  </div>
                  <div id="details">  
              
                <div class="form_raw unmusanif" <?PHP if($sad->is_musanif=='مصنف') { ?> style="display:block;"<?PHP } else { ?>style="display:none;"<?PHP } ?>  >
               
                <input class="addpartnerphone" id="addnewmusanif" value="إضافة" type="button" style="float:left; cursor:pointer;" onclick="addMusanif()">
                <?php
/*					$financeData = json_decode($sad->financing);
					$loan_amount = json_decode($sad->loan_amount);
					$amount_paid = json_decode($sad->amount_paid);
					$residual = json_decode($sad->residual);
					$monthly_installment = json_decode($sad->monthly_installment);
					$project_difficulties = json_decode($sad->project_difficulties);
					$amount_problem = json_decode($sad->amount_problem);*/
					//amount_problem
					//project_difficulties
					// exit;
				?>
                
               </div>
				<div>
				<div class="form_raw musanif"  style="display:none;">
                  <div class="user_txt"> الطلبات الغير مكتملة  </div>
                  </div>
                </div>
				<!--<input type="button" onclick="addMuwafiq()" style="float:left; cursor:pointer;" value="إضافة" id="addnewmusanif" class="addpartnerphone">-->
                <!--<div class="form_raw musanif" >
                <div id="TextBoxesGroup"></div>
                  <div class="user_txt"></div>
                  <div class="form-group">
                  		 مكتمل<input type="radio" <?PHP if(rQuote($complete_data)=='1') { ?> checked="checked"<?PHP } ?>  name="is_complete"  class="sForm" value="1" onclick="checkComplete(this.value)"/>
                     غيرمكتمل<input type="radio" <?PHP if(rQuote($complete_data)=='0') { ?> checked="checked"<?PHP } ?>  name="is_complete" class="sForm"  value="0"  onclick="checkComplete(this.value)"/>
                  </div>
                </div>-->
                
                <div class="form-group col-md-6">
                <div id="TextBoxesGroup"></div>
                    <label for="basic-input"></label>
                         		 مكتمل<input type="radio" <?PHP if(rQuote($complete_data)=='1') { ?> checked="checked"<?PHP } ?>  name="is_complete"  class="sForm" value="1"/>
                     غيرمكتمل<input type="radio" <?PHP if(rQuote($complete_data)=='0') { ?> checked="checked"<?PHP } ?>  name="is_complete" class="sForm"  value="0" />
                  </div>
				</div>
                  
                  <div class="form-group">
                    <?php if($applicant_id):?>
                	      <input type="hidden" name="form_step" id="form_step" value="5" />      
      					  <input type="hidden" name="applicant_id" id="applicant_id" value="<?php echo $applicant_id;?>" />
                    <?php endif;?>
                <button type="button" id="save_data_form_five" class="btn btn-success btn-lrg">حفظ</button>
                <button style="display:none;" type="button" id="restart_data" class="btn default">إلغاء</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<?php $this->load->view('common/footer'); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
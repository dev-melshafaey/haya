<div class="row">
  <?PHP $smx = $sms[0]; ?>
  <form name="frm_sms_template" id="frm_sms_template" method="post" autocomplete="off">
    <input type="hidden" name="template_id" id="template_id" value="<?PHP  echo $smx->templateid; ?>">
    <input type="hidden" name="from_form" id="from_form" value="1">
    <div class="col-md-12">
      <div class="col-md-11 form-group">
        <label class="text-warning">عنوان الرسئل :</label>
        <input name="templatesubject" value="<?PHP  echo $smx->templatesubject; ?>" placeholder="عنوان الرسئل" id="templatesubject" type="text" class="form-control req">
      </div>
      <div class="col-md-11 form-group">
        <label class="text-warning">الرسائل النصية : <?PHP echo implode(',',sms_lagend() ); ?></label>
        <textarea style="resize:none !important; height: 150px;" name="template" id="template" class="form-control req" placeholder="الرسائل النصية"><?PHP echo $smx->template; ?></textarea>
      </div>
      <div class="col-md-6 form-group">
        <button type="button" id="gotohellbabe" name="gotohellbabe" value="nk" class="btn btn-success" onClick="save_frm_sms_template();">حفظ</button>
        <button type="button" id="smscount" class="btn btn-warning dateinput"></button>
      </div>
      <div class="col-md-5 form-group" ></div>
    </div>
    <script>
		$(function(){
			$('#template').keyup(function(){
		var charlen = $(this).val().length;
		var maxi = 60;
		var smscount = Math.floor(charlen/maxi)+1;	
		if(smscount >= 2)
		{	$('#smscount').addClass('smsRed');	}
		else
		{	$('#smscount').removeClass('smsRed');	}
		
		$('#smscount').html(smscount+' / '+charlen);
	});	
		});
	</script>
  </form>
</div>

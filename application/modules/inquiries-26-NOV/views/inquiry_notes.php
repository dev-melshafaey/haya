<div class="row">
	<form name="frm_notes" id="frm_notes" method="post">
		<input type="hidden" name="applicantid" id="applicantid" value="<?PHP echo $applicant_id; ?>">
		<input type="hidden" name="userid" id="userid" value="<?PHP echo $user_detail['profile']->userid; ?>">
		<input type="hidden" name="branchid" id="branchid" value="<?PHP echo $user_detail['profile']->branchid; ?>">
	
    <div class="col-md-12 form-group">
     <h4 class="panel-title">الملاحظات:</h4>
	 <textarea id="notes" name="notes" style="width: 100%; height:100px;"></textarea>
	 <button id="saveNotes" type="button" class="btn btn-success btn-lg"><i class="icon-keyboard"></i> حفظ</button>
	 <button id="showAllNotes" type="button" class="btn btn-success btn-lg"><i class="icon-keyboard"></i> عرض كل الملاحظات</button>
	</div>
  </form>
</div>
<style>
	.print_th {
    border-bottom: 1px solid #ccc !important;
    width: 25% !important;
    text-align: right!important;
    padding: 3px 10px!important;
    font-size: 13px !important;
    background-color: #EFEFEF;
    font-weight: bold;
	}
	.print_td {
		color:#000;
    border-bottom: 1px solid #ccc !important;
    width: 25% !important;
    text-align: right!important;
    padding: 3px 10px!important;
    font-size: 13px !important;
    background-color: #FFF;
    
	}	
	.print_footer {
		
	padding: 8px 10px!important;
    font-size: 12px;
    font-weight: bold;
	
	}
	
</style>
<div class="row">
	<div class="col-md-12 from-group" id="noteslist">
		
	</div>
	<div class="col-md-12 from-group" style="display: none; text-align: center !important;" id="print_bar">
		<button type="button" class="btn btn-success" onclick="printthepage('noteslist');"><i class="icon-print"></i> طباعة</button>
	</div>
</div>
<script>
	$(function(){
		$('#saveNotes').click(function(){
				var notes = $('#notes').val();
				if ($.trim(notes)!='') {
					var notesData = $('#frm_notes').serialize();
					var request = $.ajax({
						url: config.BASE_URL+'inquiries/savemynotes',
						type: "POST",
						data: notesData,
						dataType: "html",
						beforeSend: function(){	$('#saveNotes').hide();  },
						success: function(msg)
						{
						   $('#saveNotes').show();
						   $('#notes').val(' ');
					       show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
						   $('#noteslist').html(msg);
						   $("#print_bar").show();
						}
					  });
				}
			});
		$('#showAllNotes').click(function(){
				var applicantid = $('#applicantid').val();
				var request = $.ajax({
						url: config.BASE_URL+'inquiries/showallnotes',
						type: "POST",
						data: {applicantid:applicantid},
						dataType: "html",
						beforeSend: function(){	  },
						success: function(msg)
						{
						   $('#noteslist').html(msg);
						   $("#print_bar").show();
						}
					  });
			});
		
		});
</script>

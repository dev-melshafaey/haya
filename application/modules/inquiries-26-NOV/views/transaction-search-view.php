<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<style>
.th
{
background: #029625 !important;
color: white !important;
font-size: 14px !important;
}
</style>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12" style="padding:0px;">
        <div class="col-md-12" style="padding: 12px 0px 12px 0px; background-color: #CCC; border-bottom: 2px solid #FFF;">
        <div style="text-align:center;"><h4>برنامج بحث المساعدة</h4></div>
        <form action="<?php echo current_url();?>" method="POST" id="transaction_search" name="transaction_search">
          <div class="col-md-3">
            <label class="text-warning">رقم البطاقة الشخصة</label>
            <input name="idcard_number" value="" placeholder="رقم البطاقة الشخصة" id="idcard_number" type="text" class="form-control req NumberInput <?PHP if($ah_applicant->idcard_number) { if(!$refil_data){?>readonly<?php }} ?>" maxlength="9">
          </div>
          <div class="col-md-3">
            <label class="text-warning">الاسم الرباعي والقبيلة:</label>
            <input name="fullname" value="" placeholder="االاسم الرباعي والقبيلة" id="fullname" type="text" class="form-control req">
          </div>
          <div class="col-md-3">
            <label class="text-warning">المساعدات:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('charity_type','charity_type','',0,''); ?>
          </div>
          <div class="col-md-3">
            <label class="text-warning">الحالة الجتماعية:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('marital_status','marital_status','',0,''); ?>
          </div>
          <div class="col-md-3">
            <label class="text-warning">رقم جواز سفر:</label>
            <input name="passport_number" value="" placeholder="رقم جواز سفر" id="passport_number" type="text" class="form-control Passport <?PHP if($ah_applicant->passport_number) { if(!$refil_data){?>readonly<?PHP } } ?>">
          </div>
          <div class="col-md-3">
            <label class="text-warning">جنسية</label>
            <?php echo $this->haya_model->create_dropbox_list('country_listmanagement','nationality','',0,''); ?>
          </div>
          <div class="col-md-3">
            <label class="text-warning">تاريخ الميلاد:</label>
            <input name="date_of_birth" value="" placeholder="تاريخ الميلاد" id="date_of_birth" type="text" class="form-control req age_datepicker" set-age="applicant_age" />
          </div>
		  <!--<div class="col-md-3">
            <label class="text-warning">المحافظة \ المنطقة:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('province','regions','',0,''); ?></div>
          <br clear="all">
          <div class="col-md-3">
          <label class="text-warning">ولاية</label>
            <?PHP echo $this->haya_model->create_dropbox_list('wilaya','wilaya','','',''); ?> </div>-->
            
          <div class="col-md-3">
          	<label class="text-warning">هاتف المنزل:</label>
            <input name="extratelephone" onBlur="checkPhoneNumber(this);" value="" placeholder="هاتف المنزل" id="extratelephone" type="text" class="form-control NumberInput">
          </div>
           <div class="col-md-3"><br><button type="submit" class="btn btn-danger">بحث</button></div>  
           <br clear="all"> 
           </form>
        </div>
        <br clear="all">
        <table class="table table-bordered table-striped dataTable">
          <thead>          
            <tr class="th">
              <th style="width:300px !important; text-align:right !important;" class="right">الإسم</th>
              <th>نوع المساعدات</th>                
              <th style="width:100px;">البطاقة الشخصة</th>
              <th>المحافظة</th>
              <th>ولاية</th> 
              <th>جنس</th>                 
              <th style="width:90px;">تاريخ التسجيل</th>
              <th>المرحلة</th>
              <th style="width:85px !important;">كل الملاحظات</th>
             </tr>
          </thead>
          <tbody>
          <?php //print_r($return_result);exit();?>
          <?php if(!empty($return_result)):?>
          
          <?php foreach($return_result as $res) { ?>
          <?php $steps	=	 config_item('steps');;?>
            <tr>
              <td class="center"><?php echo '<div class="right"><a class="iconspace" href="'.charity_edit_url($res->charity_type_id).$res->applicantid.'">'.$res->fullname.'</a></div>';?></td>
              <td class="center"><?php echo $res->charity_type;?></td>
              <td class="right"><?php echo $res->idcard_number;?></td>
              <td class="right"><?php echo $res->province;?></td>
              <td class="right"><?php echo $res->wilaya;?></td>
              <td class="right"><?php echo $res->gender;?></td>
              <td class="right"><?php echo date('Y-m-d',strtotime($res->registrationdate));?></td>
              <td class="right"><?php echo $steps[$res->step];?></td>
              <td class="center"><?php echo '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'inquiries/inquiry_notes/'.$res->applicantid.'"><i class="icon-book"></i></a>'.' ('.$this->inq->getNotesCount($res->applicantid).')';?></td>
            </tr>
          <?php } ?>
          <?php endif;?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
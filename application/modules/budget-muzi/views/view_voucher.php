<div class="row col-md-12">
    <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
        <tbody>
            <tr>
                <td colspan="3" class=" btn-success text-right">الميزانية</td>
            </tr>
            <tr>
                <td class="right"><label class="text-warning">رقم الايصال :</label><br><strong><?php echo $row->receipt_num; ?></strong></td>
                <td class="right"><label class="text-warning">المبلغ :</label><br><strong><?php echo 'OMR ' . number_format($row->amount, 3, '.', ','); ?></strong></td>
                <td class="right"><label class="text-warning">التاريخ</label><br><strong><?php echo $row->voucher_date; ?></strong></td>
            </tr>

            <tr>
                <td class="right"><label class="text-warning">اسم البرنامج</label><br><strong><?php echo $row->program_name; ?></strong></td>
                <td class="right"><label class="text-warning">اسم المتبرع</label><br><strong><?php echo $row->donater_name; ?></strong></td>

                <td class="right"><label class="text-warning">نوع السند</label><br><strong><?php echo $row->pay_type == 'cash' ? 'نقدي' :'شيك'; ?></strong></td>
            </tr>
            <?php if ($row->pay_type == 'cheque') { ?>
                <tr>
                    <td class="right"><label class="text-warning">رقم الشيك</label><br><strong><?php echo $row->cheque_num; ?></strong></td>
                    <td class="right"><label class="text-warning">تاريخ الشيك</label><br><strong><?php echo $row->cheque_date; ?></strong></td>
                    <td class="right"><label class="text-warning">البنك</label><br><strong><?php echo $row->cheque_bank; ?></strong></td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="3" class="right"><label class="text-warning">البيان</label><br><strong><?php echo $row->description; ?></strong></td>
            </tr> 



        </tbody>
    </table> 
    <button class="btn btn-success">تأكيد الحفظ</button>
</div>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Outside_help_model extends CI_Model {

	/*
	* Constructor
	*
	*/
	function __construct()
    {
        parent::__construct();
		
		$this->_login_userid = $this->session->userdata('userid');
    }

//-------------------------------------------------------------------
	/*
	* Add Country Registration Detail 
	* @param $data ARRAY
	* return LAST inserted ID
	*/

	function add_country_registration($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('outside_help_countries',$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	* Add Bank Detail according to there Countries
	* @param $data ARRAY
	* return TRUE
	*/

	function add_bank_details($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('banks_details',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Get All Registerd Companies List
	* return OBJECT
	*/

	function get_all_registerd_countries()
    {
		$this->db->select('register_country_id,country_listmanagement,opening_acount_budget,budget_year,created_date');
		
		$query	=	$this->db->get('outside_help_countries');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get All Registerd Countres List
	* return OBJECT
	*/

	function get_country_budget_detail($register_country_id)
    {
		$this->db->select('register_country_id,country_listmanagement,opening_acount_budget,budget_year,created_date');
		$this->db->where('register_country_id',$register_country_id);
		
		$query	=	$this->db->get('outside_help_countries');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
    }
//-------------------------------------------------------------------
	/*
	* Add Project Type Budgets
	* @param $data ARRAY
	* return TRUE
	*/

	function add_project_type_budget($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('outside_projects_budget',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* GET Project Type Budget Values
	* return OBJECT
	*/	
	function get_project_budget_by_country_id($project_budget_id,$payment_type	=	'DEBIT')
	{
		$this->db->select('project_budget_id,project_type,project_budget');
		$this->db->where('project_budget_id',$project_budget_id);
		$this->db->where('payment_type',$payment_type);

		$query = $this->db->get('outside_projects_budget');
		
		$project_values	=	array();
		
		if($query->num_rows() > 0)
		{
			$result	=	$query->result();

			foreach($result	as	$value)
			{
				$project_values[$value->project_type]	=	$value->project_budget_id.'_'.$value->project_budget;
			}

			return $project_values;
		}
	}	
//-------------------------------------------------------------------

	/*
	*
	*
	*/
	function get_country_detail($country_id)
	{
		$data	=	array();
		$this->db->select('register_country_id,country_listmanagement,opening_acount_budget,budget_year,created_date');
		$this->db->where('register_country_id',$country_id);
		$query	=	$this->db->get('outside_help_countries');
		
		if($query->num_rows() > 0)
		{
			$data['basic']	=	 $query->row();
		}
		
		$this->db->select('bank_id,register_country_id,bankid,branchid,account_no,phone_no,fax_no,email_address,index_num');
		$this->db->where('register_country_id',$country_id);
		$query	=	$this->db->get('banks_details');
		
		if($query->num_rows() > 0)
		{
			$data['bank_detail']	=	 $query->result();
		}
		
		return $data;
	}
//-------------------------------------------------------------------

	/*
	* Update Country Basic record
	* @param $register_country_id int
	* @param $country_detail int
	* return TRUE
	*/
	function update_country_registration($register_country_id,$country_detail)
	{
		$json_data	=	json_encode(array('data'	=>	$country_detail));
		
		$this->db->where('register_country_id',$register_country_id);
		$this->db->update('outside_help_countries',$json_data,$this->session->userdata('userid'),$country_detail);
		
		return TRUE;
	}
//-------------------------------------------------------------------

	/*
	* Update Country Banks record
	* @param $bank_id int
	* @param $register_country_id int
	* @param $bank_detail ARRAY
	* return TRUE
	*/
	function update_bank_details($bank_id,$register_country_id,$bank_detail)
	{
		$json_data	=	json_encode(array('data'	=>	$bank_detail));
		
		$this->db->where('bank_id',$bank_id);
		$this->db->where('register_country_id',$register_country_id);
		
		$this->db->update('banks_details',$json_data,$this->session->userdata('userid'),$bank_detail);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	* Check this bank ID exist OR NOT
	* @param $bank_id int
	* return str
	*/

	function bankid_exist($bank_id)
    {
		$this->db->select('bank_id');
		$this->db->where('bank_id',$bank_id);
		$query	=	$this->db->get('banks_details');

		if($query->num_rows() > 0)
		{
			return $query->row()->bank_id;
		}
    }
//-------------------------------------------------------------------
	/*
	* Check this bank ID exist OR NOT
	* @param $country_id int
	* @param $project_type int
	* return OBJECT
	*/

	function get_project_budget($country_id,$project_type,$payment_type	=	'DEBIT')
    {
		$this->db->select('project_budget_id,country_budget_id,project_type,project_budget');
		$this->db->where('country_budget_id',$country_id);
		$this->db->where('project_type',$project_type);
		$this->db->where('payment_type',$payment_type);
		
		$query	=	$this->db->get('outside_projects_budget');

		if($query->num_rows() > 0)
		{
			return $query->row();
		}
    }
//-------------------------------------------------------------------
	/*
	* UPDATE Projects Types
	* @param $country_id int
	* @param $project_type int
	* @param $data ARRAY
	* return OBJECT
	*/

	function edit_project_type_budget($project_type,$country_id,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('country_budget_id',$country_id);
		$this->db->where('project_type',$project_type);
		
		$this->db->update('outside_projects_budget',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Check this bank ID exist OR NOT
	* @param $country_id int
	* @param $project_type int
	* return OBJECT
	*/

	function project_type_exist($project_type,$country_id,$payment_type	=	'DEBIT')
    {
		$this->db->select('project_budget_id,country_budget_id,project_type,project_budget');
		$this->db->where('country_budget_id',$country_id);
		$this->db->where('project_type',$project_type);
		$this->db->where('payment_type',$payment_type);
		
		$query	=	$this->db->get('outside_projects_budget');

		if($query->num_rows() > 0)
		{
			return $query->row();
		}
    }
//-------------------------------------------------------------------
	/*
	* Check this bank ID exist OR NOT
	* @param $country_id int
	* @param $project_type int
	* return OBJECT
	*/
	public function get_project_budget_sum($country_budget_id,$payment_type	=	'DEBIT')
	{
		// ------- Old function --------
		/*$this->db->select('SUM(project_budget) as total');
		$this->db->where('country_budget_id', $country_budget_id);
		$this->db->where('payment_type',$payment_type);
		
		$query	=	$this->db->get('outside_projects_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->total;
		}*/
		

		// ------- Modifing this function --------
		$this->db->select('SUM(project_budget) as total');
		$this->db->where('country_budget_id', $country_budget_id);
		$this->db->where('payment_type','DEBIT');
		
		$query_1	=	$this->db->get('outside_projects_budget');
		$debit		=	$query_1->row()->total;
		
		$this->db->select('SUM(project_budget) as total');
		$this->db->where('country_budget_id', $country_budget_id);
		$this->db->where('payment_type','CREDIT');
		
		$query_2	=	$this->db->get('outside_projects_budget');
		$credit		=	$query_2->row()->total;
		
		return $result	=	($debit	-	$credit);
		
	}
//-------------------------------------------------------------------
	/*
	* Get Country Projects Details
	* @param $country_id int
	* return OBJECT
	*/

	function get_country_project_budget($country_id,$payment_type	=	'DEBIT')
    {
		$this->db->select('project_budget_id,country_budget_id,project_type,project_budget');
		$this->db->where('country_budget_id',$country_id);
		$this->db->where('payment_type',$payment_type);
		
		$query	=	$this->db->get('outside_projects_budget');

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Country Projects Type Budget
	* @param $country_id int
	* @param $project_type_id int
	* return OBJECT
	*/

	function get_project_type_budget($country_id,$project_type_id,$payment_type	=	'DEBIT')
    {
		$this->db->select('project_budget');
		$this->db->where('country_budget_id',$country_id);
		$this->db->where('project_type',$project_type_id);
		$this->db->where('payment_type',$payment_type);
		
		$query	=	$this->db->get('outside_projects_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->project_budget;
		}
    }
//-------------------------------------------------------------------
	/*
	* Add Spend Project Type Budget Amount
	* @param $data ARRAY
	* return TRUE
	*/

	function add_type_budget($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('outside_spending_projects_budgets',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Get Sum of all Spending Project Types Budgets
	* @param $country_id int
	* @param $project_type_id int
	* return OBJECT
	*/

	function get_total_project_type_spending_budget($country_id,$project_type_id)
    {
		$this->db->select('SUM(spending_project_budget) AS spending');
		$this->db->where('country_budget_id',$country_id);
		$this->db->where('project_type',$project_type_id);
		
		$query	=	$this->db->get('outside_spending_projects_budgets');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->spending;
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Sum of all Spending Project Types Budgets
	* @param $country_id int
	* @param $project_type_id int
	* return OBJECT
	*/

	function get_project_type_spending_budget($country_id,$project_type_id)
    {
		$this->db->select('spending_project_budget,companyid,cat_id,country_budget_id,submitted_date');
		$this->db->where('country_budget_id',$country_id);
		$this->db->where('project_type',$project_type_id);
		//$this->db->group_by('cat_id');
		
		$query	=	$this->db->get('outside_spending_projects_budgets');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Project type Categories Spending Budget
	* @param $cat_id int
	* return OBJECT
	*/

	function get_cat_spending_budget($cat_id)
    {
		$this->db->select('spending_project_budget,companyid,cat_id,country_budget_id,project_type,submitted_date');

		$this->db->where('cat_id',$cat_id);

		$query	=	$this->db->get('outside_spending_projects_budgets');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Project Type Categories
	* @param $country_id int
	* @param $project_type_id int
	* return OBJECT
	*/

	function get_project_type_categories($country_id,$project_type_id)
    {
		$this->db->select('cat_id,cat_name,cat_budget,detail');
		$this->db->where('country_id',$country_id);
		$this->db->where('project_type_id',$project_type_id);
		$this->db->where('delete_record','0');
		
		$query	=	$this->db->get('outside_project_type_categories');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Project Type Category Name
	* @param $country_id int
	* @param $project_type_id int
	* return Str
	*/

	function get_project_type_category_name($cat_id)
    {
		$this->db->select('cat_name');
		$this->db->where('cat_id',$cat_id);
		$this->db->where('delete_record','0');
		
		$query	=	$this->db->get('outside_project_type_categories');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->cat_name;
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Project Type Category Budget
	* @param $cat_id int
	* return Str
	*/

	function get_project_category_budget($cat_id)
    {
		$this->db->select('cat_budget');
		$this->db->where('cat_id',$cat_id);
		$this->db->where('delete_record','0');
		
		$query	=	$this->db->get('outside_project_type_categories');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->cat_budget;
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Project Type Category Budget
	* @param $cat_id int
	* return Str
	*/

	function get_category_data($cat_id)
    {
		$this->db->select('cat_id,country_id,project_type_id,cat_name,cat_budget,detail');
		$this->db->where('cat_id',$cat_id);
		$this->db->where('delete_record','0');
		
		$query	=	$this->db->get('outside_project_type_categories');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Project Type Category Budget
	* @param $cat_id int
	* return Str
	*/

	function sum_of_project_cat_budget()
    {
		$this->db->select('SUM(cat_budget) AS total');
		$this->db->where('delete_record','0');
		
		$query	=	$this->db->get('outside_project_type_categories');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->total;
		}
    }	
//-------------------------------------------------------------------
	/*
	* Add Project Type category
	* @param $data ARRAY
	* return TRUE
	*/

	function add_project_type_category($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('outside_project_type_categories',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Add Project Type category
	* @param $data ARRAY
	* return TRUE
	*/

	function update_project_type_category($cat_id,$data)
    {
		$json_data	=	json_encode(array('record'	=>	'delete','cat_id'	=>	$cat_id));
		
		$this->db->where('cat_id',$cat_id);
		$this->db->update('outside_project_type_categories',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* 
	* 
	* 
	* 
	*/

	function spend_from_project_category_budget($cat_id)
    {
		$this->db->select('SUM(spending_project_budget) AS spending_amount');
		$this->db->where('cat_id',$cat_id);
		
		$query	=	$this->db->get('outside_spending_projects_budgets');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->spending_amount;
		}
    }
//-------------------------------------------------------------------
	/*
	* Delete Caegory ID
	* @param $cat_id Int
	* return TRUE
	*/

	function delete_project_type_category($cat_id)
    {
		$json_data	=	json_encode(array('record'	=>	'delete','cat_id'	=>	$cat_id));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('cat_id',$cat_id);
		$this->db->update('outside_project_type_categories',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Get Project Type Category Name
	* @param $country_id int
	* @param $project_type_id int
	* return Str
	*/

	function get_companies_by_country($country_id)
    {
		$this->db->select('companyid,english_name,arabic_name');
		$this->db->where('country_listmanagement',$country_id);
		$this->db->where('delete_record','0');
		
		$query	=	$this->db->get('ah_company');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Company Name
	* @param $companyid int
	* return OBJECT
	*/

	function get_company($companyid)
    {
		$this->db->select('companyid,english_name,arabic_name');
		$this->db->where('companyid',$companyid);
		$this->db->where('delete_record','0');
		
		$query	=	$this->db->get('ah_company');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
    }
//-------------------------------------------------------------------
}
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12 form-group">
        <div class="col-md-12 form-group" style="background-color: #FFF;
    padding-bottom: 3px;
    border: 1px solid #CCC;
    margin: 15px 16px;
    width: 97%;">
          <h4 > الدولة : <?php echo $this->haya_model->get_name_from_list($country_budget->country_listmanagement); ?> -  سنة : <?php echo arabic_date($country_budget->budget_year); ?> &nbsp;&nbsp;
نوع المشروع الميزانيات</h4>
        </div>
        
        <!-- -------------------------------------------------------------------------------------- -->
        <div class="col-md-12 form-group">
          <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
            <thead>
              <tr role="row">
                <th style="text-align:center;">نوع المشروع</th>
                <th style="text-align:center;">ميزانية</th>
                <th style="text-align:center;">الإنفاق المنبع</th>
                <th style="text-align:center;">المتبقية</th>
                <th style="text-align:center;">الإجراءات</th>
              </tr>
            </thead>
            <tbody role="alert" aria-live="polite" aria-relevant="all">
              <?php $departments	=	$this->haya_model->get_dropbox_list_value('outside_projects_types');?>
              <?php if(!empty($departments)):?>
              <?php foreach($departments as $dpt):?>
              <?php $project_budget		=	$this->outside->get_project_budget($country_id,$dpt->list_id); ?>
              <?php $spending_budget	=	$this->outside->get_total_project_type_spending_budget($country_id,$dpt->list_id);?>
              <?php $remaining			=	($project_budget->project_budget	-	$spending_budget);?>
              
              <?php $actions  = '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'outside_help/project_type_categories/'.$country_id.'/'.$dpt->list_id.'"><i class="myicon icon-sitemap"></i></a>';?>
              <?php $actions .= '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'outside_help/add_project_type_category/'.$country_id.'/'.$dpt->list_id.'"><i class="icon-plus-sign-alt"></i></a>';?>
              <?php $actions .= '&nbsp;<a <a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'outside_help/add_type_budget/'.$country_id.'/'.$dpt->list_id.'"><i class="myicon icon-money"></i></a>';?>
              <?php $actions .= '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'outside_help/spending_projects_budget_amount/'.$country_id.'/'.$dpt->list_id.'"><i class="icon-eye-open"></i></a>';?>
              <tr role="row" id="<?php echo $dpt->list_id?>_durar_lm">
                <td  style="text-align:center;"><?php echo $dpt->list_name?></td>
                <td  style="text-align:center;"><?php echo number_format($project_budget->project_budget,3);?></td>
                <td><?php echo number_format($spending_budget,3);?></td>
                <td><?php echo number_format($remaining,3);?></td>
                <td><?php echo $actions;?></td>
              </tr>
              <?php unset($actions); endforeach;?>
              <?php endif;?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
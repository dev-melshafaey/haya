<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
	  <?php $msg	=	$this->session->flashdata('msg');?>
      <?php if($msg):?>
          <div class="col-md-12">
            <div style="padding: 22px 20px !important; background:#c1dfc9;">
                <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
            </div>
          </div>
      <?php endif;?>
      <div class="col-md-12 form-group">
        <form name="frm_country_registration" id="frm_country_registration"  method="post" autocomplete="off">
          <input type="hidden" id="register_country_id" name="register_country_id" value="<?php //echo $company->companyid; ?>">
          <div class="nav nav-tabs panel panel-default panel-block">
            <div class="tab-pane list-group active">
              <div class="col-md-12 form-group" style="margin-top: 10px; border-left: 1px solid #EFEFEF;">
                <div class="col-md-4 form-group">
                  <label class="text-warning">اختر الدولة :</label>
                  <?php echo $this->haya_model->create_dropbox_list('country_listmanagement','issuecountry',$company->country_listmanagement,0,''); ?> </div>
                <div class="col-md-4 form-group">
                  <label class="text-warning">الميزانية فتح الحساب :</label>
                  <input name="opening_acount_budget" value="<?php //echo($company->english_name); ?>" placeholder="الميزانية فتح الحساب" id="opening_acount_budget" type="text" class="form-control req" onKeyUp="only_numeric(this);">
                </div>
                <div class="col-md-4 form-group">
                  <label class="text-warning">عام :</label>
                  <input name="budget_year" value="<?php //echo($company->english_name); ?>" placeholder="الميزانية فتح الحساب" id="budget_year" type="text" class="form-control req" onKeyUp="only_numeric(this);">
                </div>
                <br clear="all" />
               	<h4 style="border-bottom: 2px solid #EEE;">تفاصيل البنك:</h4>
                <div class="multiple-block">
                  <div class="col-md-4 form-group">
                    <label class="text-warning">إسم البنك:</label>
                    <?PHP echo $this->haya_model->create_dropbox_list_help('bankid','bank',$bankid,0,'req','','1','branchid1'); ?> </div>
                  <div class="col-md-4 form-group">
                    <label class="text-warning">الفرع:</label>
                    <?PHP echo $this->haya_model->create_dropbox_list_help('branchid','bank_branch',$branchid,$bankid,'req','','1'); ?> </div>
                  <div class="col-md-4 form-group">
                    <label class="text-warning">رقم حساب  :</label>
                    <input name="account_no[1]"  placeholder="رقم حساب" id="account_no" type="text" class="form-control req" onKeyUp="only_numeric(this);">
                  </div>
                  <div class="col-md-4 form-group">
                    <label class="text-warning">رقم الهاتف  :</label>
                    <input name="phone_no[1]"  placeholder="رقم الهاتف" id="phone_no" type="text" class="form-control req" onKeyUp="only_numeric(this);">
                  </div>
                  <div class="col-md-4 form-group">
                    <label class="text-warning">رقم الفاكس  :</label>
                    <input name="fax_no[1]"  placeholder="رقم الفاكس" id="fax_no" type="text" class="form-control req" onKeyUp="only_numeric(this);">
                  </div>
                  <div class="col-md-4 form-group">
                    <label class="text-warning">البريد الإلكتروني  :</label>
                    <input name="email_address[1]"  placeholder="البريد الإلكتروني" id="email_address" type="text" class="form-control req">
                  </div>
                </div>
                <div class="col-md-12 form-group"> <a class="add_multifields" href="#_" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> </div>
                <br clear="all"/>
                <div class="col-md-12 form-group" id="multifields"></div>
                <div class="col-md-12 form-group">
                  <button type="button" id="save_country_registration" class="btn btn-success">Save</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
	$(function(){
		$(".add_multifields").click(function(){
			
			var total_divs	=	$(".multiple-block").length+1;
		/*$('#frmbuild .req').removeClass('parsley-error');
		var form_action = $('#frmbuild').attr('action');
		var ht = '<ul>';
		$('#frmbuild .req').each(function(index, element)
			{
				if($(this).val()=='')
				{
					$(this).addClass('parsley-error');
					ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
				}
			});
			var redline = $('#frmbuild .parsley-error').length;
			ht += '</ul>';*/
			var redline	=	5;
			if (redline == 4) {
				show_notification_error_end(ht);
			} else {
				
				$.ajax({
					url: config.BASE_URL+'outside_help/add_multifields',
					type:"POST",
					data:{total_divs:total_divs},
					dataType:"html",				
					success: function(msg)
					{
						$("#multifields").after(msg);
					}
				  });
			}
	});
	 $('#save_country_registration').click(function(){
		 $('#frm_country_registration .req').removeClass('parsley-error');
			var ht = '<ul>';
			$('#frm_country_registration .req').each(function (index, element) {
				if ($(this).val() == '') 
				{
					$(this).addClass('parsley-error');
					ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
				}
			});
			var redline = $('#frm_country_registration .parsley-error').length;
			ht += '</ul>';			
			if (redline <= 0) 
			{	$("#frm_country_registration").submit();	}
			else 
			{	show_notification_error(ht);	}	 
		 
	});
	});
</script>
</div>
</body>
</html>
<?php $remaining	=	($project_type_budget	-	$spending_budget);?>
<div class="col-md-12" id="myprint"  style="direction:rtl;">
  <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <?php if(!empty($spending_project_budgets)):?>
    <tr>
      <td colspan="7" class="customhr">التفاصيل:</td>
    </tr>
    <tr>
      <th style="text-align:center;">تاريخ</th>
      <th style="text-align:center;">نوع المساعدة</th>
      <th style="text-align:center;">الشركة</th>
      <th style="text-align:center;">اسم التصنيف</th>
      <th style="text-align:center;">الميزانية المحددة</th>
      <th style="text-align:center;">الإنفاق المبلغ</th>
      <th style="text-align:center;">المتبقية</th>
    </tr>
    <?php foreach($spending_project_budgets	as $budget):?>
    <?php $category_name	=	$this->outside->get_project_type_category_name($budget->cat_id);?>
    <?php $company_name		=	$this->outside->get_company($budget->companyid);?>
    <?php $cat_budget		=	$this->outside->get_project_category_budget($budget->cat_id);?>
    <?php $remaining		=	($cat_budget	-	$budget->spending_project_budget);?>
    <?php $cat_budget_total	+=	$cat_budget;?>
    <?php $spend_total		+=	$budget->spending_project_budget;?>
    <?php $remaining_total	+=	$remaining;?>
    <tr>
      <td><?php echo $budget->submitted_date;?></td>
      <td><?php echo $this->haya_model->get_name_from_list($budget->project_type);?></td>
      <td><?php echo $company_name->english_name.'|'.$company_name->arabic_name	;?></td>
      <td><?php echo $category_name;?></td>
      <td><?php //echo number_format($cat_budget,3);?></td>
      <td><?php echo number_format($budget->spending_project_budget,3);?></td>
      <td><?php //echo number_format($remaining,3);?></td>
    </tr>
    <?php endforeach;?>
    <?php $remaining_cat_budget	=	($cat_budget	-	$spend_total);?>
    <tr>
      <td class="text-warning"><strong>مجموع</strong></td>
      <td></td>
      <td></td>
      <td></td>
      <td><strong><?php echo number_format($cat_budget,3);?></strong></td>
      <td><strong><?php echo number_format($spend_total,3);?></strong></td>
      <td><strong><?php echo number_format($remaining_cat_budget,3);?></strong></td>
    </tr>
    <?php endif;?>
  </table>
</div>
<div class="col-md-12 center">
  <button type="button" class="btn" id="print" onclick="printthepage('myprint');"><i class="icon-print"></i> طباعة</button>
</div>
<script>
$(function(){
	$('#t_heading').html('<?php echo $this->haya_model->get_name_from_list($project_type)?>');
});
</script>
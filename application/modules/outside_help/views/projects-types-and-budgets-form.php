<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
        <?php $msg	=	$this->session->flashdata('msg');?>
	  <?php if($msg):?>
          <div class="col-md-12">
            <div style="padding: 22px 20px !important; background:#c1dfc9;">
                <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
            </div>
          </div>
      <?php endif;?>
      <div class="col-md-12 form-group">
        <form method="post" action="<?php echo current_url();?>" enctype="multipart/form-data" id="add_budget" name="add_budget">
<!--          <input type="hidden" name="project_budget_id" id="project_budget_id" value="<?php echo $country_budget->project_budget_id;?>"/>
-->          <input type="hidden" name="country_budget_id" id="country_budget_id" value="<?PHP echo $country_budget->register_country_id; ?>" />
			 <input type="hidden" name="payment_type" id="payment_type" value="DEBIT" />
          <div class="col-md-12 form-group" style="background-color: #FFF;
    padding-bottom: 3px;
    border: 1px solid #CCC;
    margin: 15px 16px;
    width: 97%;">
            <h4 > الدولة : <?php echo $this->haya_model->get_name_from_list($country_budget->country_listmanagement); ?> -  سنة : <?php echo arabic_date($country_budget->budget_year); ?></h4>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">مجموع الميزانية :</label>
            <input type="text" class="form-control req NumberInput"  placeholder="مجموع الميزانية" name="budget_amount" id="budget_amount" value="<?php echo $country_budget->opening_acount_budget;?>" />
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">المتبقية :</label>
            <span id="dept-count" class="form-control" style="font-size: 16px; font-weight: bold; letter-spacing: 1px;">0</span> </div>
          <div class="col-md-12 form-group" style="background-color: #FFF;
    padding-bottom: 3px;
    border: 1px solid #CCC;
    margin: 15px 16px;
    width: 97%;">
            <h4 >قسم الميزانية</h4>
          </div>
          <?php $departments	=	$this->haya_model->get_dropbox_list_value('outside_projects_types');?>
          <?php foreach($departments as $dpt):?>
          <?php $project_budget	=	$this->outside->get_project_budget($country_id,$dpt->list_id); ?>
          <?php //$explode	=	explode('_',$project_budgets[$dpt->list_id]);?>
          <div class="col-md-4 form-group">
            <label class="text-warning"><?php echo $dpt->list_name?> :</label>
            <input type="text" class="form-control NumberInput project_type req"  placeholder="<?php echo $dpt->list_name;?>" name="project_type_budget[<?php echo $dpt->list_id?>]" id="project_type" value="<?php echo $project_budget->project_budget;?>">
          </div>
          <?php endforeach;?>
          <br clear="all">
          <div class="col-md-12 form-group newboxxxx">
            <input type="button" value="حفظ العرض" onClick="saveBudgetForm();" name="submit_budget" id="submit_budget" class="btn btn-success mws-login-button">
            <span id="show-loader" style="display:none;"><img src="<?php echo base_url();?>assets/images/hourglass.gif" /></span> <br clear="all">
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
$(document).ready(function (){
	var total_amount	=	$("#budget_amount").val();
 	var dept_count	=	0;
	var remaining	=	0;
  
  $('.project_type').each(function(index, element) 
  {
	dept_count += Number($(this).val());
	
	var remaining	=	(total_amount-dept_count);
	$("#dept-count").html(remaining);

  });
  
	});
</script> 
<script>
function saveBudgetForm()
{
	$('#add_budget .req').removeClass('parsley-error');

	 var ht = '<ul>';
		$('#add_budget .req').each(function(index, element) {
			if($(this).val()=='')
			{
				$(this).addClass('parsley-error');
				ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
			}
		});
	  var redline = $('#add_budget .parsley-error').length;
	  ht += '</ul>';
	  if(redline <= 0)
	  {
		  var total_amount	=	$("#budget_amount").val();
		  var dept_count	=	0;
		  
		  $('.project_type').each(function(index, element) {
		  	if($(this).val()!='')
			{
				dept_count += Number($(this).val());
			}
		  });
		  
		  if(total_amount > dept_count	|| total_amount == dept_count)
		  {
			  $('input[type="submit"]').attr('disabled','disabled');
		  
		  	  $("#show-loader").show();
		  
			  document.getElementById("add_budget").submit();
		  }
		  else
		  {
			  show_notification_error_end('Your Budget Amount is Less than your departments Budget.');
		  }
			
	  }
	  else
	  {	
	  	show_notification_error_end(ht);	
	  }
}
$( ".project_type" ).keyup(function() {
	
  var total_amount	=	$("#budget_amount").val();
  var dept_count	=	0;
  
  $('.project_type').each(function(index, element) {
	if($(this).val()!='')
	{
		dept_count += Number($(this).val());
		
		var remaining	=	(total_amount-dept_count);
		$("#dept-count").html(remaining);
	}
  });
  
  if(total_amount < dept_count)
  {
	  show_notification_error_end('لديك مشروع الميزانية المبلغ هو أقل من بلدكم الميزانية المبلغ.');
  }	
});
</script>
</div>
</body>
</html>
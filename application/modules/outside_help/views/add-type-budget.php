<?php $remaining	=	($project_type_budget	-	$spending_budget);?>
<div class="row col-md-12">
<br clear="all"/>
    <div class="col-md-4 form-group">
   	  <label class="text-warning">مجموع : <?php echo number_format($project_type_budget,3);?></label>
    </div>
   <div class="col-md-4 form-group">
   	  <label class="text-warning">الإنفاق المنبع : <?php echo number_format($spending_budget,3);?></label>
    </div>
   <div class="col-md-4 form-group">
      <label class="text-warning">المتبقية : <?php echo number_format($remaining,3);?></label>
    </div>
  <form class="mws-form" method="post" action="<?php echo current_url();?>" id="add_project_type_budget" name="add_project_type_budget" enctype="multipart/form-data">
    <input type="hidden" name="country_budget_id" id="country_budget_id" value="<?PHP echo $country_id; ?>">
    <input type="hidden" name="project_type_id" id="project_type_id" value="<?php echo $project_type_id;?>"  />
    <input type="hidden" name="remaining" id="remaining"/>
    <div class="col-md-6 form-group">
      <label class="text-warning">اختار :</label>
      <select class="form-control req" name="cat_id" id="cat_id">
      <option value="">اختار</option>
      <?php foreach($categories	as $category):?>
      	<option value="<?php echo $category->cat_id;?>"><?php echo $category->cat_name;?></option>
      <?php endforeach;?>
      </select>
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">اختيار الشركة :</label>
      <select class="form-control req" name="companyid" id="companyid">
      <option value="">اختيار الشركة</option>
      <?php foreach($companies	as $company):?>
      	<option value="<?php echo $company->companyid;?>"><?php echo $company->english_name.'|'.$company->arabic_name;?></option>
      <?php endforeach;?>
      </select>
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">كمية :	<span id="dynamic-data"></span></label>
      <input type="text" class="form-control req" name="spending_project_budget" id="spending_project_budget" value="<?php //echo $company->documenttitle;?>" onKeyUp="only_numeric(this);">
      <label class="text-warning" id="show-msg" style="color:#F00 !important;"></label>
    </div>
    <div class="col-md-4 form-group">
     <label class="text-warning"></label>
      <input type="button" value="حفظ" name="submit" id="spen_project_amount" class="btn btn-success spen_project_amount" onclick="submit_project_type_budget();" style="margin-top: 26px !important;">
    </div>
  </form>
</div>

<script>
$(function(){
	$('#t_heading').html('<?php echo $this->haya_model->get_name_from_list($project_type_id)?>');
	
	var	remaining	=	$("#remaining").val();
	/*if(remaining	<=	0)
	{
		$('#spen_project_amount').attr("disabled", true);
	}*/
	
	$( "#spending_project_budget" ).keyup(function() {

	var	current_amount	=	Number($("#spending_project_budget").val());
	var	remaining		=	Number($("#remaining").val());

	if(remaining < current_amount)
	{
		$('#spen_project_amount').attr("disabled", true);
		$('#show-msg').html("Your Budget is increase from "+remaining);
	}
	else
	{
		$('#spen_project_amount').attr("disabled", false);
		$('#show-msg').html("");
	}
		
	});
	$('#cat_id').change(function(){
			
		var cat_id	=	$("#cat_id").val();
			
		var request = $.ajax({
		  url: config.BASE_URL+'outside_help/get_project_category_budget',
		  type: "POST",
		  dataType:"json",
		  data: {cat_id:cat_id},
		  beforeSend: function(){},
		  success: function(data)
		  {
			  $("#remaining").val(data.remaining);
			  $("#dynamic-data").html(data.msg);
			  
			  if(data.remaining == 0)
			  {
				  $('#spen_project_amount').attr("disabled", true);
			  }
			  else
			  {
				  $('#spen_project_amount').attr("disabled", false);
			  }
			 
		  } 
		});
	});
});

function submit_project_type_budget()
{

	$('#add_project_type_budget .req').removeClass('parsley-error');
    $('#add_project_type_budget .req').each(function (index, element) {
        if ($.trim($(this).val()) == '') {
            $(this).addClass('parsley-error');
        }
    });
    var len = $('#add_project_type_budget .parsley-error').length;

	if(len<=0)
	{

		
		var str_data = 	$('#add_project_type_budget').serialize();
		
		var request = $.ajax({
		  url: config.BASE_URL+'outside_help/add_type_budget',
		  type: "POST",
		  data: str_data,
		  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(newrow)
		  {
			  window.location.href = newrow;
		  } 
		});
	}
}
</script>
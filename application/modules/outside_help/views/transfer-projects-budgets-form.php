<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12 form-group">
        <form method="post" action="<?php echo current_url();?>" enctype="multipart/form-data" id="transfer_project_budget" name="transfer_project_budget">
<!--          <input type="hidden" name="project_budget_id" id="project_budget_id" value="<?php echo $country_budget->project_budget_id;?>"/>
-->          <input type="hidden" name="country_budget_id" id="country_budget_id" value="<?PHP echo $country_budget->register_country_id; ?>" />
          <div class="col-md-12 form-group" style="background-color: #FFF;
    padding-bottom: 3px;
    border: 1px solid #CCC;
    margin: 15px 16px;
    width: 97%;">
            <h4 > الدولة : <?php echo $this->haya_model->get_name_from_list($country_budget->country_listmanagement); ?> -  سنة : <?php echo arabic_date($country_budget->budget_year); ?></h4>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">مجموع الميزانية :</label>
            <input type="text" class="form-control NumberInput"  placeholder="مجموع الميزانية" name="budget_amount" id="budget_amount" value="<?php echo $country_budget->opening_acount_budget;?>" disabled/>
          </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">المتبقية :</label>
            <span id="dept-count" class="form-control" style="font-size: 16px; font-weight: bold; letter-spacing: 1px;">0</span> </div>
          <div class="col-md-12 form-group" style="background-color: #FFF;
    padding-bottom: 3px;
    border: 1px solid #CCC;
    margin: 15px 16px;
    width: 97%;">
            <h4 >قسم الميزانية</h4>
          </div>
          <?php $departments	=	$this->haya_model->get_dropbox_list_value('outside_projects_types');?>
          <?php foreach($departments as $dpt):?>
          <?php $project_budget	=	$this->outside->get_project_budget($country_id,$dpt->list_id); ?>

          <div class="col-md-4 form-group">
            <label class="text-warning"><?php echo $dpt->list_name?> :</label>
            <input type="text" class="form-control NumberInput project_type"  placeholder="<?php echo $dpt->list_name;?>" name="project_type_budget[<?php echo $dpt->list_id?>]" id="project_type" value="<?php echo $project_budget->project_budget;?>" disabled>
          </div>
          <?php endforeach;?>
          <br clear="all">
          <div class="col-md-12 form-group" style="background-color: #FFF;
    padding-bottom: 3px;
    border: 1px solid #CCC;
    margin: 15px 16px;
    width: 97%;">
            <h4 >نقل مبلغ الميزانية</h4>
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">قسم الميزانية :</label>
            <select class="form-control req" id="list_id" name="list_id" placeholder="قسم الميزانية">
            	<option value="">اختار</option>
                <?php foreach($departments as $dpt):?>
                <?php $project_budget	=	$this->outside->get_project_budget($country_id,$dpt->list_id); ?>
                    <option value="<?php echo $dpt->list_id?>"><?php echo $dpt->list_name;?></option>
                <?php endforeach;?>
            </select>
          </div>
         <div class="col-md-4 form-group" id="dynamic-data">
            <label class="text-warning">قسم الميزانية :</label>
            <select class="form-control req" readonly>
            	<option value="">اختار</option>
                <?php foreach($departments as $dpt):?>
                <?php $project_budget	=	$this->outside->get_project_budget($country_id,$dpt->list_id); ?>
                    <option value="<?php echo $dpt->list_id?>"><?php echo $dpt->list_name;?></option>
                <?php endforeach;?>
            </select>
          </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">القيمة :</label>
            <input type="text" class="form-control req NumberInput"  placeholder="القيمة" name="transfer_amount" id="transfer_amount" />
          </div>
          <div class="col-md-12 form-group newboxxxx">
            <input type="button" value="حفظ العرض" onClick="saveBudgetForm();" name="submit_budget" id="submit_budget" class="btn btn-success mws-login-button">
            <span id="show-loader" style="display:none;"><img src="<?php echo base_url();?>assets/images/hourglass.gif" /></span> <br clear="all">
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
$(document).ready(function (){
	var total_amount	=	$("#budget_amount").val();
 	var dept_count	=	0;
	var remaining	=	0;
  
  $('.project_type').each(function(index, element) 
  {
	dept_count += Number($(this).val());
	
	var remaining	=	(total_amount-dept_count);
	$("#dept-count").html(remaining);

  });
  	$('#list_id').change(function(){
			
		var list_id		=	$("#list_id").val();
		var country_id	=	'<?php echo $country_id;?>';
			
		var request = $.ajax({
		  url: config.BASE_URL+'outside_help/get_country_projects/',
		  type: "POST",
		  data: {list_id:list_id,country_id:country_id},
		  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(data)
		  {
			  $("#dynamic-data").html(data);
		  }
		});
	});
	
	$("#transfer_amount").keyup(function(){
		
		var project_budget	=	Number($("#type-budget").text());
		var transfer_amount	=	Number($("#transfer_amount").val());
		
		//alert('Project Budget = '+project_budget+' Transfer Amount = '+transfer_amount);
		
		if(project_budget	<	transfer_amount)
		{
			show_notification_error_end('<ul><li>The amount is exceed from '+project_budget+'</li></ul>');
		}
		
	});
});
</script> 
<script>
function saveBudgetForm()
{
	$('#transfer_project_budget .req').removeClass('parsley-error');

	 var ht = '<ul>';
		$('#transfer_project_budget .req').each(function(index, element) {
			if($(this).val()=='')
			{
				$(this).addClass('parsley-error');
				ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
			}
		});
	  var redline = $('#transfer_project_budget .parsley-error').length;
	  ht += '</ul>';
	  if(redline <= 0)
	  {
		  var total_amount	=	$("#budget_amount").val();
		  var dept_count	=	0;
		  
		  $('.project_type').each(function(index, element) {
		  	if($(this).val()!='')
			{
				dept_count += Number($(this).val());
			}
		  });
		  
		  if(total_amount > dept_count	|| total_amount == dept_count)
		  {
			  $('input[type="submit"]').attr('disabled','disabled');
		  
		  	  $("#show-loader").show();
		  
			  document.getElementById("transfer_project_budget").submit();
		  }
		  else
		  {
			  show_notification_error_end('Your Budget Amount is Less than your departments Budget.');
		  }
			
	  }
	  else
	  {	
	  	show_notification_error_end(ht);	
	  }
}
$( ".project_type" ).keyup(function() {
	
  var total_amount	=	$("#budget_amount").val();
  var dept_count	=	0;
  
  $('.project_type').each(function(index, element) {
	if($(this).val()!='')
	{
		dept_count += Number($(this).val());
		
		var remaining	=	(total_amount-dept_count);
		$("#dept-count").html(remaining);
	}
  });
  
  if(total_amount < dept_count)
  {
	  show_notification_error_end('لديك مشروع الميزانية المبلغ هو أقل من بلدكم الميزانية المبلغ.');
  }	
});
</script>
</div>
</body>
</html>
<div class="col-md-12" id="myprint"  style="direction:rtl;">
  <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tr>
      <td colspan="6" class="customhr">بلد التفاصيل:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">الدولة :</label>
        <br />
        <strong><?php echo $this->haya_model->get_name_from_list($country_detail['basic']->country_listmanagement); ?></strong></td>
        <td class="right"><label class="text-warning">عام :</label>
        <br />
        <strong><?php echo $country_detail['basic']->budget_year; ?></strong></td>
      <td class="right"><label class="text-warning">الميزانية فتح الحساب :</label>
        <br />
        <strong><?php echo number_format($country_detail['basic']->opening_acount_budget,3); ?></strong></td>
      
      <td class="right"><label class="text-warning">تنفق مبلغ من المشاريع :</label>
        <br />
        <strong> <?php echo number_format(($this->outside->get_project_budget_sum($country_detail['basic']->register_country_id)),3);?> <strong></td>
      <td class="right"><label class="text-warning">المتبقية :</label>
        <br />
        <strong> <?php echo $remaining	=	number_format(($country_detail['basic']->opening_acount_budget	-	$this->outside->get_project_budget_sum($country_detail['basic']->register_country_id)),3);?> <strong></td>
    </tr>
    <tr>
      <td colspan="6" class="customhr">تفاصيل البنك:</td>
    </tr>
    <?php foreach($country_detail['bank_detail'] as $bank):?>
    <tr>
      <td class="right"><label class="text-warning">اسم البنك :</label>
        <br />
        <strong><?php echo $this->haya_model->get_name_from_list($bank->bankid); ?></strong></td>
      <td class="right"><label class="text-warning">الفرع :</label>
        <br />
        <strong><?php echo $this->haya_model->get_name_from_list($bank->branchid); ?></strong></td>
      <td class="right"><label class="text-warning">رقم الحساب :</label>
        <br />
        <strong><?php echo $bank->account_no; ?></strong></td>
      <td class="right"><label class="text-warning">رقم الهاتف :</label>
        <br />
        <strong><?php echo $bank->phone_no; ?><strong></td>
      <td class="right"><label class="text-warning">رقم الفاكس :</label>
        <br />
        <strong><?php echo $bank->fax_no; ?><strong></td>
      <td class="right"><label class="text-warning">البريد الإلكتروني :</label>
        <br />
        <strong><?php echo $bank->email_address; ?><strong></td>
    </tr>
    <?php endforeach;?>
        <?php if(!empty($project_budgets)):?>
        <tr>
          <td colspan="6" class="customhr">بلدان مشاريع الميزانيات:</td>
        </tr>
        <tr>
        	<th style="text-align:center">اسم المشروع</th>
            <th style="text-align:center">مجموع</th>
            <th style="text-align:center">الإنفاق المنبع</th>
            <th style="text-align:center">المتبقية</th>
            <th style="text-align:center">التفاصيل</th>
            
        </tr>
        <?php foreach($project_budgets	as $budget):?>
        <?php $total	+=	$budget->project_budget;?>
        
        <?php $spending_budget		=	$this->outside->get_total_project_type_spending_budget($budget->country_budget_id,$budget->project_type);?>
        <?php $remaining	=	($budget->project_budget	-	$spending_budget);?>
        
        <?php $total_spend		+=	$spending_budget;?>
        <?php $total_sremaining	+=	$remaining;?>
            <tr>
                <td><?php echo $this->haya_model->get_name_from_list($budget->project_type);?></td>
                <td><?php echo number_format($budget->project_budget,3);?></td>
                <td><?php echo number_format($spending_budget,3);?></td>
                <td><?php echo number_format($remaining,3);?></td>
                <td><a href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url();?>outside_help/spending_projects_budget_amount/<?php echo $budget->country_budget_id;?>/<?php echo $budget->project_type; ?>"><span> <i class="icon-search"></i> </span></a></td>

            </tr>
        <?php endforeach;?>
           <tr>
                <td class="text-warning"><strong>مجموع</strong></td>
                <td><strong><?php echo number_format($total,3);?></strong></td>
                <td><strong><?php echo number_format($total_spend,3);?></strong></td>
                <td><strong><?php echo number_format($total_sremaining,3);?></strong></td>
            </tr>
    <?php endif;?>
  </table>
</div>
<div class="col-md-12 center">
  <button type="button" class="btn" id="print" onclick="printthepage('myprint');"><i class="icon-print"></i> طباعة</button>
</div>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Outside_help extends CI_Controller 
{
//-------------------------------------------------------------------------------	
	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;
	
//-------------------------------------------------------------------------------
	/*
	* Costructor
	*/

	public function __construct()
	{
		parent::__construct();	
		
		// lOAD Models	
		$this->load->model('outside_help_model','outside');
			
		$this->_data['module']			=	$this->haya_model->get_module();			
		$this->_login_userid 			=	$this->session->userdata('userid');
		$this->_data['login_userid'] 	=	$this->_login_userid;			
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);	
	}
//-----------------------------------------------------------------------
	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
    public function index()
    {
        //$this->haya_model->check_permission($this->_data['module'],'v');

		$this->load->view('outside-help-countries',$this->_data);
    }
//-----------------------------------------------------------------------

	/*
	* Add / Edit Country Registration
	* @param $country_id
	*
	*/
	public function country_regestration($country_id)
	{
		if($this->input->post())
		{
			// Get all values from POST
			$data	=	$this->input->post();
			
			unset($data['country_listmanagement_text']); 	// UNSET value from ARRAY
			
			$country_detail['country_listmanagement']		=	$data['country_listmanagement'];
			
			if($data['opening_acount_budget'])
			{
				$country_detail['opening_acount_budget']	=	$data['opening_acount_budget'];
			}
			
			$country_detail['budget_year']					=	$data['budget_year'];
			

			if($data['register_country_id'])
			{
				// Update data into database
				$this->outside->update_country_registration($data['register_country_id'],$country_detail);
				
				$total_divs	=	count($data['bankid']);
				
				for($i	=	1;$i	<=	$total_divs;	$i++)
				{	
					$bank_detail	=	array(
										'register_country_id'	=>	$data['register_country_id'],
										'bankid'				=>	$data['bankid'][$i],
										'branchid'				=>	$data['branchid'][$i],
										'account_no'			=>	$data['account_no'][$i],
										'phone_no'				=>	$data['phone_no'][$i],
										'fax_no'				=>	$data['fax_no'][$i],
										'email_address'			=>	$data['email_address'][$i],
										'index_num'				=>	$i
										);
						
					$bank_id	=	$this->outside->bankid_exist($data['bank_id'][$i]);
					
					if($bank_id)
					{
						$this->outside->update_bank_details($bank_id,$data['register_country_id'],$bank_detail); // Add Bank Detail
					}
					else
					{
						$this->outside->add_bank_details($bank_detail); // Add Bank Detail
					}
				}
			}
			else
			{
				// ADD data into database
				$register_country_id	=	$this->outside->add_country_registration($country_detail);
				
				$total_divs	=	count($data['bankid']);
				for($i	=	1;$i	<=	$total_divs;	$i++)
				{
					$bank_detail	=	array(
										'register_country_id'	=>	$register_country_id,
										'bankid'				=>	$data['bankid'][$i],
										'branchid'				=>	$data['branchid'][$i],
										'account_no'			=>	$data['account_no'][$i],
										'phone_no'				=>	$data['phone_no'][$i],
										'fax_no'				=>	$data['fax_no'][$i],
										'email_address'			=>	$data['email_address'][$i],
										'index_num'				=>	$i
										);
			
					$this->outside->add_bank_details($bank_detail); // Add Bank Detail
				}
			}
			
			// Redirect to listing page
			/*redirect(base_url().'outside_help');
			exit();*/
			
			$this->load->library('user_agent');
			if ($this->agent->is_referral())
			{
				$url	=	 $this->agent->referrer();
			}
			
			$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
			redirect($url);
			exit();
		}
		else
		{
			if($country_id)
			{
				$this->_data['country_detail']	=	$this->outside->get_country_detail($country_id);
				
				$this->load->view('country-registration-form-edit',$this->_data);
			}
			else
			{
				$this->load->view('country-registration-form',$this->_data);
			}
		}
	}
//-----------------------------------------------------------------------
	/*
	* Add Multifields into Country Registration Form
	*
	*/
	
	function add_multifields()
	{
		$this->_data['total_divs']	=	$this->input->post('total_divs'); // Total Counts of Multi fields
		
		echo $html	=	$this->load->view('add_multifields',$this->_data,TRUE); // Multifields form
	}
	
//-----------------------------------------------------------------------
	/*
	* All Register Companies Listing
	*
	*/	
	public function ajax_countries_list()
	{
		$response		=	$this->outside->get_all_registerd_countries();	
		$permissions	=	$this->haya_model->check_other_permission(array('196'));
		
		foreach($response as $inq)
        {
			//$action  = '&nbsp;<a href="'.base_url().'outside_help/transfer_project_budgets/'.$inq->register_country_id.'"><i class="myicon icon-exchange"></i></a>';
			$action = '&nbsp;<a <a href="'.base_url().'outside_help/spending_projects_budgets/'.$inq->register_country_id.'"><i class="myicon icon-gear"></i></a>';
			$action .= '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'outside_help/get_outside_country_detail/'.$inq->register_country_id.'"><i class="icon-eye-open"></i></a>';
			$action .= '&nbsp;<a href="'.base_url().'outside_help/projects_budgets/'.$inq->register_country_id.'"><i class="icon-plus-sign-alt"></i></a>';

			//if($permissions[196]['u']	==	1)
			//{	
				$action .= '&nbsp;<a class="iconspace" href="'.base_url().'outside_help/country_regestration/'.$inq->register_country_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>'; 
			//}
			
			/*$debit		=	$this->outside->get_project_budget_sum($inq->register_country_id,'DEBIT');
			$credit		=	$this->outside->get_project_budget_sum($inq->register_country_id,'CREDIT');
			
			$total_budget	=	$debit	-	$credit;
			
			$remaining	=	($inq->opening_acount_budget	-	$total_budget);*/
			
			$remaining	=	($inq->opening_acount_budget	-	$this->outside->get_project_budget_sum($inq->register_country_id));
			
			$arr[] = array(
				"DT_RowId"					=>	$inq->register_country_id.'_durar_lm',				
                "الدولة"					=>	$this->haya_model->get_name_from_list($inq->country_listmanagement),
				"الميزانية فتح الحساب"		=>	number_format($inq->opening_acount_budget,3),
				"عام"						=>	$inq->budget_year,
				"المتبقية"					=>	number_format($remaining,3),           
                "الإجرائات"					=>	$action);
				
		 	unset($action,$english,$arabic);		 		
       }
	   
	   $ex['data'] = $arr;
	   
       echo json_encode($ex);	
	}

//-----------------------------------------------------------------------
	/*
	* Projects Types and there Budgets
	* @param $country_id	int
	*/
	
	public function projects_budgets($country_id,$project_id	=	NULL)
	{
		if($this->input->post())
		{
			// Get all values from POST
			$data			=	$this->input->post();
			
			// UNSET the value from ARRAY
			unset($data['submit']);

			foreach($data['project_type_budget']	as $key	=>	$value)
			{
				$project_budget	=	array(
									'project_type'		=>	$key,
									'project_budget'	=>	$value,
									'payment_type'		=>	$data['payment_type'], // This line is automatically removed from code
									'country_budget_id'	=>	$data['country_budget_id']);

				$record_exist	=	$this->outside->project_type_exist($key,$data['country_budget_id']);
				
				if($record_exist)
				{
					// Edit data into database
					$this->outside->edit_project_type_budget($key,$data['country_budget_id'],$project_budget);
				}
				else
				{
					// Add data into database
					$this->outside->add_project_type_budget($project_budget);
				}
				
			}
			
			// Redirect to listing page
			/*redirect(base_url().'outside_help');
			exit();*/
			
			$this->load->library('user_agent');
			if ($this->agent->is_referral())
			{
				$url	=	 $this->agent->referrer();
			}
			
			$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
			redirect($url);
			exit();
		}
		else
		{
			$this->_data['country_id']				=	$country_id;
			$this->_data['country_budget']			=	$this->outside->get_country_budget_detail($country_id);
			
			$this->_data['project_budgets']			=	$this->outside->get_project_budget_by_country_id($country_id);

			// Load Page
			$this->load->view('projects-types-and-budgets-form',$this->_data);
		}
	}
//-----------------------------------------------------------------------
	/*
	* Projects Types and there Budgets
	* @param $country_id	int
	*/
	
	public function transfer_project_budgets($country_id,$project_id	=	NULL)
	{
		if($this->input->post())
		{
			// Get all values from POST
			$data			=	$this->input->post();

			// UNSET the value from ARRAY
			unset($data['submit']);


			$credit_project_budget	=	array(
					'project_type'			=>	$data['list_id'],
					'project_budget'		=>	$data['transfer_amount'],
					'receive_project_type'	=>	$data['receive_project_type'],
					'payment_type'			=>	'CREDIT',
					'country_budget_id'		=>	$data['country_budget_id']);
				
			// Transfer amount from Type CREDIT
			$this->outside->add_project_type_budget($credit_project_budget);
			
			$debit_project_budget	=	array(
					'project_type'			=>	$data['receive_project_type'],
					'project_budget'		=>	$data['transfer_amount'],
					'payment_type'			=>	'DEBIT',
					'country_budget_id'		=>	$data['country_budget_id']);
			
			// Receive amount from Type DEBIT
			$this->outside->add_project_type_budget($debit_project_budget);

			// Redirect to listing page
			redirect(base_url().'outside_help');
			exit();
		}
		else
		{
			$this->_data['country_id']				=	$country_id;
			$this->_data['country_budget']			=	$this->outside->get_country_budget_detail($country_id);
			
			$this->_data['project_budgets']			=	$this->outside->get_project_budget_by_country_id($country_id);

			// Load Transfer Budget Form
			$this->load->view('transfer-projects-budgets-form',$this->_data);
		}
	}
//-----------------------------------------------------------------------
	/*
	*	Get Country Projects List from AJAX
	*	@param $country_id	int
	*/	
	public function get_country_projects()
	{
		$list_id	=	$this->input->post('list_id');
		$country_id	=	$this->input->post('country_id');
		
		
		$country_projects	=	$this->haya_model->get_country_project($list_id,'outside_projects_types');
		$project_budget		=	$this->outside->get_project_budget($country_id,$list_id);
		
		/*$current_balance	=	$account_numbers['balance'];
		
		$debit	=	$this->outside->total_amount_in_accounts(NULL,'DEBIT',$charity_id);
		$credit	=	$this->outside->total_amount_in_accounts(NULL,'CREDIT',$charity_id);
		
		$grand_amount	=	($current_balance	+	$debit	-	$credit);*/
		
		$html	 =	'<label class="text-warning">قسم الميزانية  : <span id="type-budget">'. ($project_budget->project_budget) .'</span></label>';
		$html	.=	'<select class="form-control req" name="receive_project_type" id="receive_project_type" placeholder="قسم اختار">';
		$html	.=	'<option value="">اختار</option>';
		
		foreach($country_projects	as $project)
		{
			/*$account_debit	=	$this->outside->total_amount_in_accounts($acc->bank_id,'DEBIT'); // Get SUM of  DEBIT
			$credit_debit	=	$this->outside->total_amount_in_accounts($acc->bank_id,'CREDIT'); // Get SUM of CREDIT
			
			$total_amount_in_account	=	($acc->amount_in_account	+	$account_debit	-	$credit_debit); // GET total SUM of Single Account
			*/
			$html	.=	'<option value="'.$project->list_id.'">'.$project->list_name.'</option>';
		}
		
		$html	.=	'</select>';

		echo $html;
	}
//-----------------------------------------------------------------------
	/*
	* Projects Types and there Budgets
	* @param $country_id	int
	*/
	
	public function spending_projects_budgets($country_id,$project_id	=	NULL)
	{
		$this->_data['country_id']				=	$country_id;
		$this->_data['country_budget']			=	$this->outside->get_country_budget_detail($country_id);
			
		$this->_data['project_budgets']			=	$this->outside->get_project_budget_by_country_id($country_id);
			
		// Load Page
		$this->load->view('projects-types-listing',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*	Outside Country Detail page
	*	@param $country_id	int
	*/	
	public function get_outside_country_detail($country_id)
	{
		$this->_data['country_detail']	=	$this->outside->get_country_detail($country_id); // GET country Detail
		
		$this->_data['project_budgets']	=	$this->outside->get_country_project_budget($country_id);
		
		$this->load->view('country-detail-page',$this->_data);	
	}
//-----------------------------------------------------------------------
	/*
	*	Outside Country Detail page
	*	@param $country_id	int
	*/	
	public function add_type_budget($country_id	=	NULL,$project_type_id	=	NULL)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			$project_type_budget	=	array(
				'country_budget_id'			=>	$data['country_budget_id'],
				'project_type'				=>	$data['project_type_id'],
				'user_id'					=>	$this->_login_userid,
				'cat_id'					=>	$data['cat_id'],
				'companyid'					=>	$data['companyid'],
				'spending_project_budget'	=>	$data['spending_project_budget']);
			
			$response	=	$this->outside->add_type_budget($project_type_budget);
			
			echo $url	=	base_url().'outside_help/spending_projects_budgets/'.$data['country_budget_id'];
			
		}
		else
		{
			$this->_data['country_id']			=	$country_id;
			$this->_data['project_type_id']		=	$project_type_id;
			
			$this->_data['project_type_budget']	=	$this->outside->get_project_type_budget($country_id,$project_type_id);
			$this->_data['spending_budget']		=	$this->outside->get_total_project_type_spending_budget($country_id,$project_type_id);
			$this->_data['categories']			=	$this->outside->get_project_type_categories($country_id,$project_type_id);	// Get Project Type Categories 
			
			$country_detail				=	$this->outside->get_country_budget_detail($country_id);
			
			$this->_data['companies']	=	$this->outside->get_companies_by_country($country_detail->country_listmanagement);	// Get Companies By their countries

			$this->load->view('add-type-budget',$this->_data);
		}
	}
//-----------------------------------------------------------------------
	/*
	*	Spend Project Type Budget
	*	@param $country_id	int
	*	@param $project_type	int
	*/	
	public function spending_projects_budget_amount($country_id,$project_type	=	NULL)
	{
		$this->_data['project_type']		=	$project_type;
		$this->_data['country_detail']		=	$this->outside->get_country_detail($country_id); // GET country Detail
		
		$this->_data['project_type_budget']	=	$this->outside->get_project_type_budget($country_id,$project_type);
		$this->_data['spending_budget']		=	$this->outside->get_total_project_type_spending_budget($country_id,$project_type);
			
		$this->_data['defined_cat_budgets']	=	$this->outside->sum_of_project_cat_budget(); // Get SUM of all categories Budgets

		$this->_data['spending_project_budgets']	=	$this->outside->get_project_type_spending_budget($country_id,$project_type);

		$this->load->view('spending-amount-details',$this->_data);	
	}
//-----------------------------------------------------------------------
	/*
	*	Spend Project Type Budget
	*	@param $country_id	int
	*	@param $project_type	int
	*/	
	public function single_cat_transactions($cat_id)
	{
		$this->_data['defined_cat_budgets']			=	$this->outside->sum_of_project_cat_budget(); // Get SUM of all categories Budgets
		$this->_data['spending_project_budgets']	=	$this->outside->get_cat_spending_budget($cat_id);

		$this->load->view('cat-spending-amount-details',$this->_data);	
	}

//-----------------------------------------------------------------------
	/*
	*	Add Project type Category
	*	@param $country_id	int
	*	@param $project_type_id	int
	*/	
	public function add_project_type_category($country_id	=	NULL,$project_type_id	=	NULL,$cat_id	=	NULL)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($data['cat_id'])
			{
				$project_type_budget	=	array(
					'cat_name'			=>	$data['cat_name'],
					'cat_budget'		=>	$data['cat_budget'],
					'detail'			=>	$data['detail']);
				
				$response	=	$this->outside->update_project_type_category($data['cat_id'],$project_type_budget);
				
				echo $url	=	base_url().'outside_help/spending_projects_budgets/'.$data['country_budget_id'];
				
			}
			else
			{
				$project_type_budget	=	array(
					'country_id'		=>	$data['country_budget_id'],
					'project_type_id'	=>	$data['project_type_id'],
					'user_id'			=>	$this->_login_userid,
					'cat_name'			=>	$data['cat_name'],
					'cat_budget'		=>	$data['cat_budget'],
					'detail'			=>	$data['detail']);
				
				$response	=	$this->outside->add_project_type_category($project_type_budget);
				
				echo $url	=	base_url().'outside_help/spending_projects_budgets/'.$data['country_budget_id'];
			}
			

			
		}
		else
		{
			if($cat_id)
			{
				$this->_data['cat_data']			=	$this->outside->get_category_data($cat_id);
				$this->_data['country_id']			=	$this->_data['cat_data']->country_id;
				$this->_data['project_type_id']		=	$this->_data['cat_data']->project_type_id;
				$this->_data['project_type_budget']	=	$this->outside->get_project_type_budget($this->_data['cat_data']->country_id,$this->_data['cat_data']->project_type_id);
				$this->_data['spending_budget']		=	$this->outside->get_total_project_type_spending_budget($this->_data['cat_data']->country_id,$this->_data['cat_data']->project_type_id);
			
			}
			else
			{
				$this->_data['country_id']			=	$country_id;
				$this->_data['project_type_id']		=	$project_type_id;
				$this->_data['project_type_budget']	=	$this->outside->get_project_type_budget($country_id,$project_type_id);
				$this->_data['spending_budget']		=	$this->outside->get_total_project_type_spending_budget($country_id,$project_type_id);
			
			}
			
			$this->_data['defined_cat_budgets']		=	$this->outside->sum_of_project_cat_budget(); // Get SUM of all categories Budgets

			$this->load->view('add-project-type-category',$this->_data);
		}
	}
//-----------------------------------------------------------------------
	/*
	*	Add Project type Category
	*	@param $country_id	int
	*	@param $project_type_id	int
	*/	
	public function project_type_categories($country_id,$project_type_id)
	{
		$this->_data['project_type_id']	=	$project_type_id;
		$this->_data['all_categories']	=	$this->outside->get_project_type_categories($country_id,$project_type_id);
		
		// load View
		$this->load->view('project-type-categories',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*	Get Project Type Category Budget
	*/	
	public function get_project_category_budget()
	{
		$cat_id			=	$this->input->post('cat_id');
		$cat_budget		=	$this->outside->get_project_category_budget($cat_id); // Get Project category Budget
		$spend_budget	=	$this->outside->spend_from_project_category_budget($cat_id); // Get Project category Budget
		
		$remaining		=	($cat_budget	-	$spend_budget);
		
		$response	=	array('msg'	=>	' الميزانية المخصصة لهذا المشروع ' .number_format($remaining,3). '','remaining'	=>	$remaining);
		
		echo json_encode($response);
	}
//-----------------------------------------------------------------------
	/*
	*	Delete Project Type Category
	*	@param $cat_id int
	*/	
	public function delete_project_type_category($cat_id)
	{
		$this->outside->delete_project_type_category($cat_id);
		
		echo '1';
	}
//-----------------------------------------------------------------------
	/*
	*	Delete Customer
	*/	
	public function delete_customer($customer_id)
	{
		$this->company->delete_customer($customer_id);
		
		redirect(base_url().'company/customers');
		exit();
	}
//-----------------------------------------------------------------------

	/*
	*	File Uploading
	*	@param $userid integer
	*	@param $filefield integer Input File name
	*	@param $folder integer older name where Image Upload
	*	@param $width integer
	*	@param $height integer
	*/
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';
		}
		else
		{
			$path = './'.$folder.'/';	
		}
				
		if (!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Sending Email Function
	* @param $to string
	* @param $from string
	* @param $subject string
	* @param $message string
	* @param $path string
	*
	*/
	public function send_email($to	=	NULL,$from	=	NULL,$subject	=	NULL,$message = NULL,$path	=	NULL) 
	{
		$config = Array(
			'protocol' 	=>	'smtp',
			'smtp_host' =>	SMTPHOST,
			'smtp_port' =>	465,
			'smtp_user'	=>	SMTPEMAIL,
			'smtp_pass'	=>	SMTPPASSWORD,
			'mailtype'	=>	'html',
			'charset'	=>	'utf-8',
			'wordwrap' 	=>	TRUE
			);
		
		$this->load->library('email', $config);

		$this->email->clear(true);
		$this->email->set_newline("\r\n");
		$this->email->from(SMTPEMAIL,'Al-Haya');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		
		if($path)
		{
		 	$this->email->attach($path);
		}
		
		// Send Email
		$this->email->send();
	}
//-----------------------------------------------------------------------

	/*
	*	Download File
	*	@param $userid integer
	*	@param $file_name string
	*/
	
	public function download_file($userid,$file_name)
	{
		$path	= 'resources/users/'.$userid.'/'.$file_name;

		// Download File
		downloadFile($path,$file_name);
	}
//------------------------------------------------------------------------

  	/**
   	* Dynamic Forms Listing Page
   	* @param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]	=	$this->haya_model->get_all_custom_form($moduleid);
			
		$this->_data["userid"]	=	$this->_login_userid;
		
		$this->_data['formid']	=	$moduleid;
		
		// Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);	 
	 }
//------------------------------------------------------------------------
}
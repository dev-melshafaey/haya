<div id="user-profile">
  <div class="row" dir="rtl">
    <div class="col-md-12 fox leftborder">
      <h4 class="panel-title customhr">متجر التفاصيل</h4>
      <br clear="all" />
      <?php if(!empty($store_detail)):?>
      <div class="col-md-12 form-group">
        <table width="50%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr align="center">
            <td><strong>مخزن معرف</strong></td>
            <td><?php echo arabic_date($store_detail->store_id);?></td>
          </tr>
          <tr align="center">
            <td><strong>المخزن</strong></td>
            <td><?php echo $store_detail->store_name;?>&nbsp;,<?php echo $this->haya_model->get_name_from_list($store_detail->country_id);?>&nbsp;,<?php echo $this->haya_model->get_name_from_list($store_detail->city_id);?></td>
          </tr>
<!--          <tr align="center">
            <td><strong>االدولة</strong></td>
            <td><?php echo $this->haya_model->get_name_from_list($store_detail->country_id);?></td>
          </tr>
          <tr align="center">
            <td><strong>المدينة</strong></td>
            <td><?php echo $this->haya_model->get_name_from_list($store_detail->city_id);?></td>
          </tr>-->
        </table>
        <br />
        <h4 class="panel-title customhr">البند التفاصيل</h4>
       
        <table class="table table-bordered table-striped">
          <thead>
            <tr role="row">
              <th style="text-align:center;">رقم</th>
              <th style="text-align:center;">فئة</th>
              <th style="text-align:center;">الفئة الفرعية</th>
              <th style="text-align:center;">اسم العنصر</th>
              <th style="text-align:center;">الكمية</th>
            </tr>
          </thead>
          <tbody role="alert" aria-live="polite" aria-relevant="all">
            <tr role="row">
              <td style="text-align:center;"><?php echo arabic_date($item->itemid);?></td>
              <td style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($item->list_category);?></td>
              <td style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($item->list_subcategory);?></td>
              <td style="text-align:center;"><?php echo $item->itemname;?></td>
              <td style="text-align:center;"><?php echo $this->haya_model->dataCount('ah_inventory_qty','inventoryid',$item->itemid,'SUM','quantity');?></td>
            </tr>
          </tbody>
        </table>
         <h4 class="panel-title customhr">تفاصيل التوزيع</h4>
         
         <table class="table table-bordered table-striped">
         <thead>
            <tr role="row">
              <th style="text-align:center;">رقم</th>
              <th style="text-align:center;">اسم العنصر</th>
              <th style="text-align:center;">تاريخ الموافقة</th>
              <th style="text-align:center;">تمت الموافقة عليه من قبل</th>
              <th style="text-align:center;">الكمية</th>
            </tr>
          </thead>
           <tbody role="alert" aria-live="polite" aria-relevant="all">
           <?php
		    $this->db->select('*');
			$this->db->from('ah_store_item_request_list');
			$this->db->join('ah_store_item_request',"ah_store_item_request_list.stItmReqId = ah_store_item_request.stItmReqId",'inner');						
			$this->db->where("ah_store_item_request_list.delete_record",'0');
			$this->db->where("ah_store_item_request_list.items",$item->itemid);
			$this->db->where("ah_store_item_request.status",'Accepted');
			$this->db->order_by('ah_store_item_request.appDate','ASC');
			$query = $this->db->get();
			$cnt =1;
			$user ='';
			$total =0;
		   	foreach($query->result() as $rows){
				$this->db->select('ah_userprofile.fullname');
				$this->db->from('ah_userprofile');	
				$this->db->where("ah_userprofile.userid",$rows->approvedId);
				$rowuser = $this->db->get(); 
				foreach($rowuser->result() as $us1){$user = $us1->fullname;}
				$total = $total + $rows->qty;
			?>
            <tr role="row">
              <td style="text-align:center;"><?php echo $cnt;?></td>
              <td style="text-align:center;"><?php echo $item->itemname;?></td>
              <td style="text-align:center;"><?php if( arabic_date($rows->appDate) != '0000-00-00'){echo arabic_date($rows->appDate);}else{echo "N/A";}?></td>
           	  <td style="text-align:center;"><?php echo $user;?></td>
              <td style="text-align:center;"><?php echo $rows->qty;?></td>
            </tr>    
            <?php
			$cnt++;    
			}
		    
		   ?>
           <tr role="row">
            	<td style="text-align:center;">&nbsp;</td>
                <td style="text-align:center;">&nbsp;</td>
                <td style="text-align:center;">&nbsp;</td>
                <td style="text-align:center;">&nbsp;</td>
              <td style="text-align:center; color:#F00;" >إجمالي الإنفاق: <?php echo $total;?></td>
            </tr> 
            
          </tbody>
        </table>
        
         <h4 class="panel-title customhr">التاريخ</h4>
       
        <table class="table table-bordered table-striped">
          <thead>
            <tr role="row">
              <th style="text-align:center;">رقم</th>
              <th style="text-align:center;">اسم العنصر</th>
              <th style="text-align:center;">تمت إضافته بواسطة</th>
              <th style="text-align:center;">تاريخ </th>
              <th style="text-align:center;">الكمية</th>
              <th style="text-align:center;">ملاحظة</th>
            </tr>
          </thead>
          <tbody role="alert" aria-live="polite" aria-relevant="all">
          <?php
		  	  $this->db->select('*');
			$this->db->from('ah_inventory_history');
			$this->db->where("itemid",$itemid);
			$this->db->order_by('currentdate','ASC');
			$query = $this->db->get();
			$cnt =1;
			$user ='';
			$itemname ='';
		   	foreach($query->result() as $rows){
				$this->db->select('itemname');
				$this->db->from('ah_inventory');	
				$this->db->where("itemid",$rows->itemid);
				$rowuser = $this->db->get(); 
				foreach($rowuser->result() as $thm){$itemname = $thm->itemname;}
			
			$this->db->select('ah_userprofile.fullname');
				$this->db->from('ah_userprofile');	
				$this->db->where("ah_userprofile.userid",$rows->addedby);
				$rowuser = $this->db->get(); 
				foreach($rowuser->result() as $us1){$user = $us1->fullname;}
			
				
				?>
		   <tr role="row">
              <td style="text-align:center;"><?php echo $cnt;?></td>
          	  <td style="text-align:center;"><?php echo $itemname;?></td>
              <td style="text-align:center;"><?php echo $user;?></td>
              <td style="text-align:center;"><?php echo arabic_date($rows->currentdate);?></td>
              <td style="text-align:center;"><?php echo $rows->qty;?></td>
              <td style="text-align:center;"><?php echo $rows->remark;?></td>
            </tr>
		  
		  <?php		
			}
		  ?>
           
          </tbody>
        </table>
      </div>
      <?php endif;?>
    </div>
  </div>
</div>
</div>
</div>
<div class="row" style="text-align: center;">
  <button type="button" id="" onClick="printthepage('user-profile');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
</div>

<div id="user-profile">
  <div class="row" dir="rtl">
    <div class="col-md-12 fox leftborder">
      <h4 class="panel-title customhr">متجر التفاصيل</h4>
      <br clear="all" />
      <?php if(!empty($store_detail)):?>
      <div class="col-md-12 form-group">
        <table width="50%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr align="center">
            <td><strong>مخزن معرف</strong></td>
            <td><?php echo arabic_date($store_detail->store_id);?></td>
          </tr>
          <tr align="center">
            <td><strong>المخزن</strong></td>
            <td><?php echo $store_detail->store_name;?>&nbsp;,<?php echo $this->haya_model->get_name_from_list($store_detail->country_id);?>&nbsp;,<?php echo $this->haya_model->get_name_from_list($store_detail->city_id);?></td>
          </tr>
<!--          <tr align="center">
            <td><strong>االدولة</strong></td>
            <td><?php echo $this->haya_model->get_name_from_list($store_detail->country_id);?></td>
          </tr>
          <tr align="center">
            <td><strong>المدينة</strong></td>
            <td><?php echo $this->haya_model->get_name_from_list($store_detail->city_id);?></td>
          </tr>-->
        </table>
        <br />
        <h4 class="panel-title customhr">البند التفاصيل</h4>
        <table class="table table-bordered table-striped">
          <thead>
            <tr role="row">
              <th style="text-align:center;">رقم</th>
              <th style="text-align:center;">فئة</th>
              <th style="text-align:center;">الفئة الفرعية</th>
              <th style="text-align:center;">اسم العنصر</th>
              <th style="text-align:center;">الكمية</th>
            </tr>
          </thead>
          <tbody role="alert" aria-live="polite" aria-relevant="all">
            <tr role="row">
              <td style="text-align:center;"><?php echo arabic_date($item->itemid);?></td>
              <td style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($item->list_category);?></td>
              <td style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($item->list_subcategory);?></td>
              <td style="text-align:center;"><?php echo $item->itemname;?></td>
              <td style="text-align:center;"><?php echo $this->haya_model->dataCount('ah_inventory_qty','inventoryid',$item->itemid,'SUM','quantity');?></td>
            </tr>
          </tbody>
        </table>
      </div>
      <?php endif;?>
    </div>
  </div>
</div>
</div>
</div>
<div class="row" style="text-align: center;">
  <button type="button" id="" onClick="printthepage('user-profile');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
</div>

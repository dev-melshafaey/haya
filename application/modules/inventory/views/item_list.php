<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row">
				  <?php echo add_button('inventory/additem','إضافة',$permissions[$module['moduleid']]['a']	==	1); ?>
                  <!--<div class="form-group col-md-12" align="right">
                    	<h4>رقم&nbsp;&nbsp;:&nbsp;<?php //echo arabic_date($store_detail->store_id);?></h4>
                        <h4>المخزن&nbsp;&nbsp;:&nbsp;<?php //echo $store_detail->store_name;?></h4>
                        <h4>الدولة&nbsp;&nbsp;:&nbsp;<?php //echo $this->haya_model->get_name_from_list($store_detail->country_id);?></h4>
                        <h4>المدينة&nbsp;&nbsp;:&nbsp;<?php //echo $this->haya_model->get_name_from_list($store_detail->city_id);?></h4>
                    </div>-->
                    
                
                
               <!-- /*------------------------
   				 Reeja Soni 22-5-17 start*/
                -->
                
                 <?php foreach($all_stores as $store):?>
                 <a style="cursor:pointer; color:#029625; font-size:16px; font-weight:bold; margin-left:30px;" href="<?php echo base_url()?>inventory/allitems/<?php echo $store->store_id;?>"><?php echo $store->store_name;?></a>
                  <?php endforeach;?>
                 
                
                 <!-- /*------------------------
   				 Reeja Soni 22-5-17 end*/
                -->
                
                
              
                  </div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th>رقم</th>
                        <th>مخزن</th>
                        <th>فئة</th>
                        <th>الفئة الفرعية</th>
                        <th>اسم العنصر</th>
                        <th>باركود</th>
                        <th>الكمية</th>                     
                        <th>الإجرائات </th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array(
										'ajax_url'		=>	base_url().'inventory/ajax_allitems/'.$store_id,
										'columns_array'	=>	'
														{ "data": "رقم" },
														{ "data": "مخزن" },
														{ "data": "فئة" },
														{ "data": "الفئة الفرعية" },
														{ "data": "اسم العنصر" },
														{"data": "باركود" },
														{ "data": "الكمية" },
														{ "data": "الإجرائات" }
														')); ?>
</div>
</body>
</html>
<script>
function addstore(store){
	window.location.href="<?php echo base_url();?>inventory/allitems/"+store;
}
</script>
<?php $labels	=	$this->config->item('list_types');?>

<div class="row col-md-12">
  <form action="" method="POST" id="save_data_form2" name="save_data_form2">
    <input type="hidden" id="list_type" name="list_type" value="category">
    <div class="form-group col-md-6">
      <label for="basic-input">إسم</label>
      <input type="text" class="form-control req NumberInput" name="list_name"  id="list_name"  placeholder="اسم القائمة"/>
    </div>
    <div class="form-group col-md-3">
      <label for="basic-input">ترتيب&lrm;‎</label>
      <input type="text" name="list_order" id="list_order" class="form-control" />
    </div>
    <input type="hidden" name="list_id" id="list_id"/>
  </form>
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="submit_form();" value="حفظ" />
  </div>
</div>

<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block" style="padding: 10px 10px;">
          <form method="POST" id="form_add_item" name="form_add_item" enctype="multipart/form-data">
            <input type="hidden" name="store_id" id="store_id" value="<?php echo $store_detail->store_id; ?>">
            <input type="hidden" name="olddocument" id="olddocument" value="<?php echo $store_detail->document; ?>">
            <div class="form-group col-md-6">
              <label for="basic-input"><strong>اسم العنصر:</strong></label>
              <input type="text" class="form-control req" value="<?php echo $store_detail->store_name; ?>" placeholder="اسم العنصر" name="store_name" id="store_name" />
            </div>
            <div class="form-group col-md-4">
                <label class="text-warning">جميع المستخدمين</label>
                <select name="take_care_userid" id="take_care_userid" class="search-select form-control required" placeholder="جميع المستخدمين">
                  <option value="">جميع المستخدمين</option>
                  <?php foreach($all_users as $user):?>
                  <option value="<?php echo $user->userid;?>" <?php if(isset($store_detail->take_care_userid) AND $store_detail->take_care_userid	==	$user->userid):?> selected="selected" <?php endif;?>><?php echo $user->fullname;?></option>
                  <?php endforeach;?>
                </select>
              </div>
              <div class="form-group col-md-6">
                <label class="text-warning">الدولة</label>
                <select class="form-control " id="country_id" name="country_id" onChange="loadprovince(this.value,'city_id');" >
                      <option value="">--تحديد--</option>
					  <?php 
                      if(count($issuecountry)>0){
                          foreach($issuecountry as $type){
                             if($rows->country == $type->list_id)
                              {
                                  echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
                              }
                              else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
                              }
                              
                          }
                      }
                      ?>
                      </select>
              </div>
              <div class="form-group col-md-6">
                <label class="text-warning">حافظة / البلد :</label>
                <select class="form-control " id="city_id" name="city_id"  >
               <?php 
                      if(count($province)>0){
                          foreach($province as $type){
                             if( array_search( $rows->province,$type->list_id))
                              {
                                  echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
                              }
                              else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
                              }
                              
                          }
                      }
                      ?>
              </select>
              </div>
            <div class="form-group col-md-6">
              <label for="basic-input"><strong>صورة:</strong></label>
              <input type="file" accept="image/*" id="document" name="document">
              <?php if($store_detail->document!='') { ?>
              <a class="fancybox-button" rel="gallery1" href="<?PHP echo base_url(); ?>resources/stores/<?php echo $store_detail->document; ?>"><i class="icon-eye-open"></i></a>
              <?php } ?>
            </div>
            <br clear="all">
            <div class="form-group col-md-6">
              <label for="basic-input"><strong>تفاصيل العنصر:</strong></label>
              <textarea name="description" placeholder="تفاصيل العنصر" class="form-control req" style="resize:none; height:300px;" id="description"><?php echo $store_detail->description; ?></textarea>
            </div>
            <div class="form-group col-md-6">
              <label for="basic-input"><strong>تفاصيل العنصر:</strong></label>
              <textarea name="address" placeholder="تفاصيل العنصر" class="form-control req" style="resize:none; height:300px;" id="address"><?php echo $store_detail->address; ?></textarea>
            </div>
            <div class="form-group col-md-6">
              <button type="button" id="save_item" name="save_item" class="btn btn-success">حفظ</button>
            </div>
          </form>
          <br clear="all">
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
 $(document).ready(function(){
	$(".search-select").searchable({
	maxListSize: 100,	// if list size are less than maxListSize, show them all
	maxMultiMatch: 50,	// how many matching entries should be displayed
	exactMatch: false,	// Exact matching on search
	wildcards: true,	// Support for wildcard characters (*, ?)
	ignoreCase: true,	// Ignore case sensitivity
	latency: 200,	// how many millis to wait until starting search
	warnMultiMatch: 'top {0} matches ...',	// string to append to a list of entries cut short by maxMultiMatch 
	warnNoMatch: 'no matches ...',	// string to show in the list when no entries match
	zIndex: 'auto'	// zIndex for elements generated by this plugin
	  });
});
function loadprovince(vals,id){
	$('#'+id).empty();
	$('#'+id).append('<option value="">--تحديد--</option>');
	$.ajax({
		url: config.BASE_URL+'aid/loadprovince/',
		type: "POST",
		data:{'vals':vals },
		dataType: "json",
		success: function(data)
		{	
			var al =data.province;
			for(var i=0; i<al.length; i++)
			{
				$('#'+id).append('<option value="'+al[i].list_id+'">'+al[i].list_name+'</option>');
				
			}
		}
	});
}
</script>
</div>
</body>
</html>
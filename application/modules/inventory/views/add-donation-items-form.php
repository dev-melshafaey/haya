<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block" style="padding: 10px 10px;">
          <form method="POST" action="<?php echo base_url().'inventory/add_donation_items'?>" id="add-donation-items-form" name="add-donation-form" enctype="multipart/form-data">
            <input type="hidden" name="donation_id" value="<?php echo $donation_id;?>" />
	  	<div class="form-group col-md-4">
              <label class="text-warning">الفئة:</label>
              <?PHP echo $this->haya_model->create_dropbox_list('list_category','category',$item->list_category,0,'req'); ?> </div>
            <div class="form-group col-md-4">
              <label class="text-warning">الفئة الفرعية:</label>
              <?PHP echo $this->haya_model->create_dropbox_list('list_subcategory','subcategory',$item->list_subcategory,$item->list_category,'req'); ?> </div>
            <div class="form-group col-md-4">
              <label class="text-warning">العناصر</label>
              <select id="itemid" name="itemid" class="form-control req">
                <option value="">واختيار عناصر</option>
              </select>
            </div>
           
            <div class="col-md-4 form-group">
              <label class="text-warning">الكمية</label>
              <input type="text" class="form-control req" name="total_items"  id="total_items"  placeholder="الكمية" onkeyup="addTotal(),only_numeric(this);" />
            </div>
           
            <div class="form-group col-md-4">
              <label class="text-warning">السعر:</label>
              <input type="text" class="form-control req" value="0"  placeholder="السعر" name="price" id="price" onkeyup="addTotal(),only_numeric(this);" />
            </div>
           <div class="form-group col-md-4">
              <label class="text-warning">القيمة الاجمالية:</label>
              <input type="text" class="form-control req"  placeholder="القيمة الاجمالية" name="total" id="total" readonly="readonly" value="0" />
            </div>
            <!--<div class="form-group col-md-4">
              <label class="text-warning">إيصال:</label>
              <input type="file" accept="image/*" id="donor_receipt" name="donor_receipt">
              <?PHP if($item->itemphoto!='') { ?>
              <a class="fancybox-button" rel="gallery1" href="<?PHP echo base_url(); ?>resources/items/<?PHP echo $item->itemphoto; ?>"><i class="icon-eye-open"></i></a>
              <?PHP } ?>
            </div>-->
            <br clear="all">
            <div class="form-group col-md-6">
              <label class="text-warning">وصف:</label>
              <textarea name="description" placeholder="وصف" class="form-control req" style="resize:none; height:200px;" id="description"><?PHP echo $item->description; ?></textarea>
            </div>
            <div class="form-group col-md-6">
              <label class="text-warning">ملاحظات:</label>
              <textarea name="notes" placeholder="ملاحظات" class="form-control req" style="resize:none; height:200px;" id="notes"><?PHP echo $item->notes; ?></textarea>
            </div> 
            <div class="form-group col-md-4">
              <button style="margin-top: 25PX; !important" type="button" id="save_donation_items" name="save_donation_items" class="btn btn-success">حفظ</button>
            </div>
          </form>
          <br clear="all">
        </div>
         <table class="table table-bordered table-striped dataTable" id="item-atable" >
        <thead>
          <tr role="row">
            <th style="text-align:center;">العناصر</th>
            <th style="text-align:center;">السعر</th>
            <th style="text-align:center;">كمية</th>
            <th style="text-align:center;">الإجرائات</th>
          </tr>
        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
          <?PHP foreach($donation_items as $item): ?>
          <tr role="row" id="row-<?php echo $item->d_itemid;?>">
            <td style="text-align:center;"><?php echo $this->inventory->get_item_name($item->itemid);?></td>
            <td style="text-align:center;"><?php echo $item->price;?></td>
            <td style="text-align:center;"><?php echo $item->total_items;?></td>
            <td style="text-align:center;"><a href="#_" onClick="delete_d_item('<?php echo $item->d_itemid;?>');"><i style="color:#CC0000;" class="icon-remove-sign"></i></a></td>
          </tr>
          <?PHP endforeach; ?>
        </tbody>
      </table>
      <br clear="all" /> 
      </div>
  </div>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		/*yearRange: "-80:+0",*/
		yearRange: new Date().getFullYear() + ':' + new Date().getFullYear(),
		dateFormat:'yy-mm-dd',
		});
		
	$('#save_donation_items').click(function(){
		$('#add-donation-items-form .req').removeClass('parsley-error');
		var ht = '<ul>';
		$('#add-donation-items-form .req').each(function(index, element) {
			if($(this).val()=='')
			{
				$(this).addClass('parsley-error');
				ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
			}
		});
		var redline = $('#add-donation-items-form .parsley-error').length;
		ht += '</ul>';
		if(redline <= 0)
		{
			var data	=	$('#add-donation-items-form').serialize();
			
			$.ajax({
					  url: config.BASE_URL + 'inventory/add_donation_items',
					  type: "POST",
					  data: data,
					  dataType: "html",
					  success: function(row)
					  {
						$("#item-atable tbody").append(row);
					  }
					});
		}
		else
		{
			show_notification_error_end(ht);
		}
		
	});
});

function select_item() 
{
	var cat_id		=	$("#list_category").val();
	var sub_cat_id	=	$("#list_subcategory").val();
	
	$.ajax({
			url: '<?php echo base_url();?>inventory/get_items',
			type: "POST",
			data:'cat_id='+cat_id+'&sub_cat_id='+sub_cat_id,
			dataType: "html",
			success: function(msg)
			{
				$("#itemid").html(msg);
			}
			});
}
function delete_d_item(d_itemid) 
{
	$.ajax({
			url: '<?php echo base_url();?>inventory/delete_d_item',
			type: "POST",
			data:'d_itemid='+d_itemid,
			dataType: "html",
			success: function(msg)
			{
				$('#row-'+d_itemid).remove();
			}
			});
}

</script>
 <script type="text/javascript">
			function addTotal(){
				var total_items =$('#total_items').val();
				var price =$('#price').val();
				var total = parseInt(total_items) * parseInt(price);
				$('#total').val(total);
			}
			</script>
</div>
</body>
</html>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block" style="padding: 10px 10px;">
          <form method="POST" action="<?php echo base_url().'inventory/add_donation'?>" id="add-donation-form" name="add-donation-form" enctype="multipart/form-data">
          	<input type="hidden" name="user_type" value="DONOR" />
            <input type="hidden" name="login_user_id" value="<?php echo $login_userid;?>" />
            <input type="hidden" name="donation_id" value="<?php echo $donation_id;?>" />
            <div class="form-group col-md-4">
              <label class="text-warning">اسم المتبرع:</label>
              <input type="text" class="form-control req" placeholder="اسم المتبرع" name="user_name" id="user_name" value="<?php echo $user_name;?>"/>
            </div>
            <div class="form-group col-md-4">
              <label class="text-warning">رقم البطاقة:</label>
              <input type="text" class="form-control req" placeholder="رقم البطاقة" name="card_number" id="card_number" value="<?php echo $card_number;?>"/>
            </div>
	<!--  	<div class="form-group col-md-4">
              <label class="text-warning">الفئة:</label>
              <?PHP echo $this->haya_model->create_dropbox_list('list_category','category',$item->list_category,0,'req'); ?> </div>
            <div class="form-group col-md-4">
              <label class="text-warning">الفئة الفرعية:</label>
              <?PHP echo $this->haya_model->create_dropbox_list('list_subcategory','subcategory',$item->list_subcategory,$item->list_category,'req'); ?> </div>
            <div class="form-group col-md-4">
              <label class="text-warning">العناصر</label>
              <select id="itemid" name="itemid" class="form-control req">
                <option value="">واختيار عناصر</option>
              </select>
            </div>
            <div class="form-group col-md-4">
              <label class="text-warning">السعر:</label>
              <input type="text" class="form-control req" value="" placeholder="السعر" name="price" id="price" />
            </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">كمية</label>
              <input type="text" class="form-control req" name="quantity"  id="quantity"  placeholder="كمية" value=""/>
            </div>
            <div class="col-md-4 form-group">
              <label class="text-warning">تاريخ</label>
              <input type="text" class="datepicker form-control req" name="submited_date"  id="submited_date"  placeholder="تاريخ" value=""/>
            </div>
            <div class="form-group col-md-4">
              <label class="text-warning">إيصال:</label>
              <input type="file" accept="image/*" id="donor_receipt" name="donor_receipt">
              <?PHP if($item->itemphoto!='') { ?>
              <a class="fancybox-button" rel="gallery1" href="<?PHP echo base_url(); ?>resources/items/<?PHP echo $item->itemphoto; ?>"><i class="icon-eye-open"></i></a>
              <?PHP } ?>
            </div>
            <br clear="all">
            <div class="form-group col-md-6">
              <label class="text-warning">وصف:</label>
              <textarea name="itemdescription" placeholder="وصف" class="form-control req" style="resize:none; height:300px;" id="itemdescription"><?PHP echo $item->itemdescription; ?></textarea>
            </div>
            <div class="form-group col-md-6">
              <label class="text-warning">ملاحظات:</label>
              <textarea name="itemdescription" placeholder="ملاحظات" class="form-control req" style="resize:none; height:300px;" id="itemdescription"><?PHP echo $item->itemdescription; ?></textarea>
            </div> -->
            <div class="form-group col-md-4">
              <button style="margin-top: 25PX; !important" type="button" id="save_donation" name="save_donation" class="btn btn-success">حفظ</button>
            </div>
          </form>
          <br clear="all">
        </div>
       <!--  <table class="table table-bordered table-striped dataTable" id="ajaxTable" >
        <thead>
          <tr role="row">
            <th style="text-align:center;">الحالات/أفضلية</th>
            <th style="text-align:center;">تاريخ موعد</th>
            <th style="text-align:center;">التفاصيل موعد</th>
            <th style="text-align:center;">ملاحظات</th>
          </tr>
        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
          <?PHP //foreach($appointments as $apn) { ?>
          <tr role="row">
            <td style="text-align:center;">AA</td>
            <td style="text-align:center;">BB</td>
            <td style="text-align:center;">CC</td>
            <td style="text-align:center;">DD</td>
          </tr>
          <?PHP //} ?>
        </tbody>
      </table>
      <br clear="all" /> -->
      </div>
  </div>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		/*yearRange: "-80:+0",*/
		yearRange: new Date().getFullYear() + ':' + new Date().getFullYear(),
		dateFormat:'yy-mm-dd',
		});
		
	$('#save_donation').click(function(){
		$('#add-donation-form .req').removeClass('parsley-error');
		var ht = '<ul>';
		$('#add-donation-form .req').each(function(index, element) {
			if($(this).val()=='')
			{
				$(this).addClass('parsley-error');
				ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
			}
		});
		var redline = $('#add-donation-form .parsley-error').length;
		ht += '</ul>';
		if(redline <= 0)
		{
			/*var data	=	$('#add-donation-form').serialize();
			
			$.ajax({
					  url: config.BASE_URL + 'inventory/add_donation',
					  type: "POST",
					  data: data,
					  dataType: "html",
					  success: function(msg)
					  {
						 console.log(msg);
					  }
					});*/
					
				$('#add-donation-form').submit();
		}
		else
		{
			show_notification_error_end(ht);
		}
		
	});
});

function select_item() 
{
	var cat_id		=	$("#list_category").val();
	var sub_cat_id	=	$("#list_subcategory").val();
	
	$.ajax({
			url: '<?php echo base_url();?>inventory/get_items',
			type: "POST",
			data:'cat_id='+cat_id+'&sub_cat_id='+sub_cat_id,
			dataType: "html",
			success: function(msg)
			{
				$("#itemid").html(msg);
			}
			});
}
</script>
</div>
</body>
</html>
<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"><?PHP echo add_button('inventory/additem','إضافة',$permissions[$module['moduleid']]['a']	==	1); ?></div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th>الكل</th>
                        <th>صوره المنتج</th>
                        <th>اسم المنتج</th>
                        <th>الصنف</th>
                        <th>النوع</th>
                        <th>الي مخزن</th>
                        <th>من مخزن</th>
                        <th>الكمية المحولة</th>
                        <th>السيريال</th>
                        <th>تاريخ الانشاء</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'inventory/ajax_complete_transfer/','columns_array'=>'{ "data": "الكل" },
                { "data": "فئة" },
                { "data": "صوره المنتج" },
                { "data": "اسم المنتج" },
                { "data": "الصنف" },
				{ "data": "النوع" },
				{ "data": "الي مخزن" },
				{ "data": "الكمية المحولة" },
				{ "data": "السيريال" },
                { "data": "تاريخ الانشاء" }')); ?>
</div>
</body>
</html>
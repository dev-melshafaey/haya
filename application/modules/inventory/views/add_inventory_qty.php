<div class="col-md-12">
  <div class="panel panel-default panel-block" style="padding: 10px 10px;">
    <form method="POST" id="ah_inventory_qty" name="ah_inventory_qty">
      <input type="hidden" name="inventoryid" id="inventoryid" value="<?PHP echo $item->itemid; ?>">
      <div class="form-group col-md-3">
        <label for="basic-input"><strong>الكمية:</strong></label>
        <input type="text" class="form-control req NumberInput" value="<?PHP if($item->quantity !=""){echo $item->quantity;}else{ echo '0';} ?>" placeholder="الكمية" name="quantity" id="quantity" onkeyup="addTotal();" />
      </div>
      <div class="form-group col-md-3">
        <label for="basic-input"><strong>سعر الوحدة:</strong></label>
        <input type="text" class="form-control req NumberInput" value="<?PHP if($item->price !=""){echo $item->price;}else{ echo '0';} ?>" placeholder="سعر الوحدة" name="price" id="price" onkeyup="addTotal();"  />
      </div>
      
      <div class="form-group col-md-3">
        <label for="basic-input"><strong>أجمالي :</strong></label>
        <input type="text" class="form-control req NumberInput" value="0" placeholder="أجمالي " name="total" id="total" />
      </div>
      <div class="form-group col-md-3">
        <label for="basic-input"><strong>كود المادة:</strong></label>
        <input type="text" class="form-control req " value="" placeholder="كود المادة" name="code" id="code" />
      </div>
      <br clear="all">
      <div class="form-group col-md-12">
        <label for="basic-input"><strong>ملاحظات:</strong></label>
        <textarea name="qty_remarks" placeholder="تفاصيل العنصر" class="form-control req" style="resize:none; height:300px;" id="qty_remarks"><?PHP echo $item->itemdescription; ?></textarea>
      </div>
      <div class="form-group col-md-6">
        <button type="button" id="saveQtybtn" onClick="saveQty();" class="btn btn-success">حفظ</button>
      </div>
    </form>
    <br clear="all">
  </div>
</div>
<script src="<?PHP echo base_url(); ?>assets/js/ajax_script.js"></script> 
<script type="text/javascript">
function addTotal(){
	var total_items =$('#quantity').val();
	var price =$('#price').val();
	var total = parseInt(total_items) * parseInt(price);
	$('#total').val(total);
}
<?php if($item->itemid >0){ ?>
	addTotal();
<?php } ?>

</script>
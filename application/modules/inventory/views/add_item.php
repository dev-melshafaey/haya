<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
      <?php if($msg):?>
      <div class="col-md-12">
      	<div style="padding: 22px 20px !important; background:#c1dfc9;">
        	<h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
        </div>
      </div>
      <?php endif;?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block" style="padding: 10px 10px;">
          <form method="POST" id="form_add_item" name="form_add_item" enctype="multipart/form-data">
            <input type="hidden" name="itemid" id="itemid" value="<?PHP echo $item->itemid; ?>">
            <input type="hidden" name="oldfile" id="oldfile" value="<?PHP echo $item->itemphoto; ?>">
            <div class="form-group col-md-4">
                <label class="text-warning">اختيار مخزن</label>
                <select name="store_id" id="store_id" class="search-select form-control req" placeholder="اختيار مخزن">
                  <option value="">اختيار مخزن</option>
                  <?php foreach($all_stores as $store):?>
                  <option value="<?php echo $store->store_id;?>" <?php if(isset($store->store_id) AND $store->store_id	==	$store_id):?> selected="selected" <?php endif;?>><?php echo $store->store_name;?></option>
                  <?php endforeach;?>
                </select>
              </div>
            <div class="form-group col-md-4">
              <label for="basic-input"><strong>اسم العنصر:</strong></label>
              <input type="text" class="form-control req" value="<?PHP echo $item->itemname; ?>" placeholder="اسم العنصر" name="itemname" id="itemname" />
            </div>
            <div class="form-group col-md-4">
              <label for="basic-input"><strong>باركود :</strong></label>
              <input type="text" class="form-control req" value="<?PHP echo $item->barcode; ?>" placeholder="باركود" name="barcode" id="barcode" />
            </div>
            <div class="form-group col-md-6">
              <label for="basic-input"><strong>الفئة:</strong></label>
              <?PHP echo $this->haya_model->create_dropbox_list('list_category','category',$item->list_category,0,'req'); ?> </div>
            <div class="form-group col-md-6">
              <label for="basic-input"><strong>الفئة الفرعية:</strong></label>
              <?PHP echo $this->haya_model->create_dropbox_list('list_subcategory','subcategory',$item->list_subcategory,$item->list_category,'req'); ?> </div>
            <div class="form-group col-md-6">
              <label for="basic-input"><strong>صورة:</strong></label>
              <input type="file" accept="image/*" id="itemphoto" name="itemphoto">
              <?PHP if($item->itemphoto!='') { ?>
              <a class="fancybox-button" rel="gallery1" href="<?PHP echo base_url(); ?>resources/items/<?PHP echo $item->itemphoto; ?>"><i class="icon-eye-open"></i></a>
              <?PHP } ?>
            </div>
            <br clear="all">
            <div class="form-group col-md-12">
              <label for="basic-input"><strong>تفاصيل العنصر:</strong></label>
              <textarea name="itemdescription" placeholder="تفاصيل العنصر" class="form-control req" style="resize:none; height:300px;" id="itemdescription"><?PHP echo $item->itemdescription; ?></textarea>
            </div>
            <div class="form-group col-md-6">
              <button type="button" id="save_item" name="save_item" class="btn btn-success">حفظ</button>
            </div>
          </form>
          <br clear="all">
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory extends CI_Controller 
{
//-------------------------------------------------------------------------------	
	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;
//-------------------------------------------------------------------------------

	/*
	* Costructor
	*/
	
	public function __construct()
	{
		parent::__construct();

		// Loade Models
		$this->load->model('inventory_model','inventory');
		
		$this->_data['module']			=	$this->haya_model->get_module();
			
		// SET Login USER ID
		$this->_login_userid			=	$this->session->userdata('userid');
		$this->_data['login_userid']	=	$this->_login_userid;	
		
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);	
	}
//-------------------------------------------------------------------------------

	/*
	* Home listing of all products
	*/	
    public function index()
    {
        check_permission($this->_data['module'],'v');
		redirect(base_url().'inventory/allproducts');
		exit();
    }
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function allitems($store_id	=	NULL)
	{
		$this->_data['store_id']		=	$store_id;
		$this->_data['store_detail']	=	$this->inventory->get_store_detail($store_id);
		
		$this->load->view('item_list',$this->_data);
	}	
//-------------------------------------------------------------------------------

	/*
	* Add items
	* @param $itemid integer
	*/	
	public function additem($itemid,$store_id	=	NULL)
	{

		$this->_data['store_id']	=	$store_id;
		
		if($this->input->post())
		{
			$this->inventory->saveitem();
			
			/*
			redirect(base_url().'inventory/allitems');
			exit();*/
			
				$this->load->library('user_agent');
				if ($this->agent->is_referral())
				{
					$url	=	 $this->agent->referrer();
				}
				
				$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
				redirect($url);
				exit();
		}
		
		$this->_data['item'] 		=	$this->inventory->getitem($itemid,$store_id);
		$this->_data['all_stores']	=	$this->inventory->get_all_stores();
		
		$this->load->view('add_item',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Get AllSub Categories
	*/	
	public function get_sub_category()
	{
		$categoryid 		=	$this->input->post('categoryid');
		$subcategorylist 	=	$this->haya_model->get_dropbox_list_value('',$categoryid);
		
		echo json_encode($subcategorylist);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function ajax_allitems($store_id)
	{
		$result	=	$this->inventory->get_all_items($store_id);
		
		$permissions	=	$this->haya_model->check_other_permission(array('4'));
					
		foreach($result as $lc)
		{
			$action 	=  '<a  onclick="alatadad(this);" data-url="'.base_url().'inventory/get_item_details/'.$lc->itemid.'/'.$lc->store_id.'"  href="#"><i class="my icon icon-print"></i></a> ';
			
			if($lc->itemphoto)
			{
				$action  .= '<a class="fancybox-button" rel="gallery1" href="'.base_url().'resources/items/'.$lc->itemphoto.'"><i class="icon-eye-open"></i></a>';
			}
			else
			{
				$action	.=	'';
			}
			
			$action .= ' <a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'inventory/addqty/'.$lc->itemid.'"><i class="icon-plus-sign-alt"></i></a>';
			
			if($permissions[4]['u']	==	1) 
			{	$action .= '<a class="iconspace" href="'.base_url().'inventory/additem/'.$lc->itemid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
			
			if($permissions[4]['d']	==	1) 
			{	$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->itemid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->itemid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	}
			
			$arr[] = array(
				"DT_RowId"		=>	$lc->itemid.'_durar_lm',
				"رقم" 			=>	arabic_date($lc->itemid),
				"فئة" 			=>	$this->haya_model->get_name_from_list($lc->list_category),
				"الفئة الفرعية" =>	$this->haya_model->get_name_from_list($lc->list_subcategory),
				"اسم العنصر" 	=>	$lc->itemname,
				"الكمية" 		=>	$this->haya_model->dataCount('ah_inventory_qty','inventoryid',$lc->itemid,'SUM','quantity'),              
				"الإجرائات" 		=>	$action);
				
				unset($action);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function addqty($inventoryid)
	{
		$this->_data['inventoryid']		=	$inventoryid;
		$this->_data['item']			=	$this->inventory->getitem($inventoryid);
		
		$this->load->view('add_inventory_qty',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function saveQty()
	{
		$inventoryid 		=	$this->input->post('inventoryid');
		$addedby 			=	$this->_login_userid;
		$quantity 			=	$this->input->post('quantity');
		$qty_remarks 		=	$this->input->post('qty_remarks');
		$ah_inventory_qty 	=	array('inventoryid'=>$inventoryid,'addedby'=>$addedby,'quantity'=>$quantity,'qty_remarks'=>$qty_remarks);
		
		$this->db->insert('ah_inventory_qty',$ah_inventory_qty,json_encode($ah_inventory_qty),$this->_login_userid);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store ITEM Listing Page
	*/	
	public function complete_transfer()
	{
		$this->load->view('items-listing',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store SALES ITEM RECEPIT FROM AJAX
	*/	
	public function ajax_complete_transfer()
	{
		$result			=	$this->inventory->get_item_receipt();
		$permissions	=	$this->haya_model->check_other_permission(array('4'));
					
		foreach($result as $lc)
		{
			$action = '<a class="fancybox-button" rel="gallery1" href="'.base_url().'resources/items/'.$lc->itemphoto.'"><i class="icon-eye-open"></i></a>';
			//$action .= ' <a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'inventory/addqty/'.$lc->itemid.'"><i class="icon-plus-sign-alt"></i></a>';
			
			if($permissions[4]['u']	==	1) 
			{
				$action .= ' <a class="iconspace" href="'.base_url().'inventory/additem/'.$lc->itemid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			if($permissions[4]['d']	==	1) 
			{
				$action .= ' <a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->itemid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->itemid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->itemid.'_durar_lm',
				"رقم" 				=>	'',
				"فئة" 				=>	'',
				"صوره المنتج"		=>	'',
				"اسم المنتج"		=>	'',
				"الصنف"				=>	'',
				"النوع" 			=>	'',
				"الي مخزن" 			=>	'',
				"الكمية المحولة" 	=>	'',
				"السيريال" 			=>	'',
				"تاريخ الانشاء" 		=>	'');
				
				unset($action);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store PENDING ITEM Listing Page
	*/	
	public function pending_transfer()
	{
		$this->load->view('pending-items-listing',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store SALES ITEM RECEPIT FROM AJAX
	*/	
	public function ajax_pending_transfer()
	{		
		$result			=	$this->inventory->get_pending_transfer();
		$permissions	=	$this->haya_model->check_other_permission(array('4'));
					
		foreach($query->result() as $lc)
		{
			$action = '<a class="fancybox-button" rel="gallery1" href="'.base_url().'resources/items/'.$lc->itemphoto.'"><i class="icon-eye-open"></i></a>';
			//$action .= ' <a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'inventory/addqty/'.$lc->itemid.'"><i class="icon-plus-sign-alt"></i></a>';
			
			if($permissions[4]['u']	==	1) 
			{
				$action .= ' <a class="iconspace" href="'.base_url().'inventory/additem/'.$lc->itemid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			if($permissions[4]['d']	==	1) 
			{
				$action .= ' <a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->itemid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->itemid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			
			$arr[] = array(
				"DT_RowId"		=>	$lc->itemid.'_durar_lm',
				"رقم" 			=>	'',
				"الكود" 		=>	'',
				"اسم المنتج"	=>	'',
				"صورة المنتج"	=>	'',
				"الوحدة"		=>	'',
				"الكمية" 		=>	'',
				"وصف المنتج" 	=>	'',
				"ملاحظات" 		=>	'');
				
				unset($action);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store SALES ITEM RECEPIT Listing Page
	*/	
	public function sales_receipt()
	{
		$this->load->view('sales-items-recepit-listing',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store SALES ITEM RECEPIT FROM AJAX
	*/	
	public function ajax_sales_receipt()
	{
		$this->db->select('ahl.list_name as category,ahl1.list_name as subcategory,ahi.itemname,ahi.itemphoto,ahi.itemid');
		$this->db->from('ah_inventory as ahi');			
		$this->db->join('ah_listmanagement AS ahl','ahl.list_id = ahi.list_category');
		$this->db->join('ah_listmanagement AS ahl1','ahl1.list_id = ahi.list_subcategory');
		$this->db->where("ahi.itemstatus",1);
		$this->db->order_by('ahi.itemname','DESC');
		$this->db->limit(1);
		
		$query = $this->db->get();
		
		$permissions	=	$this->haya_model->check_other_permission(array('4'));
					
		foreach($query->result() as $lc)
		{
			$action = '<a class="fancybox-button" rel="gallery1" href="'.base_url().'resources/items/'.$lc->itemphoto.'"><i class="icon-eye-open"></i></a>';
			//$action .= ' <a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'inventory/addqty/'.$lc->itemid.'"><i class="icon-plus-sign-alt"></i></a>';
			
			if($permissions[4]['u']	==	1) 
			{
				$action .= ' <a class="iconspace" href="'.base_url().'inventory/additem/'.$lc->itemid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			if($permissions[4]['d']	==	1) 
			{
				$action .= ' <a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->itemid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->itemid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			
			$arr[] = array(
				"DT_RowId"		=>	$lc->itemid.'_durar_lm',
				"رقم" 			=>	'',
				"المخزن" 		=>	'',
				"رقم الفاتوره"	=>	'',
				"العميل"		=>	'',
				"الكمية"		=>	'',
				"تاريخ الانشاء" 	=>	'',
				"الإجرائات" 		=>	'');
				
				unset($action);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store PURCHASE ITEM RECEPIT Listing Page
	*/	
	public function purchase_receipt()
	{
		$this->load->view('purchase-items-recepit-listing',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store PURCHASE ITEM RECEPIT FROM AJAX
	*/	
	public function ajax_purchase_receipt()
	{
		$this->db->select('ahl.list_name as category,ahl1.list_name as subcategory,ahi.itemname,ahi.itemphoto,ahi.itemid');
		$this->db->from('ah_inventory as ahi');			
		$this->db->join('ah_listmanagement AS ahl','ahl.list_id = ahi.list_category');
		$this->db->join('ah_listmanagement AS ahl1','ahl1.list_id = ahi.list_subcategory');
		$this->db->where("ahi.itemstatus",1);
		$this->db->order_by('ahi.itemname','DESC');
		$this->db->limit(1);
		
		$query = $this->db->get();
		
		$permissions	=	$this->haya_model->check_other_permission(array('4'));
					
		foreach($query->result() as $lc)
		{
			$action = '<a class="fancybox-button" rel="gallery1" href="'.base_url().'resources/items/'.$lc->itemphoto.'"><i class="icon-eye-open"></i></a>';
			//$action .= ' <a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'inventory/addqty/'.$lc->itemid.'"><i class="icon-plus-sign-alt"></i></a>';
			
			if($permissions[4]['u']	==	1) 
			{
				$action .= ' <a class="iconspace" href="'.base_url().'inventory/additem/'.$lc->itemid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			if($permissions[4]['d']	==	1) 
			{
				$action .= ' <a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->itemid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->itemid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			
			$arr[] = array(
				"DT_RowId"		=>	$lc->itemid.'_durar_lm',
				"رقم" 			=>	'',
				"Barcode" 		=>	'',
				"المورد"		=>	'',
				"رقم الفاتوره"	=>	'',
				"تاريخ الانشاء" 	=>	'',
				"الإجرائات" 		=>	'');
				
				unset($action);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store PENDING SALES ITEM Listing Page
	*/	
	public function pending_sales_list()
	{
		$this->load->view('pending-items-sales-listing',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store PENDING SALES ITEMS Listing from AJAX
	*/	
	public function ajax_pending_sales_list()
	{
		$this->db->select('ahl.list_name as category,ahl1.list_name as subcategory,ahi.itemname,ahi.itemphoto,ahi.itemid');
		$this->db->from('ah_inventory as ahi');			
		$this->db->join('ah_listmanagement AS ahl','ahl.list_id = ahi.list_category');
		$this->db->join('ah_listmanagement AS ahl1','ahl1.list_id = ahi.list_subcategory');
		$this->db->where("ahi.itemstatus",1);
		$this->db->order_by('ahi.itemname','DESC');
		$this->db->limit(1);
		
		$query = $this->db->get();
		
		$permissions	=	$this->haya_model->check_other_permission(array('4'));
					
		foreach($query->result() as $lc)
		{
			$action = '<a class="fancybox-button" rel="gallery1" href="'.base_url().'resources/items/'.$lc->itemphoto.'"><i class="icon-eye-open"></i></a>';
			//$action .= ' <a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'inventory/addqty/'.$lc->itemid.'"><i class="icon-plus-sign-alt"></i></a>';
			
			if($permissions[4]['u']	==	1) 
			{
				$action .= ' <a class="iconspace" href="'.base_url().'inventory/additem/'.$lc->itemid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			if($permissions[4]['d']	==	1) 
			{
				$action .= ' <a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->itemid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->itemid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			
			$arr[] = array(
				"DT_RowId"		=>	$lc->itemid.'_durar_lm',
				"رقم" 			=>	'',
				"العميل" 		=>	'',
				"رقم الفاتوره" 	=>	'',
				"تاريخ الانشاء" 	=>	'',
				"الإجرائات" 		=>	'');
				
				unset($action);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store PENDING PURCHASE ITEM Listing Page
	*/	
	public function pending_purchase_list()
	{
		$this->load->view('pending-items-purchase-listing',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store PENDING PURCHASE ITEM Listing from AJAX
	*/	
	public function ajax_pending_purchase_list()
	{
		
			$this->db->select('ahl.list_name as category,ahl1.list_name as subcategory,ahi.itemname,ahi.itemphoto,ahi.itemid');
			$this->db->from('ah_inventory as ahi');			
			$this->db->join('ah_listmanagement AS ahl','ahl.list_id = ahi.list_category');
			$this->db->join('ah_listmanagement AS ahl1','ahl1.list_id = ahi.list_subcategory');
			$this->db->where("ahi.itemstatus",1);
			$this->db->order_by('ahi.itemname','DESC');
			$this->db->limit(1);
			
			$query = $this->db->get();
			
			$permissions	=	$this->haya_model->check_other_permission(array('4'));
						
			foreach($query->result() as $lc)
			{
				$action = '<a class="fancybox-button" rel="gallery1" href="'.base_url().'resources/items/'.$lc->itemphoto.'"><i class="icon-eye-open"></i></a>';
				//$action .= ' <a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'inventory/addqty/'.$lc->itemid.'"><i class="icon-plus-sign-alt"></i></a>';
				
				if($permissions[4]['u']	==	1) 
				{
					$action .= ' <a class="iconspace" href="'.base_url().'inventory/additem/'.$lc->itemid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				}
				
				if($permissions[4]['d']	==	1) 
				{
					$action .= ' <a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->itemid.'" data-url="'.base_url().'inquiries/delete_auditor/'.$lc->itemid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
				
				$arr[] = array(
					"DT_RowId"		=>	$lc->itemid.'_durar_lm',
					"رقم" 			=>	'',
					"المورد" 		=>	'',
					"رقم الفاتوره" 	=>	'',
					"عناصر متبيقة" 	=>	'',
					"تاريخ الانشاء" 	=>	'', //$this->haya_model->dataCount('ah_inventory_qty','inventoryid',$lc->itemid,'SUM','quantity'),              
					"الإجرائات" 		=>	'');
					
					unset($action);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* Listing of all Donations
	*/	
	public function all_donations()
	{
		$this->load->view('all_donations',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Listing of all Donations from AJAX
	*/	
	public function ajax_all_donations()
	{		
		$donors	=	$this->inventory->get_all_donors('DONOR');
		
		$permissions	=	$this->haya_model->check_other_permission(array('4'));
					
		foreach($donors as $lc)
		{
			$action  = '<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inventory/donation_detail/'.$lc->donation_id.'"><i class="icon-eye-open"></i></a>';
			$action	.=	' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inventory/add_donation_items/'.$lc->donation_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt" data-hasqtip="43" aria-describedby="qtip-43"></i></a>';
			
			if($permissions[4]['u']	==	1) 
			{
				$action .= ' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inventory/add_donation/'.$lc->donation_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			if($permissions[4]['d']	==	1) 
			{
				$action .= ' <a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->donation_id.'" data-url="'.base_url().'inventory/delete_donation/'.$lc->donation_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			
			
			$arr[] = array(
				"DT_RowId"		=>	$lc->donation_id.'_durar_lm',
				"اسم المانحة" 	=>	$lc->user_name,
				"رقم البطاقة" 	=>	$lc->card_number,
/*					"رقم الفاتوره" 	=>	'',
				"العناصرن" 		=>	'',
				"السعر" 		=>	'',
				"الكمية" 		=>	'', */
				"الإجرائات" 		=>	$action);
				
				unset($action);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store PENDING PURCHASE ITEM Listing Page
	*/	
	public function add_donation($donation_id	=	NULL)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($data['donation_id'])
			{
				$this->inventory->update_donation($data['donation_id'],$data);
			}
			else
			{
				$this->inventory->add_donation($data);
			}
			
			redirect(base_url().'inventory/all_donations');
			exit();
		}
		else
		{			
			$donation_user_info				=	$this->inventory->get_donation_user_info($donation_id);
			
			$this->_data['donation_id']		=	isset($donation_user_info->donation_id) ? $donation_user_info->donation_id : NULL;
			$this->_data['card_number']		=	isset($donation_user_info->card_number) ? $donation_user_info->card_number : NULL;
			$this->_data['user_name']		=	isset($donation_user_info->user_name) ? $donation_user_info->user_name : NULL;
			
			$this->load->view('add-donation-form',$this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store PENDING PURCHASE ITEM Listing Page
	*/	
	public function add_donation_items($donation_id	=	NULL)
	{
		
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			unset($data['list_category_text']);
			unset($data['list_subcategory_text']);
			
			if($data['donation_id'])
			{	
				$d_item_id	=	$this->inventory->add_donation_items($data);
				
				$add_into_quantity	=	array('inventoryid'	=>	$data['itemid'],'d_itemid'	=>	$d_item_id,'addedby'	=>$this->_login_userid,	'quantity'	=>	$data['total_items']);
				$this->inventory->add_item_quantity($add_into_quantity);
			}
			
			$row	 =	'<tr role="row" id="row-'.$d_item_id.'">';
			$row	.=	'<td style="text-align:center;">'.$data['itemid'].'</td>';
			$row	.=	'<td style="text-align:center;">'.$data['price'].'</td>';
			$row	.=	'<td style="text-align:center;">'.$data['total_items'].'</td>';
			$row	.=	'<td style="text-align:center;"><a href="#_" onClick="delete_d_item("'.$d_item_id.'");"><i style="color:#CC0000;" class="icon-remove-sign"></i></a></td>';
			$row	.=	'</tr>';
			
			echo $row;
		}
		else
		{			
			$this->_data['donation_id']	=	$donation_id;
			
			$this->_data['donation_items']	=	$this->inventory->get_donation_items($this->_data['donation_id']);	

			$this->load->view('add-donation-items-form',$this->_data);
		}
	}
//-------------------------------------------------------------------------------

	/*
	* Listing of all Donations
	*/	
	public function donation_detail($donation_id	=	NULL)
	{
		$this->_data['donation_detail']	=	$this->inventory->get_all_detail_by_id($donation_id);
		
		$this->load->view('donation-detail-page',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Listing of all Donations
	*/	
	public function all_donation_receivers()
	{
		$this->load->view('all_donation_receivers',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Listing of all Donations from AJAX
	*/	
	public function ajax_all_donation_receivers()
	{
		
		$donors	=	$this->inventory->get_all_donors('RECEIVER');
		
		$permissions	=	$this->haya_model->check_other_permission(array('4'));
					
		foreach($donors as $lc)
		{
			$action  = '<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inventory/donation_detail/'.$lc->donation_id.'"><i class="icon-eye-open"></i></a>';
			$action	.=	' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inventory/add_donation_items/'.$lc->donation_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt" data-hasqtip="43" aria-describedby="qtip-43"></i></a>';
			
			if($permissions[4]['u']	==	1) 
			{
				$action .= ' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'inventory/add_donation/'.$lc->donation_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			if($permissions[4]['d']	==	1) 
			{
				$action .= ' <a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->donation_id.'" data-url="'.base_url().'inventory/delete_donation/'.$lc->donation_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			
			
			$arr[] = array(
				"DT_RowId"		=>	$lc->donation_id.'_durar_lm',
				"اسم المانحة" 	=>	$lc->user_name,
				"رقم البطاقة" 	=>	$lc->card_number,
				"الإجرائات" 		=>	$action);
				
				unset($action);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store PENDING PURCHASE ITEM Listing Page
	*/	
	public function add_donation_receiver()
	{
		$this->load->view('add-donation-receiver-form',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Store to Store PENDING PURCHASE ITEM Listing Page
	*/	
	public function get_items()
	{
		$data	=	$this->input->post();

		echo $response	=	$this->haya_model->get_items_by_cat_subcat($data['cat_id'],$data['sub_cat_id']);
	}		
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/

	public function delete_donation($donation_id)
	{
		$this->inventory->delete_donation($donation_id);
	}
//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/

	public function delete_d_item()
	{
		echo $d_itemid	=	$this->input->post('d_itemid');
		
		return $this->inventory->delete_d_item($d_itemid);
	}
//------------------------------------------------------------------------

  	/**
   	* Dynamic Forms Listing Page
   	* @param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]  = $this->haya_model->get_all_custom_form($moduleid);
		
		$this->_data["userid"] = $this->_login_userid;
		
		$this->_data['formid']	=	$moduleid;
		 
		 // Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);	 
	 }
//-------------------------------------------------------------------------------

	/*
	* All Stores Listing Page
	*/	
	public function all_stores()
	{
		$this->load->view('all-stores-list',$this->_data);
	}	
//-------------------------------------------------------------------------------

	/*
	* Add Store
	* @param $store_id integer
	*/	
	public function add_store($store_id	=	NULL)
	{
		// IF form submit data BY POST
		if($this->input->post())
		{
			$this->inventory->savestore(); // Add Store Detail into database
			
			/*redirect(base_url().'inventory/all_stores'); // Redirect to Store Listing Page.
			exit();*/
			
			$this->load->library('user_agent');
			if ($this->agent->is_referral())
			{
				$url	=	 $this->agent->referrer();
			}
							
			$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
			redirect($url);
			exit();
		}
		
		$this->_data['store_detail'] = $this->inventory->get_store_detail($store_id); // GET Single store Detail for EDIT record
		
		$this->_data['all_users']		=	$this->haya_model->get_all_users(); // Get all users
		
		$this->load->view('add-store',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function ajax_all_stores()
	{
			$this->db->select('store_id,store_name,login_user_id,country_id,city_id,description,take_care_userid,document,delete_record,submit_date');
			$this->db->from('ah_stores');						
			$this->db->where("delete_record",'0');
			$this->db->order_by('store_id','DESC');
			$query = $this->db->get();
			
			$permissions	=	$this->haya_model->check_other_permission(array('191'));
			
			
			foreach($query->result() as $lc)
			{
				$action 	=  '<a  onclick="alatadad(this);" data-url="'.base_url().'inventory/get_store_detail/'.$lc->store_id.'"  href="#"><i class="my icon icon-print"></i></a> ';
				
				if($lc->document)
				{
					$action	 	.= ' <a class="fancybox-button" rel="gallery1" href="'.base_url().'resources/stores/'.$lc->document.'"><i class="icon-eye-open"></i></a> ';
				}
				
				$action	.= ' <a href="'.base_url().'inventory/additem/0/'.$lc->store_id.'"><i class="icon-plus-sign-alt"></i></a>';
				
				if($permissions[191]['u']	==	1) 
				{	
					$action .= '<a class="iconspace" href="'.base_url().'inventory/add_store/'.$lc->store_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				}
				
				if($permissions[191]['d']	==	1) 
				{
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->store_id.'" data-url="'.base_url().'inventory/delete_store/'.$lc->store_id.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
				
				$items .= '<a href="'.base_url().'inventory/allitems/'.$lc->store_id.'"id="'.$lc->store_id.'">'.arabic_date($this->inventory->get_item_counts($lc->store_id)).'</a>';
				
				$arr[] = array(
					"DT_RowId"		=>	$lc->store_id.'_durar_lm',
					"رقم" 			=>	arabic_date($lc->store_id),
					"عنوان المخزن" 	=>	$lc->store_name,
					"الدولة" 		=>	$this->haya_model->get_name_from_list($lc->country_id),
					"المدينة" 		=>	$this->haya_model->get_name_from_list($lc->city_id),
					"اجمالي العناصر" 	=>	$items,
					"الإجرائات" 		=>	$action);
					
					unset($action,$items);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* Add Store
	* @param $store_id integer
	*/	
	public function get_store_detail($store_id)
	{
		$this->_data['store_detail'] 	=	$this->inventory->get_store_detail($store_id);
		$this->_data['items'] 			=	$this->inventory->get_all_items($store_id);
		
		$this->load->view('store-detail',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Get all detail for print
	* @param $store_id integer
	*/	
	public function get_item_details($itemid,$store_id)
	{
		$this->_data['item'] 		=	$this->inventory->getitem($itemid,$store_id);
		$this->_data['store_detail'] = $this->inventory->get_store_detail($store_id);
		
		$this->load->view('item-details-print',$this->_data);
	}
	
//-----------------------------------------------------------------------
	/*
	* Delete Store
	* @param $store_id integer
	*/	
	function delete_store($store_id)
	{
		$this->inventory->delete_store($store_id);		
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
	}
//-----------------------------------------------------------------------
/* End of file inventory.php */
/* Location: ./application/modules/inventory/inventory.php */
}
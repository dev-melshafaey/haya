<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventory_model extends CI_Model {
	
	/*
	* Properties
	*/
	private $_table_users;	
	private $_table_donate;	
	private $_table_applied_donation;
	private $_table_sms;
//----------------------------------------------------------------------
    
	/*
	* Constructor
	*/
	
	function __construct()
    {
        parent::__construct();
		
		$this->load->helper("file");
		
		//Load Table Names from Config
		$this->_table_users 			=  $this->config->item('table_users');
		$this->_table_donate 			=  $this->config->item('table_donate');
        $this->_table_user_profile 		=  $this->config->item('table_user_profile');
		$this->_table_applied_donation 	=  $this->config->item('table_applied_donation');
		$this->_table_sms 				=  $this->config->item('sms_management');
		$this->_login_userid			=	$this->session->userdata('userid');
    }
	
	function saveitem()
	{
		$itemid 			=	$this->input->post('itemid');
		$store_id 			=	$this->input->post('store_id');
		$addedby 			=	$this->_login_userid;
		$list_category 		=	$this->input->post('list_category');
		$list_subcategory 	=	$this->input->post('list_subcategory');
		$itemname 			=	$this->input->post('itemname');
		$itemdescription 	=	$this->input->post('itemdescription');
		$itemphoto 			=	$this->haya_model->upload_file('itemphoto','resources/items');
		$itemqty 			=	$this->input->post('itemqty');
		$itemstatus 		=	$this->input->post('itemstatus');
		$oldfile 			=	$this->input->post('oldfile');
		
		if($oldfile!='' && $itemname=='')
		{	$itemphoto = $oldfile;	}
				
		$ah_inventory = array(
				'addedby'			=>	$addedby,
				'store_id'			=>	$store_id,
				'list_category'		=>	$list_category,
				'list_subcategory'	=>	$list_subcategory,
				'itemname'			=>	$itemname,
				'itemdescription'	=>	$itemdescription,
				'itemphoto'			=>	$itemphoto,
				'itemstatus'		=>	1);
				
		if($itemid	!=''	&&	$itemname	==	'0')
		{
			$this->db->where('itemid',$itemid);
			$this->db->update('ah_inventory',json_encode($ah_inventory),$this->_login_userid,$ah_inventory);
		}
		else
		{
			$this->db->insert('ah_inventory',$ah_inventory,json_encode($ah_inventory),$this->_login_userid);
		}
		
		return TRUE;
	}
	
	function getitem($itemid,$store_id)
	{
		$this->db->where('itemid',$itemid);
		
		if($store_id)
		{
			$this->db->where('store_id',$store_id);
		}
		
        $query = $this->db->get('ah_inventory');
		return $query->row();
	}
	
//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function add_donation($data)
	 {
		 $this->db->insert('ah_donations',$data,json_encode($data),$this->_login_userid);
		 
		 return TRUE;
	 }
//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function update_donation($donation_id,$data)
	 {
	 	$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('donation_id', $donation_id);

		$this->db->update('ah_donations',$json_data,$this->_login_userid,$data);

		return TRUE;
	 }	 
	 

	 
//----------------------------------------------------------------------

	/*
	*
	* Get all Donors
	*/
	 public function get_all_donors($user_type)
	 {
		 $this->db->where('user_type',$user_type);
		$this->db->where('delete_record','0');
		$this->db->order_by('donation_id','DESC');
		
        $query = $this->db->get('ah_donations');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	 }
//----------------------------------------------------------------------

	/*
	*
	* Get Donation User Detail By donation_id
	* @param $donation_id	integer
	*/
	 public function get_donation_user_info($donation_id)
	 {
		$this->db->where('donation_id',$donation_id);
		 
        $query = $this->db->get('ah_donations');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	 }
//----------------------------------------------------------------------

	/*
	* Delete Donation Information
	*/

	function delete_donation($donation_id)
	{
		$json_data	=	json_encode(array('record'=>'delete','donation_id'	=>	$donation_id));
		
		$data	=	array('delete_record'=>'1');
		
		$this->db->where('donation_id', $donation_id);

		$this->db->update('ah_donations',$json_data,$this->session->userdata('userid'),$data);

		return TRUE;
	}
//----------------------------------------------------------------------

	/*
	* Delete Donation Information
	*/

	function delete_d_item($d_itemid)
	{
		$this->db->where('d_itemid', $d_itemid);
		$this->db->delete('ah_inventory_qty');
		
		$this->db->where('d_itemid', $d_itemid);
		$this->db->delete('ah_donation_items');
		
		return TRUE;
	}
	
//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function add_donation_items($data)
	 {
		$this->db->insert('ah_donation_items',$data,json_encode($data),$this->_login_userid);
		 
		$insert_id = $this->db->insert_id();

   		return  $insert_id;
	 }
//----------------------------------------------------------------------

	/*
	*
	* 
	* 
	*/
	 public function get_donation_items($donation_id)
	 {
		$this->db->where('donation_id',$donation_id);
        $query = $this->db->get('ah_donation_items');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	 }
	//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function add_item_quantity($data)
	 {
		 
		 $this->db->insert('ah_inventory_qty',$data,json_encode($data),$this->_login_userid);

   		return  TRUE;
	 } 
//-----------------------------------------------------------------------------
	/*
	* Get Store Detail By ID
	* @param $store_id integer
	* return OBKECT
	*/
	function get_item_counts($store_id)
	{
		$this->db->select('store_id');
		$this->db->where('store_id',$store_id);
        $query = $this->db->get('ah_inventory');
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return '0';
		}
	}
		
//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function get_all_items($store_id)
	 {
		 $query_string	=	NULL;
		 if($store_id)
		 {
			 $query_string	=	"WHERE `ah_stores`.`store_id`=".$store_id."";
		 }
		 
		$query	=	$this->db->query("SELECT
			`ah_inventory`.`itemid`
			, `ah_inventory`.`store_id`
			, `ah_inventory`.`addedby`
			, `ah_inventory`.`list_category`
			, `ah_inventory`.`list_subcategory`
			, `ah_inventory`.`itemname`
			, `ah_inventory`.`itemdescription`
			, `ah_inventory`.`itemphoto`
			, `ah_inventory`.`itemstatus`
			, `ah_stores`.`store_name`
			FROM
			`ah_inventory`
			INNER JOIN `ah_stores` 
			ON (`ah_inventory`.`store_id` = `ah_stores`.`store_id`) $query_string
			GROUP BY `ah_inventory`.`itemid`;");
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	 }
//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function get_all_detail_by_id($donation_id)
	 {
		$this->db->select('donation_id,card_number,user_name,delete_record,submitted_date');
		$this->db->where('donation_id',$donation_id);
		
		$this->db->limit(1);
		
		$query = $this->db->get('ah_donations');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	 }
//----------------------------------------------------------------------

	/*
	*
	* Add Donor Detail
	*/
	 public function get_all_items_by_item($donation_id)
	 {
		$this->db->select('d_itemid,itemid,donation_id,list_category,list_subcategory,price,total_items,description,notes');
		$this->db->where('donation_id',$donation_id);
		
		$query = $this->db->get('ah_donation_items');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	 }
//----------------------------------------------------------------------

	/*
	*
	* 
	*/	 
	function get_item_name($itemid)
	{
		$this->db->select('itemname');
		$this->db->where('itemid',$itemid);
		
        $query = $this->db->get('ah_inventory');
		
		return $query->row()->itemname;
	}
	function savestore()
	{
		$store_id 			=	$this->input->post('store_id');
		$login_user_id 		=	$this->_login_userid;
		$store_name 		=	$this->input->post('store_name');
		$country_id 		=	$this->input->post('country_id');
		$city_id 			=	$this->input->post('city_id');
		$description 		=	$this->input->post('description');
		$address 			=	$this->input->post('address');
		$take_care_userid	=	$this->input->post('take_care_userid');
		$document 			=	$this->haya_model->upload_file('document','resources/stores');
		$olddocument 		=	$this->input->post('olddocument');
		
		if($olddocument!='' && $store_name=='')
		{
			$document = $olddocument;
		}
				
		$ah_store = array(
			'login_user_id'		=>	$login_user_id,
			'store_name'		=>	$store_name,
			'country_id'		=>	$country_id,
			'city_id'			=>	$city_id,
			'description'		=>	$description,
			'address'			=>	$address,
			'take_care_userid'	=>	$take_care_userid,
			'document'			=>	$document
			);
				
		if($store_id	!=	'')
		{
			$this->db->where('store_id',$store_id);
			$this->db->update('ah_stores',json_encode($ah_store),$this->_login_userid,$ah_store);
		}
		else
		{
			$this->db->insert('ah_stores',$ah_store,json_encode($ah_store),$this->_login_userid);
		}
	}
//-----------------------------------------------------------------------------
	/*
	* Get Store Detail By ID
	* @param $store_id integer
	* return OBKECT
	*/
	function get_store_detail($store_id)
	{
		$this->db->select('store_id,store_name,login_user_id,country_id,city_id,description,address,take_care_userid,document,delete_record,submit_date');
		$this->db->where('store_id',$store_id);
        $query = $this->db->get('ah_stores');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------

	/*
	* Delete Donation Information
	* @param $store_id integer
	* retun TRUE
	*/

	function delete_store($store_id)
	{
		$json_data	=	json_encode(array('record'=>'delete','store_id'	=>	$store_id));
		
		$data	=	array('delete_record'=>'1');
		
		$this->db->where('store_id', $store_id);

		$this->db->update('ah_stores',$json_data,$this->session->userdata('userid'),$data);

		return TRUE;
	}
//-----------------------------------------------------------------------------
	/*
	* Get all stores
	* return OBKECT
	*/
	function get_all_stores()
	{
		$this->db->select('store_id,store_name');

        $query = $this->db->get('ah_stores');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-----------------------------------------------------------------------------
	/*
	* Get Store to Store Item Rceipt
	* return OBJECT
	*/
	function get_item_receipt()
	{
		$this->db->select('ahl.list_name as category,ahl1.list_name as subcategory,ahi.itemname,ahi.itemphoto,ahi.itemid');
		$this->db->from('ah_inventory as ahi');			
		$this->db->join('ah_listmanagement AS ahl','ahl.list_id = ahi.list_category');
		$this->db->join('ah_listmanagement AS ahl1','ahl1.list_id = ahi.list_subcategory');
		$this->db->where("ahi.itemstatus",1);
		$this->db->order_by('ahi.itemname','DESC');
		$this->db->limit(1);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-----------------------------------------------------------------------------
	/*
	* Get Store to Pending Trnsfer Receipt
	* return OBJECT
	*/
	function get_pending_transfer()
	{
		$this->db->select('ahl.list_name as category,ahl1.list_name as subcategory,ahi.itemname,ahi.itemphoto,ahi.itemid');
		$this->db->from('ah_inventory as ahi');			
		$this->db->join('ah_listmanagement AS ahl','ahl.list_id = ahi.list_category');
		$this->db->join('ah_listmanagement AS ahl1','ahl1.list_id = ahi.list_subcategory');
		$this->db->where("ahi.itemstatus",1);
		$this->db->order_by('ahi.itemname','DESC');
		$this->db->limit(1);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	} 
//----------------------------------------------------------------------	 
}
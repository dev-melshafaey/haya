<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Listing_managment extends CI_Controller 
{

//-------------------------------------------------------------------------------	

	/*

	* Properties

	*/

	private $_data = array();
	private $_user_info	=	array();

//-------------------------------------------------------------------------------
	/*

	* Costructor

	*/
	public function __construct()
	{
		parent::__construct();
		
		// Load Models
		$this->load->model('listing_managment_model', 'listing');
		$this->load->model('haya/haya_model', 'haya');
		
		$this->_data['module'] = $this->haya->get_module();      
	}
//-------------------------------------------------------------------------------
	/*

	*

	* Main Page

	*/

	public function index()
	{
		$this->haya->$this->haya->$this->haya->check_permission($this->_data['module'],'v');
		$this->load->view('listmanagement_landing', $this->_data);
	}

//-------------------------------------------------------------------------------



	/*

	*

	* Add List Detail

	*/
	public function addlistingview()
	{
		$this->load->view('addlistingview');
	}
	
	public function add_parent($type	=	NULL)
	{
		if($type)
		{
			
			$this->_data['type'] = $this->listing->get_type_id($type);
			
			//print_r($this->_data['type']);
			
			//$this->_data['parent_id']	=	$parent_id;
		}
		else
		{
			//$this->_data['parent_id']	=	NULL;
		}
		
		$this->load->view('add-parent',$this->_data);
	}
	public function add_child($parent_id)
	{
		if($parent_id)
		{
			$this->_data['parent_id']	=	$parent_id;
		}
		else
		{
			$this->_data['parent_id']	=	NULL;
		}
		
		$this->load->view('add-child',$this->_data);
	}
	
	public function add($listid	= NULL)

	{
        if($this->haya->check_permission($this->_data['module'],'a')!=1) {  redirect('dashboard');   }
		if($listid)

		{

			$this->_data['single_list']	=	$this->listing->get_single_record($listid);	

		}

		if($this->input->post())

		{

			

			$data		=	$this->input->post();

			

			// UNSET ARRAY key

			unset($data['submit']);

			

			if($this->input->post('list_id'))

			{

				$this->listing->update_list($this->input->post('list_id'),$data);

				

				if ($this->is_ajax())

				{

					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');

				}

				else

				{

					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');

					redirect(base_url()."listing_managment/listing");

					exit();

				}



			}

			else

			{

				$this->listing->add_list($data);


				if ($this->is_ajax())

				{

					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');



				}

				else

				{

					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');

					redirect(base_url()."listing_managment/listing");

					exit();

				}



			}

		}

		else

		{

			if($listid)

			{

				$this->_data['list_id']	=	$listid;

			}

			else

			{

				$this->_data['list_id']	=	'';

			}

			

			$this->load->view('add', $this->_data);

		}

		

	}	

//-------------------------------------------------------------------------------



	/*

	*

	* Listing Page

	*/

	public function listing($type	=	NULL)
	{
		if($type)
		{
			$this->_data['type']	=	$type;

			// Get List Name By Type
			$this->_data['listing']	=	$this->listing->get_all_by_type($type);

			$this->_data['list_type_name']	=	$type;

			$this->load->view('type-listing', $this->_data);
		}
		else
		{
			$this->_data['marital_count']	=	$this->listing->total_count('maritalstatus');
			$this->_data['situation_count']	=	$this->listing->total_count('current_situation');
			$this->_data['inquiry_type']	=	$this->listing->total_count('inquiry_type');

			$this->load->view('listing', $this->_data);
		}
	}


	public function nooaul_qaima()
	{
		$segment = $this->uri->segment(3);
		foreach($this->listing->get_all_list_type() as $listdata) 
		{
			$typename = list_types($listdata->list_type);
			
			$action =' <a href="'.base_url().'listing_managment/listing/'.$listdata->list_type.'">عرض الكل</a> '; 
			
			$action .= ' <button onClick="gototype(this);" type="button" data-url="'.base_url().'listing_managment/listing/'.$listdata->list_type.'" class="btn btn-success">'.$this->listing->total_count($listdata->list_type).'</button>';
			
			
			$arr[] = array(
				"DT_RowId"=>$listdata->list_id.'_durar_lm',
                "‫نوع القائمة" =>$typename['ar'],
				/*"عرض الكل" =>$listdata->list_type,*/
				"إجمالي عدد" =>$action);
				unset($actions);
		}
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
	
public function list_nooaul_qaima($type)
	{

		$this->db->select('*');
		$this->db->where_in('list_type',$type);
		$query = $this->db->get('list_management');
		

		//echo '<pre>'; print_r($query->result()->list_type);
		$checkbox	=	'---';
		
		foreach($query->result() as $lc)
		{
				if(check_other_permission(1,'a')==1)
				{	
									  	if($lc->list_type != 'inquiry_type' AND $lc->list_type != 'rules' AND $lc->list_type != 'qualification' AND $lc->list_type != 'business_type' AND $lc->list_type != 'activity_project' AND $lc->list_type != 'project_employment' AND $lc->list_type != 'project_type')
				{
					if($lc->other)
					{
						$checked	=	'checked="checked"';
					}
					else
					{
						$checked	=	'';
					}
					$checkbox ='<div class="other1"> <input type="checkbox" onClick="other(this);" id="'.$lc->list_id.'" name="other'.$lc->list_id.'" '.$checked.'>

                  <div id="show'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;">&#10004;</div>

                  <div id="hide'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;"><i style="color:#CC0000;" class="icon-remove-sign"></i></div>

                  </div>';
				}

				}
				
				if(check_other_permission(1,'a')==1)
				{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment/add_child_qaima/'.$lc->list_id.'/" id="'.$lc->list_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> ';	}
								
				if(check_other_permission(1,'u')==1)
				{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment/add_data_for_qaima/'.$lc->list_id.'/parent" id="'.$lc->list_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';	}
				
				if(check_other_permission(1,'d')==1)
				{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->list_id.'" data-url="'.base_url().'listing_managment/delete/'.$lc->list_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
				
					$count = $this->qaima_count($lc->list_id);
					
				 	$actions .=' <a href="'.base_url().'listing_managment/child_listing/'.$lc->list_id.'">عرض الكل ('.$count.')</a>'; 
				 $arr[] = array(
				"DT_RowId"=>$lc->list_id.'_durar_lm',
                "قائمة البرامج" =>$lc->list_name,              
				"أخرى" =>$checkbox,
				"الإجراءات" =>$actions);
				unset($actions);
		}
		$ex['data'] = $arr;
			echo json_encode($ex);
	}
public function child_nooaul_qaima($parent_id)
	{

		$this->db->select('*');
		$this->db->where('list_parent_id',$parent_id);
		$this->db->order_by('list_order','DESC');
		$query = $this->db->get('list_management');
		
		
		//echo '<pre>'; print_r($query->result());exit();
		
		foreach($query->result() as $lc)
		{
				if(check_other_permission(72,'a')==1)
				{	$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment/add_child_qaima/'.$lc->list_id.'/" id="'.$lc->list_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> ';	}

				
				if(check_other_permission(72,'d')==1)
				{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->list_id.'" data-url="'.base_url().'listing_managment/delete/'.$lc->list_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
				
					$count = $this->listing->get_list_child_count($lc->list_id);
					
				 	$actions .=' <a href="'.base_url().'listing_managment/sub_child_listing/'.$lc->list_id.'">عرض الكل ('.$count.')</a>'; 
				 $arr[] = array(
				"DT_RowId"=>$lc->list_id.'_durar_lm',
                "‫نوع القائمة" =>$lc->list_name,              
				"الإجراءات" =>$actions);
				unset($actions);
		}
		$ex['data'] = $arr;
			echo json_encode($ex);
	}
	public function sub_child_nooaul_qaima($parent_id)
	{
		$this->db->select('*');
		$this->db->where('list_parent_id',$parent_id);
		$this->db->order_by('list_order','DESC');
		$query = $this->db->get('list_management');
		
		foreach($query->result() as $lc)
		{
				if(check_other_permission(72,'d')==1)
				{
					$actions =' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->list_id.'" data-url="'.base_url().'listing_managment/delete/'.$lc->list_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
				
				 $arr[] = array(
				"DT_RowId"		=>	$lc->list_id.'_durar_lm',
                "‫نوع القائمة" 	=>	$lc->list_name,              
				"الإجراءات" 		=>	$actions);
				unset($actions);
		}
		
			$ex['data'] = $arr;
			echo json_encode($ex);
	}
//-------------------------------------------------------------------------------
	public function add_child_qaima($parentid)
	{
		$this->_data['type'] = 'child';
		$this->_data['parent_id'] = $parentid;
		
		$this->load->view('dialog/add-child-dialog',$this->_data);
	}
	
	public function add_data_for_qaima($parentid)
	{
		if($parentid!='' && $parentid!=0)
		{
			$this->_data['data'] = $this->listing->get_list_data($parentid);
			
			//echo '<pre>'; print_r($this->_data['data']);
		}
		
		$this->_data['type'] = 'parent';
		
		$this->load->view('dialog/add-child-dialog',$this->_data);
	}
	
	public function qaima_count($id)
	{
		//$this->db->where("parent_id",$id);
		//$query = $this->db->get($this->_table_loan_calculate);
		$sql = "SELECT COUNT(*) AS total FROM (`list_management`) WHERE `list_parent_id` = '".$id."'";
		$q = $this->db->query($sql);		
	
		// Check if Result is Greater Than Zero
		if($q->num_rows() > 0)
		{
			 $oneRow = $q->row();
			 return $oneRow->total;
		}
	}
//-------------------------------------------------------------------------------

	public function add_parent_qaima()
	{
		$list_id	=	$this->input->post("list_id");
		
		$data	=	$this->input->post();
		
		
		if($list_id)
		{
			$this->listing->update_list($list_id,$data);
		}
		else
		{
			$this->listing->add_list($data);
		}
	}
//-------------------------------------------------------------------------------

	/*

	*

	* Child Listing

	*/

	public function child_listing($listid	=	NULL)

	{

		$this->_data['parent_id']	=	$listid;

		

		$this->_data['listing']	=	$this->listing->get_child_listing($listid);



		

		$this->load->view('child-type-listing', $this->_data);

	}

//-------------------------------------------------------------------------------



	/*

	*

	* Subchild Listing Page

	*/

	public function sub_child_listing($listid	=	NULL)
	{
		$this->_data['parent_id']	=	$listid;

		$this->_data['listing']	=	$this->listing->get_subchild_listing($listid);



		$this->load->view('subchilds-type-listing', $this->_data);

	}	

	

//-------------------------------------------------------------------------------	

	function add_new()

	{

		$parent_id	=	$this->input->post("parent_id");
		$add_sub	=	$this->input->post("add_sub");
		
		$data		=	array(
						"list_parent_id"=>$parent_id,
						"list_name"=>$add_sub
						);
						
		//print_r($data);

		$this->listing->add_list_child($data);	

	}

//-------------------------------------------------------------------------------	

	public function get_list_data()

	{

		$list_id	=	$this->input->post('id');

		

		$data	=	$this->listing->get_list_data($list_id);

		



		echo  $data	=	json_encode(array('list_id'	=>	$data->list_id,'list_name'	=>	$data->list_name,'list_type'	=>	$data->list_type,'list_status'	=>	$data->list_status));

		

	}	

//-------------------------------------------------------------------------------



	/*

	* Delete List

	*

	*/

	

	public function delete($listid,$type)

	{

		$this->listing->delete($listid);

		

		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');

		redirect(base_url().'listing_managment/listing/'.$type);

		exit();

	}

//-------------------------------------------------------------------------------



	/*

	* Delete List

	*

	*/

	

	public function delete_child($childlistid)

	{

		$this->listing->delete_child($childlistid);

		

		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');

		redirect(base_url().'listing_managment/listing/');

		exit();



	}



//-------------------------------------------------------------------------------



	/*

	* Delete List

	*

	*/

	

	public function other()

	{

		$list_id	=	$this->input->post("id");

		$entry		=	$this->input->post("entry");

		

		$data		=	array('other' => $entry);



		$this->listing->update_record($list_id, $data);

	}

//-------------------------------------------------------------------------------



	/*

	* Logout

	* Destroy All Sessions

	*/

	

	public function logout()

	{

		// Destroy all sessions

		$this->session->sess_destroy();

		$this->session->unset_userdata('userid');

		$this->session->unset_userdata('userinfo');

		redirect(base_url());

		exit();



	}

//-------------------------------------------------------------------------------

//Function to check if the request is an AJAX request

function is_ajax() 

{

  return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

}

//-------------------------------------------------------------------------------

}





?>
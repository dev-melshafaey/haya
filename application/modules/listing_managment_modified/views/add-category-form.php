<?php $labels	=	$this->config->item('list_types');?>

<div class="row col-md-12">
  <form action="" method="POST" id="add-category-form" name="add-category-form">
    <div class="form-group col-md-6">
      <label for="basic-input">إسم</label>
      <input type="text" class="form-control req" name="list_name"  id="list_name"  placeholder="اسم القائمة" value="<?php echo (isset($category_detail->list_name) ? $category_detail->list_name : NULL)?>"/>
    </div>
    <?php if($list_type == ''):?>
    <div class="form-group col-md-3">
      <label for="basic-input">‎</label>
      <select name="list_type" id="list_type" class="form-control req">
        <?php foreach($list_types as $value):?>
         <?php if($value != 'departments'):?>
        	<option value="<?php echo $value;?>"><?php echo $this->haya_model->get_type_name($value);?></option>
           <?php endif;?>  
        <?php endforeach;?>
      </select>
    </div>
    <?php else:?>
    <div class="form-group col-md-3">
      <label for="basic-input">‎</label>
      <select name="list_type" id="list_type" class="form-control">
        <?php foreach($list_types as $value):?>
         <?php if($list_type == $value):?>
        <option value="<?php echo $value;?>" <?php if($list_type	==	$value):?> selected="selected" <?php endif;?>><?php echo $this->haya_model->get_type_name($value);?></option>
        <?php endif;?>
		<?php endforeach;?>
      </select>
    </div>
    <?php endif;?>
    <div class="form-group col-md-3">
      <label for="basic-input">ترتيب&lrm;‎</label>
      <input type="text" name="list_order" id="list_order" class="form-control" value="<?php echo (isset($category_detail->list_order) ? $category_detail->list_order : NULL)?>"/>
    </div>
    <input type="hidden" name="list_parent_id" id="list_parent_id" value="<?php echo $list_parent_id;?>"/>
  </form>
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="add_category('<?php echo $Action;?>');" value="حفظ" />
  </div>
</div>
<script>
function add_category(action)
{
	$('#add-category-form .req').removeClass('parsley-error');
    $('#add-category-form .req').each(function (index, element) {
        if ($.trim($(this).val()) == '')
		{
            $(this).addClass('parsley-error');
        }
    });
    var len = $('#add-category-form .parsley-error').length;

	if(len<=0)
	{
		var str_data = 	$('#add-category-form').serialize();
		
		var request = $.ajax({
		  url: config.BASE_URL+'listing_managment_modified/add_category/'+action,
		  type: "POST",
		  data: str_data,
		  dataType: "html",
		  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(msg)
		  {
		     $('#addingDiag').modal('hide');	  
			 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
			 $('#tableSortable').dataTable().fnDestroy();
			 $('#ajax_action').hide();	
			 create_data_table(1);
		  }
		});
	}
}
</script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Listing_managment_modified extends CI_Controller 
{

//-------------------------------------------------------------------------------	

	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;

//-------------------------------------------------------------------------------
	/*
	* Costructor
	*/
	public function __construct()
	{
		parent::__construct();
		
		// Load Models
		$this->load->model('listing_managment_model', 'listing');
		
		$this->_data['module']	=	$this->haya_model->get_module();
			
		// SET Login USER ID
		$this->_login_userid			=	$this->session->userdata('userid');
		$this->_data['login_userid']	=	$this->_login_userid;	
		
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);	
		$this->_data['list_types']		=	$this->haya_model->get_listmanagment_types();
	}

//-------------------------------------------------------------------------------

	/*
	*
	* Main Page
	*/
	public function index()
	{
		// Something
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Listing Page
	*/
	public function listing($type	=	NULL)
	{
		if($type)
		{
			$this->_data['type']	=	$type;

			// Get List Name By Type $type
			$this->_data['listing']	=	$this->listing->get_all_by_type($type);

			$this->_data['list_type_name']	=	$type;

			$this->load->view('type-listing', $this->_data);
		}
		else
		{
			$this->load->view('listing', $this->_data);
		}
	}
//-------------------------------------------------------------------------------
	/*
	*
	* Add Category form
	* @param $type	str
	*/
	public function add_category_form($type	=	NULL,$list_id	=	NULL,$Action	=	NULL)
	{
		$this->_data['Action']		=	$Action;
		$this->_data['list_type']	=	$type;
		
		// If $Action is set but Not equal to Edit set List_parent_id
		if(isset($Action) AND $Action	!=	'Edit')
		{
			$this->_data['list_parent_id']	=	$list_id;
		}
		
		if($Action	==	'Edit')
		{
			$this->_data['category_detail']	=	$this->listing->get_single_category($list_id);
		}

		// Add Category Form
		$this->load->view('add-category-form',$this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	*
	* Add Category
	*/
	public function add_category($Action)
	{
		$data	=	$this->input->post(); // Get data from POST
		
		if($Action	==	'Add' OR $Action	==	'')
		{
			// insert data
			$list_id	=	$this->listing->add_category($data);
		}
		else
		{
			// update data
			$this->listing->update_category($data['list_id'],$data);
		}
	}
//-----------------------------------------------------------------------
	/*
	* All Categories By ENUM types Listing
	*
	*/
	public function types_listing()
	{
		$segment	=	$this->uri->segment(3);
		$labels		=	$this->config->item('list_types');
		
		$permissions	=	$this->haya_model->check_other_permission(array('1'));
		
		foreach($this->listing->get_all_categories() as $listdata) 
		{
			$typename = $labels[$listdata->list_type]['en'];
			if($permissions[1]['a']	==	1)
			{
				$action =' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment_modified/add_category_form/'.$listdata->list_type.'/0/Add" id="'.$listdata->list_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> ';
			}
			$action .=' <a href="'.base_url().'listing_managment_modified/listing/'.$listdata->list_type.'">عرض الكل ('.$this->listing->total_count($listdata->list_type).')</a>'; 
			
			$arr[] = array(
				"DT_RowId"		=>	$listdata->list_id.'_durar_lm',
                "‫نوع القائمة" 	=>	$this->haya_model->get_type_name($listdata->list_type),
				"إجمالي عدد" 	=>	$action);
				
			unset($action);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//------------------------------------------------------------------------------------
	/*
	* Parent Categories Listing
	*
	*/
	public function parent_listing($type)
	{
		$permissions	=	$this->haya_model->check_other_permission(array('1')); // check permission for this listing
		
		$checkbox	=	'---';

		foreach($this->listing->get_all_categories_by_types($type) as $lc)
		{
			if($lc->list_other)
			{
				$checked	=	'checked="checked"';
			}
			else
			{
				$checked	=	'';
			}
			
			if($type	!=	'departments' && $type	!= 'user_role')
			{	
				  $checkbox ='<div class="other1"> <input type="checkbox" onClick="other(this);" id="'.$lc->list_id.'" name="other'.$lc->list_id.'" '.$checked.'>
				  <div id="show'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;">&#10004;</div>
				  <div id="hide'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;"><i style="color:#CC0000;" class="icon-remove-sign"></i></div>
				  </div>';
			}
				  
			if($lc->list_status	==	'1')
			{
				$actions .=' <a href="#_" onClick="record_status(this);" data-url="'.base_url().'listing_managment_modified/category_status/'.$lc->list_id.'/'.$lc->list_status.'" title="Activate Record"><i style="color:#CC0000;" class="icon-off"></i></a>&nbsp;&nbsp;';
			}
			else
			{
				$actions .=' <a href="#_" onClick="record_status(this);" data-url="'.base_url().'listing_managment_modified/category_status/'.$lc->list_id.'/'.$lc->list_status.'" title="Deactivate Record"><i style="color:#00CC00;" class="icon-ok"></i></a>&nbsp;&nbsp;';
			}
			
			if($permissions[1]['a']	==	1)
			{
				if($type	!=	'departments' && $type	!= 'user_role')
				{
					$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment_modified/add_category_form/'.$lc->list_type.'/'.$lc->list_id.'/Add" id="'.$lc->list_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a>&nbsp;&nbsp;';
				}
			}
			
			if($permissions[1]['u']	==	1)
			{					
				$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment_modified/add_category_form/'.$lc->list_type.'/'.$lc->list_id.'/Edit" id="'.$lc->list_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a>&nbsp;&nbsp;';
			}
			
			if($permissions[1]['d']	==	1)
			{
				if($type	!= 'user_role')
				{
					$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->list_id.'" data-url="'.base_url().'listing_managment_modified/delete/'.$lc->list_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>&nbsp;&nbsp;';
				}
			}
			
			$count = $this->listing->get_list_child_count($lc->list_id);
			
			
			if($type	!=	'departments' && $type	!= 'user_role')
			{		
				$actions .=' <a href="'.base_url().'listing_managment_modified/child_listing/'.$lc->list_type.'/'.$lc->list_id.'">عرض الكل ('.$count.')</a>'; 
			}
			
			$arr[] = array(
				"DT_RowId"		=>	$lc->list_id.'_durar_lm',
				"قائمة البرامج" =>	$lc->list_name,              
				"أخرى" 			=>	$checkbox,
				"الإجراءات" 		=>	$actions);
				
			unset($actions);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Child Listing
	*/
	public function child_listing($type	=	NULL,$listid	=	NULL)
	{
		//$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['parent_id']	=	$listid;
		$this->_data['list_type']	=	$type;

		$this->load->view('child-type-listing', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Child Type Listing
	*/
	public function child_types_listing($list_type,$parent_id)
	{
			$permissions	=	$this->haya_model->check_other_permission(array('72'));
			
			foreach($this->listing->get_all_child_categories($parent_id) as $lc)
			{
				/*$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment/add_child_qaima/'.$lc->list_id.'/" id="'.$lc->list_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> ';*/
				if($permissions[72]['d']	==	1)
				{
					$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->list_id.'" data-url="'.base_url().'listing_managment_modified/delete/'.$lc->list_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';
				}
				if($permissions[72]['u']	==	1)
				{
					$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment_modified/add_category_form/'.$lc->list_type.'/'.$lc->list_id.'/Edit" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a>&nbsp;&nbsp;';	
				}
				//$count = $this->listing->get_list_child_count($lc->list_id);
						
				//$actions .=' <a href="'.base_url().'listing_managment/sub_child_listing/'.$lc->list_id.'">عرض الكل ('.$count.')</a>'; 
				$arr[] = array(
					"DT_RowId"		=>	$lc->list_id.'_durar_lm',
					"‫نوع القائمة" 	=>	$lc->list_name,              
					"الإجراءات" 		=>	$actions);
					
				unset($actions);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
		}

//-------------------------------------------------------------------------------
	/*
	*
	* Subchild Listing Page
	*/

	public function sub_child_listing($type	=	NULL,$listid	=	NULL)
	{
		$this->_data['parent_id']	=	$listid;
		$this->_data['list_type']	=	$type;

		$this->load->view('subchilds-type-listing', $this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	* Delete Category
	*
	*/

	public function delete($listid,$type)
	{
		$this->listing->delete($listid);

		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');

		redirect(base_url().'listing_managment_modified/listing/'.$type);
		exit();
	}
//-------------------------------------------------------------------------------
	/*
	* ACTIVE / DEACTIVE status
	* @param int $list_id
	* @param int $status
	*/

	public function category_status($list_id,$status)
	{
		if($status	==	'1')
		{
			$update_status	=	'0';
		}
		else
		{
			$update_status	=	'1';
		}
		
		// Active OR Deactive Categories status
		$this->listing->category_status($list_id,$update_status);
	}
//-------------------------------------------------------------------------------
	/*
	* Add Check Box value for Extra things
	* UPDATE form Data for OTHER list
	*/
	public function other()
	{
		$list_id	=	$this->input->post("id");
		$entry		=	$this->input->post("entry");

		$data		=	array('list_other' => $entry);

		$this->listing->update_category_other_value($list_id, $data);
	}

//-------------------------------------------------------------------------------
	/*
	* To check if the request is an AJAX request
	*
	*/
	function is_ajax()
	{
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
	}
//-------------------------------------------------------------------------------
	/*
	* Logout
	* Destroy All SESSIONS
	*/
	public function logout()
	{
		// Destroy all sessions
		$this->session->sess_destroy();
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('userinfo');

		redirect(base_url());
		exit();
	}
//------------------------------------------------------------------------------------
/* End of file listing_management_modified.php */
/* Location: ./application/modules/listing_management_modified/controller/listing_management_modified.php */
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Listing_managment_model extends CI_Model {

	/*
	* Properties
	*/
	private $_table_list_management;

//----------------------------------------------------------------------
	/*
	* Constructor
	*/

	function __construct()
    {

        parent::__construct();

		//Load Table Names from Config
		$this->_table_list_management	=	$this->config->item('table_listmanagement');
    }
//----------------------------------------------------------------------
	/*
	* Total No of CHILDS count
	* @param int $listid
	* return int
	*/

	function get_list_child_count($listid)
	{
		$this->db->where('list_parent_id',$listid);
		$this->db->where('delete_record','0');
		$this->db->where('list_status','1');
		$query = $this->db->get($this->_table_list_management);

		return $query->num_rows();
	}

//----------------------------------------------------------------------
	/*
	* Get All categories By their Types
	* @param str $type
	* return OBJECT
	*/
	function get_all_by_type($type)
	{
		$this->db->where('list_type',$type);
		$this->db->order_by("list_order", "DESC");

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* Get single Category 
	* @param $listid int
	* return OBJECT
	*/

	function get_single_category($listid)
	{
		$this->db->where('list_id',$listid);

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* Get total Parents Count
	* @param str $type
	* return int
	*/
	public function total_count($type)
	{
		$this->db->where('list_type',$type);
		$this->db->where('list_parent_id','0');
		$this->db->where('delete_record','0');
		$this->db->where('list_status','1');
		$query = $this->db->get($this->_table_list_management);

		return $query->num_rows();
	}
//----------------------------------------------------------------------
	/*
	* Delete
	*/

	function delete($listid)
	{
		$json_data	=	json_encode(array('record'=>'delete','list_id'	=>	$listid));
		
		$data	=	array('delete_record'=>'1');
		
		$this->db->where('list_id', $listid);

		$this->db->update($this->_table_list_management,$json_data,$this->session->userdata('userid'),$data);

		return TRUE;
	}
	
//----------------------------------------------------------------------
     /* UPDATE category status
     * @param int $list_id
	 * @param int $status
     * return TRUE
	 */

	function category_status($list_id,$status)
	{
		$json_data	=	json_encode(array('list_id'	=>	$list_id,'list_status'=>$status));
		
		$data	=	array('list_status'	=>	$status);
		
		$this->db->where('list_id', $list_id);

		$this->db->update($this->_table_list_management,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}

//------------------------------------------------------------------------
     /* UPDATE category
     * @param int $list_id
	 * @param array $data
     * return TRUE
	 */
	function update_category($list_id,$data)
	{
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('list_id', $list_id);

		$this->db->update($this->_table_list_management,$json_data,$this->session->userdata('userid'),$data);

		return TRUE;
	}
//------------------------------------------------------------------------
    /*
     * UPDATE category OTHER EXTRA value
     * @param int $list_id
	 * @param array $data
     * return TRUE
     */
	function update_category_other_value($list_id, $data)
	{
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('list_id', $list_id);
		$this->db->update($this->_table_list_management,$json_data,$this->session->userdata('userid'),$data);

		return TRUE;
	}

//----------------------------------------------------------------------
	/*
	*
	*/
	function get_list_type()
	{
		$this->db->select("list_type"); 
		$this->db->group_by("list_type");
		$this->db->order_by("list_order", "DESC");

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();

		}
	}
//----------------------------------------------------------------------
	/*
	* Get Category Type Name
	* @param int $parent_id
	* return str
	*/
	function get_type_name($parent_id)
	{
		$this->db->select("list_type"); 
		$this->db->where("list_id",$parent_id);
		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->list_type;
		}
	}
//----------------------------------------------------------------------
	/*
	* Get all categories
	* 
	*/

	function get_all_categories()
	{
		$this->db->select("list_type,list_id"); 
		$this->db->group_by("list_type"); 
		$this->db->order_by("list_order", "DESC");
		$this->db->where("list_parent_id", "0");
		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* Get all parent categories by TYPES
	* 
	*/

	function get_all_categories_by_types($type)
	{
		$this->db->select("list_id,list_name,list_type,list_other,list_status");
		$this->db->where_in('list_type',$type);
		$this->db->where("list_parent_id", "0");
		$this->db->where("delete_record", "0");
		$this->db->order_by("list_order", "DESC");
		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* Get all child categories
	* @param int $parent_id
	* return OBJECT
	*/

	function get_all_child_categories($parent_id)
	{
		$this->db->select('list_id,list_name,list_type,list_other,list_status,delete_record');
		$this->db->where('list_parent_id',$parent_id);
		$this->db->where_in('delete_record','0');
		$this->db->order_by('list_order','DESC');
		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* GET category Detail According to $type
	* @param $type	str
	* return OBJECT
	*/
	function get_category_type_detail($type)
	{
		$this->db->select('list_id,list_parent_id,list_order,list_other,list_type,list_name,list_status,delete_record');
		$this->db->where("list_type",$type);
		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------

	/*
	* Insert User Record
	* @param array $data
	* return Last Inserted Record ID
	*/

	function add_category($data)
	{
		$json_data	=	json_encode(array('data'=>$data));
		$this->db->insert($this->_table_list_management,$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
	}
//----------------------------------------------------------------------
/* End of file listing_management_model.php */
/* Location: ./application/modules/listing_management_modified/model/listing_management_model.php */	
}
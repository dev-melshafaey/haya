<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <form action="<?php echo current_url();?>" method="POST" id="validate_form" name="validate_form" autocomplete="off">
          <div class="col-md-12">
            <div class="panel panel-default panel-block">
              <div class="list-group">
                <div class="list-group-item" id="input-fields">                  
                  <div class="form-group col-md-5">
                    <label for="basic-input">اسم الفرع</label>
                    <input type="text" class="form-control req" name="branch_name" id="branch_name" value="<?php echo (isset($single_branche->branch_name) ? $single_branche->branch_name : NULL);?>" placeholder="اسم الفرع"/>
                  </div>
                  <div class="form-group col-md-5">
                    <label for="basic-input">فرع كود</label>
                    <input type="text" class="form-control req" name="branch_code"  id="branch_code" value="<?php echo (isset($single_branche->branch_code) ? $single_branche->branch_code : NULL);?>" placeholder="فرع كود"/>
                  </div>
                 <div class="form-group col-md-5">
                    <label for="basic-input">محافظة</label>
                    <?php reigons('province',$single_branche->province); ?>
                  </div>
                  <div class="form-group col-md-5">
                    <label for="basic-input">الولاية</label>
                   <?php election_wilayats('wilayats',$single_branche->wilayats); ?>
                  </div>
                  <div class="form-group col-md-5">
                    <label for="basic-input">عنوان الفرع</label>
                   <textarea class="form-control" name="branch_address" id="branch_address" style="margin: 0px; height: 177px; width: 488px;"><?php echo (isset($single_branche->branch_address) ? $single_branche->branch_address : NULL);?></textarea>
                  </div>
                  <div class="form-group col-md-5">
                    <label for="basic-input">وضع</label>
                   <select name="status" class="form-control">
                    <option value="1" <?php if($single_branche->status	==	'1'):?> selected="selected" <?php endif;?>>نشط</option>
                    <option value="0" <?php if($single_branche->status	==	'0'):?> selected="selected" <?php endif;?>>دي نشط</option>

                  </select>
                  </div>
<br clear="all" />
                  <div class="form-group">
                    <?php if($branch_id):?>
                	<input type="hidden" name="branch_id"  value="<?php echo $branch_id;?>" />
                    <?php endif;?>
                    <input type="button" name="save_data_form" id="save_data_form" class="btn btn-success btn-lrg" value="حفظ" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<script>
    $(function(){        
      var branchid = '<?PHP echo $branchid; ?>';
	
    } );
</script>
<?php $this->load->view('common/footer'); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
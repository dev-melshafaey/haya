<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	/*
	* Properties
	*/

	private $_table_users;	

//----------------------------------------------------------------------

	/*
	* Constructor
	*/

	function __construct()
    {
        parent::__construct();

		//Load Table Names from Config
		$this->_table_users 			=  $this->config->item('table_users');
    }
//----------------------------------------------------------------------

	/*
	* Get all Admin users
	* return Object
	*/

	function get_all_admin_users()
	{

		$query = $this->db->get('admin_users');
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return '0';
		}
	}
//----------------------------------------------------------------------

	/*
	* Get All Records From Giving Table
	*/

	public function get_rows($table_name)
	{
		$count_all = $this->db->get($table_name);

		if($count_all->num_rows() > 0)
		{
			return $count_all->num_rows();
		}
	}

//----------------------------------------------------------------------

	/*
	* Insert Applied Product Record into Database
	* Return True 
	*/

	public function applied_donation($data)
	{
		$this->db->insert($this->_table_applied_donation,$data);

		return TRUE;
	}

//----------------------------------------------------------------------

	/*
	* Get Applied Product Record into Database
	* Return True 
	*/

	public function get_applied_detail($user_id,$donate_id)
	{
		$this->db->where('user_id',$user_id);
		$this->db->where('donation_id',$donate_id);

		$query = $this->db->get($this->_table_applied_donation);

		if ($query->num_rows() > 0)
		{
			return $query->row()->donation_id;
		}
	}

//----------------------------------------------------------------------

	/*
	* Search Applied Products if 
	* Return True 
	*/

	public function donation_id_exists()
	{

		$this->db->where('user_id',$this->session->userdata('user_id'));
		$this->db->select('donation_id');
		$query = $this->db->get($this->_table_applied_donation);

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* count of all applicants
	*/

	public function jamee_ul_maamlaat()
	{
		return $this->db->count_all_results('applicants');
	}
//----------------------------------------------------------------------
	/*
	*
	*/

	public function mamlaat_types_count_and_sum()
	{
		$q = $this->db->query('SELECT a.loan_category_name,a.`loan_category_id`,c.`loan_limit`,
		COUNT(c.`applicant_id`) AS lcount,
		SUM(c.`loan_amount`) AS lamount
		FROM loan_category AS a
		JOIN loan_calculate AS b ON a.`loan_category_id`=b.`loan_category_id`
		JOIN applicant_loans AS c ON b.`loan_category_id`=c.`loan_limit`
		GROUP BY a.`loan_category_id`');
		
		return $q->result_array();
	}
//----------------------------------------------------------------------
	/*
	*
	*/
	public function mamlaat_types_count_and_sum_branch($branchid,$loanid)
	{
		$q = $this->db->query("SELECT a.loan_category_name,
		COUNT(c.`applicant_id`) AS lcount,
		SUM(c.`loan_amount`) AS lamount
		FROM loan_category AS a
		JOIN loan_calculate AS b ON a.`loan_category_id`=b.`loan_category_id`
		JOIN applicant_loans AS c ON b.`loan_category_id`=c.`loan_limit`
		JOIN `applicants` AS d ON c.`applicant_id`=d.`applicant_id`
		WHERE d.`branchid`='".$branchid."' AND a.`loan_category_id`='".$loanid."'
		GROUP BY a.`loan_category_id`");
		
		return $q->result_array();
	
	}
//----------------------------------------------------------------------
	/*
	*
	*/

	public function day_month_year()
	{
		$date = date('Y-m-d');
		$qx = $this->db->query("SELECT 
		(SELECT COUNT(applicant_id) FROM applicants WHERE DAY(`applicant_regitered_date`)=DAY('".$date."')) AS count_day,
		(SELECT COUNT(applicant_id) FROM applicants WHERE MONTH(`applicant_regitered_date`)=MONTH('".$date."')) AS count_month,
		(SELECT COUNT(applicant_id) FROM applicants WHERE YEAR(`applicant_regitered_date`)=YEAR('".$date."')) AS count_year 
		FROM applicants LIMIT 0,1;");
		
		return $qx->row();	

	}
//----------------------------------------------------------------------
	/*
	*
	*/

	public function mamlaat_gair()
	{
		$this->db->select('applicants.*');
		$this->db->from('applicants');
		$this->db->join('zyarat_awalia AS snd',"snd.applicant_id=applicants.applicant_id");
		$this->db->where('snd.is_complete','1');
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
	}
//----------------------------------------------------------------------
	/*
	*
	*/

	public function qarar_ul_janna()
	{
		$qx = $this->db->query("SELECT comitte_id FROM comitte_decision WHERE commitee_decision_type='approved';");
		
		return $qx->num_rows();
	}
//----------------------------------------------------------------------
	/*
	*
	*/	
	public function zayarat_awaliya1()
	{
		$qp = $this->db->query("SELECT 
		 COUNT(a.zyarat_id) AS ck,
		 SUM(b.loan_amount) AS lamo
		FROM
		 zyarat_awalia AS a,
		 applicant_loans AS b 
		WHERE a.`applicant_id`=b.`applicant_id` AND a.is_complete = '1';");
		
		return $qp->row(); 
	}
//----------------------------------------------------------------------
	/*
	*
	*/	
	public function zayarat()
	{
		$qp = $this->db->query("SELECT COUNT(a.comitte_id) AS ck, SUM(b.loan_amount) AS lamo 
		FROM comitte_decision AS a, applicant_loans AS b 
		WHERE a.`applicant_id`=b.`applicant_id` AND a.committee_decision_is_aproved = 'queries';");
		
		return $qp->row(); 
	}
//----------------------------------------------------------------------
	/*
	*
	*/	
	public function ziker()
	{
		$query = $this->db->query("SELECT COUNT(a.zyarat_id) AS ck FROM zyarat_awalia AS a, applicants AS b WHERE a.`applicant_id`=b.`applicant_id` AND a.is_complete = '1' AND b.`applicant_gender` = 'ذكر';");
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return '0';
		}
	}
//----------------------------------------------------------------------
	/*
	*
	*/
	public function unsaa()
	{
		$query = $this->db->query("SELECT COUNT(a.zyarat_id) AS ck FROM zyarat_awalia AS a, applicants AS b WHERE a.`applicant_id`=b.`applicant_id` AND a.is_complete = '1' AND b.`applicant_gender` = 'أنثى';");
		
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return '0';
		}
	}
//----------------------------------------------------------------------
	/*
	*
	*/
	public function mushtarik()
	{
		$query = $this->db->query("SELECT COUNT(a.zyarat_id) AS ck FROM zyarat_awalia AS a, applicants AS b WHERE a.`applicant_id`=b.`applicant_id` AND a.is_complete = '1' AND b.`applicant_type` = 'مشترك';");
		
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return '0';
		}
	}
//----------------------------------------------------------------------
	/*
	*
	*/	
	public function marajeen_male_femail_count()
	{
		$query = $this->db->query("SELECT (SELECT COUNT(`applicantid`) FROM main_applicant WHERE `applicanttype`='ذكر') AS zakar, (SELECT COUNT(applicantid) FROM main_applicant WHERE `applicanttype`='أنثى') AS unsa FROM applicants LIMIT 0,1;");
		
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return '0';
		}
	}
//----------------------------------------------------------------------
	/*
	*
	*/
	public function marajeen_ferd_mushtariq_count()
	{
		$query = $this->db->query("SELECT (SELECT COUNT(`tempid`) FROM main WHERE `user_type`='فردي') AS ferd, (SELECT COUNT(`tempid`) FROM main WHERE `user_type`='مشترك') AS mushtariq FROM main LIMIT 0,1;");
		
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return '0';
		}
	}
//----------------------------------------------------------------------
	/*
	*
	*/	
	public function al_mubaligh()
	{
		$qp = $this->db->query("SELECT SUM(b.loan_amount) AS lamo 
		FROM comitte_decision AS a, applicant_loans AS b 
		WHERE a.`applicant_id`=b.`applicant_id` AND a.commitee_decision_type = '';");
		
		return $qp->row(); 
	}
//----------------------------------------------------------------------
	/*
	*
	*/
	public function al_muafqaat()
	{
		$qp = $this->db->query("SELECT SUM(b.loan_amount) AS lamo 
		FROM comitte_decision AS a, applicant_loans AS b 
		WHERE a.`applicant_id`=b.`applicant_id`;");
		
		return $qp->row(); 
	}
//----------------------------------------------------------------------
	/*
	*
	*/	
	public function getallgenderfrommuragen()
	{
		$countMur 				= $this->db->query("SELECT tempid FROM main WHERE tempid_value!='0';");
		$arr['murgaeenCount'] 	= $countMur->num_rows();
		$singleQuery 			= $this->db->query("SELECT tempid FROM main WHERE user_type='فردي' AND tempid_value!='0';");
		$arr['single'] 			= $singleQuery->num_rows();
		$singleManQuery 		= $this->db->query("SELECT m.tempid FROM main AS m, main_applicant AS ma WHERE m.`tempid`=ma.`tempid` AND m.`user_type`='فردي' AND ma.`applicanttype`='ذكر' AND ma.`marital_status`!='0';");
		$arr['singleMan'] 		= $singleManQuery->num_rows();
		$singleWomanQuery		= $this->db->query("SELECT m.tempid FROM main AS m, main_applicant AS ma WHERE m.`tempid`=ma.`tempid` AND m.`user_type`='فردي' AND ma.`applicanttype`='أنثى' AND ma.`marital_status`!='0';");
		$arr['singleWomen'] 	= $singleWomanQuery->num_rows();		
		
		$partnerQuery 			= $this->db->query("SELECT tempid FROM main WHERE user_type='مشترك' AND tempid_value!='0';");
		$arr['partner'] 		= $singleQuery->num_rows();
		$partnerManQuery 		= $this->db->query("SELECT m.tempid FROM main AS m, main_applicant AS ma WHERE m.`tempid`=ma.`tempid` AND m.`user_type`='مشترك' AND ma.`applicanttype`='ذكر' AND ma.`marital_status`!='0';");
		$arr['partnerMan'] 		= $partnerManQuery->num_rows();
		$partnerWomanQuery 		= $this->db->query("SELECT m.tempid FROM main AS m, main_applicant AS ma WHERE m.`tempid`=ma.`tempid` AND m.`user_type`='مشترك' AND ma.`applicanttype`='أنثى' AND ma.`marital_status`!='0';");
		$arr['partnerWomen'] 	= $partnerWomanQuery->num_rows();	
				
		return $arr;
	}
//----------------------------------------------------------------------
	/*
	*
	*/	
	public function branch_list_for_dashboard()
    {
        $mbc = $this->db->query("SELECT `branch_name` ,`branch_name` ,`branch_id` FROM `branches` ORDER BY branch_name ASC;");
        
        return $mbc->result();
    }
//----------------------------------------------------------------------
	/*
	*
	*/	
	public function getallapplicantsforlist()
	{
		$query = "SELECT `admin_users`.`firstname`, `admin_users`.`lastname`, `admin_users`.`id`,
(SELECT COUNT(tempid) FROM main WHERE userid=`admin_users`.`id`) AS murgeencount
FROM `main` INNER JOIN `admin_users` ON (`main`.`userid` = `admin_users`.`id`) WHERE main.`tempid_value`!='0' GROUP BY `admin_users`.`id` ORDER BY murgeencount DESC LIMIT 6";
		$q = $this->db->query($query);
		return $q->result();
	}
//----------------------------------------------------------------------
	/*
	*
	*/	
	public function getallgenderforTasgeel()
	{
			$singleQuery = $this->db->query("SELECT applicant_id FROM applicants WHERE applicant_type='فردي';");
		$arr['single'] = $singleQuery->num_rows();			
			$singleManQuery = $this->db->query("SELECT applicant_id FROM applicants WHERE applicant_type='فردي' AND `applicant_gender`='ذكر';");
		$arr['singleMan'] = $singleManQuery->num_rows();
			$singleWomanQuery = $this->db->query("SELECT applicant_id FROM applicants WHERE applicant_type='فردي' AND `applicant_gender`='أنثى'; ");
		$arr['singleWomen'] = $singleWomanQuery->num_rows();		
		
			$partnerQuery = $this->db->query("SELECT applicant_id FROM applicants WHERE applicant_type='مشترك';");
		$arr['partner'] = $partnerQuery->num_rows();
		
		
			$partnerMale = $this->db->query("SELECT applicant_id FROM applicants WHERE applicant_type='مشترك' AND `applicant_gender`='ذكر';");
			$partnerFemale = $this->db->query("SELECT applicant_id FROM applicants WHERE applicant_type='مشترك' AND `applicant_gender`='أنثى';");
		
			$partnerManQuery = $this->db->query("SELECT a.`applicant_id` FROM applicants AS a, applicant_partners AS ap WHERE a.`applicant_id`=ap.`applicant_id` AND a.`applicant_type`='مشترك' AND ap.`partner_gender`='ذكر';");
		$arr['partnerMan'] = $partnerManQuery->num_rows()+$partnerMale->num_rows();
			$partnerWomanQuery = $this->db->query("SELECT a.`applicant_id` FROM applicants AS a, applicant_partners AS ap WHERE a.`applicant_id`=ap.`applicant_id` AND a.`applicant_type`='مشترك' AND ap.`partner_gender`='أنثى';");
		$arr['partnerWomen'] = $partnerWomanQuery->num_rows()+$partnerFemale->num_rows();			
		return $arr;
	}
	
//------------------------------------------------------

	/*
	*
	* Get count of mamlaat created by admin users
	*/
	public function al_muzafeen_count()
	{
		$query = $this->db->query("
		SELECT
		`admin_users`.`firstname`
		, `admin_users`.`lastname`
		, COUNT(`applicants`.`applicant_id`) AS total
		FROM
		`admin_users`
		INNER JOIN `applicant_process_log` 
			ON (`admin_users`.`id` = `applicant_process_log`.`userid`)
		INNER JOIN `applicants` 
			ON (`applicant_process_log`.`applicantid` = `applicants`.`applicant_id`)
			WHERE applicant_process_log.stepsid = '1' GROUP BY `admin_users`.`id` ORDER BY total DESC;");
		
		return $query->result();
	}
	
//------------------------------------------------------

	/*
	*
	*
	*/
	public function bernamij_almasadaat()
	{
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='charity_type' AND list_status='1' AND list_name!='اضافة اليتيم'  AND list_name!='اضافة الكفيل' AND delete_record='0'");
		$result_1	=	$query_1->result();
		
		$almasadaat	=	array();
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(applicantid) as total FROM `ah_applicant` WHERE charity_type_id='".$type->list_id."' AND isdelete='0'");
				$result_2	=	$query_2->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2);
			}
		}
		
		return $data;
	}
//------------------------------------------------------

	/*
	* 
	* 
	*/
	public function bernamij_almasadaat_branch()
	{
		$query_b 	=	$this->db->query("SELECT branchid,branchname FROM `ah_branchs` WHERE delete_record='0'");
		$result_b	=	$query_b->result();
		
						
		$almasadaat	=	array();
		
		if ($query_b->num_rows() > 0)
		{
			foreach($result_b as $branch)
			{
				$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='charity_type' AND list_status='1'  AND list_name!='اضافة اليتيم'  AND list_name!='اضافة الكفيل' AND delete_record='0'");
				$result_1	=	$query_1->result();
				
				if ($query_1->num_rows() > 0)
				{
					foreach($result_1 as $type)
					{
						$query_2 	=	$this->db->query("SELECT COUNT(applicantid) as total FROM `ah_applicant` WHERE charity_type_id='".$type->list_id."' AND branchid='".$branch->branchid."' AND isdelete='0'");
						$result_2	=	$query_2->row()->total;
						
						$almasadaat[$branch->branchname][$type->list_name]	=	 $result_2;
					}
				}
				
			}
		}
		
		return $almasadaat;
	}
	
//------------------------------------------------------

	/*
	* Get all data Bernamij Almasadaat FOR Flow Charts
	* return ARRAY
	*/
	public function bernamij_almasadaat_new()
	{
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='charity_type' AND list_status='1'  AND list_name!='اضافة اليتيم'  AND list_name!='اضافة الكفيل' AND delete_record='0'");
		$result_1	=	$query_1->result();
		
		$almasadaat		=	array();
		$almasadaat_all	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_b 	=	$this->db->query("SELECT branchid,branchname FROM `ah_branchs` WHERE delete_record='0'");
				$result_b	=	$query_b->result();
				
				$query_all 	=	$this->db->query("SELECT COUNT(applicantid) as total FROM `ah_applicant` WHERE charity_type_id='".$type->list_id."' AND isdelete='0'");
				$result_all	=	$query_all->row()->total;
				
				$almasadaat_all[]	=	 $result_all;
				$newRes += $result_all;				
				
				foreach($result_b as $branch)
				{
					$query_2 	=	$this->db->query("SELECT COUNT(applicantid) as total FROM `ah_applicant` WHERE charity_type_id='".$type->list_id."' AND branchid='".$branch->branchid."' AND isdelete='0'");
					$result_2	=	$query_2->row()->total;
					$almasadaat[]	=	 $result_2;
					
				}				
				$almasadaat[]	=	 $query_all->row()->total;				

				$data1[]	=	array('name'	=>	$type->list_name,'data'	=> '['.implode($almasadaat,',').']');
				
				unset($almasadaat);
			}
			
			$almasadaat_all[] = $newRes;
			unset($almasadaat_all,$newRes);
			$data2[]	=	array('name'	=>	'عرض الكل','data'	=> '['.implode($almasadaat_all,',').']');
		}
		
		$data	=	array_merge($data1,$data2);
		return $data;
	}

//------------------------------------------------------

	/*
	* Get Module icon
	* @param $moduleid integer
	* return String
	*/
	public function get_icon($moduleid)
	{
		$query	=	$this->db->query("SELECT module_icon FROM `mh_modules` WHERE moduleid='".$moduleid."'");
		
		if($query->num_rows() > 0)
		{
			return $query->row()->module_icon;
		}		
	}
//------------------------------------------------------

	/*
	* Get all YATEEM and KAFEEL
	* RETURN YATEM Count and KAFEEL Count
	*/
	public function yateem_kafeel()
	{
		$query_1	=	$this->db->query("SELECT COUNT(orphan_id) AS total_yateem FROM `ah_yateem_info` WHERE delete_record='0'");
		
		$total_rocords	=	array();
		
		if($query_1->num_rows() > 0)
		{
			$total_rocords['يتيم']	=	$query_1->row()->total_yateem;
		}
		
		$query_2	=	$this->db->query("SELECT COUNT(sponser_id) AS total_kafeel FROM `ah_sponser_info` WHERE delete_record='0'");
		
		if ($query_2->num_rows() > 0)
		{
			$total_rocords['الكفيل']	=	$query_2->row()->total_kafeel;
		}
		
		return $total_rocords;
	}
//------------------------------------------------------

	/*
	* Get all users with there types 
	* Count Users
	* return ARRAY
	*/
	public function users()
	{
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='user_role' AND list_status='1' AND delete_record='0'");
		$result_1	=	$query_1->result();
		
		$query_u 	=	$this->db->query("SELECT userid FROM `ah_users` WHERE delete_record='0'");
		
		$users	=	array();
		
		$users['مجموع المسنخدمين']	=	 $query_u->num_rows();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(userid) as total FROM `ah_users` WHERE userroleid='".$type->list_id."' AND delete_record='0'");
				$result_2	=	$query_2->row()->total;
				
				$users[$type->list_name]	=	 $result_2;
			}
		}
		
		return $users;
	}
//------------------------------------------------------

	/*
	* Get all users with there types 
	* Count Users
	* return ARRAY
	*/
	public function users_pie_chart()
	{
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='user_role' AND list_status='1' AND delete_record='0'");
		$result_1	=	$query_1->result();
		
		$query_u 	=	$this->db->query("SELECT userid FROM `ah_users` WHERE delete_record='0'");
		
		$users	=	array();
		
		$users['مجموع المسنخدمين']	=	 $query_u->num_rows();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(userid) as total FROM `ah_users` WHERE userroleid='".$type->list_id."' AND delete_record='0'");
				$result_2	=	$query_2->row()->total;
				
				$users[$type->list_name]	=	 $result_2;
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2);
			}
		}
		
		return $data;
	}
	
//------------------------------------------------------
	/*
	* Get all Branches
	* return ARRAY
	*/	
	public function all_branches()
	{
		$query_b 	=	$this->db->query("SELECT branchname FROM `ah_branchs` WHERE delete_record='0'");
		
		if ($query_b->num_rows() > 0)
		{
			$result_b	=	$query_b->result();
			
			
			foreach($result_b as $bra)
			{
				$data[]	=	"'$bra->branchname'";
			}
			
			$data[]	=	"'عرض الكل'";
			
			return implode($data,',');
		}
	}
//------------------------------------------------------
	/*
	* Get all Transactions
	* return Total number of Transactions
	*/
	public function get_all_transactions()
	{
		$current_year	=	date('Y');
		
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("YEAR(ah_applicant.registrationdate)",$current_year);
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
	}
	
//------------------------------------------------------
	/*
	* Get all Items
	* return Total number of Count
	*/
	public function get_all_items()
	{
		$this->db->select('ahl.list_name as category,ahl1.list_name as subcategory,ahi.itemname,ahi.itemphoto,ahi.itemid');
		$this->db->from('ah_inventory as ahi');			
		$this->db->join('ah_listmanagement AS ahl','ahl.list_id = ahi.list_category');
		$this->db->join('ah_listmanagement AS ahl1','ahl1.list_id = ahi.list_subcategory');			
		$this->db->where("ahi.itemstatus",1);
		$this->db->order_by('ahi.itemname','DESC');
		$query = $this->db->get();
				
		if ($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
	}
//------------------------------------------------------

	/*
	* Get all NOOH SHERKA records
	* return ARRAY
	*/
	public function nooh_sherka()
	{
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='company_type' AND list_status='1' AND delete_record='0'");
		$result_1	=	$query_1->result();
		
		$nooh_sherka	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(companyid) AS total FROM ah_company WHERE company_type_listmanagement='".$type->list_id."'");
				$result_2	=	$query_2->row()->total;
				
				$nooh_sherka[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'count'	=> (int)$result_2);
			}
		}
		
		return $data;
	}
//------------------------------------------------------

	/*
	* Get all NOOH SHERKA records
	* return ARRAY
	*/
	public function nooh_sherka_pie_chart()
	{
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='company_type' AND list_status='1' AND delete_record='0'");
		$result_1	=	$query_1->result();
		
		$nooh_sherka	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(companyid) AS total FROM ah_company WHERE company_type_listmanagement='".$type->list_id."'");
				$result_2	=	$query_2->row()->total;
				
				$nooh_sherka[$type->list_name]	=	 $result_2;
				
				//$data[]	=	array('name'	=>	$type->list_name,'count'	=> (int)$result_2);
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2);
			}
		}
		
		return $data;
	}
	
//------------------------------------------------------

	/*
	* Get all NOOH SHERKA records
	* return ARRAY
	*/
	public function nooh_sherka_company_status($companystatus	=	NULL)
	{
		$query	=	$this->db->query("SELECT * FROM `ah_company` WHERE companystatus	=	'".$companystatus."';");
		
		/*$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='company_type' AND list_status='1' AND delete_record='0'");
		$result_1	=	$query_1->result();
		
		$nooh_sherka	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(companyid) AS total FROM ah_company WHERE company_type_listmanagement='".$type->list_id."'");
				$result_2	=	$query_2->row()->total;
				
				$nooh_sherka[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'count'	=> (int)$result_2);
			}
		}
		
		return $data;*/
	}
//------------------------------------------------------

	/*
	* 
	* 
	*/
	public function yateem_kafeel_count_by_branch()
	{
		$query_a 	=	$this->db->query("SELECT branchid,branchname FROM `ah_branchs` WHERE delete_record='0'");
		$result_a	=	$query_a->result();

						
		$data	=	array();
		
		if ($query_a->num_rows() > 0)
		{
			foreach($result_a as $branch)
			{
				$query_b 	=	$this->db->query("SELECT COUNT(orphan_id) AS total_yateem FROM `ah_yateem_info` WHERE user_branch_id='".$branch->branchid."' AND delete_record='0'");
				$result_b	=	$query_b->row()->total_yateem;
				
				$data['عدد الأيتام في الفرع'][$branch->branchname]	=	 $result_b;
			}
			
			foreach($result_a as $branch)
			{
				$query_c 	=	$this->db->query("SELECT COUNT(sponser_id) AS total_kafeel FROM `ah_sponser_info` WHERE user_branch_id='".$branch->branchid."' AND delete_record='0'");
				$result_c	=	$query_c->row()->total_kafeel;
				
				$data['عدد الكفلى في الفرع'][$branch->branchname]	=	 $result_c;
			}
		}

		return $data;
	}

//------------------------------------------------------

	/*
	* Get all YATEEM and KAFEEL
	* RETURN YATEM Count and KAFEEL Count
	*/
	public function yateem_kafeel1234()
	{
		$query_1	=	$this->db->query("SELECT COUNT(orphan_id) AS total_yateem FROM `ah_yateem_info` WHERE delete_record='0'");
		
		$total_rocords	=	array();
		
		if($query_1->num_rows() > 0)
		{
			$total_rocords['يتيم']	=	$query_1->row()->total_yateem;
		}
		
		$query_2	=	$this->db->query("SELECT COUNT(sponser_id) AS total_kafeel FROM `ah_sponser_info` WHERE delete_record='0'");
		
		if ($query_2->num_rows() > 0)
		{
			$total_rocords['الكفيل']	=	$query_2->row()->total_kafeel;
		}
		
		return $total_rocords;
	}
	
	public function getReqData($managerid,$moduleid,$requeststatus)
	{
		$this->db->select("ah_request_form.`fordate`,ah_request_form.`moduleid`,ah_request_form.`requestid`,ah_request_form.`requestdate`,ah_userprofile.`fullname`,ah_userprofile.`idcardNumber`,ah_users.`id_number`,pr.`list_name` AS profession, pro.`list_name` AS subprofession");
		$this->db->from("ah_request_form");
		$this->db->join("ah_userprofile","ah_userprofile.userid=ah_request_form.userid");
		$this->db->join("ah_users","ah_users.userid = ah_userprofile.userid");
		$this->db->join("ah_listmanagement AS pr","ah_userprofile.profession=pr.list_id");
		$this->db->join("ah_listmanagement AS pro","ah_userprofile.sub_profession=pro.list_id");
		$this->db->where("ah_request_form.managerid", $managerid);
		$this->db->where("ah_request_form.moduleid", $moduleid);
		$this->db->where("ah_request_form.requeststatus", $requeststatus);		
		$query = $this->db->get();
		return $query->result();
	}
	
	public function allRequestAccordingToUser($userid,$moduleid,$roleid)
	{
		$this->db->select("ah_request_form.`fordate`,ah_request_form.`actiondate`, ah_request_form.requeststatus,ah_request_form.`moduleid`,ah_request_form.`requestid`,ah_request_form.`requestdate`,ah_userprofile.`fullname`,ah_userprofile.`idcardNumber`,ah_users.`id_number`,pr.`list_name` AS profession, pro.`list_name` AS subprofession");
		$this->db->from("ah_request_form");
		$this->db->join("ah_userprofile","ah_userprofile.userid=ah_request_form.userid");
		$this->db->join("ah_users","ah_users.userid = ah_userprofile.userid");
		$this->db->join("ah_listmanagement AS pr","ah_userprofile.profession=pr.list_id");
		$this->db->join("ah_listmanagement AS pro","ah_userprofile.sub_profession=pro.list_id");		
		$this->db->where("ah_request_form.moduleid", $moduleid);
		if($roleid==102)
		{	$this->db->where("ah_request_form.managerid", $userid);	}
		else if($roleid==130 && $roleid==295 && $roleid==304)
		{	$this->db->where("ah_request_form.userid", $userid);	}
		else if($roleid==314)
		{	$this->db->where("ah_request_form.userid", $userid);	}
			$query = $this->db->get();
			return $query->result();
	}
	
	public function get_request_form_data($requestformid)
	{
		$this->db->where("requestid",$requestformid);
		$query = $this->db->get("ah_request_form");
		return $query->row();
	}
}

?>
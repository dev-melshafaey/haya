<div class="col-md-12" style="background-color:#FFF; padding: 0px !important;">
  <div class="col-md-3 from-group mtop">الأسم الكامل</div>
  <div class="col-md-2 from-group mtop">الرقم المدنية</div>
  <div class="col-md-2 from-group mtop">الرقم الوظيفي</div>
  <div class="col-md-3 from-group mtop">مهنة</div>
  <div class="col-md-2 from-group mtop">تاريخ</div>
  
  <?PHP foreach($request_all as $res) {
          $printUrl = base_url().'dashboard/printpaper/'.$res->moduleid.'/'.$res->requestid;
    ?>
  <div class="col-md-12" data-url="<?PHP echo($printUrl); ?>" data-id="<?PHP echo($res->moduleid); ?>" onclick="get_print_page(this)" style="background-color:#EFEFEF !important; padding: 0px !important; cursor: pointer !important;">
      <div class="col-md-3 from-group mtopg"><?PHP echo $res->fullname; ?></div>
      <div class="col-md-2 from-group mtopg"><?PHP echo arabic_date($res->idcardNumber); ?></div>
      <div class="col-md-2 from-group mtopg"><?PHP echo arabic_date($res->id_number); ?></div>
      <div class="col-md-3 from-group mtopg"><?PHP echo $res->profession; ?> / <?PHP echo $res->subprofession; ?></div>
      <div class="col-md-2 from-group mtopg"><?PHP echo arabic_date($res->fordate); ?></div>
   </div>   
  <?PHP } ?>
  
  <br style="clear: both !important;">
</div>

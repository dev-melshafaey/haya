<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12" style="background-color: #FFF !important;">
        <ul class="nav nav-tabs panel panel-default panel-block">
            <li class="tabsdemo-175 active"><a href="#tabsdemo-175" data-toggle="tab">خطاب مفردات راتب</a></li>
            <li class="tabsdemo-176"><a href="#tabsdemo-176" data-toggle="tab">خطاب تعريف</a></li>
            <li class="tabsdemo-177"><a href="#tabsdemo-177" data-toggle="tab">وافق</a></li>
        </ul>
        <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-175">
               <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:right;">الأسم الكامل</th>
                      <th style="text-align:center;">الرقم المدنية</th>
                      <th style="text-align:center;">الرقم الوظيفي</th>
                      <th style="text-align:right;">مهنة</th>
                      <th style="text-align:center;">تاريخ</th>
                      <th style="text-align:center;">الحالة</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?PHP foreach($mod175 as $res) {
                        $printUrl = base_url().'dashboard/printpaper/175/'.$res->requestid;
                      ?>
                    <tr role="row">
                      <td style="text-align:right;"><?PHP echo $res->fullname; ?></td>
                      <td style="text-align:center;"><?PHP echo arabic_date($res->idcardNumber); ?></td>
                      <td style="text-align:center;"><?PHP echo arabic_date($res->id_number); ?></td>
                      <td style="text-align:right;"><?PHP echo $res->profession; ?> / <?PHP echo $res->subprofession; ?></td>
                      <td style="text-align:center;"><?PHP echo arabic_date($res->fordate); ?></td>
                      <td style="text-align:center;">
                      <?PHP if($roleid==314) { ?>
                      <?php echo applicant_status($res->requeststatus, 1);?> (<?PHP echo arabic_date($res->actiondate); ?>)
                      <?PHP } else { ?>
                      <a href="#" data-url="<?PHP echo($printUrl); ?>" data-id="175" onclick="get_print_page_diag(this);"><?php echo applicant_status($res->requeststatus, 1);?> (<?PHP echo arabic_date($res->actiondate); ?>)</a></td>
                     <?PHP } ?>
                    </tr>
                    <?PHP } ?>
                  </tbody>
                </table>
              
            </div>
            <div class="tab-pane list-group" id="tabsdemo-176"><table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                  <thead>
                    <tr role="row">
                      <th style="text-align:right;">الأسم الكامل</th>
                      <th style="text-align:center;">الرقم المدنية</th>
                      <th style="text-align:center;">الرقم الوظيفي</th>
                      <th style="text-align:right;">مهنة</th>
                      <th style="text-align:center;">تاريخ</th>
                      <th style="text-align:center;">الحالة</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?PHP foreach($mod176 as $m) {
                      $tUrl = base_url().'dashboard/printpaper/175/'.$res->requestid;
                      ?>
                    <tr role="row">
                      <td style="text-align:right;"><?PHP echo $m->fullname; ?></td>
                      <td style="text-align:center;"><?PHP echo arabic_date($m->idcardNumber); ?></td>
                      <td style="text-align:center;"><?PHP echo arabic_date($m->id_number); ?></td>
                      <td style="text-align:right;"><?PHP echo $m->profession; ?> / <?PHP echo $m->subprofession; ?></td>
                      <td style="text-align:center;"><?PHP echo arabic_date($m->fordate); ?></td>
                      <td style="text-align:center;">
                       <?PHP if($roleid==314) { ?>
                      <?php echo applicant_status($m->requeststatus, 1);?> (<?PHP echo arabic_date($m->actiondate); ?>)
                      <?PHP } else { ?>
                      <a href="#" data-url="<?PHP echo($tUrl); ?>" data-id="<?PHP echo($m->moduleid); ?>" onclick="get_print_page_diag(this)"><?php echo applicant_status($m->requeststatus, 1);?> (<?PHP echo arabic_date($res->actiondate); ?>)</a></td>
                     <?PHP } ?>
                      
                    </tr>
                    <?PHP } ?>
                  </tbody>
                </table></div>
            <div class="tab-pane list-group" id="tabsdemo-177">
              <div class="list-group-item">
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center;">الأسم الكامل</th>
                        <th style="text-align:center;">سبب</th>
                        <th style="text-align:center;">من</th>
                        <th style="text-align:center;">إلى</th>
                        <th style="text-align:center;">إجازة يوم</th>
                        <th style="text-align:center;">ترك طلب الحالة</th>
                        <th style="text-align:center;">الإجرائات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <?php $date	=	date('Y-m-d')?>
                      <?php
					$all_users	=	$this->users->get_all_leave_requests($userid,'A',$roleid,$manager_id);
					if(!empty($all_users)):
						foreach($all_users as $leave):
							if($leave->leave_application)
							{
								$actions	= '&nbsp;&nbsp;<a href="'.base_url().'users/download_file/'.$leave->userid.'/'.$leave->leave_application.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
							}
							
							$actions 	 .=  '	<a  onclick="alatadad(this);" data-url="'.base_url().'users/view_request/'.$leave->leaveid.'"  href="#"><i class="icon-eye-open"></i></a>';
							
							if($permissions[143]['v']	==	1)
							{
								//$actions 	.=	'<a href="#addingDiag" onclick="alatadad(this);" data-url="'.base_url().'users/add_leave_request/'.$leave->leaveid.'" id="'.$leave->leaveid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
							}
							
							if($leave->approved	==	'0')
							{	
								$request	=	'تحت المعالجة';
							}
							elseif($leave->approved	==	'1')
							{
								$request	=	'<img src="'.base_url().'assets/images/approved.png" style="width: 24px;" alt="Approved" title="Approved"/>';
							}
							else
							{
								$request	=	'<img src="'.base_url().'assets/images/not-approved.png" style="width: 24px;" alt="Reject" title="Reject"/>';
							}
					?>
                      <tr role="row" id="<?php echo $leave->leaveid;?>_durar_lm">
                        <td  style="text-align:center;"><?php echo $leave->fullname;?></td>
                        <td  style="text-align:center;"><?php echo $leave->reason;?></td>
                        <td  style="text-align:center;"><?php echo arabic_date(date('d-m-y',strtotime($leave->start_date)));?></td>
                        <td  style="text-align:center;"><?php echo arabic_date(date('d-m-y',strtotime($leave->end_date)));?></td>
                        <td  style="text-align:center;"><?php echo arabic_date($leave->total_leaves);?></td>
                        <td  style="text-align:center;"><?php echo $request;?></td>
                        <td><?php echo $actions;?></td>
                      </tr>
                      <?php unset($actions); endforeach;?>
                      <?php endif;?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
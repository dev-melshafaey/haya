<div class="col-md-12" style="background-color:#FFF">
<ul class="nav nav-tabs panel panel-default panel-block">
  <?PHP
      $moduleArray  = array(175,176);
      $active = array('175'=>'active');
      $managerID = $user_detail['profile']->userid;
      foreach($moduleArray as $matab)
      { $mvalue = $this->haya_model->get_module_name_icon($matab);       
        echo('<li class="tabsdemo-1983'.$matab.' '.$active[$matab].'"><a href="#tabsdemo-1983'.$matab.'" data-toggle="tab">'.$mvalue->module_name.'</a></li>'); }
      echo('</ul><div class="tab-content panel panel-default panel-block">');
      foreach(array(175,176) as $macontent)
      {         
        ?>
      <div class="tab-pane list-group <?PHP echo $active[$macontent]; ?>" id="tabsdemo-1983<?PHP echo $macontent; ?>">
        <div class="col-md-12" style="padding-right:27px; padding-top: 10px; padding-bottom: 10px; padding-left: 5px;">
          <div class="col-md-12">
              <?PHP foreach(applicant_status('',3) as $appkey => $appvalue) {
                      $countArray = array('requeststatus'=>$appkey,'managerid'=>$managerID,'moduleid'=>$macontent);
                     ?>
                <a href="#" data-id="<?PHP echo $macontent; ?>" data-url="<?PHP echo base_url(); ?>dashboard/getAllRequestForManager/<?PHP echo $macontent; ?>/<?PHP echo $appkey; ?>" data-id="<?PHP echo $macontent; ?>" class="peterpan button <?PHP echo $appkey; ?>">
                <?PHP echo $appvalue; ?> (<?PHP echo arabic_date($this->haya_model->get_all_count_table('ah_request_form', $countArray)); ?>)</a>
              <?PHP
                unset($appkey,$countArray);
              } ?>
          </div>
          <div class="col-md-12" id="request<?PHP echo $macontent; ?>">
          </div>
        </div>
      </div>
  <?PHP
      unset($macontent);
      }
      echo('</div>');
  ?>

</div>
<script>
  $(function(){
     $(".peterpan").click(function(){
        var dataid = $(this).attr('data-id');
        var dataurl = $(this).attr('data-url');
        $.ajax({
			url: dataurl,
			type: "POST",
            data: {moduleid:dataid},
			success: function(msg){
               $('#request'+dataid).html(msg);          
            }
			
		});
      });
    });
</script>

<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row"> <
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12 from-group" style="background-color:#FFF">     
        <table class="table table-bordered table-striped dataTable" id="basicTable">
          <thead>
            <tr role="row">
              <th class="right">الاسم</th>
              <th class="right">طلب</th>
              <th class="right">نسخ الى</th>
              <th class="right">مدير</th>
              <th class="right">التاريخ</th>
              <th class="center">الإجراءات</th>
            </tr>
          </thead>
          <tbody role="alert" aria-live="polite" aria-relevant="all">
          <?PHP foreach($this->haya_model->get_all_request_listing() as $garl) { ?>
            <tr role="row">
              <td class="right"><?PHP echo $garl->eName; ?></td>
              <td class="right"><?PHP echo $garl->module_name; ?></td>
              <td class="right"><?PHP echo $garl->CopyTo; ?><?PHP if($garl->copyto_approved_date!=''){ ?><li class="small_li"><?PHP echo $garl->copyto_approved_date; ?> (<?PHP echo applicant_status($garl->copyto_discission,1); ?>)</li><?PHP } ?></td>
              <td class="right"><?PHP echo $this->haya_model->getmanagers($garl->ahrs); ?></td>
              <td class="right"><?PHP echo $garl->requestdate; ?></td>
              <td class="center"><?PHP
			  		$url = base_url().'dashboard/printpaper/'.$garl->moduleid.'/'.$garl->requestid.'/'.$garl->canEdit.'/'.$garl->hrlog;
			  		if($garl->canEdit=='0')
					{
						echo '<button type="button" data-url="'.$url.'" data-id="'.$garl->moduleid.'" onclick="get_print_page_diag(this);" class="btn btn-success">مشاهده</button>';
					}
					else
					{
						if($garl->ars_approved > 0 && $garl->hrlog=='manager')
						{	$xurl = base_url().'dashboard/printpaper/'.$garl->moduleid.'/'.$garl->requestid.'/0/'.$garl->hrlog;
							echo '<button type="button" data-url="'.$xurl.'" data-id="'.$garl->moduleid.'" onclick="get_print_page_diag(this);" class="btn btn-success">مشاهده</button>';	}
						else if($garl->ars_approved > 0 && $garl->copyto_approved_date!='')
						{	$xurl = base_url().'dashboard/printpaper/'.$garl->moduleid.'/'.$garl->requestid.'/0/'.$garl->hrlog;
							echo '<button type="button" data-url="'.$xurl.'" data-id="'.$garl->moduleid.'" onclick="get_print_page_diag(this);" class="btn btn-success">مشاهده</button>';	}
						else
						{	echo '<button type="button" data-url="'.$url.'" data-id="'.$garl->moduleid.'" onclick="get_print_page_diag(this);" class="btn btn-danger">تصحيح</button>';	}
					}
              
			  
			  ?></td>
          <?PHP } ?> 
          <?PHP foreach($this->haya_model->get_all_leave_request() as $garl) { ?>
            <tr role="row">
              <td class="right"><?PHP echo $garl->eName; ?></td>
              <td class="right"><?PHP echo $garl->module_name; ?></td>
              <td class="right"><?PHP echo $garl->CopyTo; ?><?PHP if($garl->copyto_approved_date!=''){ ?><li class="small_li"><?PHP echo $garl->copyto_approved_date; ?> (<?PHP echo applicant_status($garl->copyto_discission,1); ?>)</li><?PHP } ?></td>
              <td class="right"><?PHP echo $this->haya_model->getmanagers($garl->ahrs); ?></td>
              <td class="right"><?PHP echo $garl->requestdate; ?></td>
              <td class="center"><?PHP
			  		$url = base_url().'users/view_request/'.$garl->requestid;
			  		if($garl->canEdit=='0')
					{
						echo '<button type="button" data-url="'.$url.'" data-id="'.$garl->moduleid.'" onclick="get_print_page_diag(this);" class="btn btn-success">مشاهده</button>';
					}
					else
					{
						if($garl->ars_approved > 0 && $garl->hrlog=='manager')
						{	$xurl = base_url().'dashboard/printpaper/'.$garl->moduleid.'/'.$garl->requestid.'/0/'.$garl->hrlog;
							echo '<button type="button" data-url="'.$xurl.'" data-id="'.$garl->moduleid.'" onclick="get_print_page_diag(this);" class="btn btn-success">مشاهده</button>';	}
						else if($garl->ars_approved > 0 && $garl->copyto_approved_date!='')
						{	$xurl = base_url().'dashboard/printpaper/'.$garl->moduleid.'/'.$garl->requestid.'/0/'.$garl->hrlog;
							echo '<button type="button" data-url="'.$xurl.'" data-id="'.$garl->moduleid.'" onclick="get_print_page_diag(this);" class="btn btn-success">مشاهده</button>';	}
						else
						{	echo '<button type="button" data-url="'.$url.'" data-id="'.$garl->moduleid.'" onclick="get_print_page_diag(this);" class="btn btn-danger">تصحيح</button>';	}
					}
              
			  
			  ?></td>
          <?PHP } ?> 
             
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<br style="clear: both;">
<?php $this->load->view('common/footer'); ?>
</body>
</html>
<!doctype html>
<?php $this->load->view('common/header',array('module'=>'')); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar',array('udata'=>'')); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row" style="display:block;">
    <div class="col-md-12" id="container" style="height: 400px"> </div>
    <br clear="all">
    <div class="col-md-4" id="pie-1" style="height: 400px"> </div>
    <div class="col-md-4" id="pie-2" style="height: 400px"> </div>
    <div class="col-md-4" id="pie-3" style="height: 400px"> </div>
    <br clear="all">
  </div>
  <?php if(!empty($bernamij_almasadaat_branch)):?>
  <?php foreach($bernamij_almasadaat_branch as $branch	=>	$data):?>
  <div class="col-md-4">
    <section class="row">
      <div class="panel-heading text-overflow-hidden">
        <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
          <thead>
            <tr role="row">
                <thead colspan="2" style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
              <div class="table-responsive">
                <div class="col-md-1"><a href="#"></a><br>
                </div>
                <div class="col-md-7"><a href="<?PHP echo base_url(); ?>inquiries/transactions">
                  <?php //echo $branch;?>
                  </a><br>
                  <?PHP //echo arabic_date($jamee_ul_mamlaat); ?>
                </div>
                <div class="col-md-1"><a href="#"></a><br>
                </div>
              </div>
                </th>
            </tr>
          </thead>
          <tbody role="alert" aria-live="polite" aria-relevant="all">
            <tr class="gradeA odd" bgcolor="#f5f5f5">
              <td class=" sorting_1" colspan="2"><!--<img src="<?PHP echo base_url(); ?>assets/images/ico6.png" width="29" height="29"><i class="icon-money h2 text-danger"></i>-->
                
                <p><strong><?php echo $branch;?></strong></p></td>
            </tr>
            <?php foreach($data as $key	=>	$value) { ?>
            <tr class="gradeA even">
              <td><?php echo $key;?></td>
              <td><?php echo $value;?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </section>
  </div>
  <?php endforeach;?>
  <?php endif;?>
  <div class="col-md-4">
    <section class="row">
      <div class="panel-heading text-overflow-hidden">
        <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
          <tbody role="alert" aria-live="polite" aria-relevant="all">
            <tr class="gradeA odd" bgcolor="#f5f5f5">
              <td class=" sorting_1" colspan="2"><i class="<?php echo $this->dashboard->get_icon('123');?>"></i>
                <p><strong>برامج القرض</strong></p></td>
            </tr>
            <?php foreach($bernamij_almasadaat as $almasadaat) { ?>
            <tr class="gradeA even">
              <td><?php echo $almasadaat['name'];?></td>
              <td><?php echo $almasadaat['y'];?></td>
            </tr>
            <?PHP } ?>
          </tbody>
        </table>
      </div>
    </section>
  </div>
  <div class="col-md-4">
    <section class="row">
      <div class="panel-heading text-overflow-hidden">
        <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
          <tbody role="alert" aria-live="polite" aria-relevant="all">
            <tr class="gradeA odd" bgcolor="#f5f5f5">
              <td class=" sorting_1" colspan="2"><i class="<?php echo $this->dashboard->get_icon('47');?>"></i>
                <p><strong>برنامج  الموارد البشرية</strong></p></td>
            </tr>
            <?php foreach($users as $key	=>	$value) { ?>
            <tr class="gradeA even">
              <td><?php echo $key;?></td>
              <td><?php echo $value;?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </section>
  </div>
  <br clear="all"/>
  <?php if(!empty($yateem_kafeel_count_by_branch)):?>
  <?php foreach($yateem_kafeel_count_by_branch	as $key	=>	$value):?>
  <div class="col-md-4">
    <section class="row">
      <div class="panel-heading text-overflow-hidden">
        <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
          <tbody role="alert" aria-live="polite" aria-relevant="all">
            <tr class="gradeA odd" bgcolor="#f5f5f5">
              <td class=" sorting_1" colspan="2"><i class="<?php echo $this->dashboard->get_icon('47');?>"></i>
                <p><strong><?php echo $key;?></strong></p></td>
            </tr>
            <?php foreach($value as $key2	=>	$value2) { ?>
            <tr class="gradeA even">
              <td><?php echo $key2;?></td>
              <td><?php echo $value2;?></td>
            </tr>
            <?PHP } ?>
          </tbody>
        </table>
      </div>
    </section>
  </div>
  <?php endforeach;?>
  <?php endif;?>
  <div class="col-md-4">
    <section class="row">
      <div class="panel-heading text-overflow-hidden">
        <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
          <tbody role="alert" aria-live="polite" aria-relevant="all">
            <tr class="gradeA odd" bgcolor="#f5f5f5">
              <td class=" sorting_1" colspan="2"><i class="<?php echo $this->dashboard->get_icon('67');?>"></i>
                <p><strong>قائمة الشركة</strong></p></td>
            </tr>
            <?php foreach($nooh_sherka as $sherka) { ?>
            <tr class="gradeA even">
              <td><?php echo $sherka['name'];?></td>
              <td><?php echo $sherka['count'];?></td>
            </tr>
            <?PHP } ?>
          </tbody>
        </table>
      </div>
    </section>
  </div>
  <div class="col-md-4">
    <section class="row">
      <div class="panel-heading text-overflow-hidden">
        <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
          <tbody role="alert" aria-live="polite" aria-relevant="all">
            <tr class="gradeA odd" bgcolor="#f5f5f5">
              <td class=" sorting_1" colspan="2"><i class="<?php echo $this->dashboard->get_icon('47');?>"></i>
                <p><strong>برنامج  الأيتــــام</strong></p></td>
            </tr>
            <?php foreach($yateem_kafeel as $key	=>	$value) { ?>
            <tr class="gradeA even">
              <td><?php echo $key;?></td>
              <td><?php echo $value;?></td>
            </tr>
            <?PHP } ?>
          </tbody>
        </table>
      </div>
    </section>
  </div>
  <div class="col-md-4">
    <section class="row">
      <div class="panel-heading text-overflow-hidden">
        <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
          <thead>
            <tr role="row">
              <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending"> <div class="table-responsive">
                  <div class="col-md-12"><a href="#_"  style="font-size: 20px;" data-id="45"> <i class="<?php echo $this->dashboard->get_icon('4');?>"></i> برنامج  المساعدات </a><br>
                    <a href="#" class="sub_menu_activater" style="font-size: 11px;" data-id="45">مجموع (<?php echo (isset($all_transactions) ? $all_transactions : '0');?>)</a></div>
                </div>
              </th>
            </tr>
            <tr role="row" class="sub_menu_list" id="sub_menu_45">
              <th id="sub_menu_data_load_45"></th>
            </tr>
          </thead>
          <tbody role="alert" aria-live="polite" aria-relevant="all">
          </tbody>
        </table>
      </div>
    </section>
  </div>
  <div class="col-md-4">
    <section class="row">
      <div class="panel-heading text-overflow-hidden">
        <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
          <thead>
            <tr role="row">
              <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending"> <div class="table-responsive">
                  <div class="col-md-12"><a href="#_"  style="font-size: 20px;" data-id="45"> <i class="<?php echo $this->dashboard->get_icon('17');?>"></i> برنامج  المخازن </a><br>
                    <a href="#" class="sub_menu_activater" style="font-size: 11px;" data-id="45">مجموع (<?php echo (isset($all_items) ? $all_items : '0');?>)</a></div>
                </div>
              </th>
            </tr>
            <tr role="row" class="sub_menu_list" id="sub_menu_45">
              <th id="sub_menu_data_load_45"></th>
            </tr>
          </thead>
          <tbody role="alert" aria-live="polite" aria-relevant="all">
          </tbody>
        </table>
      </div>
    </section>
  </div>
</section>
<?php $this->load->view('common/footer');?>
<?php $values	=	json_encode($this->dashboard->bernamij_almasadaat_new());?>
<?php $branches	=	$this->dashboard->all_branches();?>
<?php 
		$search = array('"[',']"');
		$replace = array('[',']');
		$valu = str_replace($search, $replace, $values);

		$bernamij_almasadaat		=	json_encode($this->dashboard->bernamij_almasadaat());
		$users_pie_chart			=	json_encode($this->dashboard->users_pie_chart());
		$nooh_sherka_pie_chart		=	json_encode($this->dashboard->nooh_sherka_pie_chart());
		
		
?>
</body>
</html><script type="text/javascript">
$(function () {
	
	var chart_style	= {
					fontFamily: '"Al-Haya", "Lucida Sans Unicode", , Helvetica, sans-serif',
					fontSize: '12px',
					direction: 'rtl'
					}
					
	var chart_lagend	=	{
					align: 'center',
					rtl: true,
					itemDistance:100,
        			useHTML: true
					}  
					
    $('#container').highcharts({
        chart: {
            type: 'column',
            options3d: {
                enabled: false,
                alpha: 15,
                beta: 15,
                viewDistance: 25,
                depth: 40
            },
		style:chart_style,
            marginTop: 80,
            marginRight: 100
        },
        title: {
            text: 'برنامج المساعدات',
			useHTML: true
        },
        xAxis: {
            categories: [<?php echo $branches;?>],
			reversed: true
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'رقم المساعدة',
				useHTML: true
            },
			 opposite: true
        },
		legend: chart_lagend,
        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}',
			useHTML: true
        },

        plotOptions: {
            column: {
                stacking: 'normal',
                depth: 40
            }
        },
		style:chart_style,
		credits: {	
            enabled: true, 
            text:'درر للحلول الذكية ش.م.م',
            href: 'http://www.durar-it.com/',
			position: {
	align: 'center',
	x: 0,
	verticalAlign: 'top',
	y: 50
}
        },

        series: <?php echo $valu;?>
    });
	
/**********************************************************************/
    // Radialize the colors
   /* Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });*/
	   // Build the chart
    $('#pie-1').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: true,
            type: 'pie'
        },
        title: {
            text: 'برنامج المساعدات',
			useHTML: true
        },
		 legend: chart_lagend,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
			useHTML: true,
			style:chart_style
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                }
            }
        },
		credits: {	
            enabled: false, 
            text:'درر للحلول الذكية ش.م.م',
            href: 'http://www.durar-it.com/',
			position: {
	align: 'center',
	x: 0,
	verticalAlign: 'top',
	y: 50
}
        },
        series: [{
            name: "المساعدات",
			useHTML: true,
            data: <?php echo $bernamij_almasadaat;?>
        }]
    });
	$('#pie-2').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: true,
            type: 'pie'
        },
        title: {
            text: 'برنامج الموارد البشرية',
			useHTML: true
        },
		 legend: chart_lagend,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
			useHTML: true,
			style:chart_style
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                }
            }
        },
		credits: {	
            enabled: false, 
            text:'درر للحلول الذكية ش.م.م',
            href: 'http://www.durar-it.com/',
			position: {
	align: 'center',
	x: 0,
	verticalAlign: 'top',
	y: 50
}
        },
        series: [{
            name: "المساعدات",
			useHTML: true,
            data: <?php echo $users_pie_chart;?>
        }]
    });
	$('#pie-3').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: true,
            type: 'pie'
        },
        title: {
            text: 'قائمة الشركة',
			useHTML: true
        },
		 legend: chart_lagend,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
			useHTML: true,
			style:chart_style
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                }
            }
        },
		credits: {	
            enabled: false, 
            text:'درر للحلول الذكية ش.م.م',
            href: 'http://www.durar-it.com/',
			position: {
	align: 'center',
	x: 0,
	verticalAlign: 'top',
	y: 50
}
        },
        series: [{
            name: "المساعدات",
			useHTML: true,
            data: <?php echo $nooh_sherka_pie_chart;?>
        }]
    });
});
</script>
<!doctype html>
<?php $this->load->view('common/header',array('module'=>$module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar',array('udata'=>$user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-2">
        <section class="row">
          <div class="panel-heading text-overflow-hidden">
            <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending"> <div class="table-responsive">
                      <div class="col-md-1"><a href="#"></a><br>
                      </div>
                      <div class="col-md-7"><a href="<?PHP echo base_url(); ?>inquiries/transactions">جميع المعاملات</a><br>
                        <?PHP echo arabic_date($jamee_ul_mamlaat); ?></div>
                      <div class="col-md-1"><a href="#"></a><br>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <tr class="gradeA odd" bgcolor="#f5f5f5">
                  <td class=" sorting_1"><img src="<?PHP echo base_url(); ?>images/ico6.png" width="29" height="29"><!--<i class="icon-money h2 text-danger"></i>-->
                    <p><strong>برامج القرض</strong></p></td>
                </tr>
                <?PHP foreach($count_sum as $tcount) { ?>
                <tr class="gradeA even">
                  <td><?PHP echo $tcount['loan_category_name']; ?>&nbsp;&nbsp;<span class="text-danger"><?PHP echo arabic_date($tcount['lcount']); ?></span>
                    <h4 class="h4x"><?PHP echo arabic_date(number_format($tcount['lamount'],0)); ?> ر.ع</h4></td>
                </tr>
                <?PHP } ?>
              </tbody>
            </table>
          </div>
        </section>
      </div>
      <div class="col-md-5">
        <section class="row">
          <div class="panel-heading text-overflow-hidden">
            <table style="text-align:center;" class="table" id="tableSortable3" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="2" aria-label="Platform(s): activate to sort column ascending" style="text-align:center;"> <div class="table-responsive">
                      <div class="col-md-4"><a href="#">معاملات هذا اليوم</a><br />
                        <?PHP echo arabic_date($day_month_year->count_day); ?></div>
                      <div class="col-md-4"><a href="#"></i>معاملات هذا الشهر</a><br />
                        <?PHP echo arabic_date($day_month_year->count_month); ?></div>
                      <div class="col-md-4"><a href="#"></i>معاملات هذا العام</a><br />
                        <?PHP echo arabic_date($day_month_year->count_year); ?></div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <tr class="gradeA odd" bgcolor="#f5f5f5">
                  <td colspan="2" width="303" class=" sorting_1"><img src="<?PHP echo base_url(); ?>images/ico5.png" width="29" height="29"> 
                    <!--<i class="icon-briefcase h2 text-success">--></i>
                    <p><strong>إحصائات المعاملات</strong></p></td>
                </tr>
                <tr class="gradeA even">
                  <td class=" sorting_1"><p>معاملات مخاطر الإئتمان</p><h4><span class="label label-success"><?PHP echo arabic_date($zayarat->ck); ?></span></h4></td>
                  <td class=" sorting_1"><p>الزيارات الأولية</p> <h4><span class="label label-success"><?PHP echo arabic_date($zayarat_awaliya->ck); ?></span></h4></td>
                </tr>
                <tr class="gradeA even">
                  <td class=" sorting_1"><p>المعاملات الغير مكتملة </p><h4><span class="label label-success"><?PHP echo arabic_date($mamlaat_gair); ?></span></h4></td>
                  <td class=" sorting_1"><p>قرار اللجنة</p><h4><span class="label label-success"><?PHP echo arabic_date($qarar_ul_lajjana); ?></span></h4></td>
                </tr>
                <tr class="gradeA even">
                  <td class=" sorting_1"><p>المعاملات المؤجلة</p><h4><span class="label label-success">0</span></h4></td>
                  <td class=" sorting_1"><p>المعاملات المرفوضة</p><h4><span class="label label-success">0</span></h4></td>
                </tr>
                <tr class="gradeA even">
                  <td class=" sorting_1"><p>المعاملات المشروطه</p><h4><span class="label label-success">0</span></h4></td>
                  <td class=" sorting_1"><p>زيارات صرف الدفعات</p><h4><span class="label label-success"><?php echo arabic_date($zayarat_awaliya->ck);?></span></h4></td>
                </tr>
              </tbody>
            </table>
          </div>
        </section>
      </div>
      <div class="col-md-2">
        <section class="row">
          <div class="panel-heading text-overflow-hidden">
          
            <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending"> <div class="table-responsive">
                      <div class="col-md-5"><a href="#">فردي (<?PHP echo arabic_date($tasgeel['single']); ?>)</a><br />
                        <i class="icon-male" style="color:#1153C3 !important;"><span class="peoplesize"><?PHP echo arabic_date($tasgeel['singleMan']); ?></span></i>
                      <i class="icon-female" style="color:#DC007C !important;"><span class="peoplesize"><?PHP echo arabic_date($tasgeel['singleWomen']); ?></span></i></div>
                     
                      <div class="col-md-5"><a href="#">مشترك (<?PHP echo arabic_date($tasgeel['partnerMan']+$tasgeel['partnerWomen']); ?>)</a><br />
                        <i class="icon-male" style="color:#1153C3 !important;"><span class="peoplesize"><?PHP echo arabic_date($tasgeel['partnerMan']); ?></span></i>
                      <i class="icon-female" style="color:#DC007C !important;"><span class="peoplesize"><?PHP echo arabic_date($tasgeel['partnerWomen']); ?></span></i></div>
                     </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <tr class="gradeA odd" bgcolor="#f5f5f5">
                  <td class=" sorting_1"><img src="<?PHP echo base_url(); ?>images/ico4.png" width="29" height="29"> 
                    <!--<i class="icon-thumbs-up-alt h2 text-danger"></i>-->
                    <p><strong>الموافقات الأولية</strong></p></td>
                </tr>
                <?PHP
                	$ar = array(
						'1'=>array('heading'=>'إجمالي الموافقات الأولية','number'=>'2218654'),
						'2'=>array('heading'=>'إجمالي مبالغ الموافقات الأولية','number'=>'918654'),
						'3'=>array('heading'=>'إجمالي المبالغ المستلمة','number'=>'33654'),
						'4'=>array('heading'=>'إجمالي المبالغ المتبقية','number'=>'2218654'),
					)
				?>
                <tr class="gradeA even">
                <?PHP foreach($ar as $akey => $avalue) { ?>
                  <td class=" sorting_1">
                    <?PHP echo $avalue['heading']; ?>
                    <h4><?PHP echo arabic_date(number_format($avalue['number'],0)); ?> ر.ع</h4></td>
                </tr>
               <?PHP } ?>
              </tbody>
            </table>
          </div>
        </section>
      </div>
      <div class="col-md-3">
        <section class="row">
          <div class="panel-heading text-overflow-hidden">
        
  <!--         <div class="col-md-4"><a href="#">ذكور</a><br />
                        200</div>
                      <div class="col-md-4"><a href="#"></i>إناث</a><br />
                        31</div>-->
            <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending"> <div class="table-responsive">
                      <div class="col-md-6"><a href="#">فردي (<?PHP echo arabic_date($murageen['singleMan']+$murageen['singleWomen']); ?>)</a><br>
                      <i class="icon-male" style="color:#1153C3 !important;"><span class="peoplesize"><?PHP echo arabic_date($murageen['singleMan']); ?></span></i>
                      <i class="icon-female" style="color:#DC007C !important;"><span class="peoplesize"><?PHP echo arabic_date($murageen['singleWomen']); ?></span></i>
                      </div>                    
                      <div class="col-md-6"><a href="#">مشترك (<?PHP echo arabic_date($murageen['partnerMan']+$murageen['partnerWomen']); ?>) </a><br />
                       <i class="icon-male" style="color:#1153C3 !important;"><span class="peoplesize"><?PHP echo arabic_date($murageen['partnerMan']); ?></span></i>
                      <i class="icon-female" style="color:#DC007C !important;"><span class="peoplesize"><?PHP echo arabic_date($murageen['partnerWomen']); ?></span></i>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <tr class="gradeA odd" bgcolor="#f5f5f5">
                  <td class=" sorting_1"><img src="images/ico3.png" width="29" height="29"> 
                    <!--<i class="icon-edit h2 text-danger"></i>-->
                    <p><strong>المراجعين</strong></p></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="panel-heading text-overflow-hidden">
            <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending"> <div class="table-responsive">
                      <div class="col-md-2"><a href="#"></a><br>
                      </div>
                     
                      <div class="col-md-8"><a href="#">جميع المراجعين</a><br><?PHP echo arabic_date($murageen['murgaeenCount']); ?></div>
                      <div class="col-md-1"><a href="#"></a><br>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <tr class="gradeA odd" bgcolor="#f5f5f5">
                  <td class="sorting_1"><img src="<?PHP echo base_url(); ?>images/ico2.png" width="29" height="29">
                    <p><strong>الموظفين</strong></p></td>
                </tr>
                <tr class="gradeA odd">
                  <td class=" sorting_1"><div class="table-responsive">                  
                      <div class="row">
                        <div class="col-md-1 center"><a href="#">#</a></div>
                        <div class="col-md-6 right"><a href="#">إسم الموظف</a></div>
                        <div class="col-md-2 center"><a href="#">عدد المعاملات</a></div>
                      </div>
                      <?PHP foreach($userlist as $userindex => $user) { ?>
                      <div class="row">
                        <div class="col-md-1 center"><?PHP echo arabic_date($userindex+1); ?></div>
                        <div class="col-md-6 right"><?PHP echo $user->firstname.' '.$user->lastname; ?></div>
                        <div class="col-md-2 center"><a href="<?PHP echo base_url(); ?>inquiries/inquiries_filter/<?PHP echo $user->id; ?>"><?PHP echo arabic_date($user->murgeencount); ?></a></div>
                      </div>
                      <?PHP } ?>
                    </div></td>
                </tr>
              </tbody>
            </table>
          </div>
        </section>
      </div>
    </div>
  </div>
  <div class="panel panel-default panel-block">
    <div id="data-table" class="panel-heading datatable-heading">
      <h4 style="text-align:center" class="section-title text-info"><img src="<?PHP base_url(); ?>images/ico1.png" width="29" height="29"><br />
        <strong>الفروع</strong></h4>
    </div>
    <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
     
      <table class="table table-bordered table-striped dataTable" aria-describedby="tableSortable_info">
        <thead>
          <tr role="row">
          
            <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">م</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 222px; text-align:center;">الفروع</th>
            <?PHP foreach($count_sum as $lcatindex => $lvalue) { ?>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="tableSortable" rowspan="1" colspan="2"  style="text-align:center;"><?PHP echo $lvalue['loan_category_name']; ?></th>
          	<?PHP } ?>
          </tr>
        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
          <tr class="gradeA odd">
            <td class=" sorting_1">&nbsp;</td>
            <td class=" ">&nbsp;</td>
            <?PHP foreach($count_sum as $lcatindex => $lvalue) { ?>
            <td><p class="text-muted"> المعاملات </p></td>
            <td class="center "><p class="text-muted"> المبالغ </p></td>
            <?PHP } ?>
          </tr>
          <?PHP foreach($branchlist as $bkey => $bvalue) { 	 ?>
          <tr class="gradeA even">
            <td class=" sorting_1"><?PHP echo arabic_date($bkey+1);?></td>
            <td class=" "><?PHP echo $bvalue->branch_name; ?></td>
            <?PHP foreach($count_sum as $lcatindex => $lvalue) { 
					$sumx = $this->dashboard->mamlaat_types_count_and_sum_branch($bvalue->branch_id,$lvalue->loan_category_id);
					
			?>
            <td class=" "><span class="label label-success"><?PHP echo arabic_date(checkzero($sumx->lcount)); ?></span></td>
            <td class="center "><strong><?PHP echo arabic_date(number_format($sumx->lamount,2)); ?></strong></td>
           <?PHP } ?>
          </tr>
          <?PHP } ?>
        </tbody>
      </table>
      
    </div>
  </div>
</section>
<?php $this->load->view('common/footer');?>
</body>
</html>
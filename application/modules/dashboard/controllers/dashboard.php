<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller{
	

//-------------------------------------------------------------------------------	

	/*
	* Properties
	*/
	private $_data	=	array();
	private $_login_userid	=	NULL;

//-------------------------------------------------------------------------------

    /*
    * Costructor
    */
	
    public function __construct()
    {
        parent::__construct();
		
		//$this->session->sess_destroy();
		//exit();

        // Load Models
        $this->load->model('dashboard_model', 'dashboard');
		$this->load->model('haya/haya_model','haya');
		
		// SET Login USER ID
		$this->_login_userid		=	$this->session->userdata('userid');
		$this->_data['module']			=	$this->haya_model->get_module();
		$this->_data['user_detail'] =	$this->haya_model->get_user_detail($this->_login_userid);
		$this->haya->get_listmanagment_types();
		
		$this->_data['bernamij_almasadaat']			=	$this->dashboard->bernamij_almasadaat();
		$this->_data['bernamij_almasadaat_branch']	=	$this->dashboard->bernamij_almasadaat_branch();
		$this->_data['yateem_kafeel']				=	$this->dashboard->yateem_kafeel();
		$this->_data['users']						=	$this->dashboard->users();
		$this->_data['bernamij_almasadaat_new']		=	$this->dashboard->bernamij_almasadaat_new();
		$this->_data['users']						=	$this->dashboard->users();
		$this->_data['users_pie_chart']				=	$this->dashboard->users_pie_chart();
		$this->_data['yateem_kafeel']				=	$this->dashboard->yateem_kafeel();
		$this->_data['all_transactions']			=	$this->dashboard->get_all_transactions();
		$this->_data['all_items']					=	$this->dashboard->get_all_items();
		$this->_data['nooh_sherka']					=	$this->dashboard->nooh_sherka();
		$this->_data['nooh_sherka_pie_chart']		=	$this->dashboard->nooh_sherka_pie_chart();
		$this->_data['yateem_kafeel_count_by_branch']		=	$this->dashboard->yateem_kafeel_count_by_branch();

    }

//-------------------------------------------------------------------------------

    /*
    *
    * Main Page
    */
    public function index()
    {
		if($this->_login_userid)
		{
			$this->load->view('dashboard', $this->_data);
		}
		else
		{
			redirect(base_url().'admin/login');
			exit();
		}
    }
	
	public function my_request_form_one()
	{
		//echo '<pre>'; print_r($this->_data['user_detail']);
		$this->load->view("my_request_form_one_view", $this->_data);
	}
	
	public function my_request_form_two()
	{
		$this->load->view("my_request_form_two_view", $this->_data);
	}
	
	public function getRequestList()
	{
		$this->load->view("my_allrequest_view", $this->_data);
	}
	
	public function getAllRequestForManager($moduleid,$status)
	{
		$managerProfile =	$this->_data['user_detail']['profile'];	
		$manager_id 	=	$managerProfile->userid;
		
		$this->_data['request_all'] = $this->dashboard->getReqData($manager_id,$moduleid,$status);
		$this->load->view("allmyrequest", $this->_data);
	}
	
	public function getRequestCount()
	{
		$managerProfile	=	$this->_data['user_detail']['profile'];
		$manager_id 	=	$managerProfile->userid;
		
		$this->db->select("requeststatus,requestdate, fordate");		
		$this->db->where("requeststatus",'P');
		$this->db->where("managerid",$manager_id);	
		$query = $this->db->get("ah_request_form");

		echo $query->num_rows();
	}
//----------------------------------------------------------------------------
	/*
	* Get all User leave Requesst Status
	*
	*/	
	public function get_leave_request_status_count()
	{
		$this->db->select("leaveid");		
		$this->db->where("userid",$this->_login_userid);
		$this->db->where("read",'0');
		//$this->db->where("approved",1);
		//$this->db->or_where("approved",2);
			
		$query = $this->db->get("ah_leave_request");
		echo $query->num_rows();
	}
//----------------------------------------------------------------------------
	public function saveRequestFormOne()
	{
		$formData = $this->input->post();
		unset($formData['mybank']);
		
		$this->db->select("requeststatus,requestdate, fordate");		
		$this->db->where("userid",$formData['userid']);
		$this->db->where("branchid",$formData['branchid']);
		$this->db->where("managerid",$formData['managerid']);
		$this->db->where("fordate",$formData['fordate']);
		$this->db->where("moduleid",$formData['moduleid']);
		$query = $this->db->get("ah_request_form");		
		if($query->num_rows() > 0)
		{	$reqs = $query->row();			
			$requestMessage = '<strong>'.applicant_status($reqs->requeststatus,2).'  '.arabic_date($reqs->requestdate).'</strong>';		
			echo($requestMessage);
		}
		else
		{			
			$this->db->insert('ah_request_form',$formData,json_encode($formData),$this->_login_userid);
			echo('تلقينا طلبك، وسيتم مراجعته قريبا.<br>يرجى تتحمل معنا بينما نحن فحص بجد الطلب. سوف نتصل بك لنأخذ الامور كذلك يجب أن تطابق طلبك متطلباتنا.');
		}		
	}
	
	public function all_kind_of_request()
	{
		$this->load->view("all_kind_of_request",$this->_data);
	}
	
	public function printpaper($moduleid,$requestid,$mode,$hrlog)
	{
		$requestFormID = $this->dashboard->get_request_form_data($requestid);
		$this->_data['user_detail'] =	$this->haya_model->get_user_detail($requestFormID->userid, $requestFormID->accountid);
		$this->_data['requestid'] = $requestFormID->requestid;
		$this->_data['requeststatus'] = $requestFormID->requeststatus;
		$this->_data['moduleid'] = $moduleid;
		$this->_data['mode'] = $mode;
		$this->_data['hrlog'] = $hrlog;
		$this->load->view("printpaper",$this->_data);
	}
	
	public function updateStatus()
	{
		$requestid = $this->input->post("requestid");
		$requeststatus = $this->input->post("status");		
		$my_control = $this->input->post("my_control");
		if($my_control=='hr')
		{
			$ah_request_form['copyto_notes'] = 'Human Resource';
			$ah_request_form['copyto_approved_date'] = date('Y-m-d h:i:s');
			$ah_request_form['copyto_discission'] = $requeststatus;
		}
		else
		{
			$ah_request_form['requeststatus'] = $requeststatus;
			$ah_request_form['requestid'] = $requestid;
			$ah_request_form['actiondate'] = date('Y-m-d h:i:s');
			$ah_request_form['ars_approved'] = $this->_login_userid;
			$ah_request_form['ars_approved_notes'] = 'Manager';
			$ah_request_form['ars_approved_date'] = date('Y-m-d h:i:s');
		}
		
		$this->db->where('requestid', $requestid);
		$this->db->update('ah_request_form',json_encode($ah_request_form),$this->session->userdata('userid'),$ah_request_form);
	}
	
	public function my_request_list()
	{
		$this->load->model('users/users_model', 'users');
		
		$userid = $this->_data['user_detail']['profile']->userid;
		$userroleid = $this->_data['user_detail']['profile']->userroleid;
		
		$this->_data['mod175'] = $this->dashboard->allRequestAccordingToUser($userid,175,$userroleid);
		$this->_data['mod176'] = $this->dashboard->allRequestAccordingToUser($userid,176,$userroleid);
		$this->_data['roleid'] = $userroleid;
		$this->_data['manager_id']	=	$this->_data['user_detail']['profile']->manager_id;
		
		$this->_data['userid']	=	$userid;
		
		$this->load->view("request_list_all", $this->_data);
	}
//------------------------------------------------------------------------

  	/**
   	* Dynamic Forms Listing Page
   	* @param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]  = $this->haya_model->get_all_custom_form($moduleid);
		
		$this->_data["userid"] = $this->_login_userid;
		
		$this->_data['formid']	=	$moduleid;
		 
		 // Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);	 
	 }
}
//-----------------------------------------------------------------------
/* End of file dashboard.php */
/* Location: ./application/modules/admin/dashboard.php */
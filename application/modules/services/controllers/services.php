<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CI_Controller {

	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;
	public $administrator =	NULL;
	public $servicedepartment =	NULL;
	public $moduleid = NULL;
	public $otherservice_moduleid = NULL;
//-----------------------------------------------------------------------

	/*
	* Constructor
	*/
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('services_model','services');
		/////////////////////////////////////////	
		$this->administrator = 780;
		$this->servicedepartment = 1729;
		$this->moduleid = 242;
		$this->otherservice_moduleid = 243;
		/////////////////////////
		$this->_data['administrator']			= $this->administrator;
		$this->_data['servicedepartment']	= $this->servicedepartment;
		$this->_data['moduleid']			= $this->moduleid;
		
		$this->_data['module']			=	$this->haya_model->get_module();		
		$this->_login_userid 			=	$this->session->userdata('userid');
		$this->_data['login_userid'] 	=	$this->_login_userid;
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);
			
		
		$this->_data['login_userid']	=	$this->_login_userid;
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);
		$this->userroleid				=  $this->_data['user_detail']['profile']->userroleid;
		// Load all types
		$this->_data['list_types']		=	$this->haya_model->get_listmanagment_types();
		
		//$this->add_users_yearly_holidays(); // Add Yearly holiday into users accounts.
		
	}
//-----------------------------------------------------------------------

	/*
	*	Home Page
	*/
	public function index()
	{
		$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
		$this->load->view('listall_services',$this->_data);
		
		// Load Users Listing Page
		//$this->load->view('users-listing',$this->_data);
	}
//-----------------------------------------------------------------------
	public function loadmessage(){
		$data	=	$this->input->get();
		$serviceid = $data["serviceid"];
		$rowsmessage = $this->services->getdmessagedata($serviceid,'ah_services_messages',"serviceid");
		$message ='<table width="95%" border="0" cellspacing="0" cellpadding="10" align="center" style="text-align:right;">';
               if(count($rowsmessage)){
						  foreach($rowsmessage as $rw){
							  $this->db->select('fullname');
								$this->db->from('ah_userprofile');						
								$this->db->where("userid",$rw->userid);
								
								$rowuser = $this->db->get();
								foreach($rowuser->result() as $us){$fullname = $us->fullname;}
						 
               $message .='<tr>
                       <td  class="right">'.arabic_date($rw->dates).' : <b class="text-warning" style="margin-right:10px;">'.$fullname.'</b></td>
                         <td width="70%">'.$rw->message.'</td>
                      </tr>';
                      }
					  
					  }
                   $message .=' </table>';
                    $ex['message'] = $message;
			echo json_encode($ex);
	}
	public function view($serviceid	=	NULL)
	{
		$this->_data["rows"] = $this->services->getdata($serviceid,'ah_services',"serviceid");
		$this->db->select('fullname');
		$this->db->from('ah_userprofile');						
		$this->db->where("userid",$this->_data["rows"]->userid);
		$rowuser = $this->db->get(); 
		$this->_data["message"]  = $this->services->getdmessagedata($serviceid,'ah_services_messages',"serviceid");
		
		foreach($rowuser->result() as $us){$this->_data["usernames"] = $us->fullname;}
		$status ='';
			if($this->_data["rows"]->status == "Pending")
			{
				$status	=	'<img src="'.base_url().'assets/images/pending.png" style="width: 24px;" alt="Pending" title="Pending"/>';
			}
			else if($this->_data["rows"]->status == "In Progress")
			{
				$status	=	'<img src="'.base_url().'assets/images/inprogress.png" style="width: 24px;" alt="In Progress" title="In Progress"/>';
			}
			else if($this->_data["rows"]->status == "Accepted")
			{
				$status	=	'<img src="'.base_url().'assets/images/completed.png" style="width: 24px;" alt="Accepted" title="Accepted"/>';
			}
			else if($this->_data["rows"]->status == "Rejected")
			{
				$status	=	'<img src="'.base_url().'assets/images/rejected.png" style="width: 24px;" alt="Rejected" title="Rejected"/>';
			}
			$this->_data["status"] = $status;
		$this->load->view('view_service',$this->_data);
	}
	public function request($serviceid	=	NULL)
	{
		$this->_data["serviceid"]= $serviceid;
		if($this->input->post())
		{
			//print_r($_FILES);exit();
			$data	=	$this->input->post();
			$services	=	array();
			$services['title']	=	$data['title'];
			$services['description']	=	$data['description'];
			$services['status']	=	"Pending";
			$services['department']	=	$this->userroleid;
			if($data["serviceid"] != ""){
				$services['serviceid']	=$data["serviceid"];
				if(isset($_FILES["attachment"]) && count($_FILES["attachment"]) > 0)
				{
					$names = array();
					$files = $_FILES["attachment"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/services');
						}
						
					}
					if($data["attachment_old"] !=""){
						if(count($names)>0)	$services['attachment']	=	$data["attachment_old"].",".@implode(',',$names);
						else{$services['attachment']	=	$data["attachment_old"];}
					}
					else{
						$services['attachment']	=	@implode(',',$names);	
					}
					
				}
				else{
					$services['attachment']	=	$data["attachment_old"];
				}
				$serviceid = $this->services->savestore($services,'ah_services','serviceid');
				$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
				redirect(base_url().'services/');
				exit();
		}
		else{
			$services['dates']	=	date("Y-m-d h:i:s a");
			$services['userid']	=	$this->_login_userid;
			$services['isread']	=	'0';

			if(isset($_FILES["attachment"]) && count($_FILES["attachment"]) > 0)
			{
				$names = array();
				$files = $_FILES["attachment"];
				 foreach ($files['name'] as $key => $image) {
					if(!empty($files['name'][$key]))
					{
						$_FILES['images']['name']= $files['name'][$key];
						$_FILES['images']['type']= $files['type'][$key];
						$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['images']['error']= $files['error'][$key];
						$_FILES['images']['size']= $files['size'][$key];
						$names[] =	$this->upload_file($this->_login_userid,'images','resources/services');
					}
					
				}
				$services['attachment']	=	@implode(',',$names);
			}
			$serviceid =$this->services->savestore($services,'ah_services','serviceid');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
				redirect(base_url().'services/');
				
				exit();
		}
			
		}
		else{
			if($serviceid >0){
				$this->_data["rows"] = $this->services->getdata($serviceid,'ah_services',"serviceid");
			}
			$this->load->view('request',$this->_data);
		}
			
	}
	public function branchs()
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['all_branches']	=	$this->users->get_all_branches();
		
		// Load Users Listing Page
		$this->load->view('branches-listing',$this->_data);
		
	}
	function delete($serviceid){
		$result = $this->services->getdata($serviceid,'ah_services',"serviceid");
		if($result->attachment !=""){
			$image=@explode(',',$result->attachment);
			foreach($image as $key=>$vals){
				if($vals !=""){
					unlink('resources/services/'.$this->_login_userid."/".$vals);
				}
			}
		}
		$this->services->delete($serviceid,'serviceid',"ah_services");
		$this->services->delete($serviceid,'serviceid',"ah_services_messages");
		redirect(base_url().'services');
		exit();
	}
	function removeimage($serviceid,$imagename){
		
		unlink('resources/services/'.$this->_login_userid."/".$imagename);
		$result = $this->services->getdata($serviceid,'ah_services',"serviceid");
		if($result->attachment !=""){
			$image=@explode(',',$result->attachment);
			foreach($image as $key=>$vals){
				if($imagename == $vals){
					unset($image[$key]);
				}
			}
			$data["attachment"]=@implode(',',$image);
			$data["serviceid"] = $serviceid;
			$this->services->savestore($data,'ah_services','serviceid');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
		}
		redirect(base_url().'services');
		exit();
	}
	function removeimage_other($serviceid,$imagename){
		
		unlink('resources/services/'.$this->_login_userid."/".$imagename);
		$result = $this->services->getdata($serviceid,'ah_services_other',"otherserviceid");
		if($result->attachment !=""){
			$image=@explode(',',$result->attachment);
			foreach($image as $key=>$vals){
				if($imagename == $vals){
					unset($image[$key]);
				}
			}
			$data["attachment"]=@implode(',',$image);
			$data["otherserviceid"] = $serviceid;
			$this->services->savestore($data,'ah_services_other','otherserviceid');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
		}
		redirect(base_url().'services');
		exit();
	}
	function removeimage_additional($serviceid,$imagename){
		
		unlink('resources/services/'.$this->_login_userid."/".$imagename);
		$result = $this->services->getdata($serviceid,'ah_services_other',"otherserviceid");
		if($result->attachment_additional !=""){
			$image=@explode(',',$result->attachment_additional);
			foreach($image as $key=>$vals){
				if($imagename == $vals){
					unset($image[$key]);
				}
			}
			$data["attachment_additional"]=@implode(',',$image);
			$data["otherserviceid"] = $serviceid;
			$this->services->savestore($data,'ah_services_other','otherserviceid');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
		}
		redirect(base_url().'services');
		exit();
	}
	
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
				
		if(!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
	public function ajax_all_datas()
	{
			if($this->userroleid  == $this->administrator){
				$this->db->select('serviceid,title,description,attachment,dates,userid,department,isread,status');
				$this->db->from('ah_services');						
				$this->db->where("delete_record",'0');
				$this->db->order_by('status','DESC');	
				$this->db->order_by('serviceid','DESC');
			}
			else if($this->userroleid  == $this->servicedepartment){
				$this->db->select('serviceid,title,description,attachment,dates,userid,department,isread,status');
				$this->db->from('ah_services');						
				$this->db->where("delete_record",'0');
				$this->db->order_by('status','DESC');
				$this->db->order_by('serviceid','DESC');
			}
			else{
				$this->db->select('serviceid,title,description,attachment,dates,userid,department,isread,status');
				$this->db->from('ah_services');						
				$this->db->where("delete_record",'0');
				$this->db->where("userid",$this->_login_userid);
				$this->db->order_by('status','DESC');
				$this->db->order_by('serviceid','DESC');
			}
			
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
			
			
			foreach($query->result() as $lc)
			{
				
				
				if($lc->serviceid)
				{
					
					$this->db->select('servcemsgid');
					$this->db->from('ah_services_messages');						
					$this->db->where("delete_record",'0');
					$this->db->where("serviceid",$lc->serviceid);
					$query1 = $this->db->get();
					
					$action 	='<a onclick="alatadad(this);" data-url="'.base_url().'services/view/'.$lc->serviceid.'" href="#"><i class="my icon icon-eye-open"></i></a>';
					if($this->userroleid  != $this->servicedepartment){
						if(count($query1->result())>0){
							$action 	.='<a onclick="alatadad(this);" data-url="'.base_url().'services/viewmessage/'.$lc->serviceid.'" href="#"><i class="myicon icon-comment"></i></a>';
						}
					}else{
							$action 	.='<a onclick="alatadad(this);" data-url="'.base_url().'services/viewmessage/'.$lc->serviceid.'" href="#"><i class="myicon icon-comment"></i></a>';	
						}
					
					//$action 	=  '<a href="'.base_url().'services/view/'.$lc->serviceid.'"  href="#"><i class="icon-eye-open"></i></a> ';
				}
				if($lc->status == "Pending" and $lc->userid == $this->_login_userid and $permissions[$this->moduleid]['u'] ){
					$action	.= ' <a href="'.base_url().'services/request/'.$lc->serviceid.'" ><i class="icon-pencil"></i></a>';
				}
				if($this->userroleid == $this->servicedepartment ){
					$action	.= '<a onclick="alatadad(this);" data-url="'.base_url().'services/addcostdetails/'.$lc->serviceid.'" href="#" titile="معلومات التكلفة"><i style="color:#00CC00;" class="myicon icon-plus-sign-alt" titile="معلومات التكلفة" ></i></a>';
				}
				
				
				if($permissions[$this->moduleid]['d']	==	1 and $lc->status != "Accepted") 
				{
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->serviceid.'" data-url="'.base_url().'services/delete/'.$lc->serviceid.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
				$status ='';
				if($lc->status == "Pending")
				{
					$status	=	'<img src="'.base_url().'assets/images/pending.png" style="width: 24px;" alt="Pending" title="Pending"/>';
				}
				else if($lc->status == "In Progress")
				{
					$status	=	'<img src="'.base_url().'assets/images/inprogress.png" style="width: 24px;" alt="In Progress" title="In Progress"/>';
				}
				else if($lc->status == "Accepted")
				{
					$status	=	'<img src="'.base_url().'assets/images/completed.png" style="width: 24px;" alt="Accepted" title="Accepted"/>';
				}
				else if($lc->status == "Rejected")
				{
					$status	=	'<img src="'.base_url().'assets/images/rejected.png" style="width: 24px;" alt="Rejected" title="Rejected"/>';
				}
			$this->db->select('fullname');
			$this->db->from('ah_userprofile');						
			$this->db->where("userid",$lc->userid);
			$rowuser = $this->db->get(); 
			foreach($rowuser->result() as $us){$name = $us->fullname;}
				$arr[] = array(
					"DT_RowId"		=>	$lc->serviceid.'_durar_lm',
					"الموضوع" 		=> $lc->title,	
					"الوصف" 	=>	$lc->description,
					//"المرفقات" 	=>	$lc->attachment,
					"التاريخ" 	=>	arabic_date($lc->dates),
					"إسم المرسل" 		=>	$name, //$this->haya_model->get_name_from_list($lc->country_id),
					"من قسم" 		=>	$this->haya_model->get_name_from_list($lc->department),
					"الحالة" 		=>	$status,
					"الإجرائات" 		=>	$action
					);
					
					unset($action,$items);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
	}
	function addmessage(){
		if($this->input->post())
		{
			$data	=	$this->input->post();
			$data['dates']	=	date("Y-m-d");
			$data['userid']	=	$this->_login_userid;
			$data['status']	=	'0';
			$this->services->savemessage($data,'ah_services_messages');
			$service["serviceid"] =  $data['serviceid'];
			$service["status"] =  'In Progress';
			$service["isread"] =  '1';
			$this->services->savestore($service,'ah_services','serviceid');
			$ex['msg'] = "حفظ الرسالة بنجاح";
			echo json_encode($ex);
		}
	}
	function viewmessage($serviceid){
		$this->_data["rows"] = $this->services->getdata($serviceid,'ah_services',"serviceid");
		$this->_data["rowsmessage"] = $this->services->getdmessagedata($serviceid,'ah_services_messages',"serviceid");
		$this->_data["serviceid"]=$serviceid;
		$this->load->view('viewmessage',$this->_data);
	}
	function addcostdetails($serviceid){
		$this->_data["rows"] = $this->services->getdata($serviceid,'ah_services',"serviceid");
		$this->_data["serviceid"]=$serviceid;
		$this->load->view('addcostdetails',$this->_data);
	}
	function saveAddinfo(){
		if($this->input->post())
		{
			$data	=	$this->input->post();
			$data['responsedate']	=	date("Y-m-d");
			$data['servceuserid']	=	$this->_login_userid;
			$data['status']	=	'In Progress';
			
			$this->services->savestore($data,'ah_services','serviceid');
			
			$ex['msg'] = "حفظ الرسالة بنجاح";
			echo json_encode($ex);
		}
	}
	function submitfinished(){
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			$this->services->savestore($data,'ah_services','serviceid');
			
			$ex['msg'] = "حفظ الرسالة بنجاح";
			echo json_encode($ex);
		}
	}
	function list_otherservices(){
		$permissions	=	$this->haya_model->check_other_permission(array($this->otherservice_moduleid));
		$this->load->view('listall_otherservices',$this->_data);
	}//function list_otherservices(){
	public function other_serviece_ajax_all_datas()
	{
			//780 - administrator
			//1072 - service department
			//echo $this->userroleid;exit();
			if($this->userroleid  == $this->administrator){
				$this->db->select('otherserviceid,title,description,attachment,dates,userid,department,isread,status');
				$this->db->from('ah_services_other');						
				$this->db->where("delete_record",'0');
				$this->db->order_by('status','DESC');	
				$this->db->order_by('otherserviceid','DESC');
			}
			else if($this->userroleid  == $this->servicedepartment){
				$this->db->select('otherserviceid,title,description,attachment,dates,userid,department,isread,status');
				$this->db->from('ah_services_other');						
				$this->db->where("delete_record",'0');
				$this->db->order_by('status','DESC');
				$this->db->order_by('otherserviceid','DESC');
			}
			else{
				$this->db->select('otherserviceid,title,description,attachment,dates,userid,department,isread,status');
				$this->db->from('ah_services_other');						
				$this->db->where("delete_record",'0');
				$this->db->where("userid",$this->_login_userid);
				$this->db->or_where_in("forwhomid",$this->_login_userid);
				$this->db->order_by('status','DESC');
				$this->db->order_by('otherserviceid','DESC');
			}
			
			$query = $this->db->get();
			//print_r($query->result());exit();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
			
			
			foreach($query->result() as $lc)
			{
				
				
				if($lc->otherserviceid)
				{
					$this->db->select('servcemsgid');
					$this->db->from(' ah_services_other_messages');						
					$this->db->where("delete_record",'0');
					$this->db->where("otherserviceid",$lc->otherserviceid);
					$query1 = $this->db->get();
					
					
					$action 	='<a onclick="alatadad(this);" data-url="'.base_url().'services/other_view/'.$lc->otherserviceid.'" href="#"><i class="my icon icon-eye-open"></i></a>';
					
					if($this->userroleid  != $this->servicedepartment){
						if(count($query1->result())>0){
							$action 	.='<a onclick="alatadad(this);" data-url="'.base_url().'services/other_viewmessage/'.$lc->otherserviceid.'" href="#"><i class="myicon icon-comment"></i></a>';
						}
					}else{
							$action 	.='<a onclick="alatadad(this);" data-url="'.base_url().'services/other_viewmessage/'.$lc->otherserviceid.'" href="#"><i class="myicon icon-comment"></i></a>';	
						}
					
					
					
					//$action 	=  '<a href="'.base_url().'services/view/'.$lc->serviceid.'"  href="#"><i class="icon-eye-open"></i></a> ';
				}
				if($lc->status == "Pending" and $lc->userid == $this->_login_userid  ){
					$action	.= ' <a href="'.base_url().'services/other_serviece_request/'.$lc->otherserviceid.'" ><i class="icon-pencil"></i></a>';
				}
				if($this->userroleid == $this->servicedepartment ){
					$action	.= '<a href="'.base_url().'services/other_addcostdetails/'.$lc->otherserviceid.'"   titile="معلومات التكلفة"><i style="color:#00CC00;" class="myicon icon-plus-sign-alt" titile="معلومات التكلفة" ></i></a>';
				}
				
				
				if($permissions[$this->otherservice_moduleid]['d']	==	1 and $lc->status != "Accepted") 
				{
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->otherserviceid.'" data-url="'.base_url().'services/other_delete/'.$lc->serviceid.'/inquery"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
				$status ='';
				if($lc->status == "Pending")
				{
					$status	=	'<img src="'.base_url().'assets/images/pending.png" style="width: 24px;" alt="Pending" title="Pending"/>';
				}
				else if($lc->status == "In Progress")
				{
					$status	=	'<img src="'.base_url().'assets/images/inprogress.png" style="width: 24px;" alt="In Progress" title="In Progress"/>';
				}
				else if($lc->status == "Accepted")
				{
					$status	=	'<img src="'.base_url().'assets/images/completed.png" style="width: 24px;" alt="Accepted" title="Accepted"/>';
				}
				else if($lc->status == "Rejected")
				{
					$status	=	'<img src="'.base_url().'assets/images/rejected.png" style="width: 24px;" alt="Rejected" title="Rejected"/>';
				}
			$this->db->select('fullname');
			$this->db->from('ah_userprofile');						
			$this->db->where("userid",$lc->userid);
			$rowuser = $this->db->get(); 
			foreach($rowuser->result() as $us){$name = $us->fullname;}
				$arr[] = array(
					"DT_RowId"		=>	$lc->otherserviceid.'_durar_lm',
					"الموضوع" 		=> $lc->title,	
					"الوصف" 	=>	$lc->description,
					//"المرفقات" 	=>	$lc->attachment,
					"التاريخ" 	=>	arabic_date($lc->dates),
					"إسم المرسل" 		=>	$name, //$this->haya_model->get_name_from_list($lc->country_id),
					"من قسم" 		=>	$this->haya_model->get_name_from_list($lc->department),
					"الحالة" 		=>	$status,
					"الإجرائات" 		=>	$action
					);
					
					unset($action,$items);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
	}
	public function other_serviece_request($otherserviceid	=	NULL)
	{
		$this->_data["otherserviceid"]= $otherserviceid;
		if($this->input->post())
		{
			$data	=	$this->input->post();
			$services	=	array();
			$services['title']	=	$data['title'];
			$services['description']	=	$data['description'];
			$services['status']	=	"Pending";
			$services['department']	=	$this->userroleid;
			
		if($data["otherserviceid"] != ""){
			$services['otherserviceid']	=$data["otherserviceid"];
			if(isset($_FILES["attachment"]) && count($_FILES["attachment"]) > 0)
			{
				$names = array();
				$files = $_FILES["attachment"];
				 foreach ($files['name'] as $key => $image) {
					if(!empty($files['name'][$key]))
					{
						$_FILES['images']['name']= $files['name'][$key];
						$_FILES['images']['type']= $files['type'][$key];
						$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['images']['error']= $files['error'][$key];
						$_FILES['images']['size']= $files['size'][$key];
						$names[] =	$this->upload_file($this->_login_userid,'images','resources/services');
					}
					
				}
				if($data["attachment_old"] !=""){
					if(count($names)>0)	$services['attachment']	=	$data["attachment_old"].",".@implode(',',$names);
					else{$services['attachment']	=	$data["attachment_old"];}
				}
				else{
					$services['attachment']	=	@implode(',',$names);	
				}
				
			}
			else{
				$services['attachment']	=	$data["attachment_old"];
			}
			$otherserviceid = $this->services->savestore($services,'ah_services_other','otherserviceid');
			$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
				redirect(base_url().'services/list_otherservices');
				exit();
		}
		else{
			$services['dates']	=	date("Y-m-d");
			$services['userid']	=	$this->_login_userid;
			$services['isread']	=	'0';
			$services['title']	=	$data['title'];
			$services['description']	=	$data['description'];
			$services['status']	=	"Pending";
			$services['department']	=	$this->userroleid;
			
			if(isset($_FILES["attachment"]) && count($_FILES["attachment"]) > 0)
			{
				$names = array();
				$files = $_FILES["attachment"];
				 foreach ($files['name'] as $key => $image) {
					if(!empty($files['name'][$key]))
					{
						$_FILES['images']['name']= $files['name'][$key];
						$_FILES['images']['type']= $files['type'][$key];
						$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['images']['error']= $files['error'][$key];
						$_FILES['images']['size']= $files['size'][$key];
						$names[] =	$this->upload_file($this->_login_userid,'images','resources/services');
					}
					
				}
				$services['attachment']	=	@implode(',',$names);
			}
			$otherserviceid =$this->services->savestore($services,'ah_services_other','otherserviceid');
			$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
				redirect(base_url().'services/list_otherservices');
				exit();
		}
			
		}
		else{
			if($otherserviceid >0){
				$this->_data["rows"] = $this->services->getdata($otherserviceid,'ah_services_other',"otherserviceid");
			}
			$this->load->view('other_serviece_request',$this->_data);
		}
			
	}
	function other_delete($otherserviceid){
		$this->services->delete($otherserviceid,'otherserviceid',"ah_services_other");
		
		redirect(base_url().'services');
		exit();
	}
	public function other_view($otherserviceid	=	NULL)
	{
		$this->_data["rows"] = $this->services->getdata($otherserviceid,'ah_services_other',"otherserviceid");
		$this->db->select('fullname');
		$this->db->from('ah_userprofile');						
		$this->db->where("userid",$this->_data["rows"]->userid);
		$rowuser = $this->db->get(); 
		$this->_data["message"]  = $this->services->getdmessagedata($otherserviceid,'ah_services_other_messages',"otherserviceid");
		
		foreach($rowuser->result() as $us){$this->_data["usernames"] = $us->fullname;}
		$status ='';
			if($this->_data["rows"]->status == "Pending")
			{
				$status	=	'<img src="'.base_url().'assets/images/pending.png" style="width: 24px;" alt="Pending" title="Pending"/>';
			}
			else if($this->_data["rows"]->status == "In Progress")
			{
				$status	=	'<img src="'.base_url().'assets/images/inprogress.png" style="width: 24px;" alt="In Progress" title="In Progress"/>';
			}
			else if($this->_data["rows"]->status == "Accepted")
			{
				$status	=	'<img src="'.base_url().'assets/images/completed.png" style="width: 24px;" alt="Accepted" title="Accepted"/>';
			}
			else if($this->_data["rows"]->status == "Rejected")
			{
				$status	=	'<img src="'.base_url().'assets/images/rejected.png" style="width: 24px;" alt="Rejected" title="Rejected"/>';
			}
			$this->_data["status"] = $status;
		$this->load->view('other_view_service',$this->_data);
	}
	function other_submitfinished(){
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			$this->services->savestore($data,'ah_services_other','otherserviceid');
			
			$ex['msg'] = "حفظ الرسالة بنجاح";
			echo json_encode($ex);
		}
	}
	function other_addmessage(){
		if($this->input->post())
		{
			$data	=	$this->input->post();
			$data['dates']	=	date("Y-m-d");
			$data['userid']	=	$this->_login_userid;
			$data['status']	=	'0';
			$this->services->savemessage($data,'ah_services_other_messages');
			$service["otherserviceid"] =  $data['otherserviceid'];
			$service["status"] =  'In Progress';
			$service["isread"] =  '1';
			$this->services->savestore($service,'ah_services_other','otherserviceid');
			$ex['msg'] = "حفظ الرسالة بنجاح";
			echo json_encode($ex);
		}
	}
	function other_viewmessage($otherserviceid){
		$this->_data["rows"] = $this->services->getdata($otherserviceid,'ah_services_other',"otherserviceid");
		$this->_data["rowsmessage"] = $this->services->getdmessagedata($otherserviceid,'ah_services_other_messages',"otherserviceid");
		$this->_data["otherserviceid"]=$otherserviceid;
		$this->load->view('other_viewmessage',$this->_data);
	}
	public function other_loadmessage(){
		$data	=	$this->input->get();
		$otherserviceid = $data["otherserviceid"];
		$rowsmessage = $this->services->getdmessagedata($otherserviceid,'ah_services_other_messages',"otherserviceid");
		$message ='<table width="95%" border="0" cellspacing="0" cellpadding="10" align="center" style="text-align:right;">';
               if(count($rowsmessage)){
						  foreach($rowsmessage as $rw){
							  $this->db->select('fullname');
								$this->db->from('ah_userprofile');						
								$this->db->where("userid",$rw->userid);
								
								$rowuser = $this->db->get();
								foreach($rowuser->result() as $us){$fullname = $us->fullname;}
						 
               $message .='<tr>
                       <td  class="right">'.arabic_date($rw->dates).' : <b class="text-warning" style="margin-right:10px;">'.$fullname.'</b></td>
                         <td width="70%">'.$rw->message.'</td>
                      </tr>';
                      }
					  
					  }
                   $message .=' </table>';
                    $ex['message'] = $message;
			echo json_encode($ex);
	}
	function other_addcostdetails($otherserviceid){
		$this->_data["rows"] = $this->services->getdata($otherserviceid,'ah_services_other',"otherserviceid");
		$this->_data['all_users']		=	$this->haya_model->get_all_users();
		$this->_data["otherserviceid"]=$otherserviceid;
		if($this->input->post())
		{
			$data	=	$this->input->post();
			if($data["otherserviceid"] != ""){
				$forw = $data["forwhomid"];
				$data1["forwhomid"] = $forw;
				$data1["service_description"] = $data["service_description"];
				$data1["amount"] = $data["amount"];
				$data1["qty"] = $data["qty"];
				$data1["fromdate"] = $data["fromdate"];
				$data1["todate"] = $data["todate"];
				$data1["otherserviceid"] = $data["otherserviceid"];
				$data1["servceuserid"] = $this->_login_userid;
				$data1["status"]="In Progress";
				if(isset($_FILES["attachment_additional"]) && count($_FILES["attachment_additional"]) > 0)
				{
					$names = array();
					$files = $_FILES["attachment_additional"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/services');
						}
						
					}
					if($data["attachment_additional_old"] !=""){
						if(count($names)>0)$data1['attachment_additional']	=	$data["attachment_additional_old"].",".@implode(',',$names);
						else{$data1['attachment_additional']		=	$data["attachment_additional_old"];}
					}
					else{
						$data1['attachment_additional']	=	@implode(',',$names);	
					}
					
				}
				else{
					$data1['attachment_additional']	=	$data["attachment_additional_old"];
				}
				
				$otherserviceid = $this->services->savestore($data1,'ah_services_other','otherserviceid');
				$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
					redirect(base_url().'services/list_otherservices');
					exit();
			}
			else{
				
				$data1["forwhomid"] =  $data["forwhomid"];
				$data1["service_description"] = $data["service_description"];
				$data1["amount"] = $data["amount"];
				$data1["fromdate"] = $data["fromdate"];
				$data1["todate"] = $data["todate"];
				$data1["qty"] = $data["qty"];
				$data1["servceuserid"] = $this->_login_userid;
				
				$data1["status"]="In Progress";
				if(isset($_FILES["attachment_additional"]) && count($_FILES["attachment_additional"]) > 0)
				{
					$names = array();
					$files = $_FILES["attachment_additional"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/services');
						}
						
					}
					$data["attachment_additional"] = @implode(',',$names);
					
				}
				
				
				$otherserviceid = $this->services->savestore($data1,'ah_services_other','otherserviceid');
				$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
					redirect(base_url().'services/list_otherservices');
					exit();
			
			}
		}
		else{
			if($otherserviceid >0){
				$this->_data["rows"] = $this->services->getdata($otherserviceid,'ah_services_other',"otherserviceid");
			}
		}
		$this->load->view('other_addcostdetails',$this->_data);
	}
	
}
?>
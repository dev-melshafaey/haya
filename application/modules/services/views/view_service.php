<div id="user-profile">
<div class="row" dir="rtl">
    <div class="col-md-12 fox leftborder">
      <h4 class="panel-title customhr">طلب خدمة صيانة </h4>
      <table width="95%" border="0" cellspacing="0" cellpadding="10" align="center">
          <tr>
                <td class="right"><strong>الموضوع</strong><br /><?php echo $rows->title;?></td>
                <td width="40%"><strong>الوصف</strong><br /><?php echo $rows->description;?></td>
                <td><strong>التاريخ</strong><br /><?php echo arabic_date($rows->dates);?></td>
                <td><strong>إسم المرسل</strong><br /><?php echo $usernames;?></td>
          </tr>
            <tr>
                <td class="right"><strong>من قسم</strong><br /><?php echo $this->haya_model->get_name_from_list($rows->department);?></td>
                <td width="40%"><strong>الحالة</strong><br /><?php echo $status;?></td>
                <td><strong>المرفقات</strong><br />
                <?php
                if($rows->attachment !=""){
                $attachment = @explode(',',$rows->attachment);
                foreach($attachment as $att){
                if($att !=""){
                ?>
                <a class="fancybox-button" rel="gallery1" href="<?php echo base_url()?>resources/services/<?php echo $rows->userid."/".$att;?>"><img src="<?php echo base_url()?>resources/services/<?php echo$rows->userid."/".$att;?>" width="50" /></a>
                <?php 
                }
                }
                }
                ?>
                </td>
                <td></td>
          </tr>
           <tr>
                <td class="right"><strong>التلكلفة إذا وجدت</strong><br /><?php echo $rows->cost;?></td>
                <td width="40%"><strong>رقم الفاتورة</strong><br /><?php echo $rows->invoiceno;?></td>
                <td><strong>ملاحظات</strong><br /><?php echo $rows->notes;?></td>
                <td></td>
          </tr>
        </table>
        <?php if(count($message)>0){?>
         <h4 class="panel-title customhr">الرسائل</h4>
         <table width="95%" border="0" cellspacing="0" cellpadding="10" align="center">
         <?php 
			 	foreach($message as $msg){
					 $this->db->select('fullname');
					$this->db->from('ah_userprofile');						
					$this->db->where("userid",$msg->userid);
					$rowuser = $this->db->get();
					foreach($rowuser->result() as $us){$fullname = $us->fullname;}
			 ?>
          <tr>
                <td class="right" width="20%"><?php echo arabic_date($msg->dates);?></td>
                <td width="20%"><?php echo $fullname;?></td>
                <td colspan="2" width="60%"><?php echo $msg->message;?></td>
          </tr>
          <?php }
		  ?>
		 
         </table>
         <?php }
		  
		  ?>
    </div>
  </div>
</div>
</div>
</div>
<div class="row" style="text-align: center;">
  <button type="button" id="" onClick="printthepage('user-profile');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
  <?php if($user_detail['profile']->userroleid == $servicedepartment){ ?>
  <button type="button" id="submitfinished" name="submitfinished" class="btn btn-success" onClick="submitfinished('<?php echo $rows->serviceid;?>','Accepted','submitfinished');" >تم الإنتهاء</button>
  <button type="button" id="submitreject" name="submitreject" class="btn btn-default" onClick="submitfinished('<?php echo $rows->serviceid;?>','Rejected','submitreject');" >لم يتم الانتهاء</button>
  <?php } ?>
</div>

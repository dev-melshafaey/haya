<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block" style="padding: 10px 10px;">
          <form method="POST" id="form_addcost" name="form_addcost" enctype="multipart/form-data" >
                 	
                     <input type="hidden"  name="otherserviceid" id="otherserviceid" value="<?php echo $otherserviceid ?>" />
                     <div class="form-group col-md-12">
                      <label for="basic-input"><strong>Description:</strong></label>
                      <textarea name="service_description" placeholder="Description" class="form-control req" style="resize:none; height:100px;" id="service_description"><?php echo $rows->service_description?></textarea>
                    </div>
                    
                    <div class="form-group col-md-3">
                      <label for="basic-input"><strong>For Whom:</strong></label>
                      <select class="form-control req" id="forwhomid" name="forwhomid" >
                      <?php 
					  if(count($all_users)>0){
						  $forwhomid = @explode(',',$rows->forwhomid);
						  
						  foreach($all_users as $users){
							 if( array_search($users->userid,$forwhomid))
							  {
								  echo '<option value="'.$users->userid.'" selected="selected">'.$users->fullname.'</option>';
							  }
							  else{ echo '<option value="'.$users->userid.'" >'.$users->fullname.'</option>';}
							  
						  }
					  }
					  ?>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="basic-input"><strong>Amount:</strong></label>
                      <input type="text" class="form-control req" value="<?php echo $rows->amount?>" placeholder="Amount" name="amount" id="amount" />
                    </div>
                     <div class="form-group col-md-3">
                      <label for="basic-input"><strong>Quantity:</strong></label>
                      <input type="text" class="form-control req" value="<?php echo $rows->qty?>" placeholder="qty" name="qty" id="qty" />
                    </div>
                    <div class="col-md-3 form-group">
                        <label class="text-warning">تاريخ البدء:</label>
                        <input type="text" name="fromdate" id="fromdate" class="datepicker form-control req" placeholder="تاريخ البدء‎" value="<?php if($rows->fromdate){ echo date('Y-m-d',strtotime($rows->fromdate)); }?>" />
                      </div>
                     <div class="col-md-3 form-group">
                        <label class="text-warning">تاريخ الانتهاء:</label>
                        <input type="text" name="todate" id="todate" class="datepicker form-control req" placeholder="تاريخ الانتهاء‎" value="<?php if($rows->todate){ echo date('Y-m-d',strtotime($rows->todate)); } ?>" />
                      </div> 
                    
                    <div class="form-group col-md-6">
              <label for="basic-input"><strong>المرفقات :</strong></label>
              <input type="file" class="form-control" name="attachment_additional[]" id="attachment_additional" multiple  /><br/>
               <input type="hidden"  name="attachment_additional_old" id="attachment_additional_old" value="<?php echo $rows->attachment_additional?>" />
              	 <?php if($rows->attachment_additional !=""){
				   $i=0;
				   $attachment = @explode(',',$rows->attachment_additional);
				   foreach($attachment as $att){
					   if($att !=""){
						   $i++;
				   ?>
                   <div id="picture_<?php echo $i;?>" style="float:right;">
               <a class="fancybox-button" rel="gallery1" href="<?php echo base_url()?>resources/services/<?php echo $login_userid."/".$att?>"><img src="<?php echo base_url()?>resources/services/<?php echo $login_userid."/".$att?>" style="width:50px;" alt="image" title="image"/></a>
              <a class="iconspace" href="#deleteDiag" onClick="show_delete_dialogs(this,'picture_<?php echo $i;?>','<?php echo $att?>');" id="<?php echo  $rows->otherserviceid ?>" data-url="<?php echo base_url()?>services/removeimage_additional/<?php echo $rows->otherserviceid ?>/<?php echo $att ?>"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>
              </div>
              <?php } } }?>
              
            </div>
                    
                    
                     <br clear="all">
                    <div class="form-group col-md-6">
                      <button type="button" id="save_service_addinfo" name="save_service_addinfo" class="btn btn-success"  >حفظ</button>
                    </div>
                 </form>
          <br clear="all">
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
 $(document).ready(function(){
	$('#save_service_addinfo').click(function () {
		check_my_session();
        $('#form_addcost .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_addcost .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_addcost .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                $("#form_addcost").submit();
            
        }
        else 
		{
            show_notification_error(ht);
        }
    });
});
</script>

</div>
</body>
</html>
<script>
$(function(){
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});


</script>
<script>
function show_delete_dialogs(durar,id,image) {
	check_my_session();
    var url_redirect = $(durar).attr("data-url");
    var did = $(durar).attr('id');
    $('#delete_id').val(did);
    $('#action_url').val(url_redirect);
    $('#deleteDiag').modal();
	var attachment_old = $('#attachment_additional_old').val();
	var attachment = attachment_old.split(',');
	var index = attachment.indexOf(image);
	if (index > -1) {
	 attachment.splice(index, 1);
	}
	attachment.toString();
	$('#attachment_additional_old').val(attachment);
	$('#'+id).html('');
}
</script>
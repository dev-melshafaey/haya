<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block" style="padding: 10px 10px;">
          <form method="POST" id="form_service" name="form_service" enctype="multipart/form-data">
           <input type="hidden"  name="otherserviceid" id="otherserviceid" value="<?php echo $rows->otherserviceid?>" />
            <div class="form-group col-md-6">
              <label for="basic-input"><strong>الموضوع:</strong></label>
              <input type="text" class="form-control req" value="<?php echo $rows->title?>" placeholder="Title" name="title" id="title" />
            </div>
             <div class="form-group col-md-6">
              <label for="basic-input"><strong>المرفقات :</strong></label>
              <input type="file" class="form-control" name="attachment[]" id="attachment" multiple /><br/>
               <input type="hidden"  name="attachment_old" id="attachment_old" value="<?php echo $rows->attachment?>" />
                <?php if($rows->attachment !=""){
				   $i=0;
				   $attachment = @explode(',',$rows->attachment);
				   foreach($attachment as $att){
					   if($att !=""){
						   $i++;
				   ?>
                   <div id="picture_<?php echo $i;?>" style="float:right;">
               <a class="fancybox-button" rel="gallery1" href="<?php echo base_url()?>resources/services/<?php echo $login_userid."/".$att?>"><img src="<?php echo base_url()?>resources/services/<?php echo $login_userid."/".$att?>" style="width: 50px;" alt="image" title="image"/></a>
              <a class="iconspace" href="#deleteDiag" onClick="show_delete_dialogs(this,'picture_<?php echo $i;?>','<?php echo $att?>');" id="<?php echo  $rows->otherserviceid ?>" data-url="<?php echo base_url()?>services/removeimage_other/<?php echo $rows->otherserviceid ?>/<?php echo $att ?>"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>
              </div>
              <?php } } }?>
            </div>
             <div class="form-group col-md-6">
              <label for="basic-input"><strong>الوصف :</strong></label>
              <textarea name="description" placeholder="Description" class="form-control req" style="resize:none; height:200px;" id="description"><?php echo $rows->description?></textarea>
            </div>
             
<!--             <div class="form-group col-md-6">
              <label class="basic-input"><strong>من قسم :</strong></label>
              <?PHP //echo $this->haya_model->create_dropbox_list('departments','departments',$rows->department,0,'req'); ?> 
            </div>
                        
-->           
            
            <br clear="all">
            <div class="form-group col-md-6">
              <button type="button" id="save_service" name="save_service" class="btn btn-success">حفظ</button>
            </div>
          </form>
          <br clear="all">
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
 $(document).ready(function(){
	$('#save_service').click(function () {
		check_my_session();
        $('#form_service .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_service .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_service .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                $("#form_service").submit();
            
        }
        else 
		{
            show_notification_error(ht);
        }
    });
});
</script>
</div>
</body>
</html><script>
function show_delete_dialogs(durar,id,image) {
	check_my_session();
    var url_redirect = $(durar).attr("data-url");
    var did = $(durar).attr('id');
    $('#delete_id').val(did);
    $('#action_url').val(url_redirect);
    $('#deleteDiag').modal();
	var attachment_old = $('#attachment_old').val();
	var attachment = attachment_old.split(',');
	var index = attachment.indexOf(image);
	if (index > -1) {
	 attachment.splice(index, 1);
	}
	attachment.toString();
	$('#attachment_old').val(attachment);
	$('#'+id).html('');
}
</script>
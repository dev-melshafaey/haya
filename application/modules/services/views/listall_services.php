<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
       <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"><?php echo add_button('services/request','طلب خدمة صيانة',$permissions[$module['moduleid']]['a']	==	1); ?></div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th>الموضوع</th>
                        <th>الوصف</th>
                        <th>التاريخ</th>
                        <th>إسم المرسل</th>                 
                        <th>من قسم</th>
                        <th>الحالة</th>
                        <th width="10%">الإجرائات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'services/ajax_all_datas/','columns_array'=>'{ "data": "الموضوع" },
                { "data": "الوصف" },
                { "data": "التاريخ" },
				{ "data": "إسم المرسل" },
                { "data": "من قسم" },
				{ "data": "الحالة" },
				{ "data": "الإجرائات" }')); ?>
</div>
</body>
</html>
<script>
	function submitmessage() {
		 $('#form_service_msg .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_service_msg .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_service_msg .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) { 
		$('#save_service_message').attr('disabled',true);
		$.ajax({
				url: config.BASE_URL+'services/addmessage/',
				type: "POST",
				data:{'serviceid':$('#serviceid').val(),'message':$('#message').val()},
				dataType: "json",
				success: function(data)
				{	show_notification(data.msg);
				loadmessage($('#serviceid').val());
				$('#save_service_message').attr('disabled',false);
				location.reload();
				 }
			});
		}
	}
	function loadmessage(serviceid){
	$.ajax({
			url: config.BASE_URL+'services/loadmessage/',
			type: "GET",
			data:{'serviceid':serviceid},
			dataType: "json",
			success: function(data)
			{
				$('#messagelisting').html(data.message);
			}
		});
}
function submitaddinfo(){
	$('#save_service_addinfo').attr('disabled',true);
		$.ajax({
				url: config.BASE_URL+'services/saveAddinfo/',
				type: "POST",
				data:{'serviceid':document.form_addcost.serviceid.value,'cost':document.form_addcost.cost.value,'invoiceno':document.form_addcost.invoiceno.value,'notes':document.form_addcost.notes.value },
				dataType: "json",
				success: function(data)
				{	show_notification(data.msg);
				$('#save_service_addinfo').attr('disabled',false);
				 }
			});
}
function submitfinished(serviceid,status,id){
	$('#'+id).attr('disabled',true);
		$.ajax({
				url: config.BASE_URL+'services/submitfinished/',
				type: "POST",
				data:{'serviceid':serviceid,'status':status },
				dataType: "json",
				success: function(data)
				{	show_notification(data.msg);
				$('#'+id).attr('disabled',false);
				location.reload();
				 }
			});
}



</script>

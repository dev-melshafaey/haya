<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services_model extends CI_Model
{
	/*
	*  Properties
	*/
	private $_table_users;
	private $_table_branchs;
	private $_table_listmanagement;
 	
//-------------------------------------------------------------------

	/*
	*  Constructor
	*/
	function __construct()
	{
		parent::__construct();

		//Get Table Names from Config 
		$this->_table_users 				= 	$this->config->item('table_users');
		$this->_table_branchs 				= 	$this->config->item('table_branchs');
		$this->_table_listmanagement 		= 	$this->config->item('table_listmanagement');
		$this->_login_userid			=	$this->session->userdata('userid');
	}
	function savestore($data,$table,$feild)
	{
		$serviceid = $data[$feild];
		if($serviceid	!=	'')
		{
			$this->db->where($feild,$serviceid);
			$this->db->update($table,json_encode($data),$this->_login_userid,$data);
			return $serviceid;
		}
		else
		{
			$this->db->insert($table,$data,json_encode($data),$this->_login_userid);
			return $this->db->insert_id();;
		}
	}
	function delete($serviceid,$field,$table)
	{
			$json_data	=	json_encode(array('record'	=>	'delete',$field	=>	$serviceid));
		$data		=	array('delete_record'=>'1');
		
		$this->db->where($field,$serviceid);

		$this->db->update($table,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
			
	}
	
	function getdata($serviceid,$table,$column){
		$this->db->select('*');
		$this->db->from($table);						
		$this->db->where($column,$serviceid);
		$query = $this->db->get(); 
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	function getdmessagedata($serviceid,$table,$field){
		$this->db->select('*');
		$this->db->from($table);						
		$this->db->where($field,$serviceid);
		$this->db->order_by('servcemsgid','DESC');
		$query = $this->db->get(); 
		return $query->result();
	}
	function savemessage($data,$table)
	{
		$this->db->insert($table,$data,json_encode($data),$this->_login_userid);
	}
}

?>
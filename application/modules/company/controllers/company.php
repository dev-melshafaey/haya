<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends CI_Controller 
{
//-------------------------------------------------------------------------------
	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;
	
//-------------------------------------------------------------------------------
	/*
	* Costructor
	*/

	public function __construct()
	{
		parent::__construct();	
		
		$this->load->model('inquiries/inquiries_model','inq');
		$this->load->model('company_model','company');
		$this->load->model('bank_management/bank_management_model','outside');
			
		$this->_data['module']			=	$this->haya_model->get_module();			
		$this->_login_userid 			=	$this->session->userdata('userid');
		$this->_data['login_userid'] 	=	$this->_login_userid;
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);
	}
//-----------------------------------------------------------------------
	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
    public function index()
    {
        $this->haya_model->check_permission($this->_data['module'],'v');

		$this->_data["muhasaba_types"]  =	$this->company->muhasaba_types_count();
		$this->_data['total_orphans']	=	$this->company->total_orphan_count();
		
		// Load Dynamic Forms Listing 
		$this->load->view('muhasaba-types',$this->_data);	 
    }

//-----------------------------------------------------------------------

	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
    public function quotationlisting()
    {
        $this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->load->view('quotationlist',$this->_data);
    }
//-----------------------------------------------------------------------
	/*
	* All Companies List
	*
	*/
	
	public function company_list()
	{
		// Load Company List View
		$this->load->view('companylist',$this->_data);
	}
	
//-----------------------------------------------------------------------
	/*
	* Add Company into database
	* @param int $registrationid
	*/
	public function registration($registrationid)
	{
		if($this->input->post())
		{
			$companyid = $this->input->post('companyid');
			
			foreach($this->input->post() as $postkey => $postvalue)
			{	
				$data[$postkey] 	=	$postvalue;
			}
			
			$data['userid'] 	=	$this->_login_userid; 		
			$data['branchid'] 	=	$this->haya_model->get_branch_id($this->_login_userid);
			$supplier_types 	=	$data['supplier_type_id'];
				
			unset($data['save_company_information'],$data['companyid'],$data['supplier_type_id'],$data['country_listmanagement_text']);
		
			if($companyid!='')
			{
				$this->db->where('companyid',$companyid);
				$this->db->update('ah_company',json_encode($data),$this->_login_userid,$data);
			}
			else
			{
				$this->db->insert('ah_company',$data,json_encode($data),$this->_login_userid);

				$companyid	=	$this->db->insert_id();
			}
			
			$this->db->query("DELETE FROM ah_company_category WHERE companyid='".$companyid."'");
			
			foreach($supplier_types as $spokey => $supplier_type_id)
			{
				$supplierType['companyid'] 			=	$companyid; 		
				$supplierType['supplier_type_id'] 	=	$supplier_type_id;
				
				$this->db->insert('ah_company_category',$supplierType,json_encode($supplierType),$this->_login_userid);
			}
			
			$this->load->library('user_agent');
			if ($this->agent->is_referral())
			{
				$url	=	 $this->agent->referrer();
			}
			
			$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
			redirect($url);
			exit();
			
		}
		
		$this->_data['own_supplier_types'] 	= $this->haya_model->get_selected_supplier_types($registrationid);
		
		$this->_data['company'] = $this->company->company_detail($registrationid);
		
		$this->haya_model->check_permission($this->_data['module'],'v');
		$this->load->view('company_register',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*
	*/
	public function company_documents($companyid)
	{
		$this->_data['companyid'] 		=	$companyid;	
				
		if($this->input->post())
		{
			$data	=	$this->input->post();
						
			if($_FILES["document"]["tmp_name"])
			{
				$document			=	$this->upload_file($data['userid'],'document','resources/users');
				$data['document']	=	$document;
			}
			
			$documentid	=	$this->company->add_document($data);
						
			$actions   =	' <a class="fancybox-button" rel="gallery1" href="'.base_url().'resources/users/'.$data['userid'].'/'.$data['document'].'"><i class="icon-eye-open" data-hasqtip="9" aria-describedby="qtip-9"></i></a>';
			$actions	.=	' <a onclick="button_delete(\''.trim($documentid).'\');" href="#_" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			
			$html	 =	'<tr role="row" id="'.$documentid.'_durar_lm">';
			$html	.=  '<td style="text-align:center;">'.$data['documenttitle'].'</td>';
			$html	.=  '<td style="text-align:center;">'.$actions.'</td>';
			$html	.=	'</tr>';
			
			echo  $html;
		}
		else
		{		
			$this->_data['company'] 		=	$this->company->get_company_info($companyid);
			$this->_data['documentlist'] 	=	$this->company->get_company_documentlist($companyid);	
			
			// Load View of Documents
			$this->load->view("company_documents",$this->_data);
		}
	}
//-----------------------------------------------------------------------
	public function companylist()
	{
		$this->db->select('ah_company.companyid,countryname.list_name as country, ah_company.english_name, ah_company.arabic_name,ah_company.companystatus,ah_company.cr_number, ah_listmanagement.list_name');
        $this->db->from('ah_company');
        $this->db->join('ah_listmanagement','ah_company.company_type_listmanagement=ah_listmanagement.list_id');
		$this->db->join('ah_listmanagement AS countryname','ah_company.country_listmanagement=countryname.list_id');
        $this->db->order_by("ah_company.registrationdate", "DESC");
		
		$query = $this->db->get();
		
		$permissions	=	$this->haya_model->check_other_permission(array('152'));	
		
		foreach($query->result() as $inq)
        {   
			$action = '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'company/getinfo_company/'.$inq->companyid.'/'.$inq->cr_number.'"><i class="icon-eye-open"></i></a>';
			$action .= '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'company/company_documents/'.$inq->companyid.'"><i class="icon-book"></i></a>';
			//$action .= $this->haya_model->print_icon(154,$inq->companyid);
			
			if($permissions[152]['u']	==	1)
			{	$action .= '&nbsp;<a class="iconspace" href="'.base_url().'company/registration/'.$inq->companyid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>'; 
				$english =  '<a class="iconspace" href="'.base_url().'company/registration/'.$inq->companyid.'">'.$inq->english_name.'</a>'; 
				$arabic = '<a class="iconspace" href="'.base_url().'company/registration/'.$inq->companyid.'">'.$inq->arabic_name.'</a>'; }
			else
			{
				$english = $inq->english_name;
				$arabic = $inq->arabic_name;
			}
			
			
			$arr[] = array(
				"DT_RowId"				=>	$inq->companyid.'_durar_lm',				
                "رقم"					=>	arabic_date($inq->companyid),
				"بلد"					=>	$inq->country,
				"اسم الانجليزية"			=>	$english,
				"اسم عربي"				=>	$arabic,
                "رقم السجل التجاري"		=>	arabic_date($inq->cr_number),
                "نوع الشركة"			=>	$inq->list_name,             
                "الإجرائات"				=>	$action);
		unset($action,$english,$arabic);		
       }
	   $ex['data'] = $arr;
       echo json_encode($ex);	
	}
//-----------------------------------------------------------------------
	public function quotationlist()
	{
		$quotations	=	$this->company->get_all_quotations();
		
		$permissions	=	$this->haya_model->check_other_permission(array('152'));

		foreach($quotations as $inq)
        {   
			$query		=	$this->db->query("SELECT COUNT(companyid) total_comp FROM `ah_quote_company` WHERE quoteid=".$inq->quoteid."");
			$total_comp	=	$query->row()->total_comp;
			
			if($total_comp	>	0)
			{
				$link	=	'<a href="'.base_url().'company/companies_list_by_quotation/'.$inq->quoteid.'">('.$total_comp.')</a>';
			}
			else
			{
				$link	=	'('.$total_comp.')';
			}
			
			$action  = 	'&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'company/view_quotation_info/'.$inq->quoteid.'"><i class="icon-eye-open"></i></a>';
			$action	.=	'&nbsp;<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$inq->quoteid.'" data-url="'.base_url().'company/delete_quotation/'.$inq->quoteid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			$action	.=	'&nbsp;<a href="'.base_url().'company/download_file/'.$inq->addedby.'/'.$inq->quote_attachment.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
			
			if($permissions[152]['u']	==	1)
			{
				$action .= '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'company/quotation/'.$inq->quoteid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			$arr[] = array(
				"DT_RowId"				=>	$inq->quoteid.'_durar_lm',
				"رقم"					=>	arabic_date($inq->quoteid),				
                "عنوان العرض"			=>	$inq->quote_title,
				"مجموع الشركات"			=>	$link,
				"حالة العرض"			=>	$inq->quote_status,
				"تاريخ الانتهاء"			=>	date('Y-m-d',strtotime($inq->expiry_date)),    
                "الإجرائات"				=>	$action);
				
		unset($action);
			
       }
	   
	   $ex['data'] = $arr;
       echo json_encode($ex);	
	}
//-----------------------------------------------------------------------
	public function companies_list_by_quotation($quoteid)
	{
		$this->_data['quoteid']	=	$quoteid;
		
		$this->load->view('companies-list-by-quotation',$this->_data);
	}
//-----------------------------------------------------------------------
	public function get_companies_by_quotation($quoteid)
	{
		$quotations	=	$this->company->get_companies_by_quotation($quoteid);
		
		$permissions	=	$this->haya_model->check_other_permission(array('152'));
		
		foreach($quotations as $inq)
        {   
			$query		=	$this->db->query("SELECT COUNT(itemsid) total_item FROM `ah_quote_items` WHERE quoteid=".$inq->comp_quoteid."");
			$total_item	=	$query->row()->total_item;
			

			$action  = 	'&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'company/view_companies_quotations/'.$inq->quoteid.'"><i class="icon-eye-open"></i></a>';
			//$action	.=	'&nbsp;<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$inq->quoteid.'" data-url="'.base_url().'company/delete_quotation/'.$inq->quoteid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			//$action	.=	'&nbsp;<a href="'.base_url().'company/download_file/'.$inq->addedby.'/'.$inq->quote_attachment.'"><i style="color:#73880A !important;" class="icon-download-alt"></i></a>';
			
			if($permissions[152]['u']	==	1)
			{
				//$action .= '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'company/quotation/'.$inq->quoteid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			$arr[] = array(
				"DT_RowId"				=>	$inq->quoteid.'_durar_lm',
				"رقم"					=>	arabic_date($inq->quoteid),				
                "اسم الانجليزية"			=>	$inq->english_name,
				"اسم عربي"				=>	$inq->arabic_name,
				"عرض السعر"				=>	$inq->quote_title,
				"تاريخ الانتهاء"			=>	date('Y-m-d',strtotime($inq->quote_data)),
				"مجموع الوحدات"			=>	$total_item,    
                "الإجرائات"				=>	$action);
				
		unset($action);
			
       }
	   
	   $ex['data'] = $arr;
       echo json_encode($ex);	

	}
//-----------------------------------------------------------------------
	public function getinfo_company($companyid=0,$cr_number=0)
	{
		$this->_data['company'] = $this->company->company_detail($companyid, $cr_number);
		$this->load->view('companyview',$this->_data);
	}
//-----------------------------------------------------------------------
	public function getCompanyListEnglish()
	{
		$term = $this->input->get('term');
		foreach($this->company->getCompanyName($term,'english_name') as $data)
		{
			$fullname = $data->english_name.' ('.$data->arabic_name.')';
			$arr[] = array('id'=>$data->companyid,'label'=>$fullname,'value'=>$data->companyid);
		}
		echo json_encode($arr);
	}
//-----------------------------------------------------------------------
	public function getCompanyListArabic()
	{
		$term = $this->input->get('term');
		foreach($this->company->getCompanyName($term,'arabic_name') as $data)
		{
			$fullname = $data->arabic_name.' ('.$data->english_name.')';
			$arr[] = array('id'=>$data->companyid,'label'=>$fullname,'value'=>$data->companyid);
		}
		echo json_encode($arr);
	}
//-----------------------------------------------------------------------
	public function get_cr_number()
	{
		$cr_number = $this->input->post('cr_number');
		$this->db->select('companyid');
		$this->db->from('ah_company');
		$this->db->where('cr_number',$cr_number);	
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{	
			$rowx =  $query->row(); 
			echo $rowx->companyid;
		}
		else
		{
			echo '0';
		}
	}
//----------------------------------------------------------------------------

	/*
	*
	* ALL Customers
	*
	*/
	
	public function customers()
	{
		// Load View of Customers
		$this->load->view('customers-listing',$this->_data);
	}
//----------------------------------------------------------------------------

	/*
	*
	* All Customers Listing By AJAX
	*/
	
	public function customers_listing()
	{
		$all_customers	=	$this->company->get_all_customers();
		
		$permissions	=	$this->haya_model->check_other_permission(array('153'));
		
		foreach($all_customers as $customer) 
		{
			if($permissions[153]['d']	==	1)
			{
				$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$customer->customer_id.'" data-url="'.base_url().'company/delete_customer/'.$customer->customer_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
			}
			if($permissions[153]['u']	==	1)
			{
				$actions 	.=	'<a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'company/add_customer/'.$customer->userid.'/'.$customer->customer_id.'" id="'.$customer->customer_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			}
			
			$arr[] = array(
				"DT_RowId"			=>	$customer->customer_id.'_durar_lm',
                "اسم الزبون" 		=>	$customer->customer_name,
				"عنوان العملاء" 		=>	$customer->customer_title,
				"اسم الشركة" 		=>	$customer->company_name,
				"الرمز البريدي" 	=>	$customer->postal_code,
				"الإجراءات"			=>	$actions
				);

			unset($actions);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-----------------------------------------------------------------------

	/*
	*	Add / Update Cusomer Data
	*	@param $userid integer
	*	@param $customer_id integer
	*/
	public function add_customer($userid,$customer_id	=	NULL)
	{
		if($this->input->post())
		{
			// Get all values from POST
			$data	=	$this->input->post();
			
			// UNSET the value from ARRAY
			unset($data['submit']);
			unset($data['province_text']);
			unset($data['wilaya_text'],$data['bankid_text'],$data['bankbranchid_text']);
			
			if($data['customer_id'])
			{
				// Update data into database
				$this->company->update_customer($data['customer_id'],$data);
			}
			else
			{
				// ADD data into database
				$this->company->add_customer($data);
			}
			
			// Redirect to listing page
			redirect(base_url().'company/customers');
			exit();
		}
		else
		{
			//
			$this->_data['customer']		=	$this->company->get_single_customer($customer_id);
			 
			$this->_data['customer_id']		=	isset($this->_data['customer']->customer_id) ? $this->_data['customer']->customer_id : NULL;
			$this->_data['province']		=	isset($this->_data['customer']->province) ? $this->_data['customer']->province : NULL;
			$this->_data['wilaya']			=	isset($this->_data['customer']->wilaya) ? $this->_data['customer']->wilaya : NULL;
			$this->_data['customer_name']	=	isset($this->_data['customer']->customer_name) ? $this->_data['customer']->customer_name : NULL;
			$this->_data['company_name']	=	isset($this->_data['customer']->company_name) ? $this->_data['customer']->company_name : NULL;
			$this->_data['customer_title']	=	isset($this->_data['customer']->customer_title) ? $this->_data['customer']->customer_title : NULL;
			$this->_data['address']			=	isset($this->_data['customer']->address) ? $this->_data['customer']->address : NULL;
			$this->_data['postal_code']		=	isset($this->_data['customer']->postal_code) ? $this->_data['customer']->postal_code : NULL;
			
			$this->_data['bankid']		=	isset($this->_data['customer']->bankid) ? $this->_data['customer']->bankid : NULL;
			$this->_data['bankbranchid']		=	isset($this->_data['customer']->bankbranchid) ? $this->_data['customer']->bankbranchid : NULL;
			$this->_data['accountnumber']		=	isset($this->_data['customer']->accountnumber) ? $this->_data['customer']->accountnumber : NULL;
			$this->_data['accounttype']		=	isset($this->_data['customer']->accounttype) ? $this->_data['customer']->accounttype : NULL;

			// Load Users Listing Page
			$this->load->view('add-customer',$this->_data);
		}

	}
//-----------------------------------------------------------------------

	/*
	*	Add / Update Cusomer Data
	*	@param $userid integer
	*	@param $customer_id integer
	*/
	public function add_budget($budgetid	=	NULL)
	{
		if($this->input->post())
		{
			// Get all values from POST
			$data			=	$this->input->post();
			$total_budget	=	array();
			$year			=	date('Y',strtotime($data['budget_year']));
			
			$yearly_budget	=	$this->company->check_yearly_budget($year);
			
			if(!empty($yearly_budget))
			{
				$this->session->set_flashdata('msg', 'You already put this year budget thanks.');
				
				// Redirect to listing page
				redirect(base_url().'company/department_budgets');
				exit();
			}
			
			// UNSET the value from ARRAY
			unset($data['submit']);

			if($data['budgetid'])
			{
				// Update data into database
				$this->company->update_budget($data['budgetid'],$data);
			}
			else
			{
				$total_budget['addedby']		=	$data['addedby'];
				$total_budget['budget_year']	=	$data['budget_year'];
				$total_budget['budget_amount']	=	$data['budget_amount'];
				
				// ADD Total Budget
				$budgetid	=	$this->company->add_budget($total_budget);
				
				foreach($data['department_budget']	as $key	=>	$value)
				{
					
					$department_budget	=	array('budgetid'	=>	$budgetid,'department'	=>	$key,'dept_budget'	=>	$value);
					
					// ADD Department Budget
					$this->company->add_dept_budget($department_budget);
				}
				
			}
			
			// Redirect to listing page
			redirect(base_url().'company/department_budgets');
			exit();
		}
		else
		{
			$budget		=	$this->company->get_single_budget($budgetid);

			$this->_data['budgetid']		=	isset($budget->budgetid) ? $budget->budgetid : NULL;
			$this->_data['budget_amount']	=	isset($budget->budget_amount) ? $budget->budget_amount : NULL;
			$this->_data['budget_year']		=	isset($budget->budget_year) ? $budget->budget_year : NULL;
			
			$this->_data['budget']			=	$budget;
			
			$this->_data['year'] = $this->company->get_budget_maxyear();
			
			if(!$this->_data['year'])
			{
				$this->_data['year']	=	date("Y");
			}
			
			// Load Users Listing Page
			$this->load->view('add-department-budgets',$this->_data);
		}
	}
	
//-----------------------------------------------------------------------

	/*
	*	Edit Yearly Budget and Departments Budgets
	*	@param $budgetid integer
	*/
	public function edit_budget($budgetid	=	NULL)
	{
		$codes_data	=	array('sms_code'	=>	'','email_code'	=>	'');
		
		// Null SMS AND EMAIL Codes from total_table
		$this->company->update_codes($budgetid,$codes_data);
				
		if($this->input->post())
		{
			// Get all values from POST
			$data			=	$this->input->post();
			$year			=	date('Y',strtotime($data['budget_year']));
			
			// UNSET the value from ARRAY
			unset($data['submit']);

			if($data['budgetid'])
			{
				$codes_data	=	array('sms_code'	=>	'','email_code'	=>	'');
				
				$this->company->update_codes($data['budgetid'],$codes_data);
				
				$total_budget['addedby']		=	$data['addedby'];
				$total_budget['budget_year']	=	$data['budget_year'];
				$total_budget['budget_amount']	=	$data['budget_amount'];
				
				// EDIT Total Budget
				$this->company->edit_yearly_budget($budgetid,$total_budget);
				
				foreach($data['department_budget']	as $key	=>	$value)
				{
					$department_budget	=	array('dept_budget'	=>	$value);
					
					// Update data into database
					$this->company->edit_dept_budget($key,$department_budget);
				}
			}
			
			// Redirect to listing page
			redirect(base_url().'company/department_budgets');
			exit();
		}
		else
		{
			$budget		=	$this->company->get_single_budget($budgetid);

			$this->_data['budgetid']		=	isset($budget->budgetid) ? $budget->budgetid : NULL;
			$this->_data['budget_amount']	=	isset($budget->budget_amount) ? $budget->budget_amount : NULL;
			$this->_data['budget_year']		=	isset($budget->budget_year) ? $budget->budget_year : NULL;
			
			$this->_data['budget']			=	$budget;
			$this->_data['year'] 			= 	$this->company->get_budget_maxyear();
			$this->_data['d_budget']		=	$this->company->get_d_budget_by_tb_id($budgetid);

			// Load Users Listing Page
			$this->load->view('edit-department-budgets',$this->_data);
		}
	}
//-----------------------------------------------------------------------
	/*
	*	Delete Customer
	*/		
	public function department_budgets()
	{
		// Load Users Listing Page
		$this->load->view('department-budgets',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*	Delete Customer
	*/		
	public function departmentbudgetlist()
	{

		$departments_budget	=	$this->company->all_budgets();
		
		$permissions	=	$this->haya_model->check_other_permission(array('157'));
		
		foreach($departments_budget as $inq)
        {   
			$departments_sum	=	$this->company->dept_budget_sum($inq->budgetid);
			
			$remaining			=	($inq->budget_amount	-	$departments_sum);
			
			$action  = '<a <a href="'.base_url().'company/department_categories/'.$inq->budgetid.'"><i class="myicon icon-gear"></i></a>';
			$action .= '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'company/budget_detail/'.$inq->budgetid.'"><i class="icon-eye-open"></i></a>';
			$action	.= '&nbsp;<a href="#globalDiag" onClick="alatadad(this);" data-url="'.base_url().'company/email_form/'.$inq->budgetid.'"><i style="color:#CC0000;" class="icon-envelope"></i></a>';

			if($permissions[157]['u']	==	1)
			{
				$action .= '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'company/edit_budget/'.$inq->budgetid.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			
			$arr[] = array(
				"DT_RowId"			=>	$inq->budgetid.'_durar_lm',				
                "سنة"				=>	arabic_date($inq->budget_year),
				"مجموع الميزانية"	=>	$inq->budget_amount, 
				"المتبقية"			=>	$remaining,        
                "الإجرائات"			=>	$action);
				
			unset($action);	
       }
	   
	   $ex['data'] = $arr;
       echo json_encode($ex);	
	}
//-----------------------------------------------------------------------
	/*
	* Projects Types and there Budgets
	* @param $country_id int
	*/
	
	public function department_categories($budgetid)
	{
		$this->_data['budgetid']				=	$budgetid;
		//$this->_data['departments_budgets']		=	$this->outside->get_department_budgets($budgetid);
			
		//$this->_data['project_budgets']			=	$this->outside->get_project_budget_by_country_id($budgetid);
			
		// Load Page
		$this->load->view('department-categories-listing',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*	Delete Customer
	*/	
	public function email_form($budgetid	=	NULL)
	{
		if($this->input->post())
		{
			$budget			=	$this->company->get_single_budget($budgetid);
			$budget_amount	=	isset($budget->budget_amount) ? $budget->budget_amount : NULL;
		
			$data				=	$this->input->post();
			$email_addresses	=	$data['email'];
			
				
			$html	=	'<table class="table table-bordered table-striped dataTable">';
			
        	$html	.=	'<tbody>';
        	$html	.=	'<thead>';
            $html	.=	'<th><h4>قسم الميزانية</h4></th>';
            $html	.=	'<th><h4>ميزانية</h4></th>';
            $html	.=	'<th><h4>مكلفة</h4></th>';
       		$html	.=	'</thead>';
      $departments	=	$this->company->get_dept_budget($budgetid);
      $add_budget		=	0;
      
      foreach($departments as $dept):
      $add_budget +=	$dept->dept_budget;
          $html	.=	'<tr>';
          $html	.=	'<td>'.$this->haya_model->get_name_from_list($dept->department).'</td>';
            $html	.=	'<td>'.$dept->dept_budget.'</td>';
            $html	.=	'<td>00.00</td>';
          $html	.=	'</tr>';
      endforeach;
         $html	.=	'<tr>';
            $html	.=	'<td><label class="text-warning">مجموع</label></td>';
            $html	.=	'<td><strong>'.number_format($add_budget,2).'</strong></td>';
            $html	.=	'<td></td>';
          $html	.=	'</tr>';
          $html	.=	'<tr>';
            $html	.=	'<td><label class="text-warning">المتبقية</label></td>';
            $html	.=	'<td>';
         $remaining	=	($budget_amount-$add_budget);
             $html	.=	'<strong>'.number_format($remaining,2).'</strong>';
             $html	.=	'</td>';
             $html	.=	'<td></td>';
           $html	.=	'</tr>';
         $html	.=	'</tbody>';
       $html	.=	'</table>';
	   
	   	// Send email to all companies who realted to this Quote.
		$this->send_email($email_addresses,'No-Reply@alhaya.com','الإدارات الميزانية ('.$budget->budget_year.')',$html,$path	=	NULL);
		
		redirect(base_url().'company/department_budgets');
		exit();
	   
		}
		else
		{
			$this->load->view('email-form',$this->_data);
		}
	}
	
//-----------------------------------------------------------------------
	/*
	*	Delete Customer
	*/	
	public function budget_detail($budgetid)
	{
		$budget		=	$this->company->get_single_budget($budgetid);
		

		$this->_data['budgetid']		=	isset($budget->budgetid) ? $budget->budgetid : NULL;
		$this->_data['budget_amount']	=	isset($budget->budget_amount) ? $budget->budget_amount : NULL;
		$this->_data['budget_year']		=	isset($budget->budget_year) ? $budget->budget_year : NULL;
		
		$this->_data['budget']			=	$budget;
			
		$this->load->view('budget-detail',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*	Delete Customer
	*/	
	public function delete_customer($customer_id)
	{
		$this->company->delete_customer($customer_id);
		
		redirect(base_url().'company/customers');
		exit();
	}
//-----------------------------------------------------------------------
	/*
	*	Delete Customer
	*/	
	public function delete_quotation($quoteid)
	{
		$this->company->delete_quotation($quoteid);
		
		redirect(base_url().'company');
		exit();
	}
	
//-----------------------------------------------------------------------
	/*
	*	Delete Customer
	*/	
	public function delete_company_doc($documentid)
	{
		$this->company->delete_company_doc($documentid);
		
		//redirect(base_url().'company/company_list');
		//exit();
		
		echo 1;
	}

//-------------------------------------------------------------------------

	/*
	* Add Company Quotation FORM
	* Add Company Quotation DATA INTO DATABSE
	*/	
	public function quotation($quoteid=0)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			unset($data['dept_id_text']);
			
			if($data['quoteid'])
			{
				if($_FILES["quote_attachment"]["tmp_name"])
				{
					$quote_attachment	=	$this->upload_file($this->_login_userid,'quote_attachment','resources/users');
					
					$data['quote_attachment']	=	$quote_attachment;
				}
				else
				{
					$data['quote_attachment']	=	$data['quote_attachment_old'];
				}
				
				unset($data['quote_attachment_old']);
				
				// UPDATE the record
				$this->company->update_quote($data['quoteid'],$data);
			}
			else
			{
				if($_FILES["quote_attachment"]["tmp_name"])
				{
					$quote_attachment	=	$this->upload_file($this->_login_userid,'quote_attachment','resources/users');
					
					$data['quote_attachment']	=	$quote_attachment;
				}
				
				unset($data['quote_attachment_old']);
				
				// ADD record
				$this->company->add_quote($data);
				
				$compay_emails	=	$this->company->get_companies_email($data['dept_id']);

				if(!empty($compay_emails))
				{
					foreach($compay_emails as $email)
					{
						$path = './resources/users/'.$this->_login_userid.'/'.$data['quote_attachment'];
						
						$subject	 =	$data['quote_title'];
						
						$html = '<table style="width:100% !important;">';
						$html .= '<tr>';
						$html .= '<td><img style="width:100% !important;" src="http://www.oco.org.om/Images/Untitled-1.gif" /></td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td style="text-align:right; font-size:20px;"> مرحباً '. $email->contact_person.'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td style="text-align:right; font-size:20px;"><strong> اسم الشركة </strong>'. $email->arabic_name.'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td style="text-align:right; font-size:20px;"><strong> تاريخ الانتهاء </strong>'.arabic_date($data['expiry_date']).'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td style="text-align:right; font-size:20px;">'.$data['quote_detail'].'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
						$html .= '<td style="text-align:right; font-size:20px;"></td>';
						$html .= '</tr>';
						$html .= '</table>';
											
						// Send email to all companies who realted to this Quote.
						$this->send_email($email->email_address,'No-Reply@alhaya.com',$subject,$html,$path);
					
					unset($subject,$html,$path);
					
					}

				}
		  }
			
			redirect(base_url()."company");
			exit();
		}
		else
		{
			$this->_data['quoteid'] 	=	$quoteid;
			$this->_data['quote'] 		=	$this->company->get_quotation_by_id($quoteid);
			
			$this->load->view('new_quotation',$this->_data);
		}
	}
//-----------------------------------------------------------------------
	
	public function view_quotation_info($quoteid)
	{
		$this->_data['quoteid'] 	=	$quoteid;
		$this->_data['quote'] 		=	$this->company->get_quotation_by_id($quoteid);
		
		$this->load->view('view-quotation',$this->_data);
	}
	
	public function view_companies_quotations($quoteid)
	{
		$this->_data['quoteid'] 	=	$quoteid;
		$this->_data['quote'] 		=	$this->company->get_company_quote_info($quoteid);
		
		//echo '<pre>'; print_r($this->_data['quote']);exit();
		
		$this->load->view('companies-listview-by-quotation',$this->_data);
	}
	
//-----------------------------------------------------------------------
	
/*	public function quotation($companyid=0)
	{
		$this->_data['companyid'] = $companyid;
		$this->_data['company'] = $this->haya_model->ah_company($companyid);
		$this->load->view('quotation_view',$this->_data);
	}*/

//------------------------------------------------------------------------

	/*
	* Add Company Quotation DATA INTO DATABSE
	*/
	public function saveCompanyQuotation()
	{
		$data	=	$this->input->post();

		$company_quotation	=	array();
		$company_quotation['companyid']				=	$data['companyid'];
		$company_quotation['quoteid']				=	$data['quoteid'];
		$company_quotation['quote_discount_type']	=	$data['quote_discount_type'];
		$company_quotation['quote_status']			=	$data['quote_status'];


		$count = count($data['list_parent_id']);
		
		if($_FILES["quote_attachment"]["tmp_name"])
		{
			$quote_attachment	=	$this->upload_file($this->_login_userid,'quote_attachment','resources/users');
			
			$company_quotation['quote_attachment']	=	$quote_attachment;
		}
		else
		{
			//$company_quotation['quote_attachment']	=	$data['quote_attachment_old'];
		}
		
		unset($data['quote_attachment_old']);
				
		
		// Add Company Quotation
		$quoteid	=	$this->company->add_company_quotation($company_quotation);
		
		for($i=0;$i<$count;$i++)
		{
			$result	=	array(
				'quoteid'			=>	$quoteid,
				'list_parent_id'	=>	$data['list_parent_id'][$i],
				'list_child_id'		=>	$data['list_child_id'][$i],
				'item_qty'			=>	$data['item_qty'][$i],
				'item_price'		=>	$data['item_price'][$i]
			);

			// Add Company Quotation Items
			$this->company->add_quotation_item($result);
		}
		redirect(base_url().'company/company_quotation');
		exit();
	}
//--------------------------------------------------------------------------------

	/*
	* Company Quotation lISTING 
	*/		
	public function company_quotation($quoteid)
	{
		$this->_data['quoteid']	=	$quoteid;
		
		$this->load->view('company-quotation-list',$this->_data);
	}
//--------------------------------------------------------------------------------

	/*
	* Add Company Quotation form
	* @param $companyid	integer
	*/	
	public function add_company_quotation($companyid	=	NULL, $quoteselected_id	=	NULL)
	{
		$this->_data['companyid'] 			=	$companyid;
		$this->_data['quoteselected_id'] 	=	$quoteselected_id;
		
		// GET Company DATA BY ID
		$this->_data['company'] 	=	$this->haya_model->ah_company($companyid);
		
		if($quoteselected_id!='')
		{	$this->_data['quote'] 	=	$this->haya_model->getQuote($quoteselected_id);	}		
		
		// Load Add company quotation FORM
		$this->load->view('add-company-quotation',$this->_data);
	}
	
	public function savecategory()
	{
		$cat = $this->input->post('cat');
		$this->db->query("INSERT INTO ah_listmanagement SET `list_type`='item', `list_name`='".$cat."'");
		
	}
	
	public function savesubcategory()
	{
		$subcat = $this->input->post('subcat');
		$cat = $this->input->post('cat');
		$this->db->query("INSERT INTO ah_listmanagement SET `list_type`='item', `list_parent_id`='".$cat."', `list_name`='".$subcat."'");
		
	}	
//--------------------------------------------------------------------------------

	/*
	* Search Quotations
	*/	
	function get_quotation_search()
	{
		if (isset($_GET['term']))
		{
		  $string = strtolower($_GET['term']);
		  
		  $this->company->get_quotation_search($string);
		}
  }
//--------------------------------------------------------------------------------

	/*
	* Search Company Quotations
	*/	
	function search_company()
	{
		if (isset($_GET['term']))
		{
		  $string = strtolower($_GET['term']);
		  
		  $this->company->search_company($string);
		}
    }
//--------------------------------------------------------------------------------

	/*
	* GET Quotation Amount By ID
	*/	
	function get_quotation_amount()
	{
		$id	=	$this->input->post('id');
		
		$this->company->get_quotation_amount($id);
  }
//--------------------------------------------------------------------------------

	/*
	* All Quotations List
	*/	
	public function companyQuotationlist($quoteid	=	NULL)
	{
		$quotations	=	$this->company->get_company_quotations($quoteid);
		
		$permissions	=	$this->haya_model->check_other_permission(array('155'));
		
		foreach($quotations as $inq)
        {   
			$action = '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'company/company_quote_info/'.$inq->quoteid.'"><i class="icon-eye-open"></i></a>';
			
			if($permissions[155]['u']	==	1)
			{
				//$action .= '&nbsp;<a class="iconspace" href="#_"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
			else
			{
				$english	=	$inq->english_name;
				$arabic		=	$inq->arabic_name;
			}
			
			
			$arr[] = array(
				"DT_RowId"			=>	$inq->comp_quoteid.'_durar_lm',				
                "اقتباس العنوان"	=>	$inq->quote_title,
				"مبلغ الميزانية"	=>	$inq->budget_amount,
				"تاريخ الانتهاء"		=>	date("Y-m-d",strtotime($inq->expiry_date)),
                "الشركات العد"		=>	arabic_date($inq->tot_comp),
                "الإجرائات"			=>	$action);
				
		unset($action,$english,$arabic);
				
       }
	   
	   $ex['data'] = $arr;
       echo json_encode($ex);	
	}

//---------------------------------------------------------------------

	/*
	* All Quotations List
	*/
	public function company_quote_info($quoteid)
	{
		$this->_data['quote']	=	$quoteid;
		
		$this->_data['quote']	=	$this->company->get_company_quote_info($quoteid);
		
		$this->load->view('companies-listview-by-quotation',$this->_data);
	}

//-----------------------------------------------------------------------	
	public function item_box()
	{
		$this->_data['cat'] = $this->company->allCategory();
		$this->load->view('item_box',$this->_data);
	}
//-----------------------------------------------------------------------	
	public function child_item()
	{
		$parent_id = $this->input->post('id');
		$this->company->allSubCategory($parent_id);
	}
//-----------------------------------------------------------------------	
	public function saveQuotation()
	{
		
	}
//-----------------------------------------------------------------------

	/*
	*	Add SMS/EMAIL Codes into Total Budget Table
	*/	
	public	function add_codes()
	{

		$budgetid	=	$this->input->post('budgetid');
		
		$sms_code	=	$this->generateRandomString(10); // Genrate Code For SMS
		$email_code	=	$this->generateRandomString(10); // Genrate Code For EMAIL
		
		$data	=	array('sms_code'	=>	$sms_code,'email_code'	=>	$email_code);
		
		$this->company->update_codes($budgetid,$data);
		
		//Send EMAIL CODE BY EMAIL
		$this->send_email($email_addresses,'No-Reply@alhaya.com','Verification Code',$html,$path	=	NULL);
		
		//Send SMS code by SMS
	}
//-----------------------------------------------------------------------

	/*
	*	Match Codes 
	*/	
	public	function match_codes()
	{

		$sms_code	=	$this->input->post('sms_code');
		$email_code	=	$this->input->post('email_code');
		
		$sms_code_exist		=	$this->company->sms_code_exist($sms_code);

		if($sms_code_exist)
		{
			$sms_return	=	'1';
		}
		else
		{
			$sms_return	=	'0';
		}
		
		$email_code_exist	=	$this->company->email_code_exist($email_code);
		if($email_code_exist)
		{
			$email_return	=	'1';
		}
		else
		{
			$email_return	=	'0';
		}
		
		$data	=	array('SMS'	=>	$sms_return,'EMAIL'	=>	$email_return);
		
		echo json_encode($data);
	}	
	
//-----------------------------------------------------------------------

	/*
	*	Generate Random String
	*	@param $length integer
	*/	
	function generateRandomString($length = 10) 
	{
		$characters 		=	'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength	=	strlen($characters);
		$randomString 		=	'';
		
		for ($i = 0; $i < $length; $i++) 
		{
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		
		return $randomString;
	}
//-----------------------------------------------------------------------

	/*
	*	File Uploading
	*	@param $userid integer
	*	@param $filefield integer Input File name
	*	@param $folder integer older name where Image Upload
	*	@param $width integer
	*	@param $height integer
	*/
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
				
		if(!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Sending Email Function
	* @param $to string
	* @param $from string
	* @param $subject string
	* @param $message string
	* @param $path string
	*
	*/
	public function send_email($to	=	NULL,$from	=	NULL,$subject	=	NULL,$message = NULL,$path	=	NULL) 
	{
		$config = Array(
			'protocol' 	=>	'smtp',
			'smtp_host' =>	SMTPHOST,
			'smtp_port' =>	465,
			'smtp_user'	=>	SMTPEMAIL,
			'smtp_pass'	=>	SMTPPASSWORD,
			'mailtype'	=>	'html',
			'charset'	=>	'utf-8',
			'wordwrap' 	=>	TRUE
			);
		
		$this->load->library('email', $config);

		$this->email->clear(true);
		$this->email->set_newline("\r\n");
		$this->email->from(SMTPEMAIL,'Al-Haya');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		
		if($path)
		{
		 	$this->email->attach($path);
		}
		
		// Send Email
		$this->email->send();
	}
//-----------------------------------------------------------------------

	/*
	*	Download File
	*	@param $userid integer
	*	@param $file_name string
	*/
	
	public function download_file($userid,$file_name)
	{
		$path	= 'resources/users/'.$userid.'/'.$file_name;

		// Download File
		downloadFile($path,$file_name);
	}	
//-----------------------------------------------------------------------
	public function validateCompany()
	{
		$quoteid 	=	$this->input->post('quoteid');
		$companyid 	=	$this->input->post('companyid');
		$query 		=	$this->db->query("SELECT comp_quoteid FROM ah_quote_company WHERE `quoteid`='".$quoteid."' AND `companyid`='".$companyid."'");
		
		echo $query->num_rows();
	}
//------------------------------------------------------------------------

  	/**
   	* Dynamic Forms Listing Page
   	* @param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]  = $this->haya_model->get_all_custom_form($moduleid);
		
		$this->_data["userid"] = $this->_login_userid;
		
		$this->_data['formid']	=	$moduleid;
		 
		 // Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);	 
	 }
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/
	 public function muhasaba_types() 
	 {
		$this->_data["muhasaba_types"]  = $this->company->muhasaba_types_count();

		$this->load->view('muhasaba-types',$this->_data);	 
	 }
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/
	 public function muhasaba_type_listing($type_id	=	NULL,$branchid	=	NULL,$bankid	=	NULL,$application_Status	=	NULL,$country	=	NULL) 
	 {
		$this->load->model('inquiries/inquiries_model','inq');
		 
		$this->_data["muhasaba_type_listing"]  = $this->company->muhasaba_listing_by_type($type_id,$branchid,$bankid,$application_Status,$country);
		
		$this->_data['get_bank_branches']		=	$this->company->get_bank_branches($bankid);
		$this->_data['bankid']					=		$bankid;
	
		$this->_data['all_budgets']				=	$this->company->get_all_budgets();
		$this->_data['amount_in_alhaya_bank']	=	$this->company->amount_in_alhaya_bank($bankid);
		
		$this->_data['bankid']					=	$bankid;
		$this->_data['charity_type_id']			=	$type_id;
		
		 // Load Dynamic Forms Listing 
		$this->load->view('muhasaba-listing-by-type',$this->_data);	 
	 }
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/
	 public function muhasba_medical_applicants($type_id	=	NULL,$branchid	=	NULL,$bankid	=	NULL,$application_Status	=	NULL,$country	=	NULL) 
	 {
		$this->load->model('inquiries/inquiries_model','inq');
		 
		$this->_data["muhasaba_type_listing"]  = $this->company->muhasaba_listing_by_type($type_id,$branchid,$bankid,$application_Status,$country);
		
		$this->_data['get_bank_branches']		=	$this->company->get_bank_branches($bankid);
		$this->_data['bankid']					=	$bankid;
	
		$this->_data['all_budgets']				=	$this->company->get_all_budgets();
		$this->_data['amount_in_alhaya_bank']	=	$this->company->amount_in_alhaya_bank($bankid);
		
		$this->_data['bankid']					=	$bankid;
		$this->_data['charity_type_id']			=	$type_id;
		
		 // Load Dynamic Forms Listing 
		$this->load->view('muhasaba-medical-listing-by-type',$this->_data);	 
	 }
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/
	 public function muhasaba_applicants_listing($type_id	=	NULL,$branchid	=	NULL,$bankid	=	NULL,$application_Status	=	NULL,$country	=	NULL) 
	 {
		$this->load->model('inquiries/inquiries_model','inq');
		 
		$this->_data["muhasaba_type_listing"]  = $this->company->muhasaba_listing_by_type($type_id,$branchid,$bankid,$application_Status,$country);
		
		$this->_data['get_bank_branches']		=	$this->company->get_bank_branches($bankid);
		$this->_data['bankid']					=	$bankid;
	
		$this->_data['all_budgets']				=	$this->company->get_all_budgets();
		$this->_data['amount_in_alhaya_bank']	=	$this->company->amount_in_alhaya_bank($bankid);
		
		$this->_data['bankid']					=	$bankid;
		$this->_data['charity_type_id']			=	$type_id;
		
		 // Load Dynamic Forms Listing 
		$this->load->view('muhasaba-applicant-listing-by-type',$this->_data);	 
	 }
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/	 
	function loadcategories()
	{
		$vals	=	$this->input->post('vals');
		$type	=	$this->input->post('type');
		
		$message = array();
		
		if($vals	>	0)
		{
			$categories		=	$this->company->get_all_categories($type,$vals);
		}
		
		$html	=	'<div class="form-group col-md-4">'; 
        $html	.=	'<label for="basic-input"><strong> Categories : </strong></label>';
		$html	.=	'<select class="form-control" id="child'.$vals.'" name="child" >';
		$html	.= '<option value="" >-- SELECT --</option>';
		
	  	foreach($categories as $category)
	  	{
		  	$html	.= '<option value="'.$category->bds_id.'" >'.$category->bds_title.'</option>';
	  	}

        $html	.=	'</select>';
        $html	.=	'</div>';
		
		$ex['categories'] = $message;
		echo json_encode($html);
	}
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/	 
	function loadparentcategories()
	{
		if($_REQUEST){
		$id = $_REQUEST['parent_id'];
		
		$categories		=	$this->company->get_all_categories($type,$id);
		if(!empty($categories))
		{?>
            <select name="sub_category" class="parent">
            <option value="" selected="selected">-- Sub Category --</option>
            <?php
            foreach($categories as $category)
            {?>
                <option value="<?php echo $category->bds_id;?>" data-id="child"><?php echo $category->bds_title;?></option>
                <?php
            }?>
            </select>   
            <?php   
		}
		else
		{
			echo '<label style="padding:7px;float:left; font-size:12px;">No Record Found !</label>';}
		}
	}
/************************************************************/
	function get_child_categories_test()
	{
		$id = $_REQUEST['parent_id'];
		$type = $_REQUEST['type'];

		if($type	==	'parent')
		{
			$parent	=	"bds_categoryId='0' AND bd_id = ".$id."";
		}
		else
		{
			$parent	=	"bds_categoryId='".$id."'";
		}
		
		
		$query = $this->db->query("
		SELECT 
		bds_id,bd_id,bds_title 
		FROM 
		`ah_budget_sub` 
		WHERE delete_record='0'
		
		AND ".$parent.";");

	    if($query->num_rows() > 0)
		{
			$html = '<div class="col-md-4 form-group sub-categories">';
			$html .= '<select name="sub_category" class="parent form-control">';
			$html .= '<option value="" selected="selected">-- Select Package --</option>';
			
			foreach ($query->result() as $row)
			{
				$html .= '<option value="'.$row->bds_id.'" data-id="child">'.$row->bds_title.'</option>';
			}
	
			$html .= '</select>';
		    $html .= '</div>';
			echo $html;
		}
		else
		{
			$bank_ids		=	$this->company->get_package_accounts($id);
			$bank_accounts	=	$this->company->get_banks_account($bank_ids);
			
			$html = '<div class="col-md-4 form-group">';
			$html .= '<select name="bank_accounts" class="form-control">';
			$html .= '<option value="" selected="selected">-- Select Bank Account --</option>';
			
			$total_accounts	=	count($bank_accounts);
			
			if($total_accounts	>	0)
			{
				foreach ($bank_accounts as $acc)
				{
					$html .= '<option value="'.$acc->id.'">'.$acc->bank_name.' ( '.$acc->account_no.' )</option>';
				}
		
				$html .= '</select>';
				$html .= '</div>';
				echo $html;
			}
			else
			{
				echo '<label style="padding:7px;float:right; font-size:12px;">There is no Bank Accounts !</label>';
			}
		}
	
	}
/************************************************************/
	function get_child_categories()
	{
		$id = $_REQUEST['parent_id'];
		$type = $_REQUEST['type'];

		if($type	==	'parent')
		{
			$parent	=	"bds_categoryId='0' AND bd_id = ".$id."";
		}
		else
		{
			$parent	=	"bds_categoryId='".$id."'";
		}
		
		$query = $this->db->query("
		SELECT 
		bds_id,bd_id,bds_title,bds_totalAmount 
		FROM 
		`ah_budget_sub` 
		WHERE delete_record='0'
		
		AND ".$parent.";");

	    if($query->num_rows() > 0)
		{
			$html = '<select name="sub_category" class="parent form-control">';
			$html .= '<option value="" selected="selected">اختر الميزانية</option>';
			
			foreach ($query->result() as $row)
			{
				$html .= '<option value="'.$row->bds_id.'" data-id="child">'.$row->bds_title.'</option>';
			}
	
			$html .= '</select>';
			echo $html;
		}
		else
		{
			$query = $this->db->query("
			SELECT 
			bds_totalAmount 
			FROM 
			`ah_budget_sub` 
			WHERE delete_record='0'
			
			AND bds_id=".$id.";");
		
			$bank_ids		=	$this->company->get_package_accounts($id,$type);
			
			
			if($bank_ids)
			{
				$package_amount	=	$this->haya_model->get_total_package_amount($id);
				
				$bank_accounts	=	$this->company->get_banks_account($bank_ids);
				
				$html = '<select name="bank_accounts" class="bank-parent form-control" onchange="get_detail();">';
				$html .= '<option value="" selected="selected"> اختار رقم الحساب البنكي</option>';
				
				foreach ($bank_accounts as $acc)
				{
					$total_alhaya_local_amount	=	$this->haya_model->get_total_alhaya_local_amount($acc->id);
					
					$html .= '<option style="color:green !important;" value="'.$acc->id.'" account-balance="'.$acc->ammount.'" package-balance="'.$package_amount.'">'.$acc->bank_name.' ( '.$acc->account_no.' ) المبلغ في الحساب ( '.$total_alhaya_local_amount.' ) المزانية الحالية ('.$package_amount.' )</option>';
				}
		
				$html .= '</select>';
				echo $html;
			}
			else
			{
				echo '<label style="padding:7px;float:right; font-size:12px;">لا يوجد بنك مظاف في هذا القسم</label>';
			}
		}
	}
/************************************************************/
	function dynamic_category()
	{
		$this->_data['all_budgets']	=	$this->company->get_all_budgets();
		
		// Load Dynamic Forms Listing 
		$this->load->view('dynamic-drop-down',$this->_data);	
	}
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/
	 public function add_muhasaba_types_amount()
	 {
		$branchid	=	$this->_data['user_detail']['profile']->branchid;
		
		if($this->input->post())
		{
			$data		=	$this->input->post('data');
			
			foreach($data	as $key	=>	$value)
			{
				$muhasba_amounts	=	array(
								'user_id'			=>	$this->_login_userid,	
								'branch_id'			=>	$branchid,
								'bankid'			=>	$this->input->post('bankid'),
								'budget_id'			=>	$this->input->post('search_category'),
								'budget_cat_id'		=>	$this->input->post('sub_category'),
								'bank_account'		=>	$this->input->post('bank_accounts'),
								'applicant_id'		=>	$key,
								'payment_type'		=>	'CREDIT',
								'charity_type_id'	=>	$this->input->post('charity_type_id'),
								'amount'			=>	$value);
								
				$this->company->update_applicant($key);    // Update applicant paid type
				$this->haya_model->update_steps($key,5);  // Update Step
			 
				$this->company->add_muhasaba_types_amount($muhasba_amounts); // Add data into database

			}
			
				$alhaya_bank['login_user_id']		=		$this->_login_userid;
				$alhaya_bank['transaction_type']	=		'CREDIT';
				$alhaya_bank['charity_type_id']		=		$this->input->post('charity_type_id');
				$alhaya_bank['bank_id']				=		$this->input->post('bankid');
				$alhaya_bank['amount']				=		$this->input->post('total_appl_amount');
				$alhaya_bank['budget_id']			=		$this->input->post('search_category');
				$alhaya_bank['budget_cat_id']		=		$this->input->post('sub_category');
				$alhaya_bank['bank_account']		=		$this->input->post('bank_accounts');
				
				// Save Al-Haya Bank Managment transaction detail into database
				$this->outside->save_transaction_data($alhaya_bank); 
				
				// Save Al-Haya Bank Managment transaction detail into database
				$this->outside->save_local_transaction_data($alhaya_bank);
				
				redirect(base_url().'company/muhasaba_paid_applicants');
				exit();
		}
		else
		{
			// Load Dynamic Forms Listing 
			$this->load->view('muhasaba-listing-by-type',$this->_data);	
		}
	 }
//------------------------------------------------------------------------

  	/**
   	* Muhasaba Applicants
   	* 
   	*/
	public function muhasaba_paid_applicants($typeid	=	NULL,$branchid	=	NULL) 
	{
		$this->_data['typeid']		=	$typeid;
		$this->_data['branchid']	=	$branchid;
		
		$this->load->view('muhasba-paid-applicants',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	*	Get Muhasaba Applicants List
	*	@param $branchid int
	*	@param $charity_type int
	*/
	public function muhasaba_applicants_list($charity_type=0,$branchid=0)
	{
		$result	=	$this->company->muhasaba_applicants_list($branchid,$charity_type,'0'); // Give Money list
		
		foreach($result as $lc)
		{
			$decission = $this->getLastDecission($lc->applicantid);
			
			$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'company/stop_applicant/'.$lc->applicantid.'/idcard_number"><i class="myicon icon-cog"></i></a>';
			
			$fullname	=	'<a href="#" onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number">'.$lc->fullname.'</a>';
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم" 				=>	arabic_date($lc->applicantcode),
				"الإسم" 				=>	$fullname,
				"نوع المساعدات" 	=>	$lc->charity_type,
				"فرع" 				=>	$lc->branchname,
				"البطاقة الشخصة" 	=>	arabic_date($lc->idcard_number),					
				"القيمة" 			=>	arabic_date($decission->decission_amount),
				"تاريخ اللجنة" 		=>	arabic_date($decission->decissiontime),
				"الإجرائات" 			=>	$action);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	* 
	*/	
	public function getLastDecission($applicant_id)
	{
		// Load Models
		$this->load->model('inquiries/inquiries_model','inq');
		
		return $this->inq->get_last_decission($applicant_id);
	}
//-----------------------------------------------------------------------
	/*
	*	Add Project type Category
	*	@param $country_id	int
	*	@param $project_type_id	int
	*/	
	public function add_department_category($deptid	=	NULL,$project_type_id	=	NULL,$cat_id	=	NULL)
	{	
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($data['cat_id'])
			{
				$project_type_budget	=	array(
					'cat_name'			=>	$data['cat_name'],
					'cat_budget'		=>	$data['cat_budget'],
					'detail'			=>	$data['detail']);
				
				$response	=	$this->company->update_project_type_category($data['cat_id'],$project_type_budget);
				
				echo $url	=	base_url().'company/spending_projects_budgets/'.$data['country_budget_id'];
				
			}
			else
			{
				$project_type_budget	=	array(
					'deptid'			=>	$data['deptid'],
					'project_type_id'	=>	$data['project_type_id'],
					'user_id'			=>	$this->_login_userid,
					'cat_name'			=>	$data['cat_name'],
					'cat_budget'		=>	$data['cat_budget'],
					'detail'			=>	$data['detail']);
				
				$response	=	$this->company->add_project_type_category($project_type_budget);
				
				echo $url	=	base_url().'company/spending_projects_budgets/'.$data['country_budget_id'];
			}
		}
		else
		{
			if($cat_id)
			{
				$this->_data['cat_data']			=	$this->company->get_category_data($cat_id);
				$this->_data['deptid']				=	$this->_data['cat_data']->deptid;
				$this->_data['project_type_id']		=	$this->_data['cat_data']->department;
				$this->_data['project_type_budget']	=	$this->company->get_project_type_budget($this->_data['cat_data']->deptid,$this->_data['cat_data']->department);
				//$this->_data['spending_budget']		=	$this->outside->get_total_project_type_spending_budget($this->_data['cat_data']->deptid,$this->_data['cat_data']->project_type_id);
				$this->_data['spending_budget']		=	'0';

			}
			else
			{
				$this->_data['deptid']				=	$deptid;
				$this->_data['project_type_id']		=	$project_type_id;
				//$this->_data['project_type_budget']	=	$this->outside->get_project_type_budget($deptid,$project_type_id);
				//$this->_data['spending_budget']		=	$this->outside->get_total_project_type_spending_budget($deptid,$project_type_id);
				$this->_data['spending_budget']		=	'0';
				$this->_data['total_department_budget']	=	$this->company->get_department_budget($deptid,$project_type_id);
				$this->_data['assign_cat_budget']		=	$this->company->get_cat_budget($deptid,$project_type_id);
			}
			
			// Get SUM of all categories Budgets
			//$this->_data['defined_cat_budgets']		=	$this->company->sum_of_project_cat_budget();
			
			$this->load->view('add-department-category',$this->_data);
		}
	}
//-----------------------------------------------------------------------
	/*
	*	Add Project type Category
	*	@param $country_id	int
	*	@param $project_type_id	int
	*/	
	public function departments_categories_listing($depid,$project_type_id)
	{
		$this->_data['project_type_id']	=	$project_type_id;
		$this->_data['all_categories']	=	$this->company->get_department_categories($depid,$project_type_id);
		
		// load View
		$this->load->view('department-categories',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*	Spend Project Type Budget
	*	@param $country_id	int
	*	@param $project_type	int
	*/	
	public function single_cat_transactions($cat_id)
	{
		// Get SUM of all categories Budgets
		$this->_data['defined_cat_budgets']			=	$this->company->sum_of_project_cat_budget(); 
		$this->_data['spending_project_budgets']	=	$this->company->get_cat_spending_budget($cat_id);
		
		// Load view
		$this->load->view('cat-spending-amount-details',$this->_data);
	}
//-------------------------------------------------------------------------

//-----------------------------------------------------------------------
	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
    public function muhasaba_types_categories($type_id	=	NULL,$branchid	=	NULL)
    {
		$this->_data["muhasaba_types"]  	= 	$this->company->muhasaba_types_count();
		$this->_data["total"]  				= 	$this->company->get_banks_count($type_id);
		$this->_data["total_paid"]  		= 	$this->company->paid_appli_count($type_id,$branchid,'0');
		$this->_data["stop_total"]  		= 	$this->company->paid_appli_count($type_id,$branchid,'1');
		$this->_data['urgent_inside']		=	$this->company->urgent_inside_outside($type_id,'IN');
		$this->_data['urgent_outside']		=	$this->company->urgent_inside_outside($type_id,'OUT');
		$this->_data['total_medical_app']	=	$this->company->muhasaba_medical_applicant_count($type_id,$branchid);
		
		$this->_data['type_id']			=	$type_id;
		$this->_data['branchid']		=	$branchid;
		
		// Load Dynamic Forms Listing 
		$this->load->view('muhasaba-types-categories',$this->_data);	 
    }
//-----------------------------------------------------------------------
	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
    public function branches($typeid	=	NULL,$b	=	NULL,$c	=	NULL)
    {
		$this->_data['list_id']		=	$typeid;
		$this->_data["branches"]  = $this->company->get_all_branches($typeid);
		
		// Load Dynamic Forms Listing 
		$this->load->view('branches-listing',$this->_data);	 
    }
//-----------------------------------------------------------------------
	/*
	*	All Banks Listing
	*	@param $salaryid integer
	*/	
    public function banks_listing($typeid	=	NULL,$branchid	=	NULL)
    {
		
		
		$this->_data['type_id']		=	$typeid;
		$this->_data['branchid']	=	$branchid;
		
		$this->_data["all_banks"]	=	$this->company->get_all_banks($typeid);
		
		// Load Dynamic Forms Listing 
		$this->load->view('banks-listing',$this->_data);	 
    }
//-----------------------------------------------------------------------
	/*
	*	All Banks Listing
	*	@param $salaryid integer
	*/
	 public function stop_applicant($applicantid)
	 {
		 if($this->input->post())
		 {
			 $data['applicantid']	=	$this->input->post('applicantid');
			 $data['stop_payment']	=	'1';
			 
			$muhasba_amounts	=	array(
				'user_id'		=>	$this->_login_userid,
				'applicant_id'	=>	$this->input->post('applicantid'),
				'branch_id'		=>	$this->input->post('branchid'),
				'budget_id'		=>	$this->input->post('budget_id'),
				'budget_cat_id'	=>	$this->input->post('budget_cat_id'),
				'bank_account'	=>	$this->input->post('bank_accounts'),
				'payment_type'	=>	'DEBIT',
				'bankid'		=>	$this->input->post('bankid'),
				'bankbranchid'	=>	$this->input->post('bankbranchid'),
				'amount'		=>	$this->input->post('amount'));
								
			$this->company->add_muhasaba_types_amount($muhasba_amounts); // Add data into database
			
			$alhaya_bank['login_user_id']		=		$this->_login_userid;
			$alhaya_bank['transaction_type']	=		'DEBIT';
			$alhaya_bank['charity_type_id']		=		$this->input->post('charity_type_id');
			$alhaya_bank['bank_id']				=		$this->input->post('bankid');
			$alhaya_bank['amount']				=		$this->input->post('amount');
			$alhaya_bank['budget_id']			=		$this->input->post('budget_id');
			$alhaya_bank['budget_cat_id']		=		$this->input->post('budget_cat_id');
			$alhaya_bank['bank_account']		=		$this->input->post('bank_account');
			
			if($this->input->post('charity_type_id')	!=	'80')
			{
			
				// Save Al-Haya Bank Managment transaction detail into database
				$this->outside->save_transaction_data($alhaya_bank); 
			}
			
			// Save Al-Haya Bank Managment transaction detail into database
			$this->outside->save_local_transaction_data($alhaya_bank);
			 
			 // STOP Applicant Paymnt to proceed
			 $this->company->stop_applicant($this->input->post('applicantid'),$data);
			 
			  
			$this->load->library('user_agent');
			if ($this->agent->is_referral())
			{
				$url	=	 $this->agent->referrer();
			}
			
			redirect($url);
		 }
		 else
		 {
			 $this->_data['applicantid']		=	$applicantid;
	
			 $this->_data['applicant_detail']	=	$this->haya_model->getRequestInfo($applicantid);
			 $this->_data['decission'] 			=	$this->getLastDecission($applicantid);
			 $this->_data['last_transaction'] 	=	$this->company->get_last_transaction($applicantid,'CREDIT');

			 $budget_cat_id	=	$this->_data['last_transaction']->budget_cat_id;
			 
		  	 //GET bank account numbers from Al haya Virtual Bank Managemnt system
		 	 $this->_data['bank_account']	=	$this->company->get_bank_accounts_by_cat($budget_cat_id);

			 // Load Dynamic Forms Listing 
			 $this->load->view('stop-applicant',$this->_data);
		 }
	 }
//------------------------------------------------------------------------

  	/**
   	* Muhasaba Applicants
   	* 
   	*/
	public function stop_applicants_listing($typeid	=	NULL,$branchid	=	NULL) 
	{
		$this->_data['typeid']		=	$typeid;
		$this->_data['branchid']	=	$branchid;
		
		$this->load->view('stop-applicants',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	*	Get Muhasaba Applicants List
	*	@param $branchid int
	*	@param $charity_type int
	*/
	public function muhasaba_stop_applicants_list($charity_type=0,$branchid=0)
	{
		$result	=	$this->company->muhasaba_applicants_list($branchid,$charity_type,'1'); // Give Money list
		
		foreach($result as $lc)
		{
			$decission = $this->getLastDecission($lc->applicantid);
			
						$action = '<a href="#"  onclick="alatadad(this);" data-url="'.base_url().'company/issue_payment_form/'.$lc->applicantid.'/idcard_number"><i class="myicon icon-cog"></i></a>';
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->applicantid.'_durar_lm',
				"رقم" 				=>	arabic_date($lc->applicantcode),
				"الإسم" 				=>	'<a href="#" onclick="alatadad(this);" data-url="'.base_url().'inquiries/checkbefore/'.$lc->applicantid.'/idcard_number">'.$lc->fullname.'</a>',
				"نوع المساعدات" 	=>	$lc->charity_type,
				"فرع" 				=>	$lc->branchname,
				"البطاقة الشخصة" 	=>	arabic_date($lc->idcard_number),					
				"كمية" 				=>	arabic_date($decission->decission_amount),
				"تاريخ اللجنة" 		=>	arabic_date($decission->decissiontime),
				"الإجرائات" 			=>	$action);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-----------------------------------------------------------------------
	/*
	*	All Banks Listing
	*	@param $salaryid integer
	*	Need only $bankid for CREDIT & DEBIT
	*/
	public function get_debit_credit($bankid)
	{
		$debit	=	$this->company->get_amount_debit($bankid);
		$credit	=	$this->company->get_amount_credit($bankid);
		
		
		$get_total_amount	=	'';
	}
//-----------------------------------------------------------------------
	/*
	*	All Banks Listing
	*	@param $salaryid integer
	*/
	 public function issue_payment_form($applicantid)
	 {
		 if($this->input->post())
		 {
			 $data['applicantid']	=	$this->input->post('applicantid');
			 $data['stop_payment']	=	'0';
			 
			 $muhasba_amounts	=	array(
				'user_id'		=>	$this->_login_userid,
				'applicant_id'	=>	$this->input->post('applicantid'),
				'branch_id'		=>	$this->input->post('branchid'),
				'payment_type'	=>	'CREDIT',
				'bankid'		=>	$this->input->post('bankid'),
				'bankbranchid'	=>	$this->input->post('bankbranchid'),
				'budget_id'		=>	$this->input->post('budget_id'),
				'budget_cat_id'	=>	$this->input->post('budget_cat_id'),
				'bank_account'	=>	$this->input->post('bank_account'),
				'amount'		=>	$this->input->post('amount'));
								
			$this->company->add_muhasaba_types_amount($muhasba_amounts); // Add data into database
			
			 // ISSUE Applicant payment
			 $this->company->stop_applicant($this->input->post('applicantid'),$data);
			 
			$alhaya_bank['login_user_id']		=		$this->_login_userid;
			$alhaya_bank['transaction_type']	=		'CREDIT';
			$alhaya_bank['charity_type_id']		=		$this->input->post('charity_type_id');
			$alhaya_bank['bank_id']				=		$this->input->post('bankid');
			$alhaya_bank['amount']				=		$this->input->post('amount');
			$alhaya_bank['budget_id']			=		$this->input->post('budget_id');
			$alhaya_bank['budget_cat_id']		=		$this->input->post('budget_cat_id');
			$alhaya_bank['bank_account']		=		$this->input->post('bank_account');
			
			if($this->input->post('charity_type_id')	!=	'80')
			{
				// Save Al-Haya Bank Managment transaction detail into database
				$this->outside->save_transaction_data($alhaya_bank); 
			}
			
			// Save Al-Haya Bank Managment transaction detail into database
			$this->outside->save_local_transaction_data($alhaya_bank);
			 
			$this->load->library('user_agent');
			
			if ($this->agent->is_referral())
			{
				$url	=	 $this->agent->referrer();
			}
			
			redirect($url);
		 }
		 else
		 {
			 $this->_data['applicantid']		=	$applicantid;
			 $this->_data['applicant_detail']	=	$this->haya_model->getRequestInfo($applicantid);
			 $this->_data['decission'] 			=	$this->getLastDecission($applicantid);
			 $this->_data['last_transaction'] 	=	$this->company->get_last_transaction($applicantid,'DEBIT');
			 $budget_cat_id						=	$this->_data['last_transaction']->budget_cat_id;
			 
			 //GET bank account numbers from Al haya Virtual Bank Managemnt system
			  $this->_data['bank_account']	=	$this->company->get_bank_accounts_by_cat($budget_cat_id	);
			  
			 //Load Dynamic Forms Listing 
			 $this->load->view('issue-applicant-payment',$this->_data);
		 }
	 }
//-----------------------------------------------------------------------
	/*
	*	Proceed Amount for Applicant
	*	@param $applicantid integer
	*/
	 public function proceed_amount($applicantid)
	 {
		$this->_data["medical_applicant"]	=	$this->company->muhasaba_medical_applicant($applicantid);
		$this->_data['all_budgets']			=	$this->company->get_all_budgets();
		 
		$this->load->view('proceed-medical-applicant-amount',$this->_data); 
	 }
//-----------------------------------------------------------------------
	/*
	*	Proceed Amount for Applicant
	*	@param $applicantid integer
	*/
	 public function proceed_applicant_amount($applicantid)
	 {
		$this->_data["medical_applicant"]	=	$this->company->muhasaba_medical_applicant($applicantid);
		$this->_data['all_budgets']			=	$this->company->get_all_budgets();
		 
		$this->load->view('proceed-applicant-amount',$this->_data); 
	 }
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/
	 public function add_medical_muhasba_amount()
	 {
		if($this->input->post())
		{
			$muhasba_amounts	=	array(
							'user_id'			=>	$this->_login_userid,	
							'branch_id'			=>	$this->input->post('branchid'),
							'budget_id'			=>	$this->input->post('search_category'),
							'budget_cat_id'		=>	$this->input->post('sub_category'),
							'bank_account'		=>	$this->input->post('bank_accounts'),
							'applicant_id'		=>	$this->input->post('applicantid'),
							'payment_type'		=>	'CREDIT',
							'charity_type_id'	=>	$this->input->post('charity_type_id'),
							'amount'			=>	$this->input->post('amount'));
								
				$this->company->update_applicant($this->input->post('applicantid'));    // Update applicant paid type
				$this->haya_model->update_steps($this->input->post('applicantid'),5);  // Update Step
			 
				$this->company->add_muhasaba_types_amount($muhasba_amounts); // Add data into database

				$alhaya_local_bank['login_user_id']				=	$this->_login_userid;
				$alhaya_local_bank['transaction_type']			=	'CREDIT';
				$alhaya_local_bank['charity_type_id']			=	$this->input->post('charity_type_id');
				$alhaya_local_bank['applicantid']				=	$this->input->post('applicantid');
				$alhaya_local_bank['amount']					=	$this->input->post('amount');
				$alhaya_local_bank['budget_id']					=	$this->input->post('search_category');
				$alhaya_local_bank['budget_cat_id']				=	$this->input->post('sub_category');
				$alhaya_local_bank['bank_account']				=	$this->input->post('bank_accounts');
				$alhaya_local_bank['applicant_name']			=	$this->input->post('applicant_name');
				$alhaya_local_bank['medical_accessories']		=	$this->input->post('medical_accessories');
				$alhaya_local_bank['applicant_bank']			=	$this->input->post('applicant_bank');
				$alhaya_local_bank['applicant_account_number']	=	$this->input->post('applicant_account_number');
				
				if($this->input->post('charity_type_id')	==	'80')
				{
					$decission_detail['applicant_name']				=	$this->input->post('applicant_name');
					$decission_detail['medical_accessories']		=	$this->input->post('medical_accessories');
					$decission_detail['applicant_bank']				=	$this->input->post('applicant_bank');
					$decission_detail['applicant_account_number']	=	$this->input->post('applicant_account_number');
					
					// Update Step
					$this->company->update_decission_detail($this->input->post('decissionid'),$decission_detail);  
				}
				
				// Save Al-Haya Local Bank Managment transaction detail into database
				$this->outside->save_local_transaction_data($alhaya_local_bank);
				
				redirect(base_url().'company/muhasaba_paid_applicants');
				exit();
		}

	 }
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/
	 public function add_applicant_muhasba_amount()
	 {
		if($this->input->post())
		{
			$muhasba_amounts	=	array(
							'user_id'			=>	$this->_login_userid,	
							'branch_id'			=>	$this->input->post('branchid'),
							'budget_id'			=>	$this->input->post('search_category'),
							'budget_cat_id'		=>	$this->input->post('sub_category'),
							'bank_account'		=>	$this->input->post('bank_accounts'),
							'applicant_id'		=>	$this->input->post('applicantid'),
							'payment_type'		=>	'CREDIT',
							'charity_type_id'	=>	$this->input->post('charity_type_id'),
							'amount'			=>	$this->input->post('amount'));
								
				$this->company->update_applicant($this->input->post('applicantid'));    // Update applicant paid type
				$this->haya_model->update_steps($this->input->post('applicantid'),5);  // Update Step
			 
				$this->company->add_muhasaba_types_amount($muhasba_amounts); // Add data into database

				$alhaya_local_bank['login_user_id']				=	$this->_login_userid;
				$alhaya_local_bank['transaction_type']			=	'CREDIT';
				$alhaya_local_bank['charity_type_id']			=	$this->input->post('charity_type_id');
				$alhaya_local_bank['applicantid']				=	$this->input->post('applicantid');
				$alhaya_local_bank['amount']					=	$this->input->post('amount');
				$alhaya_local_bank['budget_id']					=	$this->input->post('search_category');
				$alhaya_local_bank['budget_cat_id']				=	$this->input->post('sub_category');
				$alhaya_local_bank['bank_account']				=	$this->input->post('bank_accounts');

				
				// Save Al-Haya Bank Managment transaction detail into database
				$this->outside->save_transaction_data($alhaya_bank); 
				
				// Save Al-Haya Local Bank Managment transaction detail into database
				$this->outside->save_local_transaction_data($alhaya_local_bank);
				
				redirect(base_url().'company/muhasaba_paid_applicants');
				exit();
		}

	 }
	//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/
	 public function muhasba_sakaniya_applicants($type_id	=	NULL,$branchid	=	NULL,$bankid	=	NULL,$application_Status	=	NULL,$country	=	NULL) 
	 {
		$this->load->model('inquiries/inquiries_model','inq');
		 
		$this->_data["muhasaba_type_listing"]  = $this->company->muhasaba_listing_by_type_sakaniya($type_id,$branchid,$bankid,$application_Status,$country);

		$this->_data['get_bank_branches']		=	$this->company->get_bank_branches($bankid);
		$this->_data['bankid']					=	$bankid;
	
		$this->_data['all_budgets']				=	$this->company->get_all_budgets();
		$this->_data['amount_in_alhaya_bank']	=	$this->company->amount_in_alhaya_bank($bankid);
		
		$this->_data['bankid']					=	$bankid;
		$this->_data['charity_type_id']			=	$type_id;
		
		 // Load Dynamic Forms Listing 
		$this->load->view('muhasaba-sakaniya-listing-by-type',$this->_data);	 
	 }
//-----------------------------------------------------------------------
	/*
	*	Proceed Amount for Applicant
	*	@param $applicantid integer
	*/
	 public function proceed_amount_sakaniya($applicantid)
	 {
		$this->_data["sakaniya_applicant"]	=	$this->company->muhasaba_sakaniya_applicant($applicantid);
		$this->_data['all_budgets']			=	$this->company->get_all_budgets();
		 
		$this->load->view('proceed-applicant-amount-sakaniya',$this->_data); 
	 }
//------------------------------------------------------------------------

  	/**
   	* 
   	* 
   	*/
	 public function add_sakaniya_muhasba_amount()
	 {
		if($this->input->post())
		{
			$muhasba_amounts	=	array(
							'user_id'			=>	$this->_login_userid,	
							'branch_id'			=>	$this->input->post('branchid'),
							'budget_id'			=>	$this->input->post('search_category'),
							'budget_cat_id'		=>	$this->input->post('sub_category'),
							'bank_account'		=>	$this->input->post('bank_accounts'),
							'applicant_id'		=>	$this->input->post('applicantid'),
							'payment_type'		=>	'CREDIT',
							'charity_type_id'	=>	$this->input->post('charity_type_id'),
							'amount'			=>	$this->input->post('amount'));
								
				$this->company->update_applicant($this->input->post('applicantid'));    // Update applicant paid type
				$this->haya_model->update_steps($this->input->post('applicantid'),5);  // Update Step
			 
				$this->company->add_muhasaba_types_amount($muhasba_amounts); // Add data into database

				$alhaya_local_bank['login_user_id']				=	$this->_login_userid;
				$alhaya_local_bank['transaction_type']			=	'CREDIT';
				$alhaya_local_bank['charity_type_id']			=	$this->input->post('charity_type_id');
				$alhaya_local_bank['applicantid']				=	$this->input->post('applicantid');
				$alhaya_local_bank['amount']					=	$this->input->post('amount');
				$alhaya_local_bank['budget_id']					=	$this->input->post('search_category');
				$alhaya_local_bank['budget_cat_id']				=	$this->input->post('sub_category');
				$alhaya_local_bank['bank_account']				=	$this->input->post('bank_accounts');
				$alhaya_local_bank['applicant_name']			=	$this->input->post('applicant_name');
				$alhaya_local_bank['medical_accessories']		=	$this->input->post('medical_accessories');
				$alhaya_local_bank['applicant_bank']			=	$this->input->post('applicant_bank');
				$alhaya_local_bank['applicant_account_number']	=	$this->input->post('applicant_account_number');
				
				if($this->input->post('charity_type_id')	==	'80')
				{
					$decission_detail['applicant_name']				=	$this->input->post('applicant_name');
					$decission_detail['medical_accessories']		=	$this->input->post('medical_accessories');
					$decission_detail['applicant_bank']				=	$this->input->post('applicant_bank');
					$decission_detail['applicant_account_number']	=	$this->input->post('applicant_account_number');
					
					// Update Step
					$this->company->update_decission_detail($this->input->post('decissionid'),$decission_detail);  
				}
				
				// Save Al-Haya Local Bank Managment transaction detail into database
				$this->outside->save_local_transaction_data($alhaya_local_bank);
				
				redirect(base_url().'company/muhasaba_paid_applicants');
				exit();
		}

	 }
//-----------------------------------------------------------------------
	/*
	*	
	*	
	*/	
    public function financial_affairs_os()
    {
		$this->_data['total']	=	$this->company->total_orphan_count();
		
		// Load Dynamic Forms Listing 
		$this->load->view('financial-affairs-orphan-sponsor-types',$this->_data);	 
    }
//-----------------------------------------------------------------------
	/*
	*	
	*	
	*/	
    public function issuing_salaries()
    {
		$this->_data['total_inside']	=	$this->company->total_orphan_count('INSIDE');
		$this->_data['total_out']		=	$this->company->total_orphan_count('OUTSIDE');
		
		// Load Dynamic Forms Listing 
		$this->load->view('issuing-salaries-types',$this->_data);	 
    }
//-----------------------------------------------------------------------
	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
    public function all_branches($status	=	NULL)
    {
		$this->_data['country_status']		=	$status;
		$this->_data["branches"]  			=	$this->company->get_all_branches_sakaniya($status);
		
		// Load Dynamic Forms Listing 
		$this->load->view('branches-listing-sakaniya',$this->_data);	 
    }
//-----------------------------------------------------------------------
	/*
	*	Add Schedule
	*	@param $salaryid integer
	*/	
    public function alkafala($country_status	=	NULL,$branchid	=	NULL)
    {
		$this->_data['country_status']		=	$country_status;
		$this->_data["branchid"]  			=	$branchid;
		
		// Load Dynamic Forms Listing 
		$this->load->view('alkafala-types',$this->_data);	 
    }
//-----------------------------------------------------------------------
	/*
	*	All Banks Listing
	*	@param $salaryid integer
	*/	
    public function alkafala_banks($orphan_status	=	NULL,$country_status	=	NULL,$branchid	=	NULL)
    {
		$this->_data['orphan_status']	=	$orphan_status;
		$this->_data['country_status']	=	$country_status;
		$this->_data['branchid']		=	$branchid;
		
		//$this->_data["all_banks"]	=	$this->company->get_all_banks($typeid);
		$this->_data['all_banks']	=	$this->company->get_all_lists_by_type('bank',$orphan_status,$country_status,$branchid);
		
		//echo '<pre>';print_r($this->_data['all_banks']);exit();
		
		// Load Dynamic Forms Listing 
		$this->load->view('banks-from-listmanagement',$this->_data);	 
    }
//-----------------------------------------------------------------------
	/*
	*	All Banks Listing
	*	@param $salaryid integer
	*/	
    public function catch_receipt_form()
    {
		// Load Dynamic Forms Listing 
		$this->load->view('catch-receipt-form',$this->_data);	 
    }
//------------------------------------------------------------------------
}
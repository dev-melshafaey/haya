<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Company_model extends CI_Model {

	/*
	* Constructor
	*
	*/
	function __construct()
    {
        parent::__construct();
		$this->_login_userid = $this->session->userdata('userid');
    }
//-------------------------------------------------------------------------------	
	/*
	* 
	*
	*/
	function company_detail($id=0,$crnumber=0)
	{
		if($id!=0 || $crnumber!=0)
		{
			$this->db->from('ah_company');
			
			if($id!='' && $id!=0)
			{
				$this->db->where('companyid',$id);
			}
			if($crnumber!='' && $crnumber!=0)
			{
				$this->db->where('cr_number',$crnumber);
			}
			
			$query = $this->db->get();
			return $query->row(); 
		}
	}
//-------------------------------------------------------------------------------	
	/*
	* 
	*
	*/	
	public function get_company_info($companyid)
	{
		$this->db->select('ah_company.companyid, ah_company.english_name, ah_company.arabic_name, ah_company_document.documentdate, ah_company_document.documentid, ah_company_document.documenttitle, ah_company_document.document');
		$this->db->from('ah_company');
		$this->db->join('ah_company_document',"ah_company_document.companyid = ah_company.companyid",'left');		
		$this->db->where('ah_company.companyid',$companyid);
		$this->db->where('ah_company.delete_record','0');
		$query = $this->db->get();
		
		return $query->row();
	}
//-------------------------------------------------------------------------------	
	/*
	* 
	*
	*/	
	public function get_company_documentlist($companyid)
	{
		$this->db->select('*');
		$this->db->from('ah_company_document');		
		$this->db->where('companyid',$companyid);
		$this->db->where('delete_record','0');
		$query = $this->db->get();
		
		return $query->result();
	}	
//-------------------------------------------------------------------------------	
	/*
	* 
	*
	*/	
	function getCompanyName($like,$searchcolumn)
	{
		$this->db->select('companyid,english_name,arabic_name,cr_number');
		$this->db->from('ah_company');
		$this->db->like($searchcolumn,$like);
		$query = $this->db->get();
		return $query->result();
	}
	
//-------------------------------------------------------------------
	/*
	 * Get Documents Detail BY Docid
	 * Return OBJECT
	 */
		
	function get_all_customers()
	{
		$this->db->select('customer_id,userid,customer_name,company_name,customer_title,address,postal_code,province,wilaya,delete_record');
		$query = $this->db->get('ah_customers');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Documents Detail BY Docid
	 * Return OBJECT
	 */
		
	function get_single_customer($customer_id)
	{
		$this->db->where('customer_id',$customer_id);		
		$query = $this->db->get('ah_customers');
				
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}	
//----------------------------------------------------------------------

	/*
	* Get all Data 
	* return OBJECT
	*/

	function update_customer($customer_id,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('customer_id',$customer_id);
		$this->db->update('ah_customers',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
	
//-------------------------------------------------------------------
	/*
	* Add List 
	* @param $data ARRAY
	* return TRUE
	*/

	function add_customer($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_customers',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Delete Branch
	 * @param $branchid integer
	 * Return TRUE
	 */
		
	function delete_customer($customer_id)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','customer_id'	=>	$customer_id));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('customer_id',$customer_id);
		$this->db->update('ah_customers',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	* Add List Document
	* @param $data ARRAY
	* return TRUE
	*/

	function add_document($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_company_document',$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	 * Delete Company Document
	 * @param $branchid integer
	 * Return TRUE
	 */
		
	function delete_company_doc($documentid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','documentid'	=>	$documentid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('documentid',$documentid);

		$this->db->update('ah_company_document',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function allCategory($parent_id=0)
	{
		$this->db->select('list_id,list_name');
		$this->db->where('list_type','item');
		$this->db->where('list_parent_id',0);
		$query = $this->db->get('ah_listmanagement');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/	
	function allSubCategory($parent_id=0)
	{
		$this->db->select('list_id,list_name');
		$this->db->where('list_type','item');
		$this->db->where('list_parent_id',$parent_id);
		$query = $this->db->get('ah_listmanagement');
		$html = '';
		
		foreach($query->result() as $as)
		{
			$html .= '<option value="'.$as->list_id.'">'.$as->list_name.'</option>';	
		}
		
		echo $html;
	}
//----------------------------------------------------------------------

	/*
	* GET All Quotation Records
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_all_quotations()
	{
		$this->db->select('quoteid,addedby,branch_id,dept_id,quote_title,quote_detail,quote_attachment,quote_status,quote_data,budget_amount,expiry_date,delete_record');
		$this->db->where('delete_record','0');
		
		$query = $this->db->get('ah_quote');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* GET Single Quotation Record
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_quotation_by_id($quoteid)
	{
		$this->db->select('quoteid,addedby,branch_id,dept_id,quote_title,quote_detail,quote_attachment,quote_status,quote_data,expiry_date,budget_amount,delete_record');
		$this->db->where('delete_record','0');
		$this->db->where('quoteid',$quoteid);
		
		$query = $this->db->get('ah_quote');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------

	/*
	* UPDATE Quotation
	* return OBJECT
	*/

	function update_quote($quoteid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('quoteid',$quoteid);
		$this->db->update('ah_quote',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Add Quotation 
	* @param $data ARRAY
	* return TRUE
	*/

	function add_quote($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_quote',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Delete Quotation
	 * @param $quoteid integer
	 * Return TRUE
	 */
		
	function delete_quotation($quoteid)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','quoteid'	=>	$quoteid));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('quoteid',$quoteid);

		$this->db->update('ah_quote',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	* Add Company Quotation 
	* @param $data ARRAY
	* return last Inserted Record ID
	*/

	function add_company_quotation($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_quote_company',$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	* Add Quotation Item 
	* @param $data ARRAY
	* return TRUE
	*/

	function add_quotation_item($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('ah_quote_items',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Search Quotations
	* @param $string string
	* return JSON
	*/
	function get_quotation_search($string)
	{
		$this->db->select('quoteid,quote_title,budget_amount');
		$this->db->like('quote_title', $string);
		$this->db->or_like('quoteid', $string);

		$query = $this->db->get('ah_quote');
		
		if($query->num_rows > 0)
		{
		  foreach ($query->result_array() as $row)
		  {
			$new_row['lable']	=	$row['quote_title'];
			$new_row['value']	=	$row['quote_title'];
			$new_row['id']		=	$row['quoteid'];
			$new_row['amount']	=	$row['budget_amount'];

			$row_set[] = $new_row; //build an array
		  }
		  
		  echo json_encode($row_set); //format the array into json data
		}
  	}

//-------------------------------------------------------------------
	/*
	* Search Company Quotations
	* @param $string string
	* return JSON
	*/	
	public function search_company($string)
	{
		$this->db->select('companyid,english_name,arabic_name,cr_number');
		$this->db->like('english_name', $string);
		$this->db->or_like('arabic_name', $string);
		$this->db->or_like('companyid', $string);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get('ah_company');
		
		if($query->num_rows > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$new_row['lable']	=	$row['english_name'].'|'.$row['arabic_name'].'('.$row['cr_number'].')';
				$new_row['value']	=	$row['english_name'].'|'.$row['arabic_name'].'('.$row['cr_number'].')';
				$new_row['id']		=	$row['companyid'];
				
				$row_set[] = $new_row; //build an array
			 }
			  
			 echo json_encode($row_set); // Convert ARRAY into Jason Format
		}
	}
//----------------------------------------------------------------------

	/*
	* GET Single Quotation Record
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_quotation_amount($quoteid)
	{
		$this->db->select('budget_amount');
		$this->db->where('quoteid',$quoteid);
		
		$query = $this->db->get('ah_quote');
		
		if($query->num_rows() > 0)
		{
			echo $query->row()->budget_amount;
		}
	}
//----------------------------------------------------------------------

	/*
	* GET All Quotation Records
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_company_quotations($quoteid)
	{
		$coloumn	=	NULL;
		
		if($quoteid)
		{
			$string		=	'WHERE ah_quote.quoteid='.$quoteid.'';
			$coloumn	=	', COUNT(ah_quote_company.companyid) AS tot_comp';
		}
			
		$query = $this->db->query("
		SELECT
			`ah_quote_company`.`comp_quoteid`
		   ,`ah_quote`.`quoteid`
			,`ah_quote`.`quote_title`
			, `ah_quote`.`budget_amount`
			, `ah_quote`.`expiry_date`
			, COUNT(`ah_quote_company`.`companyid`) AS tot_comp
		FROM
			`ah_quote`
		INNER JOIN 
			`ah_quote_company` 
		ON (`ah_quote`.`quoteid` = `ah_quote_company`.`quoteid`) ".$string." GROUP BY `ah_quote`.`quoteid`;");
			
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* GET All Quotation Records
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_companies_by_quotation($quoteid)
	{
		$query = $this->db->query("
			SELECT
				`ah_company`.`cr_number`
				, `ah_company`.`english_name`
				, `ah_company`.`arabic_name`
				, `ah_company`.`companyid`
				, `ah_quote_company`.`quote_data`
				, `ah_quote_company`.`quote_title`
				, `ah_quote_1`.`addedby`
				, `ah_quote_1`.`quote_title`
				, `ah_quote_1`.`budget_amount`
				, `ah_quote_1`.`quoteid`
				, `ah_quote_company`.`comp_quoteid`
			FROM
				`ah_quote`, 
				`ah_company`
			INNER JOIN `ah_quote_company` 
				ON (`ah_company`.`companyid` = `ah_quote_company`.`companyid`)
			INNER JOIN `ah_quote` AS `ah_quote_1` 
				ON (`ah_quote_1`.`quoteid` = `ah_quote_company`.`quoteid`) WHERE ah_quote_company.quoteid=".$quoteid." GROUP BY `ah_company`.`companyid`;");
				
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
	}
		
//----------------------------------------------------------------------

	/*
	* GET All Company Quotation and Item Detail Records
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_company_quote_info($quoteid)
	{
		$query_a = $this->db->query("
			SELECT
				`ah_company`.`cr_number`
				, `ah_company`.`english_name`
				, `ah_company`.`arabic_name`
				, `ah_company`.`companyid`
				, `ah_quote_company`.`quote_data`
				, `ah_quote_company`.`quote_title`
				, `ah_quote_1`.`addedby`
				, `ah_quote_1`.`quote_title`
				, `ah_quote_1`.`budget_amount`
				, `ah_quote_1`.`quoteid`
				, `ah_quote_company`.`comp_quoteid`
			FROM
				`ah_quote`, 
				`ah_company`
			INNER JOIN `ah_quote_company` 
				ON (`ah_company`.`companyid` = `ah_quote_company`.`companyid`)
			INNER JOIN `ah_quote` AS `ah_quote_1` 
				ON (`ah_quote_1`.`quoteid` = `ah_quote_company`.`quoteid`) WHERE ah_quote_company.quoteid=".$quoteid." GROUP BY `ah_company`.`companyid`;");
				
			
		if($query_a->num_rows() > 0)
		{
			 $data['company_data']	=	$query_a->row();
		}
			
		$query_b = $this->db->query("SELECT * FROM ah_quote_items WHERE quoteid=".$query_a->row()->comp_quoteid.";");
			
		if($query_b->num_rows() > 0)
		{
			 $data['items']	= $query_b->result();
		}
		
		return $data;
	}
//----------------------------------------------------------------------

	/*
	* GET All Budget
	* return OBJECT
	*/	
	function all_budgets()
	{
		$this->db->select('budgetid,budget_amount,budget_year,delete_record');

		$query = $this->db->get('total_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	* Add Total Budget
	* @param $data ARRAY
	* return TRUE
	*/

	function add_budget($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('total_budget',$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	* Add Department Budget
	* @param $data ARRAY
	* return TRUE
	*/

	function add_dept_budget($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('departments_budget',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* GET Single Budget BY ID
	* return OBJECT
	*/	
	function get_single_budget($budgetid)
	{
		$this->db->select('budgetid,budget_amount,budget_year,delete_record');
		$this->db->where('budgetid',$budgetid);

		$query = $this->db->get('total_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------

	/*
	* GET Department Budget by Yearly budget ID
	* return OBJECT
	*/	
	function get_d_budget_by_tb_id($budgetid)
	{
		$this->db->select('deptid,department,dept_budget');
		$this->db->where('budgetid',$budgetid);

		$query = $this->db->get('departments_budget');
		
		$deartment_values	=	array();
		
		if($query->num_rows() > 0)
		{
			$result	=	$query->result();

			foreach($result	as	$value)
			{
				$deartment_values[$value->department]	=	$value->deptid.'_'.$value->dept_budget;
			}

			return $deartment_values;
		}
	}
//----------------------------------------------------------------------	
	function get_budget_maxyear()
	{
		$query = $this->db->query('SELECT MAX(budget_year)+1 AS mybug FROM total_budget');
		return $query->row()->mybug;
	}
//----------------------------------------------------------------------

	/*
	* GET Department Budget BY ID
	* return OBJECT
	*/	
	function get_dept_budget($budgetid)
	{
		$this->db->select('deptid,budgetid,department,dept_budget');
		$this->db->where('budgetid',$budgetid);

		$query = $this->db->get('departments_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* GET Department Budget BY ID
	* return OBJECT
	*/	
	function dept_budget_sum($budgetid)
	{
		$query = $this->db->query("SELECT SUM(dept_budget) AS total FROM `departments_budget` WHERE budgetid='".$budgetid."'");
		
		if($query->num_rows() > 0)
		{
			return $query->row()->total;
		}
	}
	
//----------------------------------------------------------------------

	/*
	* GET Single Quotation Record
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_companies_email($dept_id)
	{
		$this->db->select('companyid');
		$this->db->where('departmentid',$dept_id);
		
		$this->db->group_by("companyid"); 
		
		$query = $this->db->get('ah_company_category');
		
		if($query->num_rows() > 0)
		{
			$result	=	 $query->result();

			foreach($result as $res)
			{
				if($this->get_email($res->companyid))
				{
					$data[]	=	$this->get_email($res->companyid);
				}
			}
			return $data;
		}
	}
	
//----------------------------------------------------------------------

	/*
	* GET Single Quotation Record
	* @param $quoteid integer
	* return OBJECT
	*/	
	function get_email($companyid)
	{
		$this->db->select('companyid,branchid,email_address,arabic_name,contact_person');
		$this->db->where('companyid',$companyid);
			
		$query = $this->db->get('ah_company');
		
		if($query->num_rows() > 0)
		{
			return $result	=	 $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* GET Single Budget BY ID
	* return OBJECT
	*/
	
	function check_yearly_budget($year)
	{
		$this->db->where('YEAR(budget_year)',$year);

		$query = $this->db->get('total_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	* Edit Total Budget
	* @param $data ARRAY
	* return TRUE
	*/

	function edit_yearly_budget($budgetid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('budgetid',$budgetid);
		$this->db->update('total_budget',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Edit Department Budget
	* @param $data ARRAY
	* return TRUE
	*/

	function edit_dept_budget($deptid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('deptid',$deptid);
		$this->db->update('departments_budget',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Update SMS/EMAIL Codes
	* @param $budgetid integer
	* @param $data ARRAY
	* return TRUE
	*/

	function update_codes($budgetid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('budgetid',$budgetid);
		$this->db->update('total_budget',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* Check If SMS CODE EXIST
	* return OBJECT
	*/	
	function sms_code_exist($sms_code)
	{
		$this->db->where('sms_code',$sms_code);

		$query = $this->db->get('total_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->sms_code;
		}
	}
//----------------------------------------------------------------------

	/*
	* Check If EMAIL CODE EXIST
	* return OBJECT
	*/	
	function email_code_exist($email_code)
	{
		$this->db->where('email_code',$email_code);

		$query = $this->db->get('total_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->email_code;
		}
	}
//------------------------------------------------------

	/*
	* 
	* 
	*/
	public function muhasaba_types_count()
	{
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='charity_type' AND list_status='1' AND list_name!='اضافة اليتيم'  AND list_name!='اضافة الكفيل' AND delete_record='0'");
		$result_1	=	$query_1->result();
		
		$almasadaat	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query_2 	=	$this->db->query("SELECT COUNT(applicantid) as total FROM `ah_applicant` WHERE charity_type_id='".$type->list_id."' AND isdelete='0' AND application_status != 'رفض' AND application_status != '' AND step='4' AND paid='no'");
				$result_2	=	$query_2->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		
		return $data;
	}
//-------------------------------------------------------------	
	/*
	*
	*
	*/
	function muhasaba_listing_by_type($charity_type,$branchid,$bankid,$applicantion_status,$country)
	{
		if($applicantion_status	==	'urgent')
		{
			$status	=	'1';
		}
		else
		{
			$status	=	'0';
		}
		
		if($country	==	'IN')
		{
			$countryid	=	'200';
		}
		else
		{
			$countryid	=	'!=200';
		}
		
		
		$this->db->select('ah_applicant.applicantid,ah_applicant.bankid,ah_applicant.bankbranchid,ah_applicant.accountnumber,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		$this->db->where("ah_applicant.charity_type_id",$charity_type);
		
		if($country)
		{
			$this->db->where("ah_applicant.country",$countryid);
		}

		$this->db->where("ah_applicant.isdelete",'0');
		
		if($bankid)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		
		if($bankid)
		{
			$this->db->where("ah_applicant.bankid",$bankid);
		}
		
		$this->db->where("ah_applicant.application_status != ",'رفض');
		$this->db->where("ah_applicant.application_status != ",'');
		$this->db->where("ah_applicant.step",'4');
		$this->db->where("ah_applicant.paid",'no');
		$this->db->where("ah_applicant.urgent_applicant",$status);			
		$this->db->order_by('ah_applicant.registrationdate','DESC');
					
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	* Add List 
	* @param $data ARRAY
	* return TRUE
	*/

	function add_muhasaba_types_amount($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('muhasaba_types_amounts',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------------*/
	/*
	*
	*
	*/
	public function update_applicant($applicantid)
	{
		$data = array('paid'=>'yes');

		$this->db->where('applicantid', $applicantid);

		$this->db->update('ah_applicant',json_encode($data),$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------	
	/*
	*
	*
	*/
	function muhasaba_applicants_list($branchid,$charity_type,$stop_payment)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		if($branchid	!=	0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		
		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step",'5');
		$this->db->where("ah_applicant.paid",'yes');
		$this->db->where("ah_applicant.stop_payment",$stop_payment);
		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
					
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* GET Single Department Budget BY ID
	* return OBJECT
	*/	
	function get_single_dept_budget($budgetid,$type_id)
	{
		$this->db->select('deptid,budgetid,department,dept_budget');
		$this->db->where('budgetid',$budgetid);
		$this->db->where('department',$type_id);

		$query = $this->db->get('departments_budget');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	* Add Department category
	* @param $data ARRAY
	* return TRUE
	*/

	function update_project_type_category($cat_id,$data)
    {
		$json_data	=	json_encode(array('record'	=>	'delete','cat_id'	=>	$cat_id));
		
		$this->db->where('cat_id',$cat_id);
		$this->db->update('department_categories',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Add Project Type category
	* @param $data ARRAY
	* return TRUE
	*/

	function add_project_type_category($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('department_categories',$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	* Get Project Type Category Budget
	* @param $cat_id int
	* return Str
	*/

	function get_category_data($cat_id)
    {
		$this->db->select('cat_id,country_id,project_type_id,cat_name,cat_budget,detail');
		$this->db->where('cat_id',$cat_id);
		$this->db->where('delete_record','0');
		
		$query	=	$this->db->get('department_categories');
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Project Type Category Budget
	* @param $cat_id int
	* return Str
	*/

	function sum_of_project_cat_budget()
    {
		$this->db->select('SUM(cat_budget) AS total');
		$this->db->where('delete_record','0');
		
		$query	=	$this->db->get('department_categories');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->total;
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Country Projects Type Budget
	* @param $country_id int
	* @param $project_type_id int
	* return OBJECT
	*/

	function get_department_budget($deptid,$project_type_id,$payment_type	=	'DEBIT')
    {
		$this->db->select('dept_budget');
		$this->db->where('deptid',$deptid);
		$this->db->where('department',$project_type_id);
		$this->db->where('payment_type',$payment_type);
		
		$query	=	$this->db->get('departments_budget');
		
		/*echo $this->db->last_query();
		exit();*/
		
		if($query->num_rows() > 0)
		{
			return $query->row()->dept_budget;
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Country Projects Type Budget
	* @param $country_id int
	* @param $project_type_id int
	* return OBJECT
	*/

	function get_cat_budget($deptid,$project_type_id)
    {
		$this->db->select('SUM(cat_budget) AS total');
		$this->db->where('deptid',$deptid);
		$this->db->where('project_type_id',$project_type_id);
		$this->db->where('delete_record','0');
		
		$query	=	$this->db->get('department_categories');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->total;
		}
    }
//-------------------------------------------------------------------
	/*
	* Get Project Type Categories
	* @param $country_id int
	* @param $project_type_id int
	* return OBJECT
	*/

	function get_department_categories($depid,$project_type_id)
    {
		$this->db->select('cat_id,cat_name,cat_budget,detail');
		$this->db->where('deptid',$depid);
		$this->db->where('project_type_id',$project_type_id);
		$this->db->where('delete_record','0');
		
		$query	=	$this->db->get('department_categories');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
//-------------------------------------------------------------------
	/*
	 * Get All Branches
	 * Return OBJECT
	 */
		
	function get_all_branches($typeid)
	{
		$query	=	$this->db->query("SELECT 
          b.branchid,
          b.branchname,
          IFNULL(total, 0) AS total 
        FROM
          `ah_branchs` AS b 
          LEFT JOIN 
            (SELECT 
              COUNT(applicantid) AS total,
              branchid 
            FROM
              `ah_applicant` AS A 
              WHERE charity_type_id='".$typeid."' AND isdelete='0' AND application_status != 'رفض' AND application_status != '' AND step='4' AND paid='no'
            GROUP BY A.branchid) AS lnew 
            ON lnew.branchid = b.branchid
        WHERE b.delete_record = '0' ");
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Province Name
	 * Return OBJECT
	 */
		
	function get_list_name($id)
	{
		$this->db->select('list_name');
		$this->db->where('list_id',$id);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get("ah_listmanagement");
		
		if($query->num_rows() > 0)
		{
			return $query->row()->list_name;
		}
	}
	
//-------------------------------------------------------------------
	/*
	 * Get All Banks Detail
	 * Return OBJECT
	 */
		
	function get_all_banks($typeid)
	{
		$query = $this->db->query("SELECT * FROM `alhaya_bank_management` AS BM
INNER JOIN `alhaya_banks_detail` AS BD
ON (BD.`register_country_id`=BM.`register_country_id`)
WHERE charity_type_id='".$typeid."' AND bank_type='alhaya' GROUP BY BD.`bankid`");


		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Total Banks Count
	 * Return OBJECT
	 */
		
	function get_banks_count($typeid)
	{
		$query = $this->db->query("SELECT COUNT(BM.charity_type_id)as total FROM `alhaya_bank_management` AS BM
INNER JOIN `alhaya_banks_detail` AS BD
ON (BD.`register_country_id`=BM.`register_country_id`)
WHERE BM.charity_type_id='".$typeid."' AND BM.bank_type='alhaya'");
		
		if($query->num_rows() > 0)
		{
			return $query->row()->total;
		}
	}	
//-------------------------------------------------------------------
	/*
	 * Get Total Banks Count
	 * Return OBJECT
	 */
		
	function get_total_app_in_account($typeid,$bankid,$branchid)
	{
		$query = $this->db->query("
		SELECT 
			applicantid
		FROM
		  `ah_applicant` AS A 
		WHERE charity_type_id = '".$typeid."' AND bankid='".$bankid."'
		  AND isdelete = '0' 
		  AND application_status != 'رفض' 
		  AND application_status != '' 
		  AND step = '4' 
		  AND paid = 'no'  AND urgent_applicant='0'");

		if($query->num_rows() > 0)
		{
			foreach($query->result() as $appid)
			{
				$decission = $this->inq->get_last_decission($appid->applicantid);
                $total_amount	+= $decission->decission_amount;
			}
			
			return $array	=	array('total_applicants'	=>	$query->num_rows(),'total_amount'	=>	 $total_amount);
		}
	}
//-------------------------------------------------------------	
	/*
	*
	*
	*/
	function paid_appli_count($charity_type,$branchid,$stop_payment)
	{
		$this->db->select('COUNT(ah_applicant.applicantid) AS total');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		if($branchid	!=	0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		
		if($charity_type!=0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.step",'5');
		$this->db->where("ah_applicant.paid",'yes');
		$this->db->where("ah_applicant.stop_payment",$stop_payment);
					
		$this->db->order_by('ah_applicant.registrationdate','DESC');
					
		$query = $this->db->get();
		

		if($query->num_rows() > 0)
		{
			return $query->row()->total;
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Total Banks Count
	 * Return OBJECT
	 */
		
	function urgent_inside_outside($typeid,$country)
	{
		if($country	==	'IN')
		{
			$countryid	=	'200';
		}
		else
		{
			$countryid	=	'!=200';
		}
		$query = $this->db->query("SELECT 
		  COUNT(applicantid) AS total
		FROM
		  `ah_applicant` AS A 
		WHERE charity_type_id = '".$typeid."'
		  AND isdelete = '0' 
		  AND application_status != 'رفض' 
		  AND application_status != '' 
		  AND step = '4' 
		  AND paid = 'no'  AND urgent_applicant='1' AND country='".$countryid."'");
		  

		if($query->num_rows() > 0)
		{
			return $query->row()->total;
		}
	}
	//----------------------------------------------------------------------

	/*
	* Get all Data 
	* return OBJECT
	*/

	function stop_applicant($applicantid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('applicantid',$applicantid);
		$this->db->update('ah_applicant',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Get Province Name
	 * Return OBJECT
	 */
		
	function get_bank_branches($parentid)
	{
		/*$this->db->select('list_id,list_name');
		$this->db->where('list_parent_id',$parentid);
		$this->db->where('delete_record','0');
		$this->db->where('list_status','1');
		
		$query = $this->db->get("ah_listmanagement");
		*/
		$query = $this->db->query("SELECT branchid FROM `alhaya_banks_detail` WHERE bankid='".$parentid."'");
		

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Province Name
	 * Return OBJECT
	 */
		
	function get_all_budgets()
	{
		$query = $this->db->query("SELECT bd_id,bd_year FROM `ah_budget_main` WHERE delete_record='0'");
		

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Province Name
	 * Return OBJECT
	 */
	function get_all_categories($type,$val)
	{
		if($type	==	'parent')
		{
			$parent	=	"bds_categoryId='0'";
		}
		else
		{
			$parent	=	"bds_categoryId='".$val."'";
		}
		
		$parent	=	"bds_categoryId='".$val."'";
		
		$query = $this->db->query("
		SELECT 
		bds_id,bd_id,bds_title 
		FROM 
		`ah_budget_sub` 
		WHERE delete_record='0'
		
		AND ".$parent.";");
		

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Province Name
	 * Return OBJECT
	 */
		
	function get_package_accounts($id,$type)
	{
		if($type	==	'parent')
		{
			$query_String	=	"AND bd_id=".$id."";
		}
		else
		{
			$query_String	=	"AND bds_id=".$id."";
		}
		
		$query = $this->db->query("SELECT 
		bds_bankname_id
		FROM 
		`ah_budget_sub` 
		WHERE delete_record='0'
		".$query_String."");
		
		if($query->num_rows() > 0)
		{
			return $query->row()->bds_bankname_id;
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Province Name
	 * Return OBJECT
	 */
		
	function get_banks_account($bank_ids)
	{
		$query = $this->db->query("SELECT * FROM `bank_managment_accounting` WHERE id IN(".$bank_ids.")");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Applicants transaction Record
	 * Return OBJECT
	 */
		
	function get_last_transaction($applicantid,$PAYMENT_TYPE)
	{
		$query = $this->db->query("
		SELECT
		budget_id,budget_cat_id,bank_account 
		FROM
		`muhasaba_types_amounts` 
		WHERE 
		applicant_id='".$applicantid."' AND payment_type='".$PAYMENT_TYPE."' ORDER BY submited_date DESC;");

		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	 * GET bank account numbers from Al haya Virtual Bank Managemnt system
	 * Return OBJECT
	 */
		
	function get_bank_accounts_by_cat($bds_id)
	{
		$get_bank_ids = $this->db->query("SELECT 
		bds_bankname_id 
		FROM 
		`ah_budget_sub`
		 WHERE bds_id='".$bds_id."'");

		if($get_bank_ids->num_rows() > 0)
		{
			//return $query->row()->bds_bankname_id;
			$bank_accounts = $this->db->query("
			SELECT id,bank_name,branch,account_no,swift_code 
			FROM 
			`bank_managment_accounting` 
			WHERE id IN(".$get_bank_ids->row()->bds_bankname_id.")");
			
			if($bank_accounts->num_rows() > 0)
			{
				return $bank_accounts->result();
			}
		}	
	}
//-------------------------------------------------------------------
	/*
	 * Get Applicants transaction Record
	 * Return OBJECT
	 */
		
	function amount_in_alhaya_bank($bankid)
	{
		$query = $this->db->query("SELECT SUM(amount_in_account) as TOTAL FROM `alhaya_banks_detail` WHERE bankid='".$bankid."'");

		if($query->num_rows() > 0)
		{
			return $query->row()->TOTAL;
		}
	}
//-------------------------------------------------------------	
	/*
	*
	*
	*/
	function muhasaba_medical_applicant($applicantid)
	{
		
		$this->db->select('ah_applicant.applicantid,ah_applicant.bankid,ah_applicant.branchid,ah_applicant.bankbranchid,ah_applicant.accountnumber,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		$this->db->where("ah_applicant.applicantid",$applicantid);
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.application_status != ",'رفض');
		$this->db->where("ah_applicant.application_status != ",'');
		$this->db->where("ah_applicant.step",'4');
		$this->db->where("ah_applicant.paid",'no');		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
					
		$query = $this->db->get();
		
		//echo $this->db->last_query();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------	
	/*
	*
	*
	*/
	function muhasaba_medical_applicant_count($charity_type_id,$branchid)
	{
		$this->db->select('count(ah_applicant.applicantid) AS TOTAL');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		
		$this->db->where("ah_applicant.charity_type_id",$charity_type_id);
		$this->db->where("ah_applicant.branchid",$branchid);
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.application_status != ",'رفض');
		$this->db->where("ah_applicant.application_status != ",'');
		$this->db->where("ah_applicant.step",'4');
		$this->db->where("ah_applicant.paid",'no');
		$this->db->where("ah_applicant.stop_payment",'0');
		$this->db->where("ah_applicant.urgent_applicant",'0');		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
					
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row()->TOTAL;
		}
	}
//----------------------------------------------------------------------

	/*
	* Get all Data 
	* return OBJECT
	*/

	function update_decission_detail($decissionid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('decissionid',$decissionid);
		$this->db->update('ah_applicant_decission',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
	//-------------------------------------------------------------	
	/*
	*
	*
	*/
	function muhasaba_listing_by_type_sakaniya($charity_type,$branchid,$bankid,$applicantion_status,$country)
	{
		if($applicantion_status	==	'urgent')
		{
			$status	=	'1';
		}
		else
		{
			$status	=	'0';
		}
		
		if($country	==	'IN')
		{
			$countryid	=	'200';
		}
		else
		{
			$countryid	=	'!=200';
		}
		
		
		$this->db->select('
			ah_applicant.applicantid,
			ah_applicant.bankid,
			ah_applicant.bankbranchid,
			ah_applicant.accountnumber,
			ah_branchs.branchname,
			ah_applicant.charity_type_id,
			ah_applicant.applicantcode,
			ah_applicant.fullname,
			ah_applicant.registrationdate,
			ah_applicant.idcard_number,
			ah_applicant.contract_ready,
			ah_applicant.contract_notes,
			ah_applicant.contract_document,
			c.quotationsId,
			PA.amount,
			cti.list_name AS charity_type,
			pro.list_name AS province,
			wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->join('ah_applicant_createcontract AS c','c.applicantid = ah_applicant.applicantid');
		$this->db->join('ah_applicant_percentage_amounts AS PA','PA.applicantid = ah_applicant.applicantid');

		$this->db->where("ah_applicant.charity_type_id",$charity_type);
		$this->db->where("PA.status",'1');
		$this->db->where("PA.paid",'0');
		
		if($country)
		{
			$this->db->where("ah_applicant.country",$countryid);
		}

		$this->db->where("ah_applicant.isdelete",'0');
		
		if($bankid)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		
		if($bankid)
		{
			$this->db->where("ah_applicant.bankid",$bankid);
		}
		
		$this->db->where("ah_applicant.application_status != ",'رفض');
		$this->db->where("ah_applicant.application_status != ",'');
		$this->db->where("ah_applicant.step",'4');
		$this->db->where("ah_applicant.contract_ready",'1');
		$this->db->where("ah_applicant.paid",'no');
		$this->db->where("ah_applicant.contract_ready",'1');
		$this->db->where("ah_applicant.urgent_applicant",$status);			
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		
			
		$query = $this->db->get();
		
		//echo $this->db->last_query(); exit();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------	
	/*
	*
	*
	*/
	function muhasaba_sakaniya_applicant($applicantid)
	{
		$this->db->select(
		'ah_applicant.applicantid,
		ah_applicant.bankid,
		ah_applicant.branchid,
		ah_applicant.bankbranchid,
		ah_applicant.accountnumber,
		ah_branchs.branchname,
		ah_applicant.charity_type_id,
		ah_applicant.applicantcode,
		ah_applicant.fullname,
		ah_applicant.registrationdate,
		ah_applicant.idcard_number,
		cti.list_name AS charity_type,
		pro.list_name AS province,
		wil.list_name AS wilaya,
		c.quotationsId,
		ah_applicant.contract_ready,
		ah_applicant.contract_notes,
		ah_applicant.contract_document,
		ah_applicant.approve_amount,');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->join('ah_applicant_createcontract AS c','c.applicantid = ah_applicant.applicantid');
		
		$this->db->where("ah_applicant.applicantid",$applicantid);
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->where("ah_applicant.application_status != ",'رفض');
		$this->db->where("ah_applicant.application_status != ",'');
		$this->db->where("ah_applicant.step",'4');
		$this->db->where("ah_applicant.contract_ready",'1');
		$this->db->where("ah_applicant.paid",'no');		
		$this->db->order_by('ah_applicant.registrationdate','DESC');
					
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get All Branches
	 * Return OBJECT
	 */
	
	function get_all_branches_sakaniya($STATUS	=	NULL)
	{
		if($STATUS	==	'INSIDE')
		{
			$query_string	=	"AND country_id='200'";
		}
		else
		{
			$query_string	=	"AND country_id!='200'";
		}
		
		$query	=	$this->db->query("
					SELECT 
					b.branchid,
					b.branchname,
					IFNULL(total, 0) AS total 
					FROM
					`ah_branchs` AS b 
					LEFT JOIN 
					(SELECT 
					user_branch_id,
					COUNT(orphan_id) AS total,
					TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age 
					FROM
					`ah_yateem_info` 
					WHERE delete_record = '0' 
					AND step = '3' 
					AND orphan_status = '1' 
					AND cancel = '0' 
					AND orphan_id IN 
					(SELECT 
					orphan_id 
					FROM
					`assigned_orphan` 
					WHERE is_old = '0') 
					AND DATE(expire_date) > CURDATE() ".$query_string."
					HAVING age < '18') AS lnew 
					ON lnew.user_branch_id = b.branchid 
					WHERE b.delete_record = '0' GROUP BY b.`branchid` ");
					

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get All Branches
	 * Return OBJECT
	 */
	
	function total_orphan_count($STATUS	=	NULL)
	{
		if($STATUS	==	'INSIDE')
		{
			$query_string	=	"AND country_id='200'";
		}
		elseif($STATUS	==	'OUTSIDE')
		{
			$query_string	=	"AND country_id!='200'";
		}
		else
		{
			$query_string	=	"";
		}
		
		$query	=	$this->db->query("
		SELECT
		  COUNT(orphan_id) AS total,
		  TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age 
		FROM
		  `ah_yateem_info` 
		WHERE delete_record = '0' 
		  AND step = '3' 
		  AND orphan_status = '1' 
		  AND cancel = '0' 
		  AND orphan_id IN 
		  (SELECT 
			orphan_id 
		  FROM
			`assigned_orphan` 
		  WHERE is_old = '0') 
		  AND DATE(expire_date) > CURDATE() ".$query_string."
		HAVING age < '18'");
		
		if($query->num_rows() > 0)
		{
			return $query->row()->total;
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Province Name
	 * Return OBJECT
	 */
		
	function get_contractor_detail($quotationid,$applicantid)
	{
		$query = $this->db->query("SELECT * FROM `ah_applicant_quotations` WHERE quotationsId='".$quotationid."' AND applicantid='".$applicantid."'");

		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Province Name
	 * Return OBJECT
	 */
		
	function get_all_lists_by_type($list_type,$orphan_Status,$country_status,$branchid)
	{
		if($country_status	==	'INSIDE')
		{
			$query_string	=	"AND ORPHAN.country_id='200'";
		}
		elseif($country_status	==	'OUTSIDE')
		{
			$query_string	=	"AND ORPHAN.country_id!='200'";
		}
		else
		{
			$query_string	=	"";
		}
		
		$query = $this->db->query("
		SELECT 
		l.list_id,
		l.list_name,
		IFNULL(total, 0) AS total 
		FROM
		ah_listmanagement AS l 
		LEFT JOIN 
			(SELECT
			COUNT(`ORPHAN`.`orphan_id`) AS total
			, `BANK`.`bankid`,
			TIMESTAMPDIFF(YEAR, ORPHAN.date_birth, CURDATE()) AS age
		FROM
			`ah_yateem_banksinfo` AS BANK
			INNER JOIN `ah_yateem_info` AS ORPHAN
				ON (`BANK`.`yateem_id` = `ORPHAN`.`orphan_id`)
				WHERE ORPHAN.delete_record = '0' 
		  AND ORPHAN.step = '3' 
		  AND ORPHAN.orphan_status = '1'
		  AND ORPHAN.user_branch_id = '".$branchid."'
		  AND ORPHAN.sponsor_type = '".$orphan_Status."'  
		  AND ORPHAN.cancel = '0' AND ORPHAN.orphan_id IN 
		  (SELECT 
			orphan_id 
		  FROM
			`assigned_orphan` 
		  WHERE is_old = '0') 
		  AND DATE(ORPHAN.expire_date) > CURDATE() ".$query_string." 
		GROUP BY ORPHAN.orphan_id 
		HAVING age < '18' ) AS lnew 
		ON lnew.bankid = list_id 
		WHERE l.list_type = '".$list_type."' 
		AND l.delete_record = '0' 
		AND l.list_parent_id = '0' 
		GROUP BY l.list_id ;");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}	
		
//-------------------------------------------------------------------
}
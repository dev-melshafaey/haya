<div class="row col-md-12">
  <form class="mws-form" method="post" action="<?php echo current_url();?>" id="stop_applicant" name="stop_applicant">
    <div class="col-md-4 form-group">
      <label class="text-warning">الإسم :</label>
      <?php echo $applicant_detail['ah_applicant']->fullname;?> </div>
    <?php if($applicant_detail['ah_applicant']->charity_type_id	==	'80'):?>
    <div class="col-md-4 form-group">
      <label class="text-warning">إسم المستفيد :</label>
      <?php echo $decission->applicant_name;?> </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">المستلزمات الطبية :</label>
      <?php echo $decission->medical_accessories;?></div>
    <div class="col-md-4 form-group">
      <label class="text-warning">اسم البنك :</label>
      <?php echo $decission->applicant_bank;?></div>
    <div class="col-md-4 form-group">
      <label class="text-warning">رقم الحساب :</label>
      <?php echo $decission->applicant_account_number;?> </div>
    <?php else:?>
    <div class="col-md-4 form-group">
      <label class="text-warning">اسم البنك :</label>
      <?PHP echo $this->haya_model->get_name_from_list($applicant_detail['ah_applicant']->bankid); ?> </div>
    <?php endif;?>
    <div class="col-md-4 form-group">
      <label class="text-warning">المبلغ :</label>
      <?PHP echo $decission->decission_amount; ?> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الحساب:</label>
      <select name="account_no" class="form-control" disabled="disabled">
        <?php
                                if(!empty($bank_account))
                                {
                                    foreach($bank_account as $account)
                                    {
										if($last_transaction->bank_account	==	$account->id)
										{
											$selected	=	'selected="selected"';
										}
										else
										{
											$selected	=	"";
										}
                                        echo '<option value="'.$account->id.'" '.$selected.'>'.$account->bank_name.'( '.$account->account_no.' )</option>';
                                    }
                                }
                                ?>
      </select>
    </div>
    <div id="resp"></div>
    <br clear="all" />
    <div class="col-md-6 form-group">
      <label for="basic-input"></label>
      <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $applicant_detail['ah_applicant']->applicantid;?>"/>
      <input type="hidden" name="branchid" id="branchid" value="<?php echo $applicant_detail['ah_applicant']->branchid;?>"/>
      <input type="hidden" name="amount" id="amount" value="<?php echo $decission->decission_amount?>"/>
      <input type="hidden" name="bankid" id="bankid" value="<?php echo $applicant_detail['ah_applicant']->bankid;?>"/>
      <input type="hidden" name="budget_id" id="budget_id" value="<?php echo $last_transaction->budget_id;?>"/>
      <input type="hidden" name="budget_cat_id" id="budget_cat_id" value="<?php echo $last_transaction->budget_cat_id;?>"/>
      <input type="hidden" name="bank_account" id="bank_account" value="<?php echo $last_transaction->bank_account;?>"/>
      <input type="hidden" name="charity_type_id" id="charity_type_id" value="<?php echo $applicant_detail['ah_applicant']->charity_type_id;?>"/>
      <input type="hidden" name="bankbranchid" id="bankbranchid" value="<?php echo $applicant_detail['ah_applicant']->bankbranchid;?>"/>
      <input type="submit" value="توقف" name="submit" class="btn btn-success mws-login-button">
    </div>
    <br clear="all" />
  </form>
</div>

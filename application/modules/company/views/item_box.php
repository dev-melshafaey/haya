<?php $num = $this->input->post('num');?>

<div id="heartdisk_<?PHP echo $num; ?>" class="col-md-12  form-group newboxxxx itemscollection">
  <div class="col-md-3 form-group select<?PHP echo $num; ?>">
    <label class="text-warning"><a href="#" onclick="getsetcategory('<?PHP echo $num; ?>');">الفئة: (اضافة الفئة جديد)</a></label>
    <select name="list_parent_id[]" onChange="getall_list_child('<?PHP echo $num; ?>',this);" id="<?PHP echo $num; ?>" class="form-control req item_select_box" placeholder="الفئة">
      <option value="">اختر الفئة</option>
      <?PHP foreach($cat as $cx) { ?>
      <option value="<?PHP echo $cx->list_id; ?>"><?PHP echo $cx->list_name; ?></option>
      <?PHP } ?>
    </select>
   <script>
    function getsetcategory(number) {
      $('.select'+number).hide();
      $('.input'+number).show();
    }
    
    function getsetsubcategory(number) {
      $('.subcategory'+number).hide();
      $('.subinput'+number).show();
    }
    
    function cancelCategory(number)
    {
      $('.select'+number).show();
      $('.input'+number).hide();
    }
    
    function cancelsubCategory(number)
    {
      $('.subcategory'+number).show();
      $('.subinput'+number).hide();
    }
    
    function saveCategory(number)
    {
      if ($('#input'+number)!='') {
        var request = $.ajax({
		  url: config.BASE_URL+'company/savecategory',
		  type: "POST",
		  data: {cat:$('#input'+number).val()},
		  dataType: "html",		  
		  success: function(msg)
		  {
            $('#heartdisk_'+number).remove();
            addItembox();
            
		  }
		});
      }
    }
    
    function savesubCategory(number)
    {
      if ($('#subinput'+number).val()!='') {
        var request = $.ajax({
		  url: config.BASE_URL+'company/savesubcategory',
		  type: "POST",
		  data: {subcat:$('#subinput'+number).val(),cat:$('#'+number).val()},
		  dataType: "html",		  
		  success: function(msg)
		  {
            $('#heartdisk_'+number).remove();
            addItembox();
            
		  }
		});
      }
      else
      {
        show_notification('الرجاء تحديد الفئة الأولى');
      }
    }
  </script>
  </div>
  <div class="col-md-3 form-group input<?PHP echo $num; ?>" style="display: none; ">
    <label class="text-warning">الفئة: </label>
    <input type="text" placeholder="الفئة" class="form-control input<?PHP echo $num; ?>" name="mycat" id="input<?PHP echo $num; ?>">
    <button type="button" onclick="saveCategory('<?PHP echo $num; ?>');" class="btn btn-success input<?PHP echo $num; ?>" style="margin-top:3px;">حفظ</button>
    <button type="button" onclick="cancelCategory('<?PHP echo $num; ?>');" class="btn btn-danger input<?PHP echo $num; ?>" style="margin-top:3px;">إلغاء</button>
  
  </div>
  <div class="col-md-3 form-group subcategory<?PHP echo $num; ?>">
    <label class="text-warning"><a href="#" onclick="getsetsubcategory('<?PHP echo $num; ?>');">العنصر : (إضافة عنصر جديد)</a></label>
    <select name="list_child_id[]" id="list_child_id_<?PHP echo $num; ?>" class="form-control req item_select_box" placeholder="العنصر">
    </select>
  </div>
  <div class="col-md-3 form-group subinput<?PHP echo $num; ?>" style="display: none; ">
    <label class="text-warning">العنصر:</label>
    <input type="text" placeholder="العنصر" class="form-control" name="mycats" id="subinput<?PHP echo $num; ?>">
    <button type="button" onclick="savesubCategory('<?PHP echo $num; ?>');" class="btn btn-success" style="margin-top:3px;">حفظ</button>
    <button type="button" onclick="cancelsubCategory('<?PHP echo $num; ?>');" class="btn btn-danger" style="margin-top:3px;">إلغاء</button>
  
  </div>
  <div class="col-md-2 form-group">
    <label class="text-warning">الكمية :</label>
    <input type="number" name="item_qty[]" onKeyUp="calculateAmountFor('<?PHP echo $num; ?>',this);" id="item_qty_<?PHP echo $num; ?>" value="0" placeholder="الكمية" class="form-control req reqQuantity">
  </div>
  <div class="col-md-2 form-group">
    <label class="text-warning">المبلغ :</label>
    <input type="number" name="item_price[]" onKeyUp="calculateAmountFor('<?PHP echo $num; ?>',this);" id="item_price_<?PHP echo $num; ?>" value="0" placeholder="المبلغ" class="form-control req reqAmount">
  </div>
  <div class="col-md-2 form-group">
    <label class="text-warning">المجموع : <i class="icon-remove" style="color:#FF070B; float:left;" onClick="removeItembox('<?PHP echo $num; ?>');"></i></label>
    <input type="text" class="form-control fulltotal" id="total_<?PHP echo $num; ?>" value="0" readonly>
  </div>
  <div class="col-md-12 form-group">
    <label class="text-warning">التفاصيل :</label>
    <textarea id="qdescription"  class="form-control" name="item_description[]"></textarea>
  </div>
  <br clear="all">
</div>

<script>
  $(document).ready(function(){
    $(".item_select_box").searchable({
		maxListSize: 100,						// if list size are less than maxListSize, show them all
		maxMultiMatch: 50,						// how many matching entries should be displayed
		exactMatch: false,						// Exact matching on search
		wildcards: true,						// Support for wildcard characters (*, ?)
		ignoreCase: true,						// Ignore case sensitivity
		latency: 200,							// how many millis to wait until starting search
		warnMultiMatch: 'top {0} matches ...',	// string to append to a list of entries cut short by maxMultiMatch 
		warnNoMatch: 'no matches ...',			// string to show in the list when no entries match
		zIndex: 'auto'							// zIndex for elements generated by this plugin
   	});
 });
</script>
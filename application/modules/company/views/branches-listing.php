<!doctype html>
<?php $userid	=	$this->uri->segment(3);?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $this->load->view('common/user-tabs'); ?>
      <div class="col-md-12">
      <section class="row">
      <div class="panel-heading text-overflow-hidden">
        <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
          <tbody role="alert" aria-live="polite" aria-relevant="all">
            <tr class="gradeA odd" bgcolor="#f5f5f5">
                            <td class=" sorting_1" colspan="2"><i class=""></i>
                              <p></p><h1 style="float:none !important;">المساعدات <?php echo $this->company->get_list_name($list_id);?> حسب الفرع</h1><p></p></td>
                          </tr>

            <?php foreach($branches as $branch) { ?>
            <tr class="gradeA even ">
              <td><strong><a href="<?php echo base_url();?>company/muhasaba_types_categories/<?php echo $list_id?>/<?php echo$branch->branchid ?>"><?php echo $branch->branchname;?></a></strong></td>
              <td><?php echo $branch->total;?></td>
            </tr>
            <?PHP } ?>
          </tbody>
        </table>
      </div>
    </section>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer');?>
</div>
</body>
</html>
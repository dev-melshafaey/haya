<!doctype html>
<?php $userid	=	$this->uri->segment(3);?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $this->load->view('common/user-tabs'); ?>
      <div class="col-md-12">
        <?php foreach($muhasaba_types as $muhasaba) { ?>
        <div class="col-md-2 main_body" id="<?php echo $section['list_id'];?>"> 
        
          <!--<div id="bingo<?php echo $section['list_id'];?>" class="col-md-12 center main_icon"><i  class="<?php echo $module->module_icon;?>"></i></div>-->
          
          <div class="col-md-12 center main_heading">
<!--          <a href="<?php echo base_url();?>company/muhasaba_type_listing/<?php echo $muhasaba['list_id'];?>"> <?php echo $muhasaba['name'];?></a> -->
          <a href="<?php echo base_url();?>company/branches/<?php echo $muhasaba['list_id'];?>"> <?php echo $muhasaba['name'];?></a> 
          <br/>
          </div>
          <div class="col-md-12 center"> <a class="main_count" href="#_">مجموع (<?php echo (isset($muhasaba['y']) ? $muhasaba['y'] : '0');?>)</a> </div>
        </div>
        <?PHP } ?>
         <div class="col-md-2 main_body" id="<?php echo $section['list_id'];?>"> 
          <div class="col-md-12 center main_heading">
          <a href="<?php echo base_url();?>company/financial_affairs_os"> <?php echo 'كفالة الايتام';?></a> 
          <br/>
          </div>
          <div class="col-md-12 center"> <a class="main_count" href="#_">مجموع (<?php echo (isset($total_orphans) ? $total_orphans : '0');?>)</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer');?>
</div>
</body>
</html>
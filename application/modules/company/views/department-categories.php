<?php $remaining	=	($project_type_budget	-	$spending_budget);?>
<div class="col-md-12" id="myprint"  style="direction:rtl;">
  <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <?php if(!empty($all_categories)):?>
    <tr>
      <td colspan="6" class="customhr">جميع الفئات:</td>
    </tr>
    <tr>
    <th style="text-align:center;">اسم التصنيف</th>
    <th style="text-align:center;">الميزانية</th>
    <th style="text-align:center;">التفاصيل</th>
    <th style="text-align:center;">الإجراءات</th>
    </tr>
    <?php foreach($all_categories	as $category):?>
    <?php //$actions	=	' <a onclick="delete_category(\''.trim($category->cat_id).'\');" href="#_" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';?>
    <?php //$actions = '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'outside_help/add_project_type_category/0/0/'.$category->cat_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';?>
    <?php $actions = '&nbsp;<a href="#globalDiag" onclick="alatadad(this);" data-url="'.base_url().'outside_help/single_cat_transactions/'.$category->cat_id.'"><i class="icon-eye-open"></i></a>';?>    
        <tr id="<?php echo $category->cat_id;?>">
          <td><?php echo $category->cat_name;?></td>
          <td><?php echo number_format($category->cat_budget,3);?></td>
          <td><?php echo $category->detail;?></td>
          <td><?php //echo $actions;?></td>
        </tr>
        <?php unset($actions);?>
    <?php endforeach;?>
    <?php else:?>
     <tr>
     <td colspan="2"><strong>تم العثور على أي سجل</strong></td>
     </tr>
    <?php endif;?>
  </table>
</div>
<?php if(!empty($all_categories)):?>
<!--<div class="col-md-12 center">
      <button type="button" class="btn" id="print" onclick="printthepage('myprint');"><i class="icon-print"></i> طباعة</button>
    </div>-->
<?php endif;?>
<script>
$(function(){
	$('#t_heading').html('<?php echo $this->haya_model->get_name_from_list($project_type_id);?>');
	
});

function delete_category(cat_id)
{
	$.ajax({
	  url: config.BASE_URL + 'outside_help/delete_project_type_category/'+cat_id,
	  type: "POST",
	  data: "",
	  dataType: "json",
	  success: function(msg)
	  {
		  $("#"+cat_id+"").remove();
	  }
	});
}
</script>
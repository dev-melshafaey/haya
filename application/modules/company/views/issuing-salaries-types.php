<!doctype html>
<?php $userid	=	$this->uri->segment(3);?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $this->load->view('common/user-tabs'); ?>
      <div class="col-md-12">
         <div class="col-md-2 main_body"> 
          <div class="col-md-12 center main_heading">
          <a href="<?php echo base_url();?>company/all_branches/INSIDE"> <?php echo 'الايتام داخل السلطنة ';?></a> 
          <br/>
          <a href="<?php echo base_url();?>get_all_orphan_by_status/2/2/200/0/INSIDE"><span class="myicon icon-sort-by-alphabet-alt"></span></a>
          </div>
          <div class="col-md-12 center"> <a class="main_count" href="#_">مجموع (<?php echo (isset($total_inside) ? $total_inside : '0');?>)</a> </div>
        </div>
        <div class="col-md-2 main_body"> 
          <div class="col-md-12 center main_heading">
          <a href="<?php echo base_url();?>company/all_branches/OUTSIDE"> <?php echo 'الايتام خارج السلطنة ';?></a> 
          <br/>
          <a href="<?php echo base_url();?>get_all_orphan_by_status/2/2/200/0/OUTSIDE"><span class="myicon icon-sort-by-alphabet-alt"></span></a>
          </div>
          <div class="col-md-12 center"> <a class="main_count" href="#_">مجموع (<?php echo (isset($total_out) ? $total_out : '0');?>)</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer');?>
</div>
</body>
</html>
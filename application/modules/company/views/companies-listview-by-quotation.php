<div class="row" style="background-color:#FFF !important;">
  <div class="col-md-12 form-group">
    <div class="tab-pane list-group active">
      <div class="col-md-12 form-group">
        <div class="col-md-6 form-group">
          <label class="text-warning">اسم الاقتباس :</label>
          <?php echo $quote['company_data']->arabic_name.' ('.$quote['company_data']->english_name.') '; ?> </div>
        <div class="col-md-6 form-group">
          <label class="text-warning">رقم السجل التجاري :</label>
          <?php echo arabic_date($quote['company_data']->cr_number); ?> </div>
        <div class="col-md-6 form-group">
          <label class="text-warning">عرض السعر :</label>
          <?php echo $quote['company_data']->quote_title; ?> </div>
        <div class="col-md-6 form-group">
          <label class="text-warning">تاريخ التقديم‎ :</label>
          <?php echo date('Y-m-d',strtotime($quote['company_data']->quote_data)); ?> </div>
      </div>
    </div>
  </div>
  <br clear="all" />
      <table class="table table-bordered table-striped dataTable">
        <tbody>
        <thead>
            <th><h4>الفئة</h4></th>
            <th><h4>العنصر</h4></th>
            <th><h4>الكمية</h4></th>
            <th><h4>المبلغ</h4></th>
        </thead>
      <?php //$departments	=	$this->company->get_dept_budget($budgetid);?>
      <?php //$add_budget		=	0;?>
      <?php if(!empty($quote['items'])):?>
		  <?php foreach($quote['items'] as $item):?>
          <?php $total_price	+=	$item->item_price;?>
          <?php $total_items	+=	$item->item_qty;?>
              <tr>
                <td><?php echo $this->haya_model->get_name_from_list($item->list_parent_id);?></td>
                <td><?php echo $this->haya_model->get_name_from_list($item->list_child_id);?></td>
                <td><?php echo $item->item_qty;?></td>
                <td><?php echo $item->item_price;?></td>
              </tr>
          <?php endforeach;?>
     <?php endif;?>
      <tr>
            <td><label class="text-warning">مجموع</label></td>
            <td></td>
            <td><strong><?php echo $total_items;?></strong></td>
            <td><strong><?php echo number_format($total_price,2);?></strong></td>
          </tr>
<!--         <tr>
            <td><label class="text-warning">مجموع</label></td>
            <td><strong><?php //echo number_format($add_budget,2); ?></strong></td>
            <td></td>
          </tr>
          <tr>
            <td><label class="text-warning">المتبقية</label></td>
            <td>
            <?php //$remaining	=	($budget_amount-$add_budget);?>
            <strong><?php //echo number_format($remaining,2); ?></strong>
            </td>
            <td></td>
          </tr>-->
        </tbody>
      </table>
</div>

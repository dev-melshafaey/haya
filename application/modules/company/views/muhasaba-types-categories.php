<?php 
if($type_id	==	'80')
{
	$IN_COUNTRY		=	"".base_url()."company/muhasba_medical_applicants/".$type_id."/".$branchid."/0/urgent/IN";
	$OUT_COUNTRY	=	"".base_url()."company/muhasba_medical_applicants/".$type_id."/".$branchid."/0/urgent/OUT";
}
else
{
	$IN_COUNTRY		=	"".base_url()."company/muhasaba_applicants_listing/".$type_id."/".$branchid."/0/urgent/IN";
	$OUT_COUNTRY	=	"".base_url()."company/muhasaba_applicants_listing/".$type_id."/".$branchid."/0/urgent/OUT";
}
?>

<!doctype html>
<?php $userid	=	$this->uri->segment(3);?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<style>
.main_heading {
  font-size: 20px !important;
}
</style>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $this->load->view('common/user-tabs'); ?>
      <?php if($type_id	==	'80'):?>

      <div class="col-md-2 main_body">
        <div class="col-md-12 center main_heading"><a href="<?php echo base_url();?>company/muhasba_medical_applicants/<?php echo $type_id;?>/<?php echo $branchid;?>"> <?php echo 'Medical Applicants';?></a> <br/>
        </div>
        <div class="col-md-12 center"> <a class="main_count" href="#_">مجموع (<?php echo (isset($total_medical_app) ? $total_medical_app : '0');?>)</a> </div>
      </div>
       <?php elseif($type_id	==	'78'):?>

      <div class="col-md-2 main_body">
        <div class="col-md-12 center main_heading"><a href="<?php echo base_url();?>company/muhasba_sakaniya_applicants/<?php echo $type_id;?>/<?php echo $branchid;?>"> <?php echo 'Sakaniya Applicants';?></a> <br/>
        </div>
        <div class="col-md-12 center"> <a class="main_count" href="#_">مجموع (<?php echo (isset($total_medical_app) ? $total_medical_app : '0');?>)</a> </div>
      </div>
      <?php else:?>
      <div class="col-md-2 main_body" id="<?php echo $section['list_id'];?>">
        <div class="col-md-12 center main_heading"><a href="<?php echo base_url();?>company/banks_listing/<?php echo $type_id;?>/<?php echo $branchid;?>"> <?php echo 'دفعات حسب الفئات';?></a> <br/>
        </div>
        <div class="col-md-12 center"> <a class="main_count" href="#_">مجموع (<?php echo (isset($total) ? $total : '0');?>)</a> </div>
      </div>
      <?php endif;?>
      <div class="col-md-2 main_body" id="<?php echo $section['list_id'];?>">
        <div class="col-md-12 center main_heading"><a href="<?php echo $IN_COUNTRY;?>"> <?php echo 'الحالات الطارئة داخل السلطنة
';?></a> <br/>
        </div>
        <div class="col-md-12 center"><a class="main_count" href="#_">مجموع (<?php echo (isset($urgent_inside) ? $urgent_inside : '0');?>)</a> </div>
      </div>
      <div class="col-md-2 main_body" id="<?php echo $section['list_id'];?>">
        <div class="col-md-12 center main_heading"><a href="<?php echo $OUT_COUNTRY;?>"> <?php echo 'الحالات الطارئة خارج السلطنة
';?></a> <br/>
        </div>
        <div class="col-md-12 center"><a class="main_count" href="#_">مجموع (<?php echo (isset($urgent_outside) ? $urgent_outside : '0');?>)</a> </div>
      </div>
      <div class="col-md-2 main_body" id="<?php echo $section['list_id'];?>">
        <div class="col-md-12 center main_heading"><a href="<?php echo base_url();?>company/muhasaba_type_listing/<?php echo $muhasaba['list_id'];?>"> <?php echo 'الحالات الاستثنائية
';?></a> <br/>
        </div>
        <div class="col-md-12 center"> <a class="main_count" href="#_">مجموع (<?php echo (isset($muhasaba['y']) ? $muhasaba['y'] : '0');?>)</a> </div>
      </div>
      <br/>
    </div>
  </div>
  <br clear="all">
  <div class="col-md-12" style="background-color:#f5f5f5;  padding: 9px;"> </div>
  <br clear="all">
  <div class="col-md-12"> 
    <!-------------------------------------->
    <div class="col-md-2 main_body" id="<?php echo $section['list_id'];?>">
      <div class="col-md-12 center main_heading"><a href="<?php echo base_url();?>company/stop_applicants_listing/<?php echo $type_id;?>/<?php echo $branchid;?>"> <?php echo 'المرتدات';?></a> <br/>
      </div>
      <div class="col-md-12 center"> <a class="main_count" href="#_">مجموع (<?php echo (isset($stop_total) ? $stop_total : '0');?>)</a> </div>
    </div>
    <div class="col-md-2 main_body" id="<?php echo $section['list_id'];?>">
      <div class="col-md-12 center main_heading"><a href="<?php echo base_url();?>company/muhasaba_paid_applicants/<?php echo $type_id;?>/<?php echo $branchid;?>"> <?php echo 'المدفوعات السابقة';?></a> <br/>
      </div>
      <div class="col-md-12 center"> <a class="main_count" href="#_">مجموع (<?php echo (isset($total_paid) ? $total_paid : '0');?>)</a> </div>
    </div>
    <!--------------------------------------> 
  </div>
  </div>
</section>
<?php $this->load->view('common/footer');?>
</div>
</body>
</html>
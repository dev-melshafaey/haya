<?php $remaining				=	($project_type_budget	-	$spending_budget);?>
<?php $cats_budget_remaining	=	($project_type_budget	-	$defined_cat_budgets);?>

<div class="row col-md-12"> <br clear="all"/>
  <div class="col-md-3 form-group">
    <label class="text-warning">مجموع : <?php echo number_format($project_type_budget,3);?></label>
  </div>
  <div class="col-md-3 form-group">
    <label class="text-warning">الإنفاق المنبع : <?php echo number_format($spending_budget,3);?></label>
  </div>
  <div class="col-md-3 form-group">
    <label class="text-warning">المتبقية : <?php echo number_format($remaining,3);?></label>
  </div>
  <div class="col-md-3 form-group">
    <label class="text-warning">Categories Budget Amount : <?php echo number_format($cats_budget_remaining,3);?></label>
  </div>
  <form class="mws-form" method="post" action="<?php echo current_url();?>" id="add_project_type_category" name="add_project_type_category" enctype="multipart/form-data">
    <input type="hidden" name="country_budget_id" id="country_budget_id" value="<?PHP echo $country_id; ?>">
    <input type="hidden" name="project_type_id" id="project_type_id" value="<?php echo $project_type_id;?>"  />
    <input type="hidden" name="cat_id" id="cat_id" value="<?php echo $cat_data->cat_id;?>" />
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم :</label>
      <input type="text" class="form-control req" name="cat_name" id="cat_name" value="<?php echo $cat_data->cat_name;?>">
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الميزانية :</label>
      <input type="text" class="form-control req" name="cat_budget" id="cat_budget" value="<?php echo $cat_data->cat_budget;?>" onKeyUp="only_numeric(this);">
      <label class="text-warning" id="show-msg" style="color:#F00 !important;"></label>
    </div>
    <div class="col-md-12 form-group">
      <label class="text-warning">التفاصيل :</label>
      <textarea class="form-control req" name="detail" id="detail" rows="10" cols="20"><?php echo $cat_data->detail;?></textarea>
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning"></label>
      <input type="button" value="حفظ" name="submit" id="project_type_category" class="btn btn-success" onclick="submit_project_type_category();" >
    </div>
  </form>
</div>
<div class="row col-md-12"> </div>
<script>
$(function(){
	$('#t_heading').html('<?php echo $this->haya_model->get_name_from_list($project_type_id)?>');
	
	var	remaining	=	'<?php echo $cats_budget_remaining;?>';
	
	if(remaining	<=	0)
	{
		$('#project_type_category').attr("disabled", true);
	}
	
	var cat_budget	=	$("#cat_budget").val();
	
	$( "#cat_budget" ).keyup(function() {

	var	current_amount	=	Number($("#cat_budget").val());

	var	remaining		=	Number('<?php echo $cats_budget_remaining;?>');

	if(remaining < current_amount)
	{
		$('#project_type_category').attr("disabled", true);
		$('#show-msg').html("Your Budget is increase from Remaing Budget "+remaining);
	}
	else
	{
		$('#project_type_category').attr("disabled", false);
		$('#show-msg').html("");
	}
		
	});
});

function submit_project_type_category()
{
	$('#add_project_type_category .req').removeClass('parsley-error');
    $('#add_project_type_category .req').each(function (index, element) {
        if ($.trim($(this).val()) == '') {
            $(this).addClass('parsley-error');
        }
    });
    var len = $('#add_project_type_category .parsley-error').length;

	if(len<=0)
	{
		var str_data = 	$('#add_project_type_category').serialize();
		
		var request = $.ajax({
		  url: config.BASE_URL+'outside_help/add_project_type_category',
		  type: "POST",
		  data: str_data,
		  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(newrow)
		  {
			  window.location.href = newrow;
		  } 
		});
	}
}
</script>
<div class="row col-md-12">
  <form class="mws-form" method="post" action="<?PHP base_url(); ?>company/saveCompanyQuotation" enctype="multipart/form-data" id="add_company_quotation" name="add_company_quotation">
    <input type="hidden" id="budget_amount" name="budget_amount" value="<?php echo $quote->budget_amount; ?>"/>
    <input type="hidden" id="quoteid" name="quoteid" value="<?php echo $quoteselected_id; ?>"/>
    <input type="hidden" id="companyid" name="companyid" value="<?php echo $companyid; ?>"/>
       
    <div class="col-md-6 form-group">
      <?PHP if($quoteselected_id=='') { ?>
      <input type="radio" checked="" id="search-q" tabindex="59">
      &nbsp;&nbsp;&nbsp;
      <label class="text-warning">بحث عرض السعر  \ عدد</label>
      <input type="radio" id="list-q"  tabindex="60">
      &nbsp;&nbsp;&nbsp;
      <label class="text-warning">قائمة السعر</label>
    <div class="col-md-12 form-group"  style="padding: 0px !important">
      <div id="search-record-q" style="display:none;">
       
        <input type="text" class="form-control"  placeholder="عرض السعر" id="string">
      </div>
      <div id="list-record-q" style="display:none;">       
        <select class="form-control"  placeholder="عرض السعر" id="q-select">
          <option value="">اختيار</option>
          <?PHP foreach($this->company->get_all_quotations() as $quote) { ?>
          <option value="<?PHP echo $quote->quoteid; ?>" <?php echo ($quote->quoteid	==	$quoteselected_id ? 'selected="selected"' : NULL);?> ><?PHP echo $quote->quote_title; ?></option>
          <?PHP } ?>
        </select>
      </div>
    </div>
    <?PHP } else { ?>
    <h2 style="margin-top: 0px; border-bottom: 1px solid #ddd;"><?PHP echo $quote->quote_title; ?></h2>
    <h5 style="font-size: 11px !important; text-align: justify; letter-spacing: 1px;"><?PHP echo $quote->quote_detail; ?></h5>
    <?PHP } ?>
    </div>
    
    <div class="col-md-6 form-group">
    
      <input type="radio" checked="" id="search" tabindex="59">
      &nbsp;&nbsp;&nbsp;
      <label class="text-warning">بحث شركة \ عدد</label>
      <input type="radio" id="list"  tabindex="60">
      &nbsp;&nbsp;&nbsp;
      <label class="text-warning">قائمة الشركات</label>
    <div class="col-md-12 form-group" style="padding: 0px !important">
      <div id="search-record" style="display:none;">
        
        <input type="text" class="form-control"  placeholder="شركة" id="company_string">
      </div>
      <div id="list-record" style="display:none;">
       
        
        <?php if($companyid!='' && $companyid!=0) { ?>
        <strong><?PHP echo $company->english_name.' | '.$company->arabic_name; ?> (<?PHP echo arabic_date($company->cr_number); ?>)</strong>
        <?PHP } else { ?>
        <select class="form-control"  placeholder="اختر شركة" id="c-select">
          <option value="">اختر شركة</option>
          <?PHP foreach($company as $cgroup) { ?>
          <option value="<?PHP echo $cgroup->companyid; ?>"><?PHP echo $cgroup->arabic_name.' | '.$cgroup->english_name; ?> (<?PHP echo arabic_date($cgroup->cr_number); ?>)</option>
          <?PHP } ?>
        </select>
        <?PHP } ?>
      </div>
    </div> 
    
    </div>
       
    
    <div class="col-md-6 form-group">
      <label class="text-warning">نوع الخصم :</label>
      <?PHP $this->haya_model->enum_dropbox('quote_discount_type',$quote->quote_discount_type,'ah_quote_company','quote_discount_type','نوع خصم'); ?>
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">حالة العرض :</label>
      <?PHP $this->haya_model->enum_dropbox('quote_status',$quote->quote_status,'ah_quote_company','quote_status','الحالة العرض'); ?>
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">خصم العرض :</label>
      <input type="number" class="form-control req NumberInput"  placeholder="خصم العرض" name="quote_discount" id="quote_discount" value="<?php echo $postal_code;?>">
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">مورفقات :</label>
      <input class="form-control <?php if(!isset($quote->quote_attachment)):?>req<?php endif;?>" type="file" name="quote_attachment" id="quote_attachment" placeholder="مورفقات" title='مورفقات'>
    </div>
    <br clear="all">
    <div class="col-md-12 form-group newboxxxx" id="afterme">
      <input type="button" value="حفظ العرض" style="float:left !important;" onClick="saveQuotationForm();" name="submit-company-quote" class="btn btn-success mws-login-button zeropoint">
      <div id="message" style="float:left !important;color:#F00;font-size:14px;margin-left: 17px;margin-top: 5px;"></div>
      <input type="button" value="إضافة عنصر" name="ontheway" onClick="addItembox();" class="btn btn-warning">
      <br clear="all">
    </div>
    <div class="col-md-4 form-group mytotalbox">
      <label class="text-warning">المجموع :</label>
      <input type="text" class="form-control" id="total_x" value="0" readonly>
    </div>
    <div class="col-md-4 form-group mytotalbox">
      <label class="text-warning">المجموع العرض :</label>
      <input type="text" class="form-control" id="discount_x" value="0" readonly>
    </div>
    <div class="col-md-4 form-group mytotalbox">
      <label class="text-warning">السعر الإجمال :</label>
      <input type="text" class="form-control" id="grand_total" value="0" readonly>
    </div>
  </form>
</div>
<script>
$(function()
{
  var checked 	= $("#search").attr('checked', true);
  var checked_q = $("#search-q").attr('checked', true);
  
  if(checked)
  { 
	$("#search-record").show();
  }
  if(checked_q)
  { 
	$("#search-record-q").show();
  }

/* ************************************************************** */
  
  $("#string").autocomplete({source: config.BASE_URL+"company/get_quotation_search",
  open: function( event, ui ) {
	  $(".ui-autocomplete").css("z-index",2000)
	  },
	  select: function( event, ui ) {
		  $("#quoteid").val(ui.item.id);
		  $("#budget_amount").val(ui.item.amount);
		  //$("#string").removeClass("req");
		  
		  }
  });
  
/* ************************************************************** */

    $("#company_string").autocomplete({source: config.BASE_URL+"company/search_company",
  open: function( event, ui ) {
	  $(".ui-autocomplete").css("z-index",2000)
	  },
	  select: function( event, ui ) {
		  $("#companyid").val(ui.item.id);
		  //$("#company_string").removeClass("req");
		  }
  });
  
 /* ************************************************************** */
 
  	$('#search').click(function()
	{
		$( "#list" ).prop( "checked", false );
		
		 $("#search-record").show();
		 $("#list-record").hide();
	});
	
/* ************************************************************** */

	$('#list').click(function()
	{
		$( "#search" ).prop( "checked", false );
		
		$("#list-record").show();
		 $("#search-record").hide();
	});
	
/* ************************************************************** */

	$('#search-q').click(function()
	{
		$( "#list-q" ).prop( "checked", false );
		
		 $("#search-record-q").show();
		 $("#list-record-q").hide();
	});
/* ************************************************************** */

	$('#list-q').click(function()
	{
		$( "#search-q" ).prop( "checked", false );
		
		$("#list-record-q").show();
		 $("#search-record-q").hide();
	});
/* ************************************************************** */
	
	 $('#q-select').on('change', function(){
	    $("#quoteid").val($(this).val());
		
		var taurusData = $.ajax({			
			url: config.BASE_URL+'company/get_quotation_amount',
			type:"POST",
			dataType:"html",
			data:{id:$(this).val()},			
			success: function(budget)
			{
				 $("#budget_amount").val(budget);
			}
		});
   });
/* ************************************************************** */

   	 $('#c-select').on('change', function(){
	    $("#companyid").val($(this).val());
   });
/* ************************************************************** */
});

function addItembox()
{
	var cnt = 0;
	var ht = '<ul>';
	$('#add_company_quotation .req').each(function(index, element) {
		if($(this).val()=='')
		{
			cnt++;
			ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
		}
	});
	ht += '</ul>';
	if(cnt <= 0)
	{
        //Checking Quotation Form
        var quoteid = $('#quoteid').val();
        var companyid = $('#companyid').val();
        if (quoteid!='' && companyid!='') {
          var leoData = $.ajax({			
                url: config.BASE_URL+'company/validateCompany',
                type:"POST",
                dataType:"html",
                data:{quoteid:quoteid,companyid:companyid},			
                success: function(msg)
                {
                    if (msg==0) {
                      var newNum = Math.floor((Math.random() * 9990000121000) + 1);
                      var taurusData = $.ajax({			
                          url: config.BASE_URL+'company/item_box',
                          type:"POST",
                          dataType:"html",
                          data:{num:newNum},			
                          success: function(msg)
                          {	$('#afterme').after(msg);	
                              itemscollectionCount();
                          }
                      });
                    }
                    else
                    {
                      show_notification_error_end("<li>هذه الشركة تقدمت بعرض سعر مسبقاً</li>");
                    }
                }
            });
        }
		
	}
	else
	{	show_notification_error_end(ht);	}
	
}

function saveQuotationForm()
{
	$('#add_company_quotation .req').removeClass('parsley-error');
	var ht = '<ul>';
	$('#add_company_quotation .req').each(function(index, element) {
		if($(this).val()=='')
		{
			$(this).addClass('parsley-error');
			ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
		}
	});
	var redline = $('#add_company_quotation .parsley-error').length;
	ht += '</ul>';
	if(redline <= 0)
	{
		var	grand_total		=	$("#total_x").val();
		var	total_budget	=	$("#budget_amount").val();
		
		$('#add_company_quotation').submit();
	}
	else
	{	show_notification_error_end(ht);	}
}

function removeItembox(id)
{
	$('#heartdisk_'+id).remove();
	itemscollectionCount();
	
}

function getall_list_child(obj_id,obj)
{
	
	var taurusData = $.ajax({			
			url: config.BASE_URL+'company/child_item',
			type:"POST",
			dataType:"html",
			data:{id:$(obj).val()},			
			success: function(msg)
			{	$('#list_child_id_'+obj_id).html(msg);	
			}
		});
}

function itemscollectionCount()
{
	var lenof = $('.itemscollection').length;
	if(lenof > 0)
	{	$('.zeropoint').show();	
	$('.mytotalbox').show();
		}
	else
	{	$('.zeropoint').hide();
	$('.mytotalbox').hide();	}
}

function calculateAmountFor(object_id,object)
{
	var item_qty = $('#item_qty_'+object_id).val();
	var item_price = $('#item_price_'+object_id).val();
	var total_box = '#total_'+object_id;
	var discount_value = $('#quote_discount').val();
	var quote_discount_type = $('#quote_discount_type').val();
	var total = 0;
	if(item_qty!='' && item_price!='')
	{
		$(total_box).val(parseInt(item_qty)*parseInt(item_price));
	}
	
	$('.fulltotal').each(function(index, element) {
        total += parseInt($(this).val());
    });
	
	$('#total_x').val(total);
	
	if(quote_discount_type=='نسبة مئوية')
	{	
		var totalvalue = (total*discount_value)/100;
		$('#discount_x').val(totalvalue);
		$('#grand_total').val(parseInt(total)-parseInt(totalvalue));
	}
	else
	{	$('#grand_total').val(parseInt(total)-parseInt(discount_value));
		$('#discount_x').val(discount_value);
	}
}

function get_records_by_id()
{
	var id	=	$("item_id").val();
}
</script>
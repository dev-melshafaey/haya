<?php $segment	=	$this->uri->segment(1);?>
<?php $text		=	$this->lang->line($segment);?>
<?php $labels	=	$text['branchs']['form'];?>

<div class="row col-md-12">
  <form class="mws-form" method="post" action="<?php echo current_url();?>" id="add_customer" name="add_customer">
    <div class="col-md-4 form-group">
      <label class="text-warning">اسم الزبون :</label>
      <input type="text" class="form-control req" name="customer_name" id="customer_name" value="<?php echo $customer_name;?>">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">عنوان العملاء :</label>
      <input type="text" class="form-control req" name="customer_title" id="customer_title" value="<?php echo $customer_title;?>">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">اسم الشركة :</label>
      <input type="text" class="form-control" name="company_name" id="company_name" value="<?php echo $company_name;?>">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">الرمز البريدي :</label>
      <input type="text" class="form-control req" name="postal_code" id="postal_code" value="<?php echo $postal_code;?>">
    </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">المحافظة \ المنطقة :</label>
      <?PHP echo $this->haya_model->create_dropbox_list('province','regions',$province,0,'req'); ?> </div>
    <div class="col-md-4 form-group">
      <label class="text-warning">ولاية :</label>
      <?PHP echo $this->haya_model->create_dropbox_list('wilaya','wilaya',$wilaya,$province,'req'); ?> </div>
    <div class="col-md-4 form-group">
            <label class="text-warning">اسم البنك:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('bankid','bank',$bankid,0,'req'); ?> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">الفرع:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('bankbranchid','bank_branch',$bankbranchid,$bankid,'req'); ?> </div>
          <div class="col-md-4 form-group">
            <label class="text-warning">رقم الحساب:</label>
            <input name="accountnumber" value="<?PHP echo $accountnumber; ?>" placeholder="رقم الحساب" id="accountnumber" type="text" class="form-control req">
          </div>
          <div class="col-md-12 form-group">
            <label class="text-warning">جاري \ توفير:</label>
            <input type="radio" name="accounttype" <?PHP if($ah_applicant->accounttype=='جاري') { ?> checked <?PHP } ?> id="accounttype" checked value="جاري">
            &nbsp;&nbsp;&nbsp;
            <label class="text-warning">جاري</label>
            <input type="radio" name="accounttype" <?PHP if($ah_applicant->accounttype=='توفير') { ?> checked <?PHP } ?> id="accounttype" value="توفير">
            &nbsp;&nbsp;&nbsp;
            <label class="text-warning">توفير</label>
          </div>
	
	
	
	<div class="col-md-12 form-group">
      <label class="text-warning">عنوان :</label>
      <textarea class="form-control" name="address" id="address" style="margin: 0px; width:100%; height: 150px;"><?php echo $address;?></textarea>
    </div>
    <div id="resp"></div>
    <br clear="all" />
    <div class="col-md-4 form-group">
      <label for="basic-input"></label>
      <input type="hidden" name="userid" id="userid" value="<?php echo $login_userid;?>"/>
      <input type="hidden" name="customer_id" id="customer_id" value="<?php echo $customer_id;?>"/>
      <input type="button" value="حفظ" name="submit" class="btn btn-success mws-login-button" onclick="submit_cust();">
    </div>
  </form>
</div>
<script>
function bindWillaya(type) 
{
	var provinceid	=	$("#provinceid").val();

	$.ajax({
			url: '<?php echo base_url();?>haya/get_sub_category',
			type: "POST",
			data:'provinceid='+provinceid,
			dataType: "html",
			success: function(msg)
			{
				$("#show-willaya").html(msg);
			}
		});
}
</script>
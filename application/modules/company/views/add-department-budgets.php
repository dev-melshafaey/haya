<div class="row col-md-12">
  <form method="post" action="<?php echo current_url();?>" enctype="multipart/form-data" id="add_budget" name="add_budget">
      <input type="hidden" name="addedby" id="addedby" value="<?php echo $login_userid;?>"/>
      <input type="hidden" name="budgetid" id="budgetid" value="<?php //echo $budgetid;?>"/>
      <input type="hidden" name="budget_year" id="budget_year" value="<?PHP echo $year; ?>" />
	<div class="col-md-12 form-group" style="background-color: #FFF;
    padding-bottom: 3px;
    border: 1px solid #CCC;
    margin: 15px 16px;
    width: 97%;">
		 <h4 >سنة : <?PHP echo arabic_date($year); ?></h4>
   </div>
   <div class="col-md-6 form-group">
      <label class="text-warning">مجموع الميزانية :</label>
      <input type="text" class="form-control req NumberInput"  placeholder="مجموع الميزانية" name="budget_amount" id="budget_amount" value="<?php //echo $quote->budget_amount;?>">
    </div>
   <div class="col-md-6 form-group">
		<label class="text-warning">المتبقية :</label>
		
		<span id="dept-count" class="form-control" style="font-size: 16px; font-weight: bold; letter-spacing: 1px;">0</span>
   </div>
   <div class="col-md-12 form-group" style="background-color: #FFF;
    padding-bottom: 3px;
    border: 1px solid #CCC;
    margin: 15px 16px;
    width: 97%;">
		 <h4 >قسم الميزانية</h4>
   </div>
   
   
   <?php $departments	=	$this->haya_model->get_dropbox_list_value('departments');?>
   <?php foreach($departments as $dpt):?>
   <div class="col-md-4 form-group">
      <label class="text-warning"><?php echo $dpt->list_name?> :</label>
      <input type="text" class="form-control NumberInput departments req"  placeholder="<?php echo $dpt->list_name;?>" name="department_budget[<?php echo $dpt->list_id?>]" id="department_budget" value="">
    </div>
   <?php endforeach;?>
    
    <br clear="all">
    <div class="col-md-12 form-group newboxxxx">
      <input type="button" value="حفظ العرض" onClick="saveBudgetForm();" name="submit_budget" id="submit_budget" class="btn btn-success mws-login-button">
      <span id="show-loader" style="display:none;"><img src="<?php echo base_url();?>assets/images/hourglass.gif" /></span>
      <br clear="all">
    </div>
  </form>
</div>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-0:+1",
		dateFormat:'yy-mm-dd',
		});
	});
</script>
<script>
function saveBudgetForm()
{
	$('#add_budget .req').removeClass('parsley-error');

	 var ht = '<ul>';
		$('#add_budget .req').each(function(index, element) {
			if($(this).val()=='')
			{
				$(this).addClass('parsley-error');
				ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
			}
		});
	  var redline = $('#add_budget .parsley-error').length;
	  ht += '</ul>';
	  if(redline <= 0)
	  {
		  var total_amount	=	$("#budget_amount").val();
		  var dept_count	=	0;
		  
		  $('.departments').each(function(index, element) {
		  	if($(this).val()!='')
			{
				dept_count += Number($(this).val());
			}
		  });
		  
		  if(total_amount > dept_count	|| total_amount == dept_count)
		  {
			  $('input[type="submit"]').attr('disabled','disabled');
		  
		  	  $("#show-loader").show();
		  
			  document.getElementById("add_budget").submit();
		  }
		  else
		  {
			  show_notification_error_end('Your Budget Amount is Less than your departments Budget.');
		  }
			
	  }
	  else
	  {	
	  	show_notification_error_end(ht);	
	  }
}
$( ".departments" ).keyup(function() {
	
  var total_amount	=	$("#budget_amount").val();
  var dept_count	=	0;
  
  $('.departments').each(function(index, element) {
	if($(this).val()!='')
	{
		dept_count += Number($(this).val());
		
		var remaining	=	(total_amount-dept_count);
		$("#dept-count").html(remaining);
	}
  });
  
  if(total_amount < dept_count)
  {
	  show_notification_error_end('Your Budget Amount is Less than your departments Budget.');
  }	
});
</script>
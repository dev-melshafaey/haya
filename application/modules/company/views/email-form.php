<div class="row" style="background-color:#FFF !important;direction:rtl !important;">
  <div class="col-md-12 form-group">
    <div class="tab-pane list-group active">
      <div class="col-md-12 form-group">
      <form class="mws-form" method="post" action="<?php echo current_url();?>" id="email_form" name="email_form">
        <div class="col-md-4 form-group">
          <label class="text-warning">البريد الإلكتروني :</label>
          <input type="text" class="form-control req" name="email" id="email" />
        </div>
        <div class="col-md-4 form-group">
        <label class="text-warning"></label>
          <input style="margin-top:25px !important;" type="button" value="حفظ" name="email_data" id="email_data" onclick="submitEmailForm();" class="btn btn-success" />
          <span id="show-loader"style="display:none;"><img src="<?php echo base_url();?>assets/images/hourglass.gif" /></span>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
function submitEmailForm()
{
	$('#email_form .req').removeClass('parsley-error');

	 var ht = '<ul>';
		$('#email_form .req').each(function(index, element) {
			if($(this).val()=='')
			{
				$(this).addClass('parsley-error');
				ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
			}
		});

	  var redline = $('#email_form .parsley-error').length;
	  ht += '</ul>';
	  
	  if(redline <= 0)
	  {
		  $('input[type="submit"]').attr('disabled','disabled');
		  
		  $("#show-loader").show();
		  
		  $("#email_form").submit();
	  }
}
</script>
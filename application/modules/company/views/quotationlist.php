<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="nav nav-tabs panel panel-default panel-block">
          <div class="tab-pane list-group active">
            <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th class="sorting">رقم</th>
                  <th class="sorting">عنوان العرض</th>
                  <th class="sorting">مجموع الشركات</th>
                  <th class="sorting">حالة العرض</th>
                  <th class="sorting">تاريخ الانتهاء</th>
                  <th class="sorting">الإجرائات</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'company/quotationlist/','columns_array'=>'
                { "data": "رقم" },
				{ "data": "عنوان العرض" },
				{ "data": "مجموع الشركات" },
				{ "data": "حالة العرض" },
				{ "data": "تاريخ الانتهاء" },
                { "data": "الإجرائات" }')); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
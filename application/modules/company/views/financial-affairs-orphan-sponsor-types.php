<!doctype html>
<?php $userid	=	$this->uri->segment(3);?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $this->load->view('common/user-tabs'); ?>
      <div class="col-md-12">
         <div class="col-md-2 main_body"> 
          <div class="col-md-12 center main_heading">
          <a href="<?php echo base_url();?>company/catch_receipt_form"> <?php echo 'سند قبض ';?></a> 
          <br/>
          </div>
        </div>
        <div class="col-md-2 main_body"> 
          <div class="col-md-12 center main_heading">
          <a href="<?php echo base_url();?>company/list_documents"> <?php echo 'قائمة اتسند قبض ';?></a> 
          <br/>
          </div>
          <div class="col-md-12 center"> <a class="main_count" href="#_">مجموع (<?php echo (isset($muhasaba['y']) ? $muhasaba['y'] : '0');?>)</a> </div>
        </div>
        <div class="col-md-2 main_body"> 
          <div class="col-md-12 center main_heading">
          <a href="<?php echo base_url();?>company/issuing_salaries"> <?php echo 'إصدار رواتب الايتام';?></a> 
          <br/>
          </div>
          <div class="col-md-12 center"> <a class="main_count" href="#_">مجموع (<?php echo (isset($total) ? $total : '0');?>)</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer');?>
</div>
</body>
</html>
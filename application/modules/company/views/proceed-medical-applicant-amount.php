<?php $decission = $this->inq->get_last_decission($medical_applicant->applicantid);?>
<div class="row col-md-12">
  <form class="mws-form" method="post" action="<?php echo base_url();?>company/add_medical_muhasba_amount" id="medical_applicant" name="medical_applicant">
    <div class="col-md-6 form-group">
      <label class="text-warning">الإسم :</label>
      <input name="fullname" value="<?PHP echo($medical_applicant->fullname); ?>" placeholder="الإسم" id="fullname" type="text" class="form-control" disabled="disabled">
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الرقم المدني :</label>
      <input name="fullname" value="<?PHP echo($medical_applicant->idcard_number); ?>" placeholder="الإسم" id="idcard_number" type="text" class="form-control" disabled="disabled">
    </div>
    <div class="col-md-6 form-group">
                <label class="text-warning">إسم المستفيد:</label>
                <input name="applicant_name" placeholder="إسم المستفيد" id="applicant_name" type="text" class="form-control req" value="<?php echo $decission->applicant_name;?>">
              </div>
              <div class="col-md-6 form-group">
                <label class="text-warning">المستلزمات الطبية:</label>
                <input name="medical_accessories" placeholder="المستلزمات الطبية" id="medical_accessories" type="text" class="form-control req" value="<?php echo $decission->medical_accessories;?>"></div>
                <div class="col-md-6 form-group">
                <label class="text-warning">اسم البنك:</label>
                <input name="applicant_bank" placeholder="المستلزمات الطبية" id="applicant_bank" type="text" class="form-control req" value="<?php echo $decission->applicant_bank;?>"></div>
                <div class="col-md-6 form-group">
                <label class="text-warning">رقم الحساب:</label>
                <input name="applicant_account_number" placeholder="المستلزمات الطبية" id="applicant_account_number" type="text" class="form-control req" value="<?php echo $decission->applicant_account_number;?>">
              </div>
              <div class="col-md-6 form-group">
      <label class="text-warning">كمية:</label>
      <input name="amount" value="<?PHP echo($decission->decission_amount); ?>" placeholder="الإسم" id="amount" type="text" class="form-control" readonly >
    </div>
    <div class="col-md-12">
      <h4>الميزانية / حزم التفاصيل:</h4>
      <!------------------------------------------------->
      <div id="show_sub_categories">
        <select name="search_category" class="parent form-control">
          <option value="" selected="selected">اختر الموازنة</option>
          <?php
                                if(!empty($all_budgets))
                                {
                                    foreach($all_budgets as $budget)
                                    {
                                        echo '<option value="'.$budget->bd_id.'" data-id="parent" >'.$budget->bd_year.'</option>';
                                    }
                                }
                                ?>
        </select>
      </div>
    </div>
    <br clear="all" />
    <br clear="all" />
    <div class="col-md-6 form-group">
      <label for="basic-input"></label>
      <input type="hidden" name="applicantid" id="applicantid" value="<?php echo $medical_applicant->applicantid;?>"/>
      <input type="hidden" name="branchid" id="branchid" value="<?php echo $medical_applicant->branchid;?>"/>
      <input type="hidden" name="decissionid" id="decissionid" value="<?php echo $decission->decissionid;?>"/>
      <input type="hidden" name="charity_type_id" id="charity_type_id" value="<?php echo $medical_applicant->charity_type_id;?>"/>
      <input type="hidden" name="account_balance" id="account_balance" value="00.000" />
      <input type="hidden" name="package_balance" id="package_balance" value="00.000" />
      <input type="button" value="مبلغ المتابعة" name="submit_medical" class="btn btn-success mws-login-button" onClick="amount_proceed();">
    </div> <div style="color:#F00;" id="alerts-medical"> </div >
    <br clear="all" />
  </form>
</div>
<?php $monthly_amount	=	config_item('monthly_amount');?>
<!doctype html>
<?php $userid	=	$this->uri->segment(3);?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<style>
h2{text-shadow: 0px -1px 0px #374683;text-shadow: 0px 1px 0px #e5e5ee;
filter: dropshadow(color=#e5e5ee,offX=0,offY=1); 
font-family:"Courier New", Courier, monospace; margin:0px;}
</style>
<style>
.both h4{ font-family:Arial, Helvetica, sans-serif; margin:0px; font-size:14px;}
#search_category_id{ padding:3px; width:200px;}

.parent{ padding:3px;float:right; margin-right:12px;width: 48%;  margin-top: 8px;}
.bank-parent{ padding:3px;float:right; margin-right:12px;width: 48%;  margin-top: 8px;}
.both{ float:left; margin:0 0px 0 0; padding:0px;}
</style>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $this->load->view('common/user-tabs'); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div class="panel-heading text-overflow-hidden">
                  
                  <div class="col-md-12">
                    <div class="row">
                      <?php if(!empty($muhasaba_type_listing)):?>
                      <form action="<?php echo base_url();?>company/add_muhasaba_types_amount" method="post" name="muhasaba_type_amount" id="muhasaba_type_amount">
                        <table class="table table-bordered table-striped" aria-describedby="tableSortable_info">
                          <thead>
                            <tr role="row">
                              <th style="text-align:center;">رقم</th>
                              <th style="text-align:center;">الإسم</th>
                              <th style="text-align:center;">البطاقة الشخصة</th>
                              <th style="text-align:center;">المستلزمات الطبية</th>
                              <th style="text-align:center;">إسم المستفيد</th>
                              <th style="text-align:center;">اسم البنك</th>
                              <th style="text-align:center;">رقم الحساب</th>
                              <th style="text-align:center;">كمية</th>
                              <th style="text-align:center;">صرف</th>
                            </tr>
                          </thead>
                          <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php foreach($muhasaba_type_listing	as $listing):?>
                            <?php $applicant_ids[$decission->decission_amount]	=	$listing->applicantid;?>
                            <?php $decission = $this->inq->get_last_decission($listing->applicantid);?>
                            <?php $total_amount	+= $decission->decission_amount;?>
                            <tr role="row" id="<?php echo $listing->applicantid;?>_durar_lm">
                              <td  style="text-align:center;"><?php echo $listing->applicantcode;?></td>
                              <td  style="text-align:center;"><?php echo $listing->fullname;?></td>
                              <td  style="text-align:center;"><?php echo $listing->idcard_number;?></td>
                              <td  style="text-align:center;"><?php echo $decission->medical_accessories;?></td>
                              <td  style="text-align:center;"><?php echo $decission->applicant_name;?></td>
                              <td  style="text-align:center;"><?php echo $decission->applicant_bank;?></td>
                              <td  style="text-align:center;"><?php echo $decission->applicant_account_number;?></td>
                              <td  style="text-align:center;"><?php echo number_format($decission->decission_amount,3);?></td>
                              <td  style="text-align:center;">
                              	<a href="#addingDiag" onClick="alatadad(this);" data-url="<?php echo base_url();?>company/proceed_amount/<?php echo $listing->applicantid;?>">
                                	<i style="color:#060;"class="myicon icon-external-link"></i>
                                </a>
                              </td>
                            </tr>
                          <input type="hidden" name="data[<?php echo $listing->applicantid ?>]" id="data" value="<?php echo $decission->decission_amount;?>"/>
                          
                          <?php endforeach;?>
                          <?php $applicant_ids	=	implode(',',$applicant_ids);?>
                            </tbody>
                        </table>
                      </form>
                      <?php else:?>
                      	<div><strong><?php echo 'سجل قدمت بالفعل';?></strong></div>
                      <?php endif;?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer');?>
<script>
        $(document).ready(function() {

        $('.parent').livequery('change', function() {

            $(this).nextAll('.parent').remove();
			$(this).nextAll('.bank-parent').remove();
            $(this).nextAll('label').remove();
			$("#alerts").html('');

            $('#show_sub_categories').append('<img src="<?php echo base_url();?>assets/images/hourglass.gif" style="float:right; margin-top:7px;" id="loader" alt="" />');
            $.post(config.BASE_URL+'company/get_child_categories', {
				type: $(this).find(':selected').attr('data-id'),
                parent_id: $(this).val(),
            }, function(response){
                setTimeout("finishAjax('show_sub_categories', '"+escape(response)+"')", 400);
            });
            return false;
        });
		
    });

    function finishAjax(id, response)
	{
      $('#loader').remove();
	  
      $('#'+id).append(unescape(response));
	  
    }

/***************************************************************/
/***************************************************************/

function get_detail()
{
	var local_bank_id	=	$(".bank-parent").val();
	var account_balance	=	$(".bank-parent").find('option:selected').attr('account-balance');
	var package_balance	=	$(".bank-parent").find('option:selected').attr('package-balance');
	
	// SET VALUES into hidden fields
	$("#account_balance").val(account_balance);
	$("#package_balance").val(package_balance);

}
function amount_proceed()
{
	

	var account_balance			=	$("#account_balance").val();
	var package_balance			=	$("#package_balance").val();
	var total_appl_amount		=	$("#amount").val();
	
	account_balance 		=	parseFloat(account_balance.replace(/,/g, ''));
	package_balance 		=	parseFloat(package_balance.replace(/,/g, ''));
	total_appl_amount 		=	parseFloat(total_appl_amount.replace(/,/g, ''));
	
	if(parseInt(account_balance)	<	parseInt(total_appl_amount))
	{
		$("#alerts-medical").html('<strong>There is less balance in Local Account!</strong>');
	}
	else if(parseInt(package_balance)	<	parseInt(total_appl_amount))
	{
		$("#alerts-medical").html('<strong>There is less amount in your Package!</strong>');
	}
	else
	{
		document.getElementById("medical_applicant").submit(); // Submit From JAVASCRIPT
	}

}
</script>
</div>
</body>
</html>
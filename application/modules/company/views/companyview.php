<div class="row" style="background-color:#FFF !important; ">
  <div class="col-md-12 form-group">
    <div class="tab-pane list-group active">
      <div class="col-md-12 form-group" style=" text-align: center; font-size: 25px; font-weight: bold; margin-top: 17px; border-bottom: 1px solid #eee;"> <?PHP echo($company->arabic_name); ?><br>
        <?PHP echo($company->english_name); ?> </div>
      <div class="col-md-8 form-group" style="margin-top: 10px; border-left: 1px solid #EFEFEF;">
        <div class="col-md-12 form-group">
          <label class="text-warning">نوع الشركة :</label>
          <br>
          <?PHP echo $this->haya_model->get_name_from_list($company->company_type_listmanagement); ?> </div>
        <div class="col-md-6 form-group">
          <label class="text-warning">رقم سجل القوى العاملة :</label>
          <br>
          <?PHP echo(arabic_date($company->cr_number)); ?> </div>
        <div class="col-md-6 form-group">
          <label class="text-warning">تاريخ الانتهاء :</label>
          <br>
          <?PHP echo(arabic_date($company->cr_expiry)); ?> </div>
        <div class="col-md-6 form-group">
          <label class="text-warning">المحافظة :</label>
          <br>
          <?PHP echo $this->haya_model->get_name_from_list($company->province); ?> </div>
        <div class="col-md-6 form-group">
          <label class="text-warning">الولاية :</label>
          <br>
          <?PHP echo $this->haya_model->get_name_from_list($company->wilaya); ?> </div>
        <div class="col-md-12 form-group">
          <label class="text-warning">العنوان بالتفصيل :</label>
          <br>
          <?PHP echo $company->fulladdress; ?> </div>
        <div class="col-md-12 form-group">
          <label class="text-warning">الحالة :</label>
          <br>
          <?PHP echo company_status('companystatus',$company->companystatus,1); ?> </div>
      </div>
      <div class="col-md-4 form-group" style="margin-top: 10px;">
        <div class="col-md-12 form-group">
          <label class="text-warning">الشخص الذي يمكن الاتصال به :</label>
          <br>
          <?PHP echo($company->contact_person); ?> </div>
        <div class="col-md-12 form-group">
          <label class="text-warning">البريد الإلكتروني :</label>
          <br>
          <?PHP echo($company->email_address); ?> </div>
        <div class="col-md-12 form-group">
          <label class="text-warning">نوع البطاقة :</label>
          <br>
          <?PHP echo card_type_id('contact_person_id_type',$company->contact_person_id_type,1); ?> </div>
        <div class="col-md-12 form-group">
          <label class="text-warning">رقم البطاقة :</label>
          <br>
          <?PHP echo(arabic_date($company->contact_person_id)); ?> </div>
        <div class="col-md-12 form-group">
          <label class="text-warning">بطاقة انتهاء الصلاحية :</label>
          <br>
          <?PHP echo(arabic_date($company->contact_person_expiry)); ?> </div>
        <div class="col-md-12 form-group">
          <label class="text-warning">الهاتف النقال :</label>
          <br>
          <?PHP echo(arabic_date($company->mobile_number)); ?> </div>
        <div class="col-md-6 form-group">
          <label class="text-warning">رقم الهاتف :</label>
          <br>
          <?PHP echo(arabic_date($company->phone_number)); ?> </div>
        <div class="col-md-6 form-group">
          <label class="text-warning">رقم الهاتف :</label>
          <br>
          <?PHP echo(arabic_date($company->phone_number_two)); ?> </div>
        <div class="col-md-6 form-group">
          <label class="text-warning">ص.ب :</label>
          <br>
          <?PHP echo(arabic_date($company->postboxnumber)); ?> </div>
        <div class="col-md-6 form-group">
          <label class="text-warning">ر.ب :</label>
          <br>
          <?PHP echo(arabic_date($company->postcode)); ?> </div>
      </div>
    </div>
  </div>
</div>

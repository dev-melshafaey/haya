<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="nav nav-tabs panel panel-default panel-block">
          <div class="tab-pane list-group active">
            <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th style="text-align:center;">اسم البنك</th>
                  <th style="text-align:center;">عدد الحالات</th>
                  <th style="text-align:center;">المبلغ</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?php if(!empty($all_banks)):?>
                <?php foreach($all_banks as $bank):?>
                <?php
				   // Get SUM of  DEBIT 
				   $account_debit	=	$this->outside->total_amount_in_accounts($bank->list_id,'DEBIT');
				   
				   // Get SUM of CREDIT 
				   $credit_debit	=	$this->outside->total_amount_in_accounts($bank->list_id,'CREDIT'); 
				   
				  // GET total SUM of Single Account
				  $total_amount_in_account	=	($bank->amount_in_account	+	$account_debit	-	$credit_debit); 
				   
				  $total_applicants	=	$this->company->get_total_app_in_account($type_id,$bank->bankid,$bank->branchid);
				?>
                <tr role="row" id="<?php echo $bank->bankid;?>_durar_lm">
                  <td  style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($bank->list_id);?></td>
                  <td  style="text-align:center;">
                  <a href="<?php echo base_url();?>company/orphan_applicants_listing/<?php echo $orphan_status?>/<?php echo $country_status; ?>/<?php echo $branchid; ?>/<?php echo $bank->list_id; ?>"><?php echo '( '.$bank->total.' )';?>
                  </a>
                  </td>
                  <td  style="text-align:center;"><?php echo number_format($total_applicants['total_amount'],3);?></td>
                </tr>
                <?php endforeach;?>
                <?php endif;?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>

<!-- /.modal-dialog -->
</div>
</body>
</html>
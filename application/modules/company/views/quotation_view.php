<div class="row col-md-12">
  <form class="mws-form" method="post" action="<?PHP base_url(); ?>company/saveQuotation" enctype="multipart/form-data" id="add_quotation" name="add_quotation">
    <div class="col-md-12 form-group">
      <label class="text-warning">شركة :</label>
      <br>
      <?PHP if($companyid!='' && $companyid!=0) { ?>
      <input type="hidden" class="req" name="companyid" placeholder="اختر شركة" id="companyid" value="<?PHP echo $companyid; ?>">
      <strong><?PHP echo $company->english_name.' | '.$company->arabic_name; ?> (<?PHP echo arabic_date($company->cr_number); ?>)</strong>
      <?PHP } else { ?>
      <select class="form-control req"  placeholder="اختر شركة" name="companyid" id="companyid">
        <option value="">اختر شركة</option>
        <?PHP foreach($company as $cgroup) { ?>
        <option value="<?PHP echo $cgroup->companyid; ?>"><?PHP echo $cgroup->arabic_name.' | '.$cgroup->english_name; ?> (<?PHP echo arabic_date($cgroup->cr_number); ?>)</option>
        <?PHP } ?>
      </select>
      <?PHP } ?>
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">عنوان العرض :</label>
      <input type="text" class="form-control req"  placeholder="عنوان العرض" name="quote_title" id="quote_title" value="<?php echo $quote->quote_title;?>">
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">تفاصيل العرض :</label>
      <textarea name="quote_detail" id="quote_detail"  placeholder="تفاصيل العرض" class="form-control req"><?php echo $quote->quote_detail;?></textarea>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">حالة العرض :</label>
      <?PHP $this->haya_model->enum_dropbox('quote_status',$quote->quote_status,'ah_quote','quote_status','الحالة العرض'); ?>
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">خصم العرض :</label>
      <input type="number" class="form-control req NumberInput"  placeholder="خصم العرض" name="quote_discount" id="quote_discount" value="<?php echo $postal_code;?>">
    </div>
    <div class="col-md-3 form-group">
      <label class="text-warning">نوع الخصم :</label>
      <?PHP $this->haya_model->enum_dropbox('quote_discount_type',$quote->quote_discount_type,'ah_quote','quote_discount_type','نوع خصم'); ?>
    </div>
    <div class="form-group col-md-3">
       <label class="text-warning">مورفقات :</label>
       <input class="form-control req" type="file" name="quote_attachment" id="quote_attachment" placeholder="مورفقات" title='مورفقات'>
    </div>
    <br clear="all">
    <div class="col-md-12 form-group newboxxxx" id="afterme">
      <input type="button" value="حفظ العرض" style="float:left !important;" onClick="saveQuotationForm();" name="submit" class="btn btn-success mws-login-button zeropoint">
      <input type="button" value="إضافة عنصر" name="ontheway" onClick="addItembox();" class="btn btn-warning">
      <br clear="all">
    </div>
 
 	<div class="col-md-4 form-group mytotalbox">
      <label class="text-warning">المجموع :</label>
      <input type="text" class="form-control" id="total_x" value="0" readonly>
    </div>
 <div class="col-md-4 form-group mytotalbox">
      <label class="text-warning">المجموع العرض :</label>
      <input type="text" class="form-control" id="discount_x" value="0" readonly>
    </div>
    <div class="col-md-4 form-group mytotalbox">
      <label class="text-warning">السعر الإجمال :</label>
     <input type="text" class="form-control" id="grand_total" value="0" readonly>
    </div>
 
  </form>
</div>
<script>
function addItembox()
{
	var cnt = 0;
	var ht = '<ul>';
	$('#add_quotation .req').each(function(index, element) {
		if($(this).val()=='')
		{
			cnt++;
			ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
		}
	});
	ht += '</ul>';
	if(cnt <= 0)
	{
		var newNum = Math.floor((Math.random() * 9990000121000) + 1);
		var taurusData = $.ajax({			
			url: config.BASE_URL+'company/item_box',
			type:"POST",
			dataType:"html",
			data:{num:newNum},			
			success: function(msg)
			{	$('#afterme').after(msg);	
				itemscollectionCount();
			}
		});
	}
	else
	{	show_notification_error_end(ht);	}
	
}

function saveQuotationForm()
{
	$('#add_quotation .req').each(function(index, element) {
		if($(this).val()=='')
		{
			cnt++;
			ht += '<li> طلب '+$(this).attr('placeholder')+'</li>';
		}
	});
	ht += '</ul>';
	if(cnt <= 0)
	{
		$('#add_quotation').submit();
	}
	else
	{	show_notification_error_end(ht);	}
}

function removeItembox(id)
{
	$('#heartdisk_'+id).remove();
	itemscollectionCount();
	
}

function getall_list_child(obj_id,obj)
{
	
	var taurusData = $.ajax({			
			url: config.BASE_URL+'company/child_item',
			type:"POST",
			dataType:"html",
			data:{id:$(obj).val()},			
			success: function(msg)
			{	$('#list_child_id_'+obj_id).html(msg);	
			}
		});
}

function itemscollectionCount()
{
	var lenof = $('.itemscollection').length;
	if(lenof > 0)
	{	$('.zeropoint').show();	
	$('.mytotalbox').show();
		}
	else
	{	$('.zeropoint').hide();
	$('.mytotalbox').hide();	}
}

function calculateAmountFor(object_id,object)
{
	var item_qty = $('#item_qty_'+object_id).val();
	var item_price = $('#item_price_'+object_id).val();
	var total_box = '#total_'+object_id;
	var discount_value = $('#quote_discount').val();
	var quote_discount_type = $('#quote_discount_type').val();
	var total = 0;
	if(item_qty!='' && item_price!='')
	{	$(total_box).val(parseInt(item_qty)*parseInt(item_price));	}
	
	$('.fulltotal').each(function(index, element) {
        total += parseInt($(this).val());
    });
	
	$('#total_x').val(total);
	
	if(quote_discount_type=='نسبة مئوية')
	{	
		var totalvalue = (total*discount_value)/100;
		$('#discount_x').val(totalvalue);
		$('#grand_total').val(parseInt(total)-parseInt(totalvalue));	}
	else
	{	$('#grand_total').val(parseInt(total)-parseInt(discount_value));
		$('#discount_x').val(discount_value);
	}
	
}
</script>
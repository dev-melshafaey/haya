<?php $monthly_amount	=	config_item('monthly_amount');?>
<?php			
   // Get SUM of  DEBIT 
   $account_debit	=	$this->outside->total_amount_in_accounts($bankid,'DEBIT');
   
   // Get SUM of CREDIT 
   $credit_debit	=	$this->outside->total_amount_in_accounts($bankid,'CREDIT'); 
   
  // GET total SUM of Single Account
  //$total_amount_in_account	=	($amount_in_alhaya_bank	+	$account_debit	-	$credit_debit); 
  
  $total_amount_in_account  = 	$this->haya_model->get_total_alhaya_actual_amount($bankid);
				   
?>
<!doctype html>
<?php $userid	=	$this->uri->segment(3);?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<style>
h2{text-shadow: 0px -1px 0px #374683;text-shadow: 0px 1px 0px #e5e5ee;
filter: dropshadow(color=#e5e5ee,offX=0,offY=1); 
font-family:"Courier New", Courier, monospace; margin:0px;}
</style>
<style>
.both h4{ font-family:Arial, Helvetica, sans-serif; margin:0px; font-size:14px;}
#search_category_id{ padding:3px; width:200px;}

.parent{ padding:3px;float:right; margin-right:12px;width: 32%;  margin-top: 8px;}
.bank-parent{ padding:3px;float:right; margin-right:12px;width: 32%;  margin-top: 8px;}
.both{ float:left; margin:0 0px 0 0; padding:0px;}
</style>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $this->load->view('common/user-tabs'); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div class="panel-heading text-overflow-hidden">
                  <div><strong>المبلغ الشهري : <label class="text-warning"><?php echo number_format($monthly_amount,3);?></label></strong>
                  <span style="text-align:left; margin-right:330px;"><strong><?php echo $this->haya_model->get_name_from_list($bankid);?>  &nbsp; :<label class="text-warning"> <?php echo $total_amount_in_account;?></label></strong></span>
                  </div>
                  
                  <div class="col-md-12">
                    <div class="row">
                      <?php if(!empty($muhasaba_type_listing)):?>
                      <form action="<?php echo base_url();?>company/add_muhasaba_types_amount" method="post" name="muhasaba_type_amount" id="muhasaba_type_amount">
                        <table class="table table-bordered table-striped" aria-describedby="tableSortable_info">
                          <thead>
                            <tr role="row">
                              <th style="text-align:center;">رقم</th>
                              <th style="text-align:center;">الإسم</th>
                              <th style="text-align:center;">نوع المساعدات</th>
                              <th style="text-align:center;">فرع</th>
                              <th style="text-align:center;">البطاقة الشخصة</th>
                              <th style="text-align:center;">اسم البنك</th>
                              <th style="text-align:center;">الفرع</th>
                              <th style="text-align:center;">رقم الحساب</th>
                              <th style="text-align:center;">تاريخ اللجنة</th>
                              <th style="text-align:center;">كمية</th>
                            </tr>
                          </thead>
                          <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php foreach($muhasaba_type_listing	as $listing):?>
                            <?php $applicant_ids[$decission->decission_amount]	=	$listing->applicantid;?>
                            <?php $decission = $this->inq->get_last_decission($listing->applicantid);?>
                            <?php $total_amount	+= $decission->decission_amount;?>
                            <tr role="row" id="<?php echo $listing->applicantid;?>_durar_lm">
                              <td  style="text-align:center;"><?php echo $listing->applicantcode;?></td>
                              <td  style="text-align:center;"><?php echo $listing->fullname;?></td>
                              <td  style="text-align:center;"><?php echo $listing->charity_type;?></td>
                              <td  style="text-align:center;"><?php echo $listing->branchname;?></td>
                              <td  style="text-align:center;"><?php echo $listing->idcard_number;?></td>
                              <td  style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($listing->bankid);?></td>
                              <td  style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($listing->bankbranchid);?></td>
                              <td  style="text-align:center;"><?php echo $listing->accountnumber;?></td>
                              <td  style="text-align:center;"><?php echo $decission->decissiontime;?></td>
                              <td  style="text-align:center;"><?php echo number_format($decission->decission_amount,3);?></td>
                            </tr>
                          <input type="hidden" name="data[<?php echo $listing->applicantid ?>]" id="data" value="<?php echo $decission->decission_amount;?>"/>
                          
                          <?php endforeach;?>
                          <?php $applicant_ids	=	implode(',',$applicant_ids);?>
                          <tr role="row">
                            <td colspan="9" style="text-align:center;"><strong>مجموع</strong></td>
                            <td style="text-align:center;"><strong><?php echo number_format($total_amount,3);?></strong></td>
                          </tr>
                            </tbody>
                          
                        </table>
                        <div class="col-md-12">
                          <h4>الميزانية / حزم التفاصيل:</h4>
                          <!------------------------------------------------->
                          <div id="show_sub_categories">
                            <select name="search_category" class="parent form-control">
                              <option value="" selected="selected">اختر الموازنة</option>
								<?php
                                if(!empty($all_budgets))
                                {
                                    foreach($all_budgets as $budget)
                                    {
                                        echo '<option value="'.$budget->bd_id.'" data-id="parent" >'.$budget->bd_year.'</option>';
                                    }
                                }
                                ?>
                            </select>

                          </div>
                          <!-------------------------------------------------> 
                          <br clear="all">
                          <br clear="all">
                          <?php if($total_amount	<=	$monthly_amount):?>
                          <?php if($total_amount	>	0):?>
                          <div class="col-md-4 form-group">
							<input type="hidden" name="charity_type_id" id="charity_type_id" value="<?php echo $charity_type_id;?>" />
                            <input type="hidden" name="monthly_amount" id="monthly_amount" value="<?php echo number_format($monthly_amount,3);?>" />
                            <input type="hidden" name="total_appl_amount" id="total_appl_amount" value="<?php echo number_format($total_amount,3);?>" />
                            <input type="hidden" name="account_balance" id="account_balance" value="00.000" />
                            <input type="hidden" name="package_balance" id="package_balance" value="00.000" />
                            <input type="hidden" name="bankid" id="bankid" value="<?php echo $bankid;?>" />
                            
                            <input type="hidden" name="alhaya_actual_amount" id="alhaya_actual_amount" value="<?php echo $total_amount_in_account;?>" />
							<input class="btn btn-success btn-lrg" type="button" name="submitProcess" id="submitProcess" value="Submit" onClick="submit_process();"/>
                          </div>
                          <div style="color:#F00;" id="alerts"> </div >
                          <?php endif;?>
                          <?php endif;?>
                        </div>
                      </form>
                      <?php else:?>
                      <div><strong><?php echo 'سجل قدمت بالفعل';?></strong></div>
                      <?php endif;?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer');?>
<script>
        $(document).ready(function() {

        $('.parent').livequery('change', function() {

            $(this).nextAll('.parent').remove();
			$(this).nextAll('.bank-parent').remove();
            $(this).nextAll('label').remove();
			$("#alerts").html('');

            $('#show_sub_categories').append('<img src="<?php echo base_url();?>assets/images/hourglass.gif" style="float:right; margin-top:7px;" id="loader" alt="" />');
            $.post(config.BASE_URL+'company/get_child_categories', {
				type: $(this).find(':selected').attr('data-id'),
                parent_id: $(this).val(),
            }, function(response){
                setTimeout("finishAjax('show_sub_categories', '"+escape(response)+"')", 400);
            });
            return false;
        });
		
    });

    function finishAjax(id, response)
	{
      $('#loader').remove();
	  
      $('#'+id).append(unescape(response));
	  
    }

/***************************************************************/
/***************************************************************/
function categories_uuu(vals,id){

	$('#'+id).empty();
	$('#'+id).append('<option value="">--SELECT--</option>');
	$.ajax({
		url: config.BASE_URL+'company/loadcategories',
		type: "POST",
		data:{'vals':vals,'type':id },
		dataType: "json",
		success: function(data)
		{
			$('#show-start').append(data);
		}
	});
}
function categories_yyy(vals1,id1)
{
	//var docid = $(x).attr('data-id');
	var taurusData = $.ajax({			
			url: config.BASE_URL+'company/loadcategories_2',
			type:"POST",
			dataType:"json",
			data:{docid:vals1},
			beforeSend: function() {},
			success: function(msg)
			{
				//$('#removeicons'+docid).remove();	
				console.log('msg');		
			}
		});
}

function get_detail()
{
	var local_bank_id	=	$(".bank-parent").val();
	var account_balance	=	$(".bank-parent").find('option:selected').attr('account-balance');
	var package_balance	=	$(".bank-parent").find('option:selected').attr('package-balance');
	
	// SET VALUES into hidden fields
	$("#account_balance").val(account_balance);
	$("#package_balance").val(package_balance);
	
	//console.log('bankid ='+ local_bank_id + ' account balance' + account_balance + ' package_balance' + package_balance);
}
function submit_process()
{
	var account_balance			=	$("#account_balance").val();
	var package_balance			=	$("#package_balance").val();
	var alhaya_actual_amount	=	$("#alhaya_actual_amount").val();
	var monthly_amount			=	$("#monthly_amount").val();
	var total_appl_amount		=	$("#total_appl_amount").val();
	
	account_balance 		=	parseFloat(account_balance.replace(/,/g, ''));
	package_balance 		=	parseFloat(package_balance.replace(/,/g, ''));
	alhaya_actual_amount 	=	parseFloat(alhaya_actual_amount.replace(/,/g, ''));
	monthly_amount 			=	parseFloat(monthly_amount.replace(/,/g, ''));
	total_appl_amount 		=	parseFloat(total_appl_amount.replace(/,/g, ''));
	

	if(parseInt(total_appl_amount)	<=	parseInt(monthly_amount))
	{
		if(parseInt(alhaya_actual_amount)	<	parseInt(total_appl_amount))
		{
			$("#alerts").html('<strong>There is less balance in Al Haya Account!</strong>');
		}
		else if(parseInt(account_balance)	<	parseInt(total_appl_amount))
		{
			$("#alerts").html('<strong>There is less balance in Local Account!</strong>');
		}
		else if(parseInt(package_balance)	<	parseInt(total_appl_amount))
		{
			$("#alerts").html('<strong>There is less amount in your Package!</strong>');
		}
		else
		{
			document.getElementById("muhasaba_type_amount").submit(); // Submit From JAVASCRIPT
		}
	}
	else
	{
		$("#alerts").html('<strong>Total amount is greater than Monthly Amount!</strong>');
	}
}
</script>
</div>
</body>
</html>
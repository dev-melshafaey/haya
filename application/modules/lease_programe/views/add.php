<?php $this->load->view('common/meta');?>

<div class="body">
<?php $this->load->view('common/banner');?>
<div class="body_contant">
  <?php $this->load->view('common/floatingmenu');?>
  <?PHP parentMenu(); ?>
  <div class="main_contant">
    <!--<div class="shortcuts">
      <div class="short_cut_item"> <a href="departments_view.html">الأقسام</a></div>
      <div class="short_cut_item"> <a href="questions_view.html">الأسئلة</a></div>
      <div class="short_cut_item"> <a href="schedule_view.html">المتسابقين</a></div>
    </div>-->
    <div class="data_raw">
      <div class="main_box">
        <div class="data_box_title">
          <!--<div class="data_box_title_icon"><img src="images/menu/question_s.png" width="22" height="20" /></div>-->
          <div class="data_title">إضافة الفروع</div>
          <!--<div class="page_controls">
            <div class="page_control"><a href="#"><img src="images/body/contant/refresh.png" width="28" height="26"  border="0" /></a></div>
            <div class="page_control"><a href="#"><img src="images/body/contant/back.png" width="28" height="26" border="0" /></a></div>
          </div>-->
        </div>

        <div class="data">
          <div class="main_data">
            <form action="<?php echo current_url();?>" method="post" id="validate_form" name="validate_form" autocomplete="off">
              <div class="form_raw">
                <div class="form_txt">اختار</div>
                <div class="form_field_selected">
                 	<?php echo loan_category('loan_category_id',$single_loan->loan_category_id,0);?>
                </div>
              </div>
              <div class="form_raw" id="child_parent" style="display:none;">
                <div class="form_txt">اختار</div>
                <div class="form_field_selected" id="get_child_roles">
                </div>
              </div>
              <div class="form_raw">
                <div class="form_txt">من</div>
                <div class="form_field">
                  <input type="text" class="txt_field req NumberInput" name="loan_start_amount"  id="loan_start_amount" value="<?php echo (isset($single_loan->loan_start_amount) ? $single_loan->loan_start_amount : NULL);?>" placeholder="من"/>
                </div>
                <div class="form_field"></div>
                <div class="form_txt" style="margin-left: -45px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;إلى</div>
                <div class="form_field">
                  <input type="text" class="txt_field req NumberInput" name="loan_end_amount"  id="loan_end_amount" value="<?php echo (isset($single_loan->loan_end_amount) ? $single_loan->loan_end_amount : NULL);?>" placeholder="إلى"/>
                </div>
              </div>
              
              <div class="form_raw">
                <div class="form_txt">نسبة مئوية</div>
                <div class="form_field">
                  <input type="text" class="txt_field req NumberInput" name="loan_percentage"  id="loan_percentage" value="<?php echo (isset($single_loan->loan_percentage) ? $single_loan->loan_percentage : NULL);?>" placeholder="نسبة مئوية"/>
                </div>
              </div>
              <div class="form_raw">
                <div class="form_txt">فترة السماح‎</div>
                <div class="form_field">
                  <input style="width:50px !important;" type="text" class="txt_field req NumberInput" name="loan_starting_day"  id="loan_starting_day" value="<?php echo (isset($single_loan->loan_starting_day) ? $single_loan->loan_starting_day : NULL);?>" placeholder="فترة السماح‎"/>
                </div>

                <div class="form_field">
                 <select name="loan_start_timeperiod" id="loan_start_timeperiod" style="width: 58px !important;height: 36px;background-color: #F7F7F7;">
                 	<option value="day" <?php if($single_loan->loan_start_timeperiod == 'day'):?> selected="selected" <?php endif;?>>يوم</option>
                    <option value="month" <?php if($single_loan->loan_start_timeperiod == 'month'):?> selected="selected" <?php endif;?>>شهر</option>
                    <option value="year" <?php if($single_loan->loan_start_timeperiod == 'year'):?> selected="selected" <?php endif;?>>عام</option>
                 </select>
                </div>
              </div>
              <div class="form_raw">
                <div class="form_txt">حتى</div>
                <div class="form_field">
                  <input style="width:50px !important;" type="text" class="txt_field req NumberInput" name="loan_expire_day"  id="loan_expire_day" value="<?php echo (isset($single_loan->loan_expire_day) ? $single_loan->loan_expire_day : NULL);?>" placeholder="فرع كود"/>
                </div>

                <div class="form_field">
                 <select name="loan_expire_timeperiod" id="loan_expire_timeperiod" style="width: 58px !important;height: 36px;background-color: #F7F7F7;">
                 	<option value="day" <?php if($single_loan->loan_expire_timeperiod == 'day'):?> selected="selected" <?php endif;?>>يوم</option>
                    <option value="month" <?php if($single_loan->loan_expire_timeperiod == 'month'):?> selected="selected" <?php endif;?>>شهر</option>
                    <option value="year" <?php if($single_loan->loan_expire_timeperiod == 'year'):?> selected="selected" <?php endif;?>>عام</option>
                 </select>
                </div>
              </div>
              <div class="main_withoutbg">
                <div class="add_question_btn">
                <?php if($loan_caculate_id):?>
                	<input type="hidden" name="loan_caculate_id"  value="<?php echo $loan_caculate_id;?>" />
                <?php endif;?>
                <input type="button" name="save_data_form" id="save_data_form" class="transperant_btn"  value="حفظ" />
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('common/footer');?>

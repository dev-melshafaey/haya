<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<?php $segment = $this->uri->segment(3); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
 
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>

      <div class="col-md-12" style="background-color:#FFF !important; margin-top: 2px !important; padding-top:16px !important;">


			<!--  <div class="col-md-1 form_bold">الاقسام:</div>
			  <div class="col-md-3">
				<select name="sections" id="form_status" class="form-control">
				  <option value="users" <?php if($power->sections	==	'users'):?> selected="selected" <?php endif;?>>فقط لقسم المستخدمين</option>
				  <option value="all" <?php if($power->sections	==	'all'):?> selected="selected" <?php endif;?>>للجميع</option>
				</select>				
			  </div> -->

          <form name="frmbuild" action="<?PHP echo base_url(); ?>lease_programe/formsave" id="frmbuild" method="post" autocomplete="off">			  
			  <input type="hidden" name="module_type" id="module_type" value="<?php echo (isset($power->module_type) ? $power->module_type : 'popup');?>" />
			  <?php
			    if($power->moduleid)
				{
				  echo('<input type="hidden" id="moduleid" name="moduleid" value="'.$power->moduleid.'">');
				}
				
			  ?>
<!--             <div class="col-md-1 form_bold">النوع:</div>
			  <div class="col-md-3">
				<select name="module_type" id="module_type" class="form-control">
				  <option value="popup" <?php if($power->module_type	==	'popup'):?> selected="selected" <?php endif;?>>نافذة منبثقه</option>
				  <option value="redirect" <?php if($power->module_type	==	'redirect'):?> selected="selected" <?php endif;?>>نافذة عادية</option>
				</select>				
			  </div>
              <br clear="all"/>
              <br clear="all"/>-->
			  <div class="col-md-1 form_bold">اسم استمارة:</div>
			  <div class="col-md-3"><input value="<?PHP echo $power->module_name; ?>" type="text" placeholder="اسم استمارة" class="form-control req" name="form_name" id="form_name"></div>
			  <div class="col-md-1 form_bold">التفاصيل:</div>
			  <div class="col-md-3"><input value="<?PHP echo $power->module_text; ?>" type="text" placeholder="التفاصيل" class="form-control req" name="form_description" id="form_description"></div>
			  
			  <div class="col-md-1 form_bold">وحدة الأم:</div>
			  <div class="col-md-3"><?PHP $this->haya_model->module_dropbox('form_module',$power->module_parent); ?></div>
			   <br style="clear: both;"><br style="clear: both;">
			  <div class="col-md-1 form_bold">وحدة أيقونة:</div>
			  <div class="col-md-3"><input value="<?PHP echo $power->module_icon; ?>"  type="text" placeholder="وحدة أيقونة" name="form_icon" id="form_icon" class="form-control req"></div>
			 
			  <div class="col-md-1 form_bold">الحالة:</div>
			  <div class="col-md-3">
				<select name="form_status" id="form_status" class="form-control">
				  <option value="A" <?php if($power->module_status	==	'A'):?> selected="selected" <?php endif;?>>نشط</option>
				  <option value="D" <?php if($power->module_status	==	'D'):?> selected="selected" <?php endif;?>>دي نشط</option>
				</select>				
			  </div>
			  <div class="col-md-1 form_bold">ترتيب:</div>
			  <div class="col-md-3">
				<?php number_drop_box('form_order',$power->module_order); ?>				
			  </div>
			  
			  <div class="col-md-1 form_margin"></div>
			  <div class="col-md-11 form_margin" id="addmyfield"><button class="btn btn-success" type="button" name="form_button" id="form_button">إضافة حقل جديد</button></div>
			  
			  <?php $custom_form	=	json_decode($power->custom_form, TRUE);
					$form_size		=	sizeof($custom_form['data']);
					
					$xx = '';
				    foreach($custom_form['data'] as $freemason => $darkmoon) {
						$power_of_freemason = array('key'=>$freemason,'d'=>$darkmoon);
						$this->load->view('form',$power_of_freemason);
					  ?>
			  <?php } ?>
			  
			  <div class="col-md-12" id="savebar" style="text-align: right; <?PHP if($form_size <= 0) { ?>display: none;<?PHP } ?>"><button class="btn btn-success" type="button" name="form_save" id="form_save"><i></i> حفظ</button></div>
		  </form>
		  <br style="clear: both;">
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
function set_type(e)
{
	var moduleid	=	$(e).val();
	
	if(moduleid	==	'47')
	{
		$("#module_type").val('popup');	
	}
	else
	{
		$("#module_type").val('others');
	}
}
</script>
</body>
</html>
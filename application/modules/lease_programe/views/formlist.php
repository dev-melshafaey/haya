<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<?php $segment = $this->uri->segment(3); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12" style="background-color:#FFF !important; margin-top: 2px !important; padding-top:16px !important;">
		<table class="table table-bordered table-striped basicTable" id="basicTable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th>رقم استمارة</th>
						<th>اسم استمارة</th>
                        <th>وحدة الأم</th>
						<th>وحدة التفاصيل</th>
                        <th>وحدة أيقونة</th>
						<th>الإجرائات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?PHP foreach($flist as $Illuminati) { ?>
					<tr role="row">
					  <td><?PHP echo $Illuminati->moduleid; ?></td>
                        <td><?PHP echo $Illuminati->module_name; ?></td>
                        <td><?PHP echo $this->haya_model->ModuleName($Illuminati->module_parent); ?></td>
						<td><?PHP echo $Illuminati->module_text; ?></td>
                        <td><a onclick="alatadad(this);" data-icon="<?PHP echo($Illuminati->module_icon); ?>" data-heading="<?PHP echo($Illuminati->module_name); ?>" href="#" data-url="<?PHP echo base_url().$Illuminati->module_controller; ?>"><i class="<?PHP echo $Illuminati->module_icon; ?>"></i></a></td>
						<td><a href="<?PHP echo base_url(); ?>lease_programe/formbuilder/<?PHP echo $Illuminati->moduleid; ?>"><i style="color:#F00;" class="icon icon-edit"></i></a></td>
                      </tr>
					<?PHP } ?>
					</tbody>
                  </table>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</body>
</html>
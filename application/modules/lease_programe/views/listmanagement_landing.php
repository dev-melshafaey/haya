<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<?php $segment = $this->uri->segment(3); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12" style="padding: 0px !important;">
        <?PHP		
		foreach($this->loan->get_all_module() as $lease)
		{ 
		  $subCount = arabic_date($this->loan->getSubModuleCount($lease->moduleid));				
		?>
        <div class="col-md-3 myboxModule">
         <a class="module_<?PHP echo $lease->module_status; ?>" onClick="showglobaldata(this);" href="#globalDiag" data-url="<?PHP echo base_url(); ?>lease_programe/modulemodifier/<?PHP echo $lease->moduleid; ?>" data-id="<?PHP echo $lease->moduleid; ?>"> <i class="<?PHP echo  $lease->module_icon; ?>"></i> <?PHP echo $lease->module_name; ?> </a><br>
         <a href="#" class="sub_menu_activater" style="font-size: 11px;" data-id="<?PHP echo $lease->moduleid; ?>">وحدات فرعية (<?PHP echo $subCount; ?>)</a>
         <div class="col-md-12 inside sub_menu_list" id="sub_menu_data_load_<?PHP echo $lease->moduleid; ?>">  </div>
        </div>
        <?PHP  }  ?>
      
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
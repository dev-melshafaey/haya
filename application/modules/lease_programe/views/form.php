<div id="block_<?PHP echo $key; ?>" class="col-md-12 form_fields " style="padding:0px !important;">
				  <div class="col-md-1 form_bold">نوع الحقل:</div>
				  <div class="col-md-3 form_margin"><?PHP input_types('field_type',$d['ft'],$key); ?></div>
				  
				  <div class="col-md-1 form_bold">اسم الحقل:</div>
				  <div class="col-md-3 form_margin"><input value="<?PHP echo $d['fn']; ?>" placeholder="اسم الحقل" type="text" name="field_name[]" id="field_name" class="form-control req"></div>
			  
				  <div class="col-md-1 form_bold">ترتيب الحقول:</div>
				  <div class="col-md-2 form_margin"><?PHP number_drop_box('field_order[]',$d['fo']); ?></div>
				  <div class="col-md-1 form_margin"><button style="text-shadow: none !important; background-color: #F00; padding: 8px 19px 4px 16px !important; clear: both !important;" type="button" class="btn btn-danger" name="removex" id="removex" onclick="removethispanel('<?PHP echo($key); ?>');"><i class="icon-remove-sign"></i> حذف </button></div>
				  
				  <div class="col-md-1 form_bold">حقل مطلوب:</div>
				  <div class="col-md-3 form_margin"><?PHP show_on_menu('field_required[]',$d['fr']); ?></div>
				  
				  <div class="col-md-1 form_bold">الحالة:</div>
				  <div class="col-md-3 form_margin"><?PHP status_dropbox('field_status[]',$d['fs']); ?></div>
				  
				  <div class="col-md-1 form_bold">قيمة الحقل:</div>
				  <div class="col-md-3 form_margin">
					<select name="field_value[]" id="field_value<?PHP echo $key; ?>" class="form-control">					  
					<?PHP if($d['fv']!='' && $d['fv']!='0') {
						  $html = '<option value="">اختر وحدة</option>';		
						  foreach($this->haya_model->get_listmanagment_types() as $gltkey => $gvalue)
						  {
							  $html .= '<option value="'.$gvalue.'" ';
							  if($d['fv']==$gvalue)
							  {
								  $html .= 'selected="selected"';
							  }
							  $lab = $this->config->item('list_types');
							  $html .= '>'.$lab[$gvalue]['ar'].'</option>';
						  }
						  echo $html; } ?>
					</select>
					
				  </div>
			  <br style="clear: both;">
			  </div>
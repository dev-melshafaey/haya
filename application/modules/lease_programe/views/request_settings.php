<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12" style="background-color:#FFF !important;"> <br clear="all">
        <div class="panel panel-info">
          <div class="panel-heading" style="color: #FFF !important; font-size: 22px;">إعدادات طلب الموظف</div>
          <div class="panel-body">
            <form name="frmRequest" id="frmRequest" method="post" autocomplete="off">
            <input type="hidden" name="ahrs" id="ahrs" value="<?PHP echo $ahrs; ?>">
            <?PHP
      		$ah_request_settings = $inno['main'];
			$ah_request_settings_manager = $inno['man'];
			
	  ?>
              <div class="col-md-5 form-group">
                <div class="col-md-12 form-group" style="padding:0px;">
                  <label class="text-control">موظف:</label>
                  <select id="employee" name="employee" class="form-control">
                    <?PHP echo $this->haya_model->get_all_employee_list($ah_request_settings->employee_id); ?>
                  </select>
                </div>
                <div class="col-md-12 form-group" style="padding:0px;">
                  <label class="text-control">طلب:</label>
                  <select id="requesttype" name="requesttype" class="form-control">
                    <?PHP foreach($this->haya_model->get_all_request_type('A') as $aq => $av) { ?>
                    <option <?PHP if($ah_request_settings->request_type==$aq) {?> selected <?PHP } ?> value="<?PHP echo $aq; ?>"><?PHP echo $av; ?></option>
                    <?PHP } ?>
                  </select>
                </div>
                <div class="col-md-12 form-group" style="padding:0px;">
                  <label class="text-control">نسخ الى:</label>
                  <select id="copyto" name="copyto" class="form-control">
                    <?PHP echo $this->haya_model->get_all_employee_list($ah_request_settings->copyto_id); ?>
                  </select>
                </div>
                <div class="col-md-12 form-group" style="padding:0px;">
                  <button id="save_request_settings" type="button" class="btn btn-lg btn-success">حفظ</button> <button type="reset" class="btn btn-lg btn-danger">إعادة تعيين</button>
                </div>
              </div>
              <div class="col-md-7 form-group">
                <table class="table table-bordered table-striped dataTable" id="basicTable">
                  <thead>
                    <tr role="row">
                      <th class="right">مدير الموظف</th>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?PHP foreach($this->haya_model->get_all_managers_for_request() as $u) { ?>
                    <tr role="row">
                      <td class="right"><input <?PHP if(in_array($u->userid,$ah_request_settings_manager)) { ?> checked <?PHP } ?> type="checkbox" class="managers" name="manager[]" value="<?PHP echo $u->userid; ?>">&nbsp;&nbsp;<?PHP echo $u->fullname; ?> - <small><?PHP echo $u->branchname; ?></small></td>
                    </tr>
                    <?PHP } ?>
                  </tbody>
                </table>
              </div>
            </form>
            <br clear="all">
            <div class="col-md-12 from-control">
            <table class="table table-bordered table-striped myTable">
                  <thead>
                    <tr role="row">
                      <th class="right">موظف</th>
                      <th class="right">طلب</th>
                      <th class="right">نسخ الى</th>
                      <th class="right">مدير الموظف</th>
                      <th class="center">التاريخ / الوقت</th>
                      <th class="center">لإجراءات</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?PHP foreach($this->loan->getRequestList() as $u) { ?>
                    <tr role="row">
                      <td class="right"><?PHP echo $u->eName; ?></td>
                      <td class="right"><?PHP echo $this->haya_model->get_all_request_type($u->request_type); ?></td>
                      <td class="right"><?PHP echo $u->cName; ?></td>
                      <td class="right"><?PHP echo $this->loan->get_all_managers($u->ahrs); ?></td>
                      <td class="center"><?PHP echo $u->update_date; ?></td>
                      <td class="center"><a href="<?PHP echo base_url();?>lease_programe/request_settings/<?PHP echo $u->ahrs; ?>">تصحيح</a></td>
                    </tr>
                    <?PHP } ?>
                  </tbody>
                </table>
            
            </div>
          </div>
        </div>
        <br clear="all">
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
$(function(){
	$('#save_request_settings').click(function(){
		var count = $("[type='checkbox']:checked").length;
		if(count > 0)
		{
			$('#frmRequest').submit();
		}
		else
		{
			show_notification_error_end('يرجى تحديد مدير الموظف');
		}
	});
});
</script>
</div>
</body>
</html>
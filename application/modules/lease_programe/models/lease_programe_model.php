<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lease_programe_model extends CI_Model {
	
	/*
	* Properties
	*/
	private $_table_loan_category;
	private $_table_loan_calculate;
//----------------------------------------------------------------------
    
	/*
	* Constructor
	*/
	
	function __construct()
    {
        parent::__construct();
		
		//Load Table Names from Config
		$this->_table_loan_category 		= 	$this->config->item('table_loan_category');
		$this->_table_loan_calculate 		= 	$this->config->item('table_loan_calculate');
    }
	
//----------------------------------------------------------------------
	/*
	* Insert User Record
	* @param array $data
	* return True
	*/
	function add_loan_calculate($data)
	{
		$this->db->insert($this->_table_loan_calculate,$data);
		
		return TRUE;
	}
	
	public function get_all_module_count($tablename,$columnto,$columnfrom='')
	{
		if($tablename!='' && $columnto!='')
		{
			$this->db->select($columnto);
			$this->db->from($tablename);
			if($columnto!='' && $columnfrom!='')
			{	$this->db->where($columnto,$columnfrom); }
			
				$query = $this->db->get();
				return $query->num_rows();
		}
		else
		{
			return 0; }
		
			
			//return 0;
	}
	
	public function get_all_sub_module($moduleid)
	{
		$this->db->select('*');
		$this->db->from('mh_module_child_engry');
		$this->db->where('moduleid',$moduleid);		
		$this->db->order_by("moduletitle", "ASC");  
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $dd)
			{
				$arr[] = array(
					'moduletitle'		=>	$dd->moduletitle,
					'controllerlink'	=>	base_url().$dd->controllerlink,
					'mstatus'	=>	$dd->module_status,
					'micon'	=>	$dd->module_icon,
					'count'				=>	$this->get_all_module_count($dd->tablename,$dd->columnto,$dd->columnfrom)
				);
			}
		}
		return $arr;
	}
	
	public function getSubModuleCount($parentid)
	{
		$this->db->select('moduleid');
		$this->db->from('mh_modules');
		$this->db->where('module_parent',$parentid);
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function get_all_module()
	{
		$this->db->select('*');
		$this->db->from('mh_modules');
		$this->db->where('module_parent','0');
		$this->db->where('moduleid != ','8');
			$this->db->where("moduleid != ",209);
		$this->db->where("moduleid != ",215);
		$this->db->where("moduleid != ",216);
		$this->db->where("moduleid != ",217);
		$this->db->order_by("module_order", "ASC");  
		$query = $this->db->get();
		return $query->result();
	}
	
//----------------------------------------------------------------------

	public function update_loan_calculation($id,$data)
	{
		$this->db->where('loan_caculate_id', $id);
		$this->db->update($this->_table_loan_calculate, $data);
		
		return TRUE;
	}

//----------------------------------------------------------------------
	/*
	* Insert User Record
	* @param array $data
	* return True
	*/
	function add_category($data)
	{
		$this->db->insert($this->_table_loan_category,$data);
		
		return TRUE;
	}
	
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_single_loan_calculate($loan_id)
	{
		$this->db->where('loan_caculate_id',$loan_id);

		$query = $this->db->get($this->_table_loan_calculate);
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_all_loan_calculate()
	{
		$this->db->where('parent_id','0');
		
		$query = $this->db->get($this->_table_loan_calculate);
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
//----------------------------------------------------------------------

	/*
	* Delete
	*/
	function delete($loan_id)
	{
		$this->db->where("loan_caculate_id",$loan_id);
		$this->db->delete($this->_table_loan_calculate);
		
		return true; 
	}
//----------------------------------------------------------------------

	/*
	* Delete
	*/
	function delete_category($cat_id)
	{
		$this->db->where("loan_category_id",$cat_id);
		$this->db->delete($this->_table_loan_category);
		
		return true; 
	}
	//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function child_count($id)
	{
		$sql 	=	"SELECT COUNT(*) AS total FROM (`loan_calculate`) WHERE `parent_id` = '".$id."'";
		$q 		=	$this->db->query($sql);		
	
		// Check if Result is Greater Than Zero
		if($q->num_rows() > 0)
		{
			 $oneRow = $q->row();
			 return $oneRow->total;
		}
	}
	
	
	//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_loan($id)
	{
		$sql	=	"SELECT * FROM `loan_calculate` AS lc WHERE lc.`loan_caculate_id` = '".$id."'";
		$q		=	$this->db->query($sql);		
	
		// Check if Result is Greater Than Zero
		if($q->num_rows() > 0)
		{
			return  $q->row();
		}
	}
	
//------------------------------------------------------------------------

    /**
     * 
     * Insert User Data for Registration
     * @param array $data
     * return integer
     */
	function update_category($cat_id,$data)
	{

		$this->db->where('loan_category_id', $cat_id);
		$this->db->update($this->_table_loan_category, $data);
		
		return TRUE;
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_loan_types($category_id)
	{
		$this->db->where('loan_category_id',$category_id);

		$query = $this->db->get($this->_table_loan_category);
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function category_name($category_id)
	{
		$this->db->where('loan_category_id',$category_id);

		$query = $this->db->get($this->_table_loan_category);
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->loan_category_name;
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function get_all_loan_types()
	{
		$query = $this->db->get($this->_table_loan_category);
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data
	*/
	function get_data($id)
	{
		$this->db->where('loan_caculate_id',$id);

		$query = $this->db->get($this->_table_loan_calculate);
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* Insert Record
	* @param array $data
	* return True
	*/
	function add_parent($data)
	{
		$this->db->insert($this->_table_loan_calculate,$data);
		
		return TRUE;
	}
//------------------------------------------------------------------------

    /**
     * 
     * Update Record
     * @param array $data
     * return integer
     */
	function update_parent($id,$data)
	{

		$this->db->where('loan_caculate_id', $id);
		$this->db->update($this->_table_loan_calculate, $data);
		
		return TRUE;
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function child_listing($parent_id)
	{
		$this->db->where('parent_id',$parent_id);

		$query = $this->db->get($this->_table_loan_calculate);
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data
	*/
	function get_parent_name($id)
	{
		$this->db->where('loan_caculate_id',$id);

		$query = $this->db->get($this->_table_loan_calculate);
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->loan_category_name;
		}
	}
    
    function get_all_setting_list()
    {
        $query = $this->db->get('ah_settings');
        return $query->result();
    }
	
	function get_all_custom_form($id=0)
	{
		$this->db->select("*");
		$this->db->from("mh_modules");
		$this->db->where("custom_form != ","");
		if($id==0)
		{
			$query = $this->db->get();
			return $query->result();
		}
		else
		{
			$this->db->where("moduleid", $id);
			$query = $this->db->get();
			return $query->row();
		}
		
	}
//----------------------------------------------------------------------
	/*
	* Get Dynamic Form Field Data
	* @param $moduleid int
	* @param $mmdid int
	* return OBJECT
	*/
	function get_form_field_data($module_id,$mmdid)
	{
		$this->db->where('mmdid',$mmdid);
		$this->db->where('moduleid',$module_id);
		
		$query = $this->db->get('mh_module_data');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	
	function addRequestSetting($post)
	{
		$ahrs = trim($post['ahrs']);
		$this->db->select("*");
		$this->db->from("ah_request_settings");
		$this->db->where("employee_id",$post['employee']);			
		$this->db->where("copyto_id",$post['copyto']);
		$this->db->where("request_type",$post['requesttype']);
		$numData = $this->db->get();
		///////////////////////////
		$ah_request_settings['employee_id'] = $post['employee'];
		$ah_request_settings['copyto_id'] = $post['copyto'];
		$ah_request_settings['request_type'] = $post['requesttype'];
		$ah_request_settings['update_date'] = date('Y-m-d h:i:s');
		////////////////////////////		
		if($ahrs!='')
		{
			$this->db->where("ahrs",$ahrs);
			$this->db->update('ah_request_settings',json_encode($ah_request_settings),$this->_login_userid,$ah_request_settings);
		}
		else
		{		
			if($numData->num_rows() > 0)
			{
				$ahrs = $numData->row()->ahrs;			
				$this->db->where("ahrs",$ahrs);
				$this->db->update('ah_request_settings',json_encode($ah_request_settings),$this->_login_userid,$ah_request_settings);
			}
			else
			{
				$this->db->insert('ah_request_settings',$ah_request_settings,json_encode($ah_request_settings),$this->_login_userid);	
				$ahrs = $this->db->insert_id();
			}
		}
		
		$this->db->query("DELETE FROM ah_request_settings_manager WHERE ahrs='".$ahrs."'");
		foreach($post['manager'] as $mankey => $manvalue)
		{			
			$ah_request_settings_manager['ahrs'] = $ahrs;
			$ah_request_settings_manager['managerid'] = $manvalue;
			$this->db->insert('ah_request_settings_manager',$ah_request_settings_manager,json_encode($ah_request_settings_manager),$this->_login_userid);
		}
	}
	
	function getRequestList()
	{
		$myQuery = $this->db->query("SELECT  
					a.`ahrs`,
					a.`update_date`,
					a.request_type,
					b.`fullname` AS eName,
					c.fullname AS cName,
					a.`copyto_id`,
					a.`employee_id`
					
				FROM 
				ah_request_settings AS a
				JOIN ah_userprofile AS b ON  (b.`userid`=a.`employee_id`)
				JOIN ah_userprofile AS c  ON (c.userid=a.`copyto_id`)");
		return $myQuery->result();		
	}
	
	function get_all_managers($ahrs)
	{
		$q = $this->db->query("SELECT 
								GROUP_CONCAT(ah_userprofile.`fullname`) AS aName
								FROM ah_userprofile
								JOIN ah_request_settings_manager ON (ah_request_settings_manager.`managerid`=ah_userprofile.`userid`)
								WHERE ah_request_settings_manager.`ahrs`='".$ahrs."';");
		return str_replace(',','<br>',$q->row()->aName);
	}
	
	function get_edit_mode($id)
	{
		$this->db->from("ah_request_settings");
		$this->db->where("ahrs",$id);
		$a['main'] = $this->db->get()->row();
		
		$this->db->select("managerid");
		$this->db->from("ah_request_settings_manager");
		$this->db->where("ahrs",$id);
		foreach($this->db->get()->result() as $dump)
		{
			$a['man'][] = $dump->managerid;
		}
		
		return $a;
	}
//----------------------------------------------------------------------
}
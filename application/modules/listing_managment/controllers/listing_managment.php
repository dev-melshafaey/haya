<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Listing_managment extends CI_Controller 
{

//-------------------------------------------------------------------------------	

	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;

//-------------------------------------------------------------------------------
	/*
	* Costructor
	*/
	public function __construct()
	{
		parent::__construct();
		
		// Load Models
		$this->load->model('listing_managment_model', 'listing');
		
		$this->_data['module']			=	$this->haya_model->get_module();
			
		// SET Login USER ID
		$this->_login_userid			=	$this->session->userdata('userid');
		$this->_data['login_userid']	=	$this->_login_userid;	
		
		$this->_data['user_detail'] 	= $this->haya_model->get_user_detail($this->_login_userid);	
		$this->_data['list_types']		=	$this->haya_model->get_listmanagment_types();
	}

//-------------------------------------------------------------------------------

	/*
	*
	* Main Page
	*/
	public function index()
	{
		$this->_data['list_data']	=	$this->listing->get_all_list_data();
		
		$this->load->view('listmanagement_landing', $this->_data);
	}

//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	public function financial_management($islist='')
	{
		$permissions	=	$this->haya_model->check_other_permission(array('122'));
		
		if($islist	!=	'')
		{			
			foreach($this->listing->ah_financial() as $lc)
			{
				if($permissions[122]['u']	==	1)
				{
					$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment/add_financial_management/'.$lc->financialid.'" id="'.$lc->financialid.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a>&nbsp;&nbsp;';
				}
				if($permissions[122]['d']	==	1)
				{
					$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->financialid.'" data-url="'.base_url().'listing_managment/delete_financial_management/'.$lc->financialid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>&nbsp;&nbsp;';
				}
			
			if($lc->people_start	>= 0 && $lc->people_end	<=	1)
			{
				$text = "فرد واحد";
			}
			else if($lc->people_start >= 2 && $lc->people_end	<=	3)
			{
				$text = "فردين او ".arabic_date(3)." افراد";
			}
			else
			{
				$text = arabic_date($lc->people_start)." او ".arabic_date($lc->people_end)." افراد";
			}
			
			$arr[] = array(
				"DT_RowId"			=>	$lc->financialid.'_durar_lm',
                "عدد افراد الاسرة" 	=>	$text,
				"الفئة المستحقة" 	=>	number_drop_box_arabic('',$lc->priority,1),
				"المبلغ" 			=>	arabic_date($lc->amount),
				"صافي الدخل" 		=>	arabic_date($lc->netincome).' ريال فما دون',				
				"الإجراءات" 			=>	$actions);
				
				unset($actions,$text);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
			exit();
		}
		
		$this->load->view('financial_list',$this->_data);
	}
//-------------------------------------------------------------------------------	
	public function add_financial_management($financeid=0)
	{
		if($this->input->post())
		{
			$this->listing->savefinancial_management();
		}
		
		$this->_data['ff'] = $this->listing->ah_financial_data($financeid);
		
		$this->load->view('financial_form',$this->_data);
	}
//-------------------------------------------------------------------------------	
	public function delete_financial_management($financeid)
	{
		$json_data	= json_encode(array('record'=>'delete','financialid'=>$financeid));		
		$data 		= array('financialstatus'=>'0');
					
		$this->db->where('financialid', $financeid);
		$this->db->update('ah_financial',$json_data,$this->session->userdata('userid'),$data);
		return TRUE;
	}
//-------------------------------------------------------------------------------
	/*
	*
	* Add List Detail
	*/
	public function addlistingview()
	{
		$this->load->view('addlistingview');
	}
//-------------------------------------------------------------------------------	

	public function add_parent($type	=	NULL)
	{
		$this->_data['type']	=	NULL;
		if($type)
		{
			
			$this->_data['type'] = $this->listing->get_type_id($type);
		}
		
		$this->load->view('add-parent',$this->_data);
	}
//-------------------------------------------------------------------------------

	public function add_child($parent_id)
	{
		if($parent_id)
		{
			$this->_data['parent_id']	=	$parent_id;
			$this->_data['type'] 		=	$this->listing->get_type_name($parent_id);
		}
		else
		{
			$this->_data['parent_id']	=	NULL;
		}
		
		$this->load->view('add-child',$this->_data);
	}
//-------------------------------------------------------------------------------	
	public function add($listid	= NULL)
	{
		if($listid)
		{
			$this->_data['single_list']	=	$this->listing->get_single_record($listid);	
		}

		if($this->input->post())
		{
			$data		=	$this->input->post();

			// UNSET ARRAY key
			unset($data['submit']);

			if($this->input->post('list_id'))
			{
				$this->listing->update_list($this->input->post('list_id'),$data);

				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');
				}
				else
				{
					$this->session->set_flashdata('success', 'تم تحديث تسجيلك بنجاح');

					redirect(base_url()."listing_managment/listing");
					exit();
				}
			}
			else
			{
				$this->listing->add_list($data);

				if ($this->is_ajax())
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');
				}
				else
				{
					$this->session->set_flashdata('success', 'تم إضافة تسجيلك بنجاح');

					redirect(base_url()."listing_managment/listing");
					exit();
				}
			}
		}
		else
		{
			if($listid)
			{
				$this->_data['list_id']	=	$listid;
			}
			else
			{
				$this->_data['list_id']	=	'';
			}

			$this->load->view('add', $this->_data);
		}
	}

//-------------------------------------------------------------------------------

	/*
	*
	* Listing Page
	*/
	public function listing($type	=	NULL)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		if($type)
		{
			$this->_data['type']	=	$type;
			
			// Get List Name By Type
			$this->_data['listing']	=	$this->listing->get_all_by_type($type);

			$this->_data['list_type_name']	=	$type;

			$this->load->view('type-listing', $this->_data);
		}
		else
		{
			$this->load->view('listing', $this->_data);
			
		}
	}

//-------------------------------------------------------------------------------
	/*
	*
	* 
	*/
	public function types_listing()
	{
		$segment = $this->uri->segment(3);
		$labels	=	$this->config->item('list_types');
		
		$permissions	=	$this->haya_model->check_other_permission(array('1'));
		
		foreach($this->listing->get_all_list_type() as $listdata) 
		{
			$typename = $labels[$listdata->list_type]['en'];
			if($permissions[1]['a']	==	1)
			{
				$action =' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment/listing_managment/add_parent/'.$listdata->list_type.'/" id="'.$listdata->list_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> ';
			}
			$action .=' <a href="'.base_url().'listing_managment/listing/'.$listdata->list_type.'">عرض الكل ('.$this->listing->total_count($listdata->list_type).')</a>'; 
			
			$arr[] = array(
				"DT_RowId"=>$listdata->list_id.'_durar_lm',
                "‫نوع القائمة" =>$this->haya_model->get_type_name($listdata->list_type),
				/*"عرض الكل" =>$listdata->list_type,*/
				"إجمالي عدد" =>$action);
				unset($action);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------
	/*
	*
	* 
	*/
	
	public function parent_listing($type)
	{
		$this->db->select('list_id,list_name,list_type,list_other,list_status');
		$this->db->where_in('list_type',$type);
		$this->db->where('list_parent_id','0');
		$this->db->where_in('delete_record','0');
		
		$query = $this->db->get($this->config->item('table_listmanagement'));
		
		$permissions	=	$this->haya_model->check_other_permission(array('1'));
		
		$checkbox	=	'---';

		foreach($query->result() as $lc)
		{
			if($lc->list_other)
			{
				$checked	=	'checked="checked"';
			}
			else
			{
				$checked	=	'';
			}
			
			if($type	!=	'departments' && $type	!= 'user_role' && $type	!= 'section_types' && $type	!= 'yateem_boxes')
			{	
				  $checkbox ='<div class="other1"> <input type="checkbox" onClick="other(this);" id="'.$lc->list_id.'" name="other'.$lc->list_id.'" '.$checked.'>
				  <div id="show'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;">&#10004;</div>
				  <div id="hide'.$lc->list_id.'" style="display:none; color:#060;margin-top: -20px; margin-right: 36px;"><i style="color:#CC0000;" class="icon-remove-sign"></i></div>
				  </div>';
			}
				  
			if($lc->list_status	==	'1')
			{
				$actions .=' <a href="#_" onClick="record_status(this);" data-url="'.base_url().'listing_managment/record_status/'.$lc->list_id.'/'.$lc->list_status.'" title="Activate Record"><i style="color:#CC0000;" class="icon-off"></i></a>&nbsp;&nbsp;';
			}
			else
			{
				$actions .=' <a href="#_" onClick="record_status(this);" data-url="'.base_url().'listing_managment/record_status/'.$lc->list_id.'/'.$lc->list_status.'" title="Deactivate Record"><i style="color:#00CC00;" class="icon-ok"></i></a>&nbsp;&nbsp;';
			}
			
			if($permissions[1]['a']	==	1)
			{
				if($type	!=	'departments' && $type	!= 'user_role')
				{
					$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment/add_child_qaima/'.$lc->list_id.'/" id="'.$lc->list_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a>&nbsp;&nbsp;';
				}
			}
			
			if($permissions[1]['u']	==	1)
			{					
				$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment/add_data_for_qaima/'.$lc->list_id.'/parent" id="'.$lc->list_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a>&nbsp;&nbsp;';
			}
			
			if($permissions[1]['d']	==	1)
			{
				if($type	!= 'user_role' && $type	!= 'charity_type' && $type	!= 'marital_status' && $type	!= 'nationality' && $type	!= 'regions' && $type	!= 'school_type' && $type	!= 'section_types')
				{
					$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->list_id.'" data-url="'.base_url().'listing_managment/delete/'.$lc->list_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>&nbsp;&nbsp;';
				}
			}
			
			//$count = $this->qaima_count($lc->list_id);
			$count = $this->listing->get_list_child_count($lc->list_id);
			
			if($type	!=	'departments' && $type	!= 'user_role')
			{		
				$actions .=' <a href="'.base_url().'listing_managment/child_listing/'.$lc->list_id.'">عرض الكل ('.$count.')</a>'; 
			}
			
			$arr[] = array(
				"DT_RowId"		=>	$lc->list_id.'_durar_lm',
				"قائمة البرامج" =>	$lc->list_name,              
				"أخرى" 			=>	$checkbox,
				"الإجراءات" 		=>	$actions);
				
			unset($actions);
		}
		
		$ex['data'] = $arr;
		echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Listing Page
	*/	
	public function child_types_listing($parent_id)
	{
		$this->db->select('*');
		$this->db->where('list_parent_id',$parent_id);
		$this->db->where_in('delete_record','0');
		$this->db->order_by('list_order','DESC');
		$query = $this->db->get($this->config->item('table_listmanagement'));
			
		$permissions	=	$this->haya_model->check_other_permission(array('72'));

		foreach($query->result() as $lc)
		{
			if($lc->list_type	==	'section_types' || $lc->list_type	==	'yateem_boxes')
			{
				if($lc->list_status	==	'1')
				{
					$actions .=' <a href="#_" onClick="record_status(this);" data-url="'.base_url().'listing_managment/record_status/'.$lc->list_id.'/'.$lc->list_status.'" title="Activate Record"><i style="color:#CC0000;" class="icon-off"></i></a>&nbsp;&nbsp;';
				}
				else
				{
					$actions .=' <a href="#_" onClick="record_status(this);" data-url="'.base_url().'listing_managment/record_status/'.$lc->list_id.'/'.$lc->list_status.'" title="Deactivate Record"><i style="color:#00CC00;" class="icon-ok"></i></a>&nbsp;&nbsp;';
				}
			}
			
			if($lc->list_type	==	'issuecountry')
			{
				$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment/add_child_qaima/'.$lc->list_id.'/" id="'.$lc->list_id.'" data-icon="icon-plus-sign-alt" data-heading="إضافة" data-color="#00CC00"><i style="color:#00CC00;" class="icon-plus-sign-alt"></i></a> ';
			}
			if($permissions[72]['d']	==	1)
			{
				//$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->list_id.'" data-url="'.base_url().'listing_managment/delete/'.$lc->list_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';
			}
			if($permissions[72]['u']	==	1)
			{
				$actions .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment/add_data_for_qaima/'.$lc->list_id.'/parent" id="'.$lc->list_id.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a>&nbsp;&nbsp;';	
			}
			
			if($lc->list_type	==	'issuecountry')
			{
				$count = $this->listing->get_list_child_count($lc->list_id);
				$actions .=' <a href="'.base_url().'listing_managment/sub_child_listing/'.$lc->list_id.'">عرض الكل ('.$count.')</a>'; 
			}


			$arr[] = array(
				"DT_RowId"		=>	$lc->list_id.'_durar_lm',
				"‫نوع القائمة" 	=>	$lc->list_name,              
				"الإجراءات" 		=>	$actions);
				
				unset($actions);	// UNSET ARRAY
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
		}
//-------------------------------------------------------------------------------

	/*
	*
	* Listing Page
	*/
	public function sub_child_nooaul_qaima($parent_id)
	{
		$this->db->select('*');
		$this->db->where('list_parent_id',$parent_id);
		$this->db->order_by('list_order','DESC');
		$query = $this->db->get($this->config->item('table_listmanagement'));
		
		$permissions	=	$this->haya_model->check_other_permission(array('72'));
		
		foreach($query->result() as $lc)
		{
				if($permissions[72]['d']	==	1)
				{
					$actions =' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->list_id.'" data-url="'.base_url().'listing_managment/delete/'.$lc->list_id.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
				
				 $arr[] = array(
				"DT_RowId"		=>	$lc->list_id.'_durar_lm',
                "‫نوع القائمة" 	=>	$lc->list_name,              
				"الإجراءات" 		=>	$actions);
				
				unset($actions);
		}
		
			$ex['data'] = $arr;
			echo json_encode($ex);
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Listing Page
	*/
	public function add_child_qaima($parentid)
	{
		$this->_data['type'] 		=	'child';
		$this->_data['parent_id'] 	=	$parentid;
		$this->_data['list_type'] 	=	$this->listing->get_type_name($parentid);
		
		$this->load->view('dialog/add-child-dialog',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Listing Page
	*/	
	public function add_data_for_qaima($parentid)
	{
		if($parentid!='' && $parentid!=0)
		{
			$this->_data['data'] = $this->listing->get_list_data($parentid);
		}
		
		$this->_data['type'] = 'parent';
		
		$this->load->view('dialog/add-child-dialog',$this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	*
	* Listing Page
	*/	
	public function qaima_count($id)
	{
		//$this->db->where("parent_id",$id);
		//$query = $this->db->get($this->_table_loan_calculate);
		$sql = "SELECT COUNT(*) AS total FROM (".$this->config->item('table_listmanagement').") WHERE `list_parent_id` = '".$id."'";
		$q = $this->db->query($sql);		
	
		// Check if Result is Greater Than Zero
		if($q->num_rows() > 0)
		{
			 $oneRow = $q->row();
			 return $oneRow->total;
		}
	}
//-------------------------------------------------------------------------------

	public function add_parent_qaima()
	{
		$list_id	=	$this->input->post("list_id");
		
		$data	=	$this->input->post();
		
		if($list_id)
		{
			$this->listing->update_list($list_id,$data);
		}
		else
		{
			$this->listing->add_list($data);
		}
	}
//-------------------------------------------------------------------------------
	/*
	*
	* Child Listing
	*/
	public function child_listing($listid	=	NULL)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['parent_id']	=	$listid;
		$this->_data['listing']	=	$this->listing->get_child_listing($listid);

		$this->load->view('child-type-listing', $this->_data);
	}

//-------------------------------------------------------------------------------
	/*
	*
	* 
	*/

	public function sub_child_listing($listid	=	NULL)
	{
		$this->haya_model->check_permission($this->_data['module'],'v');
		
		$this->_data['parent_id']	=	$listid;

		$this->_data['listing']		=	$this->listing->get_subchild_listing($listid);

		$this->load->view('subchilds-type-listing', $this->_data);
	}	

//-------------------------------------------------------------------------------
	/*
	*
	*
	*/
	function add_new()
	{
		$parent_id	=	$this->input->post("parent_id");
		$add_sub	=	$this->input->post("add_sub");
		$list_order	=	$this->input->post("list_order");
		$type		=	$this->input->post("list_type");
		
		if($type	==	'bank')
		{
			$type	=	'bank_branch';
		}
		
		$data		=	array(
						"list_parent_id"	=>	$parent_id,
						"list_name"			=>	$add_sub,
						"list_type"			=>	$type,
						"list_order"		=>	$list_order
						);

		$this->listing->add_list_child($data);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*
	*/
	public function get_list_data()
	{
		$list_id	=	$this->input->post('id');
		$data	=	$this->listing->get_list_data($list_id);
		echo  $data	=	json_encode(array('list_id'	=>	$data->list_id,'list_name'	=>	$data->list_name,'list_type'	=>	$data->list_type,'list_status'	=>	$data->list_status));
	}	

//-------------------------------------------------------------------------------
	/*
	* Delete List
	*
	*/

	public function delete($listid,$type)
	{
		$this->listing->delete($listid);

		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');

		redirect(base_url().'listing_managment/listing/'.$type);
		exit();
	}
//-------------------------------------------------------------------------------
	/*
	* Delete List
	*
	*/

	public function delete_child($childlistid)
	{
		$this->listing->delete_child($childlistid);

		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
		redirect(base_url().'listing_managment/listing/');
		exit();

	}
//-------------------------------------------------------------------------------
	/*
	* Delete List
	*
	*/

	public function record_status($list_id,$status)
	{
		if($status	==	'1')
		{
			$update_status	=	'0';
		}
		else
		{
			$update_status	=	'1';
		}
		
		$this->listing->record_status($list_id,$update_status);
	}


//-------------------------------------------------------------------------------

	/*
	* Delete List
	*
	*/
	public function other()
	{
		$list_id	=	$this->input->post("id");
		$entry		=	$this->input->post("entry");

		$data		=	array('list_other' => $entry);

		$this->listing->update_record($list_id, $data);
	}

//-------------------------------------------------------------------------------

	/*
	* Logout
	* Destroy All Sessions
	*/

	public function logout()
	{

		// Destroy all sessions
		$this->session->sess_destroy();
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('userinfo');

		redirect(base_url());
		exit();
	}

//-------------------------------------------------------------------------------
	/*
	*
	* Function to check if the request is an AJAX request
	*
	*/
	function is_ajax() 
	{
	  return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
	}

//-------------------------------------------------------------------------------
	/*
	*
	*
	*/
	function alldocuments()
	{
		$this->_data['doclist'] = $this->haya_model->get_dropbox_list_value('charity_type');
		$this->load->view('alldocuments',$this->_data);
	}
//-------------------------------------------------------------------------------
	/*
	*
	*
	*/	
	function add_required_document($docid)
	{
		if($this->input->post())
		{	$this->listing->savedocumenttype();	}
		
		$this->_data['item'] = $this->listing->get_all_document_list('charity_type',$docid);
		$this->load->view('add_documents',$this->_data);
	}
//-------------------------------------------------------------------------------

  	/**
   	* Dynamic Forms Listing Page
   	* @param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]  = $this->haya_model->get_all_custom_form($moduleid);
		
		$this->_data["userid"] = $this->_login_userid;
		
		$this->_data['formid']	=	$moduleid;
		 
		 // Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);	 
	 }
//-------------------------------------------------------------------------------
}
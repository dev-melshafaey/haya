<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Listing_managment_model extends CI_Model {

	/*
	* Properties
	*/
	private $_table_list_management;

//----------------------------------------------------------------------

	/*
	* Constructor
	*/

	function __construct()
    {
        parent::__construct();

		//Load Table Names from Config
		$this->_table_list_management	=	$this->config->item('table_listmanagement');
    }

//----------------------------------------------------------------------
	/*
	* Get all Data 
	* return OBJECT
	*/

	function get_all_list_data()
    {
		$this->db->select('list_id,list_name,list_type,list_other,list_parent_id');
		$this->db->group_by('list_id');
		$query	=	$this->db->get($this->_table_list_management);
		
		//Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/	
	function ah_financial($financialid = 0)
    {		
		$this->db->select('*');
		$this->db->where('financialstatus','1');
		if($financialid	!=	0 && $financialid	!=	'')
		{	$this->db->where('financialid',$financialid);	}
		$this->db->order_by("priority", "ASC");
		$query =	$this->db->get('ah_financial');	
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/	
	function ah_financial_data($financialid = 0)
    {		
		$this->db->select('*');
		$this->db->where('financialstatus','1');
		$this->db->where('financialid',$financialid);
		$this->db->order_by("priority", "ASC");
		$query =	$this->db->get('ah_financial');	
		
		return $query->row();
    }
//----------------------------------------------------------------------
	/*
	* Insert User Record
	* @param array $data
	* return True
	*/

	function add_list($data)
	{
		$json_data	=	json_encode(array('data'=>$data));
		$this->db->insert($this->_table_list_management,$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
	}

//----------------------------------------------------------------------
	/*
	* Insert  Record
	* @param array $data
	* return True
	*/

	function add_list_child($data)
	{
		$json_data	=	json_encode(array('data'=>$data));
		$this->db->insert($this->_table_list_management,$data,$json_data,$this->session->userdata('userid'));

		return TRUE;
	}

//----------------------------------------------------------------------
	/*
	* Insert  Record
	* @param array $data
	* return True
	*/

	function get_list_child_count($listid)
	{
		$this->db->where('list_parent_id',$listid);
		$this->db->where('delete_record','0');
		$this->db->where('list_status','1');
		$query = $this->db->get($this->_table_list_management);

		return $query->num_rows();
	}

//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/

	function get_child_listing($listid)
	{
		$this->db->where('list_parent_id',$listid);
		$this->db->order_by("list_order", "DESC");

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/

	function get_subchild_listing($listid)
	{
		$this->db->where('list_parent_id',$listid);
		$this->db->where('delete_record','0');
		$this->db->order_by("list_order", "DESC");

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/

	function by_type($type)
	{
		$this->db->where('list_type',$type);
		$this->db->order_by("list_order", "DESC");
		$this->db->where("list_type !=",'rules');
		$this->db->where("list_type !=",'qualification');

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/

	function get_all_by_type($type)
	{
		$this->db->where('list_type',$type);
		$this->db->order_by("list_order", "DESC");

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/

	function get_list_data($listid)
	{
		$this->db->where('list_id',$listid);

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------

	/*
	* Get Data for Listing for Store
	*/
	function rules_qualification_by_type($type)
	{
		$this->db->where('list_type',$type);
		$this->db->order_by("list_order", "DESC");

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/

	function by_type_rules_qua($type)
	{

		$this->db->where('list_type',$type);
		$this->db->order_by("list_order", "DESC");

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/
	function inquire_by_type($type)
	{
		$this->db->where('list_type',$type);

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* Get Data for Listing for Store
	*/

	function get_single_record($listid)
	{
		$this->db->where('list_id',$listid);

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	public function total_count($type)
	{
		$this->db->where('list_type',$type);
		$this->db->where('list_parent_id','0');
		$this->db->where('delete_record','0');
		$this->db->where('list_status','1');
		$query = $this->db->get($this->_table_list_management);

		return $query->num_rows();
	}
//----------------------------------------------------------------------
	/*
	* Delete
	*/

	function delete($listid)
	{
		$json_data	=	json_encode(array('record'=>'delete','list_id'	=>	$listid));
		
		$data	=	array('delete_record'=>'1');
		
		$this->db->where('list_id', $listid);

		$this->db->update($this->_table_list_management,$json_data,$this->session->userdata('userid'),$data);

		return TRUE;
	}
	
//----------------------------------------------------------------------
	/*
	* Record Status
	*/

	function record_status($list_id,$status)
	{
		$json_data	=	json_encode(array('list_id'	=>	$list_id,'list_status'=>$status));
		
		$data	=	array('list_status'=>$status);
		
		$this->db->where('list_id', $list_id);

		$this->db->update($this->_table_list_management,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}

//----------------------------------------------------------------------
	/*
	* Delete
	*/

	function delete_child($childlistid)
	{
		$this->db->where("list_id",$childlistid);

		$this->db->delete($this->_table_list_management);

		return true; 
	}
//------------------------------------------------------------------------
    /**
     * 
     * Insert User Data for Registration
     * @param array $data
     * return integer
     */

	function update_list($list_id,$data)
	{
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('list_id', $list_id);

		$this->db->update($this->_table_list_management,$json_data,$this->session->userdata('userid'),$data);

		return TRUE;
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function update_record($list_id, $data)
	{
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('list_id', $list_id);
		$this->db->update($this->_table_list_management,$json_data,$this->session->userdata('userid'),$data);

		return TRUE;
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function get_list_type()
	{
		$this->db->select("list_type"); 
		$this->db->group_by("list_type");
		$this->db->order_by("list_order", "DESC");

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function get_type_name($parent_id)
	{
		$this->db->select("list_type"); 
		$this->db->where("list_id",$parent_id);
		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row()->list_type;
		}
	}
//----------------------------------------------------------------------

/*	function child_data($parent_id)
	{
		$this->db->select("*"); 
		$this->db->where("list_id",$parent_id);

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();

		}

	}*/
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function get_list_type_more()
	{
		$this->db->select("list_type"); 
		$this->db->group_by("list_type");
		$this->db->order_by("list_order", "DESC");
		$this->db->where("list_type",'conversion');
		$this->db->or_where("list_type",'postponed');
		$this->db->or_where("list_type",'rejected');

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function get_list_type_rule_qualification()
	{
		$this->db->select("list_type"); 
		$this->db->group_by("list_type");
		$this->db->order_by("list_order", "DESC");
		$this->db->where("list_type",'rules');
		$this->db->or_where("list_type",'qualification');
		$this->db->or_where("list_type",'nature_project_site');
		$this->db->or_where("list_type",'nature_project');
		$this->db->or_where("list_type",'business_type');
		$this->db->or_where("list_type",'activity_project');
		$this->db->or_where("list_type",'project_employment');
		$this->db->or_where("list_type",'project_type');

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function rules_qualification()
	{
		$this->db->select("list_type"); 
		$this->db->group_by("list_type");
		$this->db->order_by("list_order", "DESC");
		$this->db->where("list_type",'rules');
		$this->db->or_where("list_type",'qualification');

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}	
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function get_all_list_type()
	{
		$this->db->select("list_type,list_id"); 
		$this->db->group_by("list_type"); 
		$this->db->order_by("list_order", "DESC");
		$this->db->where("list_parent_id", "0");
		$this->db->where("list_type !=", "departments");
		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function get_rules()
	{
		$this->db->where("list_type","rules"); 
		$this->db->order_by("list_order", "DESC");
		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function get_qualification()
	{
		$this->db->where("list_type","qualification"); 
		$this->db->order_by("list_order", "DESC");
		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function get_rule_qualification()
	{
		$this->db->where("list_type","qualification");
		$this->db->where("list_type","rules"); 
		$this->db->order_by("list_order", "DESC");

		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function get_type_id($type)
	{
		$this->db->where("list_type",$type);
		$query = $this->db->get($this->_table_list_management);

		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function get_all_document_list($charity_type_id,$docid=0)
	{
		$this->db->select("documentid,documenttype,isrequired,documentstatus,documentorder,charity_type_id");
		$this->db->from('ah_document_required');
		
		if($docid	!=	0 && $docid	!=	'')
		{
			$this->db->where("documentid",$docid);
			$query = $this->db->get();
			return $query->row();
		}
		else
		{
			$this->db->where("charity_type_id",$charity_type_id);		
			$this->db->order_by("documentorder", "ASC");
			$query = $this->db->get();
			return $query->result();
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/	
	function savedocumenttype()
	{
		$documentid			=	$this->input->post("documentid");
		$charity_type_id	=	$this->input->post("charity_type_id");
		$documenttype		=	$this->input->post("documenttype");
		$isrequired			=	$this->input->post("isrequired");
		$documentorder		=	$this->input->post("documentorder");
		$documentstatus		=	$this->input->post("documentstatus");
		
		$ah_document_required = array(
								'charity_type_id'	=>	$charity_type_id,
								'documenttype'		=>	$documenttype,
								'isrequired'		=>	$isrequired,
								'documentorder'		=>	$documentorder,
								'documentstatus'	=>	$documentstatus);
		if($documentid!='')
		{
			$this->db->where('documentid', $documentid);
			$this->db->update('ah_document_required',json_encode($ah_document_required),$this->session->userdata('userid'),$ah_document_required);
		}
		else
		{
			$this->db->insert('ah_document_required',$ah_document_required,json_encode($ah_document_required),$this->session->userdata('userid'));
		}
	}
//----------------------------------------------------------------------
	/*
	* 
	* 
	*/	
	function savefinancial_management()
	{
		$financialid	=	$this->input->post("financialid");
		$people_start	=	$this->input->post("people_start");
		$people_end		=	$this->input->post("people_end");
		$priority		=	$this->input->post("priority");
		$amount			=	$this->input->post("amount");
		$netincome		=	$this->input->post("netincome");
		$notes			=	$this->input->post("notes");
		$ah_financial 	= array(
							'people_start'	=>	$people_start,
							'people_end'	=>	$people_end,
							'priority'		=>	$priority,
							'amount'		=>	$amount,
							'netincome'		=>	$netincome,
							'notes'			=>	$notes);
		if($financialid	!=	'')
		{
			$this->db->where('financialid', $financialid);
			$this->db->update('ah_financial',json_encode($ah_financial),$this->session->userdata('userid'),$ah_financial);
		}
		else
		{
			$this->db->insert('ah_financial',$ah_financial,json_encode($ah_financial),$this->session->userdata('userid'));
		}
	}	
//----------------------------------------------------------------------	
}
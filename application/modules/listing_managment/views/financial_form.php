<div class="row col-md-12">
  <form action="" method="POST" id="add_financial_management" name="add_financial_management" autocomplete="off">
    <div class="form-group col-md-3">
      <label for="basic-input">عدد افراد</label>
      <input type="text" class="form-control req NumberInput" value="<?PHP echo $ff->people_start; ?>" name="people_start"  id="people_start"  placeholder="عدد افراد"/>
    </div>
    <div class="form-group col-md-3">
      <label for="basic-input">عدد افراد</label>
      <input type="text" class="form-control req NumberInput" value="<?PHP echo $ff->people_end; ?>" name="people_end"  id="people_end"  placeholder="عدد افراد"/>
    </div>
    <div class="form-group col-md-3">
      <label for="basic-input">الفئة المستحقة</label>
      <?PHP number_drop_box_arabic('priority',$ff->priority); ?>
    </div>
    <div class="form-group col-md-3">
      <label for="basic-input">المبلغ</label>
      <input type="text" class="form-control req NumberInput" value="<?PHP echo $ff->amount; ?>" name="amount"  id="amount"  placeholder="المبلغ"/>
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">صافي الدخل</label>
      <input type="text" class="form-control req NumberInput" value="<?PHP echo $ff->netincome; ?>" name="netincome"  id="netincome"  placeholder="صافي الدخل"/>
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">الملا حظات</label>
      <input type="text" class="form-control" name="notes" value="<?PHP echo $ff->notes; ?>"  id="notes"  placeholder="الملا حظات"/>
    </div>
   
    <input type="hidden" name="financialid" id="financialid" value="<?php echo $ff->financialid;?>"/>
  </form>
  <br clear="all">
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="submit_financial_management();" value="حفظ" />
  </div>
</div>

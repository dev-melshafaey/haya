<div class="col-md-12">
  <div class="panel panel-default panel-block" style="padding: 10px 10px;">
    <form method="POST" id="ah_document_required" name="ah_document_required">
      <input type="hidden" name="documentid" id="documentid" value="<?PHP echo $item->documentid; ?>">
      <div class="form-group col-md-6">
        <label for="basic-input"><strong>المساعدات:</strong></label>
        <?PHP echo $this->haya_model->create_dropbox_list('charity_type_id','charity_type',$item->charity_type_id,0,'req'); ?> </div>
      <div class="form-group col-md-6">
        <label for="basic-input"><strong>هزا الملف \ المستند المطلوب: </strong></label><br>
          نعم  <input type="radio" name="isrequired" <?PHP if($item->isrequired=='' || $item->isrequired==1) { ?> checked <?PHP } ?> id="isrequired" value="1">
          لا   <input type="radio" name="isrequired" <?PHP if($item->isrequired==0) { ?> checked <?PHP } ?> id="isrequired" value="0">
      </div>     
      <div class="form-group col-md-11">
        <label for="basic-input"><strong>الملف \ المستند:</strong></label>
        <input type="text" class="form-control req" value="<?PHP echo $item->documenttype; ?>" placeholder="الملف \ المستند" id="documenttype" name="documenttype" class="req form-control" />
      
      </div>
      <br clear="all">
      <div class="form-group col-md-6">
        <label for="basic-input"><strong>حالة:</strong></label>
        <?PHP status_dropbox('documentstatus',$item->documentstatus); ?>
      </div>
      <div class="form-group col-md-6">
        <label for="basic-input"><strong>ترتيبة:</strong></label>
        <?PHP number_drop_box('documentorder',$item->documentorder); ?>
      </div>
      <div class="form-group col-md-11">
        <button type="button" id="save_document" onClick="saveDocumentList();" name="save_document" class="btn btn-success">حفظ</button>
      </div>
    </form>
    <br clear="all">
  </div>
</div>

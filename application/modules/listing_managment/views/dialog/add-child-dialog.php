<?PHP
	$t = $type;
	$d = $data;
	$labels	=	$this->config->item('list_types');
	if($t=='child') { ?>

<div class="row col-md-12">
  <form action="" method="POST" id="add_parent" name="add_parent" autocomplete="off">
    <input type="hidden" id="parent_id" name="parent_id" value="<?php echo $parent_id; ?>"/>
    <input type="hidden" id="list_type" name="list_type" value="<?php echo $list_type; ?>"/>
    <div class="form-group col-md-6">
      <label for="basic-input">إسم</label>
      <input type="text" class="form-control req" value="<?php //echo $d->list_name; ?>" name="add_sub"  id="add_sub"  placeholder="إسم"/>
    </div>
    <div class="form-group col-md-3">
      <label for="basic-input">ترتيب&lrm;‎</label>
      <input type="text" name="list_order" id="list_order" class="form-control" />
    </div>
  </form>
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" onClick="save_qaima_child_data();" class="btn btn-success btn-lrg" name="save_data_form_new"  value="حفظ" />
  </div>
</div>
<?PHP } else { ?>
<div class="row col-md-12">
  <form action="" method="POST" id="save_data_form2" name="save_data_form2">
    <div class="form-group col-md-6">
      <label for="basic-input">إسم</label>
      <input type="text" class="form-control req" value="<?php echo $data->list_name; ?>" name="list_name"  id="list_name"  placeholder="اسم القائمة"/>
    </div>

    <?php if($data->list_type == ''):?>
    <div class="form-group col-md-3">
      <select name="list_type" id="list_type" class="form-control">
         <?php foreach($list_types as $value):?>
        <option value="<?php echo $value;?>" <?php if($single_list->list_type	==	$value):?> selected="selected" <?php endif;?>><?php echo $labels[$value]['ar'];?></option>
        <?php endforeach;?>
      </select>
    </div>
    <?php else:?>
    <div class="form-group col-md-3">
      <label for="basic-input">‎</label>
      <select name="list_type" id="list_type" class="form-control">
 <?php foreach($list_types as $value):?>
        <?php if($data->list_type == $value):?>
        <option value="<?php echo $value;?>" <?php if($single_list->list_type	==	$value):?> selected="selected" <?php endif;?>><?php echo $labels[$value]['ar'];?></option>
        <?php endif;?>
        <?php endforeach;?>
      </select>
    </div>
    <?php endif;?>
    <!--<div class="form-group col-md-3">
      <label for="basic-input">الوضع&lrm;‎</label>
      <select name="list_status" id="list_status" class="form-control">
        <option value="1" <?php if($single_list->list_status	==	'1'):?> selected="selected" <?php endif;?>>نشط</option>
        <option value="0" <?php if($single_list->list_status	==	'0'):?> selected="selected" <?php endif;?>>غير نشط</option>
      </select>
    </div>-->
    <div class="form-group col-md-3">
      <label for="basic-input">ترتيب&lrm;‎</label>
      <input type="text" name="list_order" id="list_order" class="form-control" value="<?php echo $data->list_order; ?>"/>
    </div>
    <input type="hidden" name="list_id" id="list_id" value="<?php echo isset($data->list_id) ? $data->list_id : NULL;?>" />
  </form>
</div>
<div class="row col-md-12">
      <div class="form-group  col-md-12">
        
        <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="submit_form();" value="حفظ" />
      </div>
    </div>
<!--<div class="row col-md-12">
<div class="form-group  col-md-12">
      <input type="button" onClick="save_barnamij_parent_data();" class="btn btn-success btn-lrg" name="save_data_form_new"  value="حفظ" />
  </div>
</div>-->

<?PHP } ?>
<script>
$(function(){
restrict_number();
});
</script>
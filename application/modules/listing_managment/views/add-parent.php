<?php $labels	=	$this->config->item('list_types');?>
<div class="row col-md-12">
  <form action="" method="POST" id="save_data_form2" name="save_data_form2">
    <div class="form-group col-md-6">
      <label for="basic-input">إسم</label>
      <input type="text" class="form-control req NumberInput" name="list_name"  id="list_name"  placeholder="اسم القائمة"/>
    </div>
    <?php if($type->list_type == ''):?>
    <div class="form-group col-md-3">
      <label for="basic-input">‎</label>
      <select name="list_type" id="list_type" class="form-control">
        <?php foreach($list_types as $value):?>
         <?php if($value != 'departments'):?>
        	<option value="<?php echo $value;?>"><?php echo $this->haya_model->get_type_name($value);?></option>
           <?php endif;?>  
        <?php endforeach;?>
      </select>
    </div>
    <?php else:?>
    <div class="form-group col-md-3">
      <label for="basic-input">‎</label>
      <select name="list_type" id="list_type" class="form-control">
        <?php foreach($list_types as $value):?>
         <?php if($type->list_type == $value):?>
        <option value="<?php echo $value;?>" <?php if($type->list_type	==	$value):?> selected="selected" <?php endif;?>><?php echo $this->haya_model->get_type_name($value);?></option>
        <?php endif;?>
		<?php endforeach;?>
      </select>
    </div>
    <?php endif;?>
    <div class="form-group col-md-3">
      <label for="basic-input">ترتيب&lrm;‎</label>
      <input type="text" name="list_order" id="list_order" class="form-control" />
    </div>
    <input type="hidden" name="list_id" id="list_id"/>
    <!--<input type="hidden" name="list_type" id="list_type" value="<?php echo $type->list_type;?>"/>-->
  </form>
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="submit_form();" value="حفظ" />
  </div>
</div>

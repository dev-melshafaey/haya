<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Forall extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

   public function index()
    {}
//------------------------------------------------------------------------

  	/**
   	* Dynamic Forms Listing Page
   	* @param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]  = $this->haya_model->get_all_custom_form($moduleid);
		
		$this->_data["userid"] = $this->_login_userid;
		 
		 // Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);	 
	 }   
}

?>
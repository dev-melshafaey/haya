<div id="user-profile">
<div class="row" dir="rtl">
    <div class="col-md-12 fox leftborder">
      <h4 class="panel-title customhr">شاشة برنامج المخازن ( المخزن الرئيسي )
</h4>
            <div id="collapseTwo" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                <div class="form-group col-md-12">
                 	<table width="95%" border="0" cellspacing="0" cellpadding="10" align="center">
                      <tr>
                            <td class="right"><strong>الجهة الطالبة / المستفيد </strong><br /><?php echo $rows->name;?></td>
                            <td><strong>الجنسية</strong><br /><?php echo $nationality->list_name;?></td>
                            <td><strong>الهاتف</strong><br /><?php echo $rows->phone;?></td>
                            <td><strong>تاريخ الانشاء</strong><br /><?php echo  arabic_date($rows->currentdate);?></td>
                      </tr>
                      <tr>
                            <td class="right"><strong>المرفقات </strong><br /> <?php if($rows->attachment !=""){
				   $i=0;
				   $attachment = @explode(',',$rows->attachment);
				   foreach($attachment as $att){
					   if($att !=""){
						   $i++;
				   ?>
                  
               <a class="fancybox-button" rel="gallery1" href="<?php echo base_url()?>resources/stores/request/<?php echo $rows->approvedId."/".$att?>"><?php  echo $controller->getfileicon($att,base_url().'resources/stores/request/'.$rows->approvedId);?></a>
             
              
              <?php } } }?></td>
              <?php
			  $this->db->select('ah_userprofile.fullname');
				$this->db->from('ah_userprofile');	
				$this->db->where("ah_userprofile.userid",$rows->approvedId);
				$rowuser = $this->db->get(); 
				foreach($rowuser->result() as $us1){$approvusr = $us1->fullname;}
			  ?>
                            <td><strong>الشخص المعتمد</strong><br /><?php echo$approvusr;?></td>
                            <td><strong>تاريخ الموافقة</strong><br /><?php echo  arabic_date($rows->appDate);?></td>
                      </tr>
                      <tr>
                      <td class="right" colspan="3"><strong>البيان </strong><br /><?php echo $rows->notes;?></td>
                           
                      </tr>
                       <tr>
                            <td class="right" colspan="3"><strong>البيان </strong><br /><?php echo $rows->desc_user;?></td>
                           
                      </tr>
                      
                      
                </table>      
                
                	
                    
                    
                 </div>  
           
                 <div class="form-group col-md-12">
                    <h4 class="yateem_h4">إضافة عناصر
                          
                    </h4>
                        <table class="table table-bordered table-striped "  id="external_daily_report_addId" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                    <th width="10%">رقم</th>
                                    <th  width="20%">نوع المادة</th>
                                    <th  width="10%">الكمية</th>
                                    <th  width="10%">الكمية السابقة</th>
                                     <th  width="40%">البيان</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							$extreliefTMId =0;
							if(isset($store_item_request_list)){
								if(count($store_item_request_list)>0){
									foreach($store_item_request_list as $reliefteam) { 
									$extreliefTMId ++;
										
											$items ='';
											foreach($all_items as $type){ if($reliefteam->items == $type->itemid){$items =$type->itemname;}}
											$balance =0;
											$this->db->select('quantity');
											$this->db->from('ah_inventory_qty');						
											$this->db->where("inventoryid",$reliefteam->items);
											$query = $this->db->get();
											if($query->num_rows() > 0)
											{
												$row = $query->row();
												$balance =$row->quantity;
											}
									
									?>
                                <tr id="<?php echo $reliefteam->stItmReqListId;?>_row">
                                    <td id="<?php echo $reliefteam->stItmReqListId;?>_daily_id"><?php echo $extreliefTMId;?></td>
                                    <td id="<?php echo $reliefteam->stItmReqListId;?>_daily_items"><?php echo $items;?></td>
                                    <td id="<?php echo $reliefteam->stItmReqListId;?>_daily_qty"><?php echo $reliefteam->qty;?></td>
                                    <td id="<?php echo $reliefteam->stItmReqListId;?>_daily_qty"><?php echo $reliefteam->prevqty;?></td>
                                    <td id="<?php echo $reliefteam->stItmReqListId;?>_daily_description"><?php echo $reliefteam->description;?></td>
                                   
                                </tr> 
                            <?php } } } $extreliefTMId ++;?>
                              
                            </tbody>
                        </table>
                    </div>
            
            
            </div>
            
           
        
                </div>
            </div>
  </div>
</div>
<div class="row" style="text-align: center;">
  <button type="button" id="" onClick="printthepage('user-profile');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
</div>
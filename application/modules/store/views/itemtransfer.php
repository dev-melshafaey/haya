<?php $permissions	=	$this->haya_model->check_other_permission(array($moduleid));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
      <div class="col-md-12">
       <form method="POST" id="form_service" name="form_service" enctype="multipart/form-data">
        <div class="panel panel-default panel-block" style="padding: 10px 10px;">
        <div class="form-group col-md-12">
           <div class="row">
           <h4 style="color:#029625 !important; font-weight:bold;">تحويل من</h4>
                <div class="form-group col-md-3">
                  <label for="basic-input"><strong>حدد أول متجر :</strong></label>
                   <select class="form-control search-select  req" id="store_id" name="store_id" onChange="loaditems(this.value,'item');"     >
                       <option value="">--تحديد--</option>
                              <?php 
                               if(count($all_stores)>0){
                                  foreach($all_stores as $type){
                                         echo '<option value="'.$type->store_id.'" >'.$type->store_name.'</option>'; 
                                  }
                              }
                              ?>
              </select>
                </div>
               
                <div class="form-group col-md-3">
                <label for="basic-input"><strong>اسم العنصر:</strong></label>
                <select class="form-control search-select  req" id="item" name="item" onChange="loadqty(this.value,'qty');"      >
                   <option value="">--تحديد--</option>
                         
                </select>
                </div>
                 <div class="form-group col-md-3">
                <label for="basic-input"><strong>الكمية المتوفرة:</strong></label>
                <input type="text" class="form-control req" id="qty" name="qty" readonly  />
                </div>
                 <div class="form-group col-md-3">
                <label for="basic-input"><strong>نقل الكمية:</strong></label>
                <input type="text" class="form-control req" id="qtyTo" name="qtyTo"   />
                </div>
                
           </div> 
           <div class="row">
           <h4 style="color:#029625 !important; font-weight:bold;">تحويل من</h4>
           <div class="form-group col-md-3">
                  <label for="basic-input"><strong>حول إلى :</strong></label>
                   <select class="form-control search-select  req" id="store_idTo" name="store_idTo"  >
                       <option value="">--تحديد--</option>
                              <?php 
                               if(count($all_stores)>0){
                                  foreach($all_stores as $type){
                                         echo '<option value="'.$type->store_id.'" >'.$type->store_name.'</option>'; 
                                  }
                              }
                              ?>
              </select>
                </div>
           </div>
           
         </div> 
             <div class="form-group col-md-6">
              <button type="button" id="save_service" name="save_service" class="btn btn-success">حفظ</button>
            </div>   
            
          <br clear="all">
        </div>
        <div class="panel panel-default panel-block" style="padding: 10px 10px; margin-top:20px;">
           <div class="form-group col-md-12">
          <?php if($itemid >0 ){?>
           <div class="row">
            <h4 style="color:#029625 !important; font-weight:bold;">نقل التفاصيل</h4>
            
                 <table class="table table-bordered table-striped "  id="external_daily" >
                    <thead style="background-color: #029625;">
                        <tr role="row" style="color:#fff !important;">
                            <th width="10%">رقم</th>
                            <th  width="20%">مخزن</th>
                            <th  width="20%">فئة</th>
                             <th  width="30%">الفئة الفرعية</th>
                             <th  width="10%">اسم العنصر</th>
                             <th  width="10%">الكمية</th>
                             <th  width="10%">تاريخ التحويل</th>
                             <th  width="10%">نقل من مخزن</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if(count($rows)>0){?>
                        <tr>
                            <td>1</td>
                            <td><?php 
							$all_stores	=	$this->store->get_all_stores($rows->store_id);
								echo $stores = $all_stores[0]->store_name;
							?></td>
                            <td><?php echo $this->haya_model->get_name_from_list($rows->list_category);?></td>
                            <td><?php echo $this->haya_model->get_name_from_list($rows->list_subcategory);?> </td>
                            <td><?php echo $rows->itemname;?></td>
                            <td><?php echo $this->haya_model->dataCount('ah_inventory_qty','inventoryid',$rows->itemid,'SUM','quantity');?></td>
                            <td><?php echo arabic_date($rows->transferDate);?> </td>
                            <td><?php 
							$all_stores	=	$this->store->get_all_stores($rows->transferfromStore_id);
								echo $stores = $all_stores[0]->store_name;
							?></td>
                        </tr>
                        <?php }?>
                    </tbody>
                 </table>
                
                
               
           </div> 
           
         <?php }?>
           
               
            </div>  
            
                  
         
          <br clear="all">
        </div>
         </form>
        
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
 $(document).ready(function(){
	$('#save_service').click(function () {
		check_my_session();
        $('#form_service .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_service .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
       
 		var redline = $('#form_service .parsley-error').length;
       
		if($('#qtyTo').val() > $('#qty').val()){
			
			$('#qtyTo').css('border','1px solid rgba(247,32,35,1.00)');
			redline = redline+1;
		}
		else{
			$('#qtyTo').css('border','1px solid #029625');	
		}
		if($('#store_id').val() !="" && $('#store_idTo').val() !=""){
			if($('#store_id').val() == $('#store_idTo').val()){
				
				$('#store_idTo').css('border','1px solid rgba(247,32,35,1.00)');
				redline = redline+1;
			}
			else{
				$('#store_idTo').css('border','1px solid #029625');	
			}
		}
		
		
		 ht += '</ul>';
        if (redline <= 0) {           
                $("#form_service").submit();
            
        }
        else 
		{
            show_notification_error(ht);
        }
    });
});
</script>
</div>
</body>
</html>
<script>


function loaditems(val,id){
	$('#'+id).empty();
	$('#'+id).append("<option value=''>--تحديد--</option>");
		$.ajax({
			url: config.BASE_URL+'store/loaditems/',
			type: "POST",
			data:{'store_id':val},
			dataType: "json",
			success: function(response)
			{	
				var data = response.data;
				$('#'+id).append(data);
				
			 }
		});
}
function loadqty(val,id){
	$('#'+id).empty();
	$('#'+id).append("<option value=''>--تحديد--</option>");
		$.ajax({
			url: config.BASE_URL+'store/loadqty/',
			type: "POST",
			data:{'itemid':val},
			dataType: "json",
			success: function(response)
			{	
				var data = response.data;
				$('#'+id).val(data);
				
			 }
		});
}


/*$(document).ready(function() {
		$("select").searchable({
			maxListSize: 15,						// if list size are less than maxListSize, show them all
			maxMultiMatch: 15,						// how many matching entries should be displayed
			exactMatch: false,						// Exact matching on search
			wildcards: true,						// Support for wildcard characters (*, ?)
			ignoreCase: true,						// Ignore case sensitivity
			latency: 200,							// how many millis to wait until starting search
			warnMultiMatch: 'top {0} matches ...',	// string to append to a list of entries cut short by maxMultiMatch 
			warnNoMatch: 'no matches ...',			// string to show in the list when no entries match
			zIndex: 'auto'							// zIndex for elements generated by this plugin
	   	});
});
*/

</script>

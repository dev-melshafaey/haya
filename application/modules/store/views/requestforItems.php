<?php $permissions	=	$this->haya_model->check_other_permission(array($moduleid));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block" style="padding: 10px 10px;">
          <form method="POST" id="form_service" name="form_service" enctype="multipart/form-data">
           <input type="hidden"  name="stItmReqId" id="stItmReqId" value="<?php if(isset($rows->stItmReqId)){echo $rows->stItmReqId;}else{ echo "0";}?>" />
            <?php if($servicedepartment == $userroleid){?>
           <div class="form-group col-md-6">
           <?php } else{?>
           <div class="form-group col-md-12">
           <?php }?>
                    <h4 class="yateem_h4">إضافة عناصر
                          
                    </h4>
                        <table class="table table-bordered table-striped "  id="external_daily" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                    <th width="5%">رقم</th>
                                    <th  width="20%">نوع المادة</th>
                                    <th  width="5%">الكمية</th>
                                     <th  width="30%">البيان</th>
                                   <?php if( $userroleid  == $servicedepartment){?>
                                     <th  width="10%">الكمية المتوفرة</th> 
                                     <th  width="10%">التوازن الكمية</th> 
                                     <?php } ?>
                                    <th  width="20%">الإجراءات</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							$extreliefTMId =0;
							if(isset($store_item_request_list)){
								if(count($store_item_request_list)>0){
									foreach($store_item_request_list as $reliefteam) { 
									$extreliefTMId ++;
										$action ='';
										//$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_store_item_request_list/'.$reliefteam->stItmReqListId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
											if($reliefteam->userid == $login_userid or $servicedepartment == $userroleid ){
												$action	.= ' <a onclick="edit_store_item_request_list(\''.$reliefteam->stItmReqListId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
											}
											if($permissions_d==	1) 
											{
												$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->stItmReqListId.'" data-url="'.base_url().'store/delete_store_item_request_list/'.$reliefteam->stItmReqListId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
											}
											$items ='';
											foreach($all_items as $type){ if($reliefteam->items == $type->itemid){$items =$type->itemname;}}
											$balance =0;
											$this->db->select('quantity');
											$this->db->from('ah_inventory_qty');						
											$this->db->where("inventoryid",$reliefteam->items);
											$query = $this->db->get();
											if($query->num_rows() > 0)
											{
												$row = $query->row();
												$available =$row->quantity;
											}
											$balance =$available - $reliefteam->qty;
									
									?>
                                <tr id="<?php echo $reliefteam->stItmReqListId;?>_row">
                                    <td id="<?php echo $reliefteam->stItmReqListId;?>_daily_id"><?php echo $extreliefTMId;?></td>
                                    <td id="<?php echo $reliefteam->stItmReqListId;?>_daily_items"><?php echo $items;?></td>
                                    <td id="<?php echo $reliefteam->stItmReqListId;?>_daily_qty"><?php echo $reliefteam->qty;?></td>
                                    <td id="<?php echo $reliefteam->stItmReqListId;?>_daily_description"><?php echo $reliefteam->description;?></td>
                                    <?php if( $userroleid  == $servicedepartment){?>
                                    <td style="color:#F00; font-weight:bold;" id="<?php echo $reliefteam->stItmReqListId;?>_daily_balance"><?php echo $available;?></td>
                                    <td style="color:#F00; font-weight:bold;" id="<?php echo $reliefteam->stItmReqListId;?>_daily_balance"><?php echo $balance;?></td>
                                    <?php }?>
                                    <td><?php echo $action;?></td>
                                </tr> 
                            <?php } } } $extreliefTMId ++;?>
                             <?php if($servicedepartment != $userroleid){?> 
                             <tr id="last_daily">
                                
                                 <td colspan="5">
                                 <div style="float:right; margin-bottom:10px; color:#F00;"><b>الكمية المتوفرة: </b><span style="font-size:14px; font-weight:bold;" id="availableqty<?php echo $extreliefTMId?>"></span></div>
                                 <br style="clear:both;"/>
                                 <div style="float:right; width:20%">
                                  <label for="basic-input" style="float:right; margin-left:10px;" ><strong>رقم:</strong></label>
                                 <input type="text" class="form-control " value="<?php echo $extreliefTMId?>" placeholder="رقم" name="stItmReqListIddauto" id="stItmReqListIddauto"  readonly style="float:right;"  />
                                 </div>
                                 <div style="float:right;  width:40%;">
                                  <label for="basic-input" style="float:right; margin-left:10px; margin-right:20px;" ><strong>نوع المادة:</strong></label><br/> 
                                 <select class="form-control" id="items" name="items" style="float:right;    width:400px; position:absolute;  margin-right:20px; " onChange="chkQty(this.value,'<?php echo $extreliefTMId?>');"   >
                                   <option value="0">--تحديد--</option>
                                          <?php 
                                           if(count($all_items)>0){
											  foreach($all_items as $type){
												 
													  echo '<option value="'.$type->itemid.'" >'.$type->itemname.'</option>';
												  
												  
											  }
										  }
                                          ?>
                          </select>
                          </div>
                           <div style="float:right;  width:22%;">
                           <label for="basic-input" style="float:right; margin-left:10px; margin-right:10px;" ><strong>الكمية:</strong></label> 
                          <input type="number" class="form-control " value="" placeholder="الكمية" name="qty" id="qty"  style="float:right; "  onKeyUp="validateQty(this.value,'items','availableqty<?php echo $extreliefTMId?>','qty');"  />
                          </div>
                          <br style="clear:both;"/>
                          <textarea class="form-control  " value="" placeholder="البيان" name="description" id="description" rows="5" style="margin-top:5px;" ></textarea>
                          <br/>
                           <button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float:right; margin-bottom: 8px; " onClick="add_store_item_request_list('0');" style="float:right; width:10%;"  >اضافة</button>
                          </td>
                                 
                                </tr> 
                                <?php }?>   
                            </tbody>
                        </table>
                         <?php if($servicedepartment != $userroleid){?>
                         <div style="   margin-top: 25px;">
                         <a  id="save"  class="btn btn-success"  href="<?php echo base_url()?>store/requestforItems_listing">حفظ</a></div>
                         <?php }?>
                    </div>
           <?php if($servicedepartment == $userroleid){?>
             <div class="form-group col-md-6">
             <div class="form-group col-md-12">
              <label for="basic-input"><strong>البيان :</strong></label>
              <textarea name="notes" placeholder="البيان" class="form-control req" style="resize:none; height:200px;" id="notes"><?php echo $rows->notes?></textarea>
             </div>
             <div class="form-group col-md-12">
              <label for="basic-input"><strong>المرفقات :</strong></label>
              <input type="file" class="form-control" name="attachment[]" id="attachment" multiple /><br/>
               <input type="hidden"  name="attachment_old" id="attachment_old" value="<?php echo $rows->attachment?>" />
               <?php if($rows->attachment !=""){
				   $i=0;
				   $attachment = @explode(',',$rows->attachment);
				   foreach($attachment as $att){
					   if($att !=""){
						   $i++;
				   ?>
                   <div id="picture_<?php echo $i;?>" style="float:right;">
               <a class="fancybox-button" rel="gallery1" href="<?php echo base_url()?>resources/stores/request/<?php echo $login_userid."/".$att?>"><?php  echo $controller->getfileicon($att,base_url().'resources/stores/request/'.$login_userid);?></a>
              <a class="iconspace" href="#deleteDiag" onClick="show_delete_dialogs(this,'picture_<?php echo $i;?>','<?php echo $att?>');" id="<?php echo  $rows->stItmReqId ?>" data-url="<?php echo base_url()?>store/removeimage/<?php echo $rows->stItmReqId ?>/<?php echo $att ?>"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>
              </div>
              <?php } } }?>
            </div>
             <div class="form-group col-md-6">
             <button type="button" id="save_service" name="save_service" class="btn btn-success" style="   margin-right: 15px;">حفظ</button>
            </div>
            </div>
              
            <?php } ?> 
            
            <br clear="all">
            <div class="form-group col-md-6">
            
            </div>
          </form>
          <br clear="all">
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
 $(document).ready(function(){
	$('#save_service').click(function () {
		check_my_session();
        $('#form_service .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_service .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_service .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                $("#form_service").submit();
            
        }
        else 
		{
            show_notification_error(ht);
        }
    });
});
</script>
</div>
</body>
</html>
<script>
function add_store_item_request_list(stItmReqListId){
	var error = false;
	if ($('#items').val() == '0') {
		   $("#items").css('border','1px solid rgba(247,32,35,1.00)');
		   error = true;
	 }
	 else{
		  $("#items").css('border','1px solid #029625');
	 }
	 if ($('#qty').val() > parseInt($('#availableqty').html())) {
		   $("#qty").css('border','1px solid rgba(247,32,35,1.00)');
		   error = true;
	 }
	 else{
		  $("#qty").css('border','1px solid #029625');
	 }
	 if(!error){
	 
		$.ajax({
			url: config.BASE_URL+'store/add_store_item_request_list/',
			type: "POST",
			data:{'stItmReqListIddauto':$('#stItmReqListIddauto').val(),'items':$('#items').val(),'qty':$('#qty').val(),'description':$('#description').val(),'stItmReqListId':stItmReqListId,'stItmReqId':$('#stItmReqId').val()},
			dataType: "json",
			success: function(response)
			{	
				$('#qty').val('');
				$('#description').val('');
				var stItmReqListIddauto = $('#stItmReqListIddauto').val();
				stItmReqListIddauto = parseInt(stItmReqListIddauto) + 1;
				$('#stItmReqListIddauto').val(stItmReqListIddauto);
				$('#external_daily tr#last_daily').before(response.data);
				$('#stItmReqId').val(response.stItmReqId);
			 }
		});
	 }
}

function chkQty(vals,extreliefTMId){
	var stItmReqId = $('#stItmReqId').val();
	//var items = $('#items').val()
	if(vals > 0 ){
		$.ajax({
			url: config.BASE_URL+'store/chkQty/',
			type: "POST",
			data:{'vals':vals,'stItmReqId':stItmReqId},
			dataType: "json",
			success: function(response)
			{	
				$('#availableqty'+extreliefTMId).html(response.data);
			 }
		});
	}
}
function validateQty(vals,items,availableqty,qty1){
	var error = false;
	if ($('#'+items).val() == 0) {
		   $('#'+items).css('border','1px solid rgba(247,32,35,1.00)');
		   error = true;
	 }
	 else{
		  $('#'+items).css('border','1px solid #029625');
	 }
	var qty = $('#'+availableqty).html();
	qty = parseInt(qty);
	if(vals > qty){
		$('#'+qty1).css('border','1px solid rgba(247,32,35,1.00)');
	}
	else{
		 $('#'+qty1).css('border','1px solid #029625');
	}
}
function edit_store_item_request_list(stItmReqListId){
	var id = stItmReqListId;//$('#'+stItmReqListId+'_daily_id').html();
	var autoid = $('#'+stItmReqListId+'_daily_id').html();
	var items = $('#'+stItmReqListId+'_daily_items').html();
	var qty = $('#'+stItmReqListId+'_daily_qty').html();
	var description = $('#'+stItmReqListId+'_daily_description').html();
var itemid =0;
	var cont = "<td colspan='5'><div style='float:right; margin-bottom:10px; color:#F00;'><b>الكمية المتوفرة: </b><span style='font-size:14px; font-weight:bold;' id='availableqty"+id+"'></span></div><br style='clear:both;'/><input type='text' class='form-control ' value='"+autoid+"' placeholder='رقم' name='stItmReqListIddauto' id='stItmReqListIddauto"+id+"'  readonly style='float:right; width:10%;'  /><select class='form-control' id='items"+id+"' name='items' style='float:right; width:40%;' onChange='chkQty(this.value,\""+id+"\");'   ><option value='0'>--تحديد--</option>";
                          <?php 
   if(count($all_items)>0){
	  foreach($all_items as $type){
		?>
		var t1 ='<?php echo $type->itemname;?>'; 
		if(items == t1){
			itemid='<?php echo $type->itemid;?>';
			 cont += "<option value='<?php echo $type->itemid;?>' selected><?php echo $type->itemname?></option>";
		}
		else{
			 cont += "<option value='<?php echo $type->itemid;?>'><?php echo $type->itemname?></option>";
		}
			 
		  <?php 
	 
   }
  }
  ?>                
                          cont += "</select><input type='number' class='form-control ' value='"+qty+"' placeholder='الكمية' name='qty' id='qty"+id+"'  style='float:right; width:40%;'  onKeyUp='validateQty(this.value,\"items"+id+"\",\"availableqty"+id+"\",\"qty"+id+"\");'  /><button type='button' id='btn_external_reliefteam' class='btn btn-sm btn-success' style='float:left; margin-bottom: 8px; ' onClick='update_store_item_request_list(\""+id+"\");' style='float:left; width:10%;'  >اضافة</button><br/><textarea class='form-control  ' value='' placeholder='البيان' name='description' id='description"+id+"' rows='5' style='margin-top:5px;' >"+description+"</textarea></td>";
	$('#'+stItmReqListId+'_row').html(cont);
	chkQty(itemid,id);
}
function update_store_item_request_list(stItmReqListId){
		$.ajax({
			url: config.BASE_URL+'store/add_store_item_request_list/',
			type: "POST",
			data:{'stItmReqListIddauto':$('#stItmReqListIddauto'+stItmReqListId).val(),'items':$('#items'+stItmReqListId).val(),'qty':$('#qty'+stItmReqListId).val(),'description':$('#description'+stItmReqListId).val(),'stItmReqListId':stItmReqListId,'stItmReqId':$('#stItmReqId').val()},
			dataType: "json",
			success: function(response)
			{	
				$('#'+stItmReqListId+'_row').html(response.data);
			 }
		});
}
function delete_data_system()
{
	check_my_session();
	
    var data_id 	=	$('#delete_id').val();
    var action_url 	=	$('#action_url').val();
    var rowid 		=	'#' + data_id + '_durar_lm';
	
	
    var delete_data_from_system = $.ajax({
        url: action_url,
        dataType: "json",
        success: function (msg) 
		{
            show_notification('لقد تم حذف سجل');
            $('#deleteDiag').modal('hide');
			$('#'+msg.data).hide('slow');
        }
    });
}
$(document).ready(function() {
		$("select").searchable({
			maxListSize: 15,						// if list size are less than maxListSize, show them all
			maxMultiMatch: 15,						// how many matching entries should be displayed
			exactMatch: false,						// Exact matching on search
			wildcards: true,						// Support for wildcard characters (*, ?)
			ignoreCase: true,						// Ignore case sensitivity
			latency: 200,							// how many millis to wait until starting search
			warnMultiMatch: 'top {0} matches ...',	// string to append to a list of entries cut short by maxMultiMatch 
			warnNoMatch: 'no matches ...',			// string to show in the list when no entries match
			zIndex: 'auto'							// zIndex for elements generated by this plugin
	   	});
});


</script>
<script>
function show_delete_dialogs(durar,id,image) {
	check_my_session();
    var url_redirect = $(durar).attr("data-url");
    var did = $(durar).attr('id');
    $('#delete_id').val(did);
    $('#action_url').val(url_redirect);
    $('#deleteDiag').modal();
	var attachment_old = $('#attachment_old').val();
	var attachment = attachment_old.split(',');
	var index = attachment.indexOf(image);
	if (index > -1) {
	 attachment.splice(index, 1);
	}
	attachment.toString();
	$('#attachment_old').val(attachment);
	$('#'+id).html('');
}
</script>
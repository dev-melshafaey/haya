<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store extends CI_Controller 
{
//-------------------------------------------------------------------------------	
	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;
//-------------------------------------------------------------------------------

	/*
	* Costructor
	*/
	
	public function __construct()
	{
		parent::__construct();

		// Loade Models
		$this->_data['controller']=$this; 
		$this->load->model('store_model','store');
		$this->_data['module']		=	$this->haya_model->get_module();
		$this->moduleid = 250;
		$this->mainmoduleid = 251;
		$this->transfermoduleid = 253;
		$this->servicedepartment = 1072;
		//$this->servicedepartment = 1729;
		$this->_data['servicedepartment']	=	$this->servicedepartment;
		// SET Login USER ID
		$this->_data['moduleid']			= $this->moduleid;
		$this->_data['mainmoduleid']			= $this->mainmoduleid;
		$this->_login_userid			=	$this->session->userdata('userid');
		$this->_data['login_userid']	=	$this->_login_userid;	
		
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);	
		$this->userroleid				=  $this->_data['user_detail']['profile']->userroleid;
		$this->_data['userroleid']	= $this->userroleid;
	}
//-------------------------------------------------------------------------------

	/*
	* Home listing of all products
	*/	
    public function requestforItems_listing()
    {
        $permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
		$this->_data["rows"] = $this->store->getdata($stItmReqId,'ah_store_item_request',"stItmReqId");
		
		$this->load->view('requestforItems_listing',$this->_data);
		
    }
	public function ajax_requestforItems_listing()
    {
       
			if($this->userroleid  == $this->servicedepartment){
				$this->db->select('*');
				$this->db->from('ah_store_item_request');						
				$this->db->where("delete_record",'0');
				$this->db->where("types",'1');
				$this->db->order_by('status','DESC');
				$this->db->order_by('appDate','DESC');
				$this->db->order_by('stItmReqId','DESC');
			}else{
				$this->db->select('*');
				$this->db->from('ah_store_item_request');						
				$this->db->where("delete_record",'0');
				$this->db->where("types",'1');
				$this->db->where("userid",$this->_login_userid);
				$this->db->order_by('status','DESC');
				$this->db->order_by('stItmReqId','DESC');
			}
			
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
				$cnt =1;
					
			foreach($query->result() as $lc)
			{
				$action 	='';
				if($lc->stItmReqId)
				{
					$action 	.='<a onclick="alatadad(this);" data-url="'.base_url().'store/view_requestforItems/'.$lc->stItmReqId.'" href="#"><i class="my icon icon-eye-open"></i></a>';
				}
				if($lc->status != "Accepted"){
					if($lc->userid == $this->_login_userid or $this->userroleid  == $this->servicedepartment){
						$action	.= ' <a href="'.base_url().'store/requestforItems/'.$lc->stItmReqId.'" ><i class="icon-pencil"></i></a>';
					}
					
					if($permissions[$this->moduleid]['d']	==	1 and $lc->status != "Accepted") 
					{
						$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->stItmReqId.'" data-url="'.base_url().'store/requestforItemsdelete/'.$lc->stItmReqId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
					}
				}
				$status ='';
				if($lc->status == "Pending")
				{
					$status	=	'<img src="'.base_url().'assets/images/pending.png" style="width: 24px;" alt="Pending" title="Pending"/>';
				}
				else if($lc->status == "Accepted")
				{
					$status	=	'<img src="'.base_url().'assets/images/completed.png" style="width: 24px;" alt="Accepted" title="Accepted"/>';
				}
				else if($lc->status == "Rejected")
				{
					$status	=	'<img src="'.base_url().'assets/images/rejected.png" style="width: 24px;" alt="Rejected" title="Rejected"/>';
				}
				
				$this->db->select('ah_userprofile.fullname');
				$this->db->from('ah_userprofile');	
				$this->db->where("ah_userprofile.userid",$lc->userid);
				$rowuser = $this->db->get(); 
				foreach($rowuser->result() as $us1){$beneficiary = $us1->fullname;}
				$departments =$this->store->get_all_listmanagement($lc->department);
				$arr[] = array(
					"DT_RowId"		=>	$lc->stItmReqId.'_durar_lm',
					"رقم التسلسل" 		=>$cnt,	
					"المستفيد" 		=> $beneficiary,	
					"مستخدم النظام" 	=>	$departments->list_name,
					"تاريخ الانشاء" 	=>	arabic_date($lc->currentdate),
					"الحالة" 	=>$status,
					"الإجراءات" 		=>	$action
					);
					$cnt++;
					unset($action,$items);
			}
			
			
			
			$ex['data'] = $arr;
			echo json_encode($ex);
	
    }
	public function requestforItems($stItmReqId=NULL){
	  $permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
	  $this->_data['permissions_d']=$permissions[$this->moduleid]['d'];
	  $datas = array();
	  if(isset($_POST["stItmReqId"])){
		  $datas["stItmReqId"] = $stItmReqId = $_POST["stItmReqId"];
		   $datas["notes"] =  $_POST["notes"];
		  $datas["status"] =  "Accepted";
		  $datas["appDate"] =date('Y-m-d');
			$datas["approvedId"] = $this->_login_userid;
		  if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
			{
				$names = array();
				$files = $_FILES["attachment"];
				 foreach ($files['name'] as $key => $image) {
					if(!empty($files['name'][$key]))
					{
						$_FILES['images']['name']= $files['name'][$key];
						$_FILES['images']['type']= $files['type'][$key];
						$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['images']['error']= $files['error'][$key];
						$_FILES['images']['size']= $files['size'][$key];
						$names[] =	$this->upload_file($this->_login_userid,'images','resources/stores/request');
					}
					
				}
				if($data["attachment_old"] !=""){
					if(count($names)>0)	$datas['attachment']	=	$_POST["attachment_old"].",".@implode(',',$names);
					else{$datas['attachment']	=	$_POST["attachment_old"];}
				}
				else{
					$datas['attachment']	=	@implode(',',$names);	
				}
				
			}
			else{
				$datas['attachment']	=	$_POST["attachment_old"];
			}
		  
		  
		  
		  
		  $stItmReqId = $this->store->savestore($datas,'ah_store_item_request','stItmReqId');
		   if($this->userroleid == $this->servicedepartment) {
		  	$this->db->select('*');
			$this->db->from('ah_store_item_request_list');						
			$this->db->where("delete_record",'0');
			$this->db->where("stItmReqId",$stItmReqId);
			$this->db->order_by('stItmReqId','DESC');
			$query = $this->db->get();
			foreach($query->result() as $row){
				$data = array();
				$item =$row->items;
				$qty =$row->qty;
				$dts = array();
				$dt["stItmReqListId"] =$row->stItmReqListId;
				$quatity = 0;
				$this->db->select('*');
				$this->db->from('ah_inventory_qty');						
				$this->db->where("inventoryid",$item);
				$query1 = $this->db->get();
				foreach($query1->result() as $row1){
					$quatity = $row1->quantity;
					$dt["prevqty"] =$quatity;
					$data["qtyid"] = $qtyid = $row1->qtyid;
					$data["quantity"]= $quatity - $qty;
					$stItmReqListId = $this->store->savestore($data,'ah_inventory_qty','qtyid');
				}
				$stItmReqListId = $this->store->savestore($dt,'ah_store_item_request_list','stItmReqListId');
				//////////////////
				
				$history= array();
				$history["itemid"] =$item;
				$history["currentdate"] =date('Y-m-d');
				$history["addedby"] =  $this->_login_userid;
				$history["qtyid"] =$qtyid;
				$history["qty"] =$qty;
				
				$history["remark"] = "شاشة برنامج المخازن";
				$history["ItemHisId"] ='';
				$qtyid = $this->store->savestore($history,'ah_inventory_history','ItemHisId');
				
				////////////////
				
				
				
				
				
			}//foreach
		   }//service department
			@header("Location:".base_url()."store/requestforItems_listing");
	  }
	  if($stItmReqId >0){
		$this->_data["rows"] = $rows = $this->store->getdata($stItmReqId,'ah_store_item_request',"stItmReqId");
	  }
	  	$this->db->select('*');
		$this->db->from('ah_store_item_request_list');						
		$this->db->where("delete_record",'0');
		$this->db->where("stItmReqId",$rows->stItmReqId);
		$this->db->order_by('stItmReqId','DESC');
		$query = $this->db->get();
		$this->_data["store_item_request_list"] = $query->result();
		$this->_data['all_items']		=	$this->store->get_all_items();		
		$this->load->view('requestforItems',$this->_data);
	}
	public function add_store_item_request_list($stItmReqListId = NULL){
		$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
		$this->_data['permissions_d']=$permissions[$this->moduleid]['d'];
		$data = array();
		$stItmReqListIddauto = $_POST["stItmReqListIddauto"];
		$data["items"] = $_POST["items"];
		$data["qty"] = $_POST["qty"];
		$data["description"] = $_POST["description"];
		$data["userid"] = $this->_login_userid;
		$stItmReqId = $_POST["stItmReqId"];
		if($stItmReqId <= 0){
			$dts["currentdate"] =date('Y-m-d');
			$dts["userid"] = $this->_login_userid;
			$dts["department"] = $this->userroleid;
			$dts["status"] = 'Pending';
			$dts["types"] = '1';
			 $stItmReqId = $this->store->savestore($dts,'ah_store_item_request','stItmReqId');
		}
		$ex['stItmReqId'] = $data["stItmReqId"] = $stItmReqId;
		$stItmReqListId = $_POST["stItmReqListId"];
		if($stItmReqListId>0){
			$data["stItmReqListId"] = $stItmReqListId;
		}
		$stItmReqListId = $this->store->savestore($data,'ah_store_item_request_list','stItmReqListId');
		//$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_store_item_request_list/'.$stItmReqListId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		//if($reliefteam->userid == $this->_login_userid){
			$action	.= ' <a onclick="edit_store_item_request_list(\''.$stItmReqListId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
		//}
		
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$stItmReqListId.'" data-url="'.base_url().'store/delete_store_item_request_list/'.$stItmReqListId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		//}
		$all_items	=	$this->store->get_all_items();	
		$items ='';
		$ex['data'] ='';
		foreach($all_items as $type){ if($data["items"] == $type->itemid){$items =$type->itemname;}}
		if($_POST["stItmReqListId"] <=0){
			$ex['data'] .='<tr id="'.$stItmReqListId.'_row">';
		}
		$this->db->select('quantity');
		$this->db->from('ah_inventory_qty');						
		$this->db->where("inventoryid",$data["items"]);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$available =$row->quantity;
		}
		$balance =$available - $reliefteam->qty;
		 $ex['data'] .='<td id="'.$stItmReqListId.'_daily_id">'.$stItmReqListIddauto.'</td>
                                    <td id="'.$stItmReqListId.'_daily_items">'.$items.'</td>
                                    <td id="'.$stItmReqListId.'_daily_qty">'.$data["qty"].'</td>
                                    <td id="'.$stItmReqListId.'_daily_description">'.$data["description"].'</td>';
									if( $this->userroleid  == $this->servicedepartment){
         $ex['data'] .='<td style="color:#F00; font-weight:bold;" id="'.$stItmReqListId.'_daily_balance">'.$available.'</td>';
		}
		$ex['data'] .='<td>'.$action.'</td>';
		if($_POST["stItmReqListId"] <=0){$ex['data'] .='</tr>';}
		
		
	echo json_encode($ex);
	exit();	
	}
	public function chkQty(){
		$stItmReqId = $_POST["stItmReqId"];
		$items = $_POST["vals"];
		$this->db->select('quantity');
		$this->db->from('ah_inventory_qty');						
		$this->db->where("inventoryid",$items);
		$query = $this->db->get();
		$ex['data'] = 0;
		$fst =0;
		$sec =0;
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$fst =$row->quantity;
		} 
		/*if($stItmReqId >0){
			$this->db->select('SUM(qty) as total');
			$this->db->from('ah_store_item_request_list');						
			$this->db->where("stItmReqId",$stItmReqId);
			$this->db->where("items",$items);
			$this->db->where("delete_record",'0');
			$query1 = $this->db->get();
			if($query1->num_rows() > 0)
			{
				$row = $query1->row();
				$sec =$row->total;
			} 	
		}*/
		//$ex['data'] = $fst - $sec;
		$ex['data'] = $fst;
		echo json_encode($ex);
		exit();
	}
	function delete_store_item_request_list($stItmReqListId){
		$this->store->delete($stItmReqListId,'stItmReqListId',"ah_store_item_request_list");
		$ex['data'] = $stItmReqListId."_row";
		echo json_encode($ex);
		exit();	
	}
	function requestforItemsdelete($stItmReqId){
		$this->store->delete($stItmReqId,'stItmReqId',"ah_store_item_request");
		$this->store->delete($stItmReqId,'stItmReqId',"ah_store_item_request_list");

		$ex['data'] = $stItmReqListId."_durar_lm";
		echo json_encode($ex);
		exit();	
	}
	function view_requestforItems($stItmReqId){
		$this->_data["rows"] = $this->store->getdata($stItmReqId,'ah_store_item_request',"stItmReqId");
		$this->db->select('*');
		$this->db->from('ah_store_item_request_list');						
		$this->db->where("delete_record",'0');
		$this->db->where("stItmReqId",$stItmReqId);
		$this->db->order_by('stItmReqId','DESC');
		$query = $this->db->get();
		$this->_data["store_item_request_list"] = $query->result();
		$this->_data['all_items']		=	$this->store->get_all_items();
		$this->load->view('view_requestforItems',$this->_data);
	}
	public function mainrequestforItems_listing()
    {
        $permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
		$this->_data["rows"] = $this->store->getdata($stItmReqId,'ah_store_item_request',"stItmReqId");
		
		$this->load->view('mainrequestforItems_listing',$this->_data);
		
    }
	public function ajax_mainrequestforItems_listing()
    {
       
			if($this->userroleid  == $this->servicedepartment){
				$this->db->select('*');
				$this->db->from('ah_store_item_request');						
				$this->db->where("delete_record",'0');
				$this->db->where("types",'2');
				$this->db->order_by('status','DESC');
				$this->db->order_by('appDate','DESC');
				$this->db->order_by('stItmReqId','DESC');
			}else{
				$this->db->select('*');
				$this->db->from('ah_store_item_request');						
				$this->db->where("delete_record",'0');
				$this->db->where("types",'2');
				$this->db->where("userid",$this->_login_userid);
				$this->db->order_by('status','DESC');
				$this->db->order_by('stItmReqId','DESC');
			}
			
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
				$cnt =1;
					
			foreach($query->result() as $lc)
			{
				$action 	='';
				if($lc->stItmReqId)
				{
					$action 	.='<a onclick="alatadad(this);" data-url="'.base_url().'store/view_mainrequestforItems/'.$lc->stItmReqId.'" href="#"><i class="my icon icon-eye-open"></i></a>';
				}
				if($lc->status != "Accepted"){
					if($lc->userid == $this->_login_userid or $this->userroleid  == $this->servicedepartment){
						$action	.= ' <a href="'.base_url().'store/mainrequestforItems/'.$lc->stItmReqId.'" ><i class="icon-pencil"></i></a>';
					}
					
					if($permissions[$this->moduleid]['d']	==	1 and $lc->status != "Accepted") 
					{
						$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->stItmReqId.'" data-url="'.base_url().'store/mainrequestforItemsdelete/'.$lc->stItmReqId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
					}
				}
				$status ='';
				if($lc->status == "Pending")
				{
					$status	=	'<img src="'.base_url().'assets/images/pending.png" style="width: 24px;" alt="Pending" title="Pending"/>';
				}
				else if($lc->status == "Accepted")
				{
					$status	=	'<img src="'.base_url().'assets/images/completed.png" style="width: 24px;" alt="Accepted" title="Accepted"/>';
				}
				else if($lc->status == "Rejected")
				{
					$status	=	'<img src="'.base_url().'assets/images/rejected.png" style="width: 24px;" alt="Rejected" title="Rejected"/>';
				}
				
				
				$departments =$this->store->get_all_listmanagement($lc->nationality);
				$arr[] = array(
					"DT_RowId"		=>	$lc->stItmReqId.'_durar_lm',
					"رقم التسلسل" 		=>$cnt,	
					"الجهة الطالبة / المستفيد" 		=> $lc->name,	
					"الجنسية" 	=>	$departments->list_name,
					"الهاتف" 	=>	$lc->phone,
					"تاريخ الانشاء" 	=>	arabic_date($lc->currentdate),
					"الحالة" 	=>$status,
					"الإجراءات" 		=>	$action
					);
					$cnt++;
					unset($action,$items);
			}
			
			
			
			$ex['data'] = $arr;
			echo json_encode($ex);
	
    }
	public function mainrequestforItems($stItmReqId=NULL){
	  $permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
	  $this->_data['permissions_d']=$permissions[$this->moduleid]['d'];
	  $datas = array();
	  if(isset($_POST["stItmReqId"])){
		  $datas["stItmReqId"] = $stItmReqId = $_POST["stItmReqId"];
		  
		  if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
			{
				$names = array();
				$files = $_FILES["attachment"];
				 foreach ($files['name'] as $key => $image) {
					if(!empty($files['name'][$key]))
					{
						$_FILES['images']['name']= $files['name'][$key];
						$_FILES['images']['type']= $files['type'][$key];
						$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['images']['error']= $files['error'][$key];
						$_FILES['images']['size']= $files['size'][$key];
						$names[] =	$this->upload_file($this->_login_userid,'images','resources/stores/request');
					}
					
				}
				if($data["attachment_old"] !=""){
					if(count($names)>0)	$datas['attachment']	=	$_POST["attachment_old"].",".@implode(',',$names);
					else{$datas['attachment']	=	$_POST["attachment_old"];}
				}
				else{
					$datas['attachment']	=	@implode(',',$names);	
				}
				
			}
			else{
				$datas['attachment']	=	$_POST["attachment_old"];
			}
		  if($this->userroleid == $this->servicedepartment) {
			  $datas["notes"] =  $_POST["notes"];
			  $datas["status"] =  "Accepted"; 
			  $datas["appDate"] =date('Y-m-d');
			$datas["approvedId"] = $this->_login_userid;
			
		  }
		  $datas["name"] =  $_POST["name"];
		  $datas["nationality"] =  $_POST["nationality"];
		  $datas["phone"] =  $_POST["phone"];
		  $datas["desc_user"] =  $_POST["desc_user"];
		  $stItmReqId = $this->store->savestore($datas,'ah_store_item_request','stItmReqId');
		  if($this->userroleid == $this->servicedepartment) {
		  	$this->db->select('*');
			$this->db->from('ah_store_item_request_list');						
			$this->db->where("delete_record",'0');
			$this->db->where("stItmReqId",$stItmReqId);
			$this->db->order_by('stItmReqId','DESC');
			$query = $this->db->get();
			foreach($query->result() as $row){
				$data = array();
				$item =$row->items;
				$qty =$row->qty;
				$dts = array();
				$dt["stItmReqListId"] =$row->stItmReqListId;
				
				$quatity = 0;
				$this->db->select('*');
				$this->db->from('ah_inventory_qty');						
				$this->db->where("inventoryid",$item);
				$query1 = $this->db->get();
				foreach($query1->result() as $row1){
					$quatity = $row1->quantity;
					$data["qtyid"]= $row1->qtyid;
					$qtyid = $dt["prevqty"] =$quatity;
					$data["quantity"]= $quatity - $qty;
					$stItmReqListId = $this->store->savestore($data,'ah_inventory_qty','qtyid');
				}
				$stItmReqListId = $this->store->savestore($dt,'ah_store_item_request_list','stItmReqListId');
				//if($data["qtyid"]){
				
				//}
				
			}
			
			//////////////////
				
				$history= array();
				$history["itemid"] =$item;
				$history["currentdate"] =date('Y-m-d');
				$history["addedby"] =  $this->_login_userid;
				$history["qtyid"] =$qtyid;
				$history["qty"] =$qty;
				
				$history["remark"] = "شاشة برنامج المخازن ";
				$history["ItemHisId"] ='';
				$qtyid = $this->store->savestore($history,'ah_inventory_history','ItemHisId');
				
				////////////////
			
			
		 }
			@header("Location:".base_url()."store/mainrequestforItems_listing");
	  }
	  if($stItmReqId >0){
		$this->_data["rows"] = $rows = $this->store->getdata($stItmReqId,'ah_store_item_request',"stItmReqId");
	  }
	  	$this->db->select('*');
		$this->db->from('ah_store_item_request_list');						
		$this->db->where("delete_record",'0');
		$this->db->where("stItmReqId",$rows->stItmReqId);
		$this->db->order_by('stItmReqId','DESC');
		$query = $this->db->get();
		$this->_data["store_item_request_list"] = $query->result();
		
		$this->db->select('*');
		$this->db->from('ah_listmanagement');						
		$this->db->where('list_type','nationality');
		$this->db->where("delete_record",'0');
		$query = $this->db->get(); 
		$this->_data["nationality"] = $query->result();
			
		
		
		
		$this->_data['all_items']		=	$this->store->get_all_items();		
		$this->load->view('mainrequestforItems',$this->_data);
	}
	public function add_mainstore_item_request_list($stItmReqListId = NULL){
		$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
		$this->_data['permissions_d']=$permissions[$this->moduleid]['d'];
		$data = array();
		$stItmReqListIddauto = $_POST["stItmReqListIddauto"];
		$data["items"] = $_POST["items"];
		$data["qty"] = $_POST["qty"];
		$data["description"] = $_POST["description"];
		$data["userid"] = $this->_login_userid;
		$stItmReqId = $_POST["stItmReqId"];
		if($stItmReqId <= 0){
			$dts["currentdate"] =date('Y-m-d');
			$dts["userid"] = $this->_login_userid;
			$dts["department"] = $this->userroleid;
			$dts["status"] = 'Pending';
			$dts["types"] = '2';
			 $stItmReqId = $this->store->savestore($dts,'ah_store_item_request','stItmReqId');
		}
		$ex['stItmReqId'] = $data["stItmReqId"] = $stItmReqId;
		$stItmReqListId = $_POST["stItmReqListId"];
		if($stItmReqListId>0){
			$data["stItmReqListId"] = $stItmReqListId;
		}
		$stItmReqListId = $this->store->savestore($data,'ah_store_item_request_list','stItmReqListId');
		//$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_store_item_request_list/'.$stItmReqListId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		//if($reliefteam->userid == $this->_login_userid){
			$action	.= ' <a onclick="edit_mainstore_item_request_list(\''.$stItmReqListId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
		//}
		
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$stItmReqListId.'" data-url="'.base_url().'store/delete_mainstore_item_request_list/'.$stItmReqListId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		//}
		$all_items	=	$this->store->get_all_items();	
		$items ='';
		$ex['data'] ='';
		foreach($all_items as $type){ if($data["items"] == $type->itemid){$items =$type->itemname;}}
		$available =0;
		$this->db->select('quantity');
		$this->db->from('ah_inventory_qty');						
		$this->db->where("inventoryid",$data["items"]);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$available =$row->quantity;
		}
		$balance =$available - $reliefteam->qty;
		
		
		if($_POST["stItmReqListId"] <=0){$ex['data'] .='<tr id="'.$stItmReqListId.'_row">';}
		$ex['data'] .=' <td id="'.$stItmReqListId.'_daily_id">'.$stItmReqListIddauto.'</td>
                                    <td id="'.$stItmReqListId.'_daily_items">'.$items.'</td>
                                    <td id="'.$stItmReqListId.'_daily_qty">'.$data["qty"].'</td>
                                    <td id="'.$stItmReqListId.'_daily_description">'.$data["description"].'</td>';
			if( $this->userroleid  == $this->servicedepartment){
				$ex['data'] .='<td style="color:#F00; font-weight:bold;" id="'.$stItmReqListId.'_daily_balance">'.$available.'</td>';
			}
          $ex['data'] .='<td>'.$action.'</td>';
		if($_POST["stItmReqListId"] <=0){$ex['data'] .='</tr>';}							
									
		
		
	echo json_encode($ex);
	exit();	
	}
	
	function delete_mainstore_item_request_list($stItmReqListId){
		$this->store->delete($stItmReqListId,'stItmReqListId',"ah_store_item_request_list");
		$ex['data'] = $stItmReqListId."_row";
		echo json_encode($ex);
		exit();	
	}
	function mainrequestforItemsdelete($stItmReqId){
		$this->store->delete($stItmReqId,'stItmReqId',"ah_store_item_request");
		$this->store->delete($stItmReqId,'stItmReqId',"ah_store_item_request_list");
		$ex['data'] = $stItmReqListId."_durar_lm";
		echo json_encode($ex);
		exit();	
	}
	function view_mainrequestforItems($stItmReqId){
		$this->_data["rows"] = $rows = $this->store->getdata($stItmReqId,'ah_store_item_request',"stItmReqId");
		$this->_data["nationality"]= $this->store->get_all_listmanagement($rows->nationality);
		$this->db->select('*');
		$this->db->from('ah_store_item_request_list');						
		$this->db->where("delete_record",'0');
		$this->db->where("stItmReqId",$stItmReqId);
		$this->db->order_by('stItmReqId','DESC');
		$query = $this->db->get();
		$this->_data["store_item_request_list"] = $query->result();
		$this->_data['all_items']		=	$this->store->get_all_items();
		$this->load->view('view_mainrequestforItems',$this->_data);
	}
	public function removeimage($stItmReqId,$imagename){
		unlink('resources/stores/request/'.$this->_login_userid."/".$imagename);
		$result = $this->store->getdata($stItmReqId,'ah_store_item_request',"stItmReqId");
		if($result->attachment !=""){
			$image=@explode(',',$result->attachment);
			foreach($image as $key=>$vals){
				if($imagename == $vals){
					unset($image[$key]);
				}
			}
			$data["attachment"]=@implode(',',$image);
			$data["stItmReqId"] = $stItmReqId;
			$this->services->savestore($data,'ah_store_item_request','stItmReqId');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
		}
		//redirect(base_url().'store/requestforItems/'.$stItmReqId);
		exit();
	}
	public function itemtransfer(){
		$this->_data['itemid'] =0;
		$permissions	=	$this->haya_model->check_other_permission(array($this->transfermoduleid));
		if($_POST["item"] >0){
			$item = $_POST["item"];
			$data= array();
			
			$rows = $this->store->getdata($item,'ah_inventory','itemid');
			
			if(count($rows)>0){
				$data= array();
				$data["store_id"]= $_POST["store_idTo"];
				$data["addedby"]= $this->_login_userid;
				$data["list_category"]= $rows->list_category;
				$data["list_subcategory"]= $rows->list_subcategory;
				$data["itemname"]= $rows->itemname;
				$data["itemdescription"]= $rows->itemdescription;
				$data["itemphoto"]= $rows->itemphoto;
				$data["itemstatus"]= $rows->itemstatus;
				$data["transferfromStore_id"]= $_POST["store_id"];
				$data["transferfromId"]= $rows->itemid;
				$data["transferDate"]=date('Y-m-d');
				$data["itemid"]='';
				$this->_data['itemid'] = $itemid = $this->store->savestore($data,'ah_inventory','itemid');
				///////////////////////////////////
				$data1["inventoryid"]= $itemid;
				$data1["addedby"]= $this->_login_userid;
				$data1["quantity"]= $_POST["qtyTo"];
				$data1["addeddate"]= date('Y-m-d');
				$data1["qtyid"]= '';
				$qtyid1 = $this->store->savestore($data1,'ah_inventory_qty','qtyid');
				////////////////////////////////
				$rows1 = $this->store->getdata($rows->itemid,'ah_inventory_qty','inventoryid');
				if(count($rows1)>0){
					$datafrom= array();
					$datafrom["inventoryid"]= $rows->itemid;
					$datafrom["quantity"]= $_POST["qty"]-$_POST["qtyTo"];
					$datafrom["qtyid"]= $rows1->qtyid;	
					$qtyid = $this->store->savestore($datafrom,'ah_inventory_qty','qtyid');
					
					$history= array();
					$history["itemid"] =$rows->itemid;
					$history["currentdate"] =date('Y-m-d');
					$history["addedby"] =  $this->_login_userid;
					$history["qtyid"] =$qtyid;
					$history["qty"] = $_POST["qtyTo"];
					$rows1 = $this->store->getdata($_POST["store_idTo"],'ah_stores','store_id');
					
					$history["remark"] = "ترانفر كمية إلى ".$rows1->store_name." مخزن";
					$history["ItemHisId"] ='';
					$qtyid = $this->store->savestore($history,'ah_inventory_history','ItemHisId');
					
				}
				$history= array();
				$history["itemid"] =$itemid;
				$history["currentdate"] =date('Y-m-d');
				$history["addedby"] =  $this->_login_userid;
				$history["qtyid"] =$qtyid1;
				$history["qty"] = $_POST["qtyTo"];
				$rows1 = $this->store->getdata($_POST["store_id"],'ah_stores','store_id');
				
				$history["remark"] = "ترانفر كمية من ".$rows1->store_name." مخزن";
				$history["ItemHisId"] ='';
				$qtyid = $this->store->savestore($history,'ah_inventory_history','ItemHisId');
				
				
			}
			$this->_data['rows'] = $this->store->getdata($itemid,'ah_inventory','itemid');
		}
		$this->_data['all_stores']	=	$this->store->get_all_stores();	
		$this->load->view('itemtransfer',$this->_data);
	}
/*	function loadcategories(){
		$parent_id = $_POST["parent_id"];
		$vals = $this->store->get_all_listmanagementbyParent($parent_id);
		$ex["data"]='';
		foreach($vals as $val){
			$ex["data"] .='<option value="'.$val->list_id.'">'.$val->list_name.'</option>';
		}
		$ex["vals"] =$vals;
		
		echo json_encode($ex);
		exit();	
	}
*/	function loaditems(){
		$store_id = $_POST["store_id"];
		$vals = $this->store->getdmessagedata($store_id,'ah_inventory','store_id');
		$ex["data"]='';
		foreach($vals as $val){
			$ex["data"] .='<option value="'.$val->itemid.'">'.$val->itemname.'</option>';
		}
		//$ex["vals"] =$vals;
		
		echo json_encode($ex);
		exit();	
	}
	function loadqty(){
		$itemid = $_POST["itemid"];
		$vals = $this->store->getdmessagedata($itemid,'ah_inventory_qty','inventoryid');
		$ex["data"]='';
		foreach($vals as $val){
			$ex["data"]= $val->quantity;
		}
		//$ex["vals"] =$vals;
		
		echo json_encode($ex);
		exit();	
	}
	
	
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
				
		if(!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
	function getfileicon($filename,$path)
	{
		$filename = strtolower($filename);
		$file_extension = strrchr($filename, ".");	
	
		$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_nd.png" style="width:30px; height:30px" alt="image" title="image"/></a>';
	
		if($file_extension=='.doc' || $file_extension=='.docx')
	
			 $rettype =' <a class="fancybox-button" rel="gallery1" href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_doc.png" style="width:30px; height:30px" alt="doc" title="doc"/></a>';
	
		if($file_extension=='.xls' || $file_extension=='.xlsx' || $file_extension=='.csv' || $file_extension=='.xlt' || $file_extension=='.xltx')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_xls.png" style="width:30px; height:30px" alt="xls" title="xls"/></a>';
	
		if($file_extension=='.pdf')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_pdf.png" style="width:30px; height:30px" alt="pdf" title="pdf"/></a>';
	
		if($file_extension=='.jpg' || $file_extension=='.jpeg'  || 	$file_extension=='.gif')
	
			$rettype=' <a class="fancybox-button" rel="gallery1" href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_jpg.png" style="width:30px; height:30px" alt="jpg" title="jpg"/></a>';
	
		if($file_extension=='.png' )
	
			$rettype=' <a class="fancybox-button" rel="gallery1" href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_png.png" style="width:30px; height:30px" alt="png" title="png"/></a>';
	
		if($file_extension=='.ppt' || $file_extension=='.pptx')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_ppt.png" style="width:30px; height:30px" alt="ppt" title="ppt"/></a>';		
	
		if($file_extension=='.zip' || $file_extension=='.tz' || $file_extension=='.rar' || $file_extension=='.gzip')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_zip.png" style="width:30px; height:30px" alt="zip" title="zip"/></a>';		
	
		if($file_extension=='.txt' || $file_extension=='.rtf')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_txt.png" style="width:30px; height:30px" alt="txt" title="txt"/></a>';
	
		return $rettype;
	
			
	
	}
	
	
}
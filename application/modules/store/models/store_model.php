<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Store_model extends CI_Model {
	
	/*
	* Properties
	*/
	private $_table_users;	
	private $_table_donate;	
	private $_table_applied_donation;
	private $_table_sms;
//----------------------------------------------------------------------
    
	/*
	* Constructor
	*/
	
	function __construct()
    {
        parent::__construct();
		
		$this->load->helper("file");
		
		//Load Table Names from Config
		$this->_table_users 			=  $this->config->item('table_users');
		$this->_table_donate 			=  $this->config->item('table_donate');
        $this->_table_user_profile 		=  $this->config->item('table_user_profile');
		$this->_table_applied_donation 	=  $this->config->item('table_applied_donation');
		$this->_table_sms 				=  $this->config->item('sms_management');
		$this->_login_userid			=	$this->session->userdata('userid');
    }
	function savestore($data,$table,$feild)
	{
		$serviceid = $data[$feild];
		if($serviceid	!=	'')
		{
			$this->db->where($feild,$serviceid);
			$this->db->update($table,json_encode($data),$this->_login_userid,$data);
			return $serviceid;
		}
		else
		{
			$this->db->insert($table,$data,json_encode($data),$this->_login_userid);
			return $this->db->insert_id();;
		}
	}
	function delete($serviceid,$field,$table)
	{
			$json_data	=	json_encode(array('record'	=>	'delete',$field	=>	$serviceid));
		$data		=	array('delete_record'=>'1');
		
		$this->db->where($field,$serviceid);

		$this->db->update($table,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
			
	}
	
	function getdata($serviceid,$table,$column){
		$this->db->select('*');
		$this->db->from($table);						
		$this->db->where($column,$serviceid);
		$query = $this->db->get(); 
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	function get_all_listmanagement($list_id=0){
		$this->db->select('*');
		$this->db->from('ah_listmanagement');						
		if($list_id>0){$this->db->where('list_id',$list_id);}
		$this->db->where("delete_record",'0');
		$query = $this->db->get(); 
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	function get_all_listmanagementbyParent($list_parent_id=0){
		$this->db->select('*');
		$this->db->from('ah_listmanagement');						
		if($list_parent_id>0){$this->db->where('list_parent_id',$list_parent_id);}
		$this->db->where("delete_record",'0');
		$query = $this->db->get(); 
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	 public function get_all_items()
	 {
		 $query_string	=	NULL;
		 
		$query	=	$this->db->query("SELECT
			`ah_inventory`.`itemid`
			, `ah_inventory`.`store_id`
			, `ah_inventory`.`addedby`
			, `ah_inventory`.`list_category`
			, `ah_inventory`.`list_subcategory`
			, `ah_inventory`.`itemname`
			, `ah_inventory`.`itemdescription`
			, `ah_inventory`.`itemphoto`
			, `ah_inventory`.`itemstatus`
			FROM
			`ah_inventory`;");
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	 }
	 //-----------------------------------------------------------------------------
	/*
	* Get all stores
	* return OBKECT
	*/
	function get_all_stores($store_id = NULL)
	{
		$this->db->select('store_id,store_name');
		if($store_id >0){$this->db->where('store_id',$store_id);}
        $query = $this->db->get('ah_stores');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-----------------------------------------------------------------------------
	function getdmessagedata($serviceid,$table,$field){
		$this->db->select('*');
		$this->db->from($table);						
		$this->db->where($field,$serviceid);
		$query = $this->db->get(); 
		return $query->result();
	}
	
	
}
<h3><?php echo $detail->fullname;?></h3>
<h2 align="center" style="margin-top: -35px;">ملاحظات على مقابلة</h2>
<div class="row">
  <div class="col-md-4 form-group">
    <label class="text-warning">يجري في وقت متأخر</label>
    <strong><?php echo $detail->being_late; ?> </strong> </div>
  <div class="col-md-4 form-group">
    <label class="text-warning">مظهر</label>
    <strong><?php echo $detail->appearance; ?> </strong> </div>
  <div class="col-md-4 form-group">
    <label class="text-warning">الانطباع</label>
    <strong><?php echo $detail->impression; ?> </strong> </div>
  <div class="col-md-4 form-group">
    <label class="text-warning">أخلاق</label>
    <strong><?php echo $detail->manners; ?> </strong> </div>
  <div class="col-md-4 form-group">
    <label class="text-warning">انقطاع</label>
    <strong><?php echo $detail->interruptions; ?> </strong> </div>
  <div class="col-md-4 form-group">
    <label class="text-warning">التعب</label>
    <strong><?php echo $detail->being_tired; ?> </strong> </div>
  <div class="col-md-8 form-group">
    <label class="text-warning">الرسالة</label>
    <strong><?php echo $detail->email_msg; ?> </strong> </div>
</div>

      <div class="col-md-12">
        <div class="panel panel-default panel-block" style="padding: 10px 10px;">       
          <form method="POST" id="ah_inventory_qty" name="ah_inventory_qty">
            <input type="hidden" name="inventoryid" id="inventoryid" value="<?PHP echo $item->itemid; ?>">           
            <div class="form-group col-md-12">
              <label for="basic-input"><strong>الكمية:</strong></label>
              <input type="text" class="form-control req NumberInput" value="<?PHP echo $item->quantity; ?>" placeholder="الكمية" name="quantity" id="quantity" />
            </div>
            <br clear="all">
            <div class="form-group col-md-12">
              <label for="basic-input"><strong>ملاحظات:</strong></label>
              <textarea name="qty_remarks" placeholder="تفاصيل العنصر" class="form-control req" style="resize:none; height:300px;" id="qty_remarks"><?PHP echo $item->itemdescription; ?></textarea>              
            </div>
           
            <div class="form-group col-md-6">
              <button type="button" id="saveQtybtn" onClick="saveQty();" class="btn btn-success">حفظ</button>
            </div>
          </form>
          <br clear="all">
        </div>
      </div>
      <script src="<?PHP echo base_url(); ?>assets/js/ajax_script.js"></script>
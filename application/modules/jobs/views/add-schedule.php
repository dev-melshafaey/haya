<?php $user_detail	=	$this->haya_model->get_user_detail($userid);?>
<?php //echo '<pre>'; print_r($user_detail); exit();?>

<h3><?php echo $user_detail['profile']->fullname;?></h3><h2 align="center" style="margin-top: -35px;">نموذج الجدول الزمني</h2>
<div class="row">
  <div class="row col-md-12">
  
    <form action="" method="POST" id="add_schedule" name="add_schedule" enctype="multipart/form-data" autocomplete="off">
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ</label>
        <input type="text" name="schedule_date" id="schedule_date" class="datepicker form-control req" placeholder="تاريخ البدء‎"/>
      </div>
      <div class="col-md-8 form-group">
        <label class="text-warning">التفاصيل</label>
        <textarea name="detail" id="detail" placeholder="التفاصيل" style="margin-top: 0px; margin-bottom: 0px; height: 125px;" class="form-control req"></textarea>
      </div>
      <input type="hidden" name="userid" id="userid" value="<?php echo $userid;?>"/>
      <input type="hidden" name="jobid" id="jobid" value="<?php echo $jobid;?>"/>
    </form>
  </div>
  <div class="row col-md-12">
    <div class="form-group  col-md-12">
      <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit" onclick="add_sss();" value="حفظ" />
    </div>
  </div>
</div>
<br clear="all" />
<div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
  <table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info">
    <thead>
      <tr role="row">
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">رقم الحساب</th>
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">نوع الراتب</th>
        <th style="text-align:center;" class="sorting_asc" role="columnheader" tabindex="0" aria-controls="tableSortable" aria-sort="ascending" aria-label="">الإجراءات</th>
      </tr>
    </thead>
    <tbody role="alert" aria-live="polite" aria-relevant="all">
      <?php if(!empty($user_schedules)):?>
		  <?php foreach($user_schedules as $schedule):?>
			  <?php
                //$actions	=	'<a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$schedule->scheduleid.'" data-url="'.base_url().'users/delete_salary/'.$salary->salaryid.'" data-icon="icon-remove-sign"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
              ?>
              <tr role="row" id="<?php echo $salary->salaryid.'_durar_lm';?>">
                <td style="text-align:center;" ><?php echo date('d-m-Y',strtotime($schedule->schedule_date));?></td>
                <td style="text-align:center;" ><?php echo $schedule->detail;?></td>
                <td style="text-align:center;" ><?php echo $actions;?></td>
              </tr>
          <?php unset($actions); endforeach;?>
      <?php endif;?>
    </tbody>
  </table>
</div>
<script>
function create_data_table(mingo)
{
	if($('.newbasicTable').length > 0)
	{
		 $('.newbasicTable thead th').each( function (index, value) {				
				 var title = $.trim($(this).html());
				 var attr_id = $('.newbasicTable thead th').eq( $(this).index() ).attr('id');
				 if(title!='الإجراءات' && title!='' && title!='متوسط صافي الريح' && title!='متوسط الايرادات' && title!='الشهرية' && title!='السنوية')
				 {				 
				 	$(this).html(title+'<input type="search" class="form-control '+attr_id+' search_filter" placeholder="'+title+'" />' );
				 }
		});
		
		 var basic_table = $('.newbasicTable').DataTable({
			 "ordering": false,
			 "oLanguage": {
				 "sSearch": "",
				 "oPaginate": { "sNext": "التالی", "sPrevious": "السابق" }
				}
			});
		
		basic_table.columns().eq(0).each(function (colIdx) 
		{
			$('input', basic_table.column(colIdx).header()).on('keyup change', function() 
			{
				basic_table.column(colIdx).search(this.value).draw();
			});
		});	
		
		$('.search_filter').keyup(function(){
			$('.newbasicTable td').removeHighlight().highlight($(this).val());
		});	
		
		basic_table.on( 'draw', function () {
    		tiptop();
		} );
	}
}
$(function(){
	
	create_data_table(0);
	
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});
</script>
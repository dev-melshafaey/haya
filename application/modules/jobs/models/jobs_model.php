<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Jobs_model extends CI_Model {

	private $_table_users;
	private $_table_branchs;
	private $_table_listmanagement;
	private $_table_userprofile;
	private $_table_users_bankaccount;
	private	$_table_users_salary;
	private	$_table_users_basic_salary;
	private $_table_users_salary_detail;
	private $_table_users_communications;
	private $_table_users_family;
	private $_table_users_documents;
	private $_table_experiences;
	private $_table_leave_request;
	private $_table_schedule;
	private $_table_interview_response;	
	function __construct()
    {
        parent::__construct();
				$this->_table_users 				= 	$this->config->item('table_users');
		$this->_table_branchs 				= 	$this->config->item('table_branchs');
		$this->_table_listmanagement 		= 	$this->config->item('table_listmanagement');
		$this->_table_userprofile 			= 	$this->config->item('table_userprofile');
		$this->_table_users_bankaccount 	= 	$this->config->item('table_users_bankaccount');
		$this->_table_users_salary 			= 	$this->config->item('table_users_salary');
		$this->_table_users_salary_detail 	= 	$this->config->item('table_users_salary_detail');
		$this->_table_users_family 			= 	$this->config->item('table_users_family');
		$this->_table_users_communications 	= 	$this->config->item('table_users_communications');
		$this->_table_users_documents 		= 	$this->config->item('table_users_documents');
		$this->_table_experiences 			= 	$this->config->item('table_experiences');
		$this->_table_leave_request 		= 	$this->config->item('table_leave_request');
		$this->_table_users_basic_salary 	= 	$this->config->item('table_users_basic_salary');
		$this->_table_schedule 				= 	$this->config->item('table_schedule');
		$this->_table_interview_response 	= 	$this->config->item('table_interview_response');
		
		$this->load->helper("file");
		$this->_login_userid			=	$this->session->userdata('userid');
    }
	
	function savejob()
	{
		$jobsid = $this->input->post('jobsid');
		$userid = $this->_login_userid;
		$jobtitle = $this->input->post('jobtitle');
		$jobdescription = $this->input->post('jobdescription');
		$expirydate = $this->input->post('expirydate');
		$jobstatus = $this->input->post('jobstatus');		
		$location = $this->input->post('location');
		$salaryStart = $this->input->post('salaryStart');
		$salaryEnd = $this->input->post('salaryEnd');				
		$ah_jobs = array('userid'=>$userid,'jobtitle'=>$jobtitle,'jobdescription'=>$jobdescription,'expirydate'=>$expirydate,'jobstatus'=>$jobstatus,'location'=>$location,'salaryStart'=>$salaryStart,'salaryEnd'=>$salaryEnd);
		if($jobsid!='')
		{
			$this->db->where('jobsid',$jobsid);
			$this->db->update('ah_jobs',json_encode($ah_jobs),$this->_login_userid,$ah_jobs);
		}
		else
		{
			$this->db->insert('ah_jobs',$ah_jobs,json_encode($ah_jobs),$this->_login_userid);
		}
	}
	
	function getjob($jobsid)
	{
		$this->db->where('jobsid',$jobsid);
        $query = $this->db->get('ah_jobs');
		return $query->row();
	}
	
	function get_all_candidate()
	{
		/*$this->db->select($this->_table_users.'.userid,'.$this->_table_listmanagement.'.list_name,'.$this->_table_userprofile.'.fullname,'.$this->_table_userprofile.'.resume,'.$this->_table_userprofile.'.email,'.$this->_table_userprofile.'.gender,ah_jobs.jobtitle,ah_jobs.jobsid,ah_candidate.applydate');
        $this->db->from('ah_users');
				
		$this->db->join($this->_table_userprofile,$this->_table_userprofile.'.userid='.$this->_table_users.'.userid');
		$this->db->join($this->_table_listmanagement,$this->_table_listmanagement.'.list_id='.$this->_table_userprofile.'.maritialstatus');
        $this->db->join('ah_candidate','ah_candidate.userid='.$this->_table_users.'.userid');
		$this->db->join('ah_jobs','ah_jobs.jobsid=ah_candidate.positionappliedfor');
		$this->db->where($this->_table_users.'.userstatus','1');
		$this->db->where($this->_table_users.'.isemploy','1');
		$this->db->where($this->_table_users.'.delete_record','0');
        $this->db->order_by($this->_table_userprofile.".fullname", "ASC");*/
		
		
		$query	=	$this->db->query("SELECT `ah_users`.`userid`, 
		`ah_listmanagement`.`list_name`, 
		`ah_userprofile`.`fullname`, 
		`ah_userprofile`.`resume`, 
		`ah_userprofile`.`email`, 
		`ah_userprofile`.`gender`, 
		`ah_jobs`.`jobtitle`, 
		`ah_jobs`.`jobsid`, 
		`interview_response`.`status`,
		`ah_candidate`.`applydate`
		FROM (`ah_users`)
			JOIN `ah_userprofile` ON `ah_userprofile`.`userid`=`ah_users`.`userid`
			JOIN `ah_listmanagement` ON `ah_listmanagement`.`list_id`=`ah_userprofile`.`maritialstatus`
			JOIN `ah_candidate` ON `ah_candidate`.`userid`=`ah_users`.`userid`
			LEFT JOIN `interview_response` ON `interview_response`.`userid`=`ah_users`.`userid`
			JOIN `ah_jobs` ON `ah_jobs`.`jobsid`=`ah_candidate`.`positionappliedfor`
		WHERE `ah_users`.`userstatus` =  '1'
			AND `ah_users`.`isemploy` =  '1' 
			AND `ah_users`.`delete_record` =  '0'
		ORDER BY `ah_userprofile`.`fullname` ASC");

		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	* Get All Approved Candidayes
	* return OBJECT
	*/
	public function get_all_approved_candidate()
	{	
		$query	=	$this->db->query("SELECT `ah_users`.`userid`, 
		`ah_listmanagement`.`list_name`, 
		`ah_userprofile`.`fullname`, 
		`ah_userprofile`.`resume`, 
		`ah_userprofile`.`email`, 
		`ah_userprofile`.`gender`, 
		`ah_jobs`.`jobtitle`, 
		`ah_jobs`.`jobsid`,
		`interview_response`.`status`,
		`interview_response`.`interview_responseid`,
		`ah_candidate`.`applydate`
		FROM (`ah_users`)
			JOIN `ah_userprofile` ON `ah_userprofile`.`userid`=`ah_users`.`userid`
			JOIN `ah_listmanagement` ON `ah_listmanagement`.`list_id`=`ah_userprofile`.`maritialstatus`
			JOIN `ah_candidate` ON `ah_candidate`.`userid`=`ah_users`.`userid`
			JOIN `interview_response` ON `interview_response`.`userid`=`ah_users`.`userid`  AND `interview_response`.`status` =  '1'
			JOIN `ah_jobs` ON `ah_jobs`.`jobsid`=`ah_candidate`.`positionappliedfor`
		WHERE `ah_users`.`userstatus` =  '1'
			AND `ah_users`.`isemploy` =  '1' 
			AND `ah_users`.`delete_record` =  '0'
		ORDER BY `ah_userprofile`.`fullname` ASC");

		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	* Get All Reject Candidayes
	* return OBJECT
	*/
	public function get_all_reject_candidate()
	{	
		$query	=	$this->db->query("SELECT `ah_users`.`userid`, 
		`ah_listmanagement`.`list_name`, 
		`ah_userprofile`.`fullname`, 
		`ah_userprofile`.`resume`, 
		`ah_userprofile`.`email`, 
		`ah_userprofile`.`gender`, 
		`ah_jobs`.`jobtitle`, 
		`ah_jobs`.`jobsid`, 
		`interview_response`.`status`,
		`interview_response`.`interview_responseid`,
		`ah_candidate`.`applydate`
		FROM (`ah_users`)
			JOIN `ah_userprofile` ON `ah_userprofile`.`userid`=`ah_users`.`userid`
			JOIN `ah_listmanagement` ON `ah_listmanagement`.`list_id`=`ah_userprofile`.`maritialstatus`
			JOIN `ah_candidate` ON `ah_candidate`.`userid`=`ah_users`.`userid`
			JOIN `interview_response` ON `interview_response`.`userid`=`ah_users`.`userid`  AND `interview_response`.`status` =  '2'
			JOIN `ah_jobs` ON `ah_jobs`.`jobsid`=`ah_candidate`.`positionappliedfor`
		WHERE `ah_users`.`userstatus` =  '1'
			AND `ah_users`.`isemploy` =  '1' 
			AND `ah_users`.`delete_record` =  '0'
		ORDER BY `ah_userprofile`.`fullname` ASC");

		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
	
	function interview_remarks($interview_responseid)
	{
		$query	=	$this->db->query("SELECT `ah_users`.`userid`, 
		`ah_listmanagement`.`list_name`, 
		`ah_userprofile`.`fullname`, 
		`ah_userprofile`.`resume`, 
		`ah_userprofile`.`email`, 
		`ah_userprofile`.`gender`, 
		`ah_jobs`.`jobtitle`, 
		`ah_jobs`.`jobsid`, 
		`interview_response`.*,
		`ah_candidate`.`applydate`
		FROM (`ah_users`)
			JOIN `ah_userprofile` ON `ah_userprofile`.`userid`=`ah_users`.`userid`
			JOIN `ah_listmanagement` ON `ah_listmanagement`.`list_id`=`ah_userprofile`.`maritialstatus`
			JOIN `ah_candidate` ON `ah_candidate`.`userid`=`ah_users`.`userid`
			LEFT JOIN `interview_response` ON `interview_response`.`userid`=`ah_users`.`userid`
			JOIN `ah_jobs` ON `ah_jobs`.`jobsid`=`ah_candidate`.`positionappliedfor`
		WHERE `ah_users`.`userstatus` =  '1'
			AND `ah_users`.`isemploy` =  '1' 
			AND `ah_users`.`delete_record` =  '0' AND `interview_response`.`interview_responseid`=".$interview_responseid."
		ORDER BY `ah_userprofile`.`fullname` ASC");

		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}	
//-------------------------------------------------------------------
	/*
	* Add Schedule
	* @param $data ARRAY
	* return TRUE
	*/

	function add_schedule($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_schedule,$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	* Add Schedule
	* @param $data ARRAY
	* return TRUE
	*/
	
	function all_schedule($userid)
    {
		$this->db->where('userid',$userid);
		$this->db->order_by('scheduleid','DESC');
        $query = $this->db->get($this->_table_schedule);
		
		if($query->num_rows > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------
	/*
	* Add Remarks on Interview
	* @param $data ARRAY
	* return TRUE
	*/

	function add_response($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_interview_response,$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	* Get All remarks about Interviews
	* @param $data ARRAY
	* return TRUE
	*/
	
	function all_interview_responses($userid)
    {
		$this->db->where('userid',$userid);
		$this->db->order_by('interview_responseid','DESC');
        $query = $this->db->get($this->_table_interview_response);
		
		if($query->num_rows > 0)
		{
			return $query->result();
		}
    }	

//-------------------------------------------------------------------	
}

?>
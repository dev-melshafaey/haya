<div class="row col-md-12">
    <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
        <tbody>
            <tr>
                <td colspan="4" class=" btn-success text-right">التفاصيل</td>
            </tr>
            <tr>
                <td class="right"><label class="text-warning">التاريخ</label><br><strong><?php echo $row->voucher_date; ?></strong></td>
                                <td class="right"><label class="text-warning">نوع السند</label><br><strong><?php echo $row->pay_type == 'cash' ? 'نقدي' :'شيك'; ?></strong></td>
                                <td class="right"><label class="text-warning">قائمة البنود</label><br /><strong><?php echo $this->haya_model->get_name_from_list($row->terms_list);?></strong></td>
            </tr>

            <?php if ($row->pay_type == 'cheque') { ?>
                <tr>
                    <td class="right"><label class="text-warning">رقم الشيك</label><br><strong><?php echo $row->cheque_num; ?></strong></td>
                    <td class="right"><label class="text-warning">تاريخ الشيك</label><br><strong><?php echo $row->cheque_date; ?></strong></td>
                    <td class="right"><label class="text-warning">البنك</label><br><strong><?php echo $row->cheque_bank; ?></strong></td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="3" class="right"><label class="text-warning">البيان</label><br><strong><?php echo $row->description; ?></strong></td>
            </tr> 
<?php $package_detail	=	$this->budget->get_package_detail($row->budget_id,$row->budget_cat_id);?>
<?php $bank_detail		=	$this->budget->get_banks($row->bank_account)?>
            <tr>
                <td colspan="3" class=" btn-success text-right">الموازنة والحسابات البنكية</td>
            </tr>
        <tr>
        	<td class="right"><label class="text-warning">اختر الموازنة</label><br /><strong><?php echo $package_detail->bd_year;?></strong></td>
            <td class="right"><label class="text-warning">اختر الميزانية</label><br /><strong><?php echo $package_detail->bds_title;?></strong></td>
            <td class="right"><label class="text-warning"> اختار رقم الحساب البنكي</label><br /><strong><?php echo $bank_detail->bank_name;?> (<?php echo $bank_detail->account_no;?>)</strong></td>
        </tr>
        </tbody>
    </table> 

</div>

<div class="row col-md-12">
  <form class="mws-form" method="post" action="<?php echo base_url();?>budget/confirm_proceed" id="confirm_proceed" name="confirm_proceed">
  <div class="col-md-6 form-group">
      <label class="text-warning">رقم الايصال :</label>
      <input name="unique_id" value="<?PHP echo($row->receipt_num); ?>" placeholder="الإسم" id="unique_id" type="text" class="form-control" readonly>
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الإسم :</label>
      <input name="user_name" value="<?PHP echo($row->donater_name); ?>" placeholder="الإسم" id="user_name" type="text" class="form-control" readonly>
    </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم البرنامج :</label>
      <input name="program_name" value="<?PHP echo($row->program_name); ?>" placeholder="الإسم" id="program_name" type="text" class="form-control" readonly>
    </div>
              <div class="col-md-6 form-group">
      <label class="text-warning">كمية:</label>
      <input name="amount" value="<?PHP echo($row->amount); ?>" placeholder="الإسم" id="amount" type="text" class="form-control" readonly >
    </div>
    
    <br clear="all"/>
    <br clear="all"/>
    <div class="col-md-6 form-group">
      <label for="basic-input"></label>
      <input type="hidden" name="budget_id" id="budget_id" value="<?php echo $row->budget_id;?>" />
      <input type="hidden" name="budget_cat_id" id="budget_cat_id" value="<?php echo $row->budget_cat_id;?>" />
      <input type="hidden" name="bank_account" id="bank_account" value="<?php echo $row->bank_account;?>" />
      <input type="hidden" name="id" id="id" value="<?php echo $row->id;?>" />
      <input type="hidden" name="terms_list" id="terms_list" value="<?php echo $row->terms_list;?>" />
      <?php if($row->amount_proceed	==	0):?>
      <input type="submit" value="تأكيد الحفظ" name="submit_medical" class="btn btn-success mws-login-button" />
      <?php endif;?>
    </div> <div style="color:#F00;" id="alerts-medical"> </div >
    <br clear="all" />
  </form>
</div>
<!------------------------------------------------------------------------>

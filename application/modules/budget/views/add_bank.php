<h4 style="border-bottom: 2px solid #EEE;">تفاصيل البنك:</h4>
<?php if ($type == 'view') { ?>
    <div class="row col-md-12">
        <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
            <tbody>
                <tr>
                    <td colspan="4" class=" btn-success text-right"></td>
                </tr>
                <tr>
                    <td class="right"><label class="text-warning">اسم البنك :</label><br><strong><?php echo $bank->bank_name ?></strong></td>
                    <td class="right"><label class="text-warning">الفرع:</label><br><strong><?= $bank->branch ?></strong></td>
                    <td class="right"><label class="text-warning">رقم الحساب :</label><br><strong><?= $bank->account_no ?></strong></td>
                    <td class="right"><label class="text-warning">الرصيد الإفتتاحي</label><br><strong><?= 'OMR ' . number_format($bank->ammount, 3, '.', ','); ?></strong></td>
                </tr>
                <tr>
                    <td class="right"><label class="text-warning">الهاتف :</label><br><strong><?= $bank->tel ?></strong></td>
                    <td class="right"><label class="text-warning">الفاكس</label><br><strong><?= $bank->fax ?></strong></td>
                    <td class="right"><label class="text-warning">البريد الإلكتروني</label><br><strong><?= $bank->email ?></strong></td>
                    <td class="right"><label class="text-warning">السويفت كود:</label><br><strong><?= $bank->swift_code ?></strong></td>
                </tr>
            </tbody>
        </table>
    </div>
<?php } else { ?>
    <form  method="POST" id="save_bank_management_form2" name="save_bank_management_form2">
        <input type="hidden" name="id" id="id" value="<?= $bank->id ? $bank->id : false ?>">
        <div class="multiple-block">
            <div class="col-md-3 form-group">
                <label class="text-warning">إسم البنك:</label>

                <input value="<?= $bank->bank_name ?>" name="bank_name"  placeholder="اسم البنك" id="bank_name" type="text" class="form-control req" >
            </div>
            <div class="col-md-3 form-group">
                <label class="text-warning">الفرع:</label>

                <input  value="<?= $bank->branch ?>" name="branch"  placeholder="الفرع" id="branch" type="text" class="form-control req" >
            </div>
            <div class="col-md-3 form-group">
                <label class="text-warning">رقم حساب  :</label>
                <input  value="<?= $bank->account_no ?>" name="account_no"  placeholder="رقم حساب" id="account_no" type="text" class="form-control req" onKeyUp="only_numeric(this);">
            </div>
            <div class="col-md-3 form-group">
                <label class="text-warning">الرصيد الإفتتاحي  :</label>
                <input  value="<?= $bank->ammount ?>" name="ammount" placeholder="القيمة" id="ammount" type="text" class="form-control amount_in_account req" onKeyUp="add_total_budget(this);">
            </div>
            <div class="col-md-3 form-group">
                <label class="text-warning">رقم الهاتف  :</label>
                <input  value="<?= $bank->tel ?>" name="tel"  placeholder="رقم الهاتف" id="tel" type="text" class="form-control" onKeyUp="only_numeric(this);">
            </div>
            <div class="col-md-3 form-group">
                <label class="text-warning">رقم الفاكس  :</label>
                <input  value="<?= $bank->fax ?>" name="fax"  placeholder="رقم الفاكس" id="fax" type="text" class="form-control" onKeyUp="only_numeric(this);">
            </div>
            <div class="col-md-3 form-group">
                <label class="text-warning">البريد الإلكتروني  :</label>
                <input  value="<?= $bank->email ?>" name="email"  placeholder="البريد الإلكتروني" id="email" type="text" class="form-control">
            </div>
            <div class="col-md-3 form-group">
                <label class="text-warning">Swift Code  :</label>
                <input  value="<?= $bank->swift_code ?>" name="swift_code"  placeholder="Swift Code" id="swift_code" type="text" class="form-control req" >
            </div>
        </div>
        <br clear="all"/>
        <div class="col-md-12 form-group" id="multifields"></div>
        <div class="col-md-12 form-group">
            <button type="button" id="save_bank_management" class="btn btn-success">حفظ</button>
        </div>
    </form>
<?php } ?>

<script>
    $(document).ready(function () {
        $('#save_bank_management').click(function () {
            check_my_session();
            $('#save_bank_management_form2 .req').removeClass('parsley-error');
            var ht = '<ul>';
            $('#save_bank_management_form2 .req').each(function (index, element) {
                if ($(this).val() == '') {
                    $(this).addClass('parsley-error');
                    ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
                }
            });
            var redline = $('#save_bank_management_form2 .parsley-error').length;

            ht += '</ul>';

            if (redline <= 0) {
                var request = $.ajax({
                    url: config.BASE_URL + 'budget/add_new_bank',
                    type: "POST",
                    data: $('#save_bank_management_form2').serialize(),
                    dataType: "html",
                    beforeSend: function () {
                        $('#ajax_action').show();
                        $(this).hide();
                    },
                    success: function (msg)
                    {
                        if (msg) {
                            $('#ajax_action').hide();

                            $('#addingDiag').modal('hide');
                            show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
                            location.reload();
                        } else {
                            var ht = '<ul><li>حدث خطأ .. العملية لم تتم</li></ul>';
                            show_notification_error_end(ht);
                        }
                    }
                });
            }
            else
            {
                show_notification_error_end(ht);
            }

        });
    });
</script>
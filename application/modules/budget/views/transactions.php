<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <br clear="all"/>
      <div class="col-md-12 form-group">
        <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
         <div class="row table-header-row">
         </div>
        	<table class="table table-bordered table-striped dataTable newbasicTable" aria-describedby="tableSortable_info" style="width: 1300px;">
                  <thead>
                    <tr role="row">
                      <th style="text-align:center;">Transaction ID</th>
                      <th style="text-align:center;">المساعدات</th>
                      <th style="text-align:center;">قائمة البنود</th>
                      <th style="text-align:center;">package Detail</th>
                      <th style="text-align:center;">التفاصيل البنك</th>
                      <th style="text-align:center;">تاريخ</th>
                      <th style="text-align:center;">CREDIT</th>
                      <th style="text-align:center;">DEBIT</th>
                      <?php if($bankid):?>
                      <th style="text-align:center;">REMAINING</th>
                      <?php endif;?>
                    </tr>
                  </thead>
                  <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php $all_transactions	=	$this->budget->get_all_transactions($bankid);?>
                    <?php
					
					?>
                    <?php if(!empty($all_transactions)):?>
                    <?php $credit	=	''; $debit	=	'';?>
						<?php foreach($all_transactions as $transaction):?>
                        <?php if($transaction->transaction_type	==	'CREDIT'):?>
                        	<?php $credit	+=	$transaction->amount;?>
                        <?php endif;?>
                        <?php if($transaction->transaction_type	==	'DEBIT'):?>
                        	<?php $debit	+=	$transaction->amount;?>
                        <?php endif;?>
                        <?php 
						$package_detail	=	$this->budget->get_package_detail($transaction->budget_id,$transaction->budget_cat_id);
						?>
                            <tr role="row" id="<?php echo $transaction->transaction_id;?>">
                              <td  style="text-align:center;"><?php echo(isset($transaction->unique_id) ? $transaction->unique_id : 'DX58FF32100');?></td>
                              <td  style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($transaction->charity_type_id);?></td>
                               <td  style="text-align:center;"><?php echo $this->haya_model->get_name_from_list($transaction->terms_list);?></td>
                              <td  style="text-align:center;"><?php echo $package_detail->bd_year. ' / ' . $package_detail->bds_title;?></td>
                              <td  style="text-align:center;"><?php echo $transaction->bank_name;?></td>
                              <td  style="text-align:center;"><?php echo $transaction->submit_date;?></td>
                              <td  style="text-align:center;"><?php echo ((isset($transaction->transaction_type) AND $transaction->transaction_type	==	'CREDIT') ? $transaction->amount : '-- --');?></td>
                              <td style="text-align:center;"><?php echo ((isset($transaction->transaction_type) AND $transaction->transaction_type	==	'DEBIT') ? $transaction->amount : '-- --');?></td>
                               <?php if($bankid):?>
                               <td  style="text-align:center;"><?php echo number_format($remaining_amount,3);?></td>
                               <?php endif;?>
                            </tr>
                            
                        <?php endforeach;?>
                        <tr role="row">
                            <td colspan="7"></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr role="row">
                            <td colspan="6"></td>
                            <td><?php echo number_format($credit,3);?></td>
                            <td><?php echo number_format($debit,3);?></td>

                        </tr>
                    <?php endif;?>
                  </tbody>
                </table>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
	$(function(){
		$('#charity_type_id').change(function(){
			
			var charity_id	=	$("#charity_type_id").val();
			
		var request = $.ajax({
		  url: config.BASE_URL+'bank_management/get_account_number',
		  type: "POST",
		  data: {charity_id:charity_id},
		  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(data)
		  {
			  $("#dynamic-data").html(data);
		  } 
		});
			
			//alert('working!!!');
			});
	 $('#save_transaction').click(function(){
		 $('#frm_bank_management .req').removeClass('parsley-error');
			var ht = '<ul>';
			$('#frm_bank_management .req').each(function (index, element) {
				if ($(this).val() == '') 
				{
					$(this).addClass('parsley-error');
					ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
				}
			});
			var redline = $('#frm_bank_management .parsley-error').length;
			//var redline = 0;
			ht += '</ul>';			
			if (redline <= 0) 
			{	
				$("#frm_bank_management").submit();	
			}
			else 
			{	
				show_notification_error(ht);
			}	 
		 
	});
	});
</script>
</div>
</body>
</html>
<?php $permissions = $this->haya_model->check_other_permission(array($module['moduleid'])); ?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<style>
.row {
	margin: 15px 0;
}
</style>
<style>
h2 {
	text-shadow: 0px -1px 0px #374683;
	text-shadow: 0px 1px 0px #e5e5ee;
	filter: dropshadow(color=#e5e5ee, offX=0, offY=1);
	font-family: "Courier New", Courier, monospace;
	margin: 0px;
}
</style>
<style>
.both h4 {
	font-family: Arial, Helvetica, sans-serif;
	margin: 0px;
	font-size: 14px;
}
#search_category_id {
	padding: 3px;
	width: 200px;
}
.parent {
	padding: 3px;
	float: right;
	margin-right: 12px;
	width: 31%;
	margin-top: 8px;
}
.bank-parent {
	padding: 3px;
	float: right;
	margin-right: 12px;
	width: 31%;
	margin-top: 8px;
}
.both {
	float: left;
	margin: 0 0px 0 0;
	padding: 0px;
}
</style>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default panel-block panel-title-block">
        <div class="panel-heading">
          <h4 class="tex-right" style="">سند قبض</h4>
        </div>
      </div>
      <?php $msg = $this->session->flashdata('msg'); ?>
      <?php if ($msg): ?>
      <div class="col-md-12">
        <div style="padding: 22px 20px !important; background:#c1dfc9;">
          <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg; ?></h4>
        </div>
      </div>
      <?php endif; ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <form  method="POST" id="exchange_voucher_form" name="" >
                    <div class="panel panel-success">
                      <div class="panel-body">
                        <div class="row">
                          <input type="hidden" name="id" value="<?= $row->id ?>">
                          <div class="form-group col-md-4">
                            <label class="text-warning pull-right">رقم الايصال</label>
                            <input id="receipt_num" value="<?= $row->receipt_num ?>" readonly name="receipt_num" type="text" class="form-control req " >
                          </div>
                          
                          <!--                                                        <div class="form-group col-md-2"></div>-->
                          
                          <div class="form-group col-md-3">
                            <label class="text-warning pull-right">التاريخ</label>
                            <input id="voucher_date" value="<?= $row->voucher_date ?>" placeholder="التاريخ" name="voucher_date" type="text" class="form-control req dateinput" >
                          </div>
                          <div class="form-group col-md-3">
                            <label class="text-warning pull-right">المبلغ</label>
                            <input name="amount" id="amount" value="<?= $row->amount ?>" placeholder="المبلغ" type="number" class="form-control req" >
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-4">
                            <label class="text-warning pull-right">إسم البرنامج:</label>
                            <input name="program_name" value="<?= $row->program_name ?>" placeholder="اسم البرنامج" type="text" class="col-md-4 form-control  req " >
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-4">
                            <label class="text-warning pull-right">إسم المتبرع:</label>
                            <input name="donater_name" value="<?= $row->donater_name ?>" placeholder="اسم المتبرع" type="text" class="form-control req " >
                          </div>
                          <!--                                                        <div class="form-group col-md-2"></div>-->
                          <div class="form-group col-md-3">
                            <label class="text-warning pull-right">نوع السند :</label>
                            <select id="pay_type" name="pay_type" onchange="get_details()" class="form-control required">
                              <option <?php echo $row->pay_type == 'cash' ? 'selected' : '' ?> value="cash">نقدي</option>
                              <option <?php echo $row->pay_type == 'cheque' ? 'selected' : '' ?> value="cheque">شيك</option>
                            </select>
                          </div>
                        </div>
                        <div class="row" id="checqdetails" style="display: none">
                          <div class="form-group col-md-4">
                            <label class="text-warning pull-right">رقم الشيك :</label>
                            <input value="<?= $row->cheque_num ?>" name="cheque_num" type="text" class="form-control  " >
                          </div>
                          <div class="form-group col-md-3">
                            <label class="text-warning pull-right">التاريخ :</label>
                            <input value="<?= $row->cheque_date ?>" type="text" name="cheque_date" class="form-control  dateinput" >
                          </div>
                          <div class="form-group col-md-3">
                            <label class="text-warning pull-right">البنك :</label>
                            <input value="<?= $row->cheque_bank ?>" type="text" name="cheque_bank" class="form-control  " >
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12">
                            <label class="text-warning pull-right">البيان</label>
                            <textarea value="<?= $row->description ?>" rows="5" name="description" class="form-control"></textarea>
                          </div>
                        </div>
                        <div class="col-md-4 form-group">
            <label class="text-warning pull-right">قائمة البنود:</label>
            <?PHP echo $this->haya_model->create_dropbox_list('terms_list','terms_list',$row->terms_list,0,'req'); ?> </div>
                        <div class="col-md-12">
                          <h4>الموازنة والحسابات البنكية:</h4>
                          <!------------------------------------------------->
                          <div id="show_sub_categories">
                            <select name="search_category" class="parent form-control req">
                              <option value="" selected="selected">اختر الموازنة</option>
							<?php
								if(!empty($all_budgets))
								{
									foreach($all_budgets as $budget)
									{
										echo '<option value="'.$budget->bd_id.'" data-id="parent" >'.$budget->bd_year.'</option>';
									}
								}
                            ?>
                            </select>
                          </div>
                        </div>
                        <br clear="all">
                        <div class="col-md-12 form-group">
                         <input type="hidden" name="account_balance" id="account_balance" value="00.000" />
                         <input type="hidden" name="package_balance" id="package_balance" value="00.000" />
                          <button type="button" id="save_exchange_voucher" class="btn btn-success">حفظ</button>
                        </div>
                        <div id="alerts-medical" style="color:#F00;"></div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
        $(document).ready(function () {
            if ($('#receipt_num').val() === '') {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yy = today.getFullYear();
                var hh = today.getHours();
                var ii = today.getMinutes();
                var ss = today.getSeconds();

                $('#receipt_num').val('RV'.concat(yy, mm, dd, hh, ii, ss));
            }
			
            $('#save_exchange_voucher').click(function () {
                check_my_session();
                $('#exchange_voucher_form .req').removeClass('parsley-error');
                var ht = '<ul>';
                $('#exchange_voucher_form .req').each(function (index, element) {
                    if ($(this).val() == '') {
                        $(this).addClass('parsley-error');
                        ht += '<li>' + $(this).attr('placeholder') + ' مطلوب  </li>';
                    }
                });
                var redline = $('#exchange_voucher_form .parsley-error').length;

                ht += '</ul>';

                if (redline <= 0) 
				{
					var account_balance			=	$("#account_balance").val();
					var package_balance			=	$("#package_balance").val();
					var total_appl_amount		=	$("#amount").val();
					
					account_balance 		=	parseFloat(account_balance.replace(/,/g, ''));
					package_balance 		=	parseFloat(package_balance.replace(/,/g, ''));
					total_appl_amount 		=	parseFloat(total_appl_amount.replace(/,/g, ''));
					
					var request = $.ajax({
						url: config.BASE_URL + 'budget/save_exchange_voucher',
						type: "POST",
						data: $('#exchange_voucher_form').serialize(),
						dataType: "html",
						beforeSend: function () {
							$('#ajax_action').show();
							$(this).hide();
						},
						success: function (msg)
						{
							if (msg) {
								$('#ajax_action').hide();

								$('#addingDiag').modal('hide');
								show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
								setTimeout(location.reload(), 6000)
							} else {
								var ht = '<ul><li>حدث خطأ .. العملية لم تتم</li></ul>';
								show_notification_error_end(ht);
							}
						}
					});
                }
                else
                {
                    show_notification_error_end(ht);
                }

            });
			
			/**************************************************/
			 /*************** Muzaffar Work ******************/
			/**************************************************/
			 $('.parent').livequery('change', function() {

            $(this).nextAll('.parent').remove();
			$(this).nextAll('.bank-parent').remove();
            $(this).nextAll('label').remove();
			$("#alerts").html('');

            $('#show_sub_categories').append('<img src="<?php echo base_url();?>assets/images/hourglass.gif" style="float:right; margin-top:7px;" id="loader" alt="" />');
            $.post(config.BASE_URL+'company/get_child_categories', {
				type: $(this).find(':selected').attr('data-id'),
                parent_id: $(this).val(),
            }, function(response){
                setTimeout("finishAjax('show_sub_categories', '"+escape(response)+"')", 400);
            });
            return false;
        });

        });

        function get_details() {
            if ($('#pay_type').val() === 'cheque') {
                $('#checqdetails').show(200);
            } else {
                $('#checqdetails').hide(200);
            }
        }
	/**************************************************/
	 /*************** Muzaffar Work ******************/
	/**************************************************/
    function finishAjax(id, response)
	{
		
      $('#loader').remove();
	  
      $('#'+id).append(unescape(response));
	  
    
	}
	function amount_proceed()
	{
		
	
		var account_balance			=	$("#account_balance").val();
		var package_balance			=	$("#package_balance").val();
		var total_appl_amount		=	$("#amount").val();
		
		account_balance 		=	parseFloat(account_balance.replace(/,/g, ''));
		package_balance 		=	parseFloat(package_balance.replace(/,/g, ''));
		total_appl_amount 		=	parseFloat(total_appl_amount.replace(/,/g, ''));
		
		if(parseInt(account_balance)	<	parseInt(total_appl_amount))
		{
			$("#alerts-medical").html('<strong>There is less balance in Local Account!</strong>');
		}
		else if(parseInt(package_balance)	<	parseInt(total_appl_amount))
		{
			$("#alerts-medical").html('<strong>There is less amount in your Package!</strong>');
		}
		else
		{
			document.getElementById("medical_applicant").submit(); // Submit From JAVASCRIPT
		}
	
	}
/****************************************************************/
/*****************************Muzaffar Work*******************/
/****************************************************************/	
	function get_detail()
	{
		var local_bank_id	=	$(".bank-parent").val();
		var account_balance	=	$(".bank-parent").find('option:selected').attr('account-balance');
		var package_balance	=	$(".bank-parent").find('option:selected').attr('package-balance');
		
		// SET VALUES into hidden fields
		$("#account_balance").val(account_balance);
		$("#package_balance").val(package_balance);
	
	}
    </script> 
<?php echo '<script type="text/javascript">get_details();</script>'; ?>
</body>
</html>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Budget extends CI_Controller {

    private $_data = array();
    private $_login_userid = NULL;
    public $administrator = NULL;
    public $servicedepartment = NULL;
    public $moduleid = NULL;
    public $otherservice_moduleid = NULL;

    function __construct() {
        parent::__construct();
        $this->load->model('budget_model', 'budget');
        $this->load->model('company/company_model', 'company');
        /////////////////////////////////////////	
        $this->_data['controller'] = $this;
        $this->moduleid = 264;
        /////////////////////////
        $this->_data['moduleid'] = $this->moduleid;

        $this->_data['module'] = $this->haya_model->get_module();
        $this->_login_userid = $this->session->userdata('userid');
        $this->_data['login_userid'] = $this->_login_userid;
        $this->_data['user_detail'] = $this->haya_model->get_user_detail($this->_login_userid);


        $this->_data['login_userid'] = $this->_login_userid;
        $this->_data['user_detail'] = $this->haya_model->get_user_detail($this->_login_userid);
        $this->userroleid = $this->_data['user_detail']['profile']->userroleid;
        // Load all types
        $this->_data['list_types'] = $this->haya_model->get_listmanagment_types();

        //$this->add_users_yearly_holidays(); // Add Yearly holiday into users accounts.
    }

    public function index() {
        //$permissions = $this->haya_model->check_other_permission(array($this->moduleid));
        $this->load->view('listall_budget_by_year', $this->_data);
    }

    public function ajax_all_budget_by_year() {
		/*
		 $this->db->select('
		ah_budget_main.*,
		ah_budget_main_in.bd_totalamount as bd_totalamount2,
		ah_budget_main_in.bd_remainingAmount as bd_remainingAmount2
		');
        $this->db->from('ah_budget_main');
        $this->db->join('ah_budget_main_in', 'ah_budget_main_in.bd_id = ah_budget_main.bd_id','LEFT');
        $this->db->where("ah_budget_main.delete_record", '0');
        $this->db->order_by('ah_budget_main.bd_id', 'DESC');

        $query = $this->db->get();
		*/
        $this->db->select('ah_budget_main.*,ah_budget_main_in.bd_totalamount as bd_totalamount2,ah_budget_main_in.bd_remainingAmount as bd_remainingAmount2');
        $this->db->from('ah_budget_main');
        $this->db->join('ah_budget_main_in', 'ah_budget_main_in.bd_id = ah_budget_main.bd_id','LEFT');
        $this->db->where("ah_budget_main.delete_record", '0');
        $this->db->order_by('ah_budget_main.bd_id', 'DESC');

        $query = $this->db->get();
        //var_dump($this->db->last_query());
        //$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));

        foreach ($query->result() as $index => $lc) {


            if ($lc->bd_id) {
                $action = '<a onclick="alatadad(this);" data-url="' . base_url() . 'budget/view_budget_by_year/' . $lc->bd_id . '" href="#"><i class="my icon icon-eye-open"></i></a>';
            }
            if ($lc->userid == $this->_login_userid) {
                $action .= ' <a href="#" onclick="alatadad(this);" data-url="' . base_url() . 'budget/add_budget_by_year/' . $lc->bd_id . '" id="' . $lc->bd_id . '"><i class="icon-pencil"></i></a>';
            }

            /* if($permissions[$this->moduleid]['d']	==	1 ) 
              { */
            if ($lc->bd_totalamount == $lc->bd_remainingAmount && $lc->bd_totalamount2 == $lc->bd_remainingAmount2) {
                $action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="' . $lc->bd_id . '" data-url="' . base_url() . 'budget/delete_budget_by_year/' . $lc->bd_id . '"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
                /* } */
            }

            $arr[$index] = array(
                "DT_RowId" => $lc->bd_id . '_durar_lm',
                "Year" => '<a href="' . base_url() . 'budget/list_budget_by_department/' . $lc->bd_id . '/0" >' . $lc->bd_year . '</a>',
                "Total Amount" => '<a href="' . base_url() . 'budget/list_budget_by_department/' . $lc->bd_id . '/0" > OMR ' . number_format($lc->bd_totalamount, 3, '.', ',') . '</a>',
                "Remaining Amount" => '<a href="' . base_url() . 'budget/list_budget_by_department/' . $lc->bd_id . '/0" > OMR ' . number_format($lc->bd_remainingAmount, 3, '.', ',') . '</a>',
                "Total Amount2" => '<a href="' . base_url() . 'budget/list_budget_by_department/' . $lc->bd_id . '/0" > OMR ' . number_format($lc->bd_totalamount2, 3, '.', ',') . '</a>',
                "Remaining Amount2" => '<a href="' . base_url() . 'budget/list_budget_by_department/' . $lc->bd_id . '/0" > OMR ' . number_format($lc->bd_remainingAmount2, 3, '.', ',') . '</a>',
                "الإجراءات" => $action
            );
        }

        $ex['data'] = $arr;
        echo json_encode($ex);
    }

    public function add_budget_by_year($bd_id = 0) {
        if ($this->input->post()) {
            $data = $this->input->post();

            $buget["bd_year"] = $data["bd_year"];
            $buget["bd_totalamount"] = str_replace(",", "", $data["bd_totalamount"]);
            $buget["bd_remainingAmount"] = str_replace(",", "", $data["bd_totalamount"]);
            $buget["bd_date"] = date('Y-m-d');
            $buget["userid"] = $this->_login_userid;
            if ($data["bd_id"] <= 0) {
                $buget["bd_id"] = 0;
            } else {
                $buget["bd_id"] = $data["bd_id"];
            }

            $re = $this->budget->savestore($buget, 'ah_budget_main', 'bd_id');

            $buget["bd_totalamount"] = str_replace(",", "", $data["bd_totalamount2"]);
            $buget["bd_remainingAmount"] = str_replace(",", "", $data["bd_totalamount2"]);
            $re2 = $this->budget->savestore($buget, 'ah_budget_main_in', 'bd_id');
            if ($re != 0 && $re2 != 0) {
                $this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
            } else {
                $this->session->set_flashdata('msg', '* المبلغ المكتوب لايطابق الحد الأدنى');
            }
        }
        $this->_data['bd_id'] = $bd_id;
        if ($bd_id > 0) {

            $this->_data['row'] = $this->budget->getdata($bd_id, 'ah_budget_main', 'bd_id');
            $this->_data['row2'] = $this->budget->getdata($bd_id, 'ah_budget_main_in', 'bd_id');
        }
        $this->load->view('add_budget_by_year', $this->_data);
    }

    function delete_budget_by_year($bd_id) {//mahmoud
        $this->budget->delete($bd_id, 'bd_id', "ah_budget_main");
        $this->budget->delete($bd_id, 'bd_id', "ah_budget_main_in");
    }

    function delete_budget_by_department($bds_id, $list_type) {
        $this->budget->delete_budget_by_department($bds_id, $list_type);
    }

    function view_budget_by_year($bd_id) {
        $this->_data['row'] = $this->budget->getdata($bd_id, 'ah_budget_main', 'bd_id');
        $this->_data['row2'] = $this->budget->getdata($bd_id, 'ah_budget_main_in', 'bd_id');
        $this->load->view('view_budget_by_year', $this->_data);
    }

    function view_budget_by_department($bds_id, $list_type) {
        if ($list_type == 'in') {
            $this->_data['row'] = $this->budget->getdata($bds_id, 'ah_budget_sub_in', 'bds_id');
        } else {
            $this->_data['row'] = $this->budget->getdata($bds_id, 'ah_budget_sub', 'bds_id');
        }
        $this->load->view('view_budget_by_department', $this->_data);
    }

    function getBreadCrumb($bd_id, $bds_categoryId, $year) {
        $sub = $this->budget->getdata($bds_categoryId, 'ah_budget_sub', 'bds_id');
        if ($bds_categoryId == 0) {
            $this->session->set_userdata('breadcrumb', array($bd_id => 'عام ' . $year));
        }

        $this->session->userdata('breadcrumb') ? $breadcrumb = $this->session->userdata('breadcrumb') : $breadcrumb = [];
        if (!in_array($sub->bds_title, $breadcrumb)) {
            //array_push($breadcrumb, $sub->bds_title);
            $breadcrumb[$bd_id . '/' . $bds_categoryId] = $sub->bds_title;
            $this->session->set_userdata('breadcrumb', $breadcrumb);
        } else {
            $position = array_search($bd_id . '/' . $bds_categoryId, array_keys($breadcrumb));
            if ($position !== false) {
                array_slice($breadcrumb, 0, ($position + 1), FALSE);
                $breadcrumb[$bd_id . '/' . $bds_categoryId] = $sub->bds_title;
                $this->session->set_userdata('breadcrumb', $breadcrumb);
            }
        }
        return $breadcrumb;
    }

    function list_budget_by_department($bd_id, $bds_categoryId = 0, $list_type = null) {
        $this->_data['row'] = $this->budget->get_budgets($bd_id, $bds_categoryId, $list_type);

        $this->_data['breadcrumb'] = $this->getBreadCrumb($bd_id, $bds_categoryId, $this->_data['row'][0]->bd_year);
        $this->_data['bd_id'] = $bd_id;
        $this->_data['bds_categoryId'] = $bds_categoryId;
        $this->_data['list_type'] = $list_type;
        $this->load->view('list_budget_by_department', $this->_data);
    }

    function banks() {
        $this->load->view('banks', $this->_data);
    }

    function get_all_banks() {
        $this->db->select('*');
        $this->db->from('bank_managment_accounting');
        $this->db->where("delete_record", '0');

        $this->db->order_by('created_date', 'DESC');

        $query = $this->db->get();

        foreach ($query->result() as $lc) {
            $action = ' <a href="#" onclick="alatadad(this);" data-url="' . base_url() . 'budget/add_bank/' . $lc->id . '" id="' . $lc->id . '"><i class="icon-pencil"></i></a>';
            $action .= '<a onclick="alatadad(this);" data-url="' . base_url() . 'budget/view_bank/' . $lc->id . '" href="#"><i class="my icon icon-eye-open"></i></a>';

            $url = '<a href="' . base_url() . 'budget/bank_transactions/' . $lc->id . '">' . $lc->bank_name . '</a>';

            $debit_amount = $this->budget->get_debit_from_local($lc->id);

            $arr[] = array(
                "DT_RowId" => $lc->id . '_durar_lm',
                "bank_name" => $url,
                "branch" => $lc->branch,
                "account_no" => $lc->account_no,
                "ammount" => number_format(($debit_amount + $lc->ammount), 3),
                "القيمة المتبقية" => $this->haya_model->get_total_alhaya_local_amount($lc->id),
                "fax" => $lc->fax,
                "email" => $lc->email,
                "swift_code" => $lc->swift_code,
                "created_date" => $lc->created_date,
                "الإجراءات" => $action
            );

            unset($action);
        }

        $ex['data'] = $arr;

        echo json_encode($ex);
    }

    function view_bank($bank_id) {
        $this->_data['bank'] = $this->budget->get_banks($bank_id);
        $this->_data['type'] = 'view';
        $this->load->view('add_bank', $this->_data);
    }

    function add_bank($bank_id = null) {
        if ($bank_id) {
            $this->_data['bank'] = $this->budget->get_banks($bank_id);
        }
        $this->load->view('add_bank', $this->_data);
    }

    function add_new_bank() {
        $data = $this->input->post();
        if ($data['id']) {
            $this->db->where('id', $data['id']);
            echo json_encode($this->db->update('bank_managment_accounting', json_encode($data), $this->_login_userid, $data));
        } else {
            echo json_encode($this->db->insert('bank_managment_accounting', $data));
        }
    }

    public function ajax_all_budget_by_department($bd_id, $bds_categoryId, $list_type = null) {
        $this->db->select('*');
        if ($list_type == 'in') {
            $this->db->from('ah_budget_sub_in');
        } else {
            $list_type = '';
            $this->db->from('ah_budget_sub');
        }

        $this->db->where("delete_record", '0');
        $this->db->where("bd_id", $bd_id);
        $this->db->where("bds_categoryId", $bds_categoryId);
        $this->db->order_by('bds_id', 'DESC');

        $query = $this->db->get();
        //$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));

        foreach ($query->result() as $lc) {
            if ($lc->bds_id) {
                $action = '<a onclick="alatadad(this);" data-url="' . base_url() . 'budget/view_budget_by_department/' . $lc->bds_id . '/' . $list_type . '" href="#"><i class="my icon icon-eye-open"></i></a>';
            }
            if ($lc->userid == $this->_login_userid) {
                $action .= ' <a href="#" onclick="alatadad(this);" data-url="' . base_url() . 'budget/add_budget_by_department/' . $lc->bd_id . '/' . $lc->bds_categoryId . '/' . $lc->bds_id . '/' . $list_type . '" id="' . $lc->bds_id . '"><i class="icon-pencil"></i></a>';
            }
            if ($lc->bds_totalAmount == $lc->bds_remainingAmount) {
                $action .= '<a class="iconspace" href="#" onClick="show_delete_diag(this);" id="' . $lc->bds_id . '" data-url="' . base_url() . 'budget/delete_budget_by_department/' . $lc->bds_id . '/' . $list_type . '"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
            }


            $arr[] = array(
                "DT_RowId" => $lc->bd_id . '_durar_lm',
                "Title" => '<a href="' . base_url() . 'budget/list_budget_by_department/' . $lc->bd_id . '/' . $lc->bds_id . '/' . $list_type . '" >' . $lc->bds_title . '</a>',
                "Total Amount" => '<a href="' . base_url() . 'budget/list_budget_by_department/' . $lc->bd_id . '/' . $lc->bds_id . '/' . $list_type . '" >OMR ' . number_format($lc->bds_totalAmount, 3, '.', ',') . '</a>',
                "Remaining Amount" => '<a href="' . base_url() . 'budget/list_budget_by_department/' . $lc->bd_id . '/' . $lc->bds_id . '/' . $list_type . '" >OMR ' . number_format($lc->bds_remainingAmount, 3, '.', ',') . '</a>',
                "الرصيد الحالي" => $this->haya_model->get_total_package_amount($lc->bds_id),
                "الإجراءات" => $action
            );

            unset($action);
        }

        $ex['data'] = $arr;

        echo json_encode($ex);
    }

    public function add_budget_by_department($bd_id, $bds_categoryId = 0, $bds_id = 0, $list_type = null) {
        if ($list_type == 'in') {
            $table = 'ah_budget_sub_in';
        } else {
            $table = 'ah_budget_sub';
        }
        if ($bds_id > 0) {
            $this->_data['row'] = $this->budget->getdata($bds_id, $table, 'bds_id');
        }
        $this->_data['mrow'] = $this->budget->getdata($bd_id, 'ah_budget_main', 'bd_id');
        $this->_data['mrow2'] = $this->budget->getdata($bd_id, 'ah_budget_main_in', 'bd_id');
        if ($bds_categoryId > 0) {

            $this->_data['prev'] = $this->budget->getdata($bds_categoryId, $table, 'bds_id');
        }
        if ($this->input->post()) {
            $data = $this->input->post();
           
            $buget["userid"] = $this->_login_userid;
            $buget["bds_date"] = date('Y-m-d');
            $buget["bd_id"] = $bd_id = $data["bd_id"];
            $buget["bds_title"] = $data["bds_title"];
            $buget["bds_totalAmount"] = str_replace(",", "", $data["bds_totalAmount"]);
            $buget["bds_remainingAmount"] = str_replace(",", "", $data["bds_totalAmount"]);
            $buget["bds_bankname_id"] = implode(',', $data["bds_bankname_id"]);
            $buget["bds_categoryId"] = $data["bds_categoryId"];
            $buget["bds_id"] = $data["bds_id"];

            if (!isset($data["bds_id"]) || $data["bds_id"] == 0) {//add
                $this->budget->savestore($buget, 'ah_budget_sub', 'bds_id');
                $buget["bds_totalAmount"] = str_replace(",", "", $data["bds_totalAmount2"]);
                $buget["bds_remainingAmount"] = str_replace(",", "", $data["bds_totalAmount2"]);
                $buget["bds_bankname_id"] = implode(',', $data["bds_bankname_id2"]);
                $this->budget->savestore($buget, 'ah_budget_sub_in', 'bds_id');

                $remaining1 = doubleval(str_replace(',', '', $data["bds_totalAmount"]));
                $remaining2 = doubleval(str_replace(',', '', $data["bds_totalAmount2"]));
                $this->budget->update_remaining($data["bds_categoryId"], $remaining1, $remaining2, $bd_id, $data['table']);
            } 
            else {//edit
                if ($data['table'] == 'ah_budget_sub_in') {
                    $buget["bds_totalAmount"] = str_replace(",", "", $data["bds_totalAmount2"]);
                    $buget["bds_remainingAmount"] = str_replace(",", "", $data["bds_totalAmount2"]);
                    $buget["bds_bankname_id"] = implode(',', $data["bds_bankname_id2"]);
                }

                $this->budget->savestore($buget, $data['table'], 'bds_id');
            }

            $this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
            exit();
        }
        $this->_data['bd_id'] = $bd_id;
        $this->_data['bds_id'] = $bds_id;
        $this->_data['bds_categoryId'] = $bds_categoryId;
        $this->_data['table'] = $table;

        if ($bds_id > 0) {
            $this->_data['rows'] = $this->budget->getdata($bds_id, $table, 'bds_id');
        }


        $this->load->view('add_budget_by_department', $this->_data);
    }

//-----------------------------------------------------------------------
    /*     * **************Mahmood Work Start******************** */
//-----------------------------------------------------------------------
    function list_exchange_voucher() {
        $this->load->view('list_exchange_voucher', $this->_data);
    }

    function add_exchange_voucher($id = null) {
        if ($id) {
            $this->_data['row'] = $this->budget->getdata($id, 'exchange_voucher', 'id');
        }

        $this->_data['all_budgets'] = $this->company->get_all_budgets();

        $this->load->view('add_exchange_voucher', $this->_data);
    }

    function save_exchange_voucher() {
        $data = $this->input->post();

        $data['budget_id'] 		= $data['search_category'];
        $data['budget_cat_id'] 	= $data['sub_category'];
        $data['bank_account'] 	= $data['bank_accounts'];

        unset($data['search_category'], $data['sub_category'], $data['bank_accounts'], $data['account_balance'], $data['package_balance']);



        if ($data['id']) {
            $this->db->where('id', $data['id']);
            echo json_encode($this->db->update('exchange_voucher', json_encode($data), $this->_login_userid, $data));
        } else {
            echo json_encode($this->db->insert('exchange_voucher', $data));
        }
    }

    function view_voucher($id) {
        $this->_data['row'] = $this->budget->getdata($id, 'exchange_voucher', 'id');
        $this->load->view('view_voucher', $this->_data);
    }

    function delete_voucher($id) {
        $this->db->where('id', $id);
        $this->db->delete('exchange_voucher');
    }

    function get_all_exchange_voucher() {

        $query = $this->db->get('exchange_voucher');

        foreach ($query->result() as $index => $lc) {
			
			if($lc->amount_proceed	==	0)
			{
				$class	=	'<span><i style="color:#F60;" class="myicon icon-ellipsis-horizontal" title="قيد الانتظار"></i></span>';
			}
			else
			{
				$class	=	'<span><i style="color:#00CC00;" class="icon-ok" title="تقدم"></i></span>';
			}

            $actions = '<a onclick="alatadad(this)" data-url="' . base_url() . 'budget/view_voucher/' . $lc->id . '" href="#" ><i class="my icon icon-eye-open"></i></a>';
            $actions .= '<a target="_blank" href="' . base_url() . 'budget/add_exchange_voucher/' . $lc->id . '" ><i class="icon-pencil"></i></a>';
            $actions .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" data-url="' . base_url() . 'budget/delete_voucher/' . $lc->id . '"><i style="color:red" class="icon-remove-sign"></i></a>';

            $arr[$index] = array(
                'receipt_num' => '<a href="' . base_url() . 'budget/add_exchange_voucher/' . $lc->id . '">' . $lc->receipt_num . '</a>',
                'voucher_date' => '<a href="' . base_url() . 'budget/add_exchange_voucher/' . $lc->id . '">' . $lc->voucher_date . '</a>',
                'amount' => '<a href="' . base_url() . 'budget/add_exchange_voucher/' . $lc->id . '">' . $lc->amount . '</a>',
                'program_name' => '<a href="' . base_url() . 'budget/add_exchange_voucher/' . $lc->id . '">' . $lc->program_name . '</a>',
                'donater_name' => '<a href="' . base_url() . 'budget/add_exchange_voucher/' . $lc->id . '">' . $lc->donater_name . '</a>',
                'pay_type' => ($lc->pay_type == 'cash') ? 'نقدي' : 'شيك',
				'الحالة' => $class,
                'actions' => $actions,
            );
        }

        $ex['data'] = $arr;
        echo json_encode($ex);
    }

//-----------------------------------------------------------------------
    /*
     * 	List of All AL HAYA Local Transactions
     */
    public function transactions($bankid = NULL)
	{
        $this->_data['bankid'] = $bankid;
		
        $this->load->view('transactions', $this->_data);
    }
//-----------------------------------------------------------------------
    /*
     * 	List of All AL HAYA Local Transactions
     */
    public function bank_transactions($bankid = NULL)
	{
        $this->_data['bankid'] = $bankid;
		
        $this->load->view('single-bank-transactions', $this->_data);
    }

/*********************************************************** */

    function get_child_categories() {
        $id = $_REQUEST['parent_id'];
        $type = $_REQUEST['type'];

        if ($type == 'parent') {
            $parent = "bds_categoryId='0' AND bd_id = " . $id . "";
        } else {
            $parent = "bds_categoryId='" . $id . "'";
        }

        $query = $this->db->query("
		SELECT 
		bds_id,bd_id,bds_title,bds_totalAmount 
		FROM 
		`ah_budget_sub` 
		WHERE delete_record='0'
		
		AND " . $parent . ";");

        if ($query->num_rows() > 0) {
            $html = '<select name="sub_category" class="parent form-control">';
            $html .= '<option value="" selected="selected">اختر الميزانية</option>';

            foreach ($query->result() as $row) {
                $html .= '<option value="' . $row->bds_id . '" data-id="child">' . $row->bds_title . '</option>';
            }

            $html .= '</select>';
            echo $html;
        } else {
            $query = $this->db->query("
			SELECT 
			bds_totalAmount 
			FROM 
			`ah_budget_sub` 
			WHERE delete_record='0'
			
			AND bds_id=" . $id . ";");

            $bank_ids = $this->company->get_package_accounts($id, $type);


            if ($bank_ids) {
                $package_amount = $this->haya_model->get_total_package_amount($id);

                $bank_accounts = $this->company->get_banks_account($bank_ids);

                $html = '<select name="bank_accounts" class="bank-parent form-control" onchange="get_detail();">';
                $html .= '<option value="" selected="selected"> اختار رقم الحساب البنكي</option>';

                foreach ($bank_accounts as $acc) {
                    $total_alhaya_local_amount = $this->haya_model->get_total_alhaya_local_amount($acc->id);

                    $html .= '<option style="color:green !important;" value="' . $acc->id . '" account-balance="' . $acc->ammount . '" package-balance="' . $package_amount . '">' . $acc->bank_name . ' ( ' . $acc->account_no . ' ) المبلغ في الحساب ( ' . $total_alhaya_local_amount . ' ) المزانية الحالية (' . $package_amount . ' )</option>';
                }

                $html .= '</select>';
                echo $html;
            } else {
                echo '<label style="padding:7px;float:right; font-size:12px;">لا يوجد بنك مظاف في هذا القسم</label>';
            }
        }
    }
//----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function confirm_proceed()
	{
			$alhaya_bank['login_user_id']		=		$this->_login_userid;
			$alhaya_bank['transaction_type']	=		'DEBIT';
			$alhaya_bank['unique_id']			=		$this->input->post('unique_id');
			$alhaya_bank['user_name']			=		$this->input->post('user_name');
			$alhaya_bank['program_name']		=		$this->input->post('program_name');
			$alhaya_bank['amount']				=		$this->input->post('amount');
			$alhaya_bank['budget_id']			=		$this->input->post('budget_id');
			$alhaya_bank['budget_cat_id']		=		$this->input->post('budget_cat_id');
			$alhaya_bank['bank_account']		=		$this->input->post('bank_account');
			$alhaya_bank['terms_list']			=		$this->input->post('terms_list');
			
			// Save Al-Haya Bank Managment transaction detail into database
			$this->budget->save_local_transaction_data($alhaya_bank);
			
			// 
			$this->budget->amount_proceed($this->input->post('id'),array('amount_proceed'	=>	1));
			
			redirect(base_url().'budget/list_exchange_voucher');
			exit();
	}
//----------------------------------------------------------------------	
}
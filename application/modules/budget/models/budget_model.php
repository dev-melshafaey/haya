<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Budget_model extends CI_Model {
    /*
     *  Properties
     */

    private $_table_users;
    private $_table_branchs;
    private $_table_listmanagement;

//-------------------------------------------------------------------

    /*
     *  Constructor
     */
    function __construct() {
        parent::__construct();

        //Get Table Names from Config 
        $this->_table_users = $this->config->item('table_users');
        $this->_login_userid = $this->session->userdata('userid');
    }

    function get_all_listmanagement($list_type, $list_parent_id = 0, $list_id = 0) {
        $this->db->select('*');
        $this->db->from('ah_listmanagement');
        $this->db->where('list_type', $list_type);
        if ($list_parent_id > 0) {
            $this->db->where('list_parent_id', $list_parent_id);
        }
        if ($list_id > 0) {
            $this->db->where('list_id', $list_id);
        }
        $this->db->where("delete_record", '0');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function update_budgets($data, $table, $feild) {
        $serviceid = $data[$feild];
        if ($table == 'ah_budget_main' || $table == 'ah_budget_main_in') {
            $bd_data = $this->db->get_where($table, array('bd_id' => $data['bd_id']))->row();
            $diff = $bd_data->bd_totalamount - $bd_data->bd_remainingAmount;
            if ($data['bd_totalamount'] >= $diff) {
                $this->db->where($feild, $serviceid);
                $s_data = array(
                    'bd_year' => $data['bd_year'],
                    'bd_totalamount' => $data['bd_totalamount'],
                    'bd_date' => $data['bd_date'],
                    'userid' => $data['userid'],
                    'bd_remainingAmount' => $data['bd_totalamount'] - $diff
                );

                $this->db->update($table, json_encode($s_data), $this->_login_userid, $s_data);
                return $serviceid;
            } else {
                return 0;
            }
        } else {
            $bd_data = $this->db->get_where($table, array('bds_id' => $data['bds_id']))->row();
            $diff = $bd_data->bds_totalAmount - $bd_data->bds_remainingAmount;
            if ($data['bds_totalAmount'] >= $diff) {
                $this->db->where($feild, $serviceid);
                $s_data = array(
                    'bds_title' => $data['bds_title'],
                    'bds_totalAmount' => $data['bds_totalAmount'],
                    'bds_date' => $data['bds_date'],
                    'userid' => $data['userid'],
                    'bds_bankname_id' => $data['bds_bankname_id'],
                    'bds_remainingAmount' => $data['bds_remainingAmount'] - $diff
                );

                $this->db->update($table, json_encode($s_data), $this->_login_userid, $s_data);
                if ($bd_data->bds_categoryId == 0) {
                    $this->db->where('bd_id', $data['bd_id']);
                    $this->db->set('bd_remainingAmount', 'bd_remainingAmount+' . ($bd_data->bds_totalAmount - $data['bds_totalAmount']), FALSE);
                    if ($table === 'ah_budget_sub_in') {
                        $this->db->update('ah_budget_main_in');
                    } else {
                        $this->db->update('ah_budget_main');
                    }
                } else {
                    $this->db->where('bds_id', $bd_data->bds_categoryId);
                    $this->db->set('bds_remainingAmount', 'bds_remainingAmount+' . ($bd_data->bds_totalAmount - $data['bds_totalAmount']), FALSE);
                    $this->db->update($table);
                }
                return $serviceid;
            } else {
                return 0;
            }
        }
    }

    function savestore($data, $table, $feild) {

        $serviceid = $data[$feild];

        if ($serviceid > 0) {
            if ($table == 'ah_budget_main' || $table == 'ah_budget_sub' || $table == 'ah_budget_sub_in' || $table == 'ah_budget_main_in') {
                return $this->update_budgets($data, $table, $feild);
            } else {
                $this->db->where($feild, $serviceid);
                $this->db->update($table, json_encode($data), $this->_login_userid, $data);
                return $serviceid;
            }
        } else {
            $this->db->insert($table, $data, json_encode($data), $this->_login_userid);

            return $this->db->insert_id();
        }
        //echo $this->db->last_query();
    }

    function delete($serviceid, $field, $table) {
        $json_data = json_encode(array('record' => 'delete', $field => $serviceid));
        $data = array('delete_record' => '1');

        $this->db->where($field, $serviceid);
        $this->db->update($table, $json_data, $this->session->userdata('userid'), $data);
        return TRUE;
    }

    function delete_budget_by_department($bds_id, $list_type = null) {
        if ($list_type == 'in') {
            $table = 'ah_budget_sub_in';
            $main = 'ah_budget_main_in';
        } else {
            $table = 'ah_budget_sub';
            $main = 'ah_budget_main';
        }
        $bds_data = $this->db->get_where($table, array('bds_id' => $bds_id))->row();
        $parent_id = $bds_data->bds_categoryId;
        if ($parent_id != 0) {
            $this->db->where('bds_id', $parent_id);
            $this->db->set('bds_remainingAmount', 'bds_remainingAmount+' . $bds_data->bds_totalAmount, FALSE);
            $this->db->update($table);
        } else {
            $main_id = $bds_data->bd_id;
            $this->db->where('bd_id', $main_id);
            $this->db->set('bd_remainingAmount', 'bd_remainingAmount+' . $bds_data->bds_totalAmount, FALSE);
            $this->db->update($main);
        }

        $this->delete($bds_id, 'bds_id', $table);
    }

    function getdata($serviceid, $table, $column) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($column, $serviceid);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    function get_budgets($bd_id, $bds_categoryId, $list_type) {

        if ($list_type == 'in') {
            $this->db->join('ah_budget_main', 'ah_budget_main.bd_id = ah_budget_sub_in.bd_id');
            $data = $this->db->get_where('ah_budget_sub_in', array('ah_budget_sub_in.bd_id' => $bd_id, 'bds_categoryId' => $bds_categoryId));
        } else {
            $this->db->join('ah_budget_main', 'ah_budget_main.bd_id = ah_budget_sub.bd_id');
            $data = $this->db->get_where('ah_budget_sub', array('ah_budget_sub.bd_id' => $bd_id, 'bds_categoryId' => $bds_categoryId));
        }
        return $data->result();
    }

    function get_remaning($bd_id, $bds_categoryId) {
        if ($bds_categoryId > 0) {
            $query = "SELECT `bds_totalAmount` FROM `ah_budget_sub` WHERE bd_id=$bd_id AND `bds_categoryId` = 0
UNION SELECT SUM(`bds_totalAmount`) FROM `ah_budget_sub` WHERE bd_id=$bd_id AND `bds_categoryId` = $bds_categoryId";
            $data = $this->db->query($query);
            return $data->result()[0]->bds_totalAmount - $data->result()[1]->bds_totalAmount;
        }
    }

    function update_remaining($bds_categoryId, $amount1, $amount2, $bd_id, $table) {
        if ($bds_categoryId > 0) {
            $amount = $amount1 + $amount2;
            $query = "UPDATE $table SET `bds_remainingAmount` = `bds_remainingAmount` - $amount WHERE bds_id = $bds_categoryId";
        } else {
            $query = "UPDATE `ah_budget_main` SET bd_remainingAmount = bd_remainingAmount - $amount1 WHERE bd_id = $bd_id";
            $this->db->query($query);
            $query = "UPDATE `ah_budget_main_in` SET bd_remainingAmount = bd_remainingAmount - $amount2 WHERE bd_id = $bd_id";
        }

        $this->db->query($query);
    }

    function getdmessagedata($serviceid, $table, $field) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($field, $serviceid);
        $this->db->where("delete_record", "0");
        //$this->db->order_by('servcemsgid','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    function savemessage($data, $table) {
        $this->db->insert($table, $data, json_encode($data), $this->_login_userid);
    }

    function update_bd($bd_id, $bds_categoryId, $oldm) {
        $data = $this->input->post();
        $buget = str_replace(",", "", $data["bds_totalAmount"]);
        if ($bd_id > 0 && $bds_categoryId == 0) {
            $query = "UPDATE `ah_budget_sub` SET `bds_totalAmount` =  $buget WHERE bds_id = $bds_categoryId";
            echo $query;
            $this->db->query($query);
            $query = "UPDATE `ah_budget_main` SET bd_totalAmount =  bd_totalAmount-($buget-$oldm) WHERE bd_id = $bd_id";
            $this->db->query($query);
        } else {
            $query = "UPDATE `ah_budget_main` SET bd_totalAmount =  $buget WHERE bd_id = $bd_id";
        }
        echo $query;
        $this->db->query($query);
    }

    public function get_all_items($bd_id, $bds_categoryId) {
        //$query_string = NULL;

        $query = $this->db->query("SELECT * FROM ah_budget_sub WHERE bd_id='" . $bd_id . "' AND  bds_categoryId='" . $bds_categoryId . "' and delete_record = 0");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $value) {
                if ($value && $value->bds_bankname_id) {
                    $d = explode(',', $value->bds_bankname_id);
                    $a = '';
                    foreach ($d as $v) {
                        $q = "SELECT * FROM bank_managment_accounting WHERE `id`= $v";
                        $dd = $this->db->query($q);
                        $a .= $dd->row()->bank_name . ' - ' . $dd->row()->account_no . '<br>';
                    }
                    $value->bds_bankname_id = $a;
                }
            }
            return $query->result();
        }
    }

    function get_banks($bank_id) {
        if ($bank_id) {
            return $this->db->get_where('bank_managment_accounting', array('id' => $bank_id))->row();
        } else {
            return $this->db->get('bank_managment_accounting')->result();
        }
    }

//-------------------------------------------------------------------
    /*
     * Get All AL-HAYA Local Transactions
     * return OBJECT
     */

    function get_all_transactions($bankid) {
        if ($bankid) {
            $query_string = "WHERE BM.id='" . $bankid . "'";
        } else {
            $query_string = "";
        }

        $budget = $this->db->query("
		SELECT 
		LT.transaction_id,
		LT.unique_id,
		LT.terms_list,
		LT.login_user_id,
		LT.charity_type_id,
		LT.account_number,
		LT.user_name,
		LT.program_name,
		LT.user_address,
		LT.user_mobile_number,
		LT.transaction_type,
		LT.amount,
		LT.budget_id,
		LT.budget_cat_id,
		LT.bank_account,
		LT.submit_date,
		BM.id,
		BM.bank_name,
		BM.branch,
		BM.account_no
		FROM
			`alhaya_local_transactions` AS LT
			INNER JOIN `bank_managment_accounting` AS BM
				ON (LT.`bank_account` = BM.`id`) " . $query_string . ";
");

        if ($budget->num_rows() > 0) {
            return $budget->result();
        }
    }

//-------------------------------------------------------------------
    /*
     * Get All AL-HAYA Local Transactions
     * return OBJECT
     */

    function get_package_detail($budget_id, $budget_cat_id) {
        $budget = $this->db->query("SELECT P.`bd_id`,P.`bd_year`,C.`bds_title` FROM `ah_budget_main` AS P,`ah_budget_sub` AS C WHERE P.`bd_id`='" . $budget_id . "' AND C.`bds_id`='" . $budget_cat_id . "'");

        if ($budget->num_rows() > 0)
		{
            return $budget->row();
        }
    }

//-------------------------------------------------------------------
    /*
     * Get Total SUM of Debit from Local Bank Management
     * return OBJECT
     */

    function get_debit_from_local($bankid)
	{
        $budget = $this->db->query("SELECT SUM(amount) AS DEBIT FROM `alhaya_local_transactions` WHERE bank_account='" . $bankid . "' AND transaction_type='DEBIT'");

        if ($budget->num_rows() > 0)
		{
            return $budget->row()->DEBIT;
        }
    }
//-------------------------------------------------------------------
	/*
	* Insert Al Haya Local Transaction Detail
	* @param $data ARRAY
	* return LAST inserted ID
	*/

	function save_local_transaction_data($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert('alhaya_local_transactions',$data,$json_data,$this->session->userdata('userid'));
		
		return $this->db->insert_id();
    }
//-------------------------------------------------------------------
	/*
	* 
	* @param $data ARRAY
	* @param $id ARRAY
	* return TRUE
	*/

	function amount_proceed($id,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('id',$id);
		$this->db->update('exchange_voucher',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
}
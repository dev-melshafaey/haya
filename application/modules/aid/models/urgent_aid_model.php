<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Urgent_aid_model extends CI_Model
{
	/*
	*  Properties
	*/
	private $_table_users;
	private $_table_branchs;
	private $_table_listmanagement;
 	
//-------------------------------------------------------------------

	/*
	*  Constructor
	*/
	function __construct()
	{
		parent::__construct();

		//Get Table Names from Config 
		$this->_table_users 				= 	$this->config->item('table_users');
		$this->_login_userid			=	$this->session->userdata('userid');
	}
	function get_all_listmanagement($list_type,$list_parent_id=0,$list_id=0){
		$this->db->select('*');
		$this->db->from('ah_listmanagement');						
		$this->db->where('list_type',$list_type);
		if($list_parent_id >0){$this->db->where('list_parent_id',$list_parent_id);}
		if($list_id>0){$this->db->where('list_id',$list_id);}
		$this->db->where("delete_record",'0');
		$query = $this->db->get(); 
		
		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	function savestore($data,$table,$feild)
	{
		$serviceid = $data[$feild];
		if($serviceid	!=	'' or $serviceid	> 0 )
		{
			$this->db->where($feild,$serviceid);
			$this->db->update($table,json_encode($data),$this->_login_userid,$data);
			return $serviceid;
		}
		else
		{
			$this->db->insert($table,$data,json_encode($data),$this->_login_userid);
			return $this->db->insert_id();;
		}
	}
	function delete($serviceid,$field,$table)
	{
			$json_data	=	json_encode(array('record'	=>	'delete',$field	=>	$serviceid));
		$data		=	array('delete_record'=>'1');
		
		$this->db->where($field,$serviceid);

		$this->db->update($table,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
			
	}
	
	function getdata($serviceid,$table,$column){
		$this->db->select('*');
		$this->db->from($table);						
		$this->db->where($column,$serviceid);
		$query = $this->db->get(); 
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	function getdmessagedata($serviceid,$table,$field){
		$this->db->select('*');
		$this->db->from($table);						
		$this->db->where($field,$serviceid);
		$this->db->where("delete_record","0");
		//$this->db->order_by('servcemsgid','DESC');
		$query = $this->db->get(); 
		return $query->result();
	}
	function savemessage($data,$table)
	{
		$this->db->insert($table,$data,json_encode($data),$this->_login_userid);
	}
	 public function get_all_items()
	 {
		 $query_string	=	NULL;
		 
		$query	=	$this->db->query("SELECT
			`ah_inventory`.`itemid`
			, `ah_inventory`.`store_id`
			, `ah_inventory`.`addedby`
			, `ah_inventory`.`list_category`
			, `ah_inventory`.`list_subcategory`
			, `ah_inventory`.`itemname`
			, `ah_inventory`.`itemdescription`
			, `ah_inventory`.`itemphoto`
			, `ah_inventory`.`itemstatus`
			FROM
			`ah_inventory`;");
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	 }
//-----------------------------------------------------------------------

	/*
	*
	* Get Total Records
	*/
	public function get_total_records()
	{
		$this->db->select('COUNT(extreliefId) AS TOTAL');
		$this->db->from('ah_external_relief');
		
		$query	=	$this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row()->TOTAL;
		}
	}
//-----------------------------------------------------------------------

	/*
	*
	* Get Total Records
	*/
	public function get_total_records_aids()
	{
		$this->db->select('COUNT(urgentaidId) AS TOTAL');
		$this->db->from('ah_urgent_aid');
		
		$query	=	$this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row()->TOTAL;
		}
	}
//-----------------------------------------------------------------------

	/*
	*
	* Get All Detail about plan
	*/
	public function get_aid_plan_detail($extreliefId)
	{
		$this->db->select('planid,extreliefId,userid,budget,transfer_money,plan_attachment,instructions,plan,give_suggestion');
		$this->db->where('extreliefId',$extreliefId);
		$this->db->from('aid_plan');
		
		$query	=	$this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-----------------------------------------------------------------------

	/*
	*
	* Get All Detail about plan
	*/
	public function get_internal_aid_plan_detail($extreliefId)
	{
		$this->db->select('planid,extreliefId,userid,budget,transfer_money,plan_attachment,instructions,plan,give_suggestion');
		$this->db->where('extreliefId',$extreliefId);
		$this->db->from('internal_aid_plan');
		
		$query	=	$this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-----------------------------------------------------------------------

	/*
	*
	* Add/Update Plan Detail
	*/
	function save_aid_pan_detail($planid,$data)
	{
		if($planid)
		{
			$this->db->where('planid',$planid);
			$this->db->update('aid_plan',json_encode($data),$this->_login_userid,$data);
			return TRUE;
		}
		else
		{
			$this->db->insert('aid_plan',$data,json_encode($data),$this->_login_userid);
			return TRUE;
		}
	}
//-----------------------------------------------------------------------

	/*
	*
	* Add/Update Internal Plan Detail
	*/
	function save_internal_aid_plan($planid,$data)
	{
		if($planid)
		{
			$this->db->where('planid',$planid);
			$this->db->update('internal_aid_plan',json_encode($data),$this->_login_userid,$data);
			return TRUE;
		}
		else
		{
			$this->db->insert('internal_aid_plan',$data,json_encode($data),$this->_login_userid);
			return TRUE;
		}
	}
//--------------------------------------------------------------
	/*
	* UPDATE the Record
	* @param $id integer
	* @param $field string
	* @param $data array
	* @param $table string
	* return TRUE
	*/
	function update_external_record($id,$field,$table,$data)
	{	
		$this->db->where($field,$id);

		$this->db->update($table,json_encode($data),$this->_login_userid,$data);
		
		return TRUE;	
	}
//--------------------------------------------------------------
	/*
	* GET detail
	* @param $id integer
	* @param $field string
	* @param $data array
	* @param $table string
	* return TRUE
	*/
	function get_detail($id,$field,$table)
	{
		$this->db->select('*');
		$this->db->from($table);						
		$this->db->where($field,$id);
		$this->db->where("delete_record","0");

		$query = $this->db->get();
		 
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//--------------------------------------------------------------
}
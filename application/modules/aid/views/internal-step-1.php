<div class="row col-md-12">
  <form action="" method="POST" id="step-1-form" name="committee_meeting" autocomplete="off" enctype="multipart/form-data">
    <div class="form-group col-md-4">
      <label for="basic-input">رقم محضر الاجتماع</label>
      <input type="text" class="form-control " placeholder="رقم محضر الاجتماع" name="minutes" id="minutes" value="<?php echo $all_details->minutes;?>"/>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">التاريخ</label>
      <input type="text" class="form-control  datepicker" placeholder="التاريخ" name="currentdate" id="step-1-currentdate" value="<?php echo $all_details->currentdate;?>"/>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">المرفقات</label>
      <input type="file" class="form-control" name="aid_attachemnt" id="aid_attachemnt" placeholder="المرفقات :" />
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">قرار اللجنة</label>
      <textarea class="form-control"  placeholder="قرار اللجنة" name="decision" id="decision" rows="5" ><?php echo $all_details->decision;?></textarea>
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">الملاحظات</label>
      <textarea class="form-control" placeholder="الملاحظات" name="notes" id="notes" rows="5" ><?php echo $all_details->notes;?></textarea>
    </div>
    <input type="hidden" name="internalMeetingId" id="internalMeetingId" value="<?php echo $all_details->internalMeetingId;?>" />

  <div class="form-group col-md-6">
  <button type="button" id="x_step_1" class="btn btn-sm btn-success">تحديث</button>
  </div>
  </form>
  
</div>
<script>
$(function(){
	$(".datepicker").datepicker({
	changeMonth: true,
	changeYear: true,
	yearRange: "-80:+0",
	dateFormat:'yy-mm-dd',
	});
/***************************************************************/	
	$( "#x_step_1" ).click(function() {
		var fd = new FormData(document.getElementById("step-1-form"));
		
		var exe_id	=	$("#urgentaidId").val();
		
		$.ajax({
			url: config.BASE_URL+'aid/internal_step_1',
			type: "POST",
			data:fd,
			enctype: 'multipart/form-data',
		  	dataType: "html",
		  	processData: false,  // tell jQuery not to process the data
          	contentType: false ,  // tell jQuery not to set contentType
			success: function(response)
			{
				$(location).attr('href', config.BASE_URL+'aid/addUrgentAid/'+exe_id);
      		}
    	});
	});
/***************************************************************/
});
</script>
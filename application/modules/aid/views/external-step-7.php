<div class="row col-md-12">
  <form action="" method="POST" id="step-7-form" name="committee_meeting" autocomplete="off" enctype="multipart/form-data">
      <div class="form-group col-md-4">
      <label for="basic-input">رقم محضر الاجتماع</label>
            <input type="text" class="form-control" value="<?php echo $all_details->minutes;?>" placeholder="رقم محضر الاجتماع" name="minutes" id="minutes" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">التاريخ</label>
            <input type="text" class="form-control datepicker" value="<?php echo $all_details->currentdate;?>" placeholder="التاريخ" name="currentdate" id="step-7-currentdate"  />
    </div>

    <div class="form-group col-md-4">
      <label for="basic-input">المرفقات</label>
      <input type="file" class="form-control " name="final_releif_attachemnt" id="final_releif_attachemnt" placeholder="المرفقات :" />
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">قرار اللجنة</label>
      <textarea class="form-control" value="" placeholder="قرار اللجنة" name="decision" id="decision" rows="5" ><?php echo $all_details->decision;?></textarea>
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">الملاحظات</label>
      <textarea class="form-control" value="" placeholder="الملاحظات" name="notes" id="notes" rows="5" ><?php echo $all_details->notes;?></textarea>
    </div>
    <br clear="all"/>
    <div class="form-group col-md-6">
    <input type="hidden" name="extrelMeetingId" id="extrelMeetingId" value="<?php echo $all_details->extrelMeetingId;?>" />
    <button type="button" id="x_step_7" class="btn btn-sm btn-success">تحديث</button>
    </div>
  </form>
</div>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
/***************************************************************/	
	$( "#x_step_7" ).click(function() {
		var fd = new FormData(document.getElementById("step-7-form"));
		
		var exe_id	=	$("#extreliefId").val();
		
		$.ajax({
			url: config.BASE_URL+'aid/external_step_7',
			type: "POST",
			data:fd,
			enctype: 'multipart/form-data',
		  	dataType: "html",
		  	processData: false,  // tell jQuery not to process the data
          	contentType: false ,  // tell jQuery not to set contentType
			success: function(response)
			{
				$(location).attr('href', config.BASE_URL+'aid/addexternal_relief/'+exe_id);
      		}
    	});
	});
/***************************************************************/
});
</script>
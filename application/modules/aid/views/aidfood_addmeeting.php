  <form action="" method="POST" id="frm_meeting" name="frm_meeting" action="<?php echo base_url()?>aid/addaidfood_meeting/<?php echo $aidfoodId;?>/<?php echo $section;?>/<?php echo $aidfoodMeetingId;?>" enctype="multipart/form-data" onsubmit="return uploadFiles();" >
<div class="row col-md-12">
    
     <div class="form-group col-md-6">
      <label for="basic-input">رقم الاجتماع</label>
      <input type="text" class="form-control" value="<?php if($rows->aidfoodMeetingId >0){echo $rows->aidfoodMeetingId;}else{echo $count;} ?>" placeholder="رقم الاجتماع" name="autoid" id="autoid" readonly="readonly" />
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">التاريخ</label>
      <input type="text" class="form-control req datepicker" value="<?php echo $controller->dateformat($rows->currentdate);?>" placeholder="التاريخ" name="currentdate" id="datepicker" />
    </div>
    
    <div class="form-group col-md-6">
      <label for="basic-input">‎اسم الشركة</label>
     <select name="companyName" id="companyName" class="form-control">
        <?php 
		  if(count($company)>0){
			  foreach($company as $type){
				  if($rows->companyName == $type->companyid){
					  echo '<option value="'.$type->companyid.'" selected >'.$type->arabic_name.'</option>';
				  }
				  else{
					  echo '<option value="'.$type->companyid.'" >'.$type->arabic_name.'</option>';
				  }
				  
			  }
		  }
		  ?>
      </select>
     
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">ارفاق المسح الضوئي</label>
      <input type="file" class="form-control " value="<?php echo $rows->attachment;?>" placeholder="ارفاق المسح الضوئي" name="attachment[]" id="attachment"  multiple="multiple"/>
       <input type="hidden"  name="attachment_old" id="attachment_old" value="<?php echo $rows->attachment;?>"  />
   
    </div>
      <div class="form-group col-md-12">
      <label for="basic-input">قرار اللجنة</label>
      <textarea   class="form-control req"  placeholder="قرار اللجنة" name="decision" id="decision"><?php echo $rows->decision;?></textarea>
    </div>
     <div class="form-group col-md-12">
      <label for="basic-input">الملاحظات</label>
      <textarea   class="form-control req"  placeholder="الملاحظات" name="notes" id="notes"><?php echo $rows->notes;?></textarea>
    </div>
    <input type="hidden" name="aidfoodId" id="aidfoodId_popup" value="<?php echo $aidfoodId;?>"/>
    <input type="hidden" name="aidfoodMeetingId" id="aidfoodMeetingId" value="<?php echo $aidfoodMeetingId;?>"/>
     <input type="hidden" name="section" id="section" value="<?php echo $section;?>"/>

</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="submit" class="btn btn-success btn-lrg" name="submit"  id="save_meeting" value="حفظ"  />
  </div>
</div>
  </form>
<script>
function uploadFiles()
{
	check_my_session();
        $('#frm_meeting .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_meeting .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_meeting .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {
	var form_data = new FormData(this); 
 $.ajax({ //ajax form submit
            url :  config.BASE_URL+'aid/addaidfood_meeting/<?php echo $aidfoodId;?>',
            type: 'POST',
            data : form_data,
            dataType : "html",
            contentType: false,
            cache: false,
            processData:false,
			beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(msg)
		  {		
			  $('#ajax_action').hide();
			 $('#addingDiag').modal('hide');	  
			 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
			 listallaidfood_meeting('<?php echo $aidfoodId ?>','<?php echo $section ?>');
			 return false;
		  }
        });
		}
		 else 
		{
            show_notification_error(ht);
        }
		return false;
		
}
 $(document).ready(function(){

$('form').on('submit', uploadFiles);
});


$(function(){
	$("#datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});
</script>
  <form action="" method="POST" id="frm_expence" name="frm_expence" action="<?php echo base_url()?>aid/addaidfood_unloading/<?php echo $aidfoodId;?>/<?php echo  $aidfoodUnId;?>"  enctype="multipart/form-data" onsubmit="return uploadFiles();">
<div class="row col-md-12">
    <div class="form-group col-md-4">
      <label for="basic-input">‎اسم الشركة</label>
     <select name="companyName" id="companyName" class="form-control">
        <?php 
		  if(count($company)>0){
			  foreach($company as $type){
				  if($rows->companyName == $type->companyid){
					  echo '<option value="'.$type->companyid.'" selected >'.$type->arabic_name.'</option>';
				  }
				  else{
					  echo '<option value="'.$type->companyid.'" >'.$type->arabic_name.'</option>';
				  }
				  
			  }
		  }
		  ?>
      </select>
     
    </div>
   
    <div class="form-group col-md-4">
      <label for="basic-input">المبلغ</label>
      <input type="number" class="form-control req" value="<?php echo $rows->amount;?>" placeholder="المبلغ" name="amount" id="amount" />
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">عدد الطرود</label>
      <input type="number" class="form-control req" value="<?php echo $rows->noOfPackages;?>" placeholder="عدد الطرود" name="noOfPackages" id="noOfPackages" />
    </div>
    
     <div class="form-group col-md-8">
      <label for="basic-input">الملاحظات</label>
      <textarea   class="form-control req"  placeholder="الملاحظات" name="notes" id="notes"><?php echo $rows->notes;?></textarea>
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">ارفاق عروض الأسع</label>
      <input type="file" class="form-control" name="attachment[]" id="attachment" multiple="multiple" />
      <input type="hidden"  name="attachment_old" id="attachment_old" value="<?php echo $rows->attachment;?>"  /><br/>
      <?php
	  if($rows->attachment !=""){
		$attachment = @explode(',',$rows->attachment);
	   foreach($attachment as $att){
		   if($att !=""){
			  echo $controller->getfileicon($att,base_url().'resources/aid/'.$login_userid);
			  
		   }
	   }
	}
	  ?>
    </div>
    
    <input type="hidden" name="aidfoodId" id="aidfoodId" value="<?php echo $aidfoodId;?>"/>
    <input type="hidden" name="aidfoodUnId" id="aidfoodUnId" value="<?php echo $rows->aidfoodUnId;?>"/>

</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="submit" class="btn btn-success btn-lrg" name="submit"  id="save_expence" value="حفظ" />
  </div>
</div>
  </form>
<script>
 $(document).ready(function(){

$('form').on('submit', uploadFiles);
});
 
function uploadFiles()
{		check_my_session();
        $('#frm_expence .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_expence .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_expence .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
             /*   var str_data = 	$('#frm_expence').serialize();
		
			var request = $.ajax({
			  url: config.BASE_URL+'aid/addaidfood_unloading/',
			  type: "POST",
			  data: str_data,
			  dataType: "html",
			  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
			  success: function(msg)
			  {		
			  
				console.log(msg);
				
				 $('#addingDiag').modal('hide');	  
				 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
				 loadaidfood_unloading('<?php echo $aidfoodId ?>');
			  }
			});*/
			var form_data = new FormData(this); 
 $.ajax({ //ajax form submit
            url :  config.BASE_URL+'aid/addaidfood_unloading',
            type: 'POST',
            data : form_data,
            dataType : "html",
            contentType: false,
            cache: false,
            processData:false,
			beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(msg)
		  {	
		  	 $('#ajax_action').hide();	
			 $('#addingDiag').modal('hide');	  
			 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
			 loadaidfood_unloading('<?php echo $aidfoodId ?>','<?php echo $section ?>');
			 return false;
		  }
        });
            
        }
        else 
		{
            show_notification(ht);
        }
		return false;
	}


</script>
<div class="row col-md-12">
  <form action="" method="POST" id="step-4-form" name="committee_meeting" autocomplete="off" enctype="multipart/form-data">
    <div class="form-group col-md-4">
      <label for="basic-input">النثرية</label>
      <input type="number" class="form-control" placeholder="النثرية" name="prose" id="prose" value="<?php echo $all_details->prose;?>"/>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">المواد الغذائية</label>
      <input type="number" class="form-control" placeholder="المواد الغذائية" name="food" id="food" value="<?php echo $all_details->food;?>"/>
    </div>
        <div class="form-group col-md-4">
      <label for="basic-input">المواد الاغاثية</label>
      <input type="text" class="form-control" placeholder="المواد الاغاثية" name="reliefArticles" id="reliefArticles" value="<?php echo $all_details->reliefArticles;?>"/>
    </div>
            <div class="form-group col-md-4">
      <label for="basic-input">المواد الطبية</label>
      <input type="text" class="form-control" placeholder="المواد الطبية" name="medicalMaterials" id="medicalMaterials" value="<?php echo $all_details->medicalMaterials;?>"/>
    </div>
            <div class="form-group col-md-4">
      <label for="basic-input">المواد الاغاثية</label>
      <input type="text" class="form-control" placeholder="المواد الاغاثية" name="reliefArticles" id="reliefArticles" value="<?php echo $all_details->reliefArticles;?>"/>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">المرفقات</label>
      <input type="file" class="form-control" name="attachment1" id="attachment1" placeholder="المرفقات :" />
    </div>
        <div class="form-group col-md-6">
      <label for="basic-input">الملاحظات</label>
      <textarea name="notes" id="notes_1" class="form-control " rows="5" ><?php echo $all_details->notes;?></textarea>
    </div>
<br clear="all" />
    <div class="form-group col-md-6">
       <input type="hidden" name="extExpencesId" id="extExpencesId" value="<?php echo $all_details->extExpencesId;?>" />
      <button type="button" id="x_step_4" class="btn btn-sm btn-success">تحديث</button>
    </div>
  </form>
</div>
<script>
$(function(){
	$(".datepicker").datepicker({
	changeMonth: true,
	changeYear: true,
	yearRange: "-80:+0",
	dateFormat:'yy-mm-dd',
	});
/***************************************************************/	
	$( "#x_step_4" ).click(function() {
		var fd = new FormData(document.getElementById("step-4-form"));
		
		var exe_id	=	$("#extreliefId").val();
		
		$.ajax({
			url: config.BASE_URL+'aid/external_step_4',
			type: "POST",
			data:fd,
			enctype: 'multipart/form-data',
		  	dataType: "html",
		  	processData: false,  // tell jQuery not to process the data
          	contentType: false ,  // tell jQuery not to set contentType
			success: function(response)
			{
				$(location).attr('href', config.BASE_URL+'aid/addexternal_relief/'+exe_id);
      		}
    	});
	});
/***************************************************************/
});
</script>
<div class="row col-md-12">
  <form action="" method="POST" id="step-3-form" name="committee_meeting" autocomplete="off" enctype="multipart/form-data">
    <div class="form-group col-md-4">
      <label for="basic-input">نوع المواد</label>
            <?php echo $this->haya_model->create_dropbox_list('typeOfMaterial','type_of_material',$all_details->typeOfMaterial,0,''); ?>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">المواد</label>
      <input type="text" class="form-control" placeholder="المواد" name="materials" id="materials" value="<?php echo $all_details->materials;?>"/>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">الكمية</label>
      <input type="text" class="form-control" placeholder="الكمية" name="quantity" id="quantity" value="<?php echo $all_details->quantity;?>"/>
    </div>
        <div class="form-group col-md-4">
      <label for="basic-input">الموازنة</label>
      <input type="text" class="form-control" placeholder="الموازنة" name="budgeting" id="budgeting" value="<?php echo $all_details->budgeting;?>"/>
    </div>
    <input type="hidden" name="requirementsId" id="requirementsId" value="<?php echo $all_details->requirementsId;?>" />
    <div class="form-group col-md-6">
      <button type="button" id="x_step_3" class="btn btn-sm btn-success">تحديث</button>
    </div>
  </form>
</div>
<script>
$(function(){
	$(".datepicker").datepicker({
	changeMonth: true,
	changeYear: true,
	yearRange: "-80:+0",
	dateFormat:'yy-mm-dd',
	});
/***************************************************************/	
	$( "#x_step_3" ).click(function() {
		var fd = new FormData(document.getElementById("step-3-form"));
		
		var exe_id	=	$("#extreliefId").val();
		
		$.ajax({
			url: config.BASE_URL+'aid/external_step_3',
			type: "POST",
			data:fd,
			enctype: 'multipart/form-data',
		  	dataType: "html",
		  	processData: false,  // tell jQuery not to process the data
          	contentType: false ,  // tell jQuery not to set contentType
			success: function(response)
			{
				$(location).attr('href', config.BASE_URL+'aid/addexternal_relief/'+exe_id);
      		}
    	});
	});
/***************************************************************/
});
</script>
<div class="row col-md-12">
  <form action="" method="POST" id="frm_expence" name="frm_expence" action="<?php echo base_url()?>aid/addExpences/<?php echo $urgentaidId;?>/<?php echo  $rgentaidexpId;?>">
    <div class="form-group col-md-4">
      <label for="basic-input">‎اسم المادة</label>
      <select name="beneficiary" id="beneficiary" class="form-control">
        <?php 
		  if(count($all_items)>0){
			  foreach($all_items as $type){
				  if($rows->beneficiary == $type->itemid){
					  echo '<option value="'.$type->itemid.'" selected >'.$type->itemname.'</option>';
				  }
				  else{
					  echo '<option value="'.$type->itemid.'" >'.$type->itemname.'</option>';
				  }
				  
			  }
		  }
		  ?>
      </select>
    </div>
   
    <div class="form-group col-md-4">
      <label for="basic-input">الوزن للوحدة</label>
      <input type="text" class="form-control req" value="<?php echo $rows->weight;?>" placeholder="الوزن للوحدة" name="weight" id="weight" />
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">الكمية</label>
      <input type="text" class="form-control req" value="<?php echo $rows->qty;?>" placeholder="الكمية" name="qty" id="qty" />
    </div>
     <div class="form-group col-md-12">
      <label for="basic-input">تفاصيل المادة</label>
      <textarea   class="form-control req"  placeholder="تفاصيل المادة" name="articledetails" id="articledetails"><?php echo $rows->articledetails;?></textarea>
    </div>
    
    <input type="hidden" name="urgentaidId" id="urgentaidId_popup" value="<?php echo $urgentaidId;?>"/>
    <input type="hidden" name="urgentaidexpId" id="urgentaidexpId" value="<?php echo $rows->urgentaidexpId;?>"/>
  </form>
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="save_expence" value="حفظ" />
  </div>
</div>
<script>
 $(document).ready(function(){
	$('#save_expence').click(function () {
		check_my_session();
        $('#frm_expence .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_expence .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_expence .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                var str_data = 	$('#frm_expence').serialize();
		
			var request = $.ajax({
			  url: config.BASE_URL+'aid/addExpences/<?php echo $urgentaidId;?>',
			  type: "POST",
			  data: str_data,
			  dataType: "html",
			  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
			  success: function(msg)
			  {		
			  
				 $('#ajax_action').hide();
				 $('#addingDiag').modal('hide');	  
				 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
				 loadexpences('<?php echo $urgentaidId ?>');
			  }
			});
            
        }
        else 
		{
            show_notification_error(ht);
        }
    });
});
</script>
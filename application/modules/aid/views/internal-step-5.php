<div class="row col-md-12">
  <form action="" method="POST" id="step-5-form" name="committee_meeting" autocomplete="off" enctype="multipart/form-data">
    <div class="form-group col-md-6">
      <label for="basic-input">التاريخ</label>
            <input type="text" class="form-control datepicker" value="<?php echo $all_details->currentdate;?>" placeholder="التاريخ" name="currentdate" id="step-5-currentdate"  />
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">المرفقات</label>
      <input type="file" class="form-control " name="report_attachment" id="report_attachment" multiple placeholder="المرفقات :" />
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">البيان</label>
      <textarea class="form-control" value="" placeholder="البيان" name="statement" id="statement" rows="5" ><?php echo $all_details->statement;?></textarea>
    </div>
    <br clear="all"/>
    <div class="form-group col-md-6">
    <input type="hidden" name="extDataReportId" id="extDataReportId" value="<?php echo $all_details->extDataReportId;?>" />
    <button type="button" id="x_step_5" class="btn btn-sm btn-success">تحديث</button>
    </div>
  </form>
</div>
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
/***************************************************************/	
	$( "#x_step_5" ).click(function() {
		var fd = new FormData(document.getElementById("step-5-form"));
		
		var exe_id	=	$("#urgentaidId").val();
		
		$.ajax({
			url: config.BASE_URL+'aid/internal_step_5',
			type: "POST",
			data:fd,
			enctype: 'multipart/form-data',
		  	dataType: "html",
		  	processData: false,  // tell jQuery not to process the data
          	contentType: false ,  // tell jQuery not to set contentType
			success: function(response)
			{
				$(location).attr('href', config.BASE_URL+'aid/addUrgentAid/'+exe_id);
      		}
    	});
	});
/***************************************************************/
});
</script>
<div class="row col-md-12">
  <form action="" method="POST" id="frm_expence" name="frm_expence" action="<?php echo base_url()?>aid/addFoodexpulsion/<?php echo $aidfoodId;?>/<?php echo  $aidfoodExpId;?>">
    <div class="form-group col-md-4">
      <label for="basic-input">‎اسم المادة</label>
     <select name="subjectName" id="subjectName" class="form-control req" style="width:274px !important;"  >
        <?php 
		  if(count($all_items)>0){
			  foreach($all_items as $type){
				  if($rows->subjectName == $type->itemid){
					  echo '<option value="'.$type->itemid.'" selected >'.$type->itemname.'</option>';
				  }
				  else{
					  echo '<option value="'.$type->itemid.'" >'.$type->itemname.'</option>';
				  }
				  
			  }
		  }
		  ?>
      </select>
    </div>
   
    <div class="form-group col-md-4">
      <label for="basic-input">تفاصيل المادة</label>
      <input type="text" class="form-control req" value="<?php echo $rows->articledetails;?>" placeholder="تفاصيل المادة" name="articledetails" id="articledetails" />
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">الوزن للوحدة</label>
      <input type="number" class="form-control req" value="<?php echo $rows->weight;?>" placeholder="الوزن للوحدة" name="weight" id="weight" />
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">الكمية</label>
      <input type="number" class="form-control req" value="<?php echo $rows->qty;?>" placeholder="الكمية" name="qty" id="qty" />
    </div>
    
     <div class="form-group col-md-8">
      <label for="basic-input">الملاحظات</label>
      <textarea   class="form-control req"  placeholder="الملاحظات" name="notes" id="notes"><?php echo $rows->notes;?></textarea>
    </div>
    
    <input type="hidden" name="aidfoodId" id="aidfoodId" value="<?php echo $aidfoodId;?>"/>
    <input type="hidden" name="aidfoodExpId" id="aidfoodExpId" value="<?php echo $rows->aidfoodExpId;?>"/>
  </form>
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="save_expence" value="حفظ" />
  </div>
</div>
<script>
 $(document).ready(function(){
	$('#save_expence').click(function () {
		check_my_session();
        $('#frm_expence .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_expence .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_expence .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                var str_data = 	$('#frm_expence').serialize();
		
			var request = $.ajax({
			  url: config.BASE_URL+'aid/addFoodexpulsion/',
			  type: "POST",
			  data: str_data,
			  dataType: "html",
			  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
			  success: function(msg)
			  {		
				 $('#ajax_action').hide();	
				 $('#addingDiag').modal('hide');	  
				 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
				 loadaidfood_expulsion('<?php echo $aidfoodId ?>');
			  }
			});
            
        }
        else 
		{
            show_notification_error(ht);
        }
    });
});
$(document).ready(function() {
		$("select").searchable({
			maxListSize: 5,						// if list size are less than maxListSize, show them all
			maxMultiMatch: 5,						// how many matching entries should be displayed
			exactMatch: false,						// Exact matching on search
			wildcards: true,						// Support for wildcard characters (*, ?)
			ignoreCase: true,						// Ignore case sensitivity
			latency: 200,							// how many millis to wait until starting search
			warnMultiMatch: 'top {0} matches ...',	// string to append to a list of entries cut short by maxMultiMatch 
			warnNoMatch: 'no matches ...',			// string to show in the list when no entries match
			zIndex: 'auto'							// zIndex for elements generated by this plugin
	   	});
	});

</script>
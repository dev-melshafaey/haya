  <form action="" method="POST" id="frm_meeting" name="frm_meeting" action="<?php echo base_url()?>aid/addmeeting/<?php echo $urgentaidId;?>/<?php echo $urgentaidmeetId;?>" enctype="multipart/form-data" onsubmit="return uploadFiles();" >
<div class="row col-md-12">
    <div class="form-group col-md-6">
      <label for="basic-input">التاريخ</label>
      <input type="text" class="form-control req datepicker" value="<?php echo $rows->currentdate;?>" placeholder="التاريخ" name="currentdate" id="datepicker" />
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">رقم الاجتماع</label>
      <input type="text" class="form-control req " value="<?php echo $rows->meetingno;?>" placeholder="رقم الاجتماع" name="meetingno" id="meetingno" />
    </div>
    
     <div class="form-group col-md-12">
      <label for="basic-input">قرار اللجنة</label>
      <textarea class="form-control req"  placeholder="قرار اللجنة" name="decision" id="decision" rows="3" ><?php echo $rows->decision;?></textarea>
    </div>
    
     <div class="form-group col-md-12">
      <label for="basic-input">الملاحظات</label>
      <textarea   class="form-control req"  placeholder="الملاحظات" name="notes" id="notes" rows="3" ><?php echo $rows->notes;?></textarea>
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">ارفاق المسح الضوئي</label>
      <input type="file" class="form-control " value="<?php echo $rows->attachment;?>" placeholder="ارفاق المسح الضوئي" name="attachment[]" id="attachment"  multiple="multiple"/>
       <input type="hidden"  name="attachment_old" id="attachment_old" value="<?php echo $rows->attachment;?>"  />
   	<?php if($rows->attachment !=""){
				   $i=0;
				   $attachment = @explode(',',$rows->attachment);
				   foreach($attachment as $att){
					   if($att !=""){
						   $i++;
				   ?>
                   <div id="att_<?php echo $i;?>" style="float:right;">
              <?php echo $controller->getfileicon($att,base_url().'resources/aid/'.$login_userid);?>
             
              </div>
              <?php } } }?>
    </div>
    <input type="hidden" name="urgentaidId" id="urgentaidId_popup" value="<?php echo $urgentaidId;?>"/>
    <input type="hidden" name="urgentaidmeetId" id="urgentaidmeetId" value="<?php echo $urgentaidmeetId;?>"/>
     <input type="hidden" name="section" id="section" value="<?php echo $section;?>"/>

</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="submit" class="btn btn-success btn-lrg" name="submit"  id="save_meeting" value="حفظ"  />
  </div>
</div>
  </form>
<script>
function uploadFiles()
{
	check_my_session();
        $('#frm_meeting .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_meeting .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_meeting .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {
	var form_data = new FormData(this); 
 $.ajax({ //ajax form submit
            url :  config.BASE_URL+'aid/addmeeting/<?php echo $urgentaidId;?>',
            type: 'POST',
            data : form_data,
            dataType : "html",
            contentType: false,
            cache: false,
            processData:false,
			beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
		  success: function(msg)
		  {		
			  $('#ajax_action').hide();
			 $('#addingDiag').modal('hide');	  
			 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
			 loadmeeting('<?php echo $urgentaidId ?>','<?php echo $section ?>');
		  }
        });
		}
		 else 
		{
            show_notification_error(ht);
        }
		return false;
		
}
 $(document).ready(function(){

$('form').on('submit', uploadFiles);
});


$(function(){
	$("#datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});
</script>
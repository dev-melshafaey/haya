<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid'])); ?>
<?php $total_records	= $this->urgent_aid->get_total_records_aids()+1;?>
<?php $curent_year_list	=	date('Y').'/'.$total_records;?>
<?php //echo $rows->currentdate.'<pre>'; print_r($rows);exit();?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<style>
th{  text-align: center !important;}
</style>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
      <div class="col-md-12">
    
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <i class="more-less glyphicon  <?php if($rows->urgentaidId > 0){ ?> glyphicon-plus<?php }else{?>glyphicon-minus<?php }?>"></i>
                        تفاصيل
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse <?php if($rows->urgentaidId<=0){ ?> in<?php }?>" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                   <form method="POST" id="form_urgent" name="form_urgent" enctype="multipart/form-data">
           <input type="hidden"  name="urgentaidId" id="urgentaidId" value="<?php echo $rows->urgentaidId;?>" />
            <div class="row">
            <div class="form-group col-md-3">
              <label for="basic-input"><strong>رقم الإغاثة : </strong></label>
              <input type="text" class="form-control " readonly value="<?php echo $curent_year_list;?>" placeholder="الرقم التسلسلي" name="autoid" id="autoid" />
            </div>
            <div class="form-group col-md-3">
              <label for="basic-input"><strong>التاريخ :</strong></label>
              <input type="text" class="form-control req datepicker" value="<?php echo $rows->currentdate?>" placeholder="التاريخ" name="currentdate" id="currentdate" />
            </div>
            <div class="form-group col-md-3">
              <label for="basic-input"><strong>تصنيف الإغاثة  :</strong></label>
              <select class="form-control req" id="beneficiary" name="beneficiary" onChange="loaddata(this.value)" >
                      <?php 
					  if(count($beneficiary)>0){
						  foreach($beneficiary as $type){
							// if( array_search( $rows->beneficiary,$type->list_id))
							 if($rows->beneficiary	==	$type->list_id)
							  {
								  echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
							  }
							  else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
							  }
							  
						  }
					  }
					  ?>
                      </select>
            </div>
             <div class="form-group col-md-3">
              <label for="basic-input"><strong>  نوع الإغاثة :</strong></label>
              <select class="form-control req" id="reliefType" name="reliefType" >
                      <?php 
					  if(count($reliefType)>0){
						  foreach($reliefType as $type){
							 //if( array_search( $rows->reliefType,$type->list_id))
							 if($rows->reliefType	==	$type->list_id)
							  {
								  echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
							  }
							  else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
							  }
							  
						  }
					  }
					  ?>
                      </select>
            </div>
             
            <div class="form-group col-md-3">
              <label for="basic-input"><strong>الدفعة :</strong></label>
              <input type="text" class="form-control req " value="<?php echo $rows->beneficiaryName?>" placeholder="اسم الجهة المستفيدة" name="beneficiaryName" id="beneficiaryName" />
            </div>
                     <div class="form-group col-md-3">
                      <label for="basic-input"><strong>الدولة :</strong></label>
                      <select class="form-control " id="country" name="country" onChange="loadprovince(this.value,'province');" >
                      <?php if($rows->country):?>
                      <option value="">--اختر عمان--</option>
                      <?php endif;?>
                              <?php 
                              if(count($issuecountry)>0){
                                  foreach($issuecountry as $type){
                                     /*if( array_search($rows->country,$type->list_id))
                                      {
                                          echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
                                      }
                                      else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
                                      }*/
									  
									  if($type->list_id	==	'200')
									  {
										  echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
									  }
                                      
                                  }
                              }
                              ?>
                              </select>
                    </div>
<!--                     <div class="form-group col-md-3">
                      <label for="basic-input"><strong>حافظة / البلد :</strong></label>
                      <select class="form-control " id="province" name="province" onChange="loadprovince(this.value,'city');" >
                              </select>
                    </div>
                     <div class="form-group col-md-3">
                      <label for="basic-input"><strong> الولايه / المدينة:</strong></label>
                      <select class="form-control " id="city" name="city" >
                              </select>
                    </div>-->
                        <div class="form-group col-md-3">
            <label for="basic-input"><strong>المحافظة :</strong></label>
            <select class="form-control " id="province" name="province" onChange="loadprovince(this.value,'city');" >
             <?php 
             if(count($province)>0){
              foreach($province as $type){
              // if( array_search( $rows->province,$type->list_id))
			  if($rows->province == $type->list_id)
              {
                echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
              }
              else
			  { 
			  	echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
              }

          }
        }
        ?>
      </select>

    </div>
    					<div class="form-group col-md-3">
      <label for="basic-input"><strong>الولاية:</strong></label>
      <select class="form-control " id="city" name="city" >
       <?php 
       if(count($city)>0){
        foreach($city as $type){
         //if( array_search( $rows->city,$type->list_id))
		 if($rows->city == $type->list_id)
         {
          echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
        }
        else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
      }

    }
  }
  ?>
</select>
</div>
                    <div class="form-group col-md-3">
              <label for="basic-input"><strong>القرية :</strong></label>
              <input type="text" class="form-control  " value="<?php echo $rows->village?>" placeholder="القرية" name="village" id="village" />
            </div>
            <div class="NonCorporation">
                    
                     
            </div>
             
            
            
            <div class="Corporation" style="display:none;">
            <div class="form-group col-md-3">
                      <label for="basic-input"><strong> الحالة الاجتماعية:</strong></label>
                      <select class="form-control " id="socialstatus" name="socialstatus" >
                              <?php 
                              if(count($marital_status)>0){
                                  foreach($marital_status as $type){
                                     if( array_search( $rows->socialstatus,$type->list_id))
                                      {
                                          echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
                                      }
                                      else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
                                      }
                                      
                                  }
                              }
                              ?>
                              </select>
                    </div>
           <div class="form-group col-md-3">
              <label for="basic-input"><strong>المتضرر :</strong></label>
              <input type="text" class="form-control  " value="<?php echo $rows->person_name?>" placeholder="المتضرر" name="person_name" id="person_name" />
            </div>
            <div class="form-group col-md-3">
              <label for="basic-input"><strong>عدد الافراد :</strong></label>
              <input type="number" class="form-control  " value="<?php echo $rows->numberofIndividual?>" placeholder="عدد الافراد" name="numberofIndividual" id="numberofIndividual" />
            </div>
            <div class="form-group col-md-3">
                      <label for="basic-input"><strong>العمر :</strong></label>
                      <input type="number" class="form-control " value="<?php echo $rows->age?>" placeholder="العمر" name="age" id="age" />
                    </div>
            <div class="form-group col-md-3">
                      <label for="basic-input"><strong>هاتف :</strong></label>
                      <input type="tel" class="form-control  " value="<?php echo $rows->phone?>" placeholder="هاتف" name="phone" id="phone" />
                    </div>
                                        
                     <div class="form-group col-md-3">
                      <label for="basic-input"><strong>العنوان  :</strong></label>
                      <input type="text" class="form-control  " value="<?php echo $rows->address?>" placeholder="عدد الافراد" name="address" id="address" />
                    </div>
                    
                     <div class="form-group col-md-3">
                      <label for="basic-input"><strong>  مكان العمل  :</strong></label>
                      <input type="text" class="form-control  " value="<?php echo $rows->work?>" placeholder=" مكان العمل" name="work" id="work" />
                    </div>
            </div>
            <div class="NonCorporation" >
                    

            </div>
            </div>
             <div class="row">
                 <div class="form-group col-md-6">
                  <label for="basic-input"><strong>البيان  :</strong></label>
                  <textarea name="statement" placeholder="البيان" class="form-control req"  id="statement"><?php echo $rows->statement?></textarea>
                </div>
                 <div class="form-group col-md-6">
                  <label for="basic-input"><strong>ملخص الحالة  :</strong></label>
                  <textarea name="statussummary" placeholder="ملخص الحالة" class="form-control "  id="statussummary"><?php echo $rows->statussummary?></textarea>
                </div>
            </div>
            <div class="row">
                 <div class="form-group col-md-6">
                  <label for="basic-input"><strong>بيانات المنزل  :</strong></label>
                  <textarea name="urbansituation" placeholder="بيانات المنزل" class="form-control "  id="urbansituation"><?php echo $rows->urbansituation?></textarea>
                </div>
                 <div class="form-group col-md-6">
                  <label for="basic-input"><strong>وضع الاسر المتأثرة  :</strong></label>
                  <textarea name="familiesaffected" placeholder="وضع الاسر المتأثرة" class="form-control "  id="familiesaffected"><?php echo $rows->familiesaffected?></textarea>
                </div>
            </div>
            <div class="row">
                 <div class="form-group col-md-6" style="display:none;">
                  <label for="basic-input"><strong>الاحتياجات المطلوبة :</strong></label>
                  <textarea name="requirements" placeholder="الاحتياجات المطلوبة" class="form-control "  id="requirements"><?php echo $rows->requirements?></textarea>
                </div>
                 <div class="form-group col-md-6" style="display:none;">
                  <label for="basic-input"><strong>البيان التفصيلي:</strong></label>
                  <textarea name="breakdown" placeholder="البيان التفصيلي" class="form-control "  id="breakdown"><?php echo $rows->breakdown?></textarea>
                </div>
            </div>
            <div class="row">
                 <div class="form-group col-md-6" style="display:none;">
                  <label for="basic-input"><strong>الاقتراح :</strong></label>
                  <textarea name="proposal" placeholder="الاقتراح " class="form-control "  id="proposal"><?php echo $rows->proposal?></textarea>
                </div>
                 <div class="form-group col-md-6">
                  <label for="basic-input"><strong>الإجراءات :</strong></label>
                  <textarea name="measures" placeholder="الإجراءات " class="form-control "  id="measures"><?php echo $rows->measures?></textarea>
                </div>
            </div>
            <div class="row">
                 <div class="form-group col-md-6">
                  <label for="basic-input"><strong>ارفاق المسح الضوئي :</strong></label>
                 <input type="file" name="attachment[]"  class="form-control " id="attachment" multiple>
                 <input type="hidden" name="attachment_old"  class="form-control " id="attachment_old" value="<?php echo $rows->attachment?>" />
                  <?php if($rows->attachment !=""){
				   $i=0;
				   $attachment = @explode(',',$rows->attachment);
				   foreach($attachment as $att){
					   if($att !=""){
						   $i++;
				   ?>
                   <div id="att_<?php echo $i;?>" style="float:right;">
              <?php echo $controller->getfileicon($att,base_url().'resources/aid/'.$login_userid);?>
              <a class="iconspace" href="#deleteDiag" onClick="show_delete_dialogs(this,'att_<?php echo $i;?>','<?php echo $att?>');" id="<?php echo  $rows->serviceid ?>" data-url="<?php echo base_url()?>aid/removeimage/<?php echo $rows->serviceid ?>/<?php echo $att ?>"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>
              </div>
              <?php } } }?>
                </div>
                 <div class="form-group col-md-6">
                  <label for="basic-input"><strong>ارفاق الصور :</strong></label>
                  <input type="file" name="attachments[]"  class="form-control " id="attachments" multiple>
                  <input type="hidden" name="images_old"  class="form-control " id="images_old" value="<?php echo $rows->images?>" />
                   <?php if($rows->images !=""){
				   $i=0;
				   $attachment1 = @explode(',',$rows->images);
				   foreach($attachment1 as $att){
					   if($att !=""){
						   $i++;
				   ?>
                   <div id="picture_<?php echo $i;?>" style="float:right;">
                <?php echo $controller->getfileicon($att,base_url().'resources/aid/'.$login_userid);?>
              <a class="iconspace" href="#deleteDiag" onClick="show_delete_dialogs(this,'picture_<?php echo $i;?>','<?php echo $att?>');" id="<?php echo  $rows->serviceid ?>" data-url="<?php echo base_url()?>aid/removeimage/<?php echo $rows->serviceid ?>/<?php echo $att ?>"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>
              </div>
              <?php } } }?>
                </div>
            </div>
            <br clear="all">
            <div class="form-group col-md-6">
              <button type="button" id="save_service" name="save_service" class="btn btn-success">حفظ</button>
            </div>
          </form>
                </div>
            </div>
        </div>
        <?php if($rows->urgentaidId	>	0){ ?>
        
<!--    <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFour">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        <i class="more-less glyphicon <?php if($rows->urgentaidId > 0){ ?> glyphicon-minus<?php }else{?>glyphicon-plus<?php }?>"></i>
                       اجتماع اللجنة المبدئي
                    </a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse <?php if($rows->urgentaidId>0){ ?> in <?php }?>" role="tabpanel" aria-labelledby="headingFour">
                <div class="panel-body">
                     <div class="row table-header-row" style="margin-right: 0px;"><a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>aid/addmeeting/<?php echo $rows->urgentaidId ?>/1" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">إضافة</a> </div>
                     <div id="listallmeeting1"></div>
                </div>
            </div>
        </div>
		<div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="more-less glyphicon  <?php if($rows->urgentaidId > 0){ ?> glyphicon-minus<?php }else{?>glyphicon-plus<?php }?>"></i>
                       المصروفات
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse <?php if($rows->urgentaidId>0){ ?> in <?php }?>" role="tabpanel "  aria-labelledby="headingOne">
                <div class="panel-body">
                     <div class="row table-header-row" style=" margin-right: 0px;"><a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>aid/addExpences/<?php echo $rows->urgentaidId ?>" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">إضافة</a> </div>
                        <div  id="listingexpences"></div>
                         
                          
                     
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <i class="more-less glyphicon <?php if($rows->urgentaidId > 0){ ?> glyphicon-minus<?php }else{?>glyphicon-plus<?php }?>"></i>
                       اجتماع اللجنة
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse <?php if($rows->urgentaidId>0){ ?> in <?php }?>" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                     <div class="row table-header-row" style="margin-right: 0px;"><a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>aid/addmeeting/<?php echo $rows->urgentaidId ?>/2" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">إضافة</a> </div>
                     <div id="listallmeeting2"></div>
                </div>
            </div>
        </div>-->
        
        
        <div class="panel panel-default">
  <div class="panel-heading" role="tab" id="headingSeven">
    <h4 class="panel-title">
      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
        <i class="more-less glyphicon  glyphicon-minus "></i>
        اجتماع لجنة الإغاثة
      </a>
    </h4>
  </div>
  
  <div id="collapseSeven" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="headingSeven">
    <div class="panel-body">
     <table class="table table-bordered table-striped " id="internal_relief_committee_meeting_addId"  >
      <thead style="background-color: #029625;">
        <tr role="row" style="color:#fff !important;">
          <th align="center">رقم محضر الاجتماع</th>
          <th align="center">التاريخ</th>
          <th align="center">قرار اللجنة</th>
          <th align="center">الملاحظات</th>
          <th align="center">الإجراءات</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $extreliefTMId =0;
        if(isset($ah_internal_relief_committee_meeting)){
          if(count($ah_internal_relief_committee_meeting)>0){
            foreach($ah_internal_relief_committee_meeting as $reliefteam) { 
              $extreliefTMId ++;
              $action ='';
              $action .= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_internal_relief_committee_meeting/'.$reliefteam->internalMeetingId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a> ';
		if($reliefteam->aid_attachemnt)
		{
					//$action	.=	' <a class="fancybox-button" rel="gallery1" href="'.base_url().'resources/aid/'.$reliefteam->userid.'/'.$reliefteam->aid_attachemnt.'"><i class="myicon icon-camera" data-hasqtip="'.$reliefteam->extrelMeetingId.'" aria-describedby="qtip-'.$reliefteam->extrelMeetingId.'"></i></a>';
				$action	.=	' <i href="'.base_url().'resources/aid/'.$reliefteam->userid.'/'.$reliefteam->aid_attachemnt.'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "></i> ';
		}
              //if($reliefteam->userid == $login_userid)
			 // {
				$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_1/'.$reliefteam->internalMeetingId.'" id="'.$reliefteam->internalMeetingId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
             // }
              
              //if($permissions_d== 1) 
              //{
                $action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->internalMeetingId.'" data-url="'.base_url().'aid/delete_internal_relief_committee_meeting/'.$reliefteam->urgentaidId.'/'.$reliefteam->internalMeetingId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
             // }
              
              ?>
              <tr  id="<?php echo $reliefteam->internalMeetingId;?>_meeting">
                <td id="<?php echo $reliefteam->internalMeetingId;?>_meeting_minutes"><?php echo $reliefteam->minutes;?></td>
                <td id="<?php echo $reliefteam->internalMeetingId;?>_meeting_currentdate"><?php echo $reliefteam->currentdate;?></td>
                <td id="<?php echo $reliefteam->internalMeetingId;?>_meeting_decision"><?php echo $this->haya_model->limit($reliefteam->decision, '50');?></td>
                <td id="<?php echo $reliefteam->internalMeetingId;?>_meeting_notes"><?php echo $this->haya_model->limit($reliefteam->notes, '50');?></td>
                <td><?php echo $action;?></td>
              </tr> 
              <?php }
              ?>
              <script type="text/javascript">progress = progress + 11; setprogressbar(progress);</script>
              <?php
            } } $extreliefTMId ++;?> 
            <form action="" method="POST" id="committee_meeting" name="committee_meeting" autocomplete="off" enctype="multipart/form-data">
            <tr>
              
             <td colspan="5">
             
              <input type="text" class="form-control  " value="" placeholder="رقم محضر الاجتماع" name="minutes" id="minutes" style="float:right; margin-left:5px; width:25%" />

              <input type="text" class="form-control  datepicker" value="" placeholder="التاريخ" name="currentdates" id="currentdates" style="float:right; margin-left:5px; width:25%" />
                       <input type="file" class="form-control " name="aid_attachemnt" id="aid_attachemnt" placeholder="المرفقات :" style="float:right; margin-left:5px; width:25%" />

              <button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px; " onClick="add_internal_relief_committee_meeting_muzi('0');">اضافة</button>

              <textarea class="form-control  "  placeholder="قرار اللجنة" name="decision" id="decision"  style="float:right; margin-top:5px;" rows="5" ></textarea>
              <textarea class="form-control  " placeholder="الملاحظات" name="notes" id="notes" style="float:right; margin-top:5px;" rows="5" ></textarea>

                            <input type="hidden"  name="urgentaidId" id="urgentaidId" value="<?php if(isset($rows->urgentaidId	) and $rows->urgentaidId	 >0){echo $rows->urgentaidId;} else {echo '0';} ?>" />

                 </td>
                 
               </tr>  
               </form>  
             </tbody>
           </table>
         </div>
       </div>
     </div>
        <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingFour">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
            <i class="more-less glyphicon  glyphicon-minus "></i>
            أعضاء فريق الاغاثة
          </a>
        </h4>
      </div>

      <div id="collapseFour" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="headingFour">
        <div class="panel-body">
         <table class="table table-bordered table-striped "  id="internal_reliefteam_addId"  >
          <thead style="background-color: #029625;">
            <tr role="row" style="color:#fff !important;">
              <th>رقم</th>
              <th>الاسم</th>
              <th>الوظيفة</th>
              <th>الجهة</th>
              <th>الإجراءات</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $extreliefTMId =0;
            if(isset($internal_reliefteam)){
              if(count($internal_reliefteam)>0){
                foreach($internal_reliefteam as $reliefteam) { 
                  $extreliefTMId ++;
                  $action ='';
                  $action .= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_internal_reliefteam/'.$reliefteam->extreliefTMId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
				  
                 // if($reliefteam->userid == $login_userid)
				  //{
				$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_2/'.$reliefteam->extreliefTMId.'" id="'.$reliefteam->extreliefTMId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
                  //}


                  //if($permissions_d== 1) 
                  //{
                    $action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->urgentaidexpId.'" data-url="'.base_url().'aid/delete_internal_reliefteam/'.$reliefteam->extreliefId.'/'.$reliefteam->extreliefTMId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
                  //}
                  
                  ?>
                  <tr id="<?php echo $reliefteam->extreliefTMId;?>_team">
                    <td id="<?php echo $reliefteam->extreliefTMId;?>_team_id"><?php echo $extreliefTMId; ?></td>
                    <td id="<?php echo $reliefteam->extreliefTMId;?>_team_name"><?php echo $reliefteam->name;?></td>
                    <td id="<?php echo $reliefteam->extreliefTMId;?>_team_name"><?php echo $reliefteam->job;?></td>
                    <td id="<?php echo $reliefteam->extreliefTMId;?>_team_organization"><?php echo $reliefteam->organization;?></td>
                    <td><?php echo $action;?></td>
                  </tr> 
                  <?php } 
                  ?>
                  <script type="text/javascript">progress = progress + 11; setprogressbar(progress);</script>
                  <?php
                } } $extreliefTMId ++;?> 
                <tr>
                 <td><input type="text" class="form-control " value="<?php echo $extreliefTMId?>" placeholder="رقم" name="extreliefTMIdauto" id="extreliefTMIdauto"  readonly /></td>
                 <td><input type="text" class="form-control  " value="" placeholder="الاسم" name="new_name" id="new_name" /></td>
                 <td><input type="text" class="form-control  " value="" placeholder="الوظيفة" name="job" id="job" /></td>
                 <td><input type="text" class="form-control  " value="" placeholder="الجهة" name="organization" id="organization" /></td>
                 <td><button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px; " onClick="add_internal_reliefteam('0');">اضافة</button></td>
                 <input type="hidden" name="autoid_1" id="autoid_1" value="<?php echo$extreliefTMId; ?>" /> 
               </tr>    
             </tbody>
           </table>
         </div>
       </div>
     </div>
		<div class="panel panel-default">
  <div class="panel-heading" role="tab" id="headingEight">
    <h4 class="panel-title">
      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
        <i class="more-less glyphicon  glyphicon-minus "></i>
        خطة الإغاثة
      </a>
    </h4>
  </div>

  <div id="collapseEight" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="headingEight">
    <div class="panel-body">
    <?php $aid_plan_detail	=	$this->urgent_aid->get_internal_aid_plan_detail($rows->urgentaidId);?>
    <?php
	if($aid_plan_detail->plan_attachment)
	{
	$image 	=	'<span><i href="'.base_url().'resources/aid/'.$aid_plan_detail->userid.'/'.$aid_plan_detail->plan_attachment.'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "=""></i></span>'; 
	}
	else
	{
		$image	=	'';
	}
	?>
           <form method="POST" action="<?php echo base_url();?>aid/internal_aid_plan" id="aid_plan_form" name="aid_plan_form" enctype="multipart/form-data"  >
                  <input type="hidden"  name="aid_extreliefId" id="aid_extreliefId" value="<?php if(isset($rows->urgentaidId) and $rows->urgentaidId >0){echo $rows->urgentaidId;} else {echo '0';} ?>" />
                  <input type="hidden"  name="planid" id="planid" value="<?php echo (isset($aid_plan_detail->planid) ? $aid_plan_detail->planid : NULL)?>" />
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group col-md-4">
                        <label for="basic-input"><strong>الموازنة  :</strong></label>
                        <input type="text" class="form-control"  value="<?php echo (isset($aid_plan_detail->budget) ? $aid_plan_detail->budget : NULL)?>" placeholder="رقم الموازنة" name="budget" id="budget"/>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="basic-input"><strong>التحويل :</strong></label>
                        <input type="text" class="form-control" value="<?php echo (isset($aid_plan_detail->transfer_money) ? $aid_plan_detail->transfer_money : NULL)?>" placeholder="التاريخ " name="transfer_money" id="transfer_money" />
                      </div>
                      <div class="form-group col-md-4">
                        <label for="basic-input"><strong>المرفقات : <?php echo $image;?></strong></label>
                        <input type="file" class="form-control  <?php if(isset($aid_plan_detail->planid) and $aid_plan_detail->planid >0){?> <?php }else{?> req <?php }?>" name="plan_attachment" id="plan_attachment" />
                        <br/>
                        <input type="hidden"  name="plan_attachment_old" id="plan_attachment_old" value="<?php echo (isset($aid_plan_detail->plan_attachment) ? $aid_plan_detail->plan_attachment : NULL)?>" />
   <?php if($rows->plan_attachment !=""){
   $i=0;
   $attachment = @explode(',',$rows->plan_attachment);
   foreach($attachment as $att){
    if($att !=""){
     $i++;
     ?>
                        <div id="picture_<?php echo $i;?>" style="float:right;"> <?php echo $controller->getfileicon($att,base_url().'resources/aid/'.$rows->userid)?> <a class="iconspace" href="#deleteDiag" onClick="show_delete_dialogs(this,'picture_<?php echo $i;?>','<?php echo $att?>');" id="<?php echo  $rows->serviceid ?>" data-url="<?php echo base_url()?>aid/removeimage/<?php echo $rows->planid ?>/<?php echo $att ?>"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> </div>
                        <?php } } }?>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="basic-input"><strong>التوجيهات :</strong></label>
                        <textarea class="form-control  "  placeholder="التوجيهات" name="instructions" id="instructions" rows="5" ><?php echo (isset($aid_plan_detail->instructions) ? $aid_plan_detail->instructions : NULL)?></textarea>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="basic-input"><strong>الخطة :</strong></label>
                        <textarea class="form-control  "  placeholder="الخطة" name="plan" id="plan" rows="5" ><?php echo (isset($aid_plan_detail->plan) ? $aid_plan_detail->plan : NULL)?></textarea>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="basic-input"><strong>إضافة مقترح :</strong></label>
                        <textarea class="form-control  "  placeholder="التوجيهات" name="give_suggestion" id="give_suggestion" rows="5" ><?php echo (isset($aid_plan_detail->give_suggestion) ? $aid_plan_detail->give_suggestion : NULL)?></textarea>
                      </div>
                    <div class="form-group col-md-6">
  					<button type="submit" id="save_aid_plan" name="save_aid_plan" class="btn btn-success">حفظ</button>
                    </div>
                    </div>
                  </div>
                </form>
                    </div>
</div>
</div>
        <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingFive">
                        <h4 class="panel-title">
                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            <i class="more-less glyphicon  glyphicon-minus "></i>
                            الاحتياجات المطلوبة
                          </a>
                        </h4>
                      </div>

                      <div id="collapseFive" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="headingFive">
                        <div class="panel-body">
                         <table class="table table-bordered table-striped "   id="internal_requirements_required_addId" >
                          <thead style="background-color: #029625;">
                            <tr role="row" style="color:#fff !important;">
                              <th width="10%">رقم</th>
                              <th width="20%">نوع المواد</th>
                              <th width="20%">المواد</th>
                              <th width="15%">الكمية</th>
                              <th width="20%">الموازنة</th>
                              <th width="15%">الإجراءات</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $extreliefTMId =0;
                            if(isset($internal_requirements_required)){
                              if(count($internal_requirements_required)>0){
                                foreach($internal_requirements_required as $reliefteam) { 
                                  $extreliefTMId ++;
                                  $action ='';
                                  $action .= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_internal_requirements_required/'.$reliefteam->requirementsId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
                                 // if($reliefteam->userid == $login_userid)
								 // {
				$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_3/'.$reliefteam->requirementsId.'" id="'.$reliefteam->requirementsId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
                                 // }

                                 // if($permissions_d== 1) 
                                  //{
                                    $action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->requirementsId.'" data-url="'.base_url().'aid/delete_internal_requirements_required/'.$reliefteam->extreliefId.'/'.$reliefteam->requirementsId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
                                  //}

                                  ?>
                                  <tr id="<?php echo $reliefteam->requirementsId;?>_required">
                                    <td id="<?php echo $reliefteam->requirementsId;?>_required_id"><?php echo $extreliefTMId; ?></td>
                                    <td id="<?php echo $reliefteam->requirementsId;?>_required_typeOfMaterial"><?php echo$this->haya_model->get_name_from_list($reliefteam->typeOfMaterial);?></td>
                                    <td id="<?php echo $reliefteam->requirementsId;?>_required_materials"><?php echo $reliefteam->materials;?></td>
                                    <td id="<?php echo $reliefteam->requirementsId;?>_required_quantity"><?php echo $reliefteam->quantity;?></td>
                                    <td id="<?php echo $reliefteam->requirementsId;?>_required_budgeting"><?php echo number_format($reliefteam->budgeting,3);?></td>
                                    <td><?php echo $action;?></td>                               
                                  </tr> 
                                  <?php } 
                                  ?>
                                  <script type="text/javascript">progress = progress + 11; setprogressbar(progress);</script>
                                  <?php
                                } } $extreliefTMId ++;?> 
                                <tr>
                                 <td><input type="text" class="form-control " value="<?php echo $extreliefTMId?>" placeholder="رقم" name="requirementsIdauto" id="requirementsIdauto"  readonly /></td>
                                 <td>
                                   <select class="form-control " id="typeOfMaterial" name="typeOfMaterial"  >
                                    <option value="">--تحديد--</option>
                                    <?php 
                                    if(count($typeOfMaterial)>0){
                                      foreach($typeOfMaterial as $type){
                                       if($rows->typeOfMaterial == $type->list_id)
                                       {
                                        echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
                                      }
                                      else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
                                    }

                                  }
                                }
                                ?>
                              </select>
                              <input type="hidden" name="autoid_2" id="autoid_2" value="<?php echo $extreliefTMId;?>" />
                              <!--<input type="text" class="form-control  " value="" placeholder="نوع المواد" name="typeOfMaterial" id="typeOfMaterial" />-->
                            </td>
                            <td><input type="text" class="form-control " value="" placeholder="المواد" name="materials" id="materials" /></td>                                 
                            <td><input type="text" class="form-control  " value="" placeholder="الكمية" name="quantity" id="quantity" /></td>
                            <td><input type="text" class="form-control  " value="" placeholder="الموازنة" name="extbudgeting" id="extbudgeting" /></td>
                            <td><button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px; " onClick="add_internal_requirements_required('0');">اضافة</button></td>
                          </tr>    
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
        <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingSix">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
            <i class="more-less glyphicon  glyphicon-minus "></i>
            المصروفات
          </a>
        </h4>
      </div>

      <div id="collapseSix" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="headingSix">
        <div class="panel-body">
          <form id="frm_internal_expenses_addId" name="frm_internal_expenses_addId" method="post" enctype="multipart/form-data">
            <input type="hidden"  name="extreliefIds" id="extreliefIds" value="<?php if(isset($rows->urgentaidId) and $rows->urgentaidId >0){echo $rows->urgentaidId;} else {echo '0';} ?>" />
            <table class="table table-bordered table-striped " id="internal_expenses_addId" >
              <thead style="background-color: #029625;">
                <tr role="row" style="color:#fff !important;">
                  <th width="10%">التسلسل</th>
                  <th width="10%">مصروفات إدارية</th>
                  <th width="10%">المواد الغذائية</th>
                  <th width="10%">المواد الاغاثية</th>
                  <th width="15%">المواد الطبية</th>
                  <th width="15%">اخرى</th>
                  <th width="15%">الاجمالي</th>
                  <th width="15%">الملاحظات</th>
                  <th width="15%">المرفقات</th>
                  <th width="15%">الإجراءات</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $extreliefTMId	=	0;
                if(isset($internal_expenses)){
                  if(count($internal_expenses)>0){
                    foreach($internal_expenses as $reliefteam) { 
					
					$total_amount	=	($reliefteam->prose+$reliefteam->food+$reliefteam->reliefArticles+$reliefteam->medicalMaterials+$reliefteam->other);
                      $extreliefTMId ++;
                      $action ='';
                      $action .= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_internal_expenses/'.$reliefteam->extExpencesId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
                      //if($reliefteam->userid == $login_userid)
					  //{
			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_4/'.$reliefteam->extExpencesId.'" id="'.$reliefteam->extExpencesId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
                     // }
                      
                     // if($permissions_d== 1) 
                     // {
                        $action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->extExpencesId.'" data-url="'.base_url().'aid/delete_internal_expenses/'.$reliefteam->extreliefId.'/'.$reliefteam->extExpencesId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
                    //  }

                      ?>
                      <tr  id="<?php echo $reliefteam->extExpencesId;?>_expenses">
                        <td  id="<?php echo $reliefteam->extExpencesId;?>_expenses_id"><?php echo $extreliefTMId; ?></td>
                        <td  id="<?php echo $reliefteam->extExpencesId;?>_expenses_prose"><?php echo number_format($reliefteam->prose,3);?></td>
                        <td  id="<?php echo $reliefteam->extExpencesId;?>_expenses_food"><?php echo number_format($reliefteam->food,3);?></td>
                        <td id="<?php echo $reliefteam->extExpencesId;?>_expenses_reliefArticles"><?php echo number_format($reliefteam->reliefArticles,3);?></td>
                        <td id="<?php echo $reliefteam->extExpencesId;?>_expenses_medicalMaterials"><?php echo number_format($reliefteam->medicalMaterials,3);?></td>
                        <td id="<?php echo $reliefteam->extExpencesId;?>_expenses_other"><?php echo number_format($reliefteam->other,3);?></td>
                        <td id="<?php echo $reliefteam->extExpencesId;?>_expenses_total"><?php echo number_format($total_amount,3);?></td>
                        <td id="<?php echo $reliefteam->extExpencesId;?>_expenses_notes"><?php echo $this->haya_model->limit($reliefteam->notes, '50');?></td>
                        <td id="<?php echo $reliefteam->extExpencesId;?>_expenses_attachment"> <input type="hidden" id="<?php echo $reliefteam->extExpencesId;?>_expenses_attachment_old" name="<?php echo $reliefteam->extExpencesId;?>_expenses_attachment_old" value="<?php echo $reliefteam->attachment;?>" />
                          <?php if($reliefteam->attachment !=""){
                           $i=0;
                           $attachment = @explode(',',$reliefteam->attachment);
                           foreach($attachment as $att){
                             if($att !=""){
                               $i++;
                               ?>
                               <div id="picture_<?php echo $i;?>" style="float:right;">
                                 <?php echo $controller->getfileicon($att,base_url().'resources/aid/'.$reliefteam->userid)?>
                                 <!--  <a class="iconspace" href="#deleteDiag" onClick="show_delete_dialogs1(this,'picture_<?php echo $i;?>','<?php echo $att?>','<?php echo $reliefteam->extExpencesId;?>');" id="<?php echo  $reliefteam->extreliefId ?>" data-url="<?php echo base_url()?>aid/removeimage/<?php echo $reliefteam->extreliefId ?>/<?php echo $att ?>"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>-->
                               </div>
                               <?php } } }?></td>
                               <td><?php echo $action;?></td>
                             </tr> 
                             <?php } 
                             ?>
                             <script type="text/javascript">progress = progress + 11; setprogressbar(progress);</script>
                             <?php
                           } } $extreliefTMId ++;?> 
                           <tr>

                            <td><input type="text" class="form-control " value="<?php echo $extreliefTMId?>" placeholder="التسلسل" name="extExpencesIdauto" id="extExpencesIdauto"  readonly /><br/>
                             <button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px; " onClick="add_internal_expenses('0');">حفظ</button></td>
                             <td><input type="number" class="form-control" value="" placeholder="النثرية" name="prose" id="prose" />
                             </td>
                             <td><input type="number" class="form-control  " value="" placeholder="المواد الغذائية" name="food" id="food" />
                             </td>



                             <td><input type="number" class="form-control  " value="" placeholder="المواد الاغاثية" name="reliefArticles" id="reliefArticles" />
                             </td>




                             <td>
                               <input type="number" class="form-control  " value="" placeholder="المواد الطبية" name="medicalMaterials" id="medicalMaterials" />
                             </td>  

                             <td>
                               <input type="number" class="form-control  " value="" placeholder="اخرى" name="other" id="other" />
                             </td>
                             <td>
                              <input type="number" class="form-control  " value="" placeholder="الاجمالي" name="total" id="total" style="display:none;"/></td>

                              <td>
                                <textarea name="notes" id="notes_3" class="form-control  " rows="5"  ></textarea> </td>  
                                <td colspan="2"><input type="file" class="form-control "  placeholder="المرفقات" name="attachment1[]" id="attachment1"  multiple="multiple" style="border:none;"/>
                                 <input type="hidden"  name="attachment1_old" id="attachment1_old"  />
                               </td> 


                                <!-- <td colspan="3"><input type="number" class="form-control" value="" placeholder="النثرية" name="prose" id="prose" />
                                 <br/>
                                 <input type="number" class="form-control  " value="" placeholder="المواد الطبية" name="medicalMaterials" id="medicalMaterials" />
                               </td>-->
                                <!-- <td colspan="2"><input type="number" class="form-control  " value="" placeholder="المواد الغذائية" name="food" id="food" />
                                 <br/>
                                 <input type="number" class="form-control  " value="" placeholder="اخرى" name="other" id="other" />
                               </td>-->

                                  <!--<td colspan="2"><input type="number" class="form-control  " value="" placeholder="المواد الاغاثية" name="reliefArticles" id="reliefArticles" />
                                  <br/>
                                  <input type="number" class="form-control  " value="" placeholder="الاجمالي" name="total" id="total" /></td>-->
                                  
                                </tr>    
                              </tbody>
                            </table>
                            <input type="hidden"  name="autoid_5" id="autoid_5"  value="<?php echo $extreliefTMId;?>"/>
                          </form>        
                        </div>
                      </div>
                    </div>
        <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingNine">
                    <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                        <i class="more-less glyphicon  glyphicon-minus "></i>
                        التقرير اليومي
                      </a>
                    </h4>
                  </div>
                  <div id="collapseNine" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="headingNine">
                    <div class="panel-body">
                      <table class="table table-bordered table-striped "  id="internal_daily_report_addId" >
                        <thead style="background-color: #029625;">
                          <tr role="row" style="color:#fff !important;">
                            <th>رقم</th>
                            <th>التاريخ</th>
                            <th>البيان</th>
                            <th>الإجراءات</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $extreliefTMId =0;
                          if(isset($internal_daily_report)){
                            if(count($internal_daily_report)	>	0){
                             foreach($internal_daily_report as $reliefteam) { 
                               $extreliefTMId ++;
                               $action ='';
                               $action  .= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_internal_daily_report_muzi/'.$reliefteam->extDataReportId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
                              // if($reliefteam->userid == $login_userid)
							   //{
			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_5/'.$reliefteam->extDataReportId.'" id="'.$reliefteam->extDataReportId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
                              //}
                              //if($permissions_d== 1) 
                              //{
                                $action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->extrelMeetingId.'" data-url="'.base_url().'aid/delete_internal_daily_report_muzi/'.$reliefteam->extreliefId.'/'.$reliefteam->extDataReportId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
                              //}
		if($reliefteam->report_attachment)
		{
			$action	.=	' <i href="'.base_url().'resources/aid/'.$reliefteam->userid.'/'.$reliefteam->report_attachment.'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "></i> ';
		}

                              ?>
                              <tr id="<?php echo $reliefteam->extDataReportId;?>_daily">
                                <td id="<?php echo $reliefteam->extDataReportId;?>_daily_id"><?php echo $extreliefTMId;?></td>
                                <td id="<?php echo $reliefteam->extDataReportId;?>_daily_currentdate"><?php echo $reliefteam->currentdate;?></td>
                                <td id="<?php echo $reliefteam->extDataReportId;?>_daily_statement"><?php echo $this->haya_model->limit($reliefteam->statement, '50');?></td><td><?php echo $action;?></td>
                              </tr> 
                              <?php } 
                              ?>
                              <script type="text/javascript">progress = progress + 11; setprogressbar(progress);</script>

                              <?php
                            } } $extreliefTMId ++;?> 

            <form action="" method="POST" id="internal_reliefteam" name="internal_reliefteam" autocomplete="off" enctype="multipart/form-data">
                            <tr id="last_daily">

                             <td colspan="4">
                              <input type="text" class="form-control " value="<?php echo $extreliefTMId?>" placeholder="رقم" name="extDataReportIddauto" id="extDataReportIddauto"  readonly style="width:20%; float:right;" />

                              <input type="text" class="form-control  datepicker" value="" placeholder="التاريخ" name="currentdaterep" id="currentdaterep" style="width:20%; float:right; margin-right:5px;"  />
<input type="file" class="form-control " name="report_attachment" id="report_attachment" multiple placeholder="المرفقات :"  style="width:20%; float:right; margin-right:5px;"/>
                                   <button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float:left; margin-bottom: 8px; " onClick="add_internal_daily_report_muzi('0');" >اضافة</button>
                                   <br/>
                                   <textarea class="form-control" value="" placeholder="البيان" name="statement" id="statement_2" rows="5" style="margin-top:5px;" ></textarea>
                                  
                                   </td>

                                 </tr> 

                                  <input type="hidden"  name="external_extreliefId" id="external_extreliefId" value="<?php if(isset($rows->urgentaidId) and $rows->urgentaidId >0){echo $rows->urgentaidId;} else {echo '0';} ?>" />
                                  <input type="hidden"  name="autoid_4" id="autoid_4" value="<?php echo $extreliefTMId; ?>" />
                                </form>   
                               </tbody>
                             </table> 

                           </div>
                     </div>
                   </div>

		<?php //if(isset($extreliefId) and $extreliefId >0){?>
            <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                <i class="more-less glyphicon  glyphicon-minus "></i>
                التقرير النهائي
              </a>
            </h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <table class="table table-bordered table-striped "  id="internal_daily_final_report" >
                <thead style="background-color: #029625;">
                  <tr role="row" style="color:#fff !important;">
                    <th>رقم</th>
                    <th>التاريخ</th>
                    <th>نسبة التنفيذ</th>
                    <th>البيان</th>
                    <th>الإجراءات</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $extreliefTMId =0;
                  if(isset($ah_internal_final_report)){
                    if(count($ah_internal_final_report)	>	0){
                     foreach($ah_internal_final_report as $reliefteam) { 
                       $extreliefTMId ++;
                       $action ='';
                       $action  .= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_internal_final_report/'.$reliefteam->extDataReportId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
                      // if($reliefteam->userid == $login_userid){
			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_6/'.$reliefteam->extDataReportId.'" id="'.$reliefteam->extDataReportId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
                      //}
                      //if($permissions_d== 1) 
                      //{
                        $action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->extrelMeetingId.'" data-url="'.base_url().'aid/delete_internal_final_report/'.$reliefteam->extreliefId.'/'.$reliefteam->extDataReportId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
                     // }
if($reliefteam->report_attachment)
{
    $action	.=	' <i href="'.base_url().'resources/aid/'.$reliefteam->userid.'/'.$reliefteam->report_attachment.'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "></i> ';
}

                      ?>
                      <tr id="<?php echo $reliefteam->extDataReportId;?>_daily">
                      <td id="<?php echo $reliefteam->extDataReportId;?>_daily_id"><?php echo $extreliefTMId;?></td>
                        <td id="<?php echo $reliefteam->extDataReportId;?>_daily_currentdate"><?php echo $reliefteam->currentdate;?></td>
                        <td id="<?php echo $reliefteam->final_percent;?>_final_percent"><?php echo $reliefteam->final_percent;?></td>
                        <td id="<?php echo $reliefteam->extDataReportId;?>_daily_statement"><?php echo $this->haya_model->limit($reliefteam->statement, '50');?></td><td><?php echo $action;?></td>
                      </tr> 
                      <?php } 
                      ?>
                      <script type="text/javascript">progress = progress + 11; setprogressbar(progress);</script>

                      <?php
                    } } $extreliefTMId ++;?> 

    <form action="" method="POST" id="the_internal_final_report" name="the_internal_final_report" autocomplete="off" enctype="multipart/form-data">
                    <tr id="last_daily">

                     <td colspan="5">
                      <input type="text" class="form-control " value="<?php echo $extreliefTMId?>" placeholder="رقم" name="final_extDataReportIddauto" id="final_extDataReportIddauto"  readonly style="width:20%; float:right;" />

         <input type="text" class="form-control  datepicker" value="" placeholder="التاريخ" name="final_currentdaterep" id="final_currentdaterep" style="width:20%; float:right; margin-right:5px;"  />
         <input type="text" class="form-control " value="" placeholder="نسبة التنفيذ" name="final_percent" id="final_percent" style="width:20%; float:right; margin-right:5px;"  />
<input type="file" class="form-control " name="final_attachment" id="final_attachment" multiple placeholder="المرفقات :"  style="width:20%; float:right; margin-right:5px;"/>
                           <button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float:left; margin-bottom: 8px; " onClick="add_internal_final_report_muzi('0');" >اضافة</button>
                           <br/>
                           <textarea class="form-control" value="" placeholder="البيان" name="final_statement" id="final_statement" rows="5" style="margin-top:5px;" ></textarea>
                          
                           </td>

                         </tr> 

                          <input type="hidden"  name="final_extreliefId" id="final_extreliefId" value="<?php if(isset($rows->urgentaidId) and $rows->urgentaidId >0){echo $rows->urgentaidId;} else {echo '0';} ?>" />
                          <input type="hidden"  name="autoid_3" id="autoid_3" value="<?php echo $extreliefTMId; ?>" />
                        </form>   
                       </tbody>
                     </table> 

                   </div>
                 </div>
               </div>
        <?php //}?> 
            
        <div class="panel panel-default">
  <div class="panel-heading" role="tab" id="headingTen">
    <h4 class="panel-title">
      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
        <i class="more-less glyphicon  glyphicon-minus "></i>
        قرار لجنة الإغاثة النهائي
      </a>
    </h4>
  </div>
  
  <div id="collapseTen" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTen">
    <div class="panel-body">
     <table class="table table-bordered table-striped " id="final_internal_relief_committee_table">
      <thead style="background-color: #029625;">
        <tr role="row" style="color:#fff !important;">
          <th align="center">رقم محضر الاجتماع</th>
          <th align="center">التاريخ</th>
          <th align="center">قرار اللجنة</th>
          <th align="center">الملاحظات</th>
          <th align="center">الإجراءات</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $extreliefTMId =0;
        if(isset($ah_internal_final_relief_committee_meeting)){
          if(count($ah_internal_final_relief_committee_meeting)>0){
            foreach($ah_internal_final_relief_committee_meeting as $reliefteam) { 
              $extreliefTMId ++;
              $action ='';
              $action .= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_final_internal_relief_committee_meeting_muzi/'.$reliefteam->extrelMeetingId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a> ';
		if($reliefteam->final_releif_attachemnt)
		{
				$action	.=	' <i href="'.base_url().'resources/aid/'.$reliefteam->userid.'/'.$reliefteam->final_releif_attachemnt.'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "></i> ';
		}
             // if($reliefteam->userid == $login_userid){
			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_7/'.$reliefteam->extrelMeetingId.'" id="'.$reliefteam->extrelMeetingId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
              //}
              
             // if($permissions_d== 1) 
             // {
                $action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->extrelMeetingId.'" data-url="'.base_url().'aid/delete_internal_final_relief_committee_meeting/'.$reliefteam->extreliefId.'/'.$reliefteam->extrelMeetingId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
              //}
              
              ?>
              <tr  id="<?php echo $reliefteam->extrelMeetingId;?>_meeting">
                <td id="<?php echo $reliefteam->extrelMeetingId;?>_meeting_minutes"><?php echo $reliefteam->minutes;?></td>
                <td id="<?php echo $reliefteam->extrelMeetingId;?>_meeting_currentdate"><?php echo $reliefteam->currentdate;?></td>
                <td id="<?php echo $reliefteam->extrelMeetingId;?>_meeting_decision"><?php echo $this->haya_model->limit($reliefteam->decision, '50');?></td>
                <td id="<?php echo $reliefteam->extrelMeetingId;?>_meeting_notes"><?php echo $this->haya_model->limit($reliefteam->notes, '50');?></td>
                <td><?php echo $action;?></td>
              </tr> 
              <?php }
              ?>
              <script type="text/javascript">progress = progress + 11; setprogressbar(progress);</script>
              <?php
            } } $extreliefTMId ++;?> 
            <form action="" method="POST" id="final_internal_relief_committee" name="final_internal_relief_committee" autocomplete="off" enctype="multipart/form-data">
            <tr>
              
             <td colspan="5">
             
              <input type="text" class="form-control  " value="" placeholder="رقم محضر الاجتماع" name="minutes" id="final_relief_minutes" style="float:right; margin-left:5px; width:25%" />

              <input type="text" class="form-control  datepicker" value="" placeholder="التاريخ" name="currentdates" id="final_relief_currentdates" style="float:right; margin-left:5px; width:25%" />
                       <input type="file" class="form-control " name="final_releif_attachemnt" id="final_releif_attachemnt" placeholder="المرفقات :" style="float:right; margin-left:5px; width:25%" />

              <button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px; " onClick="add_internal_final_relief_committee_meeting_muzi('0');">اضافة</button>

              <textarea class="form-control  "  placeholder="قرار اللجنة" name="decision" id="final_relief_decision"  style="float:right; margin-top:5px;" rows="5" ></textarea>
              <textarea class="form-control  " placeholder="الملاحظات" name="notes" id="final_relief_notes" style="float:right; margin-top:5px;" rows="5" ></textarea>

                            <input type="hidden"  name="new_extreliefId" id="new_extreliefId" value="<?php if(isset($rows->urgentaidId) and $rows->urgentaidId >0){echo $rows->urgentaidId;} else {echo '0';} ?>" />

                 </td>
                 
               </tr>  
               </form>  
             </tbody>
           </table>
         </div>
       </div>
     </div>
        
		<?php }?>
    </div><!-- panel-group -->
    
    
        
        
        
        
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
<script>
$(function(){
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
	
	<?php if($rows->urgentaidId == null):?>
	loadprovince(200,'province');
	<?php endif;?>
	
	<?php if($rows->urgentaidId): ?>
		editloaddata();
	<?php endif;?>
});
function loadprovince(vals,id){
	$('#'+id).empty();
	$.ajax({
		url: config.BASE_URL+'aid/loadprovince/',
		type: "POST",
		data:{'vals':vals },
		dataType: "json",
		success: function(data)
		{	
			var al =data.province;
			<!--<option value="">--تحديد--</option>-->
			$('#'+id).append('<option value="">--تحديد--</option>');
			for(var i=0; i	<	al.length; i++)
			{
				$('#'+id).append('<option value="'+al[i].list_id+'">'+al[i].list_name+'</option>');
				
			}
		}
	});
}
function loaddata(vals){
	if(vals == '1752'){
		$('.Corporation').css('display','block');
		$('.NonCorporation').css('display','none');
		
	}
	else{
		$('.Corporation').css('display','none');
		$('.NonCorporation').css('display','block');
	}
}
function editloaddata(){
	var vals = $( "#beneficiary option:selected" ).val();
	if(vals == '1752'){
		$('.Corporation').css('display','block');
		$('.NonCorporation').css('display','none');
		
	}
	else{
		$('.Corporation').css('display','none');
		$('.NonCorporation').css('display','block');
	}
}

/**************************Muzafafr Work Start*************************************/
function add_internal_relief_committee_meeting_muzi(extrelMeetingId)
{
	var fd = new FormData(document.getElementById("committee_meeting"));

		$.ajax({
			url: config.BASE_URL+'aid/internal_committee_meeting_addId_muzi/',
			type: "POST",
			data:fd,
			enctype: 'multipart/form-data',
		  	dataType: "html",
		  	processData: false,  // tell jQuery not to process the data
          	contentType: false ,  // tell jQuery not to set contentType
			success: function(response)
			{	
				$('#minutes').val('');
				$('#currentdates').val('');
				$('#aid_attachemnt').val('');
				$('#decision').val('');
				$('#notes').val('');
				//progress = progress + 11; 
				//setprogressbar(progress);
				$('#internal_relief_committee_meeting_addId tr:last').before(response);
      }
    });
}
/***************************************************************/
function add_internal_reliefteam(extreliefTMId){
		$.ajax({
			url: config.BASE_URL+'aid/add_internal_reliefteam/',
			type: "POST",
			data:{'new_name':$('#new_name').val(),'organization':$('#organization').val(),'job':$('#job').val(),'extreliefTMIdauto':$('#extreliefTMIdauto').val(),'extreliefId':$('#urgentaidId').val(),'extreliefTMId':extreliefTMId,'autoid_1':$('#autoid_1').val()},
			dataType: "json",
			success: function(response)
			{	
				$('#new_name').val('');
				$('#organization').val('');
				$('#job').val('');
				var autoid	=	parseInt($("#autoid_1").val())+1;
				$('#autoid_1').val(autoid);
				var extreliefTMIdauto = $('#extreliefTMIdauto').val();
				extreliefTMIdauto = parseInt(extreliefTMIdauto) +  1;
				$('#extreliefTMIdauto').val(extreliefTMIdauto);
				//if(extreliefTMIdauto == 2){progress = progress + 11; setprogressbar(progress);}
				$('#internal_reliefteam_addId tr:last').before(response.data);
      }
    });
}
/***************************************************************/
function add_internal_requirements_required(requirementsId){
		$.ajax({
			url: config.BASE_URL+'aid/add_internal_requirements_required/',
			type: "POST",
			data:{'typeOfMaterial':$('#typeOfMaterial').val(),'materials':$('#materials').val(),'requirementsIdauto':$('#requirementsIdauto').val(),'budgeting':$('#extbudgeting').val(),'quantity':$('#quantity').val(),'requirementsId':requirementsId,'extreliefId':$('#urgentaidId').val(),'autoid_2':$('#autoid_2').val()},
			dataType: "json",
			success: function(response)
			{	
				$('#typeOfMaterial').val('');
				$('#materials').val('');
				$('#extbudgeting').val('');
				$('#quantity').val('');
				
				var autoid	=	parseInt($("#autoid_2").val())+1;
				$('#autoid_2').val(autoid);
				
				var requirementsIdauto = $('#requirementsIdauto').val();
				requirementsIdauto = parseInt(requirementsIdauto) + 1;
				$('#requirementsIdauto').val(requirementsIdauto);
				//if(requirementsIdauto == 2){progress = progress + 11; setprogressbar(progress);}
				$('#internal_requirements_required_addId tr:last').before(response.data);
      }
    });
}
/***************************************************************/
function add_internal_expenses(extExpencesId){

		var form_data = new FormData($('#frm_internal_expenses_addId')[0]);
		var ins = document.getElementById('attachment1').files.length;
		for (var x = 0; x < ins; x++) {
			form_data.append("files[]", document.getElementById('attachment1').files[x]);
		} 
		$.ajax({
			url: config.BASE_URL+'aid/add_internal_expenses/',
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			dataType: "json",
			success: function(response)
			{	
				$('#prose').val('');
				$('#food').val('');
				$('#notes_3').val('');
				$('#reliefArticles').val('');
				$('#medicalMaterials').val('');
				$('#other').val('');
				$('#total').val('');
				var autoid	=	parseInt($("#autoid_5").val())+1;
				$('#autoid_5').val(autoid);
				
				var extExpencesIdauto = $('#extExpencesIdauto').val();
				extExpencesIdauto = parseInt(extExpencesIdauto) + 1;
				$('#extExpencesIdauto').val(extExpencesIdauto);
				//if(extExpencesIdauto == 2){progress = progress + 11; setprogressbar(progress);}
				$('#internal_expenses_addId tr:last').before(response.data);
				return false;
      }
    });
}
/***************************************************************/
function add_internal_daily_report_muzi(extDataReportId)
{
	var fd = new FormData(document.getElementById("internal_reliefteam"));

		$.ajax({
			url: config.BASE_URL+'aid/add_internal_daily_report_muzi/',
			type: "POST",
			data:fd,
			enctype: 'multipart/form-data',
		  	dataType: "html",
		  	processData: false,  // tell jQuery not to process the data
          	contentType: false ,  // tell jQuery not to set contentType
			success: function(response)
			{	
				$('#statement_2').val('');
				$('#currentdaterep').val('');
				$('#report_attachment').val('');
				var autoid	=	parseInt($("#autoid_4").val())+1;
				$('#autoid_4').val(autoid);
				
				var extDataReportIddauto = $('#extDataReportIddauto').val();
				extDataReportIddauto = parseInt(extDataReportIddauto) + 1;
				$('#extDataReportIddauto').val(extDataReportIddauto);
				
				if(extDataReportIddauto == 2)
				{
					//progress = progress + 11; setprogressbar(progress);
				}
				$('#internal_daily_report_addId tr#last_daily').before(response);
      }
    });
}
/***************************************************************/
function add_internal_final_report_muzi(extDataReportId)
{
	var fd = new FormData(document.getElementById("the_internal_final_report"));

		$.ajax({
			url: config.BASE_URL+'aid/add_internal_final_report_muzi/',
			type: "POST",
			data:fd,
			enctype: 'multipart/form-data',
		  	dataType: "html",
		  	processData: false,  // tell jQuery not to process the data
          	contentType: false ,  // tell jQuery not to set contentType
			success: function(response)
			{	
				$('#final_statement').val('');
				$('#final_currentdaterep').val('');
				$('#final_attachment').val('');
				$('#final_percent').val('');
				var autoid	=	parseInt($("#autoid_3").val())+1;
				$('#autoid_3').val(autoid);
				
				var extDataReportIddauto = $('#final_extDataReportIddauto').val();
				extDataReportIddauto = parseInt(extDataReportIddauto) + 1;
				$('#final_extDataReportIddauto').val(extDataReportIddauto);
				if(extDataReportIddauto == 2)
				{
					//progress = progress + 11; setprogressbar(progress);
				}
				$('#internal_daily_final_report tr#last_daily').before(response);
      }
    });
}
/***************************************************************/
function add_internal_final_relief_committee_meeting_muzi(extrelMeetingId)
{
	var fd = new FormData(document.getElementById("final_internal_relief_committee"));

		$.ajax({
			url: config.BASE_URL+'aid/add_internal_final_relief_committee_meeting_muzi/',
			type: "POST",
			data:fd,
			enctype: 'multipart/form-data',
		  	dataType: "html",
		  	processData: false,  // tell jQuery not to process the data
          	contentType: false ,  // tell jQuery not to set contentType
			success: function(response)
			{	
				$('#final_relief_minutes').val('');
				$('#final_relief_currentdates').val('');
				$('#final_relief_decision').val('');
				$('#final_relief_notes').val('');
				$('#final_releif_attachemnt').val('');
				//progress = progress + 11; 
				//setprogressbar(progress);
				$('#final_internal_relief_committee_table tr:last').before(response);
      }
    });
}
/***************************************************************/	
function setprogressbar(vals){
	$( "#progressbar" ).progressbar({
   value: vals
 });
	$('#displayper').html(vals+'%');
}
/**************************Muzafafr Work End*************************************/
</script>
<script>
function loadexpences(urgentaidId){
	$.ajax({
			url: config.BASE_URL+'aid/loadexpences/',
			type: "POST",
			data:{'urgentaidId':urgentaidId },
			dataType: "json",
			success: function(response)
			{	
				$('#listingexpences').html(response.data);
			 }
		});	
}

function loadmeeting(urgentaidId,section){
	$.ajax({
			url: config.BASE_URL+'aid/loadmeeting/',
			type: "POST",
			data:{'urgentaidId':urgentaidId,'section':section},
			dataType: "json",
			success: function(response)
			{	
				$('#listallmeeting'+section).html(response.data);
			 }
		});	
}

 $(document).ready(function(){
	$('#save_service').click(function () {
		check_my_session();
        $('#form_urgent .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_urgent .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_urgent .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                $("#form_urgent").submit();
            
        }
        else 
		{
            show_notification_error(ht);
        }
    });
	<?php if($rows->urgentaidId){ ?>
	loadexpences('<?php echo $rows->urgentaidId ?>');
	loadmeeting('<?php echo $rows->urgentaidId ?>','1');
	loadmeeting('<?php echo $rows->urgentaidId ?>','2');
	<?php } ?>
	
	
});
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
function delete_data_system()
{
	
	check_my_session();
	
    var data_id 	=	$('#delete_id').val();
    var action_url 	=	$('#action_url').val();
    var rowid 		=	'#' + data_id + '_durar_lm';
	
	
    var delete_data_from_system = $.ajax({
        url: action_url,
        dataType: "html",
        success: function (msg) 
		{
            //$('#tableSortable').dataTable().fnDestroy();
            show_notification('لقد تم حذف سجل');
            $('#deleteDiag').modal('hide');
			window.location.reload();
            //$(rowid).slideUp('slow');
        }
    });
}

</script>
<?php 
$permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));
?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
      <div class="col-md-12">
    
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <i class="more-less glyphicon  glyphicon-minus "></i>
                        تفاصيل
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                   <form method="POST" id="form_urgent" name="form_urgent" enctype="multipart/form-data"  >
           <input type="hidden"  name="extreliefId" id="extreliefId" value="<?php if(isset($extreliefId) and $extreliefId >0){echo $extreliefId;} else {echo '0';} ?>" />
            <input type="hidden"  name="extreliefDate" id="extreliefDate" value="<?php echo $extreliefDate?>" />
            <div class="row">
            	<div class="col-md-6">
                	<div class="form-group col-md-4">
                  <label for="basic-input"><strong>التسلسل  :</strong></label>
                  <input type="text" class="form-control " readonly value="<?php if($rows->extreliefId >0){echo $rows->extreliefId;}else{ echo $count;}?>" placeholder="التسلسل" name="autoid" id="autoid" />
                </div>
                <div class="form-group col-md-4">
                  <label for="basic-input"><strong>التاريخ :</strong></label>
                  
                  <input type="text" class="form-control req datepicker" value="<?php echo $controller->dateformat($rows->currentdate)?>" placeholder="التاريخ " name="currentdate" id="currentdate" />
                </div>
                <div class="form-group col-md-4">
                  <label for="basic-input"><strong>الإغاثة:</strong></label>
                  <input type="text" class="form-control req " value="<?php echo $rows->relief?>" placeholder="الإغاثة" name="relief" id="relief" onBlur="addexternal_relief();" />
                </div>
                <div class="form-group col-md-4">
                  <label for="basic-input"><strong>نوع الإغاثة:</strong></label>
                  <select class="form-control req" id="typeOfRelief" name="typeOfRelief" >
                   <option value="">--تحديد--</option>
                          <?php 
                          if(count($reliefType)>0){
                              foreach($reliefType as $type){
                                 if($rows->typeOfRelief == $type->list_id)
                                  {
                                      echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
                                  }
                                  else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
                                  }
                                  
                              }
                          }
                          ?>
                          </select>
                </div>
                 <div class="form-group col-md-4">
                  <label for="basic-input"><strong>الأسباب:</strong></label>
                  <input type="text" class="form-control req " value="<?php echo $rows->reasons?>" placeholder="الأسباب" name="reasons" id="reasons" />
                </div>
                <div class="form-group col-md-4">
                  <label for="basic-input"><strong>الدفعة:</strong></label>
                  <input type="text" class="form-control req " value="<?php echo $rows->batch?>" placeholder="الدفعة" name="batch" id="batch" />
                </div>
                <div class="form-group col-md-4">
                      <label for="basic-input"><strong>الموازنة:</strong></label>
                      <input type="number" class="form-control req " value="<?php echo $rows->budgeting?>" placeholder="الموازنة" name="budgeting" id="budgeting" />
                    </div>
                    <div class="form-group col-md-4">
                      <label for="basic-input"><strong>التحويل:</strong></label>
                      <input type="number" class="form-control req " value="<?php echo $rows->transfer?>" placeholder="التحويل" name="transfer" id="transfer" />
                    </div>
                    <div class="form-group col-md-4">
                      <label for="basic-input"><strong>التوجيهات:</strong></label>
                      <input type="text" class="form-control req " value="<?php echo $rows->directions?>" placeholder="التوجيهات" name="directions" id="directions" />
                    </div>
                    
                    <div class="form-group col-md-12">
                      <label for="basic-input"><strong>الخطة:</strong></label>
                      <textarea class="form-control req "  placeholder="الخطة" name="plan" id="plan" rows="10"><?php echo $rows->plan?></textarea>
                    </div>
                    <div class="form-group col-md-12">
                    <h4 class="yateem_h4">التقرير اليومي
                          
                    </h4>
                        <table class="table table-bordered table-striped "  id="external_daily_report_addId" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                    <th width="10%">رقم</th>
                                    <th  width="20%">التاريخ</th>
                                    <th  width="50%">البيان</th>
                                    <th  width="20%">الإجراءات</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							$extreliefTMId =0;
							if(isset($external_daily_report)){
								if(count($external_daily_report)>0){
									foreach($external_daily_report as $reliefteam) { 
									$extreliefTMId ++;
										$action ='';
										$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_daily_report/'.$reliefteam->extDataReportId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
											if($reliefteam->userid == $login_userid){
												$action	.= ' <a onclick="edit_external_daily_report(\''.$reliefteam->extDataReportId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
											}
											if($permissions_d==	1) 
											{
												$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->extrelMeetingId.'" data-url="'.base_url().'aid/delete_external_daily_report/'.$reliefteam->extreliefId.'/'.$reliefteam->extDataReportId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
											}
									
									?>
                                <tr id="<?php echo $reliefteam->extDataReportId;?>_daily">
                                    <td id="<?php echo $reliefteam->extDataReportId;?>_daily_id"><?php echo $extreliefTMId;?></td>
                                    <td id="<?php echo $reliefteam->extDataReportId;?>_daily_currentdate"><?php echo $reliefteam->currentdate;?></td>
                                    <td id="<?php echo $reliefteam->extDataReportId;?>_daily_statement"><?php echo $reliefteam->statement;?></td><td><?php echo $action;?></td>
                                </tr> 
                            <?php } } } $extreliefTMId ++;?> 
                             <tr id="last_daily">
                                
                                 <td colspan="4"><input type="text" class="form-control " value="<?php echo $extreliefTMId?>" placeholder="رقم" name="extDataReportIddauto" id="extDataReportIddauto"  readonly style="width:20%; float:right;" /><input type="text" class="form-control  datepicker" value="" placeholder="التاريخ" name="currentdaterep" id="currentdaterep" style="width:60%; float:right; margin-right:5px;"  />
                                 <button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float:left; margin-bottom: 8px; " onClick="add_external_daily_report('0');" >اضافة</button>
                                <br/>
                                  <textarea class="form-control  " value="" placeholder="البيان" name="statement" id="statement" rows="5" style="margin-top:5px;" ></textarea></td>
                                 
                                </tr>    
                            </tbody>
                        </table>
                    </div>
                    <!--<div class="form-group col-md-12">
                      <label for="basic-input"><strong>التقرير النهائي:</strong></label>
                      <textarea class="form-control req "  placeholder="التقرير النهائي" name="finalReport" id="finalReport" rows="5"><?php echo $rows->finalReport?></textarea>
                    </div>-->
                    <div class="form-group col-md-12">
              <label for="basic-input"><strong>المرفقات :</strong></label>
              <input type="file" class="form-control" name="attachment[]" id="attachment" multiple /><br/>
               <input type="hidden"  name="attachment_old" id="attachment_old" value="<?php echo $rows->attachment?>" />
               <?php if($rows->attachment !=""){
				   $i=0;
				   $attachment = @explode(',',$rows->attachment);
				   foreach($attachment as $att){
					   if($att !=""){
						   $i++;
				   ?>
                   <div id="picture_<?php echo $i;?>" style="float:right;">
               <a class="fancybox-button" rel="gallery1" href="<?php echo base_url()?>resources/services/<?php echo $login_userid."/".$att?>"><img src="<?php echo base_url()?>resources/services/<?php echo $login_userid."/".$att?>" style="width:50px;" alt="image" title="image"/></a>
              <a class="iconspace" href="#deleteDiag" onClick="show_delete_dialogs(this,'picture_<?php echo $i;?>','<?php echo $att?>');" id="<?php echo  $rows->serviceid ?>" data-url="<?php echo base_url()?>services/removeimage/<?php echo $rows->serviceid ?>/<?php echo $att ?>"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>
              </div>
              <?php } } }?>
            
                    </div>
                    
                    
                
                </div>
                <div class="col-md-6">
                    <div class="form-group col-md-12">
                    <h4 class="yateem_h4">أعضاء فريق الاغاثة
                          
                    </h4>
                        <table class="table table-bordered table-striped "  id="external_reliefteam_addId"  >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                    <th width="15%">رقم</th>
                                    <th width="35%">الاسم</th>
                                    <th  width="35%">الجهة</th>
                                    <th  width="15%">الإجراءات</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							$extreliefTMId =0;
							if(isset($external_reliefteam)){
								if(count($external_reliefteam)>0){
									foreach($external_reliefteam as $reliefteam) { 
									$extreliefTMId ++;
										$action ='';
											$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_reliefteam/'.$reliefteam->extreliefTMId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
											if($reliefteam->userid == $login_userid){
												$action	.= ' <a onclick="edit_external_reliefteam(\''.$reliefteam->extreliefTMId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
											}
											
											
											if($permissions_d==	1) 
											{
												$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->urgentaidexpId.'" data-url="'.base_url().'aid/delete_external_reliefteam/'.$reliefteam->extreliefId.'/'.$reliefteam->extreliefTMId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
											}
									
									?>
                                <tr id="<?php echo $reliefteam->extreliefTMId;?>_team">
                                    <td id="<?php echo $reliefteam->extreliefTMId;?>_team_id"><?php echo $extreliefTMId; ?></td>
                                    <td id="<?php echo $reliefteam->extreliefTMId;?>_team_name"><?php echo $reliefteam->name;?></td>
                                    <td id="<?php echo $reliefteam->extreliefTMId;?>_team_organization"><?php echo $reliefteam->organization;?></td><td><?php echo $action;?></td>
                                </tr> 
                            <?php } } } $extreliefTMId ++;?> 
                             <tr>
                                 <td><input type="text" class="form-control " value="<?php echo $extreliefTMId?>" placeholder="رقم" name="extreliefTMIdauto" id="extreliefTMIdauto"  readonly /></td>
                                 <td><input type="text" class="form-control  " value="" placeholder="الاسم" name="name" id="name" /></td>
                                 <td><input type="text" class="form-control  " value="" placeholder="الجهة" name="organization" id="organization" /></td>
                                 <td><button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px; " onClick="add_external_reliefteam('0');">اضافة</button></td>
                                </tr>    
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group col-md-12">
                    <h4 class="yateem_h4">الاحتياجات المطلوبة
                         
                    </h4>
                        <table class="table table-bordered table-striped "   id="external_requirements_required_addId" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                    <th width="10%">رقم</th>
                                    <th width="20%">نوع المواد</th>
                                    <th width="20%">المواد</th>
                                    <th width="20%">الموازنة</th>
                                    <th width="15%">الكمية</th>
                                    <th width="15%">الإجراءات</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							$extreliefTMId =0;
							if(isset($external_requirements_required)){
								if(count($external_requirements_required)>0){
									foreach($external_requirements_required as $reliefteam) { 
									$extreliefTMId ++;
											$action ='';
											$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_requirements_required/'.$reliefteam->requirementsId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
											if($reliefteam->userid == $login_userid){
												$action	.= ' <a onclick="edit_requirements_required(\''.$reliefteam->requirementsId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
											}
											
											if($permissions_d==	1) 
											{
												$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->requirementsId.'" data-url="'.base_url().'aid/delete_requirements_required/'.$reliefteam->extreliefId.'/'.$reliefteam->requirementsId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
											}
									
									?>
                                <tr id="<?php echo $reliefteam->requirementsId;?>_required">
                                    <td id="<?php echo $reliefteam->requirementsId;?>_required_id"><?php echo $extreliefTMId; ?></td>
                                    <td id="<?php echo $reliefteam->requirementsId;?>_required_typeOfMaterial"><?php echo $reliefteam->typeOfMaterial;?></td>
                                    <td id="<?php echo $reliefteam->requirementsId;?>_required_materials"><?php echo $reliefteam->materials;?></td>
                                    <td id="<?php echo $reliefteam->requirementsId;?>_required_budgeting"><?php echo $reliefteam->budgeting;?></td>
                                    <td id="<?php echo $reliefteam->requirementsId;?>_required_quantity"><?php echo $reliefteam->quantity;?></td><td><?php echo $action;?></td>
                                </tr> 
                            <?php } } } $extreliefTMId ++;?> 
                             <tr>
                                 <td><input type="text" class="form-control " value="<?php echo $extreliefTMId?>" placeholder="رقم" name="requirementsIdauto" id="requirementsIdauto"  readonly /></td>
                                 <td><input type="text" class="form-control  " value="" placeholder="نوع المواد" name="typeOfMaterial" id="typeOfMaterial" /></td>
                                 <td><input type="text" class="form-control " value="" placeholder="المواد" name="materials" id="materials" /></td>
                                  <td><input type="text" class="form-control  " value="" placeholder="الموازنة" name="extbudgeting" id="extbudgeting" /></td>
                                  <td><input type="number" class="form-control  " value="" placeholder="الكمية" name="quantity" id="quantity" /></td>
                                 <td><button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px; " onClick="add_external_requirements_required('0');">اضافة</button></td>
                                </tr>    
                            </tbody>
                        </table>
                    </div>
   					<div class="form-group col-md-12">
                        <h4 class="yateem_h4">المصروفات
                             
                        </h4>
                            <table class="table table-bordered table-striped " id="external_expenses_addId" >
                                <thead style="background-color: #029625;">
                                    <tr role="row" style="color:#fff !important;">
                                        <th width="10%">التسلسل</th>
                                        <th width="10%">النثرية</th>
                                        <th width="10%">المواد الغذائية</th>
                                        <th width="10%">المواد الاغاثية</th>
                                        <th width="15%">المواد الطبية</th>
                                        <th width="15%">اخرى</th>
                                        <th width="15%">الاجمالي</th>
                                        <th width="15%">الإجراءات</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
							$extreliefTMId =0;
							if(isset($external_expenses)){
								if(count($external_expenses)>0){
									foreach($external_expenses as $reliefteam) { 
									$extreliefTMId ++;
										$action ='';
											$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_expenses/'.$reliefteam->extExpencesId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
											if($reliefteam->userid == $login_userid){
												$action	.= ' <a onclick="edit_external_expenses(\''.$reliefteam->extExpencesId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
											}
											
											if($permissions_d==	1) 
											{
												$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->extExpencesId.'" data-url="'.base_url().'aid/delete_external_expenses/'.$reliefteam->extreliefId.'/'.$reliefteam->extExpencesId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
											}
									
									?>
                                <tr  id="<?php echo $reliefteam->extExpencesId;?>_expenses">
                                    <td  id="<?php echo $reliefteam->extExpencesId;?>_expenses_id"><?php echo $extreliefTMId; ?></td>
                                    <td  id="<?php echo $reliefteam->extExpencesId;?>_expenses_prose"><?php echo $reliefteam->prose;?></td>
                                    <td  id="<?php echo $reliefteam->extExpencesId;?>_expenses_food"><?php echo $reliefteam->food;?></td>
                                    <td id="<?php echo $reliefteam->extExpencesId;?>_expenses_reliefArticles"><?php echo $reliefteam->reliefArticles;?></td>
                                    <td id="<?php echo $reliefteam->extExpencesId;?>_expenses_medicalMaterials"><?php echo $reliefteam->medicalMaterials;?></td>
                                    <td id="<?php echo $reliefteam->extExpencesId;?>_expenses_other"><?php echo $reliefteam->other;?></td>
                                    <td id="<?php echo $reliefteam->extExpencesId;?>_expenses_total"><?php echo $reliefteam->total;?></td>
                                    <td><?php echo $action;?></td>
                                </tr> 
                            <?php } } } $extreliefTMId ++;?> 
                             <tr>
                                 <td><input type="text" class="form-control " value="<?php echo $extreliefTMId?>" placeholder="التسلسل" name="extExpencesIdauto" id="extExpencesIdauto"  readonly /><br/>
                                 <button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px; " onClick="add_external_expenses('0');">اضافة</button></td>
                                 <td colspan="3"><input type="number" class="form-control" value="" placeholder="النثرية" name="prose" id="prose" />
                                 <br/>
                                 <input type="number" class="form-control  " value="" placeholder="المواد الطبية" name="medicalMaterials" id="medicalMaterials" />
                                 </td>
                                 <td colspan="2"><input type="number" class="form-control  " value="" placeholder="المواد الغذائية" name="food" id="food" />
                                 <br/>
                                 <input type="number" class="form-control  " value="" placeholder="اخرى" name="other" id="other" />
                                 </td>
                                  <td colspan="2"><input type="number" class="form-control  " value="" placeholder="المواد الاغاثية" name="reliefArticles" id="reliefArticles" />
                                  <br/>
                                  <input type="number" class="form-control  " value="" placeholder="الاجمالي" name="total" id="total" /></td>
                                </tr>    
                            </tbody>
                            </table>
                        </div>
                    <div class="form-group col-md-12">
                        <h4 class="yateem_h4">اجتماع لجنة الإغاثة
                        </h4>
                            <table class="table table-bordered table-striped " id="external_relief_committee_meeting_addId"  >
                                <thead style="background-color: #029625;">
                                    <tr role="row" style="color:#fff !important;">
                                        <th  width="20%">رقم محضر الاجتماع</th>
                                        <th  width="20%">التاريخ</th>
                                        <th  width="20%">قرار اللجنة</th>
                                        <th  width="20%">الملاحظات</th>
                                        <th width="20%">الإجراءات</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
							$extreliefTMId =0;
							if(isset($external_relief_committee_meeting)){
								if(count($external_relief_committee_meeting)>0){
									foreach($external_relief_committee_meeting as $reliefteam) { 
									$extreliefTMId ++;
										$action ='';
											$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_relief_committee_meeting/'.$reliefteam->extrelMeetingId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
											if($reliefteam->userid == $login_userid){
												$action	.= ' <a onclick="edit_external_relief_committee_meeting(\''.$reliefteam->extrelMeetingId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
											}
											
											if($permissions_d==	1) 
											{
												$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$reliefteam->extrelMeetingId.'" data-url="'.base_url().'aid/delete_external_relief_committee_meeting/'.$reliefteam->extreliefId.'/'.$reliefteam->extrelMeetingId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
											}
									
									?>
                                <tr  id="<?php echo $reliefteam->extrelMeetingId;?>_meeting">
                                    <td id="<?php echo $reliefteam->extrelMeetingId;?>_meeting_minutes"><?php echo $reliefteam->minutes;?></td>
                                    <td id="<?php echo $reliefteam->extrelMeetingId;?>_meeting_currentdate"><?php echo $reliefteam->currentdate;?></td>
                                    <td id="<?php echo $reliefteam->extrelMeetingId;?>_meeting_decision"><?php echo $reliefteam->decision;?></td>
                                    <td id="<?php echo $reliefteam->extrelMeetingId;?>_meeting_notes"><?php echo $reliefteam->notes;?></td>
                                    <td><?php echo $action;?></td>
                                </tr> 
                            <?php } } } $extreliefTMId ++;?> 
                             <tr>
                                
                                 <td colspan="5"><input type="text" class="form-control  " value="" placeholder="رقم محضر الاجتماع" name="minutes" id="minutes" style="float:right; margin-left:5px; width:40%" /><input type="text" class="form-control  datepicker" value="" placeholder="التاريخ" name="currentdates" id="currentdates" style="float:right; margin-left:5px; width:40%" /><button type="button" id="btn_external_reliefteam" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px; " onClick="add_external_relief_committee_meeting('0');">اضافة</button>
                                 <textarea class="form-control  "  placeholder="قرار اللجنة" name="decision" id="decision"  style="float:right; margin-top:5px;" rows="5" ></textarea>
                                 <textarea class="form-control  " placeholder="الملاحظات" name="notes" id="notes" style="float:right; margin-top:5px;" rows="5" ></textarea></td>
                                
                                </tr>    
                            </tbody>
                            </table>
                        </div>
                    
                    
               </div> 
            
            
            </div>
            
            <br clear="all">
            <div class="form-group col-md-6">
              <button type="button" id="save_service" name="save_service" class="btn btn-success">حفظ</button>
            </div>
          </form>
                </div>
            </div>
        </div>
        
    </div><!-- panel-group -->
    
    
        
        
        
        
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
<script>
$(function(){
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});
function validation() {
	if ($('#relief').val() == '') {
		   $("#relief").css('border','1px solid rgba(247,32,35,1.00)');
	 }
	 else{
		  $("#relief").css('border','1px solid #029625');
	 }
	if ($('#currentdate').val() == '') {
		   $("#currentdate").css('border','1px solid rgba(247,32,35,1.00)');
	 }
	 else{
		$("#currentdate").css('border','1px solid #029625'); 
	 }
	 return true;
}
function addexternal_relief(){
	if($('#extreliefId').val() <=0){
		if($('#relief').val() !="" && $('#currentdate').val() != ""){
		$.ajax({
			url: config.BASE_URL+'aid/addexternal_relief_load/',
			type: "POST",
			data:{'relief':$('#relief').val(),'currentdate':$('#currentdate').val() },
			dataType: "json",
			success: function(response)
			{	
				$('#extreliefId').val(response.data);
			 }
		});
		}
	}
}
function add_external_reliefteam(extreliefTMId){
	validation();
	if($('#extreliefId').val() > 0){
		$.ajax({
			url: config.BASE_URL+'aid/add_external_reliefteam/',
			type: "POST",
			data:{'name':$('#name').val(),'organization':$('#organization').val(),'extreliefTMIdauto':$('#extreliefTMIdauto').val(),'extreliefId':$('#extreliefId').val(),'extreliefTMId':extreliefTMId},
			dataType: "json",
			success: function(response)
			{	
				$('#name').val('');
				$('#organization').val('');
				var extreliefTMIdauto = $('#extreliefTMIdauto').val();
				extreliefTMIdauto = parseInt(extreliefTMIdauto) +  1;
				$('#extreliefTMIdauto').val(extreliefTMIdauto);
				$('#external_reliefteam_addId tr:last').before(response.data);
			 }
		});
	}
}
function add_external_requirements_required(requirementsId){
	validation();
	if($('#extreliefId').val() > 0){
		$.ajax({
			url: config.BASE_URL+'aid/add_external_requirements_required/',
			type: "POST",
			data:{'typeOfMaterial':$('#typeOfMaterial').val(),'materials':$('#materials').val(),'requirementsIdauto':$('#requirementsIdauto').val(),'budgeting':$('#extbudgeting').val(),'quantity':$('#quantity').val(),'requirementsId':requirementsId,'extreliefId':$('#extreliefId').val()},
			dataType: "json",
			success: function(response)
			{	
				$('#typeOfMaterial').val('');
				$('#materials').val('');
				$('#extbudgeting').val('');
				$('#quantity').val('');
				var requirementsIdauto = $('#requirementsIdauto').val();
				requirementsIdauto = parseInt(requirementsIdauto) + 1;
				$('#requirementsIdauto').val(requirementsIdauto);
				$('#external_requirements_required_addId tr:last').before(response.data);
			 }
		});
	}
}
function add_external_expenses(extExpencesId){
	validation();
	if($('#extreliefId').val() > 0){
		$.ajax({
			url: config.BASE_URL+'aid/add_external_expenses/',
			type: "POST",
			data:{'prose':$('#prose').val(),'food':$('#food').val(),'reliefArticles':$('#reliefArticles').val(),'medicalMaterials':$('#medicalMaterials').val(),'other':$('#other').val(),'extExpencesId':extExpencesId,'extreliefId':$('#extreliefId').val(),'total':$('#total').val(),'extExpencesIdauto':$('#extExpencesIdauto').val()},
			dataType: "json",
			success: function(response)
			{	
				$('#prose').val('');
				$('#food').val('');
				$('#reliefArticles').val('');
				$('#medicalMaterials').val('');
				$('#other').val('');
				$('#total').val('');
				var extExpencesIdauto = $('#extExpencesIdauto').val();
				extExpencesIdauto = parseInt(extExpencesIdauto) + 1;
				$('#extExpencesIdauto').val(extExpencesIdauto);
				$('#external_expenses_addId tr:last').before(response.data);
			 }
		});
	}
}
function add_external_relief_committee_meeting(extrelMeetingId){
	validation();
	if($('#extreliefId').val() > 0){
		$.ajax({
			url: config.BASE_URL+'aid/external_relief_committee_meeting_addId/',
			type: "POST",
			data:{'minutes':$('#minutes').val(),'currentdate':$('#currentdates').val(),'decision':$('#decision').val(),'notes':$('#notes').val(),'extrelMeetingId':extrelMeetingId,'extreliefId':$('#extreliefId').val()},
			dataType: "json",
			success: function(response)
			{	
				$('#minutes').val('');
				$('#currentdates').val('');
				$('#decision').val('');
				$('#notes').val('');
				
				$('#external_relief_committee_meeting_addId tr:last').before(response.data);
			 }
		});
	}
}
function add_external_daily_report(extDataReportId){
	validation();
	if($('#extreliefId').val() > 0){
		$.ajax({
			url: config.BASE_URL+'aid/add_external_daily_report/',
			type: "POST",
			data:{'statement':$('#statement').val(),'currentdate':$('#currentdaterep').val(),'extDataReportIddauto':$('#extDataReportIddauto').val(),'extDataReportId':extDataReportId,'extreliefId':$('#extreliefId').val()},
			dataType: "json",
			success: function(response)
			{	
				$('#statement').val('');
				$('#currentdaterep').val('');
				var extDataReportIddauto = $('#extDataReportIddauto').val();
				extDataReportIddauto = parseInt(extDataReportIddauto) + 1;
				$('#extDataReportIddauto').val(extDataReportIddauto);
				$('#external_daily_report_addId tr#last_daily').before(response.data);
			 }
		});
	}
}

function delete_data_system()
{
	check_my_session();
	
    var data_id 	=	$('#delete_id').val();
    var action_url 	=	$('#action_url').val();
    var rowid 		=	'#' + data_id + '_durar_lm';
	
	
    var delete_data_from_system = $.ajax({
        url: action_url,
        dataType: "json",
        success: function (msg) 
		{
            show_notification('لقد تم حذف سجل');
            $('#deleteDiag').modal('hide');
			$('#'+msg.data).hide('slow');
        }
    });
}
 $(document).ready(function(){
	$('#save_service').click(function () {
		check_my_session();
        $('#form_urgent .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_urgent .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_urgent .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                $("#form_urgent").submit();
            
        }
        else 
		{
            show_notification_error_end(ht);
        }
    });
	
});
function edit_external_daily_report(extDataReportId){
	var id = $('#'+extDataReportId+'_daily_id').html();
	var currentdate = $('#'+extDataReportId+'_daily_currentdate').html();
	var statement = $('#'+extDataReportId+'_daily_statement').html();
	var cont = "<td colspan='4'><input type='text' class='form-control ' value='"+id+"' placeholder='رقم' name='extDataReportIddauto' id='extDataReportId_"+extDataReportId+"'  readonly style='width:20%; float:right;' /><input type='text' class='form-control  datepicker' value='"+currentdate+"' placeholder='التاريخ' name='currentdaterep' id='currentdaterep_"+extDataReportId+"' style='width:60%; float:right; margin-right:5px;'  /><button type='button' id='btn_external_reliefteam' class='btn btn-sm btn-success' style='float:left; margin-bottom: 8px; ' onClick='update_external_daily_report(\""+extDataReportId+"\");' >اضافة</button><button type='button' id='btn_external_reliefteam' class='btn btn-sm btn-success' style='float:left; margin-bottom: 8px; ' onClick='cancel_external_daily_report(\""+extDataReportId+"\",\""+id+"\");' >إلغاء</button><br/><textarea class='form-control  '  placeholder='البيان' name='statement' id='statement_"+extDataReportId+"' rows='5' style='margin-top:5px;' >"+statement+"</textarea></td>";
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
	$('#'+extDataReportId+'_daily').html(cont);
}

function update_external_daily_report(extDataReportId){
	if($('#extreliefId').val() > 0){
		$.ajax({
			url: config.BASE_URL+'aid/add_external_daily_report/',
			type: "POST",
			data:{'statement':$('#statement_'+extDataReportId).val(),'currentdate':$('#currentdaterep_'+extDataReportId).val(),'extDataReportIddauto':$('#extDataReportId_'+extDataReportId).val(),'extDataReportId':extDataReportId,'extreliefId':$('#extreliefId').val()},
			dataType: "json",
			success: function(response)
			{	
				var extDataReportIddauto = $('#extDataReportIddauto').val();
				extDataReportIddauto = parseInt(extDataReportIddauto) + 1;
				$('#'+extDataReportId+'_daily').html(response.data);
			 }
		});
	}
}
function cancel_external_daily_report(extDataReportId,id){
	$.ajax({
			url: config.BASE_URL+'aid/view_row_external_daily_report/',
			type: "POST",
			data:{'extDataReportId':extDataReportId,'id':id},
			dataType: "json",
			success: function(response)
			{	
				$('#'+extDataReportId+'_daily').html(response.data);
			 }
		});
}
//////////////////////
function edit_external_reliefteam(extreliefTMId){
	var id = $('#'+extreliefTMId+'_team_id').html();
	var name = $('#'+extreliefTMId+'_team_name').html();
	var organization = $('#'+extreliefTMId+'_team_organization').html();
	var cont = "<td><input type='text' class='form-control ' value='"+id+"' placeholder='رقم' name='extreliefTMIdauto' id='extreliefTMIdauto_"+extreliefTMId+"'  readonly /></td><td><input type='text' class='form-control  ' value='"+name+"' placeholder='الاسم' name='name' id='name_"+extreliefTMId+"' /></td><td><input type='text' class='form-control  ' value='"+organization+"' placeholder='الجهة' name='organization' id='organization_"+extreliefTMId+"' /></td><td><button type='button' id='btn_external_reliefteam' class='btn btn-sm btn-success' style='float:left; margin-bottom: 8px; ' onClick='update_external_reliefteam(\""+extreliefTMId+"\");' >اضافة</button><button type='button' id='btn_external_reliefteam' class='btn btn-sm btn-success' style='float:left; margin-bottom: 8px; ' onClick='cancel_external_reliefteam(\""+extreliefTMId+"\",\""+id+"\");' >إلغاء</button></td>";
	$('#'+extreliefTMId+'_team').html(cont);
}

function update_external_reliefteam(extreliefTMId){
	if($('#extreliefId').val() > 0){
		$.ajax({
			url: config.BASE_URL+'aid/add_external_reliefteam/',
			type: "POST",
			data:{'name':$('#name_'+extreliefTMId).val(),'organization':$('#organization_'+extreliefTMId).val(),'extreliefTMIdauto':$('#extreliefTMIdauto_'+extreliefTMId).val(),'extreliefTMId':extreliefTMId,'extreliefId':$('#extreliefId').val()},
			dataType: "json",
			success: function(response)
			{	
				$('#'+extreliefTMId+'_team').html(response.data);
			 }
		});
	}
}
function cancel_external_reliefteam(extreliefTMId,id){
	$.ajax({
			url: config.BASE_URL+'aid/view_row_external_reliefteam/',
			type: "POST",
			data:{'extreliefTMId':extreliefTMId,'id':id},
			dataType: "json",
			success: function(response)
			{	
				$('#'+extreliefTMId+'_team').html(response.data);
			 }
		});
}

//////////////////////
function edit_requirements_required(requirementsId){
	var id = $('#'+requirementsId+'_required_id').html();
	var typeOfMaterial = $('#'+requirementsId+'_required_typeOfMaterial').html();
	var materials = $('#'+requirementsId+'_required_materials').html();
	var budgeting = $('#'+requirementsId+'_required_budgeting').html();
	var quantity = $('#'+requirementsId+'_required_quantity').html();
	var cont = "<td><input type='text' class='form-control ' value='"+id+"' placeholder='رقم' name='requirementsIdauto' id='requirementsIdauto_"+requirementsId+"'  readonly /></td><td><input type='text' class='form-control  ' value='"+typeOfMaterial+"' placeholder='نوع المواد' name='typeOfMaterial' id='typeOfMaterial_"+requirementsId+"' /></td><td><input type='text' class='form-control ' value='"+materials+"' placeholder='المواد' name='materials' id='materials_"+requirementsId+"' /></td><td><input type='text' class='form-control  ' value='"+budgeting+"' placeholder='الموازنة' name='extbudgeting' id='extbudgeting_"+requirementsId+"' /></td><td><input type='number' class='form-control  ' value='"+quantity+"' placeholder='الكمية' name='quantity' id='quantity_"+requirementsId+"' /></td><td><button type='button' id='btn_external_reliefteam' class='btn btn-sm btn-success' style='float:left; margin-bottom: 8px; ' onClick='update_requirements_required(\""+requirementsId+"\");' >اضافة</button><button type='button' id='btn_external_reliefteam' class='btn btn-sm btn-success' style='float:left; margin-bottom: 8px; ' onClick='cancel_requirements_required(\""+requirementsId+"\",\""+id+"\");' >إلغاء</button></td>";
	$('#'+requirementsId+'_required').html(cont);
}

function update_requirements_required(requirementsId){
	if($('#extreliefId').val() > 0){
		$.ajax({
			url: config.BASE_URL+'aid/add_external_requirements_required/',
			type: "POST",
			data:{'typeOfMaterial':$('#typeOfMaterial_'+requirementsId).val(),'materials':$('#materials_'+requirementsId).val(),'requirementsIdauto':$('#requirementsIdauto_'+requirementsId).val(),'budgeting':$('#extbudgeting_'+requirementsId).val(),'quantity':$('#quantity_'+requirementsId).val(),'requirementsId':requirementsId,'extreliefId':$('#extreliefId').val()},
			dataType: "json",
			success: function(response)
			{	
				$('#'+requirementsId+'_required').html(response.data);
			 }
		});
	}
}
function cancel_requirements_required(requirementsId,id){
	$.ajax({
			url: config.BASE_URL+'aid/view_row_requirements_required/',
			type: "POST",
			data:{'requirementsId':requirementsId,'id':id},
			dataType: "json",
			success: function(response)
			{	
				$('#'+requirementsId+'_required').html(response.data);
			 }
		});
}


//////////////////////
function edit_external_expenses(extExpencesId){
	var id = $('#'+extExpencesId+'_expenses_id').html();
	var prose = $('#'+extExpencesId+'_expenses_prose').html();
	var food = $('#'+extExpencesId+'_expenses_food').html();
	var reliefArticles = $('#'+extExpencesId+'_expenses_reliefArticles').html();
	var medicalMaterials = $('#'+extExpencesId+'_expenses_medicalMaterials').html();
	var other = $('#'+extExpencesId+'_expenses_other').html();
	var total = $('#'+extExpencesId+'_expenses_total').html();
	var cont = "<td><input type='text' class='form-control ' value='"+id+"' placeholder='التسلسل' name='extExpencesIdauto' id='extExpencesIdauto_"+extExpencesId+"'  readonly /><br/><button type='button' id='btn_external_reliefteam' class='btn btn-sm btn-success' style='float:left; margin-bottom: 8px; ' onClick='update_external_expenses(\""+extExpencesId+"\");' >اضافة</button><button type='button' id='btn_external_reliefteam' class='btn btn-sm btn-success' style='float:left; margin-bottom: 8px; ' onClick='cancel_external_expenses(\""+extExpencesId+"\",\""+id+"\");' >إلغاء</button></td><td colspan='3'><input type='number' class='form-control' value='"+prose+"' placeholder='النثرية' name='prose' id='prose_"+extExpencesId+"' /><br/><input type='number' class='form-control  ' value='"+medicalMaterials+"' placeholder='المواد الطبية' name='medicalMaterials' id='medicalMaterials_"+extExpencesId+"' /></td><td colspan='2'><input type='number' class='form-control  ' value='"+food+"' placeholder='المواد الغذائية' name='food' id='food_"+extExpencesId+"' /><br/><input type='number' class='form-control  ' value='"+other+"' placeholder='اخرى' name='other' id='other_"+extExpencesId+"' /></td><td colspan='2'><input type='number' class='form-control  ' value='"+reliefArticles+"' placeholder='المواد الاغاثية' name='reliefArticles' id='reliefArticles_"+extExpencesId+"' /><br/><input type='number' class='form-control  ' value='"+total+"' placeholder='الاجمالي' name='total' id='total_"+extExpencesId+"' /></td>";
	$('#'+extExpencesId+'_expenses').html(cont);
}

function update_external_expenses(extExpencesId){
	if($('#extreliefId').val() > 0){
		$.ajax({
			url: config.BASE_URL+'aid/add_external_expenses/',
			type: "POST",
			data:{'prose':$('#prose_'+extExpencesId).val(),'food':$('#food_'+extExpencesId).val(),'reliefArticles':$('#reliefArticles_'+extExpencesId).val(),'medicalMaterials':$('#medicalMaterials_'+extExpencesId).val(),'other':$('#other_'+extExpencesId).val(),'extExpencesId':extExpencesId,'extreliefId':$('#extreliefId').val(),'total':$('#total_'+extExpencesId).val(),'extExpencesIdauto':$('#extExpencesIdauto_'+extExpencesId).val()},
			dataType: "json",
			success: function(response)
			{	
				$('#'+extExpencesId+'_expenses').html(response.data);
			 }
		});
	}
}
function cancel_external_expenses(extExpencesId,id){
	$.ajax({
			url: config.BASE_URL+'aid/view_row_external_expenses/',
			type: "POST",
			data:{'extExpencesId':extExpencesId,'id':id},
			dataType: "json",
			success: function(response)
			{	
				$('#'+extExpencesId+'_expenses').html(response.data);
			 }
		});
}
//////////////////////////
function edit_external_relief_committee_meeting(extrelMeetingId){
	
	var minutes = $('#'+extrelMeetingId+'_meeting_minutes').html();
	var currentdate = $('#'+extrelMeetingId+'_meeting_currentdate').html();
	var decision = $('#'+extrelMeetingId+'_meeting_decision').html();
	var notes = $('#'+extrelMeetingId+'_meeting_notes').html();
	var cont = "<td colspan='5'><input type='text' class='form-control  ' value='"+minutes+"' placeholder='رقم محضر الاجتماع' name='minutes' id='minutes_"+extrelMeetingId+"' style='float:right; margin-left:5px; width:40%' /><input type='text' class='form-control  datepicker' value='"+currentdate+"' placeholder='التاريخ' name='currentdates' id='currentdates_"+extrelMeetingId+"' style='float:right; margin-left:5px; width:40%' /><button type='button' id='btn_external_reliefteam' class='btn btn-sm btn-success' style='float:left; margin-bottom: 8px; ' onClick='update_external_relief_committee_meeting(\""+extrelMeetingId+"\");' >اضافة</button><button type='button' id='btn_external_reliefteam' class='btn btn-sm btn-success' style='float:left; margin-bottom: 8px; ' onClick='cancel_external_relief_committee_meeting(\""+extrelMeetingId+"\");' >إلغاء</button><textarea class='form-control  '  placeholder='قرار اللجنة' name='decision' id='decision_"+extrelMeetingId+"'  style='float:right; margin-top:5px;' rows='5' >"+decision+"</textarea><textarea class='form-control  ' placeholder='الملاحظات' name='notes' id='notes_"+extrelMeetingId+"' style='float:right; margin-top:5px;' rows='5' >"+notes+"</textarea></td>";
	$('#'+extrelMeetingId+'_meeting').html(cont);
}

function update_external_relief_committee_meeting(extrelMeetingId){
	if($('#extreliefId').val() > 0){
		$.ajax({
			url: config.BASE_URL+'aid/external_relief_committee_meeting_addId/',
			type: "POST",
			data:{'minutes':$('#minutes_'+extrelMeetingId).val(),'currentdate':$('#currentdates_'+extrelMeetingId).val(),'decision':$('#decision_'+extrelMeetingId).val(),'notes':$('#notes_'+extrelMeetingId).val(),'extrelMeetingId':extrelMeetingId,'extreliefId':$('#extreliefId').val()},
			dataType: "json",
			success: function(response)
			{	
				$('#'+extrelMeetingId+'_meeting').html(response.data);
			 }
		});
	}
}
function cancel_external_relief_committee_meeting(extrelMeetingId){
	$.ajax({
			url: config.BASE_URL+'aid/view_row_external_relief_committee_meeting/',
			type: "POST",
			data:{'extrelMeetingId':extrelMeetingId},
			dataType: "json",
			success: function(response)
			{	
				$('#'+extrelMeetingId+'_meeting').html(response.data);
			 }
		});
}
</script>


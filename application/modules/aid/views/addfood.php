<?php 
$permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));
?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
      <div class="col-md-12">
    
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <i class="more-less glyphicon  glyphicon-minus "></i>
                        تفاصيل
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                   <form method="POST" id="form_urgent" name="form_urgent" enctype="multipart/form-data">
           <input type="hidden"  name="aidfoodId" id="aidfoodId" value="<?php echo $rows->aidfoodId?>" />
            <div class="row">
            <div class="form-group col-md-3">
              <label for="basic-input"><strong>الرقم :</strong></label>
              <input type="text" class="form-control " readonly value="<?php if($rows->aidfoodId >0){echo $rows->aidfoodId;}else{ echo $count;}?>" placeholder="الرقم التسلسلي" name="autoid" id="autoid" />
            </div>
            
             <div class="form-group col-md-3">
              <label for="basic-input"><strong>   نوع الاغاثة:</strong></label>
              <select class="form-control req" id="reliefType" name="reliefType" onChange="relieftypefun(this.value);" >
               <option value="">--تحديد--</option>
                      <?php 
					  if(count($reliefType)>0){
						  foreach($reliefType as $type){
							 if($rows->reliefType == $type->list_id)
							  {
								  echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
							  }
							  else if($Typerelief !=""){
								  if($type->list_id == $Typerelief)
								  {
									  echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
								  }
								  else{
									   echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
								  }
							  }
							  else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
							  }
							  
						  }
					  }
					  ?>
                      </select>
            </div>
             <div class="form-group col-md-3">
              <label for="basic-input"><strong> نوع المستفيد:</strong></label>
              <select class="form-control req" id="beneficiary" name="beneficiary" onChange="loaddata(this.value)" >
                      <?php 
					  if(count($beneficiary)>0){
						  foreach($beneficiary as $type){
							 if($rows->beneficiary == $type->list_id)
							  {
								  echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
							  }
							  else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
							  }
							  
						  }
					  }
					  ?>
                      </select>
            </div>
             
            <div class="form-group col-md-3">
              <label for="basic-input"><strong>التاريخ :</strong></label>
              
              <input type="text" class="form-control req datepicker" value="<?php echo $controller->dateformat($rows->currentdate)?>" placeholder="التاريخ" name="currentdate" id="currentdate" />
            </div>
            <div class="form-group col-md-3">
                      <label for="basic-input"><strong>اسم الجهة المستفيدة :</strong></label>
                      <input type="text" class="form-control req " value="<?php echo $rows->beneficiaryName?>" placeholder="اسم الجهة المستفيدة" name="beneficiaryName" id="beneficiaryName" />
                    </div>
            <div class="NonCorporation" >
                    
            
                    <div class="form-group col-md-3">
                      <label for="basic-input"><strong>العمر :</strong></label>
                      <input type="number" class="form-control  " value="<?php echo $rows->age?>" placeholder="العمر" name="age" id="age" max="3" />
                    </div>
                     <div class="form-group col-md-3">
                      <label for="basic-input"><strong>هاتف :</strong></label>
                      <input type="text" class="form-control  " value="<?php echo $rows->phone?>" placeholder="هاتف" name="phone" id="phone" onKeyUp="only_numeric(this);" />
                    </div>
            </div>
            
             <div class="form-group col-md-3">
              <label for="basic-input"><strong>عنوان المشروع:</strong></label>
              <input type="text" class="form-control req " value="<?php echo $rows->projectTitle?>" placeholder="عنوان المشروع" name="projectTitle" id="projectTitle" />
            </div>
             <div class="form-group col-md-3">
              <label for="basic-input"><strong>عدد المستفيدين:</strong></label>
              <input type="number" class="form-control  " value="<?php echo $rows->beneficiaries?>" placeholder="عدد المستفيدين" name="beneficiaries" id="beneficiaries" onKeyUp="balance_listing('<?php echo $rows->aidfoodId?>');" />
              <br/>
              <label for="basic-input"><strong style="color:#F00;">إجمالي متبقيات للمستفيدين: <span class="balance_listing"></span></strong></label>
            </div>
            <div id="relieftypefun">
             <div class="form-group col-md-3">
              <label for="basic-input"><strong> الدولة :</strong></label>
              <select class="form-control " id="country" name="country" onChange="loadprovince(this.value,'province');" >
                      <option value="">--تحديد--</option>
					  <?php 
                      if(count($issuecountry)>0){
                          foreach($issuecountry as $type){
                             if($rows->country == $type->list_id)
                              {
                                  echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
                              }
                              else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
                              }
                              
                          }
                      }
                      ?>
                      </select>
             </div>
            </div>        
             <div class="form-group col-md-3">
              <label for="basic-input"><strong>حافظة / البلد :</strong></label>
              <select class="form-control " id="province" name="province" onChange="loadprovince(this.value,'city');" >
               <?php 
                      if(count($province)>0){
                          foreach($province as $type){
                             if( array_search( $rows->province,$type->list_id))
                              {
                                  echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
                              }
                              else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
                              }
                              
                          }
                      }
                      ?>
              </select>
            </div>
             <div class="form-group col-md-3">
              <label for="basic-input"><strong> الولايه / المدينة:</strong></label>
              <select class="form-control " id="city" name="city" >
               <?php 
                      if(count($city)>0){
                          foreach($city as $type){
                             if( array_search( $rows->city,$type->list_id))
                              {
                                  echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
                              }
                              else{ echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
                              }
                              
                          }
                      }
                      ?>
              </select>
            </div>
            
            
            </div>
             <div class="row">
                 <div class="form-group col-md-12">
                  <label for="basic-input"><strong>الدعم :</strong></label>
                  <textarea name="support" placeholder="الدعم" class="form-control req"  id="support"><?php echo $rows->support?></textarea>
                </div>
            </div>
            
            <br clear="all">
            <div class="form-group col-md-6">
              <button type="button" id="save_service" name="save_service" class="btn btn-success">حفظ</button>
            </div>
          </form>
                </div>
            </div>
        </div>
        <?php if($rows->aidfoodId>0){ ?>
		<div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="more-less glyphicon  <?php if($rows->aidfoodId > 0){ ?> glyphicon-minus<?php }else{?>glyphicon-plus<?php }?>"></i>
                       تفاصيل الطرد الغذائي
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse <?php if($rows->aidfoodId>0){ ?> in <?php }?>" role="tabpanel "  aria-labelledby="headingOne">
                <div class="panel-body">
                     <div class="row table-header-row" style=" margin-right: 0px;"><a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>aid/addFoodexpulsion/<?php echo $rows->aidfoodId ?>" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">تفاصيل الطرد الغذائي</a> </div>
                        <div  id="aidfood_expulsion"></div>
                         
                          
                     
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <i class="more-less glyphicon <?php if($rows->aidfoodId > 0){ ?> glyphicon-minus<?php }else{?>glyphicon-plus<?php }?>"></i>
                       تفريغ عروض أسعار شراء مواد الطرد الغذائي
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse <?php if($rows->aidfoodId>0){ ?> in <?php }?>" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                     <div class="row table-header-row" style="margin-right: 0px;"><a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>aid/addaidfood_unloading/<?php echo $rows->aidfoodId ?>" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">تفريغ عروض أسعار شراء مواد الطرد الغذائي</a> </div>
                     <div id="listallaidfood_unloading"></div>
                </div>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfour">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                        <i class="more-less glyphicon <?php if($rows->aidfoodId > 0){ ?> glyphicon-minus<?php }else{?>glyphicon-plus<?php }?>"></i>
                       اجتماع اللجنة
                    </a>
                </h4>
            </div>
            <div id="collapsefour" class="panel-collapse collapse <?php if($rows->aidfoodId>0){ ?> in <?php }?>" role="tabpanel" aria-labelledby="headingfour">
                <div class="panel-body">
                     <div class="row table-header-row" style="margin-right: 0px;"><a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>aid/addaidfood_meeting/<?php echo $rows->aidfoodId ?>/1" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">اجتماع اللجنة</a> </div>
                     <div id="listallaidfood_meeting1"></div>
                </div>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFive">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        <i class="more-less glyphicon <?php if($rows->aidfoodId > 0){ ?> glyphicon-minus<?php }else{?>glyphicon-plus<?php }?>"></i>
                       قائمة التوزيع
                    </a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse <?php if($rows->aidfoodId>0){ ?> in <?php }?>" role="tabpanel" aria-labelledby="headingFive">
                <div class="panel-body">
                     <div class="row table-header-row" style="margin-right: 0px;"><a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>aid/addaidfood_distributionlist/<?php echo $rows->aidfoodId ?>" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">قائمة التوزيع</a> </div>
                     <div id="listallloadaidfood_distributionlist"></div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingSeven">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                        <i class="more-less glyphicon <?php if($rows->aidfoodId > 0){ ?> glyphicon-minus<?php }else{?>glyphicon-plus<?php }?>"></i>
                       التكلفة الفعلية
                    </a>
                </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse <?php if($rows->aidfoodId>0){ ?> in <?php }?>" role="tabpanel" aria-labelledby="headingSeven">
                <div class="panel-body">
                     <div class="row table-header-row" style="margin-right: 0px;"><a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>aid/addaidfood_actualcost/<?php echo $rows->aidfoodId ?>" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">التكلفة الفعلية</a> </div>
                     <div id="loadaidfoodactualcost"></div>
                </div>
            </div>
        </div>
       <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingSix">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                        <i class="more-less glyphicon <?php if($rows->aidfoodId > 0){ ?> glyphicon-minus<?php }else{?>glyphicon-plus<?php }?>"></i>
                       اجتماع اللجنة
                    </a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse <?php if($rows->aidfoodId>0){ ?> in <?php }?>" role="tabpanel" aria-labelledby="headingSix">
                <div class="panel-body">
                     <div class="row table-header-row" style="margin-right: 0px;"><a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>aid/addaidfood_meeting/<?php echo $rows->aidfoodId ?>/2" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">اجتماع اللجنة</a> </div>
                     <div id="listallaidfood_meeting2"></div>
                </div>
            </div>
        </div> 
        
        
         
		<?php }?>
    </div><!-- panel-group -->
    
    
        
        
        
        
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
<script>
$(function(){
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
	});
});
function loadprovince(vals,id){
	$('#'+id).empty();
	$('#'+id).append('<option value="">--تحديد--</option>');
	$.ajax({
		url: config.BASE_URL+'aid/loadprovince/',
		type: "POST",
		data:{'vals':vals },
		dataType: "json",
		success: function(data)
		{	
			var al =data.province;
			for(var i=0; i<al.length; i++)
			{
				$('#'+id).append('<option value="'+al[i].list_id+'">'+al[i].list_name+'</option>');
				
			}
		}
	});
}
function loaddata(vals){
	if(vals == '1741'){
		//if(vals == 1752){
		$('.Corporation').css('display','block');
		$('.NonCorporation').css('display','none');
	}
	else{
		$('.Corporation').css('display','none');
		$('.NonCorporation').css('display','block');
	}
}
</script>
<script>
function loadaidfood_expulsion(aidfoodId){
	$.ajax({
			url: config.BASE_URL+'aid/loadaidfood_expulsion/',
			type: "POST",
			data:{'aidfoodId':aidfoodId },
			dataType: "json",
			success: function(response)
			{	
				$('#aidfood_expulsion').html(response.data);
			 }
		});	
}

function loadaidfood_unloading(aidfoodId){
	$.ajax({
			url: config.BASE_URL+'aid/loadaidfood_unloading/',
			type: "POST",
			data:{'aidfoodId':aidfoodId },
			dataType: "json",
			success: function(response)
			{	
				$('#listallaidfood_unloading').html(response.data);
			 }
		});	
}

function listallaidfood_meeting(aidfoodId,section){
	$.ajax({
			url: config.BASE_URL+'aid/listallaidfood_meeting/',
			type: "POST",
			data:{'aidfoodId':aidfoodId,'section':section },
			dataType: "json",
			success: function(response)
			{	
				$('#listallaidfood_meeting'+section).html(response.data);
			 }
		});	
}
function loadaidfood_distributionlist(aidfoodId){
	$.ajax({
			url: config.BASE_URL+'aid/loadaidfood_distributionlist/',
			type: "POST",
			data:{'aidfoodId':aidfoodId },
			dataType: "json",
			success: function(response)
			{	
				$('#listallloadaidfood_distributionlist').html(response.data);
			 }
		});	
}
function loadaidfoodactualcost(aidfoodId){
	$.ajax({
			url: config.BASE_URL+'aid/loadaidfoodactualcost/',
			type: "POST",
			data:{'aidfoodId':aidfoodId },
			dataType: "json",
			success: function(response)
			{	
				$('#loadaidfoodactualcost').html(response.data);
			 }
		});	
}
function balance_listing(aidfoodId){
	var beneficiaries = $('#beneficiaries').val();
	$.ajax({
			url: config.BASE_URL+'aid/balance_listing/',
			type: "POST",
			data:{'aidfoodId':aidfoodId,'beneficiaries':beneficiaries },
			dataType: "json",
			success: function(response)
			{	
				$('.balance_listing').html(response.data);
				<?php if($rows->aidfoodId >0){?>
				loadaidfood_distributionlist('<?php echo $rows->aidfoodId ?>');
				<?php }?>
			 }
		});	
}


 $(document).ready(function(){
	$('#save_service').click(function () {
		check_my_session();
        $('#form_urgent .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_urgent .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_urgent .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                $("#form_urgent").submit();
            
        }
        else 
		{
            show_notification_error_end(ht);
        }
    });
	<?php if($rows->aidfoodId){ ?>
	loadaidfood_expulsion('<?php echo $rows->aidfoodId ?>');
	loadaidfood_unloading('<?php echo $rows->aidfoodId ?>');
	listallaidfood_meeting('<?php echo $rows->aidfoodId ?>','1');
	listallaidfood_meeting('<?php echo $rows->aidfoodId ?>','2');
	loadaidfood_distributionlist('<?php echo $rows->aidfoodId ?>');
	balance_listing('<?php echo $rows->aidfoodId ?>');
	loadaidfoodactualcost('<?php echo $rows->aidfoodId ?>');
	loaddata('<?php echo $rows->beneficiary ?>')
	relieftypefun('<?php echo $rows->relieftype ?>')
	<?php } ?>
	
	
});
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
function delete_data_system()
{
	check_my_session();
	
    var data_id 	=	$('#delete_id').val();
    var action_url 	=	$('#action_url').val();
    var rowid 		=	'#' + data_id + '_durar_lm';
	
	
    var delete_data_from_system = $.ajax({
        url: action_url,
        dataType: "html",
        success: function (msg) 
		{
            //$('#tableSortable').dataTable().fnDestroy();
            show_notification('لقد تم حذف سجل');
            $('#deleteDiag').modal('hide');
			window.location.reload();
            //$(rowid).slideUp('slow');
        }
    });
}
function relieftypefun(vals){
	if(vals == 82){
		
		$('#relieftypefun').css('display','none');
		loadprovince(200,'province');
	}
	else{
		$('#relieftypefun').css('display','block');	
	}
}
<?php if($Typerelief !=""){?>
relieftypefun('<?php echo $Typerelief;?>');

<?php }?>
</script>

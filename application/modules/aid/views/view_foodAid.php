<div id="user-profile">
<div class="row" dir="rtl">
    <div class="col-md-12 fox leftborder">
      <h4 class="panel-title customhr">شاشة الإغاثة ( المساعدات الغذائية + إفطار صائم )</h4>
       <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
           <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <i class="more-less glyphicon  glyphicon-minus"></i>
                          تفاصيل
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse  in  " role="tabpanel "  aria-labelledby="headingOne">
                    <div class="panel-body">
                         <table width="95%" border="0" cellspacing="0" cellpadding="10" align="center">
                          <tr>
                                <td class="right"><strong>الرقم </strong><br /><?php echo $aidfood->aidfoodId;?></td>
                                
                                <td><strong>نوع المستفيد</strong><br /><?php echo $beneficiary;?></td>
                                <td><strong>التاريخ</strong><br /><?php echo  $controller->dateformat($aidfood->currentdate);?></td>
                          </tr>
                            <tr>
                                <td class="right"><strong>اسم الجهة المستفيدة</strong><br /><?php echo $aidfood->beneficiaryName;?></td>
								<td><strong>العمر</strong><br /><?php echo $aidfood->age;?></td>
								<td><strong>هاتف</strong><br /><?php echo $aidfood->phone;?></td>
                                <td><strong>عنوان المشروع</strong><br /><?php echo $aidfood->projectTitle;?></td>
                          </tr>
                           <tr>
                                <td class="right"><strong>عدد المستفيدين</strong><br /><?php echo $aidfood->beneficiaries;?></td>
								<td><strong>الدولة</strong><br /><?php echo $country;?></td>
								<td><strong>حافظة / البلد</strong><br /><?php echo $province;?></td>
                                <td><strong>الولايه / المدينة</strong><br /><?php echo $city;?></td>
                          </tr>
                          <tr>
                                <td class="right" colspan="4"><strong>الدعم</strong><br /><?php echo $aidfood->support;?></td>
                          </tr>     
                             
                        </table>
                    </div>
                </div>
            </div>
      <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                            <i class="more-less glyphicon  glyphicon-minus"></i>
                          تفاصيل الطرد الغذائي
                        </a>
                    </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel "  aria-labelledby="headingFour">
                    <div class="panel-body">
                         <table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                <th width="10%">التسلسل</th>
                                <th  width="20%">اسم المادة</th>
                                <th  width="20%">تفاصيل المادة</th>
                                <th  width="10%">الوزن للوحدة</th>                 
                                <th  width="20%">الكمية</th>
								 <th>الملاحظات</th>                     
                              </tr>
                            </thead>
                            <tbody>
                            <?php
							$no =1;
							foreach($expulsion as $lc)
							{
								echo ' <tr><td>'.$no.'</td><td>';
								if(count($all_items)>0){
									foreach($all_items as $item){
										if($lc->subjectName == $item->itemid){
											echo $item->itemname;	
										}
									}
								}
								echo '</td><td>'.$lc->articledetails.'</td><td>'.$lc->weight.'</td><td>'.$lc->qty.'</td><td>'.$lc->notes.'</td>
										</tr>';
										$no++;
							}
							?>
							</tbody>
                          </table>
                    </div>
                </div>
            </div>
       <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingtwo">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
                            <i class="more-less glyphicon  glyphicon-minus"></i>
                          تفريغ عروض أسعار شراء مواد الطرد الغذائي
                        </a>
                    </h4>
                </div>
                <div id="collapsetwo" class="panel-collapse collapse in" role="tabpanel "  aria-labelledby="headingtwo">
                    <div class="panel-body">
                         <table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                <th width="5%">التسلسل</th>
                                <th  width="30%">اسم الشركة</th>
                                <th  width="5%">المبلغ</th>
                                <th  width="5%">عدد الطرود</th>                 
                                <th  width="30%">الملاحظات</th>
								<th width="15%">ارفاق عروض الأسع</th>              
                              </tr>
                            </thead>
                            <tbody>
                            <?php
							$no =1;
							//print_r($expences);
							foreach($unloading as $lc)
							{
								echo ' <tr><td>'.$no.'</td><td>';
								if(count($company)>0){
									  foreach($company as $type){
										  if($lc->companyName == $type->companyid){
											  echo $type->arabic_name;
										  }
									  }
								  }
								echo '</td><td>'.$lc->amount.'</td><td>'.$lc->noOfPackages.'</td><td>'.$lc->notes.'</td><td>';
								if($lc->attachment !=""){
									$attachment = @explode(',',$lc->attachment);
								   foreach($attachment as $att){
									   if($att !=""){
										   echo $controller->getfileicon($att,base_url().'resources/aid/'.$login_userid);
									   }
								   }
								}
								
								
								echo '</td></tr>';
								$no++;
							}
							?>
							</tbody>
                          </table>
                    </div>
                </div>
            </div>
       		<div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                            <i class="more-less glyphicon  glyphicon-minus"></i>
                          اجتماع اللجنة
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel "  aria-labelledby="headingThree">
                    <div class="panel-body">
                         <table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                 <th width="10%">رقم الاجتماع</th>
                                <th  width="10%">التاريخ</th>
                                <th  width="25%">قرار اللجنة</th>
                                <th width="25%">الملاحظات</th> 
								<th width="10%">اسم الشركة</th> 
								<th  width="20%">ارفاق الصور</th>                     
                              </tr>
                            </thead>
                            <tbody>
                            <?php
							$no =1;
							foreach($meeting1 as $lc)
							{
								echo ' <tr><td>'.$no.'</td><td>'.arabic_date($controller->dateformat($lc->currentdate)).'</td><td>'.$lc->decision.'</td><td>'.$lc->notes.'</td><td>';
								if(count($company)>0){
								  foreach($company as $type){
									  if($lc->companyName == $type->companyid){
										 echo $type->arabic_name;
									  }
								  }
							  }
							  echo'</td><td>';
								if($lc->attachment !=""){
								 $attachment = @explode(',',$lc->attachment);
								   foreach($attachment as $att){
									   if($att !=""){
												echo $controller->getfileicon($att,base_url().'resources/aid/'.$login_userid);
									   }
										
										
									}
								}echo'</td>
										</tr>';
										$no++;
							}
							?>
							</tbody>
                          </table>
                    </div>
                </div>
            </div>
       
       <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                            <i class="more-less glyphicon  glyphicon-minus"></i>
                          قائمة التوزيع
                        </a>
                    </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel "  aria-labelledby="headingFour">
                    <div class="panel-body">
                         <table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                 <th>التسلسل</th>
                                <th>اسم الشركة</th>
                                <th>المحافظة / المنطقة</th>
								<th>الولاية</th>
								<th>عدد الطرود</th>
								<th>منطقة التوزيع</th>
								<th>اجمالي الصرف</th>                     
                              </tr>
                            </thead>
                            <tbody>
                            <?php
							$no =1;
							$total_noOfPackage =0;
							$total_totalExchange =0;
							foreach($distributionlist as $lc)
							{
								echo ' <tr><td>'.$no.'</td><td>';
								if(count($company)>0){
								  foreach($company as $type){
									  if($lc->companyName == $type->companyid){
										 echo $type->arabic_name;
									  }
								  }
							 	 }
							  echo'</td><td>';
							   $total_noOfPackage = $total_noOfPackage +$lc->noOfPackage;
					  $total_totalExchange = $total_totalExchange +$lc->totalExchange;
					  
					   $province		=	$this->urgent_aid->get_all_listmanagement('issuecountry',200,$lc->region);
						$city		=	$this->urgent_aid->get_all_listmanagement('issuecountry',$lc->region,$lc->state);
						echo $province[0]->list_name.'</td><td>'.$city[0]->list_name.'</td><td>'.$lc->noOfPackage.'</td><td>'.$lc->distributionArea.'</td> <td>'.$lc->totalExchange.'</td></tr>';
										$no++;
							}
							$this->db->select('*');
								$this->db->from('ah_aidfood_distributionlist');						
								$this->db->where("delete_record",'0');
								$this->db->where("aidfoodId",$aidfoodId);
								$this->db->order_by('aidfoodDisId','DESC');
							
								$query = $this->db->get();
								$total_noOfPackage =0;
								foreach($query->result() as $lc)
								{
									 $total_noOfPackage = $total_noOfPackage +$lc->noOfPackage;
								 
								}
								$this->db->select('beneficiaries');
								$this->db->from('ah_aidfood');						
								$this->db->where("delete_record",'0');
								$this->db->where("aidfoodId",$aidfoodId);
								$this->db->order_by('aidfoodId','DESC');
								$query1 = $this->db->get();
								foreach($query1->result() as $lc)
								{
									 $beneficiaries =$lc->beneficiaries;
								 
								}
								
								$tot = $beneficiaries - $total_noOfPackage;
								echo ' <tr><td colspan="4"><strong style="color:#F00;">إجمالي متبقيات للمستفيدين: <span class="balance_listing">'.$tot.'</span></strong></td><td style="color:#029625;"><b>  اجمالي عدد الطرود: </b><b>'.$total_noOfPackage.'</b></td><td></td> <td style="color:#029625;"><b> اجمالي الصرف: </b><b>'.$total_totalExchange.'</b></td></tr>';
							?>
							</tbody>
                          </table>
                    </div>
                </div>
            </div>
       <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFive">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                            <i class="more-less glyphicon  glyphicon-minus"></i>
                         التكلفة الفعلية
                        </a>
                    </h4>
                </div>
                <div id="collapseFive" class="panel-collapse collapse in" role="tabpanel "  aria-labelledby="headingFive">
                    <div class="panel-body">
                         <table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                 <th>التسلسل</th>
                                 <th>اسم الشركة</th>
								 <th>منطقة التوزيع</th>
								 <th>عدد الطرود</th>
								 <th>قيمة الطرد</th>
								 <th>اجمالي تكلفة الطرد</th>                  
                              </tr>
                            </thead>
                            <tbody>
                            <?php
							$no =1;
							foreach($actualcost as $lc)
							{
								echo ' <tr><td>'.$no.'</td><td>';
								if(count($company)>0){
								  foreach($company as $type){
									  if($lc->companyName == $type->companyid){
										 echo $type->arabic_name;
									  }
								  }
							  }
							  echo '</td><td>'.$lc->distributionArea.'</td><td>'.$lc->noOfPackages.'</td><td>'.$lc->expulsionValue.'</td><td>'.$lc->totalCostOfPackages.'</td></tr>';
								
								
										$no++;
							}
							?>
							</tbody>
                          </table>
                    </div>
                </div>
            </div>
       <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSix">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                            <i class="more-less glyphicon  glyphicon-minus"></i>
                          اجتماع اللجنة
                        </a>
                    </h4>
                </div>
                <div id="collapseSix" class="panel-collapse collapse in" role="tabpanel "  aria-labelledby="headingSix">
                    <div class="panel-body">
                         <table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                 <th width="10%">رقم الاجتماع</th>
                                <th  width="10%">التاريخ</th>
                                <th  width="25%">قرار اللجنة</th>
                                <th width="25%">الملاحظات</th> 
								<th width="10%">اسم الشركة</th> 
								<th  width="20%">ارفاق الصور</th>                     
                              </tr>
                            </thead>
                            <tbody>
                            <?php
							$no =1;
							foreach($meeting2 as $lc)
							{
								echo ' <tr><td>'.$no.'</td><td>'.arabic_date($controller->dateformat($lc->currentdate)).'</td><td>'.$lc->decision.'</td><td>'.$lc->notes.'</td><td>';
								if(count($company)>0){
								  foreach($company as $type){
									  if($lc->companyName == $type->companyid){
										 echo $type->arabic_name;
									  }
								  }
							  }
							  echo'</td><td>';
								if($lc->attachment !=""){
								 $attachment = @explode(',',$lc->attachment);
								   foreach($attachment as $att){
									   if($att !=""){
												echo $controller->getfileicon($att,base_url().'resources/aid/'.$login_userid);
									   }
										
										
									}
								}echo'</td>
										</tr>';
										$no++;
							}
							?>
							</tbody>
                          </table>
                    </div>
                </div>
            </div>
       </div>
    </div>
  </div>
</div>
<div class="row" style="text-align: center;">
  <button type="button" id="" onClick="printthepage('user-profile');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
  <?php if($user_detail['profile']->userroleid == $servicedepartment){ ?>
  <button type="button" id="submitfinished" name="submitfinished" class="btn btn-success" onClick="submitfinished('<?php echo $rows->serviceid;?>','Accepted','submitfinished');" >تم الإنتهاء</button>
  <button type="button" id="submitreject" name="submitreject" class="btn btn-default" onClick="submitfinished('<?php echo $rows->serviceid;?>','Rejected','submitreject');" >لم يتم الانتهاء</button>
  <?php } ?>
</div>
<script>
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
<div class="row col-md-12">
  <form action="" method="POST" id="step-2-form" name="committee_meeting" autocomplete="off" enctype="multipart/form-data">
    <div class="form-group col-md-4">
      <label for="basic-input">الاسم</label>
      <input type="text" class="form-control" placeholder="الاسم" name="name" id="name" value="<?php echo $all_details->name;?>"/>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">الوظيفة</label>
      <input type="text" class="form-control" placeholder="الوظيفة" name="job" id="job" value="<?php echo $all_details->job;?>"/>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">الجهة</label>
      <input type="text" class="form-control" placeholder="الجهة" name="organization" id="organization" value="<?php echo $all_details->organization;?>"/>
    </div>
    <input type="hidden" name="extreliefTMId" id="extreliefTMId" value="<?php echo $all_details->extreliefTMId;?>" />
    <div class="form-group col-md-6">
      <button type="button" id="x_step_2" class="btn btn-sm btn-success">تحديث</button>
    </div>
  </form>
</div>
<script>
$(function(){
	$(".datepicker").datepicker({
	changeMonth: true,
	changeYear: true,
	yearRange: "-80:+0",
	dateFormat:'yy-mm-dd',
	});
/***************************************************************/	
	$( "#x_step_2" ).click(function() {
		var fd = new FormData(document.getElementById("step-2-form"));
		
		var exe_id	=	$("#extreliefId").val();
		
		$.ajax({
			url: config.BASE_URL+'aid/external_step_2',
			type: "POST",
			data:fd,
			enctype: 'multipart/form-data',
		  	dataType: "html",
		  	processData: false,  // tell jQuery not to process the data
          	contentType: false ,  // tell jQuery not to set contentType
			success: function(response)
			{
				$(location).attr('href', config.BASE_URL+'aid/addexternal_relief/'+exe_id);
      		}
    	});
	});
/***************************************************************/
});
</script>
<?php $aid_plan_detail	=	$this->urgent_aid->get_aid_plan_detail($extreliefId);?>
<style>
th {
	text-align: center !important;
}
</style>
<div id="user-profile">
  <div class="row" dir="rtl">
    <div class="col-md-12 fox leftborder"> 
      <!--<h4 class="panel-title customhr">اعمال الإغاثة الخارجية</h4>-->
      <div id="collapseTwo" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="headingTwo">
        <div class="panel-body">
          <div class="form-group col-md-12">
            <h4 class="yateem_h4">تفاصيل</h4>
            <table class="table table-bordered table-striped ">
              <tr>
                <td class="right"><strong>رقم الإغاثة : </strong><?php echo $rows->autoid;?></td>
                <td><strong>التاريخ : </strong><?php echo $controller->dateformat($rows->currentdate);?></td>
                <td><strong>تصنيف الإغاثة : </strong><?php echo  $rows->relief;?></td>
              </tr>
              <tr>
                <td class="right"><strong>الدولة : </strong><?php echo $this->haya_model->get_name_from_list($rows->country);?></td>
                <td><strong>المحافظه : </strong><?php echo $this->haya_model->get_name_from_list($rows->province);?></td>
                <td><strong>الولايه : </strong><?php echo  $this->haya_model->get_name_from_list($rows->city);?></td>
              </tr>
              <tr>
                <td class="right"><strong>نوع الإغاثة : </strong><?php echo $rows->reasons;?></td>
                <td><strong>الدفعة : </strong><?php echo  $rows->batch;?></td>
                <td>&nbsp;</td>
              </tr>
            </table>
              <h4 class="yateem_h4">اجتماع لجنة الإغاثة</h4>
              <table class="table table-bordered table-striped">
                <thead style="background-color: #029625;">
                  <tr role="row" style="color:#fff !important;">
                      <th align="center">رقم محضر الاجتماع</th>
                      <th align="center">التاريخ</th>
                      <th align="center">قرار اللجنة</th>
                      <th align="center">الملاحظات</th>
                  </tr>
                </thead>
                <tbody>
                <?php
				$extreliefTMId =0;
				if(isset($external_relief_committee_meeting)){
				  if(count($external_relief_committee_meeting)>0){
					foreach($external_relief_committee_meeting as $reliefteam) { 
					  $extreliefTMId ++;
				?>
                  <tr id="<?php echo $reliefteam->extDataReportId;?>_daily">
                      <td><?php echo $reliefteam->minutes;?></td>
                      <td><?php echo $reliefteam->currentdate;?></td>
                      <td><?php echo $reliefteam->decision;?></td>
                      <td><?php echo $this->haya_model->limit($reliefteam->notes, '50');?></td>
                  </tr>
                  <?php } } } $extreliefTMId ++;?>
                </tbody>
              </table>
              <h4 class="yateem_h4">أعضاء فريق الاغاثة</h4>
              <table class="table table-bordered table-striped">
                <thead style="background-color: #029625;">
                  <tr role="row" style="color:#fff !important;">
                      <th>رقم</th>
                      <th>الاسم</th>
                      <th>الوظيفة</th>
                      <th>الجهة</th>
                  </tr>
                </thead>
                <tbody>
                <?php
				$extreliefTMId =0;
				if(isset($external_reliefteam)){
				  if(count($external_reliefteam)>0){
					foreach($external_reliefteam as $reliefteam) { 
					  $extreliefTMId ++;
				?>
                  <tr>
                      <td><?php echo $extreliefTMId; ?></td>
                      <td><?php echo $reliefteam->name;?></td>
                      <td id="<?php echo $reliefteam->extreliefTMId;?>_team_name"><?php echo $reliefteam->job;?></td>
                      <td><?php echo $reliefteam->organization;?></td>
                  </tr>
                  <?php } } } $extreliefTMId ++;?>
                </tbody>
              </table>
              <h4 class="yateem_h4">خطة الإغاثة</h4>
              <table class="table table-bordered table-striped ">
              <tr>
                <td class="right"><strong>الموازنة  : </strong><?php echo $aid_plan_detail->budget;?></td>
                <td><strong>المخصصات : </strong><?php echo $aid_plan_detail->transfer_money;?></td>
                <td><strong>التوجيهات : </strong><?php echo  $aid_plan_detail->instructions;?></td>
              </tr>
              <tr>
                <td class="right"><strong>الخطة : </strong><?php echo $aid_plan_detail->plan;?></td>
                <td><strong>إضافة مقترح : </strong><?php echo $aid_plan_detail->give_suggestion;?></td>
                <td>&nbsp;</td>
              </tr>
            </table>
              <h4 class="yateem_h4">الاحتياجات المطلوبة</h4>
              <table class="table table-bordered table-striped">
                <thead style="background-color: #029625;">
                  <tr role="row" style="color:#fff !important;">
                      <th>رقم</th>
                      <th>نوع المواد</th>
                      <th>المواد</th>
                      <th>الكمية</th>
                      <th>الموازنة</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
					$extreliefTMId =0;
					if(isset($external_requirements_required)){
					  if(count($external_requirements_required)>0){
						foreach($external_requirements_required as $reliefteam) { 
						  $extreliefTMId ++;
					?>
                  <tr>
                      <td><?php echo $extreliefTMId; ?></td>
                      <td><?php echo $this->haya_model->get_name_from_list($reliefteam->typeOfMaterial);?></td>
                      <td><?php echo $reliefteam->materials;?></td>
                      <td><?php echo $reliefteam->quantity;?></td>
                      <td><?php echo number_format($reliefteam->budgeting,3);?></td>
                  </tr>
                  <?php } } } $extreliefTMId ++;?>
                </tbody>
              </table>
                            <h4 class="yateem_h4">المصروفات</h4>
              <table class="table table-bordered table-striped">
                <thead style="background-color: #029625;">
                  <tr role="row" style="color:#fff !important;">
                        <th>التسلسل</th>
                        <th>مصروفات إدارية</th>
                        <th>المواد الغذائية</th>
                        <th>المواد الاغاثية</th>
                        <th>المواد الطبية</th>
                        <th>اخرى</th>
                        <th>الاجمالي</th>
                        <th>الملاحظات</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $extreliefTMId =0;
                if(isset($external_expenses)){
                  if(count($external_expenses)>0){
                    foreach($external_expenses as $reliefteam)
					{
					$total_amount	=	($reliefteam->prose+$reliefteam->food+$reliefteam->reliefArticles+$reliefteam->medicalMaterials+$reliefteam->other);
                      $extreliefTMId ++;
					 ?>
                  <tr>
                        <td><?php echo $extreliefTMId; ?></td>
                        <td><?php echo number_format($reliefteam->prose,3);?></td>
                        <td><?php echo number_format($reliefteam->food,3);?></td>
                        <td><?php echo number_format($reliefteam->reliefArticles,3);?></td>
                        <td><?php echo number_format($reliefteam->medicalMaterials,3);?></td>
                        <td><?php echo number_format($reliefteam->other,3);?></td>
                        <td><?php echo number_format($total_amount,3);?></td>
                        <td><?php echo $this->haya_model->limit($reliefteam->notes, '50');?></td>
                  </tr>
                  <?php } } } $extreliefTMId ++;?>
                </tbody>
              </table>
              <h4 class="yateem_h4">التقرير اليومي</h4>
              <table class="table table-bordered table-striped">
                <thead style="background-color: #029625;">
                  <tr role="row" style="color:#fff !important;">
                    <th>رقم</th>
                    <th>التاريخ</th>
                    <th>البيان</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
				$extreliefTMId =0;
				if(isset($external_daily_report)){
					if(count($external_daily_report)>0){
						foreach($external_daily_report as $reliefteam) { 
						$extreliefTMId ++;
						?>
                  <tr id="<?php echo $reliefteam->extDataReportId;?>_daily">
                    <td><?php echo $extreliefTMId;?></td>
                    <td><?php echo $reliefteam->currentdate;?></td>
                    <td><?php echo $reliefteam->statement;?></td>
                  </tr>
                  <?php } } } $extreliefTMId ++;?>
                </tbody>
              </table>
              <h4 class="yateem_h4">التقرير النهائي</h4>
              <table class="table table-bordered table-striped">
                <thead style="background-color: #029625;">
                  <tr role="row" style="color:#fff !important;">
                      <th>رقم</th>
                      <th>التاريخ</th>
                      <th>نسبة التنفيذ</th>
                      <th>البيان</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                      $extreliefTMId =0;
                      if(isset($ah_external_final_report)){
                        if(count($ah_external_final_report)>0){
                         foreach($ah_external_final_report as $reliefteam) { 
                           $extreliefTMId ++;
				?>
                  <tr>
                   <td><?php echo $extreliefTMId;?></td>
                      <td><?php echo $reliefteam->currentdate;?></td>
                      <td><?php echo $reliefteam->final_percent;?></td>
                      <td><?php echo $this->haya_model->limit($reliefteam->statement, '50');?></td>
                  </tr>
                  <?php } } } $extreliefTMId ++;?>
                </tbody>
              </table>
              <h4 class="yateem_h4">قرار لجنة الإغاثة النهائي</h4>
              <table class="table table-bordered table-striped">
                <thead style="background-color: #029625;">
                  <tr role="row" style="color:#fff !important;">
                      <th align="center">رقم محضر الاجتماع</th>
                      <th align="center">التاريخ</th>
                      <th align="center">قرار اللجنة</th>
                      <th align="center">الملاحظات</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
				$extreliefTMId =0;
				if(isset($ah_final_relief_committee_meeting)){
				  if(count($ah_final_relief_committee_meeting)>0){
					foreach($ah_final_relief_committee_meeting as $reliefteam) { 
					  $extreliefTMId ++;
						?>
                  <tr>
                      <td><?php echo $reliefteam->minutes;?></td>
                      <td><?php echo $reliefteam->currentdate;?></td>
                      <td><?php echo $this->haya_model->limit($reliefteam->decision, '50');?></td>
                      <td><?php echo $this->haya_model->limit($reliefteam->notes, '50');?></td>
                  </tr>
                  <?php } } } $extreliefTMId ++;?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row" style="text-align: center;">
  <button type="button" id="" onClick="printthepage('user-profile');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
    <button type="button" id="" onClick="printthepage('user-profile');" class="btn btn-default"><i class="icon-print"></i> PDF </button>
</div>

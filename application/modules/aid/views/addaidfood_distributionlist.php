  <form action="" method="POST" id="frm_expence" name="frm_expence" action="<?php echo base_url()?>aid/addaidfood_distributionlist/<?php echo $aidfoodId;?>/<?php echo  $aidfoodDisId;?>"  enctype="multipart/form-data">
  <div class="row">
  <div class="form-group col-md-6"><b style="color:#F00">عدد المستفيدين:  <span id="balance"><?php echo $main->beneficiaries;?></span></b></div>
  <div class="form-group col-md-6"><b style="color:#F00">إجمالي متبقيات للمستفيدين: <span id="balance_listing"></span></b></div>
  </div>
<div class="row col-md-12">
    <div class="form-group col-md-4">
      <label for="basic-input">‎اسم الشركة</label>
     <select name="companyName" id="companyName" class="form-control">
        <?php 
		  if(count($company)>0){
			  foreach($company as $type){
				  if($rows->companyName == $type->companyid){
					  echo '<option value="'.$type->companyid.'" selected >'.$type->arabic_name.'</option>';
				  }
				  else{
					  echo '<option value="'.$type->companyid.'" >'.$type->arabic_name.'</option>';
				  }
				  
			  }
		  }
		  ?>
      </select>
     
    </div>
   
    <div class="form-group col-md-4">
      <label for="basic-input">المحافظة / المنطقة</label>
      <select name="region" id="region" class="form-control" onChange="loadprovince(this.value,'state');">
        <?php 
		  if(count($province)>0){
			  foreach($province as $type){
				  if($rows->region == $type->list_id){
					  echo '<option value="'.$type->list_id.'" selected="selected">'.$type->list_name.'</option>';
				  }
				  else{
					  echo '<option value="'.$type->list_id.'" >'.$type->list_name.'</option>';
				  }
				  
			  }
		  }
		  ?>
      </select>
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">الولاية</label>
      <select name="state" id="state" class="form-control">
      </select>
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">عدد الطرود</label>
      <input type="number" class="form-control req" value="<?php echo intval($rows->noOfPackage);?>" placeholder="عدد الطرود" name="noOfPackage" id="noOfPackage" onkeyup="validatevslu(this.value);" />
    </div>
    
     <div class="form-group col-md-4">
      <label for="basic-input">منطقة التوزيع</label>
      <input type="text"   class="form-control req"  placeholder="منطقة التوزيع" name="distributionArea" id="distributionArea" value="<?php echo $rows->distributionArea;?>">
    </div>
    
    <input type="hidden" name="aidfoodId" id="aidfoodId" value="<?php echo $aidfoodId;?>"/>
    <input type="hidden" name="aidfoodDisId" id="aidfoodDisId" value="<?php echo $aidfoodDisId;?>"/>

</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="save_expence" value="حفظ" />
  </div>
</div>
  </form>
<script>
 
 $(document).ready(function(){
	$('#save_expence').click(function () {
		check_my_session();
        $('#frm_expence .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_expence .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_expence .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                var str_data = 	$('#frm_expence').serialize();
		
			var request = $.ajax({
			  url: config.BASE_URL+'aid/addaidfood_distributionlist/<?php echo $aidfoodId;?>',
			  type: "POST",
			  data: str_data,
			  dataType: "html",
			  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
			  success: function(msg)
			  {		
			  	 $('#ajax_action').hide();
				console.log(msg);
				
				 $('#addingDiag').modal('hide');	  
				 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
				 loadaidfood_distributionlist('<?php echo $aidfoodId ?>');
				 balance_listing('<?php echo $aidfoodId ?>')
			  }
			});
            
        }
        else 
		{
            show_notification_error(ht);
        }
    });
});
function loadprovince(vals,id){
	$('#'+id).empty();
	$.ajax({
		url: config.BASE_URL+'aid/loadprovince/',
		type: "POST",
		data:{'vals':vals },
		dataType: "json",
		success: function(data)
		{	
			var al =data.province;
			for(var i=0; i<al.length; i++)
			{
				$('#'+id).append('<option value="'+al[i].list_id+'">'+al[i].list_name+'</option>');
				
			}
		}
	});
}
function balance_listing(aidfoodId){
	var beneficiaries = '<?php echo $main->beneficiaries;?>';
	$.ajax({
			url: config.BASE_URL+'aid/balance_listing/',
			type: "POST",
			data:{'aidfoodId':aidfoodId,'beneficiaries':beneficiaries },
			dataType: "json",
			success: function(response)
			{	
				$('#balance_listing').html(response.data);
				
			 }
		});	
}
function validatevslu(vals){
	var balance = parseInt($('#balance').html());
	if(vals >= balance ){
		$('#noOfPackage').css('border-color','#F00');
		$('#noOfPackage').val('');
	}
	else{
		$('#noOfPackage').css('border','1px solid #029625');
	}
}
 balance_listing('<?php echo $aidfoodId;?>');
</script>

<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
       <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
       
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"><?php echo add_button('aid/addfood','شاشة الإغاثة',$permissions[$module['moduleid']]['a']	==	1); ?></div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th>الرقم</th>
                        <th>نوع الاغاثة</th>
                        <th>نوع المستفيد</th>
                        <th>التاريخ</th>                 
                        <th>عنوان المشروع</th>
                        <th>المستفيدين</th>
                        <th>إجمالي متبقيات للمستفيدين</th>
                        <th>الإجراءات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">                     
                    </tbody>
                  </table>
                  <dddddddddddd>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'aid/ajax_all_datas_aid/','columns_array'=>'{ "data": "الرقم" },
                { "data": "نوع الاغاثة" },
                { "data": "نوع المستفيد" },
				{ "data": "التاريخ" },
                { "data": "عنوان المشروع" },
				 { "data": "المستفيدين" },
				 { "data": "إجمالي متبقيات للمستفيدين" },
				{ "data": "الإجراءات" }')); ?>
</div>
</body>
</html>



</script>

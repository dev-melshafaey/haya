  <form action="" method="POST" id="frm_expence" name="frm_expence" action="<?php echo base_url()?>aid/addaidfood_actualcost/<?php echo $aidfoodId;?>/<?php echo  $aidfoodCostId;?>"  enctype="multipart/form-data">
<div class="row col-md-12">
    <div class="form-group col-md-4">
      <label for="basic-input">‎اسم الشركة</label>
     <select name="companyName" id="companyName" class="form-control">
        <?php 
		  if(count($company)>0){
			  foreach($company as $type){
				  if($rows->companyName == $type->companyid){
					  echo '<option value="'.$type->companyid.'" selected >'.$type->arabic_name.'</option>';
				  }
				  else{
					  echo '<option value="'.$type->companyid.'" >'.$type->arabic_name.'</option>';
				  }
				  
			  }
		  }
		  ?>
      </select>
     
    </div>
   
    <div class="form-group col-md-4">
      <label for="basic-input">منطقة التوزيع</label>
      <input type="text" class="form-control req" value="<?php echo $rows->distributionArea;?>" placeholder="منطقة التوزيع" name="distributionArea" id="distributionArea" />
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">عدد الطرود</label>
      <input type="number" class="form-control req" value="<?php echo $rows->noOfPackages;?>" placeholder="عدد الطرود" name="noOfPackages" id="noOfPackages" onkeyup="totalcost();" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">قيمة الطرد</label>
      <input type="text" class="form-control req" value="<?php echo $rows->expulsionValue;?>" placeholder="قيمة الطرد" name="expulsionValue" id="expulsionValue" onkeyup="totalcost();" />
    </div>
     <div class="form-group col-md-4">
      <label for="basic-input">اجمالي تكلفة الطرد</label>
      <input type="text" class="form-control req" value="<?php echo $rows->totalCostOfPackages;?>" placeholder="اجمالي تكلفة الطرد" name="totalCostOfPackages" id="totalCostOfPackages" readonly="readonly" />
    </div>
    
    
    
    <input type="hidden" name="aidfoodId" id="aidfoodId" value="<?php echo $aidfoodId;?>"/>
    <input type="hidden" name="aidfoodCostId" id="aidfoodCostId" value="<?php echo $aidfoodCostId;?>"/>

</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="save_expence" value="حفظ" />
  </div>
</div>
  </form>
<script>
 function totalcost(){
	 var noOfPackages = $('#noOfPackages').val();
	 var expulsionValue = $('#expulsionValue').val();
	 var totalCostOfPackages = noOfPackages * expulsionValue;
	 $('#totalCostOfPackages').val(totalCostOfPackages);
 }
 $(document).ready(function(){
	$('#save_expence').click(function () {
		check_my_session();
        $('#frm_expence .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_expence .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_expence .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                var str_data = 	$('#frm_expence').serialize();
		
			var request = $.ajax({
			  url: config.BASE_URL+'aid/addaidfood_actualcost/<?php echo $aidfoodId;?>',
			  type: "POST",
			  data: str_data,
			  dataType: "html",
			  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
			  success: function(msg)
			  {		
			  $('#ajax_action').hide();
				console.log(msg);
				
				 $('#addingDiag').modal('hide');	  
				 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
				 loadaidfoodactualcost('<?php echo $aidfoodId ?>');
			  }
			});
            
        }
        else 
		{
            show_notification_error(ht);
        }
    });
});
</script>
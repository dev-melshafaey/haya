<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aid extends CI_Controller {

	/*
	* Properties
	*/
	private $_data					=	array();
	private $_login_userid			=	NULL;
	public $administrator 			=	NULL;
	public $servicedepartment 		=	NULL;
	public $moduleid 				= 	NULL;
	public $otherservice_moduleid 	= 	NULL;
//-----------------------------------------------------------------------

	/*
	* Constructor
	*/
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('urgent_aid_model','urgent_aid');
		/////////////////////////////////////////	
		$this->_data['controller']=$this; 
		$this->moduleid = 245;
		$this->urgentaid_moduleid = 246;
		$this->external_moduleid = 249;
		/////////////////////////
		$this->_data['urgentaid_moduleid']	= $this->urgentaid_moduleid;
		$this->_data['moduleid']			= $this->moduleid;
		$this->_data['external_moduleid']			= $this->external_moduleid;
		
		$this->_data['module']		=	$this->haya_model->get_module();		
		$this->_login_userid 			=	$this->session->userdata('userid');
		$this->_data['login_userid'] 	=	$this->_login_userid;
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);
			
		
		$this->_data['login_userid']	=	$this->_login_userid;
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);
		$this->userroleid				=  $this->_data['user_detail']['profile']->userroleid;
		// Load all types
		$this->_data['list_types']		=	$this->haya_model->get_listmanagment_types();
		
		//$this->add_users_yearly_holidays(); // Add Yearly holiday into users accounts.
		
	}
	public function index()
	{
		$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
		$this->load->view('listall_aid',$this->_data);
	}
	public function ajax_all_datas_aid()
	{
			$this->db->select('*');
			$this->db->from('ah_aidfood');						
			$this->db->where("delete_record",'0');
			//$this->db->where("userid",$this->_login_userid);
			$this->db->order_by('status','DESC');
			$this->db->order_by('aidfoodId','DESC');
			
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
						
			foreach($query->result() as $lc)
			{
				if($lc->aidfoodId)
				{
					$action 	='<a onclick="alatadad(this);" data-url="'.base_url().'aid/view_foodAid/'.$lc->aidfoodId.'" href="#"><i class="my icon icon-eye-open"></i></a>';
				}
				if($lc->userid == $this->_login_userid){
					$action	.= ' <a href="'.base_url().'aid/addfood/'.$lc->aidfoodId.'" ><i class="icon-pencil"></i></a>';
				}
				
				if($permissions[$this->moduleid]['d']	==	1 and $lc->status != "Accepted") 
				{
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->aidfoodId.'" data-url="'.base_url().'aid/fooddelete/'.$lc->aidfoodId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
				$status ='';
				if($lc->status == "Pending")
				{
					$status	=	'<img src="'.base_url().'assets/images/pending.png" style="width: 24px;" alt="Pending" title="Pending"/>';
				}
				else if($lc->status == "In Progress")
				{
					$status	=	'<img src="'.base_url().'assets/images/inprogress.png" style="width: 24px;" alt="In Progress" title="In Progress"/>';
				}
				else if($lc->status == "Accepted")
				{
					$status	=	'<img src="'.base_url().'assets/images/completed.png" style="width: 24px;" alt="Accepted" title="Accepted"/>';
				}
				else if($lc->status == "Rejected")
				{
					$status	=	'<img src="'.base_url().'assets/images/rejected.png" style="width: 24px;" alt="Rejected" title="Rejected"/>';
				}
				$beneficiary ='';
				$this->db->select('list_name');
				$this->db->from('ah_listmanagement');						
				$this->db->where("list_type","donation_type");
				$this->db->where("list_id",$lc->reliefType);
				$rowuser = $this->db->get(); 
				foreach($rowuser->result() as $us){$reliefType = $us->list_name;}
				$this->db->select('list_name');
				$this->db->from('ah_listmanagement');						
				$this->db->where("list_type","beneficiary");
				$this->db->where("list_id",$lc->beneficiary);
				$rowuser1 = $this->db->get(); 
				foreach($rowuser1->result() as $us){$beneficiary = $us->list_name;}
				
				$balance =0;
				
				$this->db->select('*');
				$this->db->from('ah_aidfood_distributionlist');						
				$this->db->where("delete_record",'0');
				$this->db->where("aidfoodId",$lc->aidfoodId);
				$this->db->order_by('aidfoodDisId','DESC');
			
				$query = $this->db->get();
				$total_noOfPackage =0;
				foreach($query->result() as $lc1)
				{
					 $total_noOfPackage = $total_noOfPackage +$lc1->noOfPackage;
				 
				}
				$this->db->select('beneficiaries');
				$this->db->from('ah_aidfood');						
				$this->db->where("delete_record",'0');
				$this->db->where("aidfoodId",$lc->aidfoodId);
				$this->db->order_by('aidfoodId','DESC');
				$query1 = $this->db->get();
				foreach($query1->result() as $lc2)
				{
					 $beneficiaries =$lc2->beneficiaries;
				 
				}
				
				$balance = $beneficiaries - $total_noOfPackage;
				
				
				foreach($rowuser1->result() as $us1){$beneficiary = $us1->list_name;}
				$arr[] = array(
					"DT_RowId"		=>	$lc->aidfoodId.'_durar_lm',
					"الرقم" 		=> $lc->aidfoodId,	
					"نوع الاغاثة" 		=> $reliefType,	
					"نوع المستفيد" 	=>	$beneficiary,
					"التاريخ" 	=>	arabic_date($this->dateformat($lc->currentdate)),
					"عنوان المشروع" 		=>	$lc->projectTitle,
					"المستفيدين" 		=>	$lc->beneficiaries,
					"إجمالي متبقيات للمستفيدين" 		=>	$balance,
					"الإجراءات" 		=>	$action //$this->haya_model->get_name_from_list($lc->country_id),
					);
					
					unset($action,$items);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
	}
	public function listall_UrgentAid()
	{
		$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
		$this->load->view('listall_UrgentAid',$this->_data);
	}
	public function ajax_all_datas()
	{
				$this->db->select('urgentaidId,userid,reliefType,province,city,autoid,beneficiary,currentdate,beneficiaryName');
				$this->db->from('ah_urgent_aid');						
				$this->db->where("delete_record",'0');
				//$this->db->where("userid",$this->_login_userid);
				$this->db->order_by('status','DESC');
				$this->db->order_by('urgentaidId','DESC');
			
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
			
			
			foreach($query->result() as $lc)
			{
				
				
				if($lc->urgentaidId)
				{
					
					
					$action 	='<a onclick="alatadad(this);" data-url="'.base_url().'aid/view_UrgentAid/'.$lc->urgentaidId.'" href="#"><i class="my icon icon-eye-open"></i></a>';
					
					
				}
				if($lc->userid == $this->_login_userid){
					$action	.= ' <a href="'.base_url().'aid/addUrgentAid/'.$lc->urgentaidId.'" ><i class="icon-pencil"></i></a>';
				}
				
				
				if($permissions[$this->moduleid]['d']	==	1 and $lc->status != "Accepted") 
				{
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->urgentaidId.'" data-url="'.base_url().'aid/delete/'.$lc->urgentaidId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
				$status ='';
				if($lc->status == "Pending")
				{
					$status	=	'<img src="'.base_url().'assets/images/pending.png" style="width: 24px;" alt="Pending" title="Pending"/>';
				}
				else if($lc->status == "In Progress")
				{
					$status	=	'<img src="'.base_url().'assets/images/inprogress.png" style="width: 24px;" alt="In Progress" title="In Progress"/>';
				}
				else if($lc->status == "Accepted")
				{
					$status	=	'<img src="'.base_url().'assets/images/completed.png" style="width: 24px;" alt="Accepted" title="Accepted"/>';
				}
				else if($lc->status == "Rejected")
				{
					$status	=	'<img src="'.base_url().'assets/images/rejected.png" style="width: 24px;" alt="Rejected" title="Rejected"/>';
				}
			$this->db->select('list_name');
			$this->db->from('ah_listmanagement');						
			$this->db->where("list_type","donation_type");
			$this->db->where("list_id",$lc->reliefType);
			$rowuser = $this->db->get(); 
			foreach($rowuser->result() as $us){$reliefType = $us->list_name;}
			$this->db->select('list_name');
			$this->db->from('ah_listmanagement');						
			$this->db->where("list_type","beneficiary");
			$this->db->where("list_id",$lc->beneficiary);
			$rowuser1 = $this->db->get(); 
			foreach($rowuser1->result() as $us1){$beneficiary = $us1->list_name;}
			
				$arr[] = array(
					"DT_RowId"		=>	$lc->urgentaidId.'_durar_lm',
					"التسلسل" 		=>	$lc->autoid,
					"التاريخ" 		=>	arabic_date($this->dateformat($lc->currentdate)),
					"تصنيف الإغاثة" 	=>	$beneficiary,	
					"نوع الإغاثة" 	=>	$reliefType,	
					"الولاية" 		=>	$this->haya_model->get_name_from_list($lc->city),
					"الإجراءات" 		=>	$action //$this->haya_model->get_name_from_list($lc->country_id),
					);
					
					unset($action,$items);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
	}
	function addUrgentAid($urgentaidId	=	NULL){
		
		
		$this->_data["urgentaidId"]= $urgentaidId;
		$this->db->select('urgentaidId');
		$this->db->from('ah_urgent_aid');						
		$this->db->order_by('urgentaidId','DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$count =1;
		foreach($query->result() as $lc){
			$count =$lc->urgentaidId;
		}
		$this->_data["count"]=++$count;
		if($this->input->post())
		{
			$data	=	$this->input->post();
			$services	=	array();
			$services["reliefType"]			= 	$data["reliefType"];
			$services["beneficiary"]		= 	$data["beneficiary"];
			$services["currentdate"]		= 	$data["currentdate"];
			$services["beneficiaryName"]	= 	$data["beneficiaryName"];
			$services["age"]				= 	$data["age"];
			$services["phone"]				= 	$data["phone"];
			$services["numberofIndividual"]	=	$data["numberofIndividual"];
			$services["country"]			=	$data["country"];
			$services["province"]			=	$data["province"];
			$services["city"]				=	$data["city"];
			$services["socialstatus"]		=	$data["socialstatus"];
			$services["address"]			=	$data["address"];
			$services["work"]				=	$data["work"];
			$services["statement"]			=	$data["statement"];
			$services["statussummary"]		=	$data["statussummary"];
			$services["urbansituation"]		=	$data["urbansituation"];
			$services["familiesaffected"]	=	$data["familiesaffected"];
			$services["requirements"]		=	$data["requirements"];
			$services["breakdown"]			=	$data["breakdown"];
			$services["proposal"]			= 	$data["proposal"];
			$services["measures"]			=	$data["measures"];
			$services["village"]			=	$data["village"];
			$services["person_name"]		=	$data["person_name"];
			$services["autoid"]				=	$data["autoid"];
			
			if($data["urgentaidId"] != ""){
				$services["urgentaidId"]	= $data["urgentaidId"];
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
					$names = array();
					$files = $_FILES["attachment"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
						}
						
					}
					if($data["attachment_old"] !=""){
						if(count($names)>0)	$services['attachment']	=	$data["attachment_old"].",".@implode(',',$names);
						else{$services['attachment']	=	$data["attachment_old"];}
					}
					else{
						$services['attachment']	=	@implode(',',$names);	
					}
					
				}
				else{
					$services['attachment']	=	$data["attachment_old"];
				}
				if(isset($_FILES["attachments"]) && count($_FILES["attachments"]) > 0)
				{
					$names = array();
					$files = $_FILES["attachments"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
						}
						
					}
					if($data["images_old"] !=""){
						if(count($names)>0)	$services['images']	=	$data["images_old"].",".@implode(',',$names);
						else{$services['images']	=	$data["images_old"];}
					}
					else{
						$services['images']	=	@implode(',',$names);	
					}
					
				}
				else{
					$services['images']	=	$data["images_old"];
				}
				
				
				$urgentaidId = $this->urgent_aid->savestore($services,'ah_urgent_aid','urgentaidId');
				$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
				redirect(base_url().'aid/addUrgentAid/'.$urgentaidId);
				exit();
			}else{
				$services["status"]	= 'Pending';
				$services['userid']	=	$this->_login_userid;
				$services["urgentaidId"]	= $data["urgentaidId"];
				//print_r($_FILES);exit();
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
					$names = array();
					$files = $_FILES["attachment"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
						}
						
					}
					$services['attachment']	=	@implode(',',$names);
				}
				if(isset($_FILES["attachments"]) && count($_FILES["attachments"]) > 0)
				{
					$names = array();
					$files = $_FILES["attachments"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
						}
						
					}
					$services['images']	=	@implode(',',$names);
				}
				
				$urgentaidId = $this->urgent_aid->savestore($services,'ah_urgent_aid','urgentaidId');
				$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
				redirect(base_url().'aid/addUrgentAid/'.$urgentaidId);
				exit();
			}
			
			
			
		}
		if($urgentaidId >0)
		{
			$this->_data["rows"] = $this->urgent_aid->getdata($urgentaidId,'ah_urgent_aid',"urgentaidId");
			$this->_data['province']	=	$this->urgent_aid->get_all_listmanagement('issuecountry',$this->_data["rows"]->country);
			$this->_data['city']		=	$this->urgent_aid->get_all_listmanagement('issuecountry',$this->_data["rows"]->province);
			}
		$permissions					=	$this->haya_model->check_other_permission(array($this->moduleid));
		$this->_data['reliefType']		=	$this->urgent_aid->get_all_listmanagement('donation_type');
		$this->_data['beneficiary']		=	$this->urgent_aid->get_all_listmanagement('beneficiary');
		$this->_data['issuecountry']	=	$this->urgent_aid->get_all_listmanagement('issuecountry');
		$this->_data['marital_status']	=	$this->urgent_aid->get_all_listmanagement('marital_status');
		$this->_data['typeOfMaterial']	=	$this->urgent_aid->get_all_listmanagement('type_of_material');

		if($urgentaidId){
			
			$this->db->select('*');
			$this->db->from('ah_internal_reliefteam');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$urgentaidId);
			$this->db->order_by('extreliefTMId','ASC');
			$query = $this->db->get();
			$this->_data["internal_reliefteam"] = $query->result();
			
			//echo $this->db->last_query();
			//////////////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_internal_relief_committee_meeting');						
			$this->db->where("delete_record",'0');
			$this->db->where("urgentaidId",$urgentaidId);
			$this->db->order_by('internalMeetingId','ASC');
			$query = $this->db->get();
			$this->_data["ah_internal_relief_committee_meeting"] = $query->result();
			//////////////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_internal_final_relief_committee_meeting');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$urgentaidId);
			$this->db->order_by('extrelMeetingId','ASC');
			$query = $this->db->get();
			$this->_data["ah_internal_final_relief_committee_meeting"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_internal_requirements_required');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$urgentaidId);
			$this->db->order_by('requirementsId','ASC');
			$query1 = $this->db->get();
			$this->_data["internal_requirements_required"] = $query1->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_internal_expenses');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$urgentaidId);
			$this->db->order_by('extExpencesId','ASC');
			$query = $this->db->get();
			$this->_data["internal_expenses"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_internal_final_report');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$urgentaidId);
			$this->db->order_by('extDataReportId','ASC');
			$query = $this->db->get();
			$this->_data["ah_internal_final_report"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_internal_daily_report');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$urgentaidId);
			$this->db->order_by('extDataReportId','ASC');
			$query = $this->db->get();
			$this->_data["internal_daily_report"] = $query->result();
			/////////////////////////////////////////////////////////
			//$this->_data["rows"] = $this->urgent_aid->getdata($extreliefId,'ah_external_relief',"extreliefId");
			
			
		}
		
		
		$this->load->view('addUrgentAid',$this->_data);
	}
	
	function loadprovince()
	{
		$vals	=	$this->input->post('vals');
		
		$message = array();
		
		if($vals	>	0)
		{
			$message		=	$this->urgent_aid->get_all_listmanagement('issuecountry',$vals);
		}
		
		$ex['province'] = $message;
		echo json_encode($ex);
	}
	
	function addExpences($urgentaidId,$urgentaidexpId=NULL)
	{
		$this->_data['urgentaidId']		=	$urgentaidId;
		$this->_data['urgentaidexpId']	=	$urgentaidexpId;
		
		if($this->input->post())
		{	
			$data = $this->input->post();
			
			$services["userid"] 		=	$this->_login_userid;;
			$services["urgentaidId"] 	=	$data["urgentaidId"];
			$services["beneficiary"] 	=	$data["beneficiary"];
			$services["articledetails"] =	$data["articledetails"];
			$services["weight"] 		=	$data["weight"];
			$services["qty"] 			=	$data["qty"];
			
			if($data["urgentaidexpId"] >0)
			{
				$services["urgentaidexpId"] = $data["urgentaidexpId"];
			}
			else
			{
				$services["urgentaidexpId"] = '';
			}
			
			$this->urgent_aid->savestore($services,'ah_urgent_aid_expences','urgentaidexpId');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			exit();
		}
		if($urgentaidexpId >0){
			$this->_data["rows"] = $this->urgent_aid->getdata($urgentaidexpId,'ah_urgent_aid_expences',"urgentaidexpId");
		}
		//$this->_data['beneficiary']		=	$this->urgent_aid->get_all_listmanagement('beneficiary');
		$this->_data['all_items']		=	$this->urgent_aid->get_all_items();
		$this->load->view('addExpences',$this->_data);
	}
	
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
				
		if(!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
	function loadexpences(){
		$urgentaidId = $_POST["urgentaidId"];
		
				$this->db->select('*');
				$this->db->from('ah_urgent_aid_expences');						
				$this->db->where("delete_record",'0');
				$this->db->where("urgentaidId",$urgentaidId);
				$this->db->where("delete_record",'0');
				$this->db->order_by('urgentaidexpId','DESC');
			$all_items		=	$this->urgent_aid->get_all_items();
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
			$data = '<table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                <th>التسلسل</th>
                                <th>اسم المادة</th>
                                <th>تفاصيل المادة</th>
                                <th>الوزن للوحدة</th>                 
                                <th>الكمية</th>
								 <th>الإجراءات</th>
                              </tr>
                            </thead>
                            <tbody>                     
                           ';
			$no =1;
			foreach($query->result() as $lc)
			{
				$action ='';
				if($lc->userid == $this->_login_userid){
					$action	.= ' <a href="#globalDiag" onclick="alatadad(this);"  data-url="'.base_url().'aid/addExpences/'.$lc->urgentaidId.'/'.$lc->urgentaidexpId.'" style="margin-left:5px;" ><i class="icon-pencil"></i></a>';
				}
				
				
				if($permissions[$this->moduleid]['d']	==	1 and $lc->status != "Accepted") 
				{
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);loadexpences(\''.$lc->urgentaidId.'\');" id="'.$lc->urgentaidexpId.'" data-url="'.base_url().'aid/delete_expences/'.$lc->urgentaidexpId.'/'.$lc->urgentaidId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
				
				$data .=' <tr><td>'.$no.'</td><td>';
				if(count($all_items)>0){
					foreach($all_items as $item){
						if($lc->beneficiary == $item->itemid){
							$data .= $item->itemname;	
						}
					}
				}
				
				$data .='</td><td>'.$lc->articledetails.'</td><td>'.$lc->weight.'</td><td>'.$lc->qty.'</td><td>'.$action.'</td></tr>';
				$no++;
			}
			$data .=' </tbody>
                          </table>';
			
			$ex['data'] = $data;
			echo json_encode($ex);
	
	}
	function loadmeeting(){
		$urgentaidId = $_POST["urgentaidId"];
		$section = $_POST["section"];
				$this->db->select('*');
				$this->db->from('ah_urgent_meeting');						
				$this->db->where("delete_record",'0');
				$this->db->where("urgentaidId",$urgentaidId);
				$this->db->where("section",$section);
				$this->db->where("delete_record",'0');
				$this->db->order_by('urgentaidmeetId','DESC');
			
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
			$data = '<table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                <th>رقم الاجتماع</th>
                                <th>التاريخ</th>
                                <th>قرار اللجنة</th>
                                <th>الملاحظات</th>
								 <th>ارفاق الصور</th>
								 <th>الإجراءات</th>                
                              </tr>
                            </thead>
                            <tbody>                     
                           ';
			$no =1;
			foreach($query->result() as $lc)
			{
				$action ='';
				if($lc->userid == $this->_login_userid){
					$action	.= ' <a href="#globalDiag" onclick="alatadad(this);"  data-url="'.base_url().'aid/addmeeting/'.$lc->urgentaidId.'/'.$lc->section.'/'.$lc->urgentaidmeetId.'" style="margin-left:5px;" ><i class="icon-pencil"></i></a>';
				}
				
				
				if($permissions[$this->moduleid]['d']	==	1 and $lc->status != "Accepted") 
				{
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->urgentaidmeetId.'" data-url="'.base_url().'aid/delete_meeting/'.$lc->urgentaidmeetId.'/'.$lc->urgentaidId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
				$data .=' <tr><td>'.$lc->meetingno.'</td><td>'.arabic_date($this->dateformat($lc->currentdate)).'</td><td>'.$lc->decision.'</td><td>'.$lc->notes.'</td>
				<td>';
				if($lc->attachment !=""){
					$attachment = @explode(',',$lc->attachment);
				   foreach($attachment as $att){
					   if($att !=""){
						   $data .=$this->getfileicon($att,base_url().'resources/aid/'.$this->_login_userid);
						 
					   }
				   }
				}
				$data .='</td>
				<td>'.$action.'</td></tr>';
				$no++;
			}
			$data .=' </tbody>
                          </table>';
			
			$ex['data'] = $data;
			echo json_encode($ex);
	
	}
	
	function addmeeting($urgentaidId,$section,$urgentaidmeetId = NULL){
		$this->_data['urgentaidId']	 = $urgentaidId;
		$this->_data["urgentaidmeetId"]= $urgentaidmeetId;
		$this->_data["section"]= $section;
		//print_r($_FILES);
		if($this->input->post())
		{	
			$data = $this->input->post();
			$services["userid"] = $this->_login_userid;;
			$services["urgentaidId"] = $data["urgentaidId"];
			$services["currentdate"] = $data["currentdate"];
			$services["decision"] = $data["decision"];
			$services["notes"] = $data["notes"];
			$services["section"] = $data["section"];
			$services["meetingno"] = $data["meetingno"];
			if($data["urgentaidmeetId"] >0){
				$services["urgentaidmeetId"] = $data["urgentaidmeetId"];
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
					$names = array();
					$files = $_FILES["attachment"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
						}
						
					}
					$services['attachment']	=	@implode(',',$names);
				}
			}
			else{
				$services["urgentaidmeetId"] = '';
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
				$names = array();
				$files = $_FILES["attachment"];
				 foreach ($files['name'] as $key => $image) {
					if(!empty($files['name'][$key]))
					{
						$_FILES['images']['name']= $files['name'][$key];
						$_FILES['images']['type']= $files['type'][$key];
						$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['images']['error']= $files['error'][$key];
						$_FILES['images']['size']= $files['size'][$key];
						$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
					}
					
				}
				$services['attachment']	=	@implode(',',$names);
			}
			}
			
			 $this->urgent_aid->savestore($services,'ah_urgent_meeting','urgentaidmeetId');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			//$ex['data'] = $data;
			//echo json_encode($ex);
			//exit();
		}
		if($urgentaidmeetId >0){
			$this->_data["rows"] = $this->urgent_aid->getdata($urgentaidmeetId,'ah_urgent_meeting',"urgentaidmeetId");
		}
		$this->_data['beneficiary']		=	$this->urgent_aid->get_all_listmanagement('beneficiary');
		$this->load->view('addmeeting',$this->_data);
	}
	function view_UrgentAid($urgentaidId)
	{
		if($urgentaidId >0)
		{
			$this->_data['urgentaidId']	=	$urgentaidId;
			$this->_data["rows"] = $rows = $this->urgent_aid->getdata($urgentaidId,'ah_urgent_aid',"urgentaidId");
			
			
			$this->db->select('*');
			$this->db->from('ah_internal_reliefteam');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$urgentaidId);
			$this->db->order_by('extreliefTMId','ASC');
			$query = $this->db->get();
			$this->_data["internal_reliefteam"] = $query->result();
			
			//echo $this->db->last_query();
			//////////////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_internal_relief_committee_meeting');						
			$this->db->where("delete_record",'0');
			$this->db->where("urgentaidId",$urgentaidId);
			$this->db->order_by('internalMeetingId','ASC');
			$query = $this->db->get();
			$this->_data["ah_internal_relief_committee_meeting"] = $query->result();
			//////////////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_internal_final_relief_committee_meeting');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$urgentaidId);
			$this->db->order_by('extrelMeetingId','ASC');
			$query = $this->db->get();
			$this->_data["ah_internal_final_relief_committee_meeting"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_internal_requirements_required');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$urgentaidId);
			$this->db->order_by('requirementsId','ASC');
			$query1 = $this->db->get();
			$this->_data["internal_requirements_required"] = $query1->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_internal_expenses');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$urgentaidId);
			$this->db->order_by('extExpencesId','ASC');
			$query = $this->db->get();
			$this->_data["internal_expenses"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_internal_final_report');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$urgentaidId);
			$this->db->order_by('extDataReportId','ASC');
			$query = $this->db->get();
			$this->_data["ah_internal_final_report"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_internal_daily_report');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$urgentaidId);
			$this->db->order_by('extDataReportId','ASC');
			$query = $this->db->get();
			$this->_data["internal_daily_report"] = $query->result();
			
			
		}
		$this->_data['all_items']		=	$this->urgent_aid->get_all_items();
		$this->load->view('view_UrgentAid',$this->_data);
	}
	function delete_expences($urgentaidexpId,$urgentaidId){
		$this->urgent_aid->delete($urgentaidexpId,'urgentaidexpId',"ah_urgent_aid_expences");
		redirect(base_url().'aid/addUrgentAid/'.$urgentaidId);
		exit();
	}
	function delete_meeting($urgentaidmeetId,$urgentaidId){
		$this->urgent_aid->delete($urgentaidmeetId,'urgentaidmeetId',"ah_urgent_meeting");
		redirect(base_url().'aid/addUrgentAid/'.$urgentaidId);
		exit();
	}
	function delete($urgentaidId){
		$this->urgent_aid->delete($urgentaidId,'urgentaidId',"ah_urgent_aid");
		$this->urgent_aid->delete($urgentaidId,'urgentaidId',"ah_urgent_meeting");
		$this->urgent_aid->delete($urgentaidId,'urgentaidId',"ah_urgent_aid_expences");
		redirect(base_url().'aid/addUrgentAid/'.$urgentaidId);
		exit();
	}
	function addfood($aidfoodId = NULL,$Typerelief=''){
		
		$this->_data['Typerelief']		= $Typerelief;
		$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
		$this->_data['reliefType']		=	$this->urgent_aid->get_all_listmanagement('donation_type');
		$this->_data['beneficiary']		=	$this->urgent_aid->get_all_listmanagement('beneficiary');
		$this->_data['issuecountry']		=	$this->urgent_aid->get_all_listmanagement('issuecountry');
		$this->db->select('aidfoodId');
		$this->db->from('ah_aidfood');						
		$this->db->order_by('aidfoodId','DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$count =1;
		foreach($query->result() as $lc){
			$count =$lc->aidfoodId;
		}
		$this->_data["count"]=++$count;
		if($this->input->post())
		{
			$data	=	$this->input->post();
			$services	=	array();
			$services["reliefType"]	= $data["reliefType"];
			$services["beneficiary"]	= $data["beneficiary"];
			$services["currentdate"]	= $data["currentdate"];
			$services["beneficiaryName"]	= $data["beneficiaryName"];
			$services["age"]	= $data["age"];
			$services["phone"]	= $data["phone"];
			$services["projectTitle"]	= $data["projectTitle"];
			$services["beneficiaries"]	= $data["beneficiaries"];
			$services["country"]	= $data["country"];
			$services["province"]	= $data["province"];
			$services["city"]	= $data["city"];
			$services["support"]	= $data["support"];
			if($data["aidfoodId"] >0 && $services["projectTitle"] !=""){
				$aidfoodId =$services["aidfoodId"] = $data["aidfoodId"];
				$this->urgent_aid->savestore($services,'ah_aidfood','aidfoodId');
			}
			else if( $services["projectTitle"] !=""){
				$services["aidfoodId"] = '';
				$services["status"]	= 'Pending';
				$services['userid']	=	$this->_login_userid;
				$aidfoodId = $this->urgent_aid->savestore($services,'ah_aidfood','aidfoodId');
			}
			$this->_data["aidfoodId"]= $aidfoodId;
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			redirect(base_url().'aid/addfood/'.$aidfoodId);
			exit();
			
		}
		if($aidfoodId >0){
			$this->_data["rows"] = $rows =  $this->urgent_aid->getdata($aidfoodId,'ah_aidfood',"aidfoodId");
			$this->_data['province']		=	$this->urgent_aid->get_all_listmanagement('issuecountry',$rows->country);
			$this->_data['city']		=	$this->urgent_aid->get_all_listmanagement('issuecountry',$rows->province);
		}
		
		$this->load->view('addfood',$this->_data);
	}//function addfood(){
	function loadaidfood_expulsion(){
		$aidfoodId = $_POST["aidfoodId"];
		
				$this->db->select('*');
				$this->db->from('ah_aidfood_expulsion');						
				$this->db->where("delete_record",'0');
				$this->db->where("aidfoodId",$aidfoodId);
				$this->db->where("delete_record",'0');
				$this->db->order_by('aidfoodExpId','DESC');
			
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
			$data = '<table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                <th>التسلسل</th>
                                <th>اسم المادة</th>
                                <th>تفاصيل المادة</th>
                                <th>الوزن للوحدة</th>                 
                                <th>الكمية</th>
								 <th>الملاحظات</th>
								 <th>الإجراءات</th>
                              </tr>
                            </thead>
                            <tbody>                     
                           ';
			$no =1;
			$all_items		=	$this->urgent_aid->get_all_items();
			foreach($query->result() as $lc)
			{
				$action ='';
				if($lc->userid == $this->_login_userid){
					$action	.= ' <a href="#globalDiag" onclick="alatadad(this);"  data-url="'.base_url().'aid/addFoodexpulsion/'.$lc->aidfoodId.'/'.$lc->aidfoodExpId.'" style="margin-left:5px;" ><i class="icon-pencil"></i></a>';
				}
				
				
				if($permissions[$this->moduleid]['d']	==	1 and $lc->status != "Accepted") 
				{
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->urgentaidexpId.'" data-url="'.base_url().'aid/delete_Foodexpulsion/'.$lc->aidfoodExpId.'/'.$lc->aidfoodId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
				
				$data .=' <tr><td>'.$no.'</td><td>';
				if(count($all_items)>0){
					foreach($all_items as $item){
						if($lc->subjectName == $item->itemid){
							$data .= $item->itemname;	
						}
					}
				}
				$data .='</td><td>'.$lc->articledetails.'</td><td>'.$lc->weight.'</td><td>'.$lc->qty.'</td><td>'.$lc->notes.'</td><td>'.$action.'</td></tr>';
				$no++;
			}
			$data .=' </tbody>
                          </table>';
			
			$ex['data'] = $data;
			echo json_encode($ex);
	
	}
	function delete_Foodexpulsion($aidfoodExpId,$aidfoodId){
		$this->urgent_aid->delete($aidfoodExpId,'aidfoodExpId',"ah_aidfood_expulsion");
		redirect(base_url().'aid/addfood/'.$aidfoodId);
		exit();
	}
	function addFoodexpulsion($aidfoodId,$aidfoodExpId=NULL){
		$this->_data['aidfoodId']	 = $aidfoodId;
		$this->_data['aidfoodExpId']	 = $aidfoodExpId;
		$this->_data['all_items']		=	$this->urgent_aid->get_all_items();
		if($this->input->post())
		{	
			$data = $this->input->post();
			$services["userid"] = $this->_login_userid;;
			$services["subjectName"] = $data["subjectName"];
			$services["articledetails"] = $data["articledetails"];
			$services["weight"] = $data["weight"];
			$services["qty"] = $data["qty"];
			$services["notes"] = $data["notes"];
			$services["aidfoodId"] = $data["aidfoodId"];
			if($data["aidfoodExpId"] >0){
				$services["aidfoodExpId"] = $data["aidfoodExpId"];
			}
			else{
				$services["aidfoodExpId"] = '';
			}
			 $this->urgent_aid->savestore($services,'ah_aidfood_expulsion','aidfoodExpId');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			exit();
		}
		if($aidfoodExpId >0){
			$this->_data["rows"] = $this->urgent_aid->getdata($aidfoodExpId,'ah_aidfood_expulsion',"aidfoodExpId");
		}
		//$this->_data['beneficiary']		=	$this->urgent_aid->get_all_listmanagement('beneficiary');
		$this->load->view('addFoodexpulsion',$this->_data);
	}
	function loadaidfood_unloading(){
		$aidfoodId = $_POST["aidfoodId"];
		$this->db->select('ah_company.companyid,ah_company.arabic_name');
        $this->db->from('ah_company');
        $this->db->order_by("ah_company.registrationdate", "DESC");
		$query1 = $this->db->get();
		$company = $query1->result();
		
		
				$this->db->select('*');
				$this->db->from('ah_aidfood_unloading');						
				$this->db->where("delete_record",'0');
				$this->db->where("aidfoodId",$aidfoodId);
				$this->db->where("delete_record",'0');
				$this->db->order_by('aidfoodUnId','DESC');
			
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
			$data = '<table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                <th>التسلسل</th>
                                <th>اسم الشركة</th>
                                <th>المبلغ</th>
                                <th>عدد الطرود</th>                 
                                <th>الملاحظات</th>
								<th>ارفاق عروض الأسع</th>
								 <th>الملاحظات</th>
                              </tr>
                            </thead>
                            <tbody>                     
                           ';
			$no =1;
			foreach($query->result() as $lc)
			{
				$action ='';
				if($lc->userid == $this->_login_userid){
					$action	.= ' <a href="#globalDiag" onclick="alatadad(this);"  data-url="'.base_url().'aid/addaidfood_unloading/'.$lc->aidfoodId.'/'.$lc->aidfoodUnId.'" style="margin-left:5px;" ><i class="icon-pencil"></i></a>';
				}
				
				
				if($permissions[$this->moduleid]['d']	==	1 and $lc->status != "Accepted") 
				{
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->aidfoodUnId.'" data-url="'.base_url().'aid/delete_food_unloading/'.$lc->aidfoodUnId.'/'.$lc->aidfoodId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
				$data .=' <tr><td>'.$no.'</td><td>';
				if(count($company)>0){
					  foreach($company as $type){
						  if($lc->companyName == $type->companyid){
							  $data .= $type->arabic_name;
						  }
					  }
				  }
				$data .='</td><td>'.$lc->amount.'</td><td>'.$lc->noOfPackages.'</td><td>'.$lc->notes.'</td><td>';
				if($lc->attachment !=""){
					$attachment = @explode(',',$lc->attachment);
				   foreach($attachment as $att){
					   if($att !=""){
						   $data .=$this->getfileicon($att,base_url().'resources/aid/'.$this->_login_userid);
					   }
				   }
				}
				$data .='</td><td>'.$action.'</td></tr>';
				$no++;
			}
			$data .=' </tbody>
                          </table>';
			
			$ex['data'] = $data;
			echo json_encode($ex);
	
	}
	function delete_food_unloading($aidfoodUnId,$aidfoodId){
		$this->urgent_aid->delete($aidfoodUnId,'aidfoodUnId',"ah_aidfood_unloading");
		redirect(base_url().'aid/addfood/'.$aidfoodId);
		exit();
	}
	function addaidfood_unloading($aidfoodId,$aidfoodUnId=NULL){
		$this->_data['aidfoodId']	 = $aidfoodId;
		$this->_data['aidfoodUnId']	 = $aidfoodUnId;
		$this->db->select('ah_company.companyid,ah_company.arabic_name');
        $this->db->from('ah_company');
        $this->db->order_by("ah_company.registrationdate", "DESC");
		$query1 = $this->db->get();
		$this->_data["company"] = $query1->result();
		
		if($this->input->post())
		{	
			$data = $this->input->post();
			$services["userid"] = $this->_login_userid;;
			$services["companyName"] = $data["companyName"];
			$services["amount"] = $data["amount"];
			$services["noOfPackages"] = $data["noOfPackages"];
			$services["notes"] = $data["notes"];
			$services["aidfoodId"] = $data["aidfoodId"];
			if($data["aidfoodUnId"] >0){
				$services["aidfoodUnId"] = $data["aidfoodUnId"];
				//print_r($_FILES["attachment"]);exit();
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
					$names = array();
					$files = $_FILES["attachment"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
						}
						
					}
					$services['attachment']	=	@implode(',',$names);
				}
				else{
					$services['attachment']	=	$data["attachment_old"];
				}
			}
			else{
				$services["aidfoodUnId"] = '';
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
				$names = array();
				$files = $_FILES["attachment"];
				 foreach ($files['name'] as $key => $image) {
					if(!empty($files['name'][$key]))
					{
						$_FILES['images']['name']= $files['name'][$key];
						$_FILES['images']['type']= $files['type'][$key];
						$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['images']['error']= $files['error'][$key];
						$_FILES['images']['size']= $files['size'][$key];
						$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
					}
					
				}
				$services['attachment']	=	@implode(',',$names);
			}
			}
			 $this->urgent_aid->savestore($services,'ah_aidfood_unloading','aidfoodUnId');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			exit();
		}
		if($aidfoodUnId >0){
			$this->_data["rows"] = $this->urgent_aid->getdata($aidfoodUnId,'ah_aidfood_unloading',"aidfoodUnId");
		}
		//$this->_data['beneficiary']		=	$this->urgent_aid->get_all_listmanagement('beneficiary');
		$this->load->view('addaidfood_unloading',$this->_data);
	}
	function listallaidfood_meeting(){
		$aidfoodId = $_POST["aidfoodId"];
		$section = $_POST["section"];
		
				$this->db->select('*');
				$this->db->from(' ah_aidfood_meeting');						
				$this->db->where("delete_record",'0');
				$this->db->where("aidfoodId",$aidfoodId);
				$this->db->where("section",$section);
				$this->db->where("delete_record",'0');
				$this->db->order_by('aidfoodMeetingId','DESC');
			
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
			$data = '<table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                <th>رقم الاجتماع</th>
                                <th>التاريخ</th>
                                <th>قرار اللجنة</th>
                                <th>الملاحظات</th> 
								<th>اسم الشركة</th> 
								<th>ارفاق الصور</th>                 
								 <th>الإجراءات</th>
                              </tr>
                            </thead>
                            <tbody>                     
                           ';
			$no =1;
			$this->db->select('ah_company.companyid,ah_company.arabic_name');
        $this->db->from('ah_company');
        $this->db->order_by("ah_company.registrationdate", "DESC");
		$query1 = $this->db->get();
		$company = $query1->result();
			foreach($query->result() as $lc)
			{
				$action ='';
				if($lc->userid == $this->_login_userid){
					$action	.= ' <a href="#globalDiag" onclick="alatadad(this);"  data-url="'.base_url().'aid/addaidfood_meeting/'.$lc->aidfoodId.'/'.$section.'/'.$lc->aidfoodMeetingId.'" style="margin-left:5px;" ><i class="icon-pencil"></i></a>';
				}
				
				
				if($permissions[$this->moduleid]['d']	==	1 and $lc->status != "Accepted") 
				{
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->aidfoodMeetingId.'" data-url="'.base_url().'aid/delete_aidfood_meeting/'.$lc->aidfoodMeetingId.'/'.$section.'/'.$lc->aidfoodId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
				
				$data .=' <tr><td>'.$no.'</td><td>'.$this->dateformat($lc->currentdate).'</td><td>'.$lc->decision.'</td><td>'.$lc->notes.'</td><td>';
				 if(count($company)>0){
					  foreach($company as $type){
						  if($lc->companyName == $type->companyid){
							  $data .=$type->arabic_name;
						  }
					  }
				  }
				$data .='</td> <td>';
				
				if($lc->attachment !=""){
					$attachment = @explode(',',$lc->attachment);
				   foreach($attachment as $att){
					   if($att !=""){
						   $data .=$this->getfileicon($att,base_url().'resources/aid/'.$this->_login_userid);
						 
					   }
				   }
				}
				$data .='</td><td>'.$action.'</td></tr>';
				$no++;
			}
			$data .=' </tbody>
                          </table>';
			$ex['data'] = $data;
			echo json_encode($ex);
	
	}
	function delete_aidfood_meeting($aidfoodMeetingId,$aidfoodId){
		$this->urgent_aid->delete($aidfoodMeetingId,'aidfoodMeetingId',"ah_aidfood_meeting");
		redirect(base_url().'aid/addfood/'.$aidfoodId);
		exit();
	}
	function addaidfood_meeting($aidfoodId,$section,$aidfoodMeetingId = NULL){
		$this->db->select('aidfoodMeetingId');
		$this->db->from('ah_aidfood_meeting');						
		$this->db->order_by('aidfoodMeetingId','DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$count =1;
		foreach($query->result() as $lc){
			$count =$lc->aidfoodMeetingId;
		}
		$this->_data["count"]=++$count;
		
		$this->_data['aidfoodId']	 = $aidfoodId;
		$this->_data["aidfoodMeetingId"]= $aidfoodMeetingId;
		$this->_data["section"]= $section;
		$this->db->distinct('ah_company.companyid');
		$this->db->select('ah_company.arabic_name,ah_company.companyid');
        $this->db->from('ah_company');
		$this->db->join('ah_aidfood_unloading',"ah_aidfood_unloading.companyName = ah_company.companyid",'inner');
		$this->db->where('ah_aidfood_unloading.aidfoodId',$aidfoodId);
        $this->db->order_by("ah_company.registrationdate", "DESC");
		$query1 = $this->db->get();
		$this->_data["company"] = $query1->result();
		if($this->input->post())
		{	
			$data = $this->input->post();
			$services["userid"] = $this->_login_userid;;
			$services["aidfoodId"] = $data["aidfoodId"];
			$services["currentdate"] = $data["currentdate"];
			$services["decision"] = $data["decision"];
			$services["notes"] = $data["notes"];
			$services["section"] = $data["section"];
			$services["companyName"] = $data["companyName"];
			if($data["aidfoodMeetingId"] >0){
				$services["aidfoodMeetingId"] = $data["aidfoodMeetingId"];
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
					$names = array();
					$files = $_FILES["attachment"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
						}
						
					}
					$services['attachment']	=	@implode(',',$names);
				}
			}
			else{
				$services["aidfoodMeetingId"] = '';
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
				$names = array();
				$files = $_FILES["attachment"];
				 foreach ($files['name'] as $key => $image) {
					if(!empty($files['name'][$key]))
					{
						$_FILES['images']['name']= $files['name'][$key];
						$_FILES['images']['type']= $files['type'][$key];
						$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['images']['error']= $files['error'][$key];
						$_FILES['images']['size']= $files['size'][$key];
						$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
					}
					
				}
				$services['attachment']	=	@implode(',',$names);
			}
			}
			
			 $this->urgent_aid->savestore($services,'ah_aidfood_meeting','aidfoodMeetingId');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			//$ex['data'] = $data;
			//echo json_encode($ex);
			//exit();
		}
		if($aidfoodMeetingId >0){
			$this->_data["rows"] = $this->urgent_aid->getdata($aidfoodMeetingId,'ah_aidfood_meeting',"aidfoodMeetingId");
		}
		$this->_data['beneficiary']		=	$this->urgent_aid->get_all_listmanagement('beneficiary');
		$this->load->view('aidfood_addmeeting',$this->_data);
	}
	function loadaidfood_distributionlist(){
		$aidfoodId = $_POST["aidfoodId"];
		$this->db->select('ah_company.companyid,ah_company.arabic_name');
        $this->db->from('ah_company');
        $this->db->order_by("ah_company.registrationdate", "DESC");
		$query1 = $this->db->get();
		$company = $query1->result();
		
		
				$this->db->select('*');
				$this->db->from('ah_aidfood_distributionlist');						
				$this->db->where("delete_record",'0');
				$this->db->where("aidfoodId",$aidfoodId);
				$this->db->where("delete_record",'0');
				$this->db->order_by('aidfoodDisId','DESC');
			
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
			$data = '<table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                <th>التسلسل</th>
                                <th>اسم الشركة</th>
                                <th>المحافظة / المنطقة</th>
								<th>الولاية</th>
								<th>عدد الطرود</th>
								<th>منطقة التوزيع</th>
								<th>اجمالي الصرف</th>
								 <th>الملاحظات</th>
                              </tr>
                            </thead>
                            <tbody>                     
                           ';
			$no =1;
			$total_noOfPackage =0;
			$total_totalExchange =0;
			if($query->num_rows()>0){
				foreach($query->result() as $lc)
				{
					$action ='';
					if($lc->userid == $this->_login_userid){
						$action	.= ' <a href="#globalDiag" onclick="alatadad(this);"  data-url="'.base_url().'aid/addaidfood_distributionlist/'.$lc->aidfoodId.'/'.$lc->aidfoodDisId.'" style="margin-left:5px;" ><i class="icon-pencil"></i></a>';
					}
					
					
					if($permissions[$this->moduleid]['d']	==	1 and $lc->status != "Accepted") 
					{
						$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->aidfoodDisId.'" data-url="'.base_url().'aid/delete_food_distributionlist/'.$lc->aidfoodDisId.'/'.$lc->aidfoodId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
					}
					$data .=' <tr><td>'.$no.'</td><td>';
					
					if(count($company)>0){
						  foreach($company as $type){
							  if($lc->companyName == $type->companyid){
								  $data .= $type->arabic_name;
							  }
						  }
					  }
					  $total_noOfPackage = $total_noOfPackage +$lc->noOfPackage;
					  $total_totalExchange = $total_totalExchange +$lc->totalExchange;
					  $province		=	$this->urgent_aid->get_all_listmanagement('issuecountry',200,$lc->region);
						$city		=	$this->urgent_aid->get_all_listmanagement('issuecountry',$lc->region,$lc->state);
						//print_r( $province);
					$data .='</td><td>'.$province[0]->list_name.'</td><td>'.$city[0]->list_name.'</td><td>'.$lc->noOfPackage.'</td><td>'.$lc->distributionArea.'</td> <td>'.$lc->totalExchange.'</td><td>'.$action.'</td></tr>';
					$no++;
				}
				
				$this->db->select('*');
				$this->db->from('ah_aidfood_distributionlist');						
				$this->db->where("delete_record",'0');
				$this->db->where("aidfoodId",$aidfoodId);
				$this->db->order_by('aidfoodDisId','DESC');
			
				$query = $this->db->get();
				$total_noOfPackage =0;
				foreach($query->result() as $lc)
				{
					 $total_noOfPackage = $total_noOfPackage +$lc->noOfPackage;
				 
				}
				$this->db->select('beneficiaries');
				$this->db->from('ah_aidfood');						
				$this->db->where("delete_record",'0');
				$this->db->where("aidfoodId",$aidfoodId);
				$this->db->order_by('aidfoodId','DESC');
				$query1 = $this->db->get();
				foreach($query1->result() as $lc)
				{
					 $beneficiaries =$lc->beneficiaries;
				 
				}
				
				$tot = $beneficiaries - $total_noOfPackage;
				
				$data .=' <tr><td colspan="4"><strong style="color:#F00;">إجمالي متبقيات للمستفيدين: <span class="balance_listing">'.$tot.'</span></strong></td><td style="color:#029625;"><b>  اجمالي عدد الطرود: </b><b>'.$total_noOfPackage.'</b></td><td></td> <td style="color:#029625;"><b> اجمالي الصرف: </b><b>'.$total_totalExchange.'</b></td></tr>';
			}
			$data .=' </tbody>
                          </table>';
			
			$ex['data'] = $data;
			echo json_encode($ex);
	
	}
	function delete_food_distributionlist($aidfoodDisId,$aidfoodId){
		$this->urgent_aid->delete($aidfoodDisId,'aidfoodDisId',"ah_aidfood_distributionlist");
		redirect(base_url().'aid/addfood/'.$aidfoodId);
		exit();
	}
	
	function addaidfood_distributionlist($aidfoodId,$aidfoodDisId=NULL){
		$this->_data['aidfoodId']	 = $aidfoodId;
		$this->_data['aidfoodDisId']	 = $aidfoodDisId;
		$this->db->distinct('ah_company.companyid');
		$this->db->select('ah_company.arabic_name,ah_company.companyid');
        $this->db->from('ah_company');
		$this->db->join('ah_aidfood_unloading',"ah_aidfood_unloading.companyName = ah_company.companyid",'inner');
		$this->db->where('ah_aidfood_unloading.aidfoodId',$aidfoodId);
        $this->db->order_by("ah_company.registrationdate", "DESC");
		$query1 = $this->db->get();
		$this->_data["company"] = $query1->result();
		$this->_data["main"] = $this->urgent_aid->getdata($aidfoodId,'ah_aidfood',"aidfoodId");
		if($this->input->post())
		{	
			$data = $this->input->post();
			$services["userid"] = $this->_login_userid;;
			$services["companyName"] = $data["companyName"];
			$services["region"] = $data["region"];
			$services["state"] = $data["state"];
			$services["noOfPackage"] = $data["noOfPackage"];
			$services["distributionArea"] = $data["distributionArea"];
			$services["aidfoodId"] = $data["aidfoodId"];
			$services["totalExchange"] = 0;
			
			$this->db->select('amount,companyName');
			$this->db->from('ah_aidfood_unloading');
			$this->db->where("companyName",$services["companyName"]);
			$this->db->where('aidfoodId',$aidfoodId);
			$query1 = $this->db->get();
			
			$amount = $query1->row();
			//if($amount->amount >0){
				$services["totalExchange"] = $services["noOfPackage"] * $amount->amount;
			//}
			
			
			if($data["aidfoodDisId"] >0){
				$services["aidfoodDisId"] = $data["aidfoodDisId"];
				
			}
			else{
				$services["aidfoodDisId"] = '';
				
			}
			 $this->urgent_aid->savestore($services,'ah_aidfood_distributionlist','aidfoodDisId');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			exit();
		}
		if($aidfoodDisId >0){
			$this->_data["rows"] = $this->urgent_aid->getdata($aidfoodDisId,'ah_aidfood_distributionlist',"aidfoodDisId");
		}
		$this->_data['province']		=	$this->urgent_aid->get_all_listmanagement('issuecountry',200);
		$this->load->view('addaidfood_distributionlist',$this->_data);
	}
	function balance_listing(){
		$aidfoodId = $_POST["aidfoodId"];
		$beneficiaries = $_POST["beneficiaries"];
		$this->db->select('*');
				$this->db->from('ah_aidfood_distributionlist');						
				$this->db->where("delete_record",'0');
				$this->db->where("aidfoodId",$aidfoodId);
				$this->db->where("delete_record",'0');
				$this->db->order_by('aidfoodDisId','DESC');
			
			$query = $this->db->get();
		$total_noOfPackage =0;
		foreach($query->result() as $lc)
		{
			 $total_noOfPackage = $total_noOfPackage +$lc->noOfPackage;
		 
		}
		$data = $beneficiaries - $total_noOfPackage;
		
		
		$ex['data'] = $data;
		echo json_encode($ex);
	}
	function loadaidfoodactualcost(){
		$aidfoodId = $_POST["aidfoodId"];
		$this->db->select('ah_company.companyid,ah_company.arabic_name');
        $this->db->from('ah_company');
        $this->db->order_by("ah_company.registrationdate", "DESC");
		$query1 = $this->db->get();
		$company = $query1->result();
		
		
				$this->db->select('*');
				$this->db->from('ah_aidfoodactualcost');						
				$this->db->where("delete_record",'0');
				$this->db->where("aidfoodId",$aidfoodId);
				$this->db->where("delete_record",'0');
				$this->db->order_by('aidfoodCostId','DESC');
			
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
			$data = '<table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
                                <th>التسلسل</th>
                                 <th>اسم الشركة</th>
								 <th>منطقة التوزيع</th>
								 <th>عدد الطرود</th>
								 <th>قيمة الطرد</th>
								 <th>اجمالي تكلفة الطرد</th>
								 <th>الملاحظات</th>
                              </tr>
                            </thead>
                            <tbody>                     
                           ';
			$no =1;
			if($query->num_rows()>0){
				foreach($query->result() as $lc)
				{
					$action ='';
					if($lc->userid == $this->_login_userid){
						$action	.= ' <a href="#globalDiag" onclick="alatadad(this);"  data-url="'.base_url().'aid/addaidfood_distributionlist/'.$lc->aidfoodId.'/'.$lc->aidfoodCostId.'" style="margin-left:5px;" ><i class="icon-pencil"></i></a>';
					}
					
					
					if($permissions[$this->moduleid]['d']	==	1 and $lc->status != "Accepted") 
					{
						$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->aidfoodDisId.'" data-url="'.base_url().'aid/delete_food_actualcost/'.$lc->aidfoodCostId.'/'.$lc->aidfoodCostId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
					}
					$data .=' <tr><td>'.$no.'</td><td>';
					
					if(count($company)>0){
						  foreach($company as $type){
							  if($lc->companyName == $type->companyid){
								  $data .= $type->arabic_name;
							  }
						  }
					  }
					$data .='</td><td>'.$lc->distributionArea.'</td><td>'.$lc->noOfPackages.'</td><td>'.$lc->expulsionValue.'</td><td>'.$lc->totalCostOfPackages.'</td> <td>'.$action.'</td></tr>';
					$no++;
				}
			}
			$data .=' </tbody>
                          </table>';
			
			$ex['data'] = $data;
			echo json_encode($ex);
	
	}
	function delete_food_actualcost($aidfoodCostId,$aidfoodId){
		$this->urgent_aid->delete($aidfoodCostId,'aidfoodCostId',"ah_aidfoodactualcost");
		redirect(base_url().'aid/addfood/'.$aidfoodId);
		exit();
	}
	function addaidfood_actualcost($aidfoodId,$aidfoodCostId=NULL){
		$this->_data['aidfoodId']	 = $aidfoodId;
		$this->_data['aidfoodCostId']	 = $aidfoodCostId;
		$this->db->distinct('ah_company.companyid');
		$this->db->select('ah_company.arabic_name,ah_company.companyid');
        $this->db->from('ah_company');
		$this->db->join('ah_aidfood_unloading',"ah_aidfood_unloading.companyName = ah_company.companyid",'inner');
		$this->db->where('ah_aidfood_unloading.aidfoodId',$aidfoodId);
        $this->db->order_by("ah_company.registrationdate", "DESC");
		$query1 = $this->db->get();
		$this->_data["company"] = $query1->result();
		
		if($this->input->post())
		{	
			$data = $this->input->post();
			$services["userid"] = $this->_login_userid;;
			$services["companyName"] = $data["companyName"];
			$services["distributionArea"] = $data["distributionArea"];
			$services["noOfPackages"] = $data["noOfPackages"];
			$services["expulsionValue"] = $data["expulsionValue"];
			$services["totalCostOfPackages"] = $data["totalCostOfPackages"];
			$services["aidfoodId"] = $data["aidfoodId"];
			
			
			
			if($data["aidfoodCostId"] >0){
				$services["aidfoodCostId"] = $data["aidfoodCostId"];
				
			}
			else{
				$services["aidfoodCostId"] = '';
				
			}
			 $this->urgent_aid->savestore($services,'ah_aidfoodactualcost','aidfoodCostId');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			exit();
		}
		if($aidfoodDisId >0){
			$this->_data["rows"] = $this->urgent_aid->getdata($aidfoodCostId,'ah_aidfoodactualcost',"aidfoodCostId");
		}
		$this->_data['province']		=	$this->urgent_aid->get_all_listmanagement('issuecountry',200);
		$this->load->view('addaidfood_actualcost',$this->_data);
	}
	function fooddelete($aidfoodId){
		$this->urgent_aid->delete($aidfoodId,'aidfoodId',"ah_aidfood");
		$this->urgent_aid->delete($aidfoodId,'aidfoodId',"ah_aidfoodactualcost");
		$this->urgent_aid->delete($aidfoodId,'aidfoodId',"ah_aidfood_distributionlist");
		$this->urgent_aid->delete($aidfoodId,'aidfoodId',"ah_aidfood_expulsion");
		$this->urgent_aid->delete($aidfoodId,'aidfoodId',"ah_aidfood_meeting");
		$this->urgent_aid->delete($aidfoodId,'aidfoodId',"ah_aidfood_unloading");
		
		redirect(base_url().'aid/');
		exit();
	}
	
	function view_foodAid($aidfoodId){
		
		$reliefType		=	$this->urgent_aid->get_all_listmanagement('donation_type');
		$beneficiary		=	$this->urgent_aid->get_all_listmanagement('beneficiary');
		$issuecountry		=	$this->urgent_aid->get_all_listmanagement('issuecountry');
		
		$this->_data["aidfood"] =$rows = $this->urgent_aid->getdata($aidfoodId,'ah_aidfood',"aidfoodId");
		
		$province		=	$this->urgent_aid->get_all_listmanagement('issuecountry',$rows->country);
			$city		=	$this->urgent_aid->get_all_listmanagement('issuecountry',$rows->province);
		
		foreach($reliefType as $type){
				if($type->list_id == $rows->reliefType)
					$this->_data["reliefType"] = $type->list_name;
		}
		foreach($beneficiary as $type){
				if($type->list_id == $rows->beneficiary)
					$this->_data["beneficiary"] = $type->list_name;
		}
		foreach($issuecountry as $type){
			if($type->list_id == $rows->country)
				$this->_data["country"] = $type->list_name;
		}
		
		foreach($province as $type){
				if($type->list_id == $rows->province)
					$this->_data["province"] = $type->list_name;
		}
		foreach($city as $type){
			if($type->list_id == $rows->city)
				$this->_data["city"] = $type->list_name;
		}
		
		$this->_data["all_items"] =	$this->urgent_aid->get_all_items();
		$this->db->select('*');
		$this->db->from('ah_aidfood_expulsion');						
		$this->db->where("delete_record",'0');
		$this->db->where("aidfoodId",$aidfoodId);
		$this->db->where("delete_record",'0');
		$this->db->order_by('aidfoodExpId','DESC');
		$query = $this->db->get();
		$this->_data["expulsion"] = $query->result();
		
		//////////////////////////
		
		$this->db->select('ah_company.companyid,ah_company.arabic_name');
        $this->db->from('ah_company');
        $this->db->order_by("ah_company.registrationdate", "DESC");
		$query1 = $this->db->get();
		$this->_data["company"] = $query1->result();
		
		
		$this->db->select('*');
		$this->db->from('ah_aidfood_unloading');						
		$this->db->where("delete_record",'0');
		$this->db->where("aidfoodId",$aidfoodId);
		$this->db->order_by('aidfoodUnId','DESC');
		$query = $this->db->get();
		$this->_data["unloading"] = $query->result();
		///////////////////////////////////////////////////////////
		$this->db->select('*');
		$this->db->from(' ah_aidfood_meeting');						
		$this->db->where("delete_record",'0');
		$this->db->where("aidfoodId",$aidfoodId);
		$this->db->where("section",1);
		$this->db->where("delete_record",'0');
		$this->db->order_by('aidfoodMeetingId','DESC');
		$query = $this->db->get();
		$this->_data["meeting1"] = $query->result();
		/////////////////////////////////////////////////////////////////
		
		///////////////////////////////////////////////////////////
		$this->db->select('*');
		$this->db->from(' ah_aidfood_meeting');						
		$this->db->where("delete_record",'0');
		$this->db->where("aidfoodId",$aidfoodId);
		$this->db->where("section",2);
		$this->db->where("delete_record",'0');
		$this->db->order_by('aidfoodMeetingId','DESC');
		$query = $this->db->get();
		$this->_data["meeting2"] = $query->result();
		/////////////////////////////////////////////////////////////////
		$this->db->select('*');
		$this->db->from('ah_aidfood_distributionlist');						
		$this->db->where("delete_record",'0');
		$this->db->where("aidfoodId",$aidfoodId);
		$this->db->where("delete_record",'0');
		$this->db->order_by('aidfoodDisId','DESC');
	
		$query = $this->db->get();
		$this->_data["distributionlist"] = $query->result();
		/////////////////////////////////////////////////////////////
		$this->db->select('*');
		$this->db->from('ah_aidfoodactualcost');						
		$this->db->where("delete_record",'0');
		$this->db->where("aidfoodId",$aidfoodId);
		$this->db->where("delete_record",'0');
		$this->db->order_by('aidfoodCostId','DESC');
		
		$query = $this->db->get();
		$this->_data["actualcost"] = $query->result();
		
		$this->_data["aidfoodId"] = $aidfoodId;
		
		$this->load->view('view_foodAid',$this->_data);
	}
	function external_relief(){
		$permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		$this->load->view('listall_external_relief',$this->_data);
	}
	
	public function ajax_all_datas_external_relief()
	{
				$this->db->select('extreliefId,autoid,country,currentdate,relief,userid');
				$this->db->from('ah_external_relief');						
				$this->db->where("delete_record",'0');
				//$this->db->where("userid",$this->_login_userid);
				$this->db->order_by('status','DESC');
				$this->db->order_by('extreliefId','DESC');
			
			$query = $this->db->get();
			$permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
			
			
			foreach($query->result() as $lc)
			{
				
				
				if($lc->extreliefId)
				{
					$action 	='<a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_relief/'.$lc->extreliefId.'" href="#"><i class="my icon icon-eye-open"></i></a>';
					
					
				}
				
				if($lc->userid == $this->_login_userid){
					$action	.= ' <a href="'.base_url().'aid/addexternal_relief/'.$lc->extreliefId.'" ><i class="icon-pencil"></i></a>';
				}
	
				if($permissions[$this->external_moduleid]['d']	==	1 ) 
				{
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->extreliefId.'" data-url="'.base_url().'aid/delete_external_relief/'.$lc->extreliefId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}

			
				$arr[] = array(
					"DT_RowId"		=>	$lc->extreliefId.'_durar_lm',
					"التسلسل" 		=> $lc->autoid,	
					"التاريخ" 		=>	arabic_date($this->dateformat($lc->currentdate)),
					"الإغاثة" 		=>	$lc->relief,
					"الدولة" 		=>	$this->haya_model->get_name_from_list($lc->country),
					"الإجراءات" 		=>	$action //$this->haya_model->get_name_from_list($lc->country_id),
					);
					
					unset($action,$items);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
	}
	function addexternal_relief($extreliefId=NULL,$typeOfRelief='')
	{
		$this->_data['typeOfRelieftxt']		=	$typeOfRelief;
		$this->_data['permissions']			=	$permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		$this->_data['permissions_d']		=	$permissions[$this->external_moduleid]['d'];
		$this->_data['reliefType']			=	$this->urgent_aid->get_all_listmanagement('donation_type');
		$this->_data['issuecountry']		=	$this->urgent_aid->get_all_listmanagement('issuecountry');
		$this->_data['typeOfMaterial']		=	$this->urgent_aid->get_all_listmanagement('type_of_material');


		$this->_data["extreliefDate"]=0;
		if($extreliefId == NULL){$this->_data["extreliefDate"]= date('Ymds'); }
		$this->_data["extreliefId"]= $extreliefId;
		$this->db->select('extreliefId');
		$this->db->from('ah_external_relief');						
		$this->db->order_by('extreliefId','DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$count =0;
		foreach($query->result() as $lc){
			$count =$lc->urgentaidId;
		}
		$this->_data["count"]=++$count;
		if($this->input->post())
			{
				
				//echo '<pre>'; print_r($this->input->post());exit();	
				
				$data = $this->input->post();
				$services["userid"] 		= $this->_login_userid;
				$services["currentdate"] 	= $data["currentdate"];
				$services["relief"] 		= $data["relief"];
				$services["autoid"] 		= $data["autoid"];
				$services["typeOfRelief"] 	= $data["typeOfRelief"];
				
				$services["country"] 		= $data["country"];
				$services["province"] 		= $data["province"];
				$services["city"] 			= $data["city"];
				$services["name"] 			= $data["name"];
				$services["age"] 			= $data["age"];
				$services["phone"] 			= $data["phone"];
				$services["noOfIndividual"] = $data["noOfIndividual"];
				$services["socialStatus"] 	= $data["socialStatus"];
				$services["address"] 		= $data["address"];
				$services["workplace"]		= $data["workplace"];
				$services["statement"] 		= $data["statement"];
				$services["summaryStatus"] 	= $data["summaryStatus"];
				$services["buildingStatus"] = $data["buildingStatus"];
				$services["suggestion"] 	= $data["suggestion"];
				$services["measures"] 		= $data["measures"];
				
				$services["reasons"] 		= $data["reasons"];
				$services["batch"] 			= $data["batch"];
				$services["budgeting"] 		= $data["budgeting"];
				$services["transfer"] 		= $data["transfer"];
				$services["directions"] 	= $data["directions"];
				$services["plan"] 			= $data["plan"];
				$services["finalReport"] 	= $data["finalReport"];

				$services["statusOfAffectedFamilies"] = $data["statusOfAffectedFamilies"];

				
				$extreliefId = $data["extreliefId"];
				
				
				/*if($extreliefId <= 0){
					$extreliefId = $services["extreliefId"] = '';
				}
				else{
					$extreliefId = $services["extreliefId"] = $data["extreliefId"];
				}*/
				if( $extreliefId > 0){
				$services["extreliefId"]	= $data["extreliefId"];
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
					$names = array();
					$files = $_FILES["attachment"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
						}
						
					}
					if($data["attachment_old"] !=""){
						if(count($names)>0)	$services['attachment']	=	$data["attachment_old"].",".@implode(',',$names);
						else{$services['attachment']	=	$data["attachment_old"];}
					}
					else{
						$services['attachment']	=	@implode(',',$names);	
					}
					
				}
				else
				{
					$services['attachment']	=	$data["attachment_old"];
				}
				
				$extreliefId = $this->urgent_aid->savestore($services,'ah_external_relief','extreliefId');
				//echo $this->db->last_query(); exit();
				$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
				redirect(base_url().'aid/addexternal_relief/'.$extreliefId);
				exit();
			}else{
				$services['userid']	=	$this->_login_userid;
				$services["extreliefId"]	= '';
				//print_r($_FILES);exit();
				if(count($_FILES["attachment"]["name"]) > 0 and ($_FILES["attachment"]["name"][0] !=""))
				{
					$names = array();
					$files = $_FILES["attachment"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
						}
						
					}
					$services['attachment']	=	@implode(',',$names);
				}//if(isset($_FILES["attachments"]) && count($_FILES["attachments"]) > 0)
				
				
				$extreliefId = $this->urgent_aid->savestore($services,'ah_external_relief','extreliefId');
				$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
				redirect(base_url().'aid/addexternal_relief/'.$extreliefId);
				exit();
			}
			exit();
			}
		if($extreliefId > 0){
			
			
			$this->db->select('*');
			$this->db->from('ah_external_reliefteam');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('extreliefTMId','ASC');
			$query = $this->db->get();
			$this->_data["external_reliefteam"] = $query->result();
			//////////////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_external_relief_committee_meeting');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('extrelMeetingId','ASC');
			$query = $this->db->get();
			$this->_data["external_relief_committee_meeting"] = $query->result();
			//////////////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_final_relief_committee_meeting');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('extrelMeetingId','ASC');
			$query = $this->db->get();
			$this->_data["ah_final_relief_committee_meeting"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_external_requirements_required');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('requirementsId','ASC');
			$query1 = $this->db->get();
			$this->_data["external_requirements_required"] = $query1->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_external_expenses');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('extExpencesId','ASC');
			$query = $this->db->get();
			$this->_data["external_expenses"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_external_final_report');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('extDataReportId','ASC');
			$query = $this->db->get();
			$this->_data["ah_external_final_report"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_external_daily_report');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('extDataReportId','ASC');
			$query = $this->db->get();
			$this->_data["external_daily_report"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->_data["rows"] = $this->urgent_aid->getdata($extreliefId,'ah_external_relief',"extreliefId");
			
			$this->_data['province']	=	$this->urgent_aid->get_all_listmanagement('issuecountry',$this->_data["rows"]->country);
			$this->_data['city']		=	$this->urgent_aid->get_all_listmanagement('issuecountry',$this->_data["rows"]->province);
			
		}
		
		$this->load->view('addexternal_relief',$this->_data);	
	}
	
	function addexternal_relief_load(){
		
		/*$data = array();
		$data["relief"] = $_POST["relief"];
		$data["currentdate"] = $_POST["currentdate"];
		$data["userid"] = $this->_login_userid;
		$extreliefId = $this->urgent_aid->savestore($data,'ah_external_relief','extreliefId');
		$ex['data'] = $extreliefId;
		echo json_encode($ex);*/
	}
	function add_external_reliefteam($extreliefTMId){
		
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		
		//print_r($this->input->post());exit();
		
		$data = array();
		$data["name"] 			=	$_POST["new_name"];
		$data["organization"] 	=	$_POST["organization"];
		$data["extreliefId"] 	=	$_POST["extreliefId"];
		$data["job"]			=	$_POST["job"];
		$data["userid"] 		=	$this->_login_userid;
		$extreliefTMIdauto 		=	$_POST["extreliefTMIdauto"];
		$data["autoid"] 		=	$_POST["get_autoid"];
		
		$extreliefTMId = $_POST["extreliefTMId"];
		if($extreliefTMId>0){
			$data["extreliefTMId"] = $extreliefTMId;
		}
		$extreliefTMId = $this->urgent_aid->savestore($data,'ah_external_reliefteam','extreliefTMId');
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_reliefteam/'.$extreliefTMId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/external_step_2/'.$extreliefTMId.'" id="'.$extreliefTMId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_external_reliefteam/'.$data["extreliefId"].'/'.$extreliefTMId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		$ex['data'] ='';
		if($_POST["extreliefTMId"] <=0){
			$ex['data'] .= ' <tr id="'.$extreliefTMId.'_team">';
		}
		$ex['data'] .= '<td id="'.$extreliefTMId.'_team_id">'.$extreliefTMIdauto.'</td>
		<td id="'.$extreliefTMId.'_team_name">'.$data["name"].'</td>
		<td id="'.$extreliefTMId.'_team_job">'.$data["job"].'</td>
		<td id="'.$extreliefTMId.'_team_organization">'.$data["organization"].'</td><td>'.$action.'</td>';
        if($_POST["extreliefTMId"] <=0){   $ex['data'] .= '</tr> ';}
		echo json_encode($ex);
		exit();
	}
	function view_row_external_reliefteam(){
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		$extreliefTMId = $_POST["extreliefTMId"];
		$id = $_POST["id"];
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));

		$rows = $this->urgent_aid->getdata($extreliefTMId,'ah_external_reliefteam',"extreliefTMId");
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_reliefteam/'.$extreliefTMId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
			$action	.= ' <a onclick="edit_external_reliefteam(\''.$extreliefTMId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$rows->extreliefId.'" data-url="'.base_url().'aid/delete_external_reliefteam/'.$rows->extreliefId.'/'.$extreliefTMId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		$ex['data'] ='';
		$ex['data'] .= '<td>'.$id.'</td><td>'.$rows->name.'</td><td>'.$rows->organization.'</td><td>'.$action.'</td>';
		echo json_encode($ex);
		exit();
	}
	function view_external_reliefteam($extreliefTMId){
		$rows = $this->urgent_aid->getdata($extreliefTMId,'ah_external_reliefteam',"extreliefTMId");
		 $this->_data["heading"] ="أعضاء فريق الاغاثة";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="external_daily_report_addId" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                    <th>الاسم</th>
									<th>الوظيفة</th>
                                    <th>الجهة</th>
                                </tr>
                            </thead>
                            <tbody> <tr>
                                    <td>'.$rows->name.'</td>
									<td>'.$rows->job.'</td>
                                    <td>'.$rows->organization.'</td>
                                </tr> ';
		$this->load->view('view_external_all',$this->_data);
	}
	
	
	function add_external_requirements_required($requirementsId){
		
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		
		$data = array();
		$data["typeOfMaterial"] = $_POST["typeOfMaterial"];
		$data["materials"] = $_POST["materials"];
		$data["budgeting"] = $_POST["budgeting"];
		$data["quantity"] = $_POST["quantity"];
		$data["extreliefId"] = $_POST["extreliefId"];
		$data["userid"] = $this->_login_userid;
		$data["requirementsIdauto"] = $_POST["requirementsIdauto"];
		$requirementsIdauto = $_POST["requirementsIdauto"];
		$requirementsId = $_POST["requirementsId"];
		
		if($requirementsId>0){
			$data["requirementsId"] = $requirementsId;
		}
		$requirementsId = $this->urgent_aid->savestore($data,'ah_external_requirements_required','requirementsId');
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_requirements_required/'.$requirementsId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		
			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/external_step_3/'.$requirementsId.'" id="'.$requirementsId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_requirements_required/'.$data["extreliefId"].'/'.$requirementsId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		$ex['data'] = '';
		if($_POST["requirementsId"] <=0){
		$ex['data'] .= ' <tr id="'.$requirementsId.'_required">';
		}
		$ex['data'] .= '<td id="'.$requirementsId.'_required_id">'.$requirementsIdauto.'</td>
		<td id="'.$requirementsId.'_required_typeOfMaterial">'.$this->haya_model->get_name_from_list($data["typeOfMaterial"]).'</td>
		<td id="'.$requirementsId.'_required_materials">'.$data["materials"].'</td>
		<td id="'.$requirementsId.'_required_quantity">'.$data["quantity"].'</td>
		<td id="'.$requirementsId.'_required_budgeting">'.number_format($data["budgeting"],3).'</td>
		<td>'.$action.'</td>';
			if($_POST["requirementsId"] <=0){$ex['data'] .= ' </tr>';}
		echo json_encode($ex);
		exit();
	}
	function view_requirements_required($requirementsId){
		$rows = $this->urgent_aid->getdata($requirementsId,'ah_external_requirements_required',"requirementsId");
		 $this->_data["heading"] ="الاحتياجات المطلوبة";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="external_daily_report_addId" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                    <th>نوع المواد</th>
                                    <th>المواد</th>
									<th>الكمية</th>
                                    <th>الموازنة</th>
                                </tr>
                            </thead>
                            <tbody> <tr>
                                    <td>'.$this->haya_model->get_name_from_list($rows->typeOfMaterial).'</td>
                                    <td>'.$rows->materials.'</td>
									<td>'.$rows->quantity.'</td>
									<td>'.number_format($rows->budgeting,3).'</td>
                                </tr> ';
		$this->load->view('view_external_all',$this->_data);
	}
	
	
	function view_row_requirements_required(){
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		$requirementsId = $_POST["requirementsId"];
		$id = $_POST["id"];
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));

		$rows = $this->urgent_aid->getdata($requirementsId,'ah_external_requirements_required',"requirementsId");
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_requirements_required/'.$requirementsId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
			$action	.= ' <a onclick="edit_requirements_required(\''.$requirementsId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$rows->extreliefId.'" data-url="'.base_url().'aid/delete_requirements_required/'.$rows->extreliefId.'/'.$requirementsId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		$ex['data'] = '';
		$ex['data'] .= '<td id="'.$requirementsId.'_required_id">'.$id.'</td>
		<td id="'.$requirementsId.'_required_typeOfMaterial">'.$this->haya_model->get_name_from_list($rows->typeOfMaterial).'</td>
		<td id="'.$requirementsId.'_required_materials">'.$rows->materials.'</td>
		<td id="'.$requirementsId.'_required_budgeting">'.$rows->budgeting.'</td>
		<td id="'.$requirementsId.'_required_quantity">'.$rows->quantity.'</td>
		<td>'.$action.'</td>';
		echo json_encode($ex);
		exit();
	}
	
	function add_external_expenses()
	{

		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		$data = array();
		$data["prose"] = $_POST["prose"];
		$data["food"] = $_POST["food"];
		$data["reliefArticles"] = $_POST["reliefArticles"];
		$data["medicalMaterials"] = $_POST["medicalMaterials"];
		$data["autoid_t"] = $_POST["autoid_t"];
		$data["other"] = $_POST["other"];
		$data["total"] = $_POST["total"];
		
							$total_amount	=	($_POST["prose"]+$_POST["food"]+$_POST["reliefArticles"]+$_POST["medicalMaterials"]+$_POST["other"]);
		
		$data["userid"] = $this->_login_userid;
		$extExpencesIdauto = $_POST["extExpencesIdauto"];
		$extExpencesId = $_POST["extExpencesId"];
		$data["notes"]  = $_POST["notes"];
		if(count($_FILES["attachment1"]["name"]) > 0 and ($_FILES["attachment1"]["name"][0] !=""))
		{
			$names = array();
			$files = $_FILES["attachment1"];
			 foreach ($files['name'] as $key => $image) {
				if(!empty($files['name'][$key]))
				{
					$_FILES['images']['name']= $files['name'][$key];
					$_FILES['images']['type']= $files['type'][$key];
					$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
					$_FILES['images']['error']= $files['error'][$key];
					$_FILES['images']['size']= $files['size'][$key];
					$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
				}
				
			}
			if($_POST["attachment1_old"] !=""){
				if(count($names)>0)	$data['attachment']	=	$_POST["attachment1_old"].",".@implode(',',$names);
				else{$data['attachment']	=	$_POST["attachment1_old"];}
			}
			else{
				$data['attachment']	=	@implode(',',$names);	
			}
			
		}
		else{
			$data['attachment']	=	$_POST["attachment1_old"];
		}
		
		
		
		
		//$attachment = $_POST["attachment1_old"];
		if($extExpencesId>0){
			$data["extExpencesId"] = $extExpencesId;
		}
		else{
			$data["extreliefId"] = $_POST["extreliefIds"];
		}
		$extExpencesId = $this->urgent_aid->savestore($data,'ah_external_expenses','extExpencesId');
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_expenses/'.$extExpencesId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		
					$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/external_step_4/'.$extExpencesId.'" id="'.$extExpencesId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
					
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_external_expenses/'.$data["extreliefId"].'/'.$extExpencesId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		$ex['data'] ='';
		if($_POST["extExpencesId"]<=0)$ex['data'] .= ' <tr id="'.$extExpencesId.'_expenses">';
		$ex['data'] .= '<td  id="'.$extExpencesId.'_expenses_id">'.$data["autoid_t"].'</td>
		<td id="'.$extExpencesId.'_expenses_prose">'.number_format($data["prose"],3).'</td>
		<td  id="'.$extExpencesId.'_expenses_food">'.number_format($data["food"],3).'</td>
		<td id="'.$extExpencesId.'_expenses_reliefArticles">'.number_format($data["reliefArticles"],3).'</td>
		<td id="'.$extExpencesId.'_expenses_medicalMaterials">'.number_format($data["medicalMaterials"],3).'</td>
		<td id="'.$extExpencesId.'_expenses_other">'.number_format($data["other"],3).'</td>
		<td id="'.$extExpencesId.'_expenses_total">'.number_format($total_amount,3).'</td>
		<td id="'.$extExpencesId.'_expenses_notes">'.$data["notes"].'</td>
		<td id="'.$extExpencesId.'_expenses_attachment">';if($data["attachment"] !=""){
									   $i=0;
									   $attachment = @explode(',',$data["attachment"]);
									   foreach($attachment as $att){
										   if($att !=""){
											   $i++;
									   
									   $ex['data'] .= '<div id="picture_'.$i.'" style="float:right;">';
								  $ex['data'] .= $this->getfileicon($att,base_url().'resources/aid/'.$this->_login_userid);
								 $ex['data'] .='</div>';
								  } } } $ex['data'] .='</td>
		<td>'.$action.'</td>';
        if($_POST["extExpencesId"]<=0)$ex['data'] .= '</tr>';
		echo json_encode($ex);
		exit();
	}
	function view_row_external_expenses(){
		$extExpencesId = $_POST["extExpencesId"];
		$id = $_POST["id"];
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));

		$rows = $this->urgent_aid->getdata($extExpencesId,'ah_external_expenses',"extExpencesId");
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_expenses/'.$extExpencesId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		$action	.= ' <a onclick="edit_external_expenses(\''.$extExpencesId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$rows->extreliefId.'" data-url="'.base_url().'aid/delete_external_expenses/'.$rows->extreliefId.'/'.$extExpencesId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		$ex['data'] ='';
		$ex['data'] .= '<td  id="'.$extExpencesId.'_expenses_id">'.$id.'</td>
		<td id="'.$extExpencesId.'_expenses_prose">'.$rows->prose.'</td>
		<td  id="'.$extExpencesId.'_expenses_food">'.$rows->food.'</td>
		<td id="'.$extExpencesId.'_expenses_reliefArticles">'.$rows->reliefArticles.'</td>
		<td id="'.$extExpencesId.'_expenses_medicalMaterials">'.$rows->medicalMaterials.'</td>
		<td id="'.$extExpencesId.'_expenses_other">'.$rows->other.'</td>
		<td id="'.$extExpencesId.'_expenses_total">'.$rows->total.'</td>
		<td id="'.$extExpencesId.'_expenses_notes">'.$rows->notes.'</td>
		<td id="'.$extExpencesId.'_expenses_attachment">';if($rows->attachment !=""){
									   $i=0;
									   $attachment = @explode(',',$rows->attachment);
									   foreach($attachment as $att){
										   if($att !=""){
											   $i++;
									   
									   $ex['data'] .= '<div id="picture_'.$i.'" style="float:right;">';
								  $ex['data'] .= $this->getfileicon($att,base_url().'resources/aid/'.$rows->userid);
								 $ex['data'] .='</div>';
								  } } } $ex['data'] .='</td>
		
		<td>'.$action.'</td>';
		echo json_encode($ex);
		exit();
	}
	function view_external_expenses($extExpencesId)
	{
		$rows 			=	$this->urgent_aid->getdata($extExpencesId,'ah_external_expenses',"extExpencesId");
		$total_amount	=	($rows->prose+$rows->food+$rows->reliefArticles+$rows->medicalMaterials+$rows->other);

		 $this->_data["heading"] ="المصروفات";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="external_daily_report_addId" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
									<th>النثرية</th>
									<th>المواد الغذائية</th>
									<th>المواد الاغاثية</th>
									<th>المواد الطبية</th>
									<th>اخرى</th>
									<th>الاجمالي</th>
									<th>الملاحظات</th>
                                </tr>
                            </thead>
                            <tbody> <tr>
                                    <td>'.number_format($rows->prose,3).'</td>
                                    <td>'.number_format($rows->food,3).'</td>
									<td >'.number_format($rows->reliefArticles,3).'</td>
                                    <td>'.number_format($rows->medicalMaterials,3).'</td>
                                    <td>'.number_format($rows->other,3).'</td>
									<td>'.number_format($total_amount,3).'</td>
									<td>'.$rows->notes.'</td>
                                </tr> ';
		$this->load->view('view_external_all',$this->_data);
	}
	
	
	function external_relief_committee_meeting_addId(){
		
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		
		$data 					=	array();
		$data["minutes"] 		=	$_POST["minutes"];
		$data["currentdate"] 	=	$_POST["currentdates"];
		$data["decision"] 		=	$_POST["decision"];
		$data["notes"] 			=	$_POST["notes"];
		$data["extreliefId"] 	=	$_POST["extreliefId"];
		$data["userid"] 		=	$this->_login_userid;
		
	
		$extrelMeetingId 		=	$_POST["extrelMeetingId"];
		
		if($extrelMeetingId	>	0)
		{
			$data["extrelMeetingId"] = $extrelMeetingId;
		}
		
		$extrelMeetingId = $this->urgent_aid->savestore($data,'ah_external_relief_committee_meeting','extrelMeetingId');
		
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_relief_committee_meeting/'.$extrelMeetingId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		$action	.= ' <a onclick="edit_external_relief_committee_meeting(\''.$extrelMeetingId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_external_relief_committee_meeting/'.$data["extreliefId"].'/'.$extrelMeetingId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}

		$ex['data'] = '';
		if($_POST["extrelMeetingId"] <=0)$ex['data'] .= '<tr id="'.$extrelMeetingId.'_meeting">';
		$ex['data'] .= '<td id="'.$extrelMeetingId.'_meeting_minutes">'.$data["minutes"].'</td>
		<td id="'.$extrelMeetingId.'_meeting_currentdate">'.$data["currentdate"].'</td>
		<td id="'.$extrelMeetingId.'_meeting_decision">'.$data["decision"].'</td>
		<td id="'.$extrelMeetingId.'_meeting_notes">'.$data["notes"].'</td>
		<td>'.$action.'</td>';
		
        if($_POST["extrelMeetingId"] <=0) $ex['data'] .= '</tr> ';
		
		echo json_encode($ex);
		exit();
	}
//--------------------------------------------------------------------------------
	function external_relief_committee_meeting_addId_muzi(){
		
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		
		$data 					=	array();
		$data["minutes"] 		=	$_POST["minutes"];
		$data["currentdate"] 	=	$_POST["currentdates"];
		$data["decision"] 		=	$_POST["decision"];
		$data["notes"] 			=	$_POST["notes"];
		$data["extreliefId"] 	=	$_POST["new_extreliefId"];
		$data["userid"] 		=	$this->_login_userid;
		
		if($_FILES["aid_attachemnt"]["tmp_name"])
		{
			$aid_attachemnt	=	$this->upload_file($this->_login_userid,'aid_attachemnt','resources/aid');
			
			$data['aid_attachemnt']	=	$aid_attachemnt;
		}
		else
		{
			// Do Something
		}
	
		$extrelMeetingId 		=	$_POST["extrelMeetingId"];
		
		if($extrelMeetingId	>	0)
		{
			$data["extrelMeetingId"] = $extrelMeetingId;
		}
		
		$extrelMeetingId = $this->urgent_aid->savestore($data,'ah_external_relief_committee_meeting','extrelMeetingId');
		
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_relief_committee_meeting/'.$extrelMeetingId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		
			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/external_step_1/'.$extrelMeetingId.'" id="'.$extrelMeetingId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_external_relief_committee_meeting/'.$data["extreliefId"].'/'.$extrelMeetingId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		if($data['aid_attachemnt'])
		{
					$action	.=	' <i href="'.base_url().'resources/aid/'.$data['userid'].'/'.$data['aid_attachemnt'].'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "></i> ';
		}

		$ex['data'] = '';
		if($_POST["extrelMeetingId"] <=0)$ex['data'] .= '<tr id="'.$extrelMeetingId.'_meeting">';
		$ex['data'] .= '<td id="'.$extrelMeetingId.'_meeting_minutes">'.$data["minutes"].'</td>
		<td id="'.$extrelMeetingId.'_meeting_currentdate">'.$data["currentdate"].'</td>
		<td id="'.$extrelMeetingId.'_meeting_decision">'.$data["decision"].'</td>
		<td id="'.$extrelMeetingId.'_meeting_notes">'.$data["notes"].'</td>
		<td>'.$action.'</td>';
		
        if($_POST["extrelMeetingId"] <=0) $ex['data'] .= '</tr> ';
		
		echo ($ex['data']);
	}
//--------------------------------------------------------------------------------
	function add_final_relief_committee_meeting_muzi(){
		
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		
		$data 					=	array();
		$data["minutes"] 		=	$_POST["minutes"];
		$data["currentdate"] 	=	$_POST["currentdates"];
		$data["decision"] 		=	$_POST["decision"];
		$data["notes"] 			=	$_POST["notes"];
		$data["extreliefId"] 	=	$_POST["new_extreliefId"];
		$data["userid"] 		=	$this->_login_userid;
		
		if($_FILES["final_releif_attachemnt"]["tmp_name"])
		{
			$aid_attachemnt	=	$this->upload_file($this->_login_userid,'final_releif_attachemnt','resources/aid');
			
			$data['final_releif_attachemnt']	=	$aid_attachemnt;
		}
		else
		{
			// Do Something
		}
	
		$extrelMeetingId 		=	$_POST["extrelMeetingId"];
		
		if($extrelMeetingId	>	0)
		{
			$data["extrelMeetingId"] = $extrelMeetingId;
		}
		
		$extrelMeetingId = $this->urgent_aid->savestore($data,'ah_final_relief_committee_meeting','extrelMeetingId');
		
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_final_relief_committee_meeting_muzi/'.$extrelMeetingId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		
			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/external_step_7/'.$extrelMeetingId.'" id="'.$extrelMeetingId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
		
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_external_relief_committee_meeting/'.$data["extreliefId"].'/'.$extrelMeetingId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		if($data['final_releif_attachemnt'])
		{
					$action	.=	' <i href="'.base_url().'resources/aid/'.$data['userid'].'/'.$data['final_releif_attachemnt'].'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "></i> ';
		}

		$ex['data'] = '';
		if($_POST["extrelMeetingId"] <=0)$ex['data'] .= '<tr id="'.$extrelMeetingId.'_meeting">';
		$ex['data'] .= '<td id="'.$extrelMeetingId.'_meeting_minutes">'.$data["minutes"].'</td>
		<td id="'.$extrelMeetingId.'_meeting_currentdate">'.$data["currentdate"].'</td>
		<td id="'.$extrelMeetingId.'_meeting_decision">'.$data["decision"].'</td>
		<td id="'.$extrelMeetingId.'_meeting_notes">'.$data["notes"].'</td>
		<td>'.$action.'</td>';
		
        if($_POST["extrelMeetingId"] <=0) $ex['data'] .= '</tr> ';
		
		echo ($ex['data']);
	}
//--------------------------------------------------------------------------------
	
	function view_row_external_relief_committee_meeting(){
		$extrelMeetingId = $_POST["extrelMeetingId"];
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));

		$rows = $this->urgent_aid->getdata($extrelMeetingId,'ah_external_relief_committee_meeting',"extrelMeetingId");
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_relief_committee_meeting/'.$extrelMeetingId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		//$action	.= ' <a onclick="edit_external_relief_committee_meeting(\''.$extrelMeetingId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$rows->extreliefId.'" data-url="'.base_url().'aid/delete_external_relief_committee_meeting/'.$rows->extreliefId.'/'.$extrelMeetingId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		$ex['data'] = '';
		$ex['data'] .= '<td id="'.$extrelMeetingId.'_meeting_minutes">'.$rows->minutes.'</td>
		<td id="'.$extrelMeetingId.'_meeting_currentdate">'.$rows->currentdate.'</td>
		<td id="'.$extrelMeetingId.'_meeting_decision">'.$rows->decision.'</td>
		<td id="'.$extrelMeetingId.'_meeting_notes">'.$rows->notes.'</td>
		<td>'.$action.'</td>';
		echo json_encode($ex);
		exit();
	}
	function view_external_relief_committee_meeting($extrelMeetingId){
		$rows = $this->urgent_aid->getdata($extrelMeetingId,'ah_external_relief_committee_meeting',"extrelMeetingId");
		 $this->_data["heading"] ="اجتماع لجنة الإغاثة";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="external_daily_report_addId" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                   <th>رقم محضر الاجتماع</th>
									<th>التاريخ</th>
									<th>قرار اللجنة</th>
									<th>الملاحظات</th>
                                </tr>
                            </thead>
                            <tbody> <tr>
                                    <td >'.$rows->minutes.'</td>
                                    <td>'.$rows->currentdate.'</td>
                                    <td>'.$rows->decision.'</td>
									<td>'.$rows->notes.'</td>
                                </tr> ';
		$this->load->view('view_external_all',$this->_data);
	}
	function view_internal_relief_committee_meeting($extrelMeetingId){
		$rows = $this->urgent_aid->getdata($extrelMeetingId,'ah_internal_relief_committee_meeting',"internalMeetingId");
		 $this->_data["heading"] ="اجتماع لجنة الإغاثة";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="external_daily_report_addId" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                   <th>رقم محضر الاجتماع</th>
									<th>التاريخ</th>
									<th>قرار اللجنة</th>
									<th>الملاحظات</th>
                                </tr>
                            </thead>
                            <tbody> <tr>
                                    <td >'.$rows->minutes.'</td>
                                    <td>'.$rows->currentdate.'</td>
                                    <td>'.$rows->decision.'</td>
									<td>'.$rows->notes.'</td>
                                </tr> ';
		$this->load->view('view_external_all',$this->_data);
	}
	
	
	function add_external_daily_report(){
		
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		$data = array();
		$data["statement"] = $_POST["statement"];
		$data["currentdate"] = $_POST["currentdate"];
		$data["extreliefId"] = $_POST["extreliefId"];
		$data["userid"] = $this->_login_userid;
		$extDataReportIddauto = $_POST["extDataReportIddauto"];
		if($_POST["extDataReportId"]>0){
			$data["extDataReportId"] = $extDataReportId;
		}
		$extDataReportId = $this->urgent_aid->savestore($data,'ah_external_daily_report','extDataReportId');
		$action ='';
			$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_daily_report/'.$extDataReportId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		//if($rows->userid == $this->_login_userid){
			//$action	.= ' <a onclick="edit_external_daily_report(\''.$extDataReportId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
		//}
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_external_daily_report/'.$data["extreliefId"].'/'.$extDataReportId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		$ex['data'] ='';
		if($_POST["extDataReportId"]<=0){
			$ex['data'] .= '<tr id="'.$extDataReportId.'_daily">';
		}
		$ex['data'] .= '<td id="'.$extDataReportId.'_daily_id">'.$extDataReportIddauto.'</td>
		<td id="'.$extDataReportId.'_daily_currentdate">'.$data["currentdate"].'</td>
		<td id="'.$extDataReportId.'_daily_statement">'.$data["statement"].'</td><td>'.$action.'</td>';
		if($_POST["extDataReportId"]<=0){
           $ex['data'] .= '</tr>';
		}
		echo json_encode($ex);
		exit();
	}
//--------------------------------------------------------------------

function add_external_daily_report_muzi(){
		
		$this->_data['permissions']	=	$permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		
		$data 					=	array();
		$data["statement"] 		=	$_POST["statement"];
		$data["currentdate"] 	=	$_POST["currentdaterep"];
		$data["extreliefId"] 	=	$_POST["external_extreliefId"];
		$data["userid"] 		=	$this->_login_userid;
		$data["autoid_f"] 		=	$_POST["autoid_f"];
		
		$extDataReportIddauto 	=	$_POST["extDataReportIddauto"];
		
		if($_FILES["report_attachment"]["tmp_name"])
		{
			$aid_attachemnt	=	$this->upload_file($this->_login_userid,'report_attachment','resources/aid');
			
			$data['report_attachment']	=	$aid_attachemnt;
		}
		else
		{
			// Do Something
		}
		
		if($_POST["extDataReportId"]	>	0)
		{
			$data["extDataReportId"] = $extDataReportId;
		}
		
		$extDataReportId	=	$this->urgent_aid->savestore($data,'ah_external_daily_report','extDataReportId');
		$action 			=	'';
		
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_daily_report/'.$extDataReportId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';

			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/external_step_5/'.$extDataReportId.'" id="'.$extDataReportId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';

		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_external_daily_report/'.$data["extreliefId"].'/'.$extDataReportId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		if($data['report_attachment'])
		{
					$action	.=	' <i href="'.base_url().'resources/aid/'.$data['userid'].'/'.$data['report_attachment'].'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "></i> ';
		}

		$ex['data'] ='';
		if($_POST["extDataReportId"]<=0){
			$ex['data'] .= '<tr id="'.$extDataReportId.'_daily">';
		}
		$ex['data'] .= '<td id="'.$extDataReportId.'_daily_id">'.$extDataReportIddauto.'</td>
		<td id="'.$extDataReportId.'_daily_currentdate">'.$data["currentdate"].'</td>
		<td id="'.$extDataReportId.'_daily_statement">'.$data["statement"].'</td><td>'.$action.'</td>';
		if($_POST["extDataReportId"]<=0){
           $ex['data'] .= '</tr>';
		}
		
		echo ($ex['data']);
		exit();
	}
//--------------------------------------------------------------------

function add_final_report_muzi(){
		
		$this->_data['permissions']	=	$permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		
		$data 					=	array();
		$data["statement"] 		=	$_POST["final_statement"];
		$data["currentdate"] 	=	$_POST["final_currentdaterep"];
		$data["extreliefId"] 	=	$_POST["final_extreliefId"];
		$data["final_percent"] 	=	$_POST["final_percent"];
		$data["userid"] 		=	$this->_login_userid;
		$extDataReportIddauto 	=	$_POST["final_extDataReportIddauto"];
		$data["final_extDataReportIddauto"]  	=	$_POST["final_extDataReportIddauto"];
		
		if($_FILES["final_attachment"]["tmp_name"])
		{
			$final_attachment	=	$this->upload_file($this->_login_userid,'final_attachment','resources/aid');
			
			$data['report_attachment']	=	$final_attachment;
		}
		else
		{
			// Do Something
		}
		
		if($_POST["extDataReportId"]	>	0)
		{
			$data["extDataReportId"] = $extDataReportId;
		}
		
		$extDataReportId	=	$this->urgent_aid->savestore($data,'ah_external_final_report','extDataReportId');
		$action 			=	'';
		
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_final_report/'.$extDataReportId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';

			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/external_step_6/'.$extDataReportId.'" id="'.$extDataReportId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';

		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_external_daily_report/'.$data["extreliefId"].'/'.$extDataReportId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		if($data['report_attachment'])
		{
					$action	.=	' <i href="'.base_url().'resources/aid/'.$data['userid'].'/'.$data['report_attachment'].'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "></i> ';
		}

		$ex['data'] ='';
		if($_POST["extDataReportId"]<=0){
			$ex['data'] .= '<tr id="'.$extDataReportId.'_daily">';
		}
		$ex['data'] .= '<td id="'.$extDataReportId.'_daily_id">'.$data["final_extDataReportIddauto"].'</td>
		<td id="'.$extDataReportId.'_daily_currentdate">'.$data["currentdate"].'</td>
		<td id="'.$extDataReportId.'_final_percent">'.$data["final_percent"].'</td>
		<td id="'.$extDataReportId.'_daily_statement">'.$data["statement"].'</td><td>'.$action.'</td>';
		if($_POST["extDataReportId"]<=0){
           $ex['data'] .= '</tr>';
		}
		
		echo ($ex['data']);
		exit();
	}
//--------------------------------------------------------------------	
	function view_row_external_daily_report(){
		$extDataReportId = $_POST["extDataReportId"];
		$id = $_POST["id"];
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));

		$rows = $this->urgent_aid->getdata($extDataReportId,'ah_external_daily_report',"extDataReportId");
		$action ='';
			$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_external_final_report/'.$rows->extDataReportId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		//if($rows->userid == $this->_login_userid){
			$action	.= ' <a onclick="edit_external_daily_report(\''.$rows->extDataReportId.'\');"  style="margin-left:5px; cursor:pointer;" ><i class="icon-pencil"></i></a>';
		//}
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$rows->extreliefId.'" data-url="'.base_url().'aid/delete_external_final_report/'.$rows->extreliefId.'/'.$rows->extDataReportId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		$ex['data'] ='';
		$ex['data'] .= '<td id="'.$extDataReportId.'_daily_id">'.$id.'</td>
		<td id="'.$extDataReportId.'_daily_currentdate">'.$rows->currentdate.'</td>
		<td id="'.$extDataReportId.'_daily_statement">'.$rows->statement.'</td><td>'.$action.'</td>';
		echo json_encode($ex);
		exit();
	}
	function view_external_daily_report($extDataReportId){
		$rows = $this->urgent_aid->getdata($extDataReportId,'ah_external_daily_report',"extDataReportId");
		 $this->_data["heading"] ="التقرير اليومي";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="external_daily_report_addId" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                    <th>التاريخ</th>
                                    <th>البيان</th>
                                </tr>
                            </thead>
                            <tbody> <tr>
                                    <td>'.$rows->currentdate.'</td>
                                    <td>'.$rows->statement.'</td>
                                </tr> ';
		$this->load->view('view_external_all',$this->_data);
	}
	function delete_external_daily_report($extreliefId,$extDataReportId){
		$this->urgent_aid->delete($extDataReportId,'extDataReportId',"ah_external_daily_report");
		$ex['data'] = $extDataReportId."_daily";
		$result  = $this->urgent_aid->getdmessagedata($extreliefId,'ah_external_daily_report',"extreliefId");
		$data = count($result);
		$ex['count'] =++$data;
		echo json_encode($ex);
		exit();
	}
	function delete_external_reliefteam($extreliefId,$extreliefTMId){
		$this->urgent_aid->delete($extreliefTMId,'extreliefTMId',"ah_external_reliefteam");
		$ex['data'] = $extreliefTMId."_team";
		$result  = $this->urgent_aid->getdmessagedata($extreliefId,'ah_external_reliefteam',"extreliefId");
		$data = count($result);
		$ex['count'] =++$data;
		echo json_encode($ex);
		exit();
	}
	function delete_requirements_required($extreliefId,$requirementsId){
		$this->urgent_aid->delete($requirementsId,'requirementsId',"ah_external_requirements_required");
		$ex['data'] = $requirementsId."_required";
		$result  = $this->urgent_aid->getdmessagedata($extreliefId,'ah_external_requirements_required',"extreliefId");
		$data = count($result);
		$ex['count'] =++$data;
		echo json_encode($ex);
		exit();
	}
	function delete_external_expenses($extreliefId,$extExpencesId){
		$this->urgent_aid->delete($extExpencesId,'extExpencesId',"ah_external_expenses");
		$ex['data'] = $extExpencesId."_expenses";
		$result  = $this->urgent_aid->getdmessagedata($extreliefId,'ah_external_expenses',"extreliefId");
		$data = count($result);
		$ex['count'] =++$data;
		echo json_encode($ex);
		exit();
	}
	function delete_external_relief_committee_meeting($extreliefId,$extrelMeetingId){
		$this->urgent_aid->delete($extrelMeetingId,'extrelMeetingId',"ah_external_relief_committee_meeting");
		$ex['data'] = $extrelMeetingId."_meeting";
		$result  = $this->urgent_aid->getdmessagedata($extreliefId,'ah_external_relief_committee_meeting',"extreliefId");
		$data = count($result);
		$ex['count'] =++$data;
		echo json_encode($ex);
		exit();
	}
	function delete_internal_relief_committee_meeting($extreliefId,$extrelMeetingId)
	{
		$this->urgent_aid->delete($extrelMeetingId,'internalMeetingId',"ah_internal_relief_committee_meeting");
		$ex['data'] = $extrelMeetingId."_meeting";
		$result  = $this->urgent_aid->getdmessagedata($extreliefId,'ah_internal_relief_committee_meeting',"urgentaidId");
		$data = count($result);
		$ex['count'] =++$data;
		echo json_encode($ex);
		exit();
	}
	function delete_internal_final_relief_committee_meeting($extreliefId,$extrelMeetingId){
		$this->urgent_aid->delete($extrelMeetingId,'extrelMeetingId',"ah_internal_final_relief_committee_meeting");
		$ex['data'] = $extrelMeetingId."_meeting";
		$result  = $this->urgent_aid->getdmessagedata($extreliefId,'ah_internal_final_relief_committee_meeting',"extreliefId");
		$data = count($result);
		$ex['count'] =++$data;
		echo json_encode($ex);
		exit();
	}
	function delete_external_relief($extreliefId){
		$this->urgent_aid->delete($extreliefId,'extreliefId',"ah_external_relief");
		$this->urgent_aid->delete($extreliefId,'extreliefId',"ah_external_daily_report");
		$this->urgent_aid->delete($extreliefId,'extreliefId',"ah_external_reliefteam");
		$this->urgent_aid->delete($extreliefId,'extreliefId',"ah_external_requirements_required");
		$this->urgent_aid->delete($extreliefId,'extreliefId',"ah_external_expenses");
		$this->urgent_aid->delete($extreliefId,'extreliefId',"ah_external_relief_committee_meeting");
		//redirect(base_url().'aid/external_relief');
		exit();
	}
	function view_external_relief($extreliefId){
		if($extreliefId > 0){
			
			$this->_data['extreliefId']	=	$extreliefId;
			
			$this->db->select('*');
			$this->db->from('ah_external_reliefteam');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('extreliefTMId','ASC');
			$query = $this->db->get();
			$this->_data["external_reliefteam"] = $query->result();
			//////////////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_external_relief_committee_meeting');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('extrelMeetingId','ASC');
			$query = $this->db->get();
			$this->_data["external_relief_committee_meeting"] = $query->result();
			//////////////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_final_relief_committee_meeting');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('extrelMeetingId','ASC');
			$query = $this->db->get();
			$this->_data["ah_final_relief_committee_meeting"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_external_requirements_required');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('requirementsId','ASC');
			$query1 = $this->db->get();
			$this->_data["external_requirements_required"] = $query1->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_external_expenses');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('extExpencesId','ASC');
			$query = $this->db->get();
			$this->_data["external_expenses"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_external_final_report');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('extDataReportId','ASC');
			$query = $this->db->get();
			$this->_data["ah_external_final_report"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->db->select('*');
			$this->db->from('ah_external_daily_report');						
			$this->db->where("delete_record",'0');
			$this->db->where("extreliefId",$extreliefId);
			$this->db->order_by('extDataReportId','ASC');
			$query = $this->db->get();
			$this->_data["external_daily_report"] = $query->result();
			/////////////////////////////////////////////////////////
			$this->_data["rows"] = $this->urgent_aid->getdata($extreliefId,'ah_external_relief',"extreliefId");
			$reliefType		=	$this->urgent_aid->get_all_listmanagement('donation_type');
			foreach($reliefType as $type){
				if($type->list_id == $rows->reliefType)
					$this->_data["reliefType"] = $type->list_name;
			}
		}
		
		$this->load->view('view_external_relief',$this->_data);
	}
	function removeimage($extreliefId,$imagename){
		
		
		$result = $this->urgent_aid->getdata($extreliefId,'ah_external_relief',"extreliefId");
		unlink('resources/aid/'.$result->userid."/".$imagename);
		if($result->attachment !=""){
			$image=@explode(',',$result->attachment);
			foreach($image as $key=>$vals){
				if($imagename == $vals){
					unset($image[$key]);
				}
			}
			$data["attachment"]=@implode(',',$image);
			$data["extreliefId"] = $extreliefId;
			$this->urgent_aid->savestore($data,'ah_external_relief','extreliefId');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
		}
		//redirect(base_url().'aid/addexternal_relief/'.$extreliefId);
		$ex['data'] = "picture_".$extreliefId;
		echo json_encode($ex);
		exit();
	}
	
	
	function getfileicon($filename,$path)
	{
		$filename = strtolower($filename);
		$file_extension = strrchr($filename, ".");	
	
		$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_nd.png" style="width:30px; height:30px" alt="image" title="image"/></a>';
	
		if($file_extension=='.doc' || $file_extension=='.docx')
	
			 $rettype =' <a class="fancybox-button" rel="gallery1" href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_doc.png" style="width:30px; height:30px" alt="doc" title="doc"/></a>';
	
		if($file_extension=='.xls' || $file_extension=='.xlsx' || $file_extension=='.csv' || $file_extension=='.xlt' || $file_extension=='.xltx')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_xls.png" style="width:30px; height:30px" alt="xls" title="xls"/></a>';
	
		if($file_extension=='.pdf')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_pdf.png" style="width:30px; height:30px" alt="pdf" title="pdf"/></a>';
	
		if($file_extension=='.jpg' || $file_extension=='.jpeg'  || 	$file_extension=='.gif')
	
			$rettype=' <a class="fancybox-button" rel="gallery1" href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_jpg.png" style="width:30px; height:30px" alt="jpg" title="jpg"/></a>';
	
		if($file_extension=='.png' )
	
			$rettype=' <a class="fancybox-button" rel="gallery1" href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_png.png" style="width:30px; height:30px" alt="png" title="png"/></a>';
	
		if($file_extension=='.ppt' || $file_extension=='.pptx')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_ppt.png" style="width:30px; height:30px" alt="ppt" title="ppt"/></a>';		
	
		if($file_extension=='.zip' || $file_extension=='.tz' || $file_extension=='.rar' || $file_extension=='.gzip')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_zip.png" style="width:30px; height:30px" alt="zip" title="zip"/></a>';		
	
		if($file_extension=='.txt' || $file_extension=='.rtf')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_txt.png" style="width:30px; height:30px" alt="txt" title="txt"/></a>';
	
		return $rettype;
	
			
	
	}
	
	function dateformat($dates){
		$dates1 = @explode(' ',$dates);
		return $dates1[0];
	}
	
//----------------------------------------------------------
	/*
	*
	*
	*/
	public function aid_plan()
	{
		$data					=	$this->input->post();
		$data['extreliefId']	=	$this->input->post('aid_extreliefId');
		$data['userid']			=	$this->_login_userid;
		
		if($_FILES["plan_attachment"]["tmp_name"])
		{
			$plan_attachment	=	$this->upload_file($this->_login_userid,'plan_attachment','resources/aid');
			
			$data['plan_attachment']	=	$plan_attachment;
			
			if($data['plan_attachment_old'])
			{
				// Delete From Folder
			}
		}
		else
		{
			$data['plan_attachment']	=	isset($data['plan_attachment_old']) ? $data['plan_attachment_old']: NULL;
		}
		
		// UNSET THIS VALUE FROM ARRAY
		unset($data['aid_extreliefId'],$data['plan_attachment_old'],$data['save_aid_plan']);
		
		// ADD/UPDATE plan Detail
		$this->urgent_aid->save_aid_pan_detail($data['planid'],$data);

		
		$this->load->library('user_agent');
		if ($this->agent->is_referral())
		{
			$url	=	 $this->agent->referrer();
		}
						
		$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
		redirect($url);
		exit();
		
	}
//--------------------------------------------------------------------------------
	function internal_committee_meeting_addId_muzi(){
		
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		
		$data 					=	array();
		$data["minutes"] 		=	$_POST["minutes"];
		$data["currentdate"] 	=	$_POST["currentdates"];
		$data["decision"] 		=	$_POST["decision"];
		$data["notes"] 			=	$_POST["notes"];
		$data["urgentaidId"] 	=	$_POST["urgentaidId"];
		$data["userid"] 		=	$this->_login_userid;
		
		if($_FILES["aid_attachemnt"]["tmp_name"])
		{
			$aid_attachemnt	=	$this->upload_file($this->_login_userid,'aid_attachemnt','resources/aid');
			
			$data['aid_attachemnt']	=	$aid_attachemnt;
		}
		else
		{
			// Do Something
		}
	
		$internalMeetingId 		=	$_POST["internalMeetingId"];
		
		if($internalMeetingId	>	0)
		{
			$data["internalMeetingId"] = $internalMeetingId;
		}
		
		$internalMeetingId = $this->urgent_aid->savestore($data,'ah_internal_relief_committee_meeting','internalMeetingId');
		
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_internal_relief_committee_meeting/'.$internalMeetingId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		
				$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_1/'.$internalMeetingId.'" id="'.$internalMeetingId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
		
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["urgentaidId"].'" data-url="'.base_url().'aid/delete_internal_relief_committee_meeting/'.$data["urgentaidId"].'/'.$internalMeetingId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		if($data['aid_attachemnt'])
		{
					$action	.=	' <i href="'.base_url().'resources/aid/'.$data['userid'].'/'.$data['aid_attachemnt'].'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "></i> ';
		}

		$ex['data'] = '';
		if($_POST["internalMeetingId"] <=0)$ex['data'] .= '<tr id="'.$internalMeetingId.'_meeting">';
		$ex['data'] .= '<td id="'.$internalMeetingId.'_meeting_minutes">'.$data["minutes"].'</td>
		<td id="'.$internalMeetingId.'_meeting_currentdate">'.$data["currentdate"].'</td>
		<td id="'.$internalMeetingId.'_meeting_decision">'.$data["decision"].'</td>
		<td id="'.$internalMeetingId.'_meeting_notes">'.$data["notes"].'</td>
		<td>'.$action.'</td>';
		
        if($_POST["internalMeetingId"] <=0) $ex['data'] .= '</tr> ';
		
		echo ($ex['data']);
	}
//----------------------------------------------------------
function add_internal_reliefteam($extreliefTMId){
		
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		
		//print_r($this->input->post());exit();
		
		$data = array();
		$data["name"] 			=	$_POST["new_name"];
		$data["organization"] 	=	$_POST["organization"];
		$data["extreliefId"] 	=	$_POST["extreliefId"];
		$data["job"]			=	$_POST["job"];
		$data["userid"] 		=	$this->_login_userid;
		$data["autoid_1"] 		=	$_POST["autoid_1"];
		$extreliefTMIdauto 		=	$_POST["extreliefTMIdauto"];
		
		$extreliefTMId = $_POST["extreliefTMId"];
		if($extreliefTMId>0){
			$data["extreliefTMId"] = $extreliefTMId;
		}
		$extreliefTMId = $this->urgent_aid->savestore($data,'ah_internal_reliefteam','extreliefTMId');
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_internal_reliefteam/'.$extreliefTMId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		
				$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_2/'.$extreliefTMId.'" id="'.$extreliefTMId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_internal_reliefteam/'.$data["extreliefId"].'/'.$extreliefTMId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		$ex['data'] ='';
		if($_POST["extreliefTMId"] <=0){
			$ex['data'] .= ' <tr id="'.$extreliefTMId.'_team">';
		}
		$ex['data'] .= '<td id="'.$extreliefTMId.'_team_id">'.$data["autoid_1"].'</td>
		<td id="'.$extreliefTMId.'_team_name">'.$data["name"].'</td>
		<td id="'.$extreliefTMId.'_team_job">'.$data["job"].'</td>
		<td id="'.$extreliefTMId.'_team_organization">'.$data["organization"].'</td><td>'.$action.'</td>';
        if($_POST["extreliefTMId"] <=0){   $ex['data'] .= '</tr> ';}
		echo json_encode($ex);
		exit();
	}
//----------------------------------------------------------
	/*
	*
	*
	*/
	public function internal_aid_plan()
	{
		$data					=	$this->input->post();
		$data['extreliefId']	=	$this->input->post('aid_extreliefId');
		$data['userid']			=	$this->_login_userid;
		
		if($_FILES["plan_attachment"]["tmp_name"])
		{
			$plan_attachment	=	$this->upload_file($this->_login_userid,'plan_attachment','resources/aid');
			
			$data['plan_attachment']	=	$plan_attachment;
			
			if($data['plan_attachment_old'])
			{
				// Delete From Folder
			}
		}
		else
		{
			$data['plan_attachment']	=	isset($data['plan_attachment_old']) ? $data['plan_attachment_old']: NULL;
		}
		
		// UNSET THIS VALUE FROM ARRAY
		unset($data['aid_extreliefId'],$data['plan_attachment_old'],$data['save_aid_plan']);
		
		// ADD/UPDATE plan Detail
		$this->urgent_aid->save_internal_aid_plan($data['planid'],$data);

		
		$this->load->library('user_agent');
		if ($this->agent->is_referral())
		{
			$url	=	 $this->agent->referrer();
		}
						
		$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
		redirect($url);
		exit();
		
	}
//----------------------------------------------------------

function add_internal_requirements_required($requirementsId){
		
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		$data = array();
		$data["typeOfMaterial"] = $_POST["typeOfMaterial"];
		$data["materials"] = $_POST["materials"];
		$data["budgeting"] = $_POST["budgeting"];
		$data["quantity"] = $_POST["quantity"];
		$data["extreliefId"] = $_POST["extreliefId"];
		$data["autoid_2"] = $_POST["autoid_2"];
		$data["userid"] = $this->_login_userid;
		$requirementsIdauto = $_POST["requirementsIdauto"];
		$requirementsId = $_POST["requirementsId"];
		if($requirementsId>0){
			$data["requirementsId"] = $requirementsId;
		}
		$requirementsId = $this->urgent_aid->savestore($data,'ah_internal_requirements_required','requirementsId');
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_internal_requirements_required/'.$requirementsId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		
				$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_3/'.$requirementsId.'" id="'.$requirementsId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
			
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_internal_requirements_required/'.$data["extreliefId"].'/'.$requirementsId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		$ex['data'] = '';
		if($_POST["requirementsId"] <=0){
		$ex['data'] .= ' <tr id="'.$requirementsId.'_required">';
		}
		$ex['data'] .= '<td id="'.$requirementsId.'_required_id">'.$requirementsIdauto.'</td>
		<td id="'.$requirementsId.'_required_typeOfMaterial">'.$this->haya_model->get_name_from_list($data["typeOfMaterial"]).'</td>
		<td id="'.$requirementsId.'_required_materials">'.$data["materials"].'</td>
		<td id="'.$requirementsId.'_required_quantity">'.$data["quantity"].'</td>
		<td id="'.$requirementsId.'_required_budgeting">'.number_format($data["budgeting"],3).'</td>
		<td>'.$action.'</td>';
			if($_POST["requirementsId"] <=0){$ex['data'] .= ' </tr>';}
		echo json_encode($ex);
		exit();
	}
//----------------------------------------------------------
function add_internal_expenses(){
		
		//print_r($_POST);exit();
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		$data = array();
		$data["prose"] = $_POST["prose"];
		$data["food"] = $_POST["food"];
		$data["reliefArticles"] = $_POST["reliefArticles"];
		$data["medicalMaterials"] = $_POST["medicalMaterials"];
		$data["other"] = $_POST["other"];
		$data["total"] = $_POST["total"];
		$data["autoid_5"] = $_POST["autoid_5"];
		
							$total_amount	=	($_POST["prose"]+$_POST["food"]+$_POST["reliefArticles"]+$_POST["medicalMaterials"]+$_POST["other"]);
		
		$data["userid"] 	= 	$this->_login_userid;
		$extExpencesIdauto	= 	$_POST["extExpencesIdauto"];
		$extExpencesId 		= 	$_POST["extExpencesId"];
		$data["notes"]  	= 	$_POST["notes"];
		if(count($_FILES["attachment1"]["name"]) > 0 and ($_FILES["attachment1"]["name"][0] !=""))
		{
			$names = array();
			$files = $_FILES["attachment1"];
			
			 foreach ($files['name'] as $key => $image) {
				if(!empty($files['name'][$key]))
				{
					$_FILES['images']['name']		=	$files['name'][$key];
					$_FILES['images']['type']		=	$files['type'][$key];
					$_FILES['images']['tmp_name']	=	$files['tmp_name'][$key];
					$_FILES['images']['error']		=	$files['error'][$key];
					$_FILES['images']['size']		=	$files['size'][$key];
					
					$names[] =	$this->upload_file($this->_login_userid,'images','resources/aid');
				}
				
			}
			if($_POST["attachment1_old"] !=""){
				if(count($names)>0)	$data['attachment']	=	$_POST["attachment1_old"].",".@implode(',',$names);
				else{$data['attachment']	=	$_POST["attachment1_old"];}
			}
			else{
				$data['attachment']	=	@implode(',',$names);	
			}
			
		}
		else{
			$data['attachment']	=	$_POST["attachment1_old"];
		}

		if($extExpencesId>0)
		{
			$data["extExpencesId"] = $extExpencesId;
		}
		else{
			$data["extreliefId"] = $_POST["extreliefIds"];
		}
		$extExpencesId = $this->urgent_aid->savestore($data,'ah_internal_expenses','extExpencesId');
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_internal_expenses/'.$extExpencesId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		
			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_4/'.$extExpencesId.'" id="'.$extExpencesId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
		
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_internal_expenses/'.$data["extreliefId"].'/'.$extExpencesId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		$ex['data'] ='';
		if($_POST["extExpencesId"]<=0)$ex['data'] .= ' <tr id="'.$extExpencesId.'_expenses">';
		$ex['data'] .= '<td  id="'.$extExpencesId.'_expenses_id">'.$data["autoid_5"].'</td>
		<td id="'.$extExpencesId.'_expenses_prose">'.number_format($data["prose"],3).'</td>
		<td  id="'.$extExpencesId.'_expenses_food">'.number_format($data["food"],3).'</td>
		<td id="'.$extExpencesId.'_expenses_reliefArticles">'.number_format($data["reliefArticles"],3).'</td>
		<td id="'.$extExpencesId.'_expenses_medicalMaterials">'.number_format($data["medicalMaterials"],3).'</td>
		<td id="'.$extExpencesId.'_expenses_other">'.number_format($data["other"],3).'</td>
		<td id="'.$extExpencesId.'_expenses_total">'.number_format($total_amount,3).'</td>
		<td id="'.$extExpencesId.'_expenses_notes">'.$data["notes"].'</td>
		<td id="'.$extExpencesId.'_expenses_attachment">';if($data["attachment"] !=""){
									   $i=0;
									   $attachment = @explode(',',$data["attachment"]);
									   foreach($attachment as $att){
										   if($att !=""){
											   $i++;
									   
									   $ex['data'] .= '<div id="picture_'.$i.'" style="float:right;">';
								  $ex['data'] .= $this->getfileicon($att,base_url().'resources/aid/'.$this->_login_userid);
								 $ex['data'] .='</div>';
								  } } } $ex['data'] .='</td>
		<td>'.$action.'</td>';
        if($_POST["extExpencesId"]<=0)$ex['data'] .= '</tr>';
		echo json_encode($ex);
		exit();
	}
//--------------------------------------------------------------------

function add_internal_daily_report_muzi(){
		
		$this->_data['permissions']	=	$permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		
		$data 					=	array();
		$data["statement"] 		=	$_POST["statement"];
		$data["currentdate"] 	=	$_POST["currentdaterep"];
		$data["extreliefId"] 	=	$_POST["external_extreliefId"];
		$data["autoid_4"] 		=	$_POST["autoid_4"];
		$data["userid"] 		=	$this->_login_userid;
		$extDataReportIddauto 	=	$_POST["extDataReportIddauto"];
		
		if($_FILES["report_attachment"]["tmp_name"])
		{
			$aid_attachemnt	=	$this->upload_file($this->_login_userid,'report_attachment','resources/aid');
			
			$data['report_attachment']	=	$aid_attachemnt;
		}
		else
		{
			// Do Something
		}
		
		if($_POST["extDataReportId"]	>	0)
		{
			$data["extDataReportId"] = $extDataReportId;
		}
		
		$extDataReportId	=	$this->urgent_aid->savestore($data,'ah_internal_daily_report','extDataReportId');
		$action 			=	'';
		
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_internal_daily_report_muzi/'.$extDataReportId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';

			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_5/'.$extDataReportId.'" id="'.$extDataReportId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';

		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_internal_daily_report_muzi/'.$data["extreliefId"].'/'.$extDataReportId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		if($data['report_attachment'])
		{
					$action	.=	' <i href="'.base_url().'resources/aid/'.$data['userid'].'/'.$data['report_attachment'].'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "></i> ';
		}

		$ex['data'] ='';
		if($_POST["extDataReportId"]<=0){
			$ex['data'] .= '<tr id="'.$extDataReportId.'_daily">';
		}
		$ex['data'] .= '<td id="'.$extDataReportId.'_daily_id">'.$data["autoid_4"].'</td>
		<td id="'.$extDataReportId.'_daily_currentdate">'.$data["currentdate"].'</td>
		<td id="'.$extDataReportId.'_daily_statement">'.$data["statement"].'</td><td>'.$action.'</td>';
		if($_POST["extDataReportId"]<=0){
           $ex['data'] .= '</tr>';
		}
		
		echo ($ex['data']);
		exit();
	}
//--------------------------------------------------------------------

function add_internal_final_report_muzi(){
		
		$this->_data['permissions']	=	$permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		
		$data 					=	array();
		$data["statement"] 		=	$_POST["final_statement"];
		$data["currentdate"] 	=	$_POST["final_currentdaterep"];
		$data["extreliefId"] 	=	$_POST["final_extreliefId"];
		$data["final_percent"] 	=	$_POST["final_percent"];
		$data["autoid_3"] 		=	$_POST["autoid_3"];
		$data["userid"] 		=	$this->_login_userid;
		$extDataReportIddauto 	=	$_POST["final_extDataReportIddauto"];
		
		if($_FILES["final_attachment"]["tmp_name"])
		{
			$final_attachment	=	$this->upload_file($this->_login_userid,'final_attachment','resources/aid');
			
			$data['report_attachment']	=	$final_attachment;
		}
		else
		{
			// Do Something
		}
		
		if($_POST["extDataReportId"]	>	0)
		{
			$data["extDataReportId"] = $extDataReportId;
		}
		
		$extDataReportId	=	$this->urgent_aid->savestore($data,'ah_internal_final_report','extDataReportId');
		$action 			=	'';
		
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_internal_final_report/'.$extDataReportId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';

			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_6/'.$extDataReportId.'" id="'.$extDataReportId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';

		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_internal_final_report/'.$data["extreliefId"].'/'.$extDataReportId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		if($data['report_attachment'])
		{
					$action	.=	' <i href="'.base_url().'resources/aid/'.$data['userid'].'/'.$data['report_attachment'].'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "></i> ';
		}

		$ex['data'] ='';
		if($_POST["extDataReportId"]<=0){
			$ex['data'] .= '<tr id="'.$extDataReportId.'_daily">';
		}
		$ex['data'] .= '<td id="'.$extDataReportId.'_daily_id">'.$data["autoid_3"].'</td>
		<td id="'.$extDataReportId.'_daily_currentdate">'.$data["currentdate"].'</td>
		<td id="'.$extDataReportId.'_final_percent">'.$data["final_percent"].'</td>
		<td id="'.$extDataReportId.'_daily_statement">'.$data["statement"].'</td><td>'.$action.'</td>';
		if($_POST["extDataReportId"]<=0){
           $ex['data'] .= '</tr>';
		}
		
		echo ($ex['data']);
		exit();
	}
//--------------------------------------------------------------------------------
	function add_internal_final_relief_committee_meeting_muzi(){
		
		$this->_data['permissions']= $permissions	=	$this->haya_model->check_other_permission(array($this->external_moduleid));
		
		$data 					=	array();
		$data["minutes"] 		=	$_POST["minutes"];
		$data["currentdate"] 	=	$_POST["currentdates"];
		$data["decision"] 		=	$_POST["decision"];
		$data["notes"] 			=	$_POST["notes"];
		$data["extreliefId"] 	=	$_POST["new_extreliefId"];
		$data["userid"] 		=	$this->_login_userid;
		
		if($_FILES["final_releif_attachemnt"]["tmp_name"])
		{
			$aid_attachemnt	=	$this->upload_file($this->_login_userid,'final_releif_attachemnt','resources/aid');
			
			$data['final_releif_attachemnt']	=	$aid_attachemnt;
		}
		else
		{
			// Do Something
		}
	
		$extrelMeetingId 		=	$_POST["extrelMeetingId"];
		
		if($extrelMeetingId	>	0)
		{
			$data["extrelMeetingId"] = $extrelMeetingId;
		}
		
		$extrelMeetingId = $this->urgent_aid->savestore($data,'ah_internal_final_relief_committee_meeting','extrelMeetingId');
		
		$action ='';
		$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'aid/view_final_internal_relief_committee_meeting_muzi/'.$extrelMeetingId.'"  style="margin-left:5px;" ><i class="my icon icon-eye-open"></i></a>';
		
			$action .=' <a href="#addingDiag" onClick="alatadad(this);" data-url="'.base_url().'aid/internal_step_7/'.$extrelMeetingId.'" id="'.$extrelMeetingId.'" data-icon="icon-pencil" data-heading="تحديث" data-color="#4096EE"><i style="color:#4096EE;" class="icon-pencil"></i></a> ';
		
		if($permissions[$this->external_moduleid]['d']	==	1) 
		{
			$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$data["extreliefId"].'" data-url="'.base_url().'aid/delete_internal_final_relief_committee_meeting/'.$data["extreliefId"].'/'.$extrelMeetingId.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
		}
		if($data['final_releif_attachemnt'])
		{
					$action	.=	' <i href="'.base_url().'resources/aid/'.$data['userid'].'/'.$data['final_releif_attachemnt'].'" class="myicon icon-camera fancybox-button fancybox.iframe" style="color:#00A403; cursor:pointer;" "></i> ';
		}

		$ex['data'] = '';
		if($_POST["extrelMeetingId"] <=0)$ex['data'] .= '<tr id="'.$extrelMeetingId.'_meeting">';
		$ex['data'] .= '<td id="'.$extrelMeetingId.'_meeting_minutes">'.$data["minutes"].'</td>
		<td id="'.$extrelMeetingId.'_meeting_currentdate">'.$data["currentdate"].'</td>
		<td id="'.$extrelMeetingId.'_meeting_decision">'.$data["decision"].'</td>
		<td id="'.$extrelMeetingId.'_meeting_notes">'.$data["notes"].'</td>
		<td>'.$action.'</td>';
		
        if($_POST["extrelMeetingId"] <=0) $ex['data'] .= '</tr> ';
		
		echo ($ex['data']);
	}
//----------------------------------------------------------
	/*
	*
	*
	*/
	public	function view_external_final_report($extDataReportId)
	{
			
		$rows = $this->urgent_aid->getdata($extDataReportId,'ah_external_final_report',"extDataReportId");
		$this->_data["heading"] =" التقرير النهائي";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="external_daily_report_addId" >
		<thead style="background-color: #029625;">
			<tr role="row" style="color:#fff !important;">
				<th>التاريخ</th>
				<th>نسبة التنفيذ</th>
				<th>البيان</th>
			</tr>
		</thead>
		<tbody> <tr>
				<td>'.$rows->currentdate.'</td>
				<td>'.$rows->final_percent.'</td>
				<td>'.$rows->statement.'</td>
			</tr> ';
								
		$this->load->view('view_external_all',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	*
	*/
	public	function view_internal_final_report($extDataReportId)
	{
			
		$rows = $this->urgent_aid->getdata($extDataReportId,'ah_internal_final_report',"extDataReportId");
		$this->_data["heading"] =" التقرير النهائي";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="external_daily_report_addId" >
		<thead style="background-color: #029625;">
			<tr role="row" style="color:#fff !important;">
				<th>التاريخ</th>
				<th>نسبة التنفيذ</th>
				<th>البيان</th>
			</tr>
		</thead>
		<tbody> <tr>
				<td>'.$rows->currentdate.'</td>
				<td>'.$rows->final_percent.'</td>
				<td>'.$rows->statement.'</td>
			</tr> ';
								
		$this->load->view('view_external_all',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	*
	*/
	public	function view_internal_daily_report_muzi($extDataReportId)
	{
			
		$rows = $this->urgent_aid->getdata($extDataReportId,'ah_internal_daily_report',"extDataReportId");
		$this->_data["heading"] ="التقرير اليومي";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="external_daily_report_addId" >
		<thead style="background-color: #029625;">
			<tr role="row" style="color:#fff !important;">
				<th>التاريخ</th>
				<th>البيان</th>
			</tr>
		</thead>
		<tbody> <tr>
				<td>'.$rows->currentdate.'</td>
				<td>'.$rows->statement.'</td>
			</tr> ';
								
		$this->load->view('view_external_all',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	*
	*/
	function view_internal_expenses($extExpencesId)
	{
		$rows 			=	$this->urgent_aid->getdata($extExpencesId,'ah_internal_expenses',"extExpencesId");
		$total_amount	=	($rows->prose+$rows->food+$rows->reliefArticles+$rows->medicalMaterials+$rows->other);
		$this->_data["heading"] ="المصروفات";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="external_daily_report_addId" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
									<th>النثرية</th>
									<th>المواد الغذائية</th>
									<th>المواد الاغاثية</th>
									<th>المواد الطبية</th>
									<th>اخرى</th>
									<th>الاجمالي</th>
									<th>الاجمالي</th>
                                </tr>
                            </thead>
                            <tbody> <tr>
                                    <td>'.number_format($rows->prose,3).'</td>
                                    <td>'.number_format($rows->food,3).'</td>
									<td >'.number_format($rows->reliefArticles,3).'</td>
                                    <td>'.number_format($rows->medicalMaterials,3).'</td>
                                    <td>'.number_format($rows->other,3).'</td>
									<td>'.number_format($total_amount,3).'</td>
									<td>'.$rows->notes.'</td>
                                </tr> ';
								
		$this->load->view('view_external_all',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	*
	*/
	function view_internal_requirements_required($requirementsId)
	{
		$rows = $this->urgent_aid->getdata($requirementsId,'ah_internal_requirements_required',"requirementsId");
		
		$this->_data["heading"] =	"الاحتياجات المطلوبة";
		$this->_data["content"] =	'<table class="table table-bordered table-striped"  id="external_daily_report_addId" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                    <th>نوع المواد</th>
                                    <th>المواد</th>
									<th>الكمية</th>
                                    <th>الموازنة</th>  
                                </tr>
                            </thead>
                            <tbody> <tr>
                                    <td>'.$this->haya_model->get_name_from_list($rows->typeOfMaterial).'</td>
                                    <td>'.$rows->materials.'</td>
									<td>'.$rows->quantity.'</td>
									<td>'.number_format($rows->budgeting,3).'</td>
                                </tr> ';
		$this->load->view('view_external_all',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	*
	*/
	function view_internal_reliefteam($extreliefTMId)
	{
		
		$rows = $this->urgent_aid->getdata($extreliefTMId,'ah_internal_reliefteam',"extreliefTMId");
		 $this->_data["heading"] ="أعضاء فريق الاغاثة";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="internal_daily_report_addId" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                    <th>الاسم</th>
									<th>الوظيفة</th>
                                    <th>الجهة</th>
                                </tr>
                            </thead>
                            <tbody> <tr>
                                    <td>'.$rows->name.'</td>
									<td>'.$rows->job.'</td>
                                    <td>'.$rows->organization.'</td>
                                </tr> ';
		$this->load->view('view_external_all',$this->_data);
	
	}
//----------------------------------------------------------
	/*
	*
	*
	*/
	function view_final_relief_committee_meeting_muzi($extrelMeetingId){
		$rows = $this->urgent_aid->getdata($extrelMeetingId,'ah_final_relief_committee_meeting',"extrelMeetingId");
		$this->_data["heading"] =" قرار لجنة الإغاثة النهائي";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="final_relief_committee_table" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                   <th>رقم محضر الاجتماع</th>
									<th>التاريخ</th>
									<th>قرار اللجنة</th>
									<th>الملاحظات</th>
                                </tr>
                            </thead>
                            <tbody> <tr>
                                    <td >'.$rows->minutes.'</td>
                                    <td>'.$rows->currentdate.'</td>
                                    <td>'.$rows->decision.'</td>
									<td>'.$rows->notes.'</td>
                                </tr> ';
		$this->load->view('view_external_all',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	*
	*/
	function view_final_internal_relief_committee_meeting_muzi($extrelMeetingId){
		$rows = $this->urgent_aid->getdata($extrelMeetingId,'ah_internal_final_relief_committee_meeting',"extrelMeetingId");
		$this->_data["heading"] =" قرار لجنة الإغاثة النهائي";
		$this->_data["content"] ='<table class="table table-bordered table-striped "  id="final_relief_committee_table" >
                            <thead style="background-color: #029625;">
                                <tr role="row" style="color:#fff !important;">
                                   <th>رقم محضر الاجتماع</th>
									<th>التاريخ</th>
									<th>قرار اللجنة</th>
									<th>الملاحظات</th>
                                </tr>
                            </thead>
                            <tbody> <tr>
                                    <td >'.$rows->minutes.'</td>
                                    <td>'.$rows->currentdate.'</td>
                                    <td>'.$rows->decision.'</td>
									<td>'.$rows->notes.'</td>
                                </tr> ';
		$this->load->view('view_external_all',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	* Update Status 1 for DELETE the Record
	*/
	function delete_external_final_report($extreliefId,$extDataReportId)
	{
		$this->urgent_aid->delete($extDataReportId,'extDataReportId',"ah_external_final_report");
		
		$ex['data'] 	= $extDataReportId."_daily";
		$result  		= $this->urgent_aid->getdmessagedata($extreliefId,'ah_external_final_report',"extreliefId");
		$data 			= count($result);
		$ex['count'] 	=	+$data;
		
		echo json_encode($ex);
		exit();
	}
//----------------------------------------------------------
	/*
	*
	* Update Status 1 for DELETE the Record
	*/
	function delete_internal_final_report($extreliefId,$extDataReportId)
	{
		$this->urgent_aid->delete($extDataReportId,'extDataReportId',"ah_internal_final_report");
		
		$ex['data'] 	= $extDataReportId."_meeting";
		$result  		= $this->urgent_aid->getdmessagedata($extreliefId,'ah_internal_final_report',"extreliefId");
		$data 			= count($result);
		$ex['count'] 	=	+$data;
		
		echo json_encode($ex);
		exit();
	}
//----------------------------------------------------------
	/*
	*
	* Update Status 1 for DELETE the Record
	*/
	function delete_internal_daily_report_muzi($extreliefId,$extDataReportId)
	{
		$this->urgent_aid->delete($extDataReportId,'extDataReportId',"ah_internal_daily_report");
		
		$ex['data'] 	= $extDataReportId."_meeting";
		$result  		= $this->urgent_aid->getdmessagedata($extreliefId,'ah_internal_daily_report',"extreliefId");
		$data 			= count($result);
		$ex['count'] 	=	+$data;
		
		echo json_encode($ex);
		exit();
	}
//----------------------------------------------------------
	/*
	*
	* Update Status 1 for DELETE the Record
	*/
	function delete_internal_expenses($extreliefId,$extExpencesId)
	{
		$this->urgent_aid->delete($extExpencesId,'extExpencesId',"ah_internal_expenses");
		$ex['data'] 	= 	$extExpencesId."_expenses";
		$result  		= 	$this->urgent_aid->getdmessagedata($extreliefId,'ah_internal_expenses',"extreliefId");
		$data 			= 	count($result);
		$ex['count'] 	=	++$data;
		
		echo json_encode($ex);
		exit();
	}
//----------------------------------------------------------
	/*
	*
	* Update Status 1 for DELETE the Record
	*/
	function delete_internal_requirements_required($extreliefId,$requirementsId)
	{
		$this->urgent_aid->delete($requirementsId,'requirementsId',"ah_internal_requirements_required");
		
		$ex['data']	=	$requirementsId."_required";
		$result  	=	$this->urgent_aid->getdmessagedata($extreliefId,'ah_internal_requirements_required',"extreliefId");
		$data 		=	count($result);
		
		$ex['count'] =++$data;
		echo json_encode($ex);
		exit();
	}
//----------------------------------------------------------
	/*
	*
	* Update Status 1 for DELETE the Record
	*/
	function delete_internal_reliefteam($extreliefId,$extreliefTMId)
	{
		$this->urgent_aid->delete($extreliefTMId,'extreliefTMId',"ah_internal_reliefteam");
		$ex['data'] = $extreliefTMId."_team";
		$result  = $this->urgent_aid->getdmessagedata($extreliefId,'ah_internal_reliefteam',"extreliefId");
		$data = count($result);
		$ex['count'] =++$data;
		echo json_encode($ex);
		exit();
	}
//----------------------------------------------------------
	/*
	*
	* Update Status 1 for DELETE the Record
	*/
	function delete_final_relief_committee_meeting($extreliefId,$extreliefTMId)
	{
		$this->urgent_aid->delete($extreliefTMId,'extrelMeetingId',"ah_final_relief_committee_meeting");
		$ex['data'] =	$extreliefTMId."_meeting";
		$result  	=	$this->urgent_aid->getdmessagedata($extreliefId,'ah_final_relief_committee_meeting',"extreliefId");
		
		$data = count($result);
		$ex['count'] =++$data;
		echo json_encode($ex);
		exit();
	}
//----------------------------------------------------------
	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function external_step_1($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($_FILES["aid_attachemnt"]["tmp_name"])
			{
				$aid_attachemnt	=	$this->upload_file($this->_login_userid,'aid_attachemnt','resources/aid');
			
				$data['aid_attachemnt']	=	$aid_attachemnt;
			}
			else
			{
				// Do Something
			}
			// Update The Record
			$this->urgent_aid->update_external_record($data['extrelMeetingId'],'extrelMeetingId','ah_external_relief_committee_meeting',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'extrelMeetingId','ah_external_relief_committee_meeting');
		
		$this->load->view('external-step-1',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function external_step_2($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			// Update The Record
			$this->urgent_aid->update_external_record($data['extreliefTMId'],'extreliefTMId','ah_external_reliefteam',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'extreliefTMId','ah_external_reliefteam');
		
		$this->load->view('external-step-2',$this->_data);
	}
//----------------------------------------------------------

	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function external_step_3($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();

			// Update The Record
			$this->urgent_aid->update_external_record($data['requirementsId'],'requirementsId','ah_external_requirements_required',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'requirementsId','ah_external_requirements_required');
		
		$this->load->view('external-step-3',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function external_step_4($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($_FILES["attachment1"]["tmp_name"])
			{
				$aid_attachemnt	=	$this->upload_file($this->_login_userid,'attachment1','resources/aid');
			
				$data['attachment1']	=	$attachment1;
			}
			else
			{
				// Do Something
			}
			
			// Update The Record
			$this->urgent_aid->update_external_record($data['extExpencesId'],'extExpencesId','ah_external_expenses',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'extExpencesId','ah_external_expenses');
		
		$this->load->view('external-step-4',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function external_step_5($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($_FILES["report_attachment"]["tmp_name"])
			{
				$report_attachment	=	$this->upload_file($this->_login_userid,'report_attachment','resources/aid');
			
				$data['report_attachment']	=	$report_attachment;
			}
			else
			{
				// Do Something
			}
			
			// Update The Record
			$this->urgent_aid->update_external_record($data['extDataReportId'],'extDataReportId','ah_external_daily_report',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'extDataReportId','ah_external_daily_report');
		
		$this->load->view('external-step-5',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function external_step_6($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($_FILES["report_attachment"]["tmp_name"])
			{
				$report_attachment	=	$this->upload_file($this->_login_userid,'report_attachment','resources/aid');
			
				$data['report_attachment']	=	$report_attachment;
			}
			else
			{
				// Do Something
			}
			// Update The Record
			$this->urgent_aid->update_external_record($data['extDataReportId'],'extDataReportId','ah_external_final_report',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'extDataReportId','ah_external_final_report');
		
		$this->load->view('external-step-6',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function external_step_7($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($_FILES["final_releif_attachemnt"]["tmp_name"])
			{
				$final_releif_attachemnt	=	$this->upload_file($this->_login_userid,'final_releif_attachemnt','resources/aid');
			
				$data['final_releif_attachemnt']	=	$final_releif_attachemnt;
			}
			else
			{
				// Do Something
			}

			// Update The Record
			$this->urgent_aid->update_external_record($data['extrelMeetingId'],'extrelMeetingId','ah_final_relief_committee_meeting',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'extrelMeetingId','ah_final_relief_committee_meeting');
		
		$this->load->view('external-step-7',$this->_data);
	}
//----------------------------------------------------------

//----------------------------------------------------------
	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function internal_step_1($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($_FILES["aid_attachemnt"]["tmp_name"])
			{
				$aid_attachemnt	=	$this->upload_file($this->_login_userid,'aid_attachemnt','resources/aid');
			
				$data['aid_attachemnt']	=	$aid_attachemnt;
			}
			else
			{
				// Do Something
			}
			// Update The Record
			$this->urgent_aid->update_external_record($data['internalMeetingId'],'internalMeetingId','ah_internal_relief_committee_meeting',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'internalMeetingId','ah_internal_relief_committee_meeting');
		
		$this->load->view('internal-step-1',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function internal_step_2($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			// Update The Record
			$this->urgent_aid->update_external_record($data['extreliefTMId'],'extreliefTMId','ah_internal_reliefteam',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'extreliefTMId','ah_internal_reliefteam');
		
		$this->load->view('internal-step-2',$this->_data);
	}
//----------------------------------------------------------

	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function internal_step_3($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();

			// Update The Record
			$this->urgent_aid->update_external_record($data['requirementsId'],'requirementsId','ah_internal_requirements_required',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'requirementsId','ah_internal_requirements_required');
		
		$this->load->view('internal-step-3',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function internal_step_4($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($_FILES["attachment"]["tmp_name"])
			{
				$attachment	=	$this->upload_file($this->_login_userid,'attachment','resources/aid');
			
				$data['attachment']	=	$attachment;
			}
			else
			{
				// Do Something
			}
			
			// Update The Record
			$this->urgent_aid->update_external_record($data['extExpencesId'],'extExpencesId','ah_internal_expenses',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'extExpencesId','ah_internal_expenses');
		
		$this->load->view('internal-step-4',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function internal_step_5($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($_FILES["report_attachment"]["tmp_name"])
			{
				$report_attachment	=	$this->upload_file($this->_login_userid,'report_attachment','resources/aid');
			
				$data['report_attachment']	=	$report_attachment;
			}
			else
			{
				// Do Something
			}
			
			// Update The Record
			$this->urgent_aid->update_external_record($data['extDataReportId'],'extDataReportId','ah_internal_daily_report',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'extDataReportId','ah_internal_daily_report');
		
		$this->load->view('internal-step-5',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function internal_step_6($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($_FILES["report_attachment"]["tmp_name"])
			{
				$report_attachment	=	$this->upload_file($this->_login_userid,'report_attachment','resources/aid');
			
				$data['report_attachment']	=	$report_attachment;
			}
			else
			{
				// Do Something
			}
			// Update The Record
			$this->urgent_aid->update_external_record($data['extDataReportId'],'extDataReportId','ah_internal_final_report',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'extDataReportId','ah_internal_final_report');
		
		$this->load->view('internal-step-6',$this->_data);
	}
//----------------------------------------------------------
	/*
	*
	* Edit Form / Update The Records
	* @param $id integer
	*/
	public function internal_step_7($id)
	{
		if($this->input->post())
		{
			$data	=	$this->input->post();
			
			if($_FILES["final_releif_attachemnt"]["tmp_name"])
			{
				$final_releif_attachemnt	=	$this->upload_file($this->_login_userid,'final_releif_attachemnt','resources/aid');
			
				$data['final_releif_attachemnt']	=	$final_releif_attachemnt;
			}
			else
			{
				// Do Something
			}

			// Update The Record
			$this->urgent_aid->update_external_record($data['extrelMeetingId'],'extrelMeetingId','ah_internal_final_relief_committee_meeting',$data);
			
			// Redirect To the Page
			return TRUE;
			exit();
		}
		
		$this->_data['all_details']	=	$this->urgent_aid->get_detail($id,'extrelMeetingId','ah_internal_final_relief_committee_meeting');
		
		$this->load->view('internal-step-7',$this->_data);
	}
//----------------------------------------------------------					
}
<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
       <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"> <a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url();?>budget/add_budget_by_year" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> </div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                     	 <th>Year</th>
                        <th>Total Amount</th>
                        <th>Remaining Amount</th>
                        <th>الإجراءات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'budget/ajax_all_budget_by_year','columns_array'=>'{ "data": "Year" },{ "data": "Total Amount" },{ "data": "Remaining Amount" },
				{ "data": "الإجراءات" }')); ?>
</div>
</body>
</html>



</script>

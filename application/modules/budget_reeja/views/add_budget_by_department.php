<div class="row col-md-12">
  <form  method="POST" id="save_bddata_form2" name="save_bddata_form2">
  <input type="hidden" name="bd_id" id="bd_id" value="<?php echo $bd_id?>"/>
  <input type="hidden" name="bds_id" id="bds_id" value="<?php echo $bds_id?>"/>
  <input type="hidden" name="bds_categoryId" id="bds_categoryId" value="<?php echo $bds_categoryId?>"/>
   
    
    
  	<?php if($bds_categoryId == 0){?>
    <input type="hidden" name="currentremaining" id="currentremaining" value="<?php echo $mrow->bd_remainingAmount?>"/>
     <input type="hidden" name="bd_totalamount" id="bd_totalamount" value="<?php echo $mrow->bd_totalamount?>"/>
    <input type="hidden" name="bd_remainingAmount" id="bd_remainingAmount" value="<?php echo $mrow->bd_remainingAmount?>"/>
    <div class="form-group col-md-4">
      <label for="basic-input">Total Budget : </label>
      <b id="totalamount"><?php echo 'OMR '.number_format ( $mrow->bd_totalamount,3,'.',',') ?></b>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">Current : </label>
      <b id="currentAmount">0</b>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">Remaining : </label>
      <b>OMR <span id="remainingAmount"><?php echo number_format ($mrow->bd_remainingAmount,3,'.',',')  ?></span></b>
    </div>
    
    <?php }else { ?>
     <div class="form-group col-md-4">
      <label for="basic-input">Total Budget : </label>
      <b id="totalamount"><?php echo 'OMR '.number_format ($prev->bds_totalAmount,3,'.',',') ?></b>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">Current : </label>
      <b id="currentAmount">0</b>
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">Remaining : </label>
      <b id="remainingAmount"><?php echo 'OMR '.number_format ($prev->bds_remainingAmount,3,'.',',')   ?></b>
    </div>
    <input type="hidden" name="currentremaining" id="currentremaining" value="<?php echo $prev->bds_remainingAmount?>"/>
     <input type="hidden" name="bd_totalamount" id="bd_totalamount" value="<?php echo $mrow->bd_totalamount?>"/>
     <input type="hidden" name="bd_remainingAmount" id="bd_remainingAmount" value="<?php echo $prev->bds_remainingAmount?>"/>
    <?php } ?>
   
  <br clear="all" />
   <h4 style="border-bottom: 2px solid #EEE;">Budget Information</h4> 
   
    <div class="form-group col-md-6">
      <label for="basic-input">Title</label>
      <input type="text" name="bds_title" id="bds_title" class="form-control req" value="<?php echo $row->bds_title?>" required="required" />
    </div>
    <div class="form-group col-md-6">
      <label for="basic-input">Amount <span style="margin-right:20px; color:#F00; float:left;" id="erroramount" ></span>‎</label>
      <input type="text" name="bds_totalAmount" id="bds_totalAmount" class="form-control req" value="<?php echo number_format ( $row->bds_totalAmount,3,'.',',')?>" onchange ="calculateAmount(this);"  required="required"/>
    </div>
   <br clear="all" />
   <h4 style="border-bottom: 2px solid #EEE;">تفاصيل البنك</h4> 
    <div class="multiple-block">
                  <div class="col-md-3 form-group">
                    <label class="text-warning">إسم البنك:</label>
                    <?php echo $this->haya_model->create_dropbox_list_help('bds_bankname_id','bank',$row->bds_bankname_id,0,'req','','1','bds_branch_id1'); ?> </div>
                  <div class="col-md-3 form-group">
                    <label class="text-warning">الفرع:</label>
                    <?php echo $this->haya_model->create_dropbox_list_help('bds_branch_id','bank_branch',$row->bds_branch_id,$bankid,'req','','1'); ?> </div>
                  <div class="col-md-3 form-group">
                    <label class="text-warning">رقم حساب  :</label>
                    <input name="bds_account_no"  placeholder="رقم حساب" id="bds_account_no" type="text" class="form-control req" onKeyUp="only_numeric(this);"value="<?php echo $row->bds_account_no?>" >
                  </div>
                  <!--<div class="col-md-3 form-group">
                    <label class="text-warning">القيمة  :</label>
                    <input name="amount_in_account[1]" placeholder="القيمة" id="amount_in_account" type="text" class="form-control amount_in_account req" onKeyUp="add_total_budget(this);">
                  </div>-->
                  <div class="col-md-3 form-group">
                    <label class="text-warning">رقم الهاتف  :</label>
                    <input name="bds_phone_no"  placeholder="رقم الهاتف" id="bds_phone_no" type="text" class="form-control req" onKeyUp="only_numeric(this);" required="required" value="<?php echo $row->bds_phone_no?>">
                  </div>
                  <div class="col-md-3 form-group">
                    <label class="text-warning">رقم الفاكس  :</label>
                    <input name="bds_fax_no"  placeholder="رقم الفاكس" id="bds_fax_no" type="text" class="form-control req" onKeyUp="only_numeric(this);" required="required" value="<?php echo $row->bds_fax_no?>">
                  </div>
                  <div class="col-md-3 form-group">
                    <label class="text-warning">البريد الإلكتروني  :</label>
                    <input name="bds_email_address"  placeholder="البريد الإلكتروني" id="bds_email_address" type="text" class="form-control req" required="required" value="<?php echo $row->bds_email_address?>">
                  </div>
                </div>
                <div class="row col-md-12">
  <div class="form-group  col-md-12" >
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit_budget" value="حفظ" />
  </div>
</div>
  </form>
</div>

<script type="text/javascript">
 $(document).ready(function(){
	$('#submit_budget').click(function () {
		check_my_session();
        $('#save_bddata_form2 .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#save_bddata_form2 .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#save_bddata_form2 .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                var request = $.ajax({
			  url: config.BASE_URL+'budget/add_budget_by_department/<?php echo $bd_id?>',
			  type: "POST",
			  data: {'bd_id':$('#bd_id').val(),'bds_id':$('#bds_id').val() ,'bds_title':$('#bds_title').val(),'bds_totalAmount':$('#bds_totalAmount').val(),'bds_bankname_id':$('#bds_bankname_id1').val() ,'bds_branch_id':$('#bds_branch_id1').val(),'bds_account_no':$('#bds_account_no').val(),'bds_phone_no':$('#bds_phone_no').val() ,'bds_fax_no':$('#bds_fax_no').val(),'bds_email_address':$('#bds_email_address').val(),'bds_categoryId':$('#bds_categoryId').val()},
			  dataType: "html",
			  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
			  success: function(msg)
			  {		
			  	 $('#ajax_action').hide();
				console.log(msg);
				
				 $('#addingDiag').modal('hide');	  
				 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
				location.reload();
			  }
			});
            
        }
        else 
		{
            show_notification_error_end(ht);
        }
		
       
    });
	document.getElementById("bds_totalAmount").onblur =function (){    
    this.value = parseFloat(this.value.replace(/,/g, ""))
                    .toFixed(3)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    
}
});
function calculateAmount(obj){
	var vals =obj.value;
	
	var bd_remainingAmount = $('#bd_remainingAmount').val();
	var remainingAmount = bd_remainingAmount- vals;
	
	if(remainingAmount >= 0){
		$('#remainingAmount').html(remainingAmount);
		$('#currentremaining').val(remainingAmount);
		$('#currentAmount').html(vals);
		$('#bds_totalAmount').css('border-color','#029625');
	}
	else{
		$('#currentremaining').val(bd_remainingAmount);
		objs ='0.000';
		$('#bds_totalAmount').val(objs);
		$('#bds_totalAmount').css('border-color','#F00');
		$(this).focus();
	}
	/*$('#bd_remainingAmount').html($('#bd_remainingAmount').val());
	
	var remainingAmount = $('#remainingAmounth').val();
	if(parseFloat(remainingAmount) < vals){
		$('#').html('Amount is too high');
		obj.value ='';
		
	}
	else{
		$('#currentAmount').html(vals);
		
		var rem = remainingAmount - vals;
		$('#remainingAmount').html(rem);	
	}
	*/
}

</script>
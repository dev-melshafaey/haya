<div class="row col-md-12">
  <form  method="POST" id="save_bdata_form2" name="save_bdata_form2">
  <input type="hidden" name="bd_id" id="bd_id" value="<?php echo $row->bd_id?>"/>
    <div class="form-group col-md-6">
      <label for="basic-input">Year</label>
      <select  name="bd_year"  id="bd_year" class="form-control req">
      	<?php $firstD = date('Y')-5;
			 $lastD = date('Y')+10;
			 for($i=$firstD; $i<=$lastD; $i++ ) {
				 
				 if($i == date('Y')){ 
				 	
						if($row->bd_year == $i){
							echo ' <option value="'.$i.'" selected="selected">'.$i.'</option>';
						}
						else{
							echo ' <option value="'.$i.'">'.$i.'</option>';
						}
				}
				 else{ 
				 	if($row->bd_year == $i){
						 echo ' <option value="'.$i.'" selected="selected">'.$i.'</option>';
					}
					else{
						echo ' <option value="'.$i.'">'.$i.'</option>';
					}
				}
				
			}	
			 
		
		?>
       
      </select>
    </div>
   
   
    <div class="form-group col-md-6">
      <label for="basic-input">Total Amount‎</label>
      <input type="text" pattern="[0-9]*" name="bd_totalamount" id="bd_totalamount" class="form-control req" value="<?php echo number_format ($row->bd_totalamount,3,'.',',') ?>" />
    </div>
  </form>
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="submit_budget" value="حفظ" />
  </div>
</div>
<script type="text/javascript">
 $(document).ready(function(){
	$('#submit_budget').click(function () {
		check_my_session();
            var bd_totalamount=   $('#bd_totalamount').val().replace(",","");
			var request = $.ajax({
			  url: config.BASE_URL+'budget/add_budget_by_year',
			  type: "POST",
			  data: {'bd_year':$('#bd_year').val(),'bd_totalamount':bd_totalamount ,'bd_id':$('#bd_id').val()},
			  dataType: "html",
			  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
			  success: function(msg)
			  {		
			  	 $('#ajax_action').hide();
				 $('#addingDiag').modal('hide');	  
				 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
				location.reload();
			  }
			});
            
       
    });
	document.getElementById("bd_totalamount").onblur =function (){    
    this.value = parseFloat(this.value.replace(/,/g, ""))
                    .toFixed(3)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    
}
});

</script>
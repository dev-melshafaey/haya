<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Budget extends CI_Controller {

	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;
	public $administrator =	NULL;
	public $servicedepartment =	NULL;
	public $moduleid = NULL;
	public $otherservice_moduleid = NULL;
//-----------------------------------------------------------------------

	/*
	* Constructor
	*/
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('budget_model','budget');
		/////////////////////////////////////////	
		$this->_data['controller']=$this; 
		$this->moduleid = 264;
		/////////////////////////
		$this->_data['moduleid']			= $this->moduleid;
		
		$this->_data['module']		=	$this->haya_model->get_module();		
		$this->_login_userid 			=	$this->session->userdata('userid');
		$this->_data['login_userid'] 	=	$this->_login_userid;
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);
			
		
		$this->_data['login_userid']	=	$this->_login_userid;
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);
		$this->userroleid				=  $this->_data['user_detail']['profile']->userroleid;
		// Load all types
		$this->_data['list_types']		=	$this->haya_model->get_listmanagment_types();
		
		//$this->add_users_yearly_holidays(); // Add Yearly holiday into users accounts.
		
	}
	public function index()
	{
		$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
		$this->load->view('listall_budget_by_year',$this->_data);
	}
	public function ajax_all_budget_by_year()
	{
			$this->db->select('*');
			$this->db->from('ah_budget_main');						
			$this->db->where("delete_record",'0');
			$this->db->order_by('bd_id','DESC');
			
			$query = $this->db->get();
			//$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
						
			foreach($query->result() as $lc)
			{
				if($lc->bd_id)
				{
					$action 	='<a onclick="alatadad(this);" data-url="'.base_url().'budget/view_budget_by_year/'.$lc->bd_id.'" href="#"><i class="my icon icon-eye-open"></i></a>';
				}
				if($lc->userid == $this->_login_userid){
					$action	.= ' <a href="#globalDiag" onclick="alatadad(this);" data-url="'. base_url().'budget/add_budget_by_year/'.$lc->bd_id.'" id="'.$lc->bd_id.'"><i class="icon-pencil"></i></a>';
				}
				/*if($permissions[$this->moduleid]['d']	==	1 ) 
				{*/
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->bd_id.'" data-url="'.base_url().'budget/delete_budget_by_year/'.$lc->bd_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				/*}*/
				
				$arr[] = array(
					"DT_RowId"		=>	$lc->bd_id.'_durar_lm',
					"Year" 		=> '<a href="'.base_url().'budget/list_budget_by_department/'.$lc->bd_id.'/0" >'.$lc->bd_year.'</a>',
					"Total Amount" 		=> '<a href="'.base_url().'budget/list_budget_by_department/'.$lc->bd_id.'/0" >OMR '.number_format ($lc->bd_totalamount,3,'.',',').'</a>',	
					"Remaining Amount" 		=> '<a href="'.base_url().'budget/list_budget_by_department/'.$lc->bd_id.'/0" >OMR '.number_format ($lc->bd_remainingAmount,3,'.',',').'</a>',		
					"الإجراءات" 		=>	$action 
					);
					
					unset($action,$items);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
	}
	public function add_budget_by_year($bd_id=0)
	{
		if($this->input->post())
		{
			$data = $this->input->post();
			$buget["bd_year"] = $data["bd_year"];
			$buget["bd_totalamount"] = str_replace(",","",$data["bd_totalamount"]);
			$buget["bd_remainingAmount"] = str_replace(",","",$data["bd_totalamount"]);
			$buget["bd_date"] = date('Y-m-d');
			$buget["userid"] =  $this->_login_userid;
			if($data["bd_id"] <= 0){
				$buget["bd_id"] =0;
			}
			else{
				$buget["bd_id"] =$data["bd_id"];
			}
				
			 $this->budget->savestore($buget,'ah_budget_main','bd_id');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			exit();
		}
		$this->_data['bd_id'] =$bd_id;
		if($bd_id > 0){
			
			$this->_data['row'] = $this->budget->getdata($bd_id,'ah_budget_main','bd_id');
		}
		$this->load->view('add_budget_by_year',$this->_data);
	}
	function delete_budget_by_year($bd_id){
		$this->budget->delete($bd_id,'bd_id',"ah_budget_main");
		
		redirect(base_url().'aid/');
		exit();
	}
	function view_budget_by_year($bd_id){
		$this->_data['row'] = $this->budget->getdata($bd_id,'ah_budget_main','bd_id');
		
		$this->load->view('view_budget_by_year',$this->_data);
	}
	function list_budget_by_department($bd_id,$bds_categoryId){
		$this->_data['row'] = $this->budget->getdata($bd_id,'ah_budget_main','bd_id');
		$this->_data['bd_id'] = $bd_id;
		$this->_data['bds_categoryId'] = $bds_categoryId;
		$this->load->view('list_budget_by_department',$this->_data);
	}
	public function ajax_all_budget_by_department($bd_id,$bds_categoryId)
	{
			$this->db->select('*');
			$this->db->from('ah_budget_sub');						
			$this->db->where("delete_record",'0');
			$this->db->where("bd_id",$bd_id);
			$this->db->where("bds_categoryId",$bds_categoryId);
			$this->db->order_by('bds_id','DESC');
			
			$query = $this->db->get();
			//$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
						
			foreach($query->result() as $lc)
			{
				if($lc->bds_id)
				{
					//$action 	='<a onclick="alatadad(this);" data-url="'.base_url().'budget/view_budget_by_department/'.$lc->bds_id.'" href="#"><i class="my icon icon-eye-open"></i></a>';
				}
				if($lc->userid == $this->_login_userid){
					$action	.= ' <a href="#globalDiag" onclick="alatadad(this);" data-url="'. base_url().'budget/add_budget_by_department/'.$lc->bd_id.'/'.$lc->bds_id.'/'.$lc->bds_id.'" id="'.$lc->bds_id.'"><i class="icon-pencil"></i></a>';
				}
				/*if($permissions[$this->moduleid]['d']	==	1 ) 
				{*/
					//$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->bds_id.'" data-url="'.base_url().'budget/delete_budget_by_department/'.$lc->bds_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				/*}*/
				
				$arr[] = array(
					"DT_RowId"		=>	$lc->bd_id.'_durar_lm',
					"Title" 		=> '<a href="'.base_url().'budget/list_budget_by_department/'.$lc->bd_id.'/'.$lc->bds_id.'" >'.$lc->bds_title.'</a>',	
					"Total Amount" 		=> '<a href="'.base_url().'budget/list_budget_by_department/'.$lc->bd_id.'/'.$lc->bds_id.'" >OMR '.number_format ($lc->bds_totalAmount,3,'.',',').'</a>',		
					"Remaining Amount" 		=> '<a href="'.base_url().'budget/list_budget_by_department/'.$lc->bd_id.'/'.$lc->bds_id.'" >OMR '.number_format ($lc->bds_remainingAmount,3,'.',',').'</a>',
					"الإجراءات" 		=>	$action 
					);
					
					unset($action,$items);
			}
			
			$ex['data'] = $arr;
			
			echo json_encode($ex);
	}
	public function add_budget_by_department($bd_id,$bds_categoryId=0,$bds_id=0)
	{
		$this->_data['bd_id'] =$bd_id;
		$this->_data['bds_id'] =$bds_id;
		$this->_data['bds_categoryId'] =$bds_categoryId;
		if($bds_id > 0){
			$this->_data['row'] =  $this->budget->getdata($bds_id,'ah_budget_sub','bds_id');
		}
		$this->_data['mrow'] =  $this->budget->getdata($bd_id,'ah_budget_main','bd_id');
		if($bds_categoryId >0)
		$this->_data['prev']  =  $this->budget->getdata($bds_categoryId,'ah_budget_sub','bds_id');
		if($this->input->post())
		{
			$data = $this->input->post();
			$buget["userid"] =  $this->_login_userid;
			$buget["bds_date"] = date('Y-m-d');
			$buget["bd_id"] = $bd_id = $data["bd_id"];
			$buget["bds_title"] = $data["bds_title"];
			$buget["bds_totalAmount"] = str_replace(",","",$data["bds_totalAmount"]);
			$buget["bds_remainingAmount"] = str_replace(",","",$data["bds_totalAmount"]);
			$buget["bds_bankname_id"] = $data["bds_bankname_id"];
			$buget["bds_branch_id"] = $data["bds_branch_id"];
			$buget["bds_account_no"] = $data["bds_account_no"];
			$buget["bds_phone_no"] = $data["bds_phone_no"];
			$buget["bds_fax_no"] = $data["bds_fax_no"];
			$buget["bds_categoryId"] = $bds_categoryId =  $data["bds_categoryId"];
			$buget["bds_email_address"] = $data["bds_email_address"];
			$maindata =  $this->budget->getdata($bd_id,'ah_budget_main','bd_id');
			if($data["bds_id"] <= 0){
				
				if($data["bds_categoryId"] == 0){
					$remaining = $maindata->bd_remainingAmount;
					$remainingvals =$remaining - (str_replace(",","",$data["bds_totalAmount"]));
					$main["bd_remainingAmount"]=$remainingvals; 
					$main["bd_id"]=$bd_id;
					$this->budget->savestore($main,'ah_budget_main','bd_id');
				}
				else{
					$maindata =  $this->budget->getdata($bds_categoryId,'ah_budget_sub','bds_id');
					$remaining = $maindata->bds_remainingAmount;
					$remainingvals =$remaining - $data["bds_totalAmount"];
					$main["bds_remainingAmount"]=$remainingvals; 
					$main["bds_id"]=$bds_categoryId;
					
					$this->budget->savestore($main,'ah_budget_sub','bds_id');
				}
				
				$buget["bds_id"] =0;
			}
			else{
				if($bds_categoryId == 0){
					$subdata =  $this->budget->getdata($bds_id,'ah_budget_sub','bds_id');
					$prev = $subdata->bds_totalAmount;
					$maindata =  $this->budget->getdata($bd_id,'ah_budget_main','bd_id');
					$remaining = $maindata->bd_remainingAmount;
					$remainingvals =($remaining + $prev) - $data["bds_totalAmount"];
					$main["bd_remainingAmount"]=$remainingvals;
					$main["bd_id"]=$bd_id;
					$this->budget->savestore($main,'ah_budget_main','bd_id');
				}
				else{
					$maindata =  $this->budget->getdata($bds_categoryId,'ah_budget_sub','bds_id');
					$remaining = $maindata->bds_remainingAmount;
					$subdata =  $this->budget->getdata($bds_id,'ah_budget_sub','bds_id');
					$prev = $subdata->bds_remainingAmount;
					$remainingvals =($remaining + $prev) - $data["bds_totalAmount"];
					$main["bds_remainingAmount"]=$remainingvals;
					$main["bds_id"]=$bds_categoryId;
					$this->budget->savestore($main,'ah_budget_sub','bds_id');
				}
				$buget["bds_id"] = $data["bds_id"];
			}
				
			 $this->budget->savestore($buget,'ah_budget_sub','bds_id');
			 
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			exit();
		}
		
		
		if($bds_id > 0){
			$this->_data['rows'] = $this->budget->getdata($bds_id,'ah_budget_sub','bds_id');
		}
		$this->load->view('add_budget_by_department',$this->_data);
	}
	
}
?>
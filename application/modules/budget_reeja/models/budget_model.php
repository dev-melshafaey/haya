<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Budget_model extends CI_Model
{
	/*
	*  Properties
	*/
	private $_table_users;
	private $_table_branchs;
	private $_table_listmanagement;
 	
//-------------------------------------------------------------------

	/*
	*  Constructor
	*/
	function __construct()
	{
		parent::__construct();

		//Get Table Names from Config 
		$this->_table_users 				= 	$this->config->item('table_users');
		$this->_login_userid			=	$this->session->userdata('userid');
	}
	function get_all_listmanagement($list_type,$list_parent_id=0,$list_id=0){
		$this->db->select('*');
		$this->db->from('ah_listmanagement');						
		$this->db->where('list_type',$list_type);
		if($list_parent_id >0){$this->db->where('list_parent_id',$list_parent_id);}
		if($list_id>0){$this->db->where('list_id',$list_id);}
		$this->db->where("delete_record",'0');
		$query = $this->db->get(); 
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	function savestore($data,$table,$feild)
	{
		$serviceid = $data[$feild];
		if($serviceid	!=	'' or $serviceid	> 0 )
		{
			$this->db->where($feild,$serviceid);
			$this->db->update($table,json_encode($data),$this->_login_userid,$data);
			//echo $this->db->last_query();
			return $serviceid;
		}
		else
		{
			$this->db->insert($table,$data,json_encode($data),$this->_login_userid);
			//echo $this->db->last_query();
			return $this->db->insert_id();
		}
	}
	function delete($serviceid,$field,$table)
	{
			$json_data	=	json_encode(array('record'	=>	'delete',$field	=>	$serviceid));
		$data		=	array('delete_record'=>'1');
		
		$this->db->where($field,$serviceid);

		$this->db->update($table,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
			
	}
	
	function getdata($serviceid,$table,$column){
		$this->db->select('*');
		$this->db->from($table);						
		$this->db->where($column,$serviceid);
		$query = $this->db->get(); 
		//echo $this->db->last_query();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	function getdmessagedata($serviceid,$table,$field){
		$this->db->select('*');
		$this->db->from($table);						
		$this->db->where($field,$serviceid);
		$this->db->where("delete_record","0");
		//$this->db->order_by('servcemsgid','DESC');
		$query = $this->db->get(); 
		return $query->result();
	}
	function savemessage($data,$table)
	{
		$this->db->insert($table,$data,json_encode($data),$this->_login_userid);
	}
	 public function get_all_items($bd_id,$bds_categoryId)
	 {
		 $query_string	=	NULL;
		 
		$query	=	$this->db->query("SELECT * FROM ah_budget_sub WHERE bd_id='".$bd_id."' AND  bds_categoryId='".$bds_categoryId."'");
		//echo $this->db->last_query();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	 }
}

?>
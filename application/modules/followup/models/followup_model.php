<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Followup_model extends CI_Model {

	

	/*

	* Properties

	*/

	private $_table_branches;

//----------------------------------------------------------------------

    

	/*

	* Constructor

	*/

	

	function __construct()

    {

        parent::__construct();

		

		//Load Table Names from Config

		$this->_table_branches 			=  $this->config->item('table_branches');
		$this->bank = $this->load->database('bank',TRUE);

    }

	

//----------------------------------------------------------------------

	/*

	* Insert User Record

	* @param array $data

	* return True

	*/

	function add_branche($data)

	{

		$this->db->insert($this->_table_branches,$data);

		

		return TRUE;

	}
	
	public function follow_list_new($branchid)
	{
		 $bank_data	= $this->follow->get_bank_data();
		 $regno = $bank_data->regno;
		 $civilno = $bank_data->civilno;
		 if($regno!='' && $civilno!='') {
		 $this->load->model('inquiries/inquiries_model', 'durar');
		 foreach($this->follow->getCompleteFollowData($regno,$civilno,$branchid) as $fup)
		 {
			    //echo '<pre>';print_r($bank_data);exit();
				if($fup->zyara_type=='phone')
				{	$zyratype = 'الهاتف'; }
				else
				{	$zyratype = 'الميدانية'; }
				
				$evoQuery = $this->db->query("SELECT totalrating FROM evaluate_project WHERE applicant_id='".$fup->applicant_id."' ORDER BY evaluate_id DESC LIMIT 1");
				if($evoQuery->num_rows() > 0)
				{
					foreach($evoQuery->result() as $evo)
					{					
						$evo_total = $evo->totalrating/30;
					}
					
				}
				else
				{	$evo_total = 0;	}
				
				if($evo_total >= 0 && $evo_total <= 0.5)
				{
					$mess = 'غير مناسب تماما،غير متوفر،غير لائق تماما ( '.$evo_total.' )';					
				}			
				else if($evo_total > 0.5 && $evo_total <= 2.4)
				{
					$mess = 'غير كاف ( '.$evo_total.' )';
				}
				else if($evo_total > 2.5 && $evo_total < 3)
				{
					$mess = 'متوسط ( '.$evo_total.' )';
				}
				else if($evo_total >= 3 && $evo_total <= 3.5)
				{
					$mess = 'فوق المتوسط ( '.$evo_total.' )';
				}
				else if($evo_total > 3.6 && $evo_total < 4)
				{
					$mess = 'جيد ( '.$evo_total.' )';
				}
				else if($evo_total >= 4 && $evo_total <= 4.5)
				{
					$mess = 'جيد جدا ( '.$evo_total.' )';
				}
				else if($evo_total > 4.5)
				{
					$mess = 'ممتاز ( '.$evo_total.' )';
				}
				//STEP NUMBER
				$steps = get_lable('step-'.$fup->form_step);			
				$fTotal = getCountsById('financial_returns',$fup->applicant_id);
				$a1= $fTotal->total;
								
				if(check_other_permission(49,'u')==1)
				{	$actions .=' <a href="'.base_url().'followup/requestfollowup/'.$fup->applicant_id.'" id="'.$fup->applicant_id.'"><i style="color:#aece4e;" class="icon-plus-sign"></i></a> ';	}
				
				if(check_other_permission(49,'d')==1)
				{	$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$fup->applicant_id.'" data-url="'.base_url().'inquiries/delete_applicant/'.$fup->applicant_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a> ';	}
				
				$actions .=' <a href="'.base_url().'followup/viewfollowuplist/'.$fup->applicant_id.'">('.$a1.')</a> ';
                $actions .=' <a href="#" data-url="'.base_url().'inquiries/smsmodal/'.$fup->applicant_id.'/tasgeel" onclick="open_dialog_sms(this);"><i style="color:#9C0;" class="icon-comment-alt"></i></a>';
				
				$name = '<a href="'.base_url().'followup/requestfollowup/'.$fup->applicant_id.'" id="'.$fup->applicant_id.'">'.$fup->applicant_first_name.' '.$fup->applicant_middle_name.' '.$fup->applicant_last_name.' '.$fup->applicant_sur_name.'</a>';
				$phone = $this->durar->applicant_phone_number($fup->applicant_id);
				$phoneNumbers = rander_html($phone,'phone');
				
				 $arr[] = array(
				 "DT_RowId"=>$fup->applicant_id.'_durar_lm',
                "رقم" => applicant_number($fup->applicant_id),
                "اسم" => $name,
				"نوع الزيارة" => $zyratype,
				"appid" => $fup->applicant_id,
				"الرقم المدني" =>$fup->appliant_id_number,  
				"رقم الهاتف" => $phoneNumbers,
                "طريقة إسناد الأعداد" =>arabic_date($mess),                             
                "date" =>$steps['ar'],
				"الإجراءات" =>$actions);
				unset($actions,$mess,$total,$zyratype);
		}
		 }
		//$ex['data'] = $arr;
			return json_encode($arr);
	}
	
	function get_applicant_partners($applicant_id){
				
		$this->db->where("applicant_id",$applicant_id);
		$applicant_partners = $this->db->get('applicant_partners');
		if($applicant_partners->num_rows() > 0){
			$arr =  $applicant_partners->result();
		}
		return $arr;
	}

//----------------------------------------------------------------------

	/*

	* Get Data for Listing for Store

	*/
	
	function checkData($table,$id){
		//			$this->db->insert('temp_main',array('userid'=>$userid));

		$this->db->where('applicant_id',$id);

		$query = $this->db->get($table);

		

		// Check if Result is Greater Than Zero

		if($query->num_rows() > 0){
			return true;

		}
		else{
			return false;
		}
	}
	
	function getReschdual($status='0'){
		$sql= "SELECT 
			  a.*,
			  r.* 
			FROM
			  `applicants` AS a 
			  INNER JOIN `rescheduling` AS r 
				ON r.`applicant_id` = a.`applicant_id` 
			WHERE r.`is_approved` = '".$status."' ";
			 $q = $this->db->query($sql);
			return $r =  $q->result();	
	}
	
	function getDataArray($flag,$table,$data){
		if(!empty($data) && $data !=""){
			foreach($data as $i=>$d){
				$this->db->where($i,$d);
			}
		}
		
		$query = $this->db->get($table);

		if($query->num_rows() > 0){
			
			if($flag){
				return $query->row_array();
			}
			else{
					return $query->result_array();
			}
		}
		else{
			return false;
	
		}
	}
	function getdataBytable($table,$id){
			$this->db->select('*,'.$table.'.created as visit');
			$this->db->where('applicant_id',$id);
			$this->db->join('admin_users',"id=user_id");
			$query = $this->db->get($table);

		

		// Check if Result is Greater Than Zero

		if($query->num_rows() > 0){
			return $query->result();

		}
		else{
			return false;
		}
	}
	//SELECT COUNT(*)  AS total FROM `about_proejct_details`
	function getAllBankData(){
		$sql = "SELECT * FROM `bank_loan_list`";
		$q = $this->bank->query($sql);
				if($q->num_rows()>0){
					return $result = $q->result();
					//echo "<pre>";
				  //print_r($result);
				}
				else
				{
				  return FALSE;
				}
	}
	
	function followup_data_list_table_new($applicantid)
	{
		$q = $this->db->query("SELECT `monthly_financial`.`created`,`admin_users`.`id`,`admin_users`.`firstname`,`admin_users`.`lastname`,`monthly_financial`.`month`,`monthly_financial`.`total_income`,`monthly_financial`.`total_expence`,`monthly_financial`.`applicant_id` FROM `monthly_financial`  INNER JOIN `admin_users`  ON (`monthly_financial`.`user_id` = `admin_users`.`id`) WHERE `monthly_financial`.`applicant_id`='".$applicantid."' ORDER BY `monthly_financial`.`month` ASC;");
		$vx = 0;
		foreach($q->result() as $qdata)
		{
			if(check_other_permission(74,'u')==1)
			{	$vo = strtotime($qdata->created);
				$actions .=' <a href="'.base_url().'followup/editVisit/'.$qdata->applicant_id.'/'.$vo.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';	}
				$actions .=' <a href="#1" id="'.$qdata->applicant_id.'" onClick="view_follw_data(this.id,\''.$vo.'\');"><i style="color:#3699d2;" class="icon-reorder"></i></a>';
			
				$evoQuery = $this->db->query("SELECT totalrating FROM evaluate_project WHERE applicant_id='".$qdata->applicant_id."' AND DATE(created)=DATE('".date('Y-m-d',strtotime($qdata->created))."') ORDER BY evaluate_id DESC LIMIT 1");
				if($evoQuery->num_rows() > 0)
				{
					foreach($evoQuery->result() as $evo)
					{					
						$evo_total = $evo->totalrating/30;
					}
					
				}
				else
				{	$evo_total = 0;	}
				
				if($evo_total >= 0 && $evo_total <= 0.5)
				{
					$mess = 'غير مناسب تماما،غير متوفر،غير لائق تماما ( '.$evo_total.' )';					
				}			
				else if($evo_total > 0.5 && $evo_total <= 2.4)
				{
					$mess = 'غير كاف ( '.$evo_total.' )';
				}
				else if($evo_total > 2.5 && $evo_total < 3)
				{
					$mess = 'متوسط ( '.$evo_total.' )';
				}
				else if($evo_total >= 3 && $evo_total <= 3.5)
				{
					$mess = 'فوق المتوسط ( '.$evo_total.' )';
				}
				else if($evo_total > 3.6 && $evo_total < 4)
				{
					$mess = 'جيد ( '.$evo_total.' )';
				}
				else if($evo_total >= 4 && $evo_total <= 4.5)
				{
					$mess = 'جيد جدا ( '.$evo_total.' )';
				}
				else if($evo_total > 4.5)
				{
					$mess = 'ممتاز ( '.$evo_total.' )';
				}
			
			$vx++;
			$monthtotal += $qdata->total_income;
			$monthly_avg = $monthtotal/$vx;
			$yearly_avg = number_format($monthtotal/12,2);
			
			$arr[] = array(
				'visit' => arabic_date($vx),
				'visit_eng' => $vx,
				'visit_person' => '<a href="'.base_url().'inquiries/userprofile/'.$qdata->id.'">'.$qdata->firstname.' '.$qdata->lastname.'</a>',
				'visit_month' => show_date($qdata->month,5),
				'visit_total' => arabic_date($qdata->total_income),
				'visit_grand_total' => arabic_date($monthtotal),
				'a_total' => arabic_date($monthly_avg),
				'b_total' => arabic_date($yearly_avg),
				'visit_award' => $mess,
				'visit_total_expence' => arabic_date($qdata->total_expence),
				'visit_applicant' => $qdata->applicant_id,
				'visit_action' => $actions);
				unset($actions);
		}
		return $arr;
	}
	
	function getBankAdmin($id){
		$sql = "SELECT au.`bank_branch_id` FROM `admin_users` AS au WHERE au.`id` = '".$id."'";
		$q = $this->db->query($sql);
				if($q->num_rows()>0){
					 $result = $q->row();
					 $bId = $result->bank_branch_id;
		$sql = "SELECT GROUP_CONCAT(wilaya_id) AS wilaya FROM `bank_branches_wilaya` AS bbw WHERE  bbw.`branch_id` = '".$bId."'";	 	
		$q = $this->db->query($sql);
				if($q->num_rows()>0){
					 $result = $q->row();
				}
				else{
					return false;
				}
					//echo "<pre>";
				  //print_r($result);
				}
				else
				{
				  return FALSE;
				}
	}	
	function getdataBypk($table,$pk,$id){
			$this->db->select('*,'.$table.'.created as visit');
			$this->db->where($pk,$id);
			$this->db->join('admin_users',"id=user_id");
			$query = $this->db->get($table);

		// Check if Result is Greater Than Zero

		if($query->num_rows() > 0){
			return $query->result();

		}
		else{
			return false;
		}
	}	
	function getrejectList($id){
		$this->db->select('loan_id');
		$this->db->where('applicant_id',$id);
		$query = $this->db->get('bank_response');
		// Check if Result is Greater Than Zero

		if($query->num_rows() > 0){
			return $query->row();

		}
		else{
			return false;
		}
	}
	function getdataBypkOrder($table,$pk,$id,$date){
			
		$sql = "select *,".$table.". created as visit from ".$table." where applicant_id =".$id;
		
		
		if($date!='')
		{	$sql .=" and Date(created) ='".$date."' "; }
		if($table=='monthly_financial'){ 
		  $sql .= " ORDER BY month_financial DESC LIMIT 1"; 
		}
		else{
			$sql.="limit 1";
		}
		 	$qx = $this->db->query($sql);
			if($qx->num_rows() > 0){
				return $qx->result();
			}
		/*elseif($table=='monthly_financial')
		{   $sql .= " ORDER BY month_financial DESC LIMIT 1"; }
		
			$qx = $this->db->query($sql);
			if($qx->num_rows() > 0){
			return $qx->result();
		}*/
		else{
			return false;
		}
	}	
	function get_followup_model($id){
			
		$allIncome = $this->db->query("SELECT SUM(total_income) as totalincome from monthly_financial where applicant_id ='".$id."'");
		foreach($allIncome->result() as $fullresult)
		{	$arr['total_income'] = $fullresult->totalincome; }
		
		$monthlyIncom = $this->db->query("SELECT SUM(total_income) as totalincome,`month` from monthly_financial where applicant_id ='".$id."' ORDER BY month_financial DESC LIMIT 1");
		foreach($monthlyIncom->result() as $mrsult)
		{	$arr['monthly_income'] = $mrsult->totalincome; 
			$arr['month'] = $mrsult->month;
		}		
		
		$totalMonth = $this->db->query("SELECT applicant_id from monthly_financial where applicant_id ='".$id."'");
		$arr['month_count'] = $totalMonth->num_rows();
		
		$arr['monthly'] = $arr['monthly_income']/$arr['month_count'];
		
		$arr['yearly'] = number_format($arr['total_income']/12,2);
		
		return $arr;
	}	
	
	function get_followup_model_single($id,$limit){
			
		$allIncome = $this->db->query("SELECT total_income from monthly_financial where applicant_id ='".$id."' ORDER BY `month` ASC LIMIT ".$limit);
		foreach($allIncome->result() as $fullresult)
		{	
			$currentMonth = $fullresult->total_income;	
			
		
		}
		
		$monthlyIncom = $this->db->query("SELECT SUM(total_income) as totalincome,`month` from monthly_financial where applicant_id ='".$id."' ORDER BY month_financial DESC LIMIT ".$limit);
		foreach($monthlyIncom->result() as $mrsult)
		{	$arr['monthly_income'] = $mrsult->totalincome; 
			$arr['month'] = $mrsult->month;
		}		
		
		$totalMonth = $this->db->query("SELECT applicant_id from monthly_financial where applicant_id ='".$id."'");
		$arr['month_count'] = $totalMonth->num_rows();
		
		$arr['monthly'] = $arr['monthly_income']/1;
		
		$arr['yearly'] = $arr['total_income']/$arr['month_count'];
		
		return $arr;
	}
	
	function getdataBypkTypeOrder($table,$pk,$id,$typekey,$typeid,$date){
			
		 $sql = "select *,".$table.". created as visit from ".$table." where applicant_id =".$id." and Date(created) ='".$date."' and ".$typekey." = '".$typeid."' limit 1  ";
		$qx = $this->db->query($sql);
			//$qx->row_array();	
		
		// Check if Result is Greater Than Zero

		if($qx->num_rows() > 0){
			return $qx->result();
		}
		else{
			return false;
		}
	}
	function getdataByType($table,$type_name,$type,$pk,$id){
			$this->db->select('*,'.$table.'.created as visit');
			$this->db->where($pk,$id);
			$this->db->where($type_name,$type);
			$this->db->join('admin_users',"id=user_id");
			$query = $this->db->get($table);
		// Check if Result is Greater Than Zero

		if($query->num_rows() > 0){
			return $query->result();

		}
		else{
			return false;
		}
	}
	
	function getdataBytable2($table,$id,$type_column,$type){
		
				$this->db->select('*,'.$table.'.created as visit');
			$this->db->where('applicant_id',$id);
			$this->db->where($type_column,$type);
			$this->db->join('admin_users',"id=user_id");
		$query = $this->db->get($table);

		

		// Check if Result is Greater Than Zero

		if($query->num_rows() > 0){
			return $query->result();

		}
		else{
			return false;
		}
	}
	
	function getdataByTypeTable($table,$id,$type){
		$this->db->where('applicant_id',$id);
		$this->db->where('anoted_type',$type);
		$query = $this->db->get($table);

		

		// Check if Result is Greater Than Zero

		if($query->num_rows() > 0){
			return $query->result();

		}
		else{
			return false;
		}
	}
	function delete_recordById($table,$id)
	{
		$this->db->where("applicant_id",$id);
		$this->db->delete($table);
		return true; 
	}

	function get_single_branche($branch_id)

	{

		$this->db->where('branch_id',$branch_id);



		$query = $this->db->get($this->_table_branches);

		

		// Check if Result is Greater Than Zero

		if($query->num_rows() > 0)

		{

			return $query->row();

		}

	}

//----------------------------------------------------------------------

	/*

	* Get Data for Listing for Store

	*/

	function get_all_branches()

	{

		$query = $this->db->get($this->_table_branches);

		

		// Check if Result is Greater Than Zero

		if($query->num_rows() > 0)

		{

			return $query->result();

		}

	}
	
	

	

//----------------------------------------------------------------------



	/*

	* Delete

	*/

	function delete($branch_id)

	{

		$this->db->where("branch_id",$branch_id);

		$this->db->delete($this->_table_branches);

		

		return true; 

	}

//------------------------------------------------------------------------



    /**

     * 

     * Insert User Data for Registration

     * @param array $data

     * return integer

     */

	function update_branch($branch_id,$data)

	{



		$this->db->where('branch_id', $branch_id);

		$this->db->update($this->_table_branches, $data);

		

		//print_r($this->db->last_query());

		

		return TRUE;

	}

//----------------------------------------------------------------------

	/*

	* Get Data for Listing for Store

	*/

	function get_province_name($provinceid)

	{

		$this->db->where('ID',$provinceid);



		$query = $this->db->get('election_reigons');

		

		// Check if Result is Greater Than Zero

		if($query->num_rows() > 0)

		{

			return $query->row()->REIGONNAME;

		}

	}

//----------------------------------------------------------------------

	/*

	* Get Data for Listing for Store

	*/

	function get_wilayats_name($wilayatsid)

	{

		$this->db->where('WILAYATID',$wilayatsid);



		$query = $this->db->get('election_wilayats');

		

		// Check if Result is Greater Than Zero

		if($query->num_rows() > 0)

		{

			return $query->row()->WILAYATNAME;

		}

	}
	
	function get_bank_data(){
				
				
				//$this->another->select('somecol');
			//	$q = $this->another->get('sometable');
				
				$sql = "SELECT GROUP_CONCAT(bll.`CIVIL_ID`) AS civilno,GROUP_CONCAT(bll.`COMM_REG_NO`) AS regno FROM `bank_loan_list` AS bll   
WHERE bll.`REMAIN_DISB` = '0'";
				$q = $this->bank->query($sql);
				if($q->num_rows()>0){
					return $result = $q->row();
					//echo "<pre>";
				  //print_r($result);
				}
				else
				{
				  return FALSE;
				}
				
				
				
	
	}
	function check_record($table,$where){
		/*pd.`ILOM_SEQUENCE` = '31469' 
  AND pd.`DISB_DATE` = '12-16-14' 
  AND pd.`DISB_AMT` = '4800.000' 
  AND pd.DISB_DATE = '12-16-14' */
		$sql = "SELECT * FROM ".$table." Where ".$where."";
		$q = $this->bank->query($sql);
				if($q->num_rows()>0){
					return $row = $q->row();
					//return $row->list_id;
			}
			else{
				  return FALSE;
			}
	}
	
	function check_data($lm,$reg){
		  $sql = "SELECT * FROM `bank_loan_list` AS bl WHERE bl.`ILOM_SEQUENCE` = '".$lm."'";
		$q = $this->bank->query($sql);
				if($q->num_rows()>0){
					$row = $q->row();
					return $row->list_id;
			}
				else
				{
				  return FALSE;
				}

	}
	
	function check_sequance($lm,$table){
		  $sql = "SELECT * FROM ".$table."  AS bq WHERE bq.`ILOM_SEQUENCE` = '".$lm."' ";
		$q = $this->bank->query($sql);
				if($q->num_rows()>0){
					$row = $q->row();
					return $row->id;
			}
				else
				{
				  return FALSE;
				}

	}
	
	function payment_schduale(){
		
	}
	
	
	function updateRecord($data,$table,$key,$record_id){			
		$this->bank->where($key, $record_id);
		return $this->bank->update($table, $data);
	}
	
	function updateRecordDb($data,$table,$key,$record_id){			
		$this->db->where($key, $record_id);
		return $this->db->update($table, $data);
	}
	
	function insertRecord($data,$table){			
		$this->bank->insert($table,$data);
	}
	function insertRecordNew($data,$table){			
		$this->db->insert($table,$data);
		return $id = $this->db->insert_id();	
	}
	
	function getLoanIdNumber($val,$column){
		  $sql = "SELECT * FROM bank_loan_list  AS bq WHERE ".$column." = '".$val."' ";
		$q = $this->bank->query($sql);
				if($q->num_rows()>0){
					return $row = $q->row();
					//return $row->ILOM_SEQUENCE;
			}
				else
				{
				  return FALSE;
				}
	}
	function getBankDataByLoanId($table,$id){
		$sql = "SELECT * FROM ".$table."  AS bq WHERE bq.`ILOM_SEQUENCE` = '".$id."' ";
		$q = $this->bank->query($sql);
				if($q->num_rows()>0){
					return $row = $q->result();
					//return $row->ILOM_SEQUENCE;
			}
				else
				{
				  return FALSE;
				}
		
	}
	
	function getCompleteFollowData($reg,$civil,$branchid)
	{
		$sql = "SELECT a.*, ep.zyara_type FROM `applicants` AS a LEFT JOIN `evaluate_project` AS ep ON ep.`applicant_id` = a.`applicant_id` INNER JOIN `admin_users` AS au ON a.`applicant_id` = au.`id` WHERE a.`applicant_cr_number` IN (".$reg.") || a.appliant_id_number IN('".$civil."') ";
		if($branchid!='' && $branchid!='0')
		{	$sql .= " AND au.`branch_id`='".$branchid.""; }
			$sql .= " GROUP BY a.`applicant_id` ";
			$q = $this->db->query($sql);
			if($q->num_rows()>0){	return $result = $q->result();	}
	}

//----------------------------------------------------------------------

}



?>
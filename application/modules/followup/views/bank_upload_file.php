<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <form action="<?php echo base_url();?>followup/add_excel" method="post" id="validate_form" name="validate_form" autocomplete="off" enctype="multipart/form-data">
          <div class="row" style="background:#FFF !important;">
            <div id="drag0" class="form-group col-md-6 add_excel" >
              <div class="browser" style="padding: 14px; font-size: 12px;">
                <label> <span>تحميل</span>
                  <input type="file" name="files[]" multiple title='تحميل'>
                  <input type="hidden" name="attachment" id="attachment" />
                </label>
              </div>
              <div id="show-image"></div>
            </div>
            <div class="form-group col-md-6 panel-body demo-panel-files" id='demo-files0'>
              <span class="demo-note"></span>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
$(function(){
						$('.add_excel').dmUploader({
					url: config.BASE_URL+'followup/add_excel',
					dataType: 'json',
					allowedTypes: '*',					
					onInit: function()
					{
					 idd = $(this).attr('id');
					 id_index = idd.substring(4,5);
					},
					onBeforeUpload: function(id){},
					onNewFile: function(id, file){
						//alert(id);
						idd = $(this).attr('id');
						id_index = idd.substring(4,5);
					 	alert('asdas');	
						demoFile = '#demo-files'+id_index;
						//id = idd.substring(4,5);
						//console.log(idd);
						id = idd.substring(4,5);
						//alert(idd);
					  $.danidemo.addFile(demoFile, id, file);
					},
					onComplete: function(){
						//alert('complete');	
				  
					 // $.danidemo.addLog('#demo-debug', 'default', 'All pending tranfers completed');
					},
					onUploadProgress: function(id, percent){
					  var percentStr = percent + '%';

					  $.danidemo.updateFileProgress(id, percentStr);
					  
					},
					onUploadSuccess: function(id, data){
						id = idd.substring(4,5);
						$.danidemo.updateFileProgress(id, '50%');
						ff = data;	
						//$("#document_file"+id).val(ff.filename);
						//$("#attachment").val(ff.filename);
						
						if(ff.status == 'ok'){
							$.danidemo.updateFileProgress(id, '100%');
							show_notification('تم تحديث البيانات بنجاح');
						}
						else{
							//alert('else');	
							//show_notification_error_end(ff.status);
						}
						 
					 //console.log(JSON.stringify(data));
					},
					onUploadError: function(id, message){
					  $.danidemo.updateFileStatus(id, 'error', message);

					  show_notification_error_end('Failed to Upload file #' + id + ': ' + message);
					},
					onFileTypeError: function(file){
					  show_notification_error_end('File \'' + file.name + '\' cannot be added: must be an image');
					},
					onFileSizeError: function(file){
					  show_notification_error_end('File \'' + file.name + '\' cannot be added: size excess limit');
					},
					/*onFileExtError: function(file){
					  $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' has a Not Allowed Extension');
					},*/
					onFallbackMode: function(message){
					  show_notification_error_end('Browser not supported(do something else here!): ' + message);
					}
				  });
		});		
</script>
</div>
</body>
</html>
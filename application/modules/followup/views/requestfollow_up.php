<!doctype html>
<?PHP
	$type_name = $this->inq->get_type_name($applicatn_info->applicant_marital_status);
	$privince =	$this->inq->get_province_name($applicatn_info->province);
	$wilayats =	$this->inq->get_wilayats_name($applicatn_info->walaya);
	$phone = $this->inq->applicant_phone_number($applicatn_info->applicant_id);
	$tab_1 = $this->inq->get_tab_data('applicant_qualification',$applicatn_info->applicant_id);
	$tab_2 = $this->inq->get_tab_data('applicant_professional_experience',$applicatn_info->applicant_id);
	$tab_3 = $this->inq->get_tab_data('applicant_businessrecord',$applicatn_info->applicant_id);
	$professional =	$this->inq->getRequestInfo($applicatn_info->applicant_id);	
	$applicant = $applicant_data['applicants'];
	$project = $applicant_data['applicant_project'][0];	
	$professional = $applicant_data['applicant_professional_experience'];
	$loan = $applicant_data['applicant_loans'][0];
	$evo  = $applicant_data['project_evolution'][0];
	$phones = $applicant_data['applicant_phones'];
	$comitte = $applicant_data['comitte_decision'][0];
	$fullname = $applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name;
	foreach($phones as $p)
	{	$ar[] = '986'.$p->applicant_phone;	}
		$applicantphone = implode('<br>',$ar);
		
	$steps = get_lable('step-'.$applicant->form_step);
	$youm = array('day'=>'يوم','week'=>'أسبوع','month'=>'شهر','year'=>'عام');
?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <form id="validate_form" name="validate_form" method="post" action="<?PHP echo base_url().'followup/add_follow_up' ?>" autocomplete="off">
        <input type="hidden" name="form_step" id="form_step" value="5" />
        <input type="hidden" name="applicant_id" id="applicant_id" value="<?php echo $applicatn_info->applicant_id;?>" />
        <div class="col-md-12">
          <div class="panel panel-default panel-block">
            <div class="list-group">
              <div class="list-group-item" id="input-fields">
                <div class="form-group">
                  <h4 class="section-title preface-title text-warning"> <?PHP echo $fullname; ?> <span class="label label-default">رقم التسجيل <?php echo arabic_date(applicant_number($applicant_id)); ?></span></h4>
                </div>
                <div class="form-group">
                  <div class="col-md-2">
                            <label class="text-warning">عن طريق الزيارة الميدانية:</label>
                            <label><input type="radio" id="zyara_type" name="zyara_type" value="foot"></label>
                          </div>
                          <div class="col-md-2">
                            <label class="text-warning">عن طريق الهاتف:</label>
                            <label><input type="radio" id="zyara_type" name="zyara_type" value="phone"></label>
                          </div>
                          <br clear="all">
                </div>
                <!--------Starting Tabs--------->
                <div class="row">
                  <div class="col-md-12">
                    <ul class="nav nav-tabs panel panel-default panel-block">
                      <li class="active"><a href="#tabsdemo-1" data-toggle="tab">العنوان الشخصي</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-2" data-toggle="tab">الاستمار والمردود المالي</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-3" data-toggle="tab">أوجه الدعم المقدمة من الجهات الاخرى</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-4" data-toggle="tab">بطاقة تقييم مشروع</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-5" data-toggle="tab">تاريخ التقييم</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-6" data-toggle="tab">رأي المتابـــع</a></li>
                    </ul>
                    <div class="tab-content panel panel-default panel-block" style="background-color: #fff; padding: 6px 8px;">
                      <div class="tab-pane list-group active" id="tabsdemo-1"> 
                        <!------------------------------->
                        <div class="row">
                          <div class="col-md-3">
                            <label class="text-warning">طبيعة المراجعين:</label>
                            <label><strong class="st">
                              <?php  echo $applicatn_info->applicant_type;?>
                              </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">النوع:</label>
                            <label><strong class="st">
                              <?php  echo $applicatn_info->applicant_gender;?>
                              </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم الرقم المدني:</label>
                            <label><strong class="st">
                              <?php  echo is_set($applicatn_info->appliant_id_number);?>
                              </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم الهاتف:</label>
                            <label><strong class="st"><?PHP echo rander_html($phone,'phone'); ?></strong></label>
                          </div>
                        </div>
                        <?php if(!empty($professional['applicant_partners'])) { 
                                    foreach($professional['applicant_partners'] as $partners) { ?>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning">مشترك</h4>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">اسم مقدم الطلب</label>
                            <label><strong class="st"><?php echo $partners->partner_first_name.' '.$partners->partner_middle_name.' '.$partners->partner_last_name.' '.$partners->partner_sur_name;?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">النوع</label>
                            <label><strong class="st">
                              <?php  echo $partners->partner_gender;?>
                              </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم الرقم المدني</label>
                            <label><strong class="st">
                              <?php  echo $partners->partner_id_number;?>
                              </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم الهاتف:</label>
                            <label><strong class="st"><?PHP echo rander_html($phone1,'phone'); ?></strong></label>
                          </div>
                        </div>
                        <?PHP }	} ?>
                        <div class="row">
                          <div class="col-md-3">
                            <label class="text-warning">المرحلة:</label>
                            <label><strong class="st"><?php echo $steps['ar']; ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم بطاقة سجل القوى العاملة:</label>
                            <label><strong class="st"> <?php echo is_set($applicatn_info->applicant_cr_number); ?> </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم الهاتف:</label>
                            <label><strong class="st"><?PHP echo rander_html($phone,'phone'); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">تاريخ الميلاد:</label>
                            <label><strong class="st"> <?php echo show_date($applicatn_info->applicant_date_birth,4); ?> </strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <label class="text-warning">الوضع الحالي:</label>
                            <label><strong class="st"><?php echo $this->inq->get_type_name($applicatn_info->applicant_job_staus); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">الحالة الاجتماعية:</label>
                            <label><strong class="st"> <?php echo $this->inq->get_type_name($applicatn_info->applicant_marital_status); ?> </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">فئة الظمان الإجتماعي:</label>
                            <label><strong class="st">
                              <?php if($applicatn_info->option1 ==	'Y') { echo( is_set($applicatn_info->option_txt)); } else { echo 'لا'; } ?>
                              </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">فئة من ذوي الإعاقة:</label>
                            <label><strong class="st">
                              <?php if($applicatn_info->option2 ==	'Y') { echo 'نعم'; } else { echo 'لا'; } ?>
                              </strong></label>
                          </div>
                        </div>
                        <!-------------------------------------> 
                        <br>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">العنوان الشخصي</h4>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">محافظة:</label>
                            <label><strong class="st"><?php echo is_set($privince); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">الولاية:</label>
                            <label><strong class="st"><?php echo is_set($wilayats); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">القرية:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->village); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">السكة:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->way); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <label class="text-warning">المنزل/المبني:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->home); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">الشقة:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->deparment); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">ص.ب / ر.ب:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->zipcode) / is_set($applicatn_info->postalcode); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم سجل القوى العاملة:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->applicant_cr_number); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <label class="text-warning">الهاتف الثابت:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->linephone); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">الفاكس:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->fax); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">البريد الإلكتروني:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->email); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">هاتف نقال أحد الأقارب:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->refrence_number); ?></strong></label>
                          </div>
                        </div>
                        <!-------------------------------------> 
                        <br>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">المؤهلات</h4>
                          </div>
                          <div class="form-group">
                            <h5 class="section-title" style="margin-right: 14px;">1/ المستوى الدراسي</h5>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">المؤهل:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_qualification); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">التخصص:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_specialization); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">الجهة:</label>
                            <label><strong class="st"><?php echo is_set($this->inq->get_type_name($tab_1->applicant_institute)); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">سنة التخرج:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->application_institute_year); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group">
                            <h5 class="section-title" style="margin-right: 14px;">2/ التدريب المهني</h5>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">مركز التدريب:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_trainningcenter); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">التخصص:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_specializations); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">مدة التدريب (بالأشهر):</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_training_month); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">شهادة التدريب المهني المتحصل عليها:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_vtco); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <label class="text-warning">سنة الحصول على الشهادة:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_ytotc); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">دورات تدريبية ميدانية أخرى:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_other_trainning); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">مدة التدريب (بالأشهر):</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_other_specializations); ?></strong></label>
                          </div>
                          <div class="col-md-3"> </div>
                        </div>
                        <!------------------------------------->
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">الخبرة المهنية</h4>
                          </div>
                          <div class="form-group">
                            <h5 class="section-title" style="margin-right: 14px;">الخبرة في نفس نشاط المشروع</h5>
                          </div>
                          <div class="col-md-12">
                            <table class="table table-bordered table-striped dataTable">
                              <thead>
                                <tr role="row">
                                  <th>تاريخ بداية المشروع</th>
                                  <th>اسم الجهة/المؤسسة/ المشروع الخاص</th>
                                  <th>نشاط الجهة/المؤسسة/ المشروع الخاص</th>
                                  <th>المهنة المزاولة بالجهة/المؤسسة/ المشروع الخاص</th>
                                  <th>عدد سنوات الخبرة</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php 	$counter =	'1';
                                            foreach($professional['applicant_professional_experience'] as $tab)
                                            {									
                                                if($counter <= '3')
                                                { ?>
                                <tr role="row">
                                  <td><?php echo $tab->option_one;?></td>
                                  <td><?php echo $tab->option_two;?></td>
                                  <td><?php echo $tab->option_three;?></td>
                                  <td><?php echo $tab->option_four;?></td>
                                  <td><?php echo $tab->option_five;?></td>
                                </tr>
                                <?php 		} 
                                                $counter++;
                                            } ?>
                              </tbody>
                            </table>
                          </div>
                          <br>
                          <div class="form-group">
                            <h5 class="section-title" style="margin-right: 14px;">الخبرة في أنشطة أخرى</h5>
                          </div>
                          <div class="col-md-12">
                            <table class="table table-bordered table-striped dataTable">
                              <thead>
                                <tr role="row">
                                  <th>تاريخ بداية المشروع</th>
                                  <th>اسم الجهة/المؤسسة/ المشروع الخاص</th>
                                  <th>نشاط الجهة/المؤسسة/ المشروع الخاص</th>
                                  <th> المهنة المزاولة بالجهة/المؤسسة/ المشروع الخاص </th>
                                  <th>عدد سنوات الخبرة</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php 	$counter =	'1';
                                            foreach($professional['applicant_professional_experience'] as $tab)
                                            {									
                                                if($counter <= '3')
                                                { ?>
                                <tr role="row">
                                  <td><?php echo $tab->activities_one;?></td>
                                  <td><?php echo $tab->activities_two;?></td>
                                  <td><?php echo $tab->activities_three;?></td>
                                  <td><?php echo $tab->activities_four;?></td>
                                  <td><?php echo $tab->activities_five;?></td>
                                </tr>
                                <?php 		} 
                                                $counter++;
                                            } ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!------------------------------------->
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">السجلات التجارية الأخرى</h4>
                          </div>
                          <div class="form-group">
                            <h5 class="section-title" style="margin-right: 14px;">الخبرة في نفس نشاط المشروع</h5>
                          </div>
                          <div class="col-md-12">
                            <table class="table table-bordered table-striped dataTable">
                              <thead>
                                <tr role="row">
                                  <th>اسم السجل</th>
                                  <th>رقم السجل</th>
                                  <th>عدد القوى العاملة الوطنية</th>
                                  <th>عدد القوى العاملة الوافدة</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php 	$counter =	'1';
                                            foreach($professional['applicant_businessrecord'] as $tab)
                                            {									
                                                if($counter <= '3')
                                                { ?>
                                <tr role="row">
                                  <td><?php echo $tab->activity_name;?></td>
                                  <td><?php echo $tab->activity_registration_no;?></td>
                                  <td><?php echo $tab->activity_nationalmanpower;?></td>
                                  <td><?php echo $tab->activity_laborforce;?></td>
                                </tr>
                                <?php 		} 
                                                $counter++;
                                            } ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-------------------------------------> 
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-2"> 
                        <!------------------------------------->
                        <?php
                        $financial = array();
                        if(!empty($financial)){
                            foreach($financial as  $i=>$finance){
                                ?>
                        
                        <div class="row" id="first">
                          <i onClick="showMultiForms();" style="margin-right: 14px; cursor:pointer;" class="icon-plus-sign-alt icon_read"></i>
                          <div class="col-md-3 fcounter<?php echo $i; ?>">
                            <label class="text-warning">قيمة المشروع الحالية:</label>
                            <label>
                            <input type="text" name="present_value_project[]" id="present_value_project[]" value="<?php echo $finance->present_value_project; ?>" class="NumberInput form-control" />
                            <div  style="float: left; margin-right: 7px;">رع</div>
                            </label>
                          </div>
                          <div class="col-md-3 fcounter<?php echo $i; ?>">
                            <label class="text-warning">متوسط الايرادات الشهرية:</label>
                            <label>
                            <input type="text" name="average_monthly_revenue[]" id="average_monthly_revenue[]" value="<?php echo $finance->average_monthly_revenue; ?>" class="NumberInput  form-control"/>
                            <div  style="float: left; margin-right: 7px;">رع</div>
                            </label>
                          </div>
                          <div class="col-md-3 fcounter<?php echo $i; ?>">
                            <label class="text-warning">السنوية الايرادات متوسط:</label>
                            <label>
                            <input type="text" name="average_anual_revenue[]" id="average_anual_revenue[]" value="<?php echo $finance->average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            <div  style="float: left; margin-right: 7px;">رع</div>
                            </label>
                          </div>
                          <div class="col-md-3 fcounter<?php echo $i; ?>">
                            <label class="text-warning">الشهري الريح صافي متوسط:</label>
                            <label>
                            <input type="text" name="net_average_monthly_revenue[]" id="net_average_monthly_revenue[]" value="<?php echo $finance->average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            <div  style="float: left; margin-right: 7px;">رع</div>
                            </label>
                          </div>
                          <div class="col-md-3 fcounter<?php echo $i; ?>">
                            <label class="text-warning">السنوي الريح صافي متوسط:</label>
                            <label>
                            <input type="text" name="net_average_anual_revenue[]" id="net_average_anual_revenue[]"  value="<?php echo $finance->net_average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            <div  style="float: left; margin-right: 7px;">رع</div>
                            </label>
                          </div>
                        </div>
                        <?PHP } 
                        } else {  ?>
						<i onClick="showMultiForms();" style="margin-right: 14px; cursor:pointer;" class="icon-plus-sign-alt icon_read"></i>
                        <div class="row" id="first">
                          <div class="col-md-2">
                            <label class="text-warning">قيمة المشروع الحالية (رع)</label>
                            <label>
                              <input type="text" name="present_value_project[]" id="present_value_project[]" value="<?php echo $finance->present_value_project; ?>" class="NumberInput form-control" />
                            </label>
                          </div>
                          <div class="col-md-2">
                            <label class="text-warning">متوسط الايرادات الشهرية (رع)</label>
                            <label>
                              <input type="text" name="average_monthly_revenue[]" id="average_monthly_revenue[]" value="<?php echo $finance->average_monthly_revenue; ?>" class="NumberInput  form-control"/>
                            </label>
                          </div>
                          <div class="col-md-2">
                            <label class="text-warning">السنوية الايرادات متوسط (رع)</label>
                            <label>
                              <input type="text" name="average_anual_revenue[]" id="average_anual_revenue[]" value="<?php echo $finance->average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            </label>
                          </div>
                          <div class="col-md-2">
                            <label class="text-warning">الشهري الريح صافي متوسط (رع)</label>
                            <label>
                              <input type="text" name="net_average_monthly_revenue[]" id="net_average_monthly_revenue[]" value="<?php echo $finance->average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            </label>
                          </div>
                          <div class="col-md-2">
                            <label class="text-warning">السنوي الريح صافي متوسط (رع)</label>
                            <label>
                              <input type="text" name="net_average_anual_revenue[]" id="net_average_anual_revenue[]"  value="<?php echo $finance->net_average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            </label>
                          </div>
                        </div>
                        <?PHP } ?>
                        <!-------------------------------------> 
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-3">
                      
                        <?PHP $support =   array();
					  if(!empty($support))	{ 
					  	foreach($support as $i=>$sup)	{	
							$checked1 = "";
							$checked2 = "";
							if($sup->support_training == '1')
							{
								$checked1 ="checked";
								$display = "block";			}
							else
							{	$checked2 ="checked";
								$display = "none";			}
				 ?>
                        <div class="row" id="second">
                        <i onClick="showSecondMultiple();" style="cursor:pointer;" class="icon-plus-sign-alt icon_read"></i>
                          <div class="col-md-12">
                            <label class="text-warning">الدعم التدريبي:</label>
                            <label>نعم
                              <input type="radio" name="support_training[]" id="support_training<?php echo $i; ?>" value="1" <?php echo $checked1;  ?>  class="supporting_classs"/>
                              &nbsp;&nbsp; لا
                              <input type="radio" checked name="support_training[]" id="support_training<?php echo $i; ?>" value="0" <?php echo $checked2;  ?> class="supporting_classs"/>
                            </label>
                          </div>
                          <div class="col-md-12 yes_class_support support_training<?php echo $i; ?>"  style="display:<?php echo $display; ?>;">
                            <div class="col-md-2">
                            <label class="text-warning">مجال التدريب لصاحب المنشأة:</label>
                              <input class="form-control" placeholder="مجال التدريب لصاحب المنشأة" type="text"  name="training_owner_facility[]" id="training_owner_facility"  value="<?php echo $sup->training_owner_facility;  ?>"/>
                            </div>
                            <div class="col-md-2">
                            <label class="text-warning">جهة التدريب:</label>
                              <input class="form-control" placeholder="جهة التدريب" type="text"  name="training[]" id="training"  value="<?php echo $sup->training;  ?>"/>
                            </div>
                            <div class="col-md-2">
                            <label class="text-warning">المدة:</label>
                              <input id="duration[]" placeholder="المدة" name="duration[]" class="form-control" value="<?php echo $sup->duration; ?>" />
                            </div>
                            <div class="col-md-2">
                            <label class="text-warning">يوم:</label>
                              <select id="durationtype[]" name="durationtype[]"  class="form-control">
                                <?PHP foreach($youm as $y) { ?>
                                <option value="<?PHP echo $y; ?>" <?PHP if($sup->duration == $y) { ?>selected="selected" <?PHP } ?>><?PHP echo $y; ?></option>
                                <?PHP } ?>
                              </select>
                            </div>
                           <div class="col-md-2">
                                <label class="text-warning">قبل التأسيس:</label> <br>                                
                                <input type="checkbox" name="before_incoporation[]" <?PHP if($sup->before_incoporation==1) { ?> checked <?PHP } ?> id="before_incoporation1" value="1" class="supporting_classs"/>
                             
                              </div>
                              <div class="col-md-2">
                              <label class="text-warning">بعد التأسيس:</label> <br>                             
                                <input type="checkbox" name="durationtype[]" <?PHP if($sup->durationtype==1) { ?> checked <?PHP } ?> id="durationtype" value="1" class="supporting_classs"/>                             
                              </div>
                          </div>
                          <!----------------------------------> 
                          <!---------------------------------->
                          <?PHP
						$fchecked1 = "";
						$fchecked2 = "";
						if($sup->funding_support == '1')
						{
							$fchecked1 ="checked";
							$display = "block";
						}
						else
						{
							$fchecked2 ="checked";
							$display = "none";
						}
					?>
                          <div class="col-md-12">
                            <label class="text-warning">الدعم التمويلي:</label>
                            <label>نعم
                              <input type="radio" name="funding_support[]" id="funding_support<?php echo $i; ?>" value="1" class="supporting_classs" <?php echo $fchecked1; ?>/>
                              &nbsp;&nbsp; لا
                              <input type="radio" name="funding_support[]" id="funding_support<?php echo $i; ?>" value="0" class="supporting_classs" <?php echo $fchecked2; ?>/>
                            </label>
                          </div>
                          <div class="col-md-12 yes_class_funds funding_support<?php echo $i; ?>" style="display:<?php echo $display; ?>;">
                            <div class="col-md-2">
                            <label class="text-warning">مبلغ الدعم (رع):</label>
                              <input type="text" placeholder="مبلغ الدعم (رع)"  name="amount_support[]" id="amount_support" class="NumberInput form-control" value="<?php echo $sup->amount_support;  ?>"/>
                            </div>
                            <div class="col-md-2">
                            <label class="text-warning">مبلغ الدعم (رع):</label>
                              <input type="text" placeholder="جهة الدعم (رع)"  name="support_point[]" id="support_point" class="NumberInput form-control" value="<?php echo $sup->support_point;  ?>"/>
                            </div>
                            <div class="col-md-2">
                            <label class="text-warning">>نوع الدعم:</label>
                              <select name="support_type[]" id="<?php echo $i; ?>" class="others form-control">
                                <option value="">نوع الدعم</option>
                                <?PHP $aax = array('قرض'=>'قرض','منحة'=>'منحة','others'=>'اخرى يتم ذكرها');
							foreach($aax as $paax => $paaxvalue)
							{
								$html .= '<option value="'.$paaxvalue.'" ';
								if($paax==$sup->support_type)
								{
									$html .= ' selected="selected" ';
								}
								$html .= '>'.$paaxvalue.'</option>';
							}
								echo $html;				
						?>
                              </select>
                            </div>
                            <?php
							if($sup->support_type == 'others')
							{	$dislpay= "block"; 	}
							else
							{	$dislpay= "none";	}
						?>
                            <div class="col-md-2 yes_class_funds" id="others<?php echo $i; ?>" style="display:<?php echo $dislpay; ?>;">
                              <input  type="text" class="form-control" placeholder="أخرى (يتم ذكرها)" name="mention_others[]" id="mention_others" value="<?php echo $sup->mention_others;  ?>"/>
                            </div>
                          </div>
                          <!----------------------------------> 
                          <!---------------------------------->
                          <div class="col-md-12">
                            <label class="text-warning">وجه دعم أخرى: </label>
                            <label>نعم
                              <input type="radio" name="face_others_support[]" id="face_others_support0" value="1" class="supporting_classs"/>
                              &nbsp;&nbsp; لا
                              <input type="radio" checked  name="face_others_support[]" id="face_others_support0" value="0" class="supporting_classs"/>
                            </label>
                          </div>
                          <div class="col-md-12 yes_class_others face_others_support0"  style="display:none;">
                            <div class="col-md-2">
                              <input type="text" placeholder="اذكرها"  name="face_others_support_text[]" id="face_others_support_text" class="NumberInput form-control" value="<?php echo $sup->amount_support;  ?>"/>
                            </div>
                          </div>
                        </div>
                        <br>
                        <?PHP 		}
						} else { $i = 0; ?>
                        <div class="row" id="second">
                        <i onClick="showSecondMultiple();" style="cursor:pointer;" class="icon-plus-sign-alt icon_read"></i>
                          <div class="col-md-12">
                            <label class="text-warning">الدعم التدريبي:</label>
                            <label>نعم
                              <input type="radio" name="support_training[]" id="support_training<?php echo $i; ?>" value="1" <?php echo $checked1;  ?>  class="supporting_classs"/>
                              &nbsp;&nbsp; لا
                              <input type="radio" checked name="support_training[]" id="support_training<?php echo $i; ?>" value="0" <?php echo $checked2;  ?> class="supporting_classs"/>
                            </label>
                          </div>
                          <div class="row">
                            <div class="col-md-12 yes_class_support support_training<?php echo $i; ?>"  style="display:none;">
                              <div class="col-md-2">
                              <label class="text-warning">مجال التدريب لصاحب المنشأة:</label>
                                <input class="form-control" placeholder="مجال التدريب لصاحب المنشأة" type="text"  name="training_owner_facility[]" id="training_owner_facility"  value="<?php echo $sup->training_owner_facility;  ?>"/>
                              </div>
                              <div class="col-md-2">
                              <label class="text-warning">جهة التدريب:</label>
                                <input class="form-control" placeholder="جهة التدريب" type="text"  name="training[]" id="training"  value="<?php echo $sup->training;  ?>"/>
                              </div>
                              <div class="col-md-2">
                              <label class="text-warning">المدة:</label>
                                <input id="duration[]" placeholder="المدة" name="duration[]" class="form-control" value="<?php echo $sup->duration; ?>" />
                              </div>
                              <div class="col-md-2">
                              <label class="text-warning">يوم:</label>
                                <select id="durationtype[]" name="durationtype[]"  class="form-control">
                                  <?PHP foreach($youm as $y) { ?>
                                  <option value="<?PHP echo $y; ?>" <?PHP if($sup->duration == $y) { ?>selected="selected" <?PHP } ?>><?PHP echo $y; ?></option>
                                  <?PHP } ?>
                                </select>
                              </div>
                              <div class="col-md-2">
                                <label class="text-warning">قبل التأسيس:</label><br>                              
                                <input type="checkbox" name="before_incoporation[]" <?PHP if($sup->before_incoporation==1) { ?> checked <?PHP } ?> id="before_incoporation1" value="1" class="supporting_classs"/>
                             
                              </div>
                              <div class="col-md-2">
                              <label class="text-warning">بعد التأسيس:</label><br>                          
                                <input type="checkbox" name="durationtype[]" <?PHP if($sup->durationtype==1) { ?> checked <?PHP } ?> id="durationtype" value="1" class="supporting_classs"/>                             
                              </div>
                           
                            </div>
                          </div>
                          <!----------------------------------> 
                          <!---------------------------------->
                          <?PHP
						$fchecked1 = "";
						$fchecked2 = "";
						if($sup->funding_support == '1')
						{
							$fchecked1 ="checked";
							$display = "block";
						}
						else
						{
							$fchecked2 ="checked";
							$display = "none";
						}
					?>
                          <div class="col-md-12">
                            <label class="text-warning">الدعم التمويلي:</label>
                            <label>نعم
                              <input type="radio" name="funding_support[]" id="funding_support<?php echo $i; ?>" value="1" class="supporting_classs" <?php echo $fchecked1; ?>/>
                              &nbsp;&nbsp; لا
                              <input type="radio" checked name="funding_support[]" id="funding_support<?php echo $i; ?>" value="0" class="supporting_classs" <?php echo $fchecked2; ?>/>
                            </label>
                          </div>
                          <div class="row">
                            <div class="col-md-12 yes_class_funds funding_support<?php echo $i; ?>" style="display:<?php echo $display; ?>;">
                              <div class="col-md-2">
                                <input type="text" placeholder="مبلغ الدعم (رع)"  name="amount_support[]" id="amount_support" class="NumberInput form-control" value="<?php echo $sup->amount_support;  ?>"/>
                              </div>
                              <div class="col-md-2">
                                <input type="text" placeholder="جهة الدعم (رع)"  name="support_point[]" id="support_point" class="NumberInput form-control" value="<?php echo $sup->support_point;  ?>"/>
                              </div>
                              <div class="col-md-2">
                                <select name="support_type[]" id="<?php echo $i; ?>" class="others form-control">
                                  <option value="">نوع الدعم</option>
                                  <?PHP $aax = array('قرض'=>'قرض','منحة'=>'منحة','others'=>'اخرى يتم ذكرها');
							foreach($aax as $paax => $paaxvalue)
							{
								$html .= '<option value="'.$paaxvalue.'" ';
								if($paax==$sup->support_type)
								{
									$html .= ' selected="selected" ';
								}
								$html .= '>'.$paaxvalue.'</option>';
							}
								echo $html;				
						?>
                                </select>
                              </div>
                              <?php
							if($sup->support_type == 'others')
							{	$dislpay= "block"; 	}
							else
							{	$dislpay= "none";	}
						?>
                              <div class="col-md-2 yes_class_funds" id="others<?php echo $i; ?>" style="display:<?php echo $dislpay; ?>;">
                                <input  type="text" class="form-control" placeholder="أخرى (يتم ذكرها)" name="mention_others[]" id="mention_others" value="<?php echo $sup->mention_others;  ?>"/>
                              </div>
                            </div>
                          </div>
                          <!----------------------------------> 
                          <!---------------------------------->
                          <div class="col-md-12">
                            <label class="text-warning">وجه دعم أخرى: </label>
                            <label>نعم
                              <input type="radio" name="face_others_support[]" id="face_others_support0" value="1" class="supporting_classs"/>
                              &nbsp;&nbsp; لا
                              <input type="radio" checked  name="face_others_support[]" id="face_others_support0" value="0" class="supporting_classs"/>
                            </label>
                          </div>
                          <div class="col-md-12 yes_class_others face_others_support0"  style="display:none;">
                            <div class="col-md-2">
                              <input type="text" placeholder="اذكرها"  name="face_others_support_text[]" id="face_others_support_text" class="NumberInput form-control" value="<?php echo $sup->amount_support;  ?>"/>
                            </div>
                          </div>
                        </div>
                        <?PHP }  ?>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">المشروع</h4>
                          </div>
                        </div>
                        <div class="row" id="second">
                          <div class="col-md-12">
                            <label class="text-warning"></label>
                            <label>مفتوح
                              <input type="checkbox" name="parwa_open" id="parwa_open" value="1" onclick = "check_status(this)"/>
                              &nbsp;&nbsp; مغلق
                              <input type="checkbox" name="close_project" id="close_project" value="2" onclick = "check_status(this)"/>
                            </label>
                          </div>
                          <div class="col-md-12  parwa_open" style="display:none;">
                            <div class="col-md-2">
                              <label class="text-warning">نشاط متميز جدا</label>
                              <input type="radio" class="activty_type" checked name="activty_type" id="activty_type1" value="نشاط متميز جدا"  onclick="check_activity(this)" />
                            </div>
                            <div class="col-md-2">
                              <label class="text-warning">نشاط  متميز</label>
                              <input type="radio" class="activty_type" name="activty_type" id="activty_type2" value="نشاط  متميز" onClick="check_activity(this)"/>
                            </div>
                            <div class="col-md-2">
                              <label class="text-warning">نشاط عادي</label>
                              <input type="radio" class="activty_type" name="activty_type" id="activty_type3" value="نشاط عادي" onClick="check_activity(this)"/>
                            </div>
                            <div class="col-md-2">
                              <label class="text-warning">يواجه صعوبات</label>
                              <input type="radio" class="activty_type" name="activty_type" id="activty_type4" value="يواجه صعوبات" onClick="check_activity(this)"/>
                            </div>
                          </div>
                          <div class="col-md-12 difficulties_details" style="display:none;">
                            <div class="col-md-10">
                              <label class="text-warning">اذكرالصعوبات</label>
                              <br>
                              <textarea id="difficulties" name="difficulties" class="form-control txtarea"></textarea>
                            </div>
                          </div>
                          <div class="col-md-12  close_project" style="display:none;">
                            <div class="col-md-2">
                              <label class="text-warning">نهائي</label>
                              <input type="radio" name="project_status" id="project_status" class="p_status" value="نهائي" onClick="check_status2(this);" />
                            </div>
                            <div class="col-md-2">
                              <label class="text-warning">ظرفي</label>
                              <input type="radio" name="project_status" id="project_status2" class="p_status" value="ظرفي"  onclick="check_status2(this);" />
                            </div>
                          </div>
                          <br clear="all">
                          <div class="col-md-12  reason" style='display:none;'>
                            <div class="col-md-6">
                              <label class="text-warning"> الأسباب (ان وجدت)</label> 
                              <br>                             
                              <textarea id="reason_text" name="reason_text" class="form-control txtarea"></textarea>
                            </div>
                            <div class="col-md-6">
                              <label class="text-warning">رأي المتابـــع</label>
                              <br>
                              <textarea id="rai_mutaba" name="rai_mutaba" class="form-control txtarea"></textarea>
                            </div>
                          </div>
                          <!----------------------------------> 
                          <!----------------------------------> 
                          
                        </div>
                        <br>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">مقترحات صاحب المشروع لتخطي الصعوبات او تطوير الموسسة:</h4>
                          </div>
                          <div class="col-md-12">
                            <textarea id="ckeditor" name="project_propsel" class="txtarea"></textarea>
                          </div>
                        </div>
                        <!-------------------------------------> 
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-4">
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">بطاقة تقييم مشروع:</h4>
                          </div>
                          <div class="col-md-12">
                         
                          	<div class="col-md-2" style="color: #d08926;">بطاقة تقييم مشروع:</div>
                            <div class="col-md-3">
                             <?PHP if($applicant->mashrow_taqseem!='')
							 		{	echo '<strong>'.$applicant->mashrow_taqseem.'</strong><input type="hidden" name="mashrow_taqseem" value="'.$applicant->mashrow_taqseem.'">';  ?>
                                    	<script>
											$(function(){
												mashrow_taqseem('<?PHP echo $applicant->mashrow_taqseem; ?>');
											});
										</script>
                                    <?PHP
									}
									else
									{
						  ?>
                            <select place-holder="بطاقة تقييم مشروع" id="mashrow_taqseem" name="mashrow_taqseem" class="form-control req">
                                        	<option value="0">بطاقة تقييم مشروع</option>
                                            <?PHP foreach(bataka_taqseem() as $key => $value) { ?>
                                            	<option value="<?PHP echo $key; ?>"><?PHP echo $key; ?></option>
                                            <?PHP } ?>
                                        </select>
                            <?PHP  } ?>            
                                        </div>
                                        <br clear="all">
                          </div>
                          <div class="col-md-12" id="inamatlist">
                            
                          </div>
                        </div>
                       
                        
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-5">
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">تاريخ التقييم:</h4>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-3">
                              <label class="text-warning">تاريخ التقييم:</label>
                              <label>
                                <input type="text" class="form-control" name="month" id="month" value="<?php echo date('m/d/Y'); ?>">
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <table  class="table table-bordered table-striped dataTable">
                              <tr>
                                <td colspan='2'>المصروفات (ر.ع)</td>
                                <td colspan='2'>الإيرادات (ر.ع)</td>
                              </tr>
                              <tr>
                                <td>مشتريات</td>
                                <td><input  type="text" placeholder="مشتريات"  name="purchase" class="NumberInput req expence form-control" id="purchase" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                                <td rowspan='3' >إيراد</td>
                                <td rowspan='4'><input  type="text" placeholder="إيراد"  name="manpower_project" class="NumberInput req income form-control" id="manpower_project" style="width: 100%;height:100%" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>راتب صاحب المشروع (بما في ذلك التأمينات الاجتماعية)</td>
                                <td><input  type="text" placeholder="راتب صاحب المشروع (بما في ذلك التأمينات الاجتماعية)"  name="salary_project" class="NumberInput req expence form-control" id="salary_project" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>رواتب القوى العاملة بالمشروع (بما في ذلك التأمينات الاجتماعية)</td>
                                <td><input  type="text" placeholder="راتب صاحب المشروع (بما في ذلك التأمينات الاجتماعية)"  name="manpower_project" class="NumberInput req expence form-control" id="manpower_project" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>إيجار</td>
                                <td><input  type="text" placeholder="إيجار"  name="rent" class="NumberInput req expence form-control" id="rent" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>إنترنت</td>
                                <td><input  type="text" placeholder="إنترنت"  name="expence" class="NumberInput req expence form-control" id="expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                                <td rowspan='2' >إيرادات أخرى متصلة بالمشروع </td>
                             	<td rowspan='3'><input  type="text" placeholder="إيرادات أخرى متصلة بالمشروع "  name="other_income" class="NumberInput req income form-control" id="other_income" style="width: 100%;height:100%" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>كهرباء</td>
                                <td><input  type="text" placeholder="كهرباء"  name="wire_expence" class="NumberInput req expence form-control" id="wire_expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>ماء</td>
                                <td><input  type="text" placeholder="ماء"  name="water_expence" class="NumberInput req expence form-control" id="water_expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>هاتف</td>
                                <td><input  type="text" placeholder="هاتف"  name="number_expence" class="NumberInput req expence form-control" id="number_expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>فاكس</td>
                                <td><input  type="text" placeholder="فاكس"  name="fax_expence" class="NumberInput req expence form-control" id="fax_expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>خدمات مختلفة</td>
                                <td><input  type="text" placeholder="خدمات مختلفة"  name="diffrent_services" class="NumberInput req expence form-control" id="diffrent_services" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>مصروفات أخرى متصلة بالمشروع</td>
                                <td><input  type="text" placeholder="مصروفات أخرى متصلة بالمشروع"  name="other_expence" class="NumberInput req expence form-control" id="other_expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>الإجمالي</td>
                                <td><input  type="text" placeholder="الإجمالي"  name="total_expence" class="NumberInput req form-control" id="total_expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                                <td colspan='2'><input  type="text" placeholder="الإجمالي"  name="total_income" class="NumberInput req  form-control" id="total_income" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12"> صافي الدخل الشهري للمشروع   = <span id="tt2"></span>- <span id="tt"></span> = <span id="tt3"></span> ريال عماني </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="col-md-3">صافي الأرباح الشهرية   = </div>
                            <div class="col-md-3"><span>
                              <input type="text" class="form-control" id="tt5" />
                              </span> - </div>
                            <div class="col-md-3"><span id="tt4"></span> =<span id="tt6"> ريال عماني </span></div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-6">
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">رأي المتابـــع:</h4>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <textarea id="ckeditor2" name="observe_view" style="width: 100%; height: 300px;"></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <button type="button" id="save_data_form_bingo_follow" class="btn btn-success">حفظ</button>
                  </div>
                </div>
                <!-------------------------------------> 
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
$(function() {
	first_counter = '<?php echo count($financial); ?>';
	second_counter = '<?php echo count($support); ?>';
});
</script> 
<script src="<?PHP echo base_url(); ?>js/request_followup.js"></script>
</div>
</body>
</html>
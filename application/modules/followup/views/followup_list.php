<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <?PHP //$this->load->view("common/globalfilter",array('type'=>'followup')); ?>
                <div style="text-align:center;" id="tableSortable_wrapper"
                                     class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"></div>
                  <table class="table table-bordered table-striped dataTable" id="basicTable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center; width: 75px;">رقم</th>
                        <th style="text-align:center;">اسم</th>
                        <th style="text-align:center; width: 105px;">الرقم المدني</th>
                        <th style="text-align:center; width: 100px;">رقم الهاتف</th>
                        <th style="text-align:center;">طريقة إسناد الأعداد</th>
                        <th style="text-align:center;">نوع الزيارة</th>        
                        <th colspan="2" style="text-align:center;">متوسط الايرادات</th>
                        <th colspan="2" style="text-align:center;">متوسط صافي الريح </th>                                                                                                                       
                        <th style="text-align:center; width: 94px;">الإجراءات</th>
                      </tr>
                       <tr role="row">
                        <th colspan="6" style="text-align:center; width: 75px;"></th>
                        <th style="text-align:center; color:#d08926;">الشهرية</th>
                        <th style="text-align:center; color:#d08926;">السنوية</th>
                        <th style="text-align:center; color:#d08926;">الشهرية</th>
                        <th style="text-align:center; color:#d08926;">السنوية</th>                                                                                                                       
                        <th style="text-align:center; width: 94px;"></th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?PHP
                    	$followuplist = json_decode($this->follow->follow_list_new(),TRUE);
						foreach($followuplist as $fkey => $fdata)
						{	
							$monthly = $this->follow->get_followup_model($fdata['appid']);		
					?>
                    <tr role="row" id="<?PHP echo $fdata['DT_RowId']; ?>">
                        <td colspan="6">&nbsp;</td>
                        <td colspan="4" style="text-align:center; color:#d08926;"><?PHP echo arabic_date($monthly['month']); ?></td>
                        <td>&nbsp;</td>
                      </tr>
                   <tr role="row" id="<?PHP echo $fdata['DT_RowId']; ?>">
                        <td style="text-align:center; width: 75px;"><?PHP echo $fdata['رقم']; ?></td>
                        <td style="text-align:center;"><?PHP echo $fdata['اسم']; ?></td>
                        <td style="text-align:center; width: 105px;"><?PHP echo $fdata['الرقم المدني']; ?></td>
                        <td style="text-align:center; width: 100px;"><?PHP echo $fdata['رقم الهاتف']; ?></td>
                        <td style="text-align:center;"><?PHP echo $fdata['طريقة إسناد الأعداد']; ?></td>
                        <td style="text-align:center;"><?PHP echo $fdata['نوع الزيارة']; ?></td>        
                        <td style="text-align:center; color:#d08926;"><?PHP echo arabic_date($monthly['total_income']); ?></td>
                        <td style="text-align:center; color:#d08926;"><?PHP echo arabic_date($monthly['monthly_income']); ?></td>
                        <td style="text-align:center; color:#d08926;"><?PHP echo arabic_date($monthly['monthly']); ?></td>
                        <td style="text-align:center; color:#d08926;"><?PHP echo arabic_date($monthly['yearly']); ?></td>                                                                                                                       
                        <td style="text-align:center; width: 94px;"><?PHP echo $fdata['الإجراءات']; ?></td>
                      </tr>
                      <?PHP
						}
					  ?>
                    </tbody>
                  </table>
                </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
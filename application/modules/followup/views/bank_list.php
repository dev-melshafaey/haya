<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <p>
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"></div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center;">اسم فرع البنك</th>
                        <th style="text-align:center;">رقم القرض</th>
                        <th style="text-align:center;">اسم المقترض</th>
                        <th style="text-align:center;">رقم السجل التجاري</th>
                        <th style="text-align:center;">قيمة القرض</th> 
                        <th style="text-align:center;">المبلغ المتبقي للصرف</th>
                        <th style="text-align:center;">حالة القرض</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                     
                    </tbody>
                  </table>
                </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'followup/bank_list/','columns_array'=>'{ "data": "اسم فرع البنك" },
					{ "data": "رقم القرض" },
					{ "data": "اسم المقترض" },
					{ "data": "رقم السجل التجاري" },
					{ "data": "قيمة القرض" },
					{ "data": "المبلغ المتبقي للصرف"},{ "data": "حالة القرض"}')); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
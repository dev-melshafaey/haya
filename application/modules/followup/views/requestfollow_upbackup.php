<!doctype html>
<?PHP
	$type_name = $this->inq->get_type_name($applicatn_info->applicant_marital_status);
	$privince =	$this->inq->get_province_name($applicatn_info->province);
	$wilayats =	$this->inq->get_wilayats_name($applicatn_info->walaya);
	$phone = $this->inq->applicant_phone_number($applicatn_info->applicant_id);
	$tab_1 = $this->inq->get_tab_data('applicant_qualification',$applicatn_info->applicant_id);
	$tab_2 = $this->inq->get_tab_data('applicant_professional_experience',$applicatn_info->applicant_id);
	$tab_3 = $this->inq->get_tab_data('applicant_businessrecord',$applicatn_info->applicant_id);
	$professional =	$this->inq->getRequestInfo($applicatn_info->applicant_id);	
	$applicant = $applicant_data['applicants'];
	$project = $applicant_data['applicant_project'][0];	
	$professional = $applicant_data['applicant_professional_experience'];
	$loan = $applicant_data['applicant_loans'][0];
	$evo  = $applicant_data['project_evolution'][0];
	$phones = $applicant_data['applicant_phones'];
	$comitte = $applicant_data['comitte_decision'][0];
	$fullname = $applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name;
	foreach($phones as $p)
	{	$ar[] = '986'.$p->applicant_phone;	}
		$applicantphone = implode('<br>',$ar);
		
	$steps = get_lable('step-'.$applicant->form_step);
	$youm = array('day'=>'يوم','week'=>'أسبوع','month'=>'شهر','year'=>'عام');
?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <form id="validate_form" name="validate_form" method="post" action="<?PHP echo base_url().'followup/add_follow_up' ?>" autocomplete="off">
        <input type="hidden" name="form_step" id="form_step" value="5" />
        <input type="hidden" name="applicant_id" id="applicant_id" value="<?php echo $applicatn_info->applicant_id;?>" />
        <div class="col-md-12">
          <div class="panel panel-default panel-block">
            <div class="list-group">
              <div class="list-group-item" id="input-fields">
                <div class="form-group">
                  <h4 class="section-title preface-title text-warning"> <?PHP echo $fullname; ?> <span class="label label-default">رقم التسجيل <?php echo arabic_date(applicant_number($applicant_id)); ?></span></h4>
                </div>
                <!--------Starting Tabs--------->
                <div class="row">
                  <div class="col-md-12">
                    <ul class="nav nav-tabs panel panel-default panel-block">
                      <li class="active"><a href="#tabsdemo-1" data-toggle="tab">العنوان الشخصي</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-2" data-toggle="tab">الاستمار والمردود المالي</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-3" data-toggle="tab">أوجه الدعم المقدمة من الجهات الاخرى</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-4" data-toggle="tab">بطاقة تقييم مشروع</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-5" data-toggle="tab">تاريخ التقييم</a></li>
                      <li><img style="margen-top:5px;" src="<?PHP echo base_url(); ?>images/sep.png" width="20" height="44"></li>
                      <li><a href="#tabsdemo-6" data-toggle="tab">رأي المتابـــع</a></li>
                    </ul>
                    <div class="tab-content panel panel-default panel-block" style="background-color: #fff; padding: 6px 8px;">
                      <div class="tab-pane list-group active" id="tabsdemo-1"> 
                        <!------------------------------->
                        <div class="row">
                          <div class="col-md-3">
                            <label class="text-warning">طبيعة المراجعين:</label>
                            <label><strong class="st">
                              <?php  echo $applicatn_info->applicant_type;?>
                              </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">النوع:</label>
                            <label><strong class="st">
                              <?php  echo $applicatn_info->applicant_gender;?>
                              </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم الرقم المدني:</label>
                            <label><strong class="st">
                              <?php  echo is_set($applicatn_info->appliant_id_number);?>
                              </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم الهاتف:</label>
                            <label><strong class="st"><?PHP echo rander_html($phone,'phone'); ?></strong></label>
                          </div>
                        </div>
                        <?php if(!empty($professional['applicant_partners'])) { 
                                    foreach($professional['applicant_partners'] as $partners) { ?>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning">مشترك</h4>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">اسم مقدم الطلب</label>
                            <label><strong class="st"><?php echo $partners->partner_first_name.' '.$partners->partner_middle_name.' '.$partners->partner_last_name.' '.$partners->partner_sur_name;?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">النوع</label>
                            <label><strong class="st">
                              <?php  echo $partners->partner_gender;?>
                              </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم الرقم المدني</label>
                            <label><strong class="st">
                              <?php  echo $partners->partner_id_number;?>
                              </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم الهاتف:</label>
                            <label><strong class="st"><?PHP echo rander_html($phone1,'phone'); ?></strong></label>
                          </div>
                        </div>
                        <?PHP }	} ?>
                        <div class="row">
                          <div class="col-md-3">
                            <label class="text-warning">المرحلة:</label>
                            <label><strong class="st"><?php echo $steps['ar']; ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم بطاقة سجل القوى العاملة:</label>
                            <label><strong class="st"> <?php echo is_set($applicatn_info->applicant_cr_number); ?> </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم الهاتف:</label>
                            <label><strong class="st"><?PHP echo rander_html($phone,'phone'); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">تاريخ الميلاد:</label>
                            <label><strong class="st"> <?php echo show_date($applicatn_info->applicant_date_birth,4); ?> </strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <label class="text-warning">الوضع الحالي:</label>
                            <label><strong class="st"><?php echo $this->inq->get_type_name($applicatn_info->applicant_job_staus); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">الحالة الاجتماعية:</label>
                            <label><strong class="st"> <?php echo $this->inq->get_type_name($applicatn_info->applicant_marital_status); ?> </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">فئة الظمان الإجتماعي:</label>
                            <label><strong class="st">
                              <?php if($applicatn_info->option1 ==	'Y') { echo( is_set($applicatn_info->option_txt)); } else { echo 'لا'; } ?>
                              </strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">فئة من ذوي الإعاقة:</label>
                            <label><strong class="st">
                              <?php if($applicatn_info->option2 ==	'Y') { echo 'نعم'; } else { echo 'لا'; } ?>
                              </strong></label>
                          </div>
                        </div>
                        <!-------------------------------------> 
                        <br>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">العنوان الشخصي</h4>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">محافظة:</label>
                            <label><strong class="st"><?php echo is_set($privince); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">الولاية:</label>
                            <label><strong class="st"><?php echo is_set($wilayats); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">القرية:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->village); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">السكة:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->way); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <label class="text-warning">المنزل/المبني:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->home); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">الشقة:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->deparment); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">ص.ب / ر.ب:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->zipcode) / is_set($applicatn_info->postalcode); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">رقم سجل القوى العاملة:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->applicant_cr_number); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <label class="text-warning">الهاتف الثابت:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->linephone); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">الفاكس:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->fax); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">البريد الإلكتروني:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->email); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">هاتف نقال أحد الأقارب:</label>
                            <label><strong class="st"><?php echo is_set($applicatn_info->refrence_number); ?></strong></label>
                          </div>
                        </div>
                        <!-------------------------------------> 
                        <br>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">المؤهلات</h4>
                          </div>
                          <div class="form-group">
                            <h5 class="section-title" style="margin-right: 14px;">1/ المستوى الدراسي</h5>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">المؤهل:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_qualification); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">التخصص:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_specialization); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">الجهة:</label>
                            <label><strong class="st"><?php echo is_set($this->inq->get_type_name($tab_1->applicant_institute)); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">سنة التخرج:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->application_institute_year); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group">
                            <h5 class="section-title" style="margin-right: 14px;">2/ التدريب المهني</h5>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">مركز التدريب:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_trainningcenter); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">التخصص:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_specializations); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">مدة التدريب (بالأشهر):</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_training_month); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">شهادة التدريب المهني المتحصل عليها:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_vtco); ?></strong></label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <label class="text-warning">سنة الحصول على الشهادة:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_ytotc); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">دورات تدريبية ميدانية أخرى:</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_other_trainning); ?></strong></label>
                          </div>
                          <div class="col-md-3">
                            <label class="text-warning">مدة التدريب (بالأشهر):</label>
                            <label><strong class="st"><?php echo is_set($tab_1->applicant_other_specializations); ?></strong></label>
                          </div>
                          <div class="col-md-3"> </div>
                        </div>
                        <!------------------------------------->
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">الخبرة المهنية</h4>
                          </div>
                          <div class="form-group">
                            <h5 class="section-title" style="margin-right: 14px;">الخبرة في نفس نشاط المشروع</h5>
                          </div>
                          <div class="col-md-12">
                            <table class="table table-bordered table-striped dataTable">
                              <thead>
                                <tr role="row">
                                  <th>تاريخ بداية المشروع</th>
                                  <th>اسم الجهة/المؤسسة/ المشروع الخاص</th>
                                  <th>نشاط الجهة/المؤسسة/ المشروع الخاص</th>
                                  <th>المهنة المزاولة بالجهة/المؤسسة/ المشروع الخاص</th>
                                  <th>عدد سنوات الخبرة</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php 	$counter =	'1';
                                            foreach($professional['applicant_professional_experience'] as $tab)
                                            {									
                                                if($counter <= '3')
                                                { ?>
                                <tr role="row">
                                  <td><?php echo $tab->option_one;?></td>
                                  <td><?php echo $tab->option_two;?></td>
                                  <td><?php echo $tab->option_three;?></td>
                                  <td><?php echo $tab->option_four;?></td>
                                  <td><?php echo $tab->option_five;?></td>
                                </tr>
                                <?php 		} 
                                                $counter++;
                                            } ?>
                              </tbody>
                            </table>
                          </div>
                          <br>
                          <div class="form-group">
                            <h5 class="section-title" style="margin-right: 14px;">الخبرة في أنشطة أخرى</h5>
                          </div>
                          <div class="col-md-12">
                            <table class="table table-bordered table-striped dataTable">
                              <thead>
                                <tr role="row">
                                  <th>تاريخ بداية المشروع</th>
                                  <th>اسم الجهة/المؤسسة/ المشروع الخاص</th>
                                  <th>نشاط الجهة/المؤسسة/ المشروع الخاص</th>
                                  <th> المهنة المزاولة بالجهة/المؤسسة/ المشروع الخاص </th>
                                  <th>عدد سنوات الخبرة</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php 	$counter =	'1';
                                            foreach($professional['applicant_professional_experience'] as $tab)
                                            {									
                                                if($counter <= '3')
                                                { ?>
                                <tr role="row">
                                  <td><?php echo $tab->activities_one;?></td>
                                  <td><?php echo $tab->activities_two;?></td>
                                  <td><?php echo $tab->activities_three;?></td>
                                  <td><?php echo $tab->activities_four;?></td>
                                  <td><?php echo $tab->activities_five;?></td>
                                </tr>
                                <?php 		} 
                                                $counter++;
                                            } ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!------------------------------------->
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">السجلات التجارية الأخرى</h4>
                          </div>
                          <div class="form-group">
                            <h5 class="section-title" style="margin-right: 14px;">الخبرة في نفس نشاط المشروع</h5>
                          </div>
                          <div class="col-md-12">
                            <table class="table table-bordered table-striped dataTable">
                              <thead>
                                <tr role="row">
                                  <th>اسم السجل</th>
                                  <th>رقم السجل</th>
                                  <th>عدد القوى العاملة الوطنية</th>
                                  <th>عدد القوى العاملة الوافدة</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php 	$counter =	'1';
                                            foreach($professional['applicant_businessrecord'] as $tab)
                                            {									
                                                if($counter <= '3')
                                                { ?>
                                <tr role="row">
                                  <td><?php echo $tab->activity_name;?></td>
                                  <td><?php echo $tab->activity_registration_no;?></td>
                                  <td><?php echo $tab->activity_nationalmanpower;?></td>
                                  <td><?php echo $tab->activity_laborforce;?></td>
                                </tr>
                                <?php 		} 
                                                $counter++;
                                            } ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-------------------------------------> 
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-2"> 
                        <!------------------------------------->
                        <?php
                        $financial = array();
                        if(!empty($financial)){
                            foreach($financial as  $i=>$finance){
                                ?>
                        
                        <div class="row" id="first">
                          <i onClick="showMultiForms();" style="margin-right: 14px; cursor:pointer;" class="icon-plus-sign-alt icon_read"></i>
                          <div class="col-md-3 fcounter<?php echo $i; ?>">
                            <label class="text-warning">قيمة المشروع الحالية:</label>
                            <label>
                            <input type="text" name="present_value_project[]" id="present_value_project[]" value="<?php echo $finance->present_value_project; ?>" class="NumberInput form-control" />
                            <div  style="float: left; margin-right: 7px;">رع</div>
                            </label>
                          </div>
                          <div class="col-md-3 fcounter<?php echo $i; ?>">
                            <label class="text-warning">متوسط الايرادات الشهرية:</label>
                            <label>
                            <input type="text" name="average_monthly_revenue[]" id="average_monthly_revenue[]" value="<?php echo $finance->average_monthly_revenue; ?>" class="NumberInput  form-control"/>
                            <div  style="float: left; margin-right: 7px;">رع</div>
                            </label>
                          </div>
                          <div class="col-md-3 fcounter<?php echo $i; ?>">
                            <label class="text-warning">السنوية الايرادات متوسط:</label>
                            <label>
                            <input type="text" name="average_anual_revenue[]" id="average_anual_revenue[]" value="<?php echo $finance->average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            <div  style="float: left; margin-right: 7px;">رع</div>
                            </label>
                          </div>
                          <div class="col-md-3 fcounter<?php echo $i; ?>">
                            <label class="text-warning">الشهري الريح صافي متوسط:</label>
                            <label>
                            <input type="text" name="net_average_monthly_revenue[]" id="net_average_monthly_revenue[]" value="<?php echo $finance->average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            <div  style="float: left; margin-right: 7px;">رع</div>
                            </label>
                          </div>
                          <div class="col-md-3 fcounter<?php echo $i; ?>">
                            <label class="text-warning">السنوي الريح صافي متوسط:</label>
                            <label>
                            <input type="text" name="net_average_anual_revenue[]" id="net_average_anual_revenue[]"  value="<?php echo $finance->net_average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            <div  style="float: left; margin-right: 7px;">رع</div>
                            </label>
                          </div>
                        </div>
                        <?PHP } 
                        } else {  ?>
						<i onClick="showMultiForms();" style="margin-right: 14px; cursor:pointer;" class="icon-plus-sign-alt icon_read"></i>
                        <div class="row" id="first">
                          <div class="col-md-2">
                            <label class="text-warning">قيمة المشروع الحالية (رع)</label>
                            <label>
                              <input type="text" name="present_value_project[]" id="present_value_project[]" value="<?php echo $finance->present_value_project; ?>" class="NumberInput form-control" />
                            </label>
                          </div>
                          <div class="col-md-2">
                            <label class="text-warning">متوسط الايرادات الشهرية (رع)</label>
                            <label>
                              <input type="text" name="average_monthly_revenue[]" id="average_monthly_revenue[]" value="<?php echo $finance->average_monthly_revenue; ?>" class="NumberInput  form-control"/>
                            </label>
                          </div>
                          <div class="col-md-2">
                            <label class="text-warning">السنوية الايرادات متوسط (رع)</label>
                            <label>
                              <input type="text" name="average_anual_revenue[]" id="average_anual_revenue[]" value="<?php echo $finance->average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            </label>
                          </div>
                          <div class="col-md-2">
                            <label class="text-warning">الشهري الريح صافي متوسط (رع)</label>
                            <label>
                              <input type="text" name="net_average_monthly_revenue[]" id="net_average_monthly_revenue[]" value="<?php echo $finance->average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            </label>
                          </div>
                          <div class="col-md-2">
                            <label class="text-warning">السنوي الريح صافي متوسط (رع)</label>
                            <label>
                              <input type="text" name="net_average_anual_revenue[]" id="net_average_anual_revenue[]"  value="<?php echo $finance->net_average_anual_revenue; ?>" class="NumberInput  form-control"/>
                            </label>
                          </div>
                        </div>
                        <?PHP } ?>
                        <!-------------------------------------> 
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-3">
                      
                        <?PHP $support =   array();
					  if(!empty($support))	{ 
					  	foreach($support as $i=>$sup)	{	
							$checked1 = "";
							$checked2 = "";
							if($sup->support_training == '1')
							{
								$checked1 ="checked";
								$display = "block";			}
							else
							{	$checked2 ="checked";
								$display = "none";			}
				 ?>
                        <div class="row" id="second">
                        <i onClick="showSecondMultiple();" style="cursor:pointer;" class="icon-plus-sign-alt icon_read"></i>
                          <div class="col-md-12">
                            <label class="text-warning">الدعم التدريبي:</label>
                            <label>نعم
                              <input type="radio" name="support_training[]" id="support_training<?php echo $i; ?>" value="1" <?php echo $checked1;  ?>  class="supporting_classs"/>
                              &nbsp;&nbsp; لا
                              <input type="radio" checked name="support_training[]" id="support_training<?php echo $i; ?>" value="0" <?php echo $checked2;  ?> class="supporting_classs"/>
                            </label>
                          </div>
                          <div class="col-md-12 yes_class_support support_training<?php echo $i; ?>"  style="display:<?php echo $display; ?>;">
                            <div class="col-md-2">
                            <label class="text-warning">مجال التدريب لصاحب المنشأة:</label>
                              <input class="form-control" placeholder="مجال التدريب لصاحب المنشأة" type="text"  name="training_owner_facility[]" id="training_owner_facility"  value="<?php echo $sup->training_owner_facility;  ?>"/>
                            </div>
                            <div class="col-md-2">
                            <label class="text-warning">جهة التدريب:</label>
                              <input class="form-control" placeholder="جهة التدريب" type="text"  name="training[]" id="training"  value="<?php echo $sup->training;  ?>"/>
                            </div>
                            <div class="col-md-2">
                            <label class="text-warning">المدة:</label>
                              <input id="duration[]" placeholder="المدة" name="duration[]" class="form-control" value="<?php echo $sup->duration; ?>" />
                            </div>
                            <div class="col-md-2">
                              <select id="durationtype[]" name="durationtype[]"  class="form-control">
                                <?PHP foreach($youm as $y) { ?>
                                <option value="<?PHP echo $y; ?>" <?PHP if($sup->duration == $y) { ?>selected="selected" <?PHP } ?>><?PHP echo $y; ?></option>
                                <?PHP } ?>
                              </select>
                            </div>
                            <div class="col-md-2 boxx">
                              <label class="text-warning">قبل التأسيس:</label>
                              نعم
                              <input type="radio" name="before_incoporation[]" id="before_incoporation1" value="1" class="supporting_classs"/>
                              لا
                              <input type="radio" name="before_incoporation[]" id="before_incoporation" value="0" class="supporting_classs" />
                            </div>
                            <div class="col-md-2">
                              <select id="durationtype[]" name="durationtype[]"  class="form-control">
                                <option value="">بعد التأسيس</option>
                                <?PHP foreach($youm as $y) { ?>
                                <option value="<?PHP echo $y; ?>" <?PHP if($sup->duration == $y) { ?>selected="selected" <?PHP } ?>><?PHP echo $y; ?></option>
                                <?PHP } ?>
                              </select>
                            </div>
                          </div>
                          <!----------------------------------> 
                          <!---------------------------------->
                          <?PHP
						$fchecked1 = "";
						$fchecked2 = "";
						if($sup->funding_support == '1')
						{
							$fchecked1 ="checked";
							$display = "block";
						}
						else
						{
							$fchecked2 ="checked";
							$display = "none";
						}
					?>
                          <div class="col-md-12">
                            <label class="text-warning">الدعم التمويلي:</label>
                            <label>نعم
                              <input type="radio" name="funding_support[]" id="funding_support<?php echo $i; ?>" value="1" class="supporting_classs" <?php echo $fchecked1; ?>/>
                              &nbsp;&nbsp; لا
                              <input type="radio" name="funding_support[]" id="funding_support<?php echo $i; ?>" value="0" class="supporting_classs" <?php echo $fchecked2; ?>/>
                            </label>
                          </div>
                          <div class="col-md-12 yes_class_funds funding_support<?php echo $i; ?>" style="display:<?php echo $display; ?>;">
                            <div class="col-md-2">
                              <input type="text" placeholder="مبلغ الدعم (رع)"  name="amount_support[]" id="amount_support" class="NumberInput form-control" value="<?php echo $sup->amount_support;  ?>"/>
                            </div>
                            <div class="col-md-2">
                              <input type="text" placeholder="جهة الدعم (رع)"  name="support_point[]" id="support_point" class="NumberInput form-control" value="<?php echo $sup->support_point;  ?>"/>
                            </div>
                            <div class="col-md-2">
                              <select name="support_type[]" id="<?php echo $i; ?>" class="others form-control">
                                <option value="">نوع الدعم</option>
                                <?PHP $aax = array('قرض'=>'قرض','منحة'=>'منحة','others'=>'اخرى يتم ذكرها');
							foreach($aax as $paax => $paaxvalue)
							{
								$html .= '<option value="'.$paaxvalue.'" ';
								if($paax==$sup->support_type)
								{
									$html .= ' selected="selected" ';
								}
								$html .= '>'.$paaxvalue.'</option>';
							}
								echo $html;				
						?>
                              </select>
                            </div>
                            <?php
							if($sup->support_type == 'others')
							{	$dislpay= "block"; 	}
							else
							{	$dislpay= "none";	}
						?>
                            <div class="col-md-2 yes_class_funds" id="others<?php echo $i; ?>" style="display:<?php echo $dislpay; ?>;">
                              <input  type="text" class="form-control" placeholder="أخرى (يتم ذكرها)" name="mention_others[]" id="mention_others" value="<?php echo $sup->mention_others;  ?>"/>
                            </div>
                          </div>
                          <!----------------------------------> 
                          <!---------------------------------->
                          <div class="col-md-12">
                            <label class="text-warning">وجه دعم أخرى: </label>
                            <label>نعم
                              <input type="radio" name="face_others_support[]" id="face_others_support0" value="1" class="supporting_classs"/>
                              &nbsp;&nbsp; لا
                              <input type="radio" checked  name="face_others_support[]" id="face_others_support0" value="0" class="supporting_classs"/>
                            </label>
                          </div>
                          <div class="col-md-12 yes_class_others face_others_support0"  style="display:none;">
                            <div class="col-md-2">
                              <input type="text" placeholder="اذكرها"  name="face_others_support_text[]" id="face_others_support_text" class="NumberInput form-control" value="<?php echo $sup->amount_support;  ?>"/>
                            </div>
                          </div>
                        </div>
                        <br>
                        <?PHP 		}
						} else { $i = 0; ?>
                        <div class="row" id="second">
                        <i onClick="showSecondMultiple();" style="cursor:pointer;" class="icon-plus-sign-alt icon_read"></i>
                          <div class="col-md-12">
                            <label class="text-warning">الدعم التدريبي:</label>
                            <label>نعم
                              <input type="radio" name="support_training[]" id="support_training<?php echo $i; ?>" value="1" <?php echo $checked1;  ?>  class="supporting_classs"/>
                              &nbsp;&nbsp; لا
                              <input type="radio" checked name="support_training[]" id="support_training<?php echo $i; ?>" value="0" <?php echo $checked2;  ?> class="supporting_classs"/>
                            </label>
                          </div>
                          <div class="row">
                            <div class="col-md-12 yes_class_support support_training<?php echo $i; ?>"  style="display:none;">
                              <div class="col-md-2">
                                <input class="form-control" placeholder="مجال التدريب لصاحب المنشأة" type="text"  name="training_owner_facility[]" id="training_owner_facility"  value="<?php echo $sup->training_owner_facility;  ?>"/>
                              </div>
                              <div class="col-md-2">
                                <input class="form-control" placeholder="جهة التدريب" type="text"  name="training[]" id="training"  value="<?php echo $sup->training;  ?>"/>
                              </div>
                              <div class="col-md-2">
                                <input id="duration[]" placeholder="المدة" name="duration[]" class="form-control" value="<?php echo $sup->duration; ?>" />
                              </div>
                              <div class="col-md-2">
                                <select id="durationtype[]" name="durationtype[]"  class="form-control">
                                  <?PHP foreach($youm as $y) { ?>
                                  <option value="<?PHP echo $y; ?>" <?PHP if($sup->duration == $y) { ?>selected="selected" <?PHP } ?>><?PHP echo $y; ?></option>
                                  <?PHP } ?>
                                </select>
                              </div>
                              <div class="col-md-2 boxx">
                                <label class="text-warning">قبل التأسيس:</label>
                                نعم
                                <input type="radio" name="before_incoporation[]" id="before_incoporation1" value="1" class="supporting_classs"/>
                                لا
                                <input type="radio" checked name="before_incoporation[]" id="before_incoporation" value="0" class="supporting_classs" />
                              </div>
                              <div class="col-md-2">
                                <select id="durationtype[]" name="durationtype[]"  class="form-control">
                                  <option value="">بعد التأسيس</option>
                                  <?PHP foreach($youm as $y) { ?>
                                  <option value="<?PHP echo $y; ?>" <?PHP if($sup->duration == $y) { ?>selected="selected" <?PHP } ?>><?PHP echo $y; ?></option>
                                  <?PHP } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <!----------------------------------> 
                          <!---------------------------------->
                          <?PHP
						$fchecked1 = "";
						$fchecked2 = "";
						if($sup->funding_support == '1')
						{
							$fchecked1 ="checked";
							$display = "block";
						}
						else
						{
							$fchecked2 ="checked";
							$display = "none";
						}
					?>
                          <div class="col-md-12">
                            <label class="text-warning">الدعم التمويلي:</label>
                            <label>نعم
                              <input type="radio" name="funding_support[]" id="funding_support<?php echo $i; ?>" value="1" class="supporting_classs" <?php echo $fchecked1; ?>/>
                              &nbsp;&nbsp; لا
                              <input type="radio" checked name="funding_support[]" id="funding_support<?php echo $i; ?>" value="0" class="supporting_classs" <?php echo $fchecked2; ?>/>
                            </label>
                          </div>
                          <div class="row">
                            <div class="col-md-12 yes_class_funds funding_support<?php echo $i; ?>" style="display:<?php echo $display; ?>;">
                              <div class="col-md-2">
                                <input type="text" placeholder="مبلغ الدعم (رع)"  name="amount_support[]" id="amount_support" class="NumberInput form-control" value="<?php echo $sup->amount_support;  ?>"/>
                              </div>
                              <div class="col-md-2">
                                <input type="text" placeholder="جهة الدعم (رع)"  name="support_point[]" id="support_point" class="NumberInput form-control" value="<?php echo $sup->support_point;  ?>"/>
                              </div>
                              <div class="col-md-2">
                                <select name="support_type[]" id="<?php echo $i; ?>" class="others form-control">
                                  <option value="">نوع الدعم</option>
                                  <?PHP $aax = array('قرض'=>'قرض','منحة'=>'منحة','others'=>'اخرى يتم ذكرها');
							foreach($aax as $paax => $paaxvalue)
							{
								$html .= '<option value="'.$paaxvalue.'" ';
								if($paax==$sup->support_type)
								{
									$html .= ' selected="selected" ';
								}
								$html .= '>'.$paaxvalue.'</option>';
							}
								echo $html;				
						?>
                                </select>
                              </div>
                              <?php
							if($sup->support_type == 'others')
							{	$dislpay= "block"; 	}
							else
							{	$dislpay= "none";	}
						?>
                              <div class="col-md-2 yes_class_funds" id="others<?php echo $i; ?>" style="display:<?php echo $dislpay; ?>;">
                                <input  type="text" class="form-control" placeholder="أخرى (يتم ذكرها)" name="mention_others[]" id="mention_others" value="<?php echo $sup->mention_others;  ?>"/>
                              </div>
                            </div>
                          </div>
                          <!----------------------------------> 
                          <!---------------------------------->
                          <div class="col-md-12">
                            <label class="text-warning">وجه دعم أخرى: </label>
                            <label>نعم
                              <input type="radio" name="face_others_support[]" id="face_others_support0" value="1" class="supporting_classs"/>
                              &nbsp;&nbsp; لا
                              <input type="radio" checked  name="face_others_support[]" id="face_others_support0" value="0" class="supporting_classs"/>
                            </label>
                          </div>
                          <div class="col-md-12 yes_class_others face_others_support0"  style="display:none;">
                            <div class="col-md-2">
                              <input type="text" placeholder="اذكرها"  name="face_others_support_text[]" id="face_others_support_text" class="NumberInput form-control" value="<?php echo $sup->amount_support;  ?>"/>
                            </div>
                          </div>
                        </div>
                        <?PHP }  ?>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">المشروع</h4>
                          </div>
                        </div>
                        <div class="row" id="second">
                          <div class="col-md-12">
                            <label class="text-warning"></label>
                            <label>مفتوح
                              <input type="checkbox" name="parwa_open" id="parwa_open" value="1" onclick = "check_status(this)"/>
                              &nbsp;&nbsp; مغلق
                              <input type="checkbox" name="close_project" id="close_project" value="2" onclick = "check_status(this)"/>
                            </label>
                          </div>
                          <div class="col-md-12  parwa_open" style="display:none;">
                            <div class="col-md-2">
                              <label class="text-warning">نشاط متميز جدا</label>
                              <input type="radio" class="activty_type" checked name="activty_type" id="activty_type1" value="نشاط متميز جدا"  onclick="check_activity(this)" />
                            </div>
                            <div class="col-md-2">
                              <label class="text-warning">نشاط  متميز</label>
                              <input type="radio" class="activty_type" name="activty_type" id="activty_type2" value="نشاط  متميز" onclick="check_activity(this)"/>
                            </div>
                            <div class="col-md-2">
                              <label class="text-warning">نشاط عادي</label>
                              <input type="radio" class="activty_type" name="activty_type" id="activty_type3" value="نشاط عادي" onclick="check_activity(this)"/>
                            </div>
                            <div class="col-md-2">
                              <label class="text-warning">يواجه صعوبات</label>
                              <input type="radio" class="activty_type" name="activty_type" id="activty_type4" value="يواجه صعوبات" onclick="check_activity(this)"/>
                            </div>
                          </div>
                          <div class="col-md-12 difficulties_details" style="display:none;">
                            <div class="col-md-10">
                              <label class="text-warning">اذكرالصعوبات</label>
                              <br>
                              <textarea id="difficulties" name="difficulties" class="form-control txtarea"></textarea>
                            </div>
                          </div>
                          <div class="col-md-12  close_project" style="display:none;">
                            <div class="col-md-2">
                              <label class="text-warning">نهائي</label>
                              <input type="radio" name="project_status" id="project_status" class="p_status" value="نهائي" onclick="check_status2(this);" />
                            </div>
                            <div class="col-md-2">
                              <label class="text-warning">ظرفي</label>
                              <input type="radio" name="project_status" id="project_status2" class="p_status" value="ظرفي"  onclick="check_status2(this);" />
                            </div>
                          </div>
                          <div class="col-md-12  reason" style='display:none;'>
                            <div class="col-md-10">
                              <label class="text-warning"> الأسباب (ان وجدت)</label>
                              <br>
                              <textarea id="reason_text" name="reason_text" class="form-control txtarea"></textarea>
                            </div>
                          </div>
                          <!----------------------------------> 
                          <!----------------------------------> 
                          
                        </div>
                        <br>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">مقترحات صاحب المشروع لتخطي الصعوبات او تطوير الموسسة:</h4>
                          </div>
                          <div class="col-md-12">
                            <textarea id="ckeditor" name="project_propsel" class="txtarea"></textarea>
                          </div>
                        </div>
                        <!-------------------------------------> 
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-4">
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">بطاقة تقييم مشروع:</h4>
                          </div>
                          <div class="col-md-12">
                            <table class="table table-bordered table-striped dataTable">
                              <thead>
                                <tr>
                                  <th colspan='4'>عناصر التقييم</th>
                                  <th>العدد ( 0-5)</th>
                                  <th>ملاحظات</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td colspan='4'>موقع المشروع</td>
                                  <td><input placeholder="موقع المشروع"  type="text"  name="evaluate_project_card" id="evaluate_project_card" class="NumberInput req ratings form-control" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_project_card;  ?>" /></td>
                                  <td><textarea class="txtarea_input"  name="project_card_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->project_card_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>اللوحات واللافتات التوجيهية الدالة على مقر المشروع</td>
                                  <td><input  placeholder="اللوحات واللافتات التوجيهية الدالة على مقر المشروع"  type="text"  name="evaluate_paint_signs" class="NumberInput req ratings form-control" id="evaluate_paint_signs" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_paint_signs;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="paint_signs_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->paint_signs_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>واجهة مقر المشروع</td>
                                  <td><input  type="text" placeholder="واجهة مقر المشروع"  name="evaluate_interface_headquarter" id="evaluate_interface_headquarter" class="NumberInput req ratings form-control" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_interface_headquarter;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="interface_headquarter_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->interface_headquarter_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>ملائمة المحل مع نشاط المشروع</td>
                                  <td><input  type="text"  placeholder="ملائمة المحل مع نشاط المشروع" name="evaluate_convence_project" class="NumberInput req ratings form-control" id="evaluate_convence_project" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_convence_project;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="convence_project_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->convence_project_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>نظافة المحل</td>
                                  <td><input  type="text"  placeholder="نظافة المحل" name="evaluate_shop_cleanliness" id="evaluate_shop_cleanliness" class="NumberInput req ratings form-control" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_shop_cleanliness;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="shop_cleanliness_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->shop_cleanliness_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>تنظيم المحل وتنظيم  الأجنحة والوحدات والبضائع داخله</td>
                                  <td><input placeholder="تنظيم المحل وتنظيم  الأجنحة والوحدات والبضائع داخله"  type="text" class="NumberInput req ratings form-control"  name="evaluate_organize_shop" id="evaluate_organize_shop" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_organize_shop;  ?>" /></td>
                                  <td><textarea class="txtarea_input"  name="organize_shop_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->organize_shop_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>طريقة تخزين المنتجات والبضائع وتوفرها </td>
                                  <td><input  type="text" placeholder="طريقة تخزين المنتجات والبضائع وتوفرها"  name="evaluate_storage_products" class="NumberInput req ratings form-control" id="evaluate_storage_products" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_storage_products;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="storage_products_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->storage_products_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>طريقة العرض والبيع/ مراحل ومسالك الإنتاج/ طريقة تقديم الخدمات</td>
                                  <td><input placeholder="ريقة تقديم الخدمات"  type="text" class="NumberInput req ratings form-control"  name="evaluate_sales_stages" id="evaluate_sales_stages" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_sales_stages;  ?>" /></td>
                                  <td><textarea class="txtarea_input"  name="sales_stages_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->sales_stages_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>وسائل الدعاية المعتمدة</td>
                                  <td><input  type="text"  placeholder="وسائل الدعاية المعتمدة" name="evaluate_advertise_method" class="NumberInput req ratings form-control" id="evaluate_advertise_method" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_advertise_method;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="advertise_method_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->advertise_method_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>استقبال الزبائن والتعامل معهم</td>
                                  <td><input  type="text" placeholder="استقبال الزبائن والتعامل معهم" class="NumberInput req ratings form-control"  name="evaluate_receive_deal" id="evaluate_receive_deal" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_receive_deal;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="receive_deal_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->receive_deal_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>جودة الخدمة/ المنتج/ البضائع وتنوعها</td>
                                  <td><input  type="text" placeholder="ائع وتنوعها" class="NumberInput req ratings form-control"  name="evaluate_quality_service" id="evaluate_quality_service" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_quality_service;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="quality_service_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->quality_service_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>الأسعار المعتمدة</td>
                                  <td><input  type="text" placeholder="الأسعار المعتمدة"  name="evaluate_support_price" class="NumberInput req ratings form-control" id="evaluate_support_price" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_support_price;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="support_price_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->support_price_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>طريقة الترويج والتسويق</td>
                                  <td><input  type="text" placeholder="طريقة الترويج والتسويق"  name="evaluate_method_promotion" class="NumberInput req ratings form-control" id="evaluate_method_promotion" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="method_promotion_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->method_promotion_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>طريقة البيع (نقدا أو بالأجل)</td>
                                  <td><input  type="text"  placeholder="طريقة البيع (نقدا أو بالأجل)"  class="NumberInput req ratings form-control" name="evaluate_method_sale" id="evaluate_method_sale" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_method_sale;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="method_sale"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->method_sale;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>كيفية مجابهة المنافسة</td>
                                  <td><input  type="text" placeholder="كيفية مجابهة المنافسة" class="NumberInput req ratings form-control"  name="evaluate_cope_competition" id="evaluate_cope_competition" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_cope_competition;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="cope_competition_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->cope_competition_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>نوعية المعدات والتجهيزات</td>
                                  <td><input  type="text" placeholder="نوعية المعدات والتجهيزات"   class="NumberInput req ratings form-control" name="evaluate_quality_equipment" id="evaluate_quality_equipment" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_quality_equipment;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="quality_equipment_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->quality_equipment_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>المظهر العام لصاحب المشروع والعاملين معه</td>
                                  <td><input placeholder="المظهر العام لصاحب المشروع والعاملين معه"   type="text" class="NumberInput req ratings form-control" name="evaluate_appearance" id="evaluate_appearance" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) e//cho $evaluate_data->evaluate_appearance;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="appearance_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->appearance_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>احترام توقيت العمل</td>
                                  <td><input  type="text" placeholder="احترام توقيت العمل"  class="NumberInput req ratings form-control" name="evaluate_time" id="evaluate_time" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_time;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="time_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->time_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>طريقة تسيير للمشروع</td>
                                  <td><input  type="text" placeholder="طريقة تسيير للمشروع" class="NumberInput req ratings form-control" name="evaluate_conduct_product" id="evaluate_conduct_product" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_conduct_product;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="conduct_product_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->conduct_product_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>مسك الدفاتر المالية</td>
                                  <td><input  type="text" placeholder="مسك الدفاتر المالية"placeholder="مسك الدفاتر المالية" class="NumberInput req ratings form-control"  name="evaluate_keep_financial" id="evaluate_keep_financial" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_keep_financial;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="keep_financial_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->keep_financial_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>تمكن صاحب المشروع من نشاط مشروعه ومختلف مكوناته</td>
                                  <td><input placeholder="تمكن صاحب المشروع من نشاط مشروعه ومختلف مكوناته"  type="text" class="NumberInput req ratings form-control"  name="evaluate_enables_project_activity" id="evaluate_enables_project_activity" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_enables_project_activity;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="project_activity_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->project_activity_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>التعامل مع المزودين (نقدا أو بالأجل) وانتظام التزويد</td>
                                  <td><input placeholder="التعامل مع المزودين (نقدا أو بالأجل) وانتظام التزويد"  type="text" class="NumberInput req ratings form-control"  name="evaluate_supplier_cash_regularity " id="evaluate_supplier_cash_regularity" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_supplier_cash_regularity;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="supplier_cash_regularity_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->supplier_cash_regularity_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>معرفة السوق والحصة من السوق والوحدات المماثلة في نفس النشاط</td>
                                  <td><input  placeholder="معرفة السوق والحصة من السوق والوحدات المماثلة في نفس النشاط" type="text" class="NumberInput req ratings form-control"  name="evaluate_knowledge_market" id="evaluate_knowledge_market" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_knowledge_market;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="knowledge_market_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->knowledge_market_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>العلاقة بالمحيط والجهات التي يتعامل معها المشروع (العمومية والخاصة)</td>
                                  <td><input placeholder="العلاقة بالمحيط والجهات التي يتعامل معها المشروع (العمومية والخاصة)"  type="text" class="NumberInput req ratings form-control"  name="evaluate_ocean_realtionship" id="evaluate_ocean_realtionship" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_ocean_realtionship;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="ocean_realtionship_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->ocean_realtionship_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>تنمية شبكة علاقات وتحديثها</td>
                                  <td><input placeholder="تنمية شبكة علاقات وتحديثها" type="text" class="NumberInput req ratings form-control"  name="evaluate_network_upload" id="evaluate_network_upload"value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_network_upload;  ?>" /></td>
                                  <td><textarea class="txtarea_input"  name="network_upload_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->network_upload_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>القوى العاملة بالمشروع (العدد، الكفاءة، التدريب)</td>
                                  <td><input placeholder="القوى العاملة بالمشروع (العدد، الكفاءة، التدريب)"  type="text" class="NumberInput req ratings form-control"  name="evaluate_manpower_project" id="evaluate_manpower_project" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_network_upload;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="manpower_project_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->manpower_project_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>التأمينات الاجتماعية والتقاعد (صاحب المشروع والعمال والموظفين)</td>
                                  <td><input  placeholder="التأمينات الاجتماعية والتقاعد (صاحب المشروع والعمال والموظفين)" type="text" class="NumberInput req ratings form-control" name="evaluate_social_security" id="evaluate_social_security" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_social_security;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="social_security_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->social_security_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>تأمين المحل والتجهيزات (الحوادث المهنية، السرقة، الحرائق، الكوارث،...)</td>
                                  <td><input  placeholder="تأمين المحل والتجهيزات (الحوادث المهنية، السرقة، الحرائق، الكوارث،...)" type="text" class="NumberInput req ratings form-control" name="evaluate_shop_equipment_insurance" id="evaluate_shop_equipment_insurance" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_shop_equipment_insurance;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="shop_equipment_insurance"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->shop_equipment_insurance;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>احترام قواعد الصحة والسلامة المهنية والبيئة</td>
                                  <td><input placeholder="احترام قواعد الصحة والسلامة المهنية والبيئة"  type="text" class="NumberInput req ratings form-control" name="evaluate_respect_occupation" id="evaluate_respect_occupation" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_respect_occupation;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="respect_occupation_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->respect_occupation_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td colspan='4'>آفاق تطوير المشروع</td>
                                  <td><input type="text" placeholder="آفاق تطوير المشروع" class="NumberInput req ratings form-control" name="evaluate_prospects_development" id="evaluate_prospects_development" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->evaluate_prospects_development;  ?>"/></td>
                                  <td><textarea class="txtarea_input"  name="prospects_development_development_text"><?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->prospects_development_development_text;  ?>
</textarea></td>
                                </tr>
                                <tr>
                                  <td>المجموع</td>
                                  <td><input  type="text" placeholder="المجموع" class="NumberInput req form-control" name="totalrating" id="totalrating" style="width:100%;" value="<?php if(isset($evaluate_data) && !empty($evaluate_data)) //echo $evaluate_data->totalrating;  ?>" readonly/></td>
                                  <td>المتوسط: (مجموع/30)</td>
                                  <td></td>
                                  <td>التقييم</td>
                                  <td id="taqem_html"></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <br>
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">طريقة إسناد الأعداد:</h4>
                          </div>
                          <div class="col-md-12">
                            <li>(0أو0.5) : غير مناسب تماما،غير متوفر،غير لائق تماما </li>
                            <li>(1أو1.5أو 2):  غير كاف </li>
                            <li>(2.5) : متوسط</li>
                            <li>(3) : فوق المتوسط</li>
                            <li>(3.5) : جيد </li>
                            <li>(4 أو 4.5) : جيد جدا</li>
                            <li>(5) : ممتاز</li>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-5">
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">تاريخ التقييم:</h4>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-3">
                              <label class="text-warning">تاريخ التقييم:</label>
                              <label>
                                <input type="text" class="form-control" name="month" id="month" value="<?php echo date('m/d/Y'); ?>">
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <table  class="table table-bordered table-striped dataTable">
                              <tr>
                                <td colspan='2'>المصروفات (ر.ع)</td>
                                <td colspan='2'>الإيرادات (ر.ع)</td>
                              </tr>
                              <tr>
                                <td>مشتريات</td>
                                <td><input  type="text" placeholder="مشتريات"  name="purchase" class="NumberInput req expence form-control" id="purchase" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                                <td rowspan='4' >إيراد</td>
                                <td rowspan='7'><input  type="text" placeholder="إيراد"  name="manpower_project" class="NumberInput req income form-control" id="manpower_project" style="width: 100%;height:100%" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>راتب صاحب المشروع (بما في ذلك التأمينات الاجتماعية)</td>
                                <td><input  type="text" placeholder="راتب صاحب المشروع (بما في ذلك التأمينات الاجتماعية)"  name="salary_project" class="NumberInput req expence form-control" id="salary_project" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>رواتب القوى العاملة بالمشروع (بما في ذلك التأمينات الاجتماعية)</td>
                                <td><input  type="text" placeholder="راتب صاحب المشروع (بما في ذلك التأمينات الاجتماعية)"  name="manpower_project" class="NumberInput req expence form-control" id="manpower_project" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>إيجار</td>
                                <td><input  type="text" placeholder="إيجار"  name="rent" class="NumberInput req expence form-control" id="rent" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>إنترنت</td>
                                <td><input  type="text" placeholder="إنترنت"  name="expence" class="NumberInput req expence form-control" id="expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                                <td rowspan='3' >إيرادات أخرى متصلة بالمشروع </td>
                              </tr>
                              <tr>
                                <td>كهرباء</td>
                                <td><input  type="text" placeholder="كهرباء"  name="wire_expence" class="NumberInput req expence form-control" id="wire_expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>ماء</td>
                                <td><input  type="text" placeholder="ماء"  name="water_expence" class="NumberInput req expence form-control" id="water_expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>هاتف</td>
                                <td><input  type="text" placeholder="هاتف"  name="number_expence" class="NumberInput req expence form-control" id="number_expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>فاكس</td>
                                <td><input  type="text" placeholder="فاكس"  name="fax_expence" class="NumberInput req expence form-control" id="fax_expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>خدمات مختلفة</td>
                                <td><input  type="text" placeholder="خدمات مختلفة"  name="diffrent_services" class="NumberInput req expence form-control" id="diffrent_services" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>مصروفات أخرى متصلة بالمشروع</td>
                                <td><input  type="text" placeholder="مصروفات أخرى متصلة بالمشروع"  name="other_expence" class="NumberInput req expence form-control" id="other_expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                              <tr>
                                <td>الإجمالي</td>
                                <td><input  type="text" placeholder="الإجمالي"  name="total_expence" class="NumberInput req form-control" id="total_expence" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                                <td colspan='2'><input  type="text" placeholder="الإجمالي"  name="total_income" class="NumberInput req  form-control" id="total_income" value="<?php if(isset($evaluate_data) && !empty($evaluate_data))// echo $evaluate_data->evaluate_method_promotion;  ?>"/></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12"> صافي الدخل الشهري للمشروع   = <span id="tt2"></span>- <span id="tt"></span> = <span id="tt3"></span> ريال عماني </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="col-md-3">صافي الأرباح الشهرية   = </div>
                            <div class="col-md-3"><span>
                              <input type="text" class="form-control" id="tt5" />
                              </span> - </div>
                            <div class="col-md-3"><span id="tt4"></span> =<span id="tt6"> ريال عماني </span></div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane list-group" id="tabsdemo-6">
                        <div class="row">
                          <div class="form-group">
                            <h4 class="section-title preface-title text-warning" style="margin-right: 14px;">رأي المتابـــع:</h4>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <textarea id="ckeditor2" name="observe_view" style="width: 100%; height: 300px;"></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <button type="button" id="save_data_form" class="btn btn-success">حفظ</button>
                  </div>
                </div>
                <!-------------------------------------> 
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
$(function() {
	first_counter = '<?php echo count($financial); ?>';
	second_counter = '<?php echo count($support); ?>';
});
</script> 
<script src="<?PHP echo base_url(); ?>js/request_followup.js"></script>
</div>
</body>
</html>
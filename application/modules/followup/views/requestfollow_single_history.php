<!doctype html>
<?PHP
	$applicant = $applicant_data['applicants'];
	$project = $applicant_data['applicant_project'][0];	
	$professional = $applicant_data['applicant_professional_experience'];
	$loan = $applicant_data['applicant_loans'][0];
	$evo  = $applicant_data['project_evolution'][0];
	$phones = $applicant_data['applicant_phones'];
	$comitte = $applicant_data['comitte_decision'][0];
	$fullname = $applicant->applicant_first_name.' '.$applicant->applicant_middle_name.' '.$applicant->applicant_last_name.' '.$applicant->applicant_sur_name;
	foreach($phones as $p)
	{	$ar[] = '986'.$p->applicant_phone;	}
		$applicantphone = implode('<br>',$ar);
		$steps = get_lable('step-'.$applicant->form_step);
		$youm = array('day'=>'يوم','week'=>'أسبوع','month'=>'شهر','year'=>'عام');
		$aax = array('قرض'=>'قرض','منحة'=>'منحة','others'=>'اخرى يتم ذكرها');
?>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
<?php $this->load->view('common/logo'); ?>
<?php $this->load->view('common/usermenu'); ?>
<?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
<?php $this->load->view('common/quicklunchbar'); ?>
<div class="row">
  <div class="col-md-12">
    <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
    <div id="printable">
      <div class="col-md-12">
      <div class="row" style="direction:rtl !important;">
      <div class="col-md-12" style="background-color:#FFF; padding: 20px 30px;">
      <?php if(!empty($financial) && $type =='return' || $type =='all') {	?>
      <h4 class="section-title preface-title text-warning">
      الاستمار والمردود المالي
      </h3>
      <?php
					foreach($financial as  $i=>$finance) {
			   ?>
      <div class="row setMarginx">
      <div class="col-md-2">
      <label class="text-warning">قيمة المشروع الحالية:</label>
      <label class="setbold">
      <?php echo $finance->present_value_project; ?> رع
      <label>
      </div>
      <div class="col-md-2">
      <label class="text-warning">متوسط الايرادات الشهرية:</label>
      <label class="setbold">
      <?php echo $finance->average_monthly_revenue; ?> رع
      <label>
      </div>
      <div class="col-md-2">
      <label class="text-warning">السنوية الايرادات متوسط:</label>
      <label class="setbold">
      <?php echo $finance->average_anual_revenue; ?> رع
      <label>
      </div>
      <div class="col-md-3">
      <label class="text-warning">الشهري الريح صافي متوسط:</label>
      <label class="setbold">
      <?php echo $finance->average_anual_revenue; ?> رع
      <label>
      </div>
      <div class="col-md-3">
      <label class="text-warning">السنوي الريح صافي متوسط:</label>
      <label class="setbold">
      <?php echo $finance->net_average_anual_revenue; ?> رع
      <label>
      </div>
      </div>
      <div class="row setMarginx">
      <div class="col-md-6">
      <label class="text-warning">تاريخ:</label>
      <label class="setbold">
      <?php echo date('l dS, F, Y h:i:s',strtotime($finance->created)); ?>
      <label>
      </div>
      <div class="col-md-6">
      <label class="text-warning">وأضاف المستعمل من قبل:</label>
      <label class="setbold">
      <?php echo is_set($finance->user_name); ?>
      <label>
      </div>
      </div>
      <?php
					}
				}	?>
      <?php if(!empty($support) && $type == 'support' || $type =='all') { ?>
      <div class="form-group">
        <h4 class="section-title preface-title text-warning">
        اأوجه الدعم المقدمة من الجهات الاخرى
        </h3>
      </div>
      <div class="row setMarginx">
      <?PHP foreach($support as $i=>$sup) { ?>
      <div class="row">
      <div class="col-md-12">
      <div class="col-md-3">
      <label class="text-warning">الدعم التدريبي:</label>
      <label class="setbold">
      <?php echo rander_html($sup->support_training,'opt'); ?>
      <label>
      </div>
      </div>
      <?PHP if($sup->support_training=='1') { ?>
      <div class="col-md-12">
      <div class="col-md-3">
      <label class="text-warning">مجال التدريب لصاحب المنشأة:</label>
      <label class="setbold">
      <?php echo is_set($sup->training_owner_facility); ?>
      <label>
      </div>
      <div class="col-md-2">
      <label class="text-warning">جهة التدريب:</label>
      <label class="setbold">
      <?php echo is_set($sup->training); ?>
      <label>
      </div>
      <div class="col-md-2">
      <label class="text-warning">المدة:</label>
      <label class="setbold">
      <?php echo $sup->duration; ?><?php echo $sup->durationtype; ?>
      <label>
      </div>
      <div class="col-md-2">
      <label class="text-warning">قبل التأسيس:</label>
      <label class="setbold">
      <?php echo rander_html($sup->before_incoporation,'opt'); ?>
      <label>
      </div>
      <div class="col-md-2">
      <label class="text-warning">بعد التأسيس:</label>
      <label class="setbold">
      <?php echo is_set($sup->after_incoporation); ?>
      <label>
      </div>
      </div>
      <?PHP } ?>
      </div>
      <div class="row">
        <div class="col-md-12">
        <div class="col-md-3">
        <label class="text-warning">الدعم التمويلي:</label>
        <label class="setbold">
        <?php echo rander_html($sup->funding_support,'opt'); ?>
        <label>
        </div>
        </div>
        <?PHP if($sup->funding_support=='1') { ?>
        <div class="col-md-12">
          <div class="col-md-2">
          <label class="text-warning">مبلغ الدعم:</label>
          <label class="setbold">
          <?php echo is_set($sup->amount_support); ?> رع
          <label>
          </div>
          <div class="col-md-2">
          <label class="text-warning">جهة الدعم:</label>
          <label class="setbold">
          <?php echo is_set($sup->support_point); ?>
          <label>
          </div>
          <div class="col-md-2">
          <label class="text-warning">نوع الدعم:</label>
          <label class="setbold">
          <?php echo is_set($sup->support_type); ?>
          <label>
          </div>
          <div class="col-md-2">
          <label class="text-warning">أخرى (يتم ذكرها):</label>
          <label class="setbold">
          <?php echo is_set($sup->mention_others); ?>
          <label>
          </div>
          <div class="col-md-2"> </div>
        </div>
        <?PHP } ?>
      </div>
      <div class="row">
      <div class="col-md-12">
      <div class="col-md-3">
      <label class="text-warning">أوجه دعم اخرى:</label>
      <label class="setbold">
      <?php echo rander_html($sup->funding_support,'opt'); ?>
      <label>
      </div>
      </div>
      <?PHP if($sup->funding_support=='1') { ?>
      <div class="col-md-12">
      <div class="col-md-2">
      <label class="text-warning">اذكرها:</label>
      <label class="setbold">
      <?php echo is_set($sup->face_others_support_text);  ?>
      <label>
      </div>
      <div class="col-md-2">
      <label class="text-warning">تاريخ:</label>
      <label class="setbold">
      <?php echo is_set($sup->created); ?>
      <label>
      </div>
      <div class="col-md-3">
      <label class="text-warning">وأضاف المستعمل من قبل:</label>
      <label class="setbold">
      <?php echo is_set($sup->user_name); ?>
      <label>
      </div>
      </div>
      <?PHP } ?>
      </div>
      <?PHP } ?>
      </div>
      <?PHP
if(!empty($annoted1) || !empty($annoted2))
{ ?>
      <div class="form-group">
        <h4 class="section-title preface-title text-warning">
        المشروع
        </h3>
      </div>
      <?PHP } 
if(!empty($annoted1) && $type == 1 || $type =='all') { ?>
      <div class="row setMarginx">
        <div class="col-md-3">
        <label class="text-warning">مفتوح:</label>
        <label class="setbold">
        <label>
        </div>
        <?PHP foreach($annoted1  as $anot) { ?>
        <div class="col-md-10">
          <ul>
            <li><?PHP echo $anot->anoted_value; ?></li>
            <li><?PHP echo $anot->created; ?></li>
            <li>اذكرالصعوبات: <?PHP echo $anot->anoted_details; ?></li>
            <li>وأضاف المستعمل من قبل: <?PHP echo $anot->user_name; ?></li>
          </ul>
        </div>
        <?PHP } ?>
      </div>
      <?PHP } 
if(!empty($annoted2) && $type == 2 || $type =='all') { ?>
      <div class="row setMarginx">
        <div class="col-md-3">
        <label class="text-warning">مغلق:</label>
        <label class="setbold">
        <label>
        </div>
        <?PHP foreach($annoted2  as $anot) { ?>
        <div class="col-md-10">
          <ul>
            <li><?PHP echo $anot->anoted_value; ?></li>
            <li><?PHP echo $anot->created; ?></li>
            <li>اذكرالصعوبات: <?PHP echo $anot->anoted_details; ?></li>
            <li>وأضاف المستعمل من قبل: <?PHP echo $anot->user_name; ?></li>
          </ul>
        </div>
        <?PHP } ?>
      </div>
      <?PHP } ?>
      <?PHP } ?>
      <?PHP if($type == 'proposal' && !empty($prop)) { ?>
      <div class="form-group">
        <h4 class="section-title preface-title text-warning">
        مقترحات صاحب المشروع لتخطي الصعوبات او تطوير الموسسة:
        </h3>
      </div>
      <div class="row setMarginx">
        <div class="col-md-3">
        <label class="text-warning">وأضاف المستعمل من قبل:</label>
        <label class="setbold">
        <?php echo $pro->user_name;  ?>
        <label>
        </div>
        <div class="col-md-3">
        <label class="text-warning">تاريخ:</label>
        <label class="setbold">
        <?php echo $pro->created;  ?>
        <label>
        </div>
        <div class="col-md-5">
          <div class="col-md-10">
            <ul>
              <?PHP foreach($prop as $pro) { ?>
              <li><?PHP echo $pro->project_details; ?></li>
              <?PHP } ?>
            </ul>
          </div>
        </div>
      </div>
      <?PHP } 
if(!empty($evaluate_data) && $type == 'evaluate' || $type =='all') { 
					$evaluate_all = $evaluate_data;
		?>
      <div class="form-group setMarginx">
        <h4 class="section-title preface-title text-warning">بطاقة تقييم مشروع</h4>
      </div>
      <div class="row">
        <?PHP foreach($evaluate_all as $evaluate) { ?>
        <div class="col-md-12">
          <table class="table table-bordered table-striped dataTable">
            <thead>
              <tr role="row">
                <td colspan='4'>عناصر التقييم</td>
                <td>العدد ( 0-5)</td>
                <td>ملاحظات</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan='4'>موقع المشروع</td>
                <td><?php echo $evaluate->evaluate_project_card; ?></td>
                <td><?php  echo $evaluate->project_card_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>اللوحات واللافتات التوجيهية الدالة على مقر المشروع</td>
                <td><?php  echo $evaluate->evaluate_paint_signs;  ?></td>
                <td><?php  echo $evaluate->paint_signs_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>واجهة مقر المشروع</td>
                <td><?php echo $evaluate->evaluate_interface_headquarter;  ?></td>
                <td><?php  echo $evaluate->interface_headquarter_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>ملائمة المحل مع نشاط المشروع</td>
                <td><?php  echo $evaluate->evaluate_convence_project;  ?></td>
                <td><?php echo $evaluate->convence_project_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>نظافة المحل</td>
                <td><?php echo $evaluate->evaluate_shop_cleanliness;  ?></td>
                <td><?php  echo $evaluate->shop_cleanliness_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>تنظيم المحل وتنظيم  الأجنحة والوحدات والبضائع داخله</td>
                <td><?php  echo $evaluate->evaluate_organize_shop;  ?></td>
                <td><?php echo $evaluate->organize_shop_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>طريقة تخزين المنتجات والبضائع وتوفرها </td>
                <td><?php  echo $evaluate->evaluate_storage_products;  ?></td>
                <td><?php  echo $evaluate->storage_products_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>طريقة العرض والبيع/ مراحل ومسالك الإنتاج/ طريقة تقديم الخدمات</td>
                <td><?php echo $evaluate->evaluate_sales_stages ;  ?></td>
                <td><?php echo $evaluate->sales_stages_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>وسائل الدعاية المعتمدة</td>
                <td><?php echo $evaluate->evaluate_advertise_method;  ?></td>
                <td><?php echo $evaluate->advertise_method_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>استقبال الزبائن والتعامل معهم</td>
                <td><?php echo $evaluate->evaluate_receive_deal;  ?></td>
                <td><?php echo $evaluate->receive_deal_text ?></td>
              </tr>
              <tr>
                <td colspan='4'>جودة الخدمة/ المنتج/ البضائع وتنوعها</td>
                <td><?php echo $evaluate->evaluate_quality_service ?></td>
                <td><?php echo $evaluate->quality_service_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>الأسعار المعتمدة</td>
                <td><?php  echo $evaluate->evaluate_support_price;  ?></td>
                <td><?php echo $evaluate->support_price_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>طريقة الترويج والتسويق</td>
                <td><?php echo $evaluate->evaluate_method_promotion; ?></td>
                <td><?php echo $evaluate->method_promotion_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>طريقة البيع (نقدا أو بالأجل)</td>
                <td><?php echo $evaluate->evaluate_method_sale; ?></td>
                <td><?php echo $evaluate->method_sale; ?></td>
              </tr>
              <tr>
                <td colspan='4'>كيفية مجابهة المنافسة</td>
                <td><?php echo $evaluate->evaluate_cope_competition;  ?></td>
                <td><?php echo $evaluate->cope_competition_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>نوعية المعدات والتجهيزات</td>
                <td><?php echo $evaluate->evaluate_quality_equipment ?></td>
                <td><?php echo $evaluate->quality_equipment_text; ?></td>
              </tr>
              <tr>
                <td colspan='4'>المظهر العام لصاحب المشروع والعاملين معه</td>
                <td><?php echo $evolution->evaluate_appearance; ?></td>
                <td><?php echo $evaluate->appearance_text; ?></td>
              </tr>
              <tr>
                <td colspan='4'>احترام توقيت العمل</td>
                <td><?php echo $evaluate->evaluate_time;  ?></td>
                <td><?php echo $evaluate->time_text; ?></td>
              </tr>
              <tr>
                <td colspan='4'>طريقة تسيير للمشروع</td>
                <td><?php echo $evaluate->evaluate_conduct_product ?></td>
                <td><?php echo $evaluate->conduct_product_text; ?></td>
              </tr>
              <tr>
                <td colspan='4'>مسك الدفاتر المالية</td>
                <td><?php echo $evaluate->evaluate_keep_financial;  ?></td>
                <td><?php echo $evaluate->keep_financial_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>تمكن صاحب المشروع من نشاط مشروعه ومختلف مكوناته</td>
                <td><?php echo $evaluate->evaluate_enables_project_activity; ?></td>
                <td><?php echo $evaluate->project_activity_text; ?></td>
              </tr>
              <tr>
                <td colspan='4'>التعامل مع المزودين (نقدا أو بالأجل) وانتظام التزويد</td>
                <td><?php echo $evaluate->evaluate_supplier_cash_regularity;  ?></td>
                <td><?php echo $evaluate->supplier_cash_regularity_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>معرفة السوق والحصة من السوق والوحدات المماثلة في نفس النشاط</td>
                <td><?php echo $evaluate->evaluate_knowledge_market;  ?></td>
                <td><?php echo $evaluate->knowledge_market_text; ?></td>
              </tr>
              <tr>
                <td colspan='4'>العلاقة بالمحيط والجهات التي يتعامل معها المشروع (العمومية والخاصة)</td>
                <td><?php echo $evaluate->evaluate_ocean_realtionship; ?></td>
                <td><?php echo $evaluate->ocean_realtionship_text; ?></td>
              </tr>
              <tr>
                <td colspan='4'>تنمية شبكة علاقات وتحديثها</td>
                <td><?php echo $evaluate->evaluate_network_upload; ?></td>
                <td><?php echo $evaluate->network_upload_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>القوى العاملة بالمشروع (العدد، الكفاءة، التدريب)</td>
                <td><?php echo  $evaluate->manpower_project_text; ?></td>
              </tr>
              <tr>
                <td colspan='4'>التأمينات الاجتماعية والتقاعد (صاحب المشروع والعمال والموظفين)</td>
                <td><?php echo $evaluate->evaluate_social_security; ?></td>
                <td><?php echo $evaluate->social_security_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>تأمين المحل والتجهيزات (الحوادث المهنية، السرقة، الحرائق، الكوارث،...)</td>
                <td><?php echo $evaluate->evaluate_shop_equipment_insurance; ?></td>
                <td><?php echo $evaluate->shop_equipment_insurance;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>احترام قواعد الصحة والسلامة المهنية والبيئة</td>
                <td><?php  echo $evaluate_data->evaluate_respect_occupation;  ?></td>
                <td><?php echo $evaluate->respect_occupation_text;  ?></td>
              </tr>
              <tr>
                <td colspan='4'>آفاق تطوير المشروع</td>
                <td><?php echo $evaluate->evaluate_prospects_development;  ?></td>
                <td><?php echo $evaluate_data->prospects_development_development_text;  ?></td>
              </tr>
              <tr>
                <td>المجموع</td>
                <td><?php echo $evaluate->totalrating;  ?></td>
                <td>المتوسط: (مجموع/30)</td>
                <td></td>
                <td>التقييم</td>
                <td id="taqem_html"><?php echo $evaluate->totalrating/30;  ?></td>
              </tr>
            </tbody>
          </table>
          <div class="col-md-12">
          <div class="col-md-6">
          <label class="text-warning">وأضاف المستعمل من قبل:</label>
          <label class="setbold">
          <?php echo $evaluate->user_name;  ?>
          <label>
          </div>
          <div class="col-md-6">
          <label class="text-warning">تاريخ:</label>
          <label class="setbold">
          <?php echo $evaluate->created;  ?>
          <label>
          </div>
          </div>
          <div class="row setMarginx">
            <div class="col-md-12">
              <h4 class="section-title preface-title text-warning">طريقة إسناد الأعداد:</h4>
              <li>(0أو0.5):غير مناسب تماما،غير متوفر،غير لائق تماما </li>
              <li>(1أو1.5أو 2):غير كاف </li>
              <li>(2.5):متوسط</li>
              <li>(3):فوق المتوسط</li>
              <li>(3.5):جيد </li>
              <li>(4 أو 4.5):جيد جدا</li>
              <li>(5):ممتاز</li>
            </div>
          </div>
        </div>
        <?PHP } ?>
      </div>
      <?PHP } 
if(!empty($monthly_financial) && $type == 'month' || $type =='all') {?>
      <div class="form-group">
        <h4 class="section-title preface-title text-warning">تاريخ التقييم  :<?php echo $monthly->month ?></h4>
      </div>
      <div class="row setMarginx">
        <?PHP foreach($evaluate_all as $evaluate) { ?>
        <div class="col-md-12">
          <?PHP foreach($monthly_financial as $monthly){
					$total = ''; ?>
          <table class="table table-bordered table-striped dataTable">
            <thead>
              <tr>
                <th colspan='2'>المصروفات (ر.ع)</th>
                <th colspan='2'>الإيرادات (ر.ع)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="width: 37%;">مشتريات</td>
                <td style="width: 12%;"><?php echo $monthly->purchase;  ?></td>
                <td rowspan='4' >إيراد</td>
                <td rowspan='7' style="width:26%"><?php  echo $monthly->manpower_project;  ?></td>
              </tr>
              <tr>
                <td  style="width: 37%;">راتب صاحب المشروع (بما في ذلك التأمينات الاجتماعية)</td>
                <td style="width: 12%;"><?php echo $monthly->salary_project;  ?></td>
              </tr>
              <tr>
                <td  style="width: 37%;">رواتب القوى العاملة بالمشروع (بما في ذلك التأمينات الاجتماعية)</td>
                <td style="width: 12%;"><?php echo $monthly->manpower_project;  ?></td>
              </tr>
              <tr>
                <td   style="width: 37%;">إيجار</td>
                <td style="width: 12%;"><?php echo $monthly->rent;  ?></td>
              </tr>
              <tr>
                <td  style="width: 37%;">إنترنت</td>
                <td style="width: 12%;"><?php echo $monthly->expence;   ?></td>
                <td rowspan='3' >إيرادات أخرى متصلة بالمشروع </td>
              </tr>
              <tr>
                <td  style="width: 37%;">كهرباء</td>
                <td style="width: 12%;"><?php echo $monthly->wire_expence;   ?></td>
              </tr>
              <tr>
                <td  style="width: 37%;">ماء</td>
                <td style="width: 12%;"><?php echo $monthly->number_expence;  ?></td>
              </tr>
              <tr>
                <td  style="width: 37%;">هاتف</td>
                <td style="width: 12%;"><?php echo $monthly->fax_expence;   ?></td>
              </tr>
              <tr>
                <td  style="width: 37%;">فاكس</td>
                <td style="width: 12%;"><?php echo  $monthly->purchase;   ?></td>
              </tr>
              <tr>
                <td  style="width: 37%;">خدمات مختلفة</td>
                <td style="width: 12%;"><?php $monthly->diffrent_services;  ?></td>
              </tr>
              <tr>
                <td  style="width: 37%;">مصروفات أخرى متصلة بالمشروع</td>
                <td style="width: 12%;"><?php echo $monthly->other_expence;   ?></td>
              </tr>
              <tr>
                <td style="width: 37%;">الإجمالي</td>
                <td style="width: 12%;"><?php echo $monthly->total_expence;   ?></td>
                <td colspan='2'><?php echo $monthly->total_income; ?></td>
              </tr>
            </tbody>
          </table>
          <?PHP } ?>
          <div class="row setMarginx">
            <div class="col-md-2">وأضاف المستعمل من قبل: <?php echo $evaluate->user_name;  ?></div>
            <div class="col-md-2">تاريخ: <?php echo $evaluate->created;  ?></div>
            <div class="col-md-4">صافي الدخل الشهري للمشروع   = <span id="tt2"></span>- <span id="tt"></span> = <span id="tt3"></span> ريال عماني </div>
            <div class="col-md-4">صافي الأرباح الشهرية   = <span>
              <input type="text" id="tt5" />
              </span>- <span id="tt4"></span> =<span id="tt6"> ريال عماني </div>
          </div>
        </div>
        <?PHP } ?>
      </div>
      <?PHP } 
if(!empty($observer) && $type == 'observer'  || $type =='all') {?>
      <div class="form-group setMarginx">
        <h4 class="section-title preface-title text-warning">رأي المتابـــع:</h4>
      </div>
      <div class="row setMarginx">
      <div class="col-md-12">
      <div class="col-md-4">
        <?php foreach( $observer as $obs) { ?>
        <li>
          <?PHP $obs->project_details; ?>
        </li>
        <?PHP } ?>
      </div>
      <div class="col-md-4">
      <label class="text-warning">وأضاف المستعمل من قبل:</label>
      <label class="setbold">
      <?php echo $evaluate->user_name;  ?>
      <label>
      </div>
      <div class="col-md-4">
      <label class="text-warning">تاريخ:</label>
      <label class="setbold">
      <?php echo $evaluate->created;  ?>
      <label>
      </div>
      </div>
      </div>
      <?PHP } ?>
      <!---------End------>
      </div>
      </div>
      </div>
      <div class="row" style="text-align:center !important;">
        <button type="button" id="" onClick="printthepage('printable');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
      </div>
    </div>
  </div>
  </section>
  <?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
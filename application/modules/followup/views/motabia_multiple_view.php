<?PHP
		if($type=='a')
		{ ?>

<div class="row fcounter<?PHP echo $counter; ?>" id="first">
  <div class="col-md-2"> 
    <!--<label class="text-warning">قيمة المشروع الحالية (رع)</label>-->
    <label>
      <input type="text" name="present_value_project[]" id="present_value_project[]" class="NumberInput form-control" />
    </label>
  </div>
  <div class="col-md-2"> 
    <!--<label class="text-warning">متوسط الايرادات الشهرية (رع)</label>-->
    <label>
      <input type="text" name="average_monthly_revenue[]" id="average_monthly_revenue[]" class="NumberInput  form-control"/>
    </label>
  </div>
  <div class="col-md-2"> 
    <!--<label class="text-warning">السنوية الايرادات متوسط (رع)</label>-->
    <label>
      <input type="text" name="average_anual_revenue[]" id="average_anual_revenue[]" class="NumberInput  form-control"/>
    </label>
  </div>
  <div class="col-md-2"> 
    <!--<label class="text-warning">الشهري الريح صافي متوسط (رع)</label>-->
    <label>
      <input type="text" name="net_average_monthly_revenue[]" id="net_average_monthly_revenue[]" class="NumberInput  form-control"/>
    </label>
  </div>
  <div class="col-md-2"> 
    <!--<label class="text-warning">السنوي الريح صافي متوسط (رع)</label>-->
    <label>
      <input type="text" name="net_average_anual_revenue[]" id="net_average_anual_revenue[]" class="NumberInput  form-control"/>
    </label>
  </div>
  <div class="col-md-2"> <i id="fcounter<?PHP echo $counter; ?>" class="icon-remove-circle" style="font-size: 31px; color: red;" onclick="deleteOption(this.id);"></i> </div>
</div>
<?PHP 	}	else { 
			$i = $counter;
?>
<div class="row scounter<?PHP echo $counter;?>" id="second">
  <div class="form-group">
    <h4 class="section-title preface-title text-warning" style="margin-right: 14px;"></h4>
  </div>
  
  <div class="col-md-12">
  <i id="scounter<?PHP echo $counter; ?>" class="icon-remove-circle" style="font-size: 31px; color: red; float:left;" onclick="deleteOption(this.id);"></i>
    <label class="text-warning">الدعم التدريبي:</label>
    <label>نعم
      <input type="radio" name="support_training[]" onclick="change_val(this)" id="support_training<?php echo $i; ?>" value="1" class="supporting_classs"/>
      &nbsp;&nbsp; لا
      <input type="radio" checked name="support_training[]" onclick="change_val(this)" id="support_training<?php echo $i; ?>" value="0" class="supporting_classs"/>
    </label>
  </div>
  <div class="row">
    <div class="col-md-12 yes_class_support support_training<?php echo $i; ?>"  style="display:none;">
      <div class="col-md-2">
        <input class="form-control" placeholder="مجال التدريب لصاحب المنشأة" type="text"  name="training_owner_facility[]" id="training_owner_facility"/>
      </div>
      <div class="col-md-2">
        <input class="form-control" placeholder="جهة التدريب" type="text"  name="training[]" id="training"/>
      </div>
      <div class="col-md-2">
        <input id="duration[]" placeholder="المدة" name="duration[]" class="form-control" />
      </div>
      <div class="col-md-2">
        <select id="durationtype[]" name="durationtype[]"  class="form-control">
          <?PHP foreach($youm as $y) { ?>
          <option value="<?PHP echo $y; ?>" <?PHP if($sup->duration == $y) { ?>selected="selected" <?PHP } ?>><?PHP echo $y; ?></option>
          <?PHP } ?>
        </select>
      </div>
      <div class="col-md-2 boxx">
        <label class="text-warning">قبل التأسيس:</label>
        نعم
        <input type="radio" name="before_incoporation[]" onclick="change_val(this)" id="before_incoporation1" value="1" class="supporting_classs"/>
        لا
        <input type="radio" checked name="before_incoporation[]" onclick="change_val(this)" id="before_incoporation" value="0" class="supporting_classs" />
      </div>
      <div class="col-md-2">
        <select id="durationtype[]" name="durationtype[]"  class="form-control">
          <option value="">بعد التأسيس</option>
          <?PHP foreach($youm as $y) { ?>
          <option value="<?PHP echo $y; ?>" <?PHP if($sup->duration == $y) { ?>selected="selected" <?PHP } ?>><?PHP echo $y; ?></option>
          <?PHP } ?>
        </select>
      </div>
    </div>
  </div>
  <!---------------------------------->
  <div class="col-md-12">
    <label class="text-warning">الدعم التمويلي:</label>
    <label>نعم
      <input type="radio" name="funding_support[]" onclick="change_val(this)" id="funding_support<?php echo $i; ?>" value="1" class="supporting_classs"/>
      &nbsp;&nbsp; لا
      <input type="radio" checked name="funding_support[]" onclick="change_val(this)" id="funding_support<?php echo $i; ?>" value="0" class="supporting_classs"/>
    </label>
  </div>
  <div class="row">
    <div class="col-md-12 yes_class_funds funding_support<?php echo $i; ?>" style="display:none;">
      <div class="col-md-2">
        <input type="text" placeholder="مبلغ الدعم (رع)"  name="amount_support[]" id="amount_support" class="NumberInput form-control"/>
      </div>
      <div class="col-md-2">
        <input type="text" placeholder="جهة الدعم (رع)"  name="support_point[]" id="support_point" class="NumberInput form-control"/>
      </div>
      <div class="col-md-2">
        <select name="support_type[]" id="<?php echo $i; ?>" class="others form-control">
          <option value="">نوع الدعم</option>
          <?PHP $aax = array('قرض'=>'قرض','منحة'=>'منحة','others'=>'اخرى يتم ذكرها');
							foreach($aax as $paax => $paaxvalue)
							{
								$html .= '<option value="'.$paaxvalue.'" ';
								if($paax==$sup->support_type)
								{
									$html .= ' selected="selected" ';
								}
								$html .= '>'.$paaxvalue.'</option>';
							}
								echo $html;				
						?>
        </select>
      </div>
      <div class="col-md-2 yes_class_funds" id="others<?php echo $i; ?>" style="display:none;">
        <input  type="text" class="form-control" placeholder="أخرى (يتم ذكرها)" name="mention_others[]" id="mention_others"/>
      </div>
    </div>
  </div>
  <!----------------------------------> 
  <!---------------------------------->
  <div class="col-md-12">
    <label class="text-warning">وجه دعم أخرى: </label>
    <label>نعم
      <input type="radio" name="face_others_support[]" onclick="change_val(this)" id="face_others_support<?php echo $i; ?>" value="1" class="supporting_classs"/>
      &nbsp;&nbsp; لا
      <input type="radio" checked  name="face_others_support[]" onclick="change_val(this)" id="face_others_support<?php echo $i; ?>" value="0" class="supporting_classs"/>
    </label>
  </div>
  <div class="col-md-12 yes_class_others face_others_support<?php echo $i; ?>"  style="display:none;">
    <div class="col-md-2">
      <input type="text" placeholder="اذكرها"  name="face_others_support_text[]" id="face_others_support_text" class="NumberInput form-control"/>
    </div>
  </div>
</div>
<?PHP } ?>

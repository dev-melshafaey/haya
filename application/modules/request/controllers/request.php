<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request extends CI_Controller {

	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;
	public $administrator =	NULL;
	public $servicedepartment =	NULL;
	public $moduleid = NULL;
	public $otherservice_moduleid = NULL;
//-----------------------------------------------------------------------

	/*
	* Constructor
	*/
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('request_model','request');
		/////////////////////////////////////////	
		$this->_data['controller']=$this;
		$this->moduleid = 265;
		/////////////////////////
		$this->_data['management']			= $this->management = 102;
		
			$this->_data['finance']			= $this->finance = 295;
			$this->_data['procurement']			= $this->procurement = 1789
;
		//$this->add_users_yearly_holidays(); // Add Yearly holiday into users accounts.
		$this->_data['administrator']			= $this->administrator;
		
		
		$this->_data['moduleid']			= $this->moduleid;
		
		$this->_data['module']			=	$this->haya_model->get_module();		
		$this->_login_userid 			=	$this->session->userdata('userid');
		$this->_data['login_userid'] 	=	$this->_login_userid;
		$this->_data['user_detail'] = $user_detail	=	$this->haya_model->get_user_detail($this->_login_userid);
			
		
		$this->_data['login_userid']	=	$this->_login_userid;
		$this->_data['userroleid']	= $this->userroleid				=  $this->_data['user_detail']['profile']->userroleid;
		// Load all types
		$this->_data['list_types']		=	$this->haya_model->get_listmanagment_types();
		
	}
	public function index()
	{
		$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
		
	}
	public function listall($status=NULL)
	{
		$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
		$this->_data["status"]=$status;
		$this->load->view('listall_request',$this->_data);
	}
	public function ajax_all_listall_request($status=NULL)
	{
			$this->db->select('*');
			$this->db->from('ah_service_request');	
			if($this->finance == $this->userroleid){
				
				if($status !=""){
					if($status =="mylist"){
						$this->db->where("userid",$this->_login_userid);
					}else{
						$this->db->where("fin_status",$status);	
					}
				}
				else{
					$this->db->where("fin_status",NULL);	
				}
				
			}
			else if(($this->management == $this->userroleid)){
				
				if($status !=""){
					if($status =="mylist"){
						$this->db->where("userid",$this->_login_userid);
					}else{
						$this->db->where("fin_status",'accepted');
						$this->db->where("mang_status",$status);	
					}
				}
				else{
					$this->db->where("fin_status",'accepted');
					$this->db->where("mang_status","");	
				}
			}
			else if(($this->procurement == $this->userroleid)){
				
				if($status !=""){
					if($status =="mylist"){
						$this->db->where("userid",$this->_login_userid);
					}else{
						$this->db->where("mang_status",'accepted');
						$this->db->where("pro_status_sub",$status);	
					}
				}
				else{
					$this->db->where("mang_status",'accepted');
					$this->db->where("pro_status_sub","");	
				}
				
			}
			else{
				$this->db->where("userid",$this->_login_userid);	
			}
			$this->db->where("delete_record",'0');
			$this->db->order_by('id','DESC');
			
			$query = $this->db->get();
			//$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
						
			foreach($query->result() as $lc)
			{
				if($lc->id)
				{
					$action 	='<a onclick="alatadad(this);" data-url="'.base_url().'request/view_request/'.$lc->id.'" href="#"><i class="my icon icon-eye-open"></i></a>';
				}
				if($lc->userid == $this->_login_userid and $lc->fin_status != "accepted" ){
					$action	.= ' <a href="'. base_url().'request/add_request/'.$lc->id.'" id="'.$lc->id.'"><i class="icon-pencil"></i></a>';
				
					$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->id.'" data-url="'.base_url().'request/delete_request/'.$lc->id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
				}
				if($this->finance == $this->userroleid  && $lc->fin_status != "accepted"){
					$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'request/edit_finance_request/'.$lc->id.'" href="#"  id="'.$lc->id.'"><i class="icon-pencil"></i></a>';
				}
				if( $this->management == $this->userroleid  && $lc->mang_status != "accepted"){
					$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'request/edit_finance_request/'.$lc->id.'" href="#"  id="'.$lc->id.'"><i class="icon-pencil"></i></a>';
				}
				
				if($this->procurement == $this->userroleid){
					if($lc->pro_status_sub != "accepted" && $lc->pro_status != 'Quotation Submitted'){
					$action	.= ' <a href="'.base_url().'request/add_quotation/'.$lc->id.'"    id="'.$lc->id.'"><i class="icon-plus-sign-alt"></i></a>';
					
					}
					if($lc->pro_id >0 && $lc->pro_status == 'Quotation Submitted' ){
						if($lc->pro_status_sub != "accepted"){
						$action	.= ' <a onclick="alatadad(this);" data-url="'.base_url().'request/approveQuotation/'.$lc->id.'" href="#"  id="'.$lc->id.'"><i class="icon-pencil"></i></a>';
						}
					}
				}
				
				$this->db->select('list_name');
				$this->db->from('ah_listmanagement');						
				$this->db->where("list_id",$lc->department);
				$rowuser1 = $this->db->get(); 
				foreach($rowuser1->result() as $us){$department = $us->list_name;}
				if($lc->status =="0"){$status ="Urgent";}else{$status = "Not Urgent";}
				$arr[] = array(
					"DT_RowId"		=>	$lc->id.'_durar_lm',
					"الأسم" 			=> $lc->item,
					"الدائرة" 		=> $department,
					"التاريخ" 		=> $lc->duedate,
					"المبلغ" 		=> $lc->totalcost,
					"الحالة" 		=> $status,		
					"الإجراءات" 		=>	$action 
					);
					
					unset($action,$items);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
	}
	public function add_request($id=0)
	{
		//print_r($this->input->post());exit();
		if($this->input->post())
		{
			//print_r($this->input->post());exit();
			$data = $this->input->post();
			
			//get dept
			$this->db->select('profession');
			$this->db->from('ah_userprofile');	
			$this->db->where("userid",$this->_login_userid);	
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				$res = $query->row();
				$buget["department"] =$res->profession;
			}
			
			$buget["item"] = $data["item"];
			$buget["quantity"] = $data["quantity"];
			$buget["cost"] = $data["cost"];
			$buget["totalcost"] = $data["totalcost"];
			//$buget["department"] = $this->_data['user_detail']['profile']->profession;
			$buget["suggest"] = $data["suggest"];
			$buget["status"] = $data["status"];
			
			
			$buget["userid"] =  $this->_login_userid;
			if($data["id"] <= 0){
				$buget["duedate"] = date('Y-m-d');
				$buget["id"] ='';
				if(count($_FILES["Sfiles"]["name"]) > 0 and ($_FILES["Sfiles"]["name"][0] !=""))
				{
					$names = array();
					$files = $_FILES["Sfiles"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/service_request');
						}
						
					}
					if($data["Sfiles_old"] !=""){
						if(count($names)>0)	$buget['Sfiles']	=	$data["Sfiles_old"].",".@implode(',',$names);
						else{$buget['Sfiles']	=	$data["Sfiles_old"];}
					}
					else{
						$buget['Sfiles']	=	@implode(',',$names);	
					}
					
				}
				else{
					$buget['Sfiles']	=	$data["Sfiles_old"];
				}
			}
			else{
				$buget["id"] =$data["id"];
				if(count($_FILES["Sfiles"]["name"]) > 0 and ($_FILES["Sfiles"]["name"][0] !=""))
				{
					$names = array();
					$files = $_FILES["Sfiles"];
					 foreach ($files['name'] as $key => $image) {
						if(!empty($files['name'][$key]))
						{
							$_FILES['images']['name']= $files['name'][$key];
							$_FILES['images']['type']= $files['type'][$key];
							$_FILES['images']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['images']['error']= $files['error'][$key];
							$_FILES['images']['size']= $files['size'][$key];
							$names[] =	$this->upload_file($this->_login_userid,'images','resources/service_request');
						}
						
					}
					$buget['Sfiles']	=	@implode(',',$names);
				}
			}
			 $this->request->savestore($buget,'ah_service_request','id');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			redirect(base_url().'request/listall/mylist');
			exit();
			
		}
			if($id >0){
				$this->_data["rows"] = $this->request->getdata($id,'ah_service_request','id');
				//print_r($this->_data["rows"]);
			
		}
		$this->_data['id'] =$id;
		$this->load->view('add_request',$this->_data);
	}
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
				
		if(!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
	function getfileicon($filename,$path)
	{
		$filename = strtolower($filename);
		$file_extension = strrchr($filename, ".");	
	
		$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_nd.png" style="width:30px; height:30px" alt="image" title="image"/></a>';
	
		if($file_extension=='.doc' || $file_extension=='.docx')
	
			 $rettype =' <a class="fancybox-button" rel="gallery1" href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_doc.png" style="width:30px; height:30px" alt="doc" title="doc"/></a>';
	
		if($file_extension=='.xls' || $file_extension=='.xlsx' || $file_extension=='.csv' || $file_extension=='.xlt' || $file_extension=='.xltx')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_xls.png" style="width:30px; height:30px" alt="xls" title="xls"/></a>';
	
		if($file_extension=='.pdf')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_pdf.png" style="width:30px; height:30px" alt="pdf" title="pdf"/></a>';
	
		if($file_extension=='.jpg' || $file_extension=='.jpeg'  || 	$file_extension=='.gif')
	
			$rettype=' <a class="fancybox-button" rel="gallery1" href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_jpg.png" style="width:30px; height:30px" alt="jpg" title="jpg"/></a>';
	
		if($file_extension=='.png' )
	
			$rettype=' <a class="fancybox-button" rel="gallery1" href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_png.png" style="width:30px; height:30px" alt="png" title="png"/></a>';
	
		if($file_extension=='.ppt' || $file_extension=='.pptx')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_ppt.png" style="width:30px; height:30px" alt="ppt" title="ppt"/></a>';		
	
		if($file_extension=='.zip' || $file_extension=='.tz' || $file_extension=='.rar' || $file_extension=='.gzip')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_zip.png" style="width:30px; height:30px" alt="zip" title="zip"/></a>';		
	
		if($file_extension=='.txt' || $file_extension=='.rtf')
	
			$rettype='<a target="_blank"  href="'.$path.'/'.$filename.'"><img src="'.base_url().'assets/images/filestypes/file_type_txt.png" style="width:30px; height:30px" alt="txt" title="txt"/></a>';
	
		return $rettype;
	
			
	
	}
	function removeimage($id,$imagename){
		
		unlink('resources/service_request/'.$this->_login_userid."/".$imagename);
		$result = $this->request->getdata($id,'ah_service_request',"id");
		if($result->Sfiles !=""){
			$image=@explode(',',$result->Sfiles);
			foreach($image as $key=>$vals){
				if($imagename == $vals){
					unset($image[$key]);
				}
			}
			$data["Sfiles"]=@implode(',',$image);
			$data["id"] = $id;
			$this->request->savestore($data,'ah_service_request','id');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
		}
		redirect(base_url().'request/add_request');
		exit();
	}
	function view_request($id){
		$this->_data['rows'] = $this->request->getdata($id,'ah_service_request','id');
		$this->load->view('view_request',$this->_data);
	}
	function edit_finance_request($id){
		$this->_data['rows'] = $this->request->getdata($id,'ah_service_request','id');
		$this->load->view('edit_finance_request',$this->_data);
	}
	function update_finance(){
		if($this->input->post())
		{
			$data = $this->input->post();
			$buget["id"] = $data["id"];
			$buget["budget_sub_id"] = $data["budget_sub_id"];
			$buget["fin_status"] = $data["fin_status"];
			$buget["currentArbitrage"] = $data["currentArbitrage"];
			$buget["fin_reason1"] = $data["fin_reason1"];
			$buget["fin_reason2"] = $data["fin_reason2"];
			$buget["fin_date"] = date('Y-m-d');
			
			$buget["finance_userid"] =  $this->_login_userid;
			 $this->request->savestore($buget,'ah_service_request','id');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			exit();
		}
	}
	function update_management(){
		if($this->input->post())
		{
			$data = $this->input->post();
			$buget["id"] = $data["id"];
			$buget["mang_status"] = $data["mang_status"];
			$buget["mang_reason1"] = $data["mang_reason1"];
			$buget["mang_reason2"] = $data["mang_reason2"];
			$buget["mang_date"] = date('Y-m-d');
			
			$buget["mang_id"] =  $this->_login_userid;
			 $this->request->savestore($buget,'ah_service_request','id');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			exit();
		}
	}
	function add_quotation($id){
		$this->_data['id'] =$id;
		$this->_data['service'] = $service =  $this->request->getdata($id,'ah_service_request','id');
		$this->_data['rows'] = $service =  $this->request->getdata($id,'ah_service_request_quotation_main','req_id');
		$this->_data['qut_id'] =$qut_id;
		
		if($qut_id == ""){
			$result = $this->request->getalllastrows('ah_service_request_quotation_main','qut_id');
			
			if($result){$lastqut_id = $result->qut_id;}else{$lastqut_id = 0;} $lastqut_id++;
			$this->_data['qut_no'] =$service->id.'/'.$lastqut_id.'/'.date('Ymd');
			
		}
		else{
		
		}
		if($this->input->post())
		{
			$data = $this->input->post();
			$buget["req_id"] = $data["id"];
			$buget["qut_no"] = $data["qut_no"];
			$buget["qut_title"] = $data["qut_title"];
			$buget["qut_totalamount"] = $data["qut_totalamount"];
			$buget["qut_desc"] = $data["qut_desc"];
			$buget["qut_date"] = date('Y-m-d');
			
			$buget["userid"] =  $this->_login_userid;
			 $qut_id = $this->request->savestore($buget,'ah_service_request_quotation_main','qut_id');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			redirect(base_url().'request/add_quotation/'.$id.'/'.$qut_id);
			exit();
		}
		$this->load->view('add_quotation',$this->_data);
	}
	function loadallsuppliers(){
		$qut_id = $_POST["qut_id"];
		$this->db->select('*');
        $this->db->from('ah_service_request_quotation_sub');
		 $this->db->where('qut_id',$qut_id);
		 $this->db->where('delete_record','0');
        $this->db->order_by("qut_sub_id", "DESC");
		$query = $this->db->get();
		
		
			$permissions	=	$this->haya_model->check_other_permission(array($this->moduleid));
			$data = '<table class="table table-bordered table-striped "  >
                            <thead style="background-color: #029625;">
                              <tr role="row" style="color:#fff !important;">
							  
                               <th>SI.No</th> 
                                
                                <th>Supplier name</th>
								<th>company</th>
								<th>Email</th>
								<th>Phone</th>
								<th>Action</th>
                              </tr>
                            </thead>
                            <tbody>                     
                           ';
			$no =1;
			if($query->num_rows()>0){
				foreach($query->result() as $lc)
				{
					$action ='';
					if($lc->userid == $this->_login_userid){
						$action	.= ' <a href="#globalDiag" onclick="alatadad(this);"  data-url="'.base_url().'request/addsuppliers/'.$lc->qut_id.'/'.$lc->qut_sub_id.'" style="margin-left:5px;" ><i class="icon-pencil"></i></a>';
						$action	.= ' <a href="#globalDiag" onclick="alatadad(this);"  data-url="'.base_url().'request/additems/'.$lc->qut_id.'/'.$lc->qut_sub_id.'" style="margin-left:5px;" ><i class="icon-plus-sign-alt"></i></a>';
						$action	.= '<a href="#globalDiag" onclick="alatadad(this);"  data-url="'.base_url().'request/viewsupplier/'.$lc->qut_sub_id.'/" style="margin-left:5px;" ><i class="icon-eye-open"></i></a>';
					}
					
					
					if($lc->userid == $this->_login_userid) 
					{
						$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->qut_sub_id.'" data-url="'.base_url().'request/delete_suppliers/'.$lc->qut_sub_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
						
					}
					$data .=' <tr><td>'.$no.'</td>';
					
					$data .='<td>'.$lc->qut_sub_supplier_name.'</td><td>'.$lc->qut_sub_company.'</td><td>'.$lc->qut_sub_email.'</td><td>'.$lc->qut_sub_phone.'</td> <td>'.$action.'</td></tr>';
					$no++;
					/////////////////////////////
					$data .= '<tr><td colspan="5">';
					$this->db->select('*');
					$this->db->from('ah_service_request_quotation_sub_items');						
					$this->db->where("qut_sub_id",$lc->qut_sub_id);
					$this->db->where("qut_id",$lc->qut_id);
					$this->db->where('delete_record','0');
					$rowuser1 = $this->db->get(); 
					$resultt =$rowuser1->result();
					if(count($resultt)>0){
					$data .= '<table class="table table-bordered table-striped "   >
                            <thead>
                              <tr role="row" style="color:#000 !important; background-color: #C8A4E6;">
								<th>Item Title</th> 
								<th>Quantity</th>
								<th>Cost</th>
								<th>Total Cost</th>
								<th>Description</th>
								<th>Action</th>
                              </tr>
                          </thead><tbody> ';
					foreach( $resultt as $us){
						$action1 ='';
					if($lc->userid == $this->_login_userid){
						$action1	.= ' <a href="#globalDiag" onclick="alatadad(this);" 
						 data-url="'.base_url().'request/additems/'.$lc->qut_id.'/'.$lc->qut_sub_id.'/'.$us->qut_item_id.'" style="margin-left:5px;" ><i class="icon-pencil"></i></a>';
						$action1 .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$us->qut_item_id.'" data-url="'.base_url().'request/delete_item/'.$us->qut_item_id.'"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';
					}
						
						$data .= '<tr role="row" style=" background-color: #fff;" >
								<td>'.$us->qut_item_title.'</td> 
								<td>'.$us->qut_item_quantity.'</td>
								<td>'.$us->qut_item_cost.'</td>
								<td>'.$us->qut_item_total.'</td>
								<td>'.$us->qut_item_description.'</td>
								<td>'.$action1.'</td>
                              </tr>';
					}
					 $data .= '
                            </tbody></table>';
					}
					$data .= '<td></td></td></tr>';
					
					
					/////////////////////////////////
				}
				
				
			}
			$data .=' </tbody>
                          </table>';
			
			$ex['data'] = $data;
			echo json_encode($ex);
	
	}
	function addsuppliers($qut_id,$qut_sub_id=NULL){
		$this->_data["qut_id"]= $qut_id;
		$this->_data["qut_sub_id"]= $qut_sub_id;
		if($qut_sub_id != ""){
				$this->_data['rows'] = $rows =  $this->request->getdata($qut_sub_id,'ah_service_request_quotation_sub','qut_sub_id');
		}
		if($this->input->post())
		{
			$data = $this->input->post();
			$buget["qut_id"] = $data["qut_id"];
			$buget["qut_sub_supplier_name"] = $data["qut_sub_supplier_name"];
			$buget["qut_sub_company"] = $data["qut_sub_company"];
			$buget["qut_sub_email"] = $data["qut_sub_email"];
			$buget["qut_sub_desc"] = $data["qut_sub_desc"];
			$buget["qut_sub_phone"] = $data["qut_sub_phone"];
			
			$buget["qut_date"] = date('Y-m-d');
			$buget["userid"] =  $this->_login_userid;
			if($data["qut_sub_id"] !=""){
				$buget["qut_sub_id"] = $data["qut_sub_id"];
			}
			
			
			 $qut_id = $this->request->savestore($buget,'ah_service_request_quotation_sub','qut_sub_id');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			//redirect(base_url().'request/add_quotation/'.$id.'/'.$qut_id);
			exit();
		}
		$this->load->view('addsuppliers',$this->_data);
	}
	function additems($qut_id,$qut_sub_id,$qut_item_id = NULL){
		$this->_data["qut_sub_id"]= $qut_sub_id;
		$this->_data["qut_id"]= $qut_id;
		$this->_data["qut_item_id"]= $qut_item_id;
		$this->_data['quotation_main'] = $service =  $this->request->getdata($qut_id,'ah_service_request_quotation_main','qut_id');
		if($qut_item_id != ""){
				$this->_data['rows'] = $rows =  $this->request->getdata($qut_item_id,'ah_service_request_quotation_sub_items','qut_item_id');
		}
		if($this->input->post())
		{
			$data = $this->input->post();
			$buget["qut_sub_id"] = $data["qut_sub_id"];
			$buget["qut_id"] = $data["qut_id"];
			$buget["req_id"] = $data["req_id"];
			$buget["qut_item_title"] = $data["qut_item_title"];
			$buget["qut_item_quantity"] = $data["qut_item_quantity"];
			$buget["qut_item_cost"] = $data["qut_item_cost"];
			$buget["qut_item_total"] = $data["qut_item_total"];
			$buget["qut_item_description"] = $data["qut_item_description"];
			if($data["qut_item_id"] != ""){
				$buget["qut_item_id"] = $data["qut_item_id"];
			}
			 $qut_id = $this->request->savestore($buget,'ah_service_request_quotation_sub_items','qut_item_id');
			$this->session->set_flashdata('msg','* ملاحظة : تم حفط المعاملة بنجاح');
			//redirect(base_url().'request/add_quotation/'.$id.'/'.$qut_id);
			exit();
		}
		$this->load->view('additems',$this->_data);
	}
	function viewsupplier($qut_sub_id){
		if($qut_sub_id != ""){
				$this->_data['rows'] = $rows =  $this->request->getdata($qut_sub_id,'ah_service_request_quotation_sub','qut_sub_id');
		}
		
		$this->load->view('viewsupplier',$this->_data);
	}
	function delete_suppliers($qut_sub_id){
		$rows1 =  $this->request->getdata($qut_sub_id,'ah_service_request_quotation_sub','qut_sub_id');
		$rows =  $this->request->getdata($rows1->qut_id,'ah_service_request_quotation_main','qut_id');
		$this->request->delete($qut_sub_id,'qut_sub_id',"ah_service_request_quotation_sub");
		$this->request->delete($qut_sub_id,'qut_sub_id',"ah_service_request_quotation_sub_items");
		//redirect(base_url().'request/add_quotation/'.$rows->req_id);
		exit();
	}
	function delete_item($qut_item_id){
		$this->request->delete($qut_item_id,'qut_item_id',"ah_service_request_quotation_sub_items");
		//redirect(base_url().'request/add_quotation/'.$rows->req_id);
		exit();
	}
	function submitquotation(){
		$id = $this->input->post('id');
		$buget["id"] = $id;
		$buget["pro_id"] = $this->_login_userid;
		$buget["pro_date"] = date('Y-m-d');
		$buget["pro_status"] = 'Quotation Submitted';
		 $qut_id = $this->request->savestore($buget,'ah_service_request','id');
		exit();
	}
	function approveQuotation($id){
		$this->_data["id"] = $id; 
		$this->_data['rows'] = $rows =  $this->request->getdata($id,'ah_service_request_quotation_main','req_id');
		$this->_data['req'] = $req =  $this->request->getdata($id,'ah_service_request','id');
		$this->load->view('approveQuotation',$this->_data);
	}
	function approveQuotaionsub(){
		$buget["id"] = $this->input->post('id');
		$buget["pro_suggest"] = $this->input->post('pro_suggest');
		$buget["pro_status_sub"]  = $this->input->post('pro_status_sub');
		$buget["pruchase_order_no"]  = $this->input->post('pruchase_order_no');
		if($buget["pro_status_sub"] == "accepted"){
			$buget["pro_status"]  = "Quotation Approved";
		}
		 $qut_id = $this->request->savestore($buget,'ah_service_request','id');
		exit();
	}
	function rejectQuotaion(){
		$buget["id"] = $this->input->post('id');
		$buget["pro_reason1"] = $this->input->post('pro_reason1');
		$buget["pro_reason2"]  = $this->input->post('pro_reason2');
		$buget["pro_status_sub"]  = $this->input->post('pro_status_sub');
		 $qut_id = $this->request->savestore($buget,'ah_service_request','id');
		exit();
	}
	
}
?>
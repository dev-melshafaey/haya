<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
        
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <i class="more-less glyphicon  glyphicon-minus "></i>
                        Quotation
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse  in" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                   <form method="POST" id="form_urgent" name="form_urgent" enctype="multipart/form-data">
            <input type="hidden" name="id" id="id" value="<?php echo $id?>"/>
             <input type="hidden" name="qut_id" id="qut_id" value="<?php echo $qut_id?>"/>
            <div class="row">
                 <div class="form-group col-md-4">
                      <label for="basic-input">Quotation Number‎</label>
                      <input type="text" name="qut_no" id="qut_no" class="form-control" value="<?php if(isset($rows->qut_no)){echo $rows->qut_no;}else{echo $qut_no;}?>" readonly  />
                 </div>
             	<div class="form-group col-md-4">
                      <label for="basic-input">Title‎</label>
                      <input type="text" name="qut_title" id="qut_title" class="form-control req" value="<?php echo $rows->qut_title;?>"  />
                 </div>
                 <div class="form-group col-md-4">
                      <label for="basic-input">Total Amount‎</label>
                      <input type="text" name="qut_totalamount" id="qut_totalamount" class="form-control req" value="<?php echo $service->totalcost; ?>" readonly  />
                 </div>
                   <div class="form-group col-md-12">
                          <label for="basic-input">Description‎</label>
                          <textarea name="qut_desc" id="qut_desc" class="form-control req" rows="10" ><?php echo $rows->qut_desc;?></textarea>
                     </div>
            </div>
              
            
            
            
            
            <br clear="all">
            <div class="form-group col-md-6">
              <button type="button" id="save_service" name="save_service" class="btn btn-success">حفظ</button>
            </div>
          </form>
                </div>
            </div>
        </div>
        <?php if(isset($rows->qut_id)){?>
		<div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="more-less glyphicon  <?php if($rows->qut_id > 0){ ?> glyphicon-minus<?php }else{?>glyphicon-plus<?php }?>"></i>
                       Add Suppliers
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse <?php if($rows->qut_id>0){ ?> in <?php }?>" role="tabpanel "  aria-labelledby="headingOne">
                <div class="panel-body">
                     <div class="row table-header-row" style=" margin-right: 0px;"><a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url()?>request/addsuppliers/<?php echo $rows->qut_id ?>" id="0" data-icon="images/menu/team_icon.png" data-heading="المصروفات">Add Suppliers</a> </div>
                        <div id="listallsuppliers"></div>
                         
                          
                     
                </div>
            </div>
        </div>
        
        <?php }?>
        <?php if(isset($rows->qut_id)){?>
        
  <div class="form-group  col-md-12">
  
    <input type="button" class="btn btn-success btn-lrg" name="submit Quotation"  id="save_expence" value="Submit Quotation" />
    <input type="button" class="btn btn-success btn-lrg" name="cancel"  id="save_expence1" value="Cancel" onClick="javascript:window.location.href=config.BASE_URL+'request/listall';" />
  </div>

    </div>
    <?php }?>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
 $(document).ready(function(){
	$('#save_service').click(function () {
		check_my_session();
        $('#form_urgent .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_urgent .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_urgent .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                $("#form_urgent").submit();
            
        }
        else 
		{
            show_notification_error_end(ht);
        }
    });
	$('#save_expence').click(function () {
          var id = '<?php echo $id;?>';
               $.ajax({
			url: config.BASE_URL+'request/submitquotation/',
			type: "POST",
			data:{'id':id,},
			dataType: "json",
			success: function(response)
			{	
				window.location.href=config.BASE_URL+'request/listall';
			 }
		});	
            
    });
	
});
</script>
<script>
 <?php if(isset($rows->qut_id)){?>
 loadallsuppliers('<?php echo $rows->qut_id;?>');
 <?php }?>
function loadallsuppliers(qut_id){
	$.ajax({
			url: config.BASE_URL+'request/loadallsuppliers/',
			type: "POST",
			data:{'qut_id':qut_id,},
			dataType: "json",
			success: function(response)
			{	
				$('#listallsuppliers').html(response.data);
			 }
		});	
}

</script>
<script>
function show_delete_diag(durar,id,image) {
	check_my_session();
    var url_redirect = $(durar).attr("data-url");
    var did = $(durar).attr('id');
    $('#delete_id').val(did);
    $('#action_url').val(url_redirect);
    $('#deleteDiag').modal();
	var Sfiles_old = $('#Sfiles_old').val();
	var Sfiles = Sfiles_old.split(',');
	var index = Sfiles.indexOf(image);
	if (index > -1) {
	 Sfiles.splice(index, 1);
	}
	Sfiles.toString();
	$('#Sfiles_old').val(Sfiles);
	$('#'+id).html('');
	location.reload();
}
function calculateTotal(){
	var cost = $('#cost').val();
	var quantity = $('#quantity').val();
	var totalcost = cost* quantity;
	 $('#totalcost').val(totalcost);
	
}
</script>
</div>
</body>
</html>
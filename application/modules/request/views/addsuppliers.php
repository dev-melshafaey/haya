<div class="row col-md-12">
  <form action="" method="POST" id="frm_expence" name="frm_expence" action="<?php echo base_url()?>request/addsuppliers/<?php echo $qut_id;?>">
  <input type="hidden" name="qut_id" name="qut_id" value="<?php echo $qut_id;?>" />
    <input type="hidden" name="qut_sub_id" name="qut_sub_id" value="<?php echo $qut_sub_id;?>" />
   
    <div class="form-group col-md-4">
      <label for="basic-input">Supplier Name</label>
      <input type="text" class="form-control req" value="<?php echo $rows->qut_sub_supplier_name;?>" placeholder="" name="qut_sub_supplier_name" id="qut_sub_supplier_name" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">Company Name</label>
      <input type="text" class="form-control req" value="<?php echo $rows->qut_sub_company;?>" placeholder="" name="qut_sub_company" id="qut_sub_company" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">Email</label>
      <input type="text" class="form-control req" value="<?php echo $rows->qut_sub_email;?>" placeholder="" name="qut_sub_email" id="qut_sub_email" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">Phone</label>
      <input type="text" class="form-control req" value="<?php echo $rows->qut_sub_phone;?>" placeholder="" name="qut_sub_phone" id="qut_sub_phone" />
    </div>
    
    
    
     <div class="form-group col-md-8">
      <label for="basic-input">Note</label>
      <textarea   class="form-control req"  placeholder="Note" name="qut_sub_desc" id="qut_sub_desc"><?php echo $rows->qut_sub_desc;?></textarea>
    </div>
    

  </form>
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="save_expence2" value="حفظ" />
  </div>
</div>
<script>
 $(document).ready(function(){
	$('#save_expence2').click(function () {
		check_my_session();
        $('#frm_expence .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_expence .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_expence .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                var str_data = 	$('#frm_expence').serialize();
		
			var request = $.ajax({
			  url: config.BASE_URL+'request/addsuppliers/<?php echo $qut_id;?>',
			  type: "POST",
			  data: str_data,
			  dataType: "html",
			  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
			  success: function(msg)
			  {		
			  
				 $('#ajax_action').hide();
				 $('#addingDiag').modal('hide');	  
				 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
				 loadallsuppliers('<?php echo $qut_id ?>');
			  }
			});
            
        }
        else 
		{
            show_notification_error(ht);
        }
    });
});
</script>
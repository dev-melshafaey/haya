<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block" style="padding: 10px 10px;">
          <form method="POST" id="form_urgent" name="form_urgent" enctype="multipart/form-data">
           <input type="hidden" name="id" id="id" value="<?php echo $rows->id?>"/>
            <div class="row">
                 <div class="form-group col-md-4">
                      <label for="basic-input">نوع الخدمة‎</label>
                      <input type="text" name="item" id="item" class="form-control req" value="<?php echo $rows->item?>"  />
                 </div>
             	 <!--<div class="form-group col-md-4">
                      <label for="basic-input">Department‎</label>
                      <?php //echo $this->haya_model->create_dropbox_list_help('department','departments',$rows->department,0,'req','','1','department'); ?> 
                 </div>-->
                <div class="form-group col-md-4">
                      <label for="basic-input">الحالة ‎</label>
                      <select  name="status" id="status" class="form-control req">
                      	<option value="0" <?php if($rows->status== "0"){?> selected <?php } ?> >عاجل</option>
                        <option value="1"  <?php if($rows->status== "1"){?> selected <?php } ?> >غير عاجل</option>
                       </select>
                 </div>
            </div>
             <div class="row">
              <div class="form-group col-md-4">
                      <label for="basic-input">الكمية‎</label>
                      <input type="number" name="quantity" id="quantity" class="form-control req" value="<?php echo $rows->quantity?>" onBlur="calculateTotal();"  />
                 </div>
             		<div class="form-group col-md-4">
                          <label for="basic-input">القيمة‎</label>
                          <input type="number" name="cost" id="cost" class="form-control req" value="<?php echo $rows->cost?>"  onBlur="calculateTotal();" />
                     </div>
                    <div class="form-group col-md-4">
                      <label for="basic-input">القيمة الإجمالية</label>
                      <input type="text" pattern="[0-9]*" name="totalcost" id="totalcost" class="form-control req" value="<?php echo number_format ($rows->totalcost,3,'.',',') ?>" />
                    </div>
                     <div class="form-group col-md-4">
                          <label for="basic-input">تحميل الملف‎</label>
                          <input type="file" name="Sfiles[]" id="Sfiles" class="form-control" multiple  />
                           <input type="hidden" name="Sfiles_old"  class="form-control " id="Sfiles_old" value="<?php echo $rows->Sfiles?>" />
                  <?php  if($rows->Sfiles != ""){
				   $i=0;
				   $files = @explode(',',$rows->Sfiles);
				   foreach($files as $att){
					   if($att !=""){
						   $i++;
				   ?>
                   <div id="att_<?php echo $i;?>" style="float:right;">
              <?php echo $controller->getfileicon($att,base_url().'resources/service_request/'.$login_userid);?>
              <a class="iconspace" href="#deleteDiag" onClick="show_delete_dialogs(this,'att_<?php echo $i;?>','<?php echo $att?>');" id="<?php echo  $rows->id ?>" data-url="<?php echo base_url()?>request/removeimage/<?php echo $id ?>/<?php echo $att ?>"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>
              </div>
              <?php } } }?>
                     </div>
                     
                      <div class="form-group col-md-8">
                      <label for="basic-input">تقديم مقترح</label>
                      <input type="text" name="suggest" id="suggest" class="form-control" value="<?php echo $rows->suggest?>"  />
                 </div>
             </div> 
            
            
            
            
            <br clear="all">
            <div class="form-group col-md-6">
              <button type="button" id="save_service" name="save_service" class="btn btn-success">حفظ</button>
            </div>
          </form>
          <br clear="all">
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script>
 $(document).ready(function(){
	$('#save_service').click(function () {
		check_my_session();
        $('#form_urgent .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_urgent .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_urgent .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                $("#form_urgent").submit();
            
        }
        else 
		{
            show_notification_error(ht);
        }
    });
});
</script>
<script>
function show_delete_dialogs(durar,id,image) {
	check_my_session();
    var url_redirect = $(durar).attr("data-url");
    var did = $(durar).attr('id');
    $('#delete_id').val(did);
    $('#action_url').val(url_redirect);
    $('#deleteDiag').modal();
	var Sfiles_old = $('#Sfiles_old').val();
	var Sfiles = Sfiles_old.split(',');
	var index = Sfiles.indexOf(image);
	if (index > -1) {
	 Sfiles.splice(index, 1);
	}
	Sfiles.toString();
	$('#Sfiles_old').val(Sfiles);
	$('#'+id).html('');
}
function calculateTotal(){
	var cost = $('#cost').val();
	var quantity = $('#quantity').val();
	var totalcost = cost* quantity;
	 $('#totalcost').val(totalcost);
	
}
</script>
</div>
</body>
</html>
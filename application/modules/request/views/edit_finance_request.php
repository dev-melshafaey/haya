<div class="row col-md-12">
<table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tbody><tr>
      <td colspan="3" class="customhr">Service Request Details:</td>
    </tr>
	<tr>
      <td class="right"><label class="text-warning">Item Request :</label><br><strong><?php echo $rows->item;?></strong></td>
       <td class="right"><label class="text-warning">Department :</label><br><strong><?php
	   $this->db->select('list_name');
				$this->db->from('ah_listmanagement');						
				//$this->db->where("list_type","departments");
				$this->db->where("list_id",$rows->department);
				$rowuser1 = $this->db->get(); 
				foreach($rowuser1->result() as $us){$department = $us->list_name;}
	   
	    echo $department;?></strong></td>
       <td class="right"><label class="text-warning">Browsing File‎ :</label><br><strong><?php  if($rows->Sfiles != ""){
				   $i=0;
				   $files = @explode(',',$rows->Sfiles);
				   foreach($files as $att){
					   if($att !=""){
						   $i++;
				   ?>
                   <div id="att_<?php echo $i;?>" style="float:right;">
              <?php echo $controller->getfileicon($att,base_url().'resources/service_request/'.$rows->userid);?>
             
              </div>
              <?php } } }?></strong></td>
     
    </tr> 
   <tr>
   <td class="right"><label class="text-warning">Quantity :</label><br><strong><?php echo $rows->quantity;?></strong></td>
   <td class="right"><label class="text-warning">Cost :</label><br><strong><?php echo 'OMR ' .number_format ($rows->cost,3,'.',',');?></strong></td>
    <td class="right"><label class="text-warning">Total Amount :</label><br><strong><?php echo 'OMR ' .number_format ($rows->totalcost,3,'.',',');?></strong></td>
   </tr>
   <tr>
   <td class="right"><label class="text-warning">Status :</label><br><strong><?php if($rows->status =="0"){ echo "Urgent";}else{echo "Not Urgent";}?></strong></td>
   <td class="right" colspan="2"><label class="text-warning">provider suggest :</label><br><strong><?php echo $rows->suggest; ?></strong></td>
   </tr>
     

</tbody></table>
<table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tbody><tr>
      <td colspan="3" class="customhr">Finance Approval Details:</td>
    </tr>
    </tbody>
</table>

<?php if($rows->fin_status != "accepted"){?>
<?php if($rows->finance_userid >0){?>
<table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tbody>
	<tr>
      <td class="right"><label class="text-warning">Accepted Finance  :</label><br><strong>
	  <?php
	  			$this->db->select('fullname');
				$this->db->from('ah_userprofile');						
				$this->db->where("userid",$rows->finance_userid);
				$rowuser1 = $this->db->get(); 
				foreach($rowuser1->result() as $us){$user = $us->fullname;}
				 echo $user;?></strong></td>
      <td class="right"><label class="text-warning">Process Status from Finance department :</label><br><strong><?php echo $rows->fin_status; ?></strong></td>
      <td class="right" ><label class="text-warning">Date :</label><br><strong><?php echo $rows->fin_date; ?></strong></td>
      <td class="right" ><label class="text-warning">Accepted Amount :</label><br><strong><?php echo 'OMR ' .number_format ($rows->currentArbitrage,3,'.',',');?></strong></td>
   </tr>
</tbody></table>  
<?php }?> 
<?php if($finance == $userroleid) {?>    
<div class="panel panel-default panel-block" style="padding: 10px 10px;">
          <form method="POST" id="form_urgent1" name="form_urgent1" enctype="multipart/form-data">
           <input type="hidden" name="id" id="id" value="<?php echo $rows->id?>"/>
            <div class="row">
                 <div class="form-group col-md-3">
                      <label for="basic-input">department</label>
                      <select  name="bds_title[]" id="bds_title" class="form-control req bds_title" onchange="loadsubdepartment(this.value);" >
                      <option value="">-Select-</option>
                      
                      <?php
					  $querym	=	$this->db->query("SELECT * FROM ah_budget_main
					  WHERE delete_record='0' ");
					   if($querym->num_rows() > 0)
						{
							foreach($querym->result() as $rws){
								
								echo '<option value="'.$rws->bd_id.'"  class="'.$rws->bd_remainingAmount.'">'.$rws->bd_year.'</option>';
								$query	=	$this->db->query("SELECT * FROM ah_budget_sub WHERE bd_id='".$rws->bd_id."' AND bds_categoryId='0'  AND delete_record='0' ");
								$n=1;
								do{
									if($query->num_rows() > 0)
									{
										foreach($query->result() as $rw){
											
											echo '<option value="'.$rw->bds_id.'" class="'.$rw->bds_remainingAmount.'">';
											for($i=0; $i<$n; $i++){
												echo '-';	
											}
											echo $rw->bds_title.'</option>';
										}
									}
									$query	=	$this->db->query("SELECT * FROM ah_budget_sub WHERE  bds_categoryId='".$rw->bds_id."' AND delete_record='0' ");
									$n++;
								 }while($query->num_rows()>0);
								
							}
						}
					  ?>
                      
                      
                      <?php  
					 
				?> 
                      </select> 
                 </div>
                 <div id="subdipartment">
                 </div>
                  <div class="form-group col-md-3">
                      <label for="basic-input">Total Amount‎</label>
                      <input type="text" pattern="[0-9]*" name="totalcost" id="totalcost" class="form-control" value="0.000" readonly="readonly" />
                    </div>
             	
             	 <div class="form-group col-md-3">
                      <label for="basic-input">current Arbitrage ‎</label>
                      <input type="number" name="currentcost" id="currentcost" class="form-control req" value="<?php echo number_format ($rows->totalcost,3,'.',',');?>"  />
                 </div>
             		<div class="form-group col-md-3">
                          <label for="basic-input">Remaining‎</label>
                          <input type="number" name="remaining" id="remaining" class="form-control req" value=""  />
                     </div>
                   
                     
                     
             </div> 
            
            
            
            
            <br clear="all">
            <div class="form-group col-md-6">
              <button type="button" id="submit_accepted" name="accepted" class="btn btn-success">accepted</button>
              <button type="button" id="submit_rejected" name="rejected" class="btn btn-success">rejected</button>
              <button type="button" id="submit_review" name="review" class="btn btn-success">review</button>
              <button type="button" id="submit_delay" name="delay" class="btn btn-success">delay</button>
              
            </div>
            </form>
             <form method="POST" id="form_urgent2" name="form_urgent2" enctype="multipart/form-data">
            <div class="form-group col-md-12" id="rejectedsection" style="display:none;">
            
              <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
                <tbody><tr>
                  <td colspan="3" class="customhr">Rejected Reason:</td>
                </tr>
                </tbody>
            </table>

              <div class="form-group col-md-4">
                  <label for="basic-input">Reason 1‎</label>
                  <textarea name="fin_reason1" id="fin_reason1" class="form-control req" ></textarea>
              </div>
              <div class="form-group col-md-4">
                  <label for="basic-input">Reason 2‎</label>
                  <textarea name="fin_reason2" id="fin_reason2" class="form-control" ></textarea>
              </div>
             <div class="form-group col-md-4">
              <button type="button" id="btn_submit" name="btn_submit" class="btn btn-success" style="margin-top: 30px;">Submit</button>
             </div>
            </div>
          </form>
          <br clear="all">
        </div> 
<?php } ?> 
<?php }else{ ?> 
<table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tbody><tr>
<tr>
      <td class="right"><label class="text-warning">Accepted Finance  :</label><br><strong>
	  <?php
	  			$this->db->select('fullname');
				$this->db->from('ah_userprofile');						
				$this->db->where("userid",$rows->finance_userid);
				$rowuser1 = $this->db->get(); 
				foreach($rowuser1->result() as $us){$user = $us->fullname;}
				 echo $user;?></strong></td>
      <td class="right"><label class="text-warning">Process Status from Finance department :</label><br><strong><?php echo $rows->fin_status; ?></strong></td>
      <td class="right" ><label class="text-warning">Date :</label><br><strong><?php echo $rows->fin_date; ?></strong></td>
      <td class="right" ><label class="text-warning">Accepted Amount :</label><br><strong><?php echo 'OMR ' .number_format ($rows->currentArbitrage,3,'.',',');?></strong></td>
   </tr>
   <?php if( $rows->fin_status == "rejected"){?>
   <tr>
   <td class="right" ><label class="text-warning">Reason 1 :</label><br><strong><?php echo $rows->fin_reason1; ?></strong></td>
   <td class="right" ><label class="text-warning">Reason 2 :</label><br><strong><?php echo $rows->fin_reason2; ?></strong></td>
   <td></td>
   </tr>
   <?php }?>
   </tbody>
</table>
<?php }?>
<table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tbody><tr>
      <td colspan="3" class="customhr">Management Approval Details:</td>
    </tr>
    </tbody>
</table>
<?php if($rows->mang_status != "accepted" && $rows->fin_status == "accepted"){?>
<?php if($rows->mang_id >0){?>
<table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tbody>
	<tr>
      <td class="right"><label class="text-warning">Accepted Finance  :</label><br><strong>
	  <?php
	  			$this->db->select('fullname');
				$this->db->from('ah_userprofile');						
				$this->db->where("userid",$rows->mang_id);
				$rowuser1 = $this->db->get(); 
				foreach($rowuser1->result() as $us){$user1 = $us->fullname;}
				 echo $user1;?></strong></td>
      <td class="right"><label class="text-warning">Process Status from Finance department :</label><br><strong><?php echo $rows->mang_status; ?></strong></td>
      <td class="right" ><label class="text-warning">Date :</label><br><strong><?php echo $rows->mang_date; ?></strong></td>
      
   </tr>
    <?php if( $rows->mang_status == "rejected"){?>
   <tr>
   <td class="right" ><label class="text-warning">Reason 1 :</label><br><strong><?php echo $rows->mang_reason1; ?></strong></td>
   <td class="right" ><label class="text-warning">Reason 2 :</label><br><strong><?php echo $rows->mang_reason2; ?></strong></td>
   <td></td>
   </tr>
   <?php }?>
</tbody></table>  
<?php }?> 
<?php if($management == $userroleid) {?> 
<?php if($rows->fin_status == "accepted"){ ?>   
<div class="panel panel-default panel-block" style="padding: 10px 10px;">
          <form method="POST" id="form_urgent_man" name="form_urgent_man" enctype="multipart/form-data">
           <input type="hidden" name="ids" id="ids" value="<?php echo $rows->id?>"/>
             
            
            
            
            
            <div class="form-group col-md-6">
              <button type="button" id="submit_accepted1" name="submit_accepted1" class="btn btn-success">accepted</button>
              <button type="button" id="submit_rejected1" name="submit_rejected1" class="btn btn-success">rejected</button>
              <button type="button" id="submit_review1" name="submit_review1" class="btn btn-success">review</button>
              <button type="button" id="submit_delay1" name="submit_delay1" class="btn btn-success">delay</button>
              
            </div>
            </form>
             <form method="POST" id="form_urgent3" name="form_urgent3" enctype="multipart/form-data">
            <div class="form-group col-md-12" id="rejectedsection1" style="display:none;">
            
              <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
                <tbody><tr>
                  <td colspan="3" class="customhr">Rejected Reason:</td>
                </tr>
                </tbody>
            </table>

              <div class="form-group col-md-4">
                  <label for="basic-input">Reason 1‎</label>
                  <textarea name="mang_reason1" id="mang_reason1" class="form-control req" ></textarea>
              </div>
              <div class="form-group col-md-4">
                  <label for="basic-input">Reason 2‎</label>
                  <textarea name="mang_reason2" id="mang_reason2" class="form-control" ></textarea>
              </div>
             <div class="form-group col-md-4">
              <button type="button" id="btn_submit1" name="btn_submit1" class="btn btn-success" style="margin-top: 30px;">Submit</button>
             </div>
            </div>
          </form>
          <br clear="all">
        </div> 
<?php } ?>
<?php } ?>
<?php }
else{ ?> 
<table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tbody><tr>
<tr>
      <td class="right"><label class="text-warning">Approved By  :</label><br><strong>
	  <?php
	  			$this->db->select('fullname');
				$this->db->from('ah_userprofile');						
				$this->db->where("userid",$rows->mang_id);
				$rowuser1 = $this->db->get(); 
				foreach($rowuser1->result() as $us){$user = $us->fullname;}
				 echo $user;?></strong></td>
      <td class="right"><label class="text-warning">Process Status  :</label><br><strong><?php echo $rows->mang_status; ?></strong></td>
      <td class="right" ><label class="text-warning">Date :</label><br><strong><?php echo $rows->mang_date; ?></strong></td>
      <td class="right" ><label class="text-warning">Accepted Amount :</label><br><strong><?php echo 'OMR ' .number_format ($rows->currentArbitrage,3,'.',',');?></strong></td>
   </tr>
   <?php if( $rows->mang_status == "rejected"){?>
   <tr>
   <td class="right" ><label class="text-warning">Reason 1 :</label><br><strong><?php echo $rows->mang_reason1; ?></strong></td>
   <td class="right" ><label class="text-warning">Reason 2 :</label><br><strong><?php echo $rows->mang_reason2; ?></strong></td>
   <td></td>
   </tr>
   <?php }?>
   </tbody>
</table>
<?php }?>


<div class="row" style="text-align: center;">
  <button type="button" id="" onClick="printthepage('user-profile');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
</div>
</div>
<script type="text/javascript">
function submitForm(fin_status){
	var request = $.ajax({
	  url: config.BASE_URL+'request/update_finance',
	  type: "POST",
	  data: {'id':$('#id').val(),'budget_sub_id':$('#bds_title').val(),'currentArbitrage':$('#currentcost').val(),'fin_reason1':$('#fin_reason1').val(), 'fin_reason2':$('#fin_reason2').val(),'fin_status':fin_status},
	  dataType: "html",
	  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
	  success: function(msg)
	  {		
		 $('#ajax_action').hide();
		
		 $('#addingDiag').modal('hide');	  
		 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
	  }
	});
}
function submitForm1(mang_status){
	var request = $.ajax({
	  url: config.BASE_URL+'request/update_management',
	  type: "POST",
	  data: {'id':$('#ids').val(),'mang_reason1':$('#mang_reason1').val(), 'mang_reason2':$('#mang_reason2').val(),'mang_status':mang_status},
	  dataType: "html",
	  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
	  success: function(msg)
	  {		
		 $('#ajax_action').hide();
		
		 $('#addingDiag').modal('hide');	  
		 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
	  }
	});
}

$(document).ready(function(){
	$('#submit_accepted').click(function () {
		submitForm('accepted');
		
	});
	$('#submit_rejected').click(function () {
		$('#rejectedsection').show('slow');
		//submitForm('rejected');
	});
	$('#submit_review').click(function () {
		submitForm('review');
	});
	$('#submit_delay').click(function () {
		submitForm('delay');
	});
	$('#btn_submit').click(function () {
		
		check_my_session();
        $('#form_urgent2 .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_urgent2 .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_urgent2 .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                submitForm('rejected');
            
        }
        else 
		{
            show_notification_error(ht);
        }
	});
	////////////////management
	$('#submit_accepted1').click(function () {
		submitForm1('accepted');
		
	});
	$('#submit_rejected1').click(function () {
		$('#rejectedsection1').show('slow');
		//submitForm('rejected');
	});
	$('#submit_review1').click(function () {
		submitForm1('review');
	});
	$('#submit_delay1').click(function () {
		submitForm1('delay');
	});
	$('#btn_submit1').click(function () {
		
		check_my_session();
        $('#form_urgent3 .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_urgent3 .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_urgent3 .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                submitForm1('rejected');
            
        }
        else 
		{
            show_notification_error(ht);
        }
	});
	
	
});
$(".bds_title").change(function() {
  var tot = $(this).children(":selected").attr("class");
  tot = parseFloat(tot);
  $('#totalcost').val(tot);
  var currentcost = $('#currentcost').val();
  var rem = tot-currentcost;
   $('#remaining').val(rem);
});
</script>
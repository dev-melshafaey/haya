<div class="row col-md-12">
<?php  if($req->pro_status == "Quotation Submitted"){ ?>  
<table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tbody><tr>
      <td colspan="3" class="customhr">Approve Quotation:</td>
    </tr>
	<tr>
      <td class="right"><label class="text-warning">Quotation Number :</label><br><strong><?php echo $rows->qut_no;?></strong></td>
       <td class="right"><label class="text-warning">Total Amount :</label><br><strong><?php echo $rows->qut_totalamount;?></strong></td>
      
     
    </tr> 
  
   <tr>
   <td class="right" colspan="2"><label class="text-warning">provider suggest :</label><br><strong>
   <textarea name="pro_suggest"  id="pro_suggest" ><?php echo $req->pro_suggest; ?></textarea></strong></td>
   </tr>
    <tr>
   <td class="right" colspan="2"><label class="text-warning">puchase Order N0 :</label><br><strong>
   <input type="text" name="pruchase_order_no"  id="pruchase_order_no" value="<?php echo $req->pruchase_order_no; ?>"  class="form-control req"/></strong></td>
   </tr> 

</tbody></table>



 
<div class="panel panel-default panel-block" style="padding: 10px 10px;">
          <form method="POST" id="form_urgent_man" name="form_urgent_man" enctype="multipart/form-data">
           <input type="hidden" name="id" id="id" value="<?php echo $id?>"/>
             
            
            
            
            
            <div class="form-group col-md-6">
              <button type="button" id="submit_accepted" name="submit_accepted" class="btn btn-success">accepted</button>
              <button type="button" id="submit_rejected" name="submit_rejected" class="btn btn-success">rejected</button>
              <button type="button" id="submit_review" name="submit_review" class="btn btn-success">review</button>
              <button type="button" id="submit_delay" name="submit_delay" class="btn btn-success">delay</button>
              
            </div>
            </form>
             <form method="POST" id="form_urgent3" name="form_urgent3" enctype="multipart/form-data">
            <div class="form-group col-md-12" id="rejectedsection1" style="display:none;">
            
              <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
                <tbody><tr>
                  <td colspan="3" class="customhr">Rejected Reason:</td>
                </tr>
                </tbody>
            </table>

              <div class="form-group col-md-4">
                  <label for="basic-input">Reason 1‎</label>
                  <textarea name="pro_reason1" id="pro_reason1" class="form-control req" ></textarea>
              </div>
              <div class="form-group col-md-4">
                  <label for="basic-input">Reason 2‎</label>
                  <textarea name="pro_reason2" id="pro_reason2" class="form-control" ></textarea>
              </div>
             <div class="form-group col-md-4">
              <button type="button" id="btn_submit1" name="btn_submit1" class="btn btn-success" style="margin-top: 30px;">Submit</button>
             </div>
            </div>
          </form>
          <br clear="all">
        </div> 
<?php } ?>



<div class="row" style="text-align: center;">
  <button type="button" id="" onClick="printthepage('user-profile');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
</div>
</div>
<script type="text/javascript">
function submitForm(pro_status){
	var request = $.ajax({
	  url: config.BASE_URL+'request/approveQuotaionsub',
	  type: "POST",
	  data: {'id':$('#id').val(),'pro_suggest':$('#pro_suggest').val(),'pro_status_sub':pro_status,'pruchase_order_no':$('#pruchase_order_no').val()},
	  dataType: "html",
	  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
	  success: function(msg)
	  {		
		 $('#ajax_action').hide();
		console.log(msg);
		
		 $('#addingDiag').modal('hide');	  
		 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
		 window.location.reload();
	  }
	});
}
function submitForm1(mang_status){
	var request = $.ajax({
	  url: config.BASE_URL+'request/rejectQuotaion',
	  type: "POST",
	  data: {'id':$('#id').val(),'pro_reason1':$('#pro_reason1').val(), 'pro_reason2':$('#pro_reason2').val(),'pro_status_sub':mang_status},
	  dataType: "html",
	  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
	  success: function(msg)
	  {		
		 $('#ajax_action').hide();
		console.log(msg);
		
		 $('#addingDiag').modal('hide');	  
		 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
	  }
	});
}

$(document).ready(function(){
	$('#submit_accepted').click(function () {
		submitForm('accepted');
		
	});
	$('#submit_rejected').click(function () {
		$('#rejectedsection').show('slow');
		//submitForm('rejected');
	});
	$('#submit_review').click(function () {
		submitForm('review');
	});
	$('#submit_delay').click(function () {
		submitForm('delay');
	});
	$('#btn_submit').click(function () {
		
		check_my_session();
        $('#form_urgent2 .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_urgent2 .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_urgent2 .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                submitForm('rejected');
            
        }
        else 
		{
            show_notification_error(ht);
        }
	});
	////////////////management
	$('#submit_accepted1').click(function () {
		submitForm1('accepted');
		
	});
	$('#submit_rejected1').click(function () {
		$('#rejectedsection1').show('slow');
		//submitForm('rejected');
	});
	$('#submit_review1').click(function () {
		submitForm1('review');
	});
	$('#submit_delay1').click(function () {
		submitForm1('delay');
	});
	$('#btn_submit1').click(function () {
		
		check_my_session();
        $('#form_urgent3 .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#form_urgent3 .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#form_urgent3 .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                submitForm1('rejected');
            
        }
        else 
		{
            show_notification_error(ht);
        }
	});
	
	
});
$("#bds_title").change(function() {
  var tot = $(this).children(":selected").attr("class");
  tot = parseFloat(tot);
  $('#totalcost').val(tot);
  var currentcost = $('#currentcost').val();
  var rem = tot-currentcost;
   $('#remaining').val(rem);
});
</script>
<div class="row col-md-12">
  <form action="" method="POST" id="frm_expence" name="frm_expence" action="<?php echo base_url()?>request/additems/<?php echo $qut_id;?>/<?php echo $qut_sub_id;?>">
  <input type="hidden" name="qut_sub_id" name="qut_sub_id" value="<?php echo $qut_sub_id;?>" />
   <input type="hidden" name="qut_id" name="qut_id" value="<?php echo $qut_id;?>" />
    <input type="hidden" name="req_id" name="req_id" value="<?php echo $quotation_main->req_id;?>" />
    <input type="hidden" name="qut_item_id" name="qut_item_id" value="<?php echo $qut_item_id;?>" />
    <div class="form-group col-md-4">
      <label for="basic-input">Item Title</label>
      <input type="text" class="form-control req" value="<?php echo $rows->qut_item_title;?>" placeholder="" name="qut_item_title" id="qut_item_title" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">Quantity</label>
      <input type="number" class="form-control req" value="<?php echo $rows->qut_item_quantity;?>" placeholder="" name="qut_item_quantity" id="qut_item_quantity" onkeyup="calculateTotal();" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">Cost</label>
      <input type="text" class="form-control req" value="<?php echo $rows->qut_item_cost;?>" placeholder="" name="qut_item_cost" id="qut_item_cost" onkeyup="calculateTotal();" />
    </div>
    <div class="form-group col-md-4">
      <label for="basic-input">Total Cost</label>
      <input type="text" class="form-control req" value="<?php echo $rows->qut_item_total;?>" placeholder="" name="qut_item_total" id="qut_item_total" />
    </div>
    
    
    
     <div class="form-group col-md-8">
      <label for="basic-input">Note</label>
      <textarea   class="form-control req"  placeholder="Note" name="qut_item_description" id="qut_item_description"><?php echo $rows->qut_item_description;?></textarea>
    </div>
    

  </form>
</div>
<div class="row col-md-12">
  <div class="form-group  col-md-12">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="save_expence5" value="حفظ" />
  </div>
</div>
<script>
 $(document).ready(function(){
	$('#save_expence5').click(function () {
		check_my_session();
        $('#frm_expence .req').removeClass('parsley-error');
        var ht = '<ul>';
        $('#frm_expence .req').each(function (index, element) {
            if ($(this).val() == '') {
                $(this).addClass('parsley-error');
                ht += '<li> طلب ' + $(this).attr('placeholder') + '</li>';
            }
        });
        var redline = $('#frm_expence .parsley-error').length;

        ht += '</ul>';
		
        if (redline <= 0) {           
                var str_data = 	$('#frm_expence').serialize();
		
			var request = $.ajax({
			  url: config.BASE_URL+'request/additems/<?php echo $qut_id;?>/<?php echo $qut_sub_id;?>',
			  type: "POST",
			  data: str_data,
			  dataType: "html",
			  beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
			  success: function(msg)
			  {		
			  
				 $('#ajax_action').hide();
				 $('#addingDiag').modal('hide');	  
				 show_notification('تمت إضافة البيانات الخاصة بك بنجاح');
				 loadallsuppliers('<?php echo $qut_id ?>');
			  }
			});
            
        }
        else 
		{
            show_notification_error(ht);
        }
    });
});
function calculateTotal(){
	var cost = $('#qut_item_cost').val();
	var quantity = $('#qut_item_quantity').val();
	var totalcost = cost* quantity;
	 $('#qut_item_total').val(totalcost);
	
}

</script>
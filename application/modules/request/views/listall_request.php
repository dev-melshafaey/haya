<?php $permissions	=	$this->haya_model->check_other_permission(array($module['moduleid']));?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
       <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
            <div class="col-md-12">
                <div style="padding: 22px 20px !important; background:#c1dfc9;">
                    <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
                </div>
              </div>
       <?php endif;?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <div class="row table-header-row"> <a class="btn btn-success" style="float:right;" href="<?php echo base_url();?>request/add_request" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> 
                  <?php if($finance == $userroleid || $management == $userroleid){?>
                   <a  style="float:right; margin-right:100px; color:#029625; font-weight:bold;" href="<?php echo base_url();?>request/listall/" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">قائمة جديدة</a> 
                   <a  style="float:right; margin-right:100px; color:#029625; font-weight:bold;" href="<?php echo base_url();?>request/listall/mylist" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">قائمتي</a> 
                  <a  style="float:right; margin-right:100px; color:#029625; font-weight:bold;" href="<?php echo base_url();?>request/listall/accepted" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">قائمة الموافقة</a> 
                  <a  style="float:right; margin-right:100px;  color:#029625; font-weight:bold;" href="<?php echo base_url();?>request/listall/rejected" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">قائمة الرفض</a> 
                  <a  style="float:right; margin-right:100px;  color:#029625; font-weight:bold;" href="<?php echo base_url();?>request/listall/review" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">قائمة إعادة النظر</a>
                  <a  style="float:right; margin-right:100px;  color:#029625; font-weight:bold;" href="<?php echo base_url();?>request/listall/delay" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">القائمة المتأخرة</a>
                  <?php } ?>
                  
                  </div>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                     	 <th>الأسم</th>
                        <th>الدائرة</th>
                        <th>التاريخ</th>
                        <th>المبلغ</th>
                        <th>الحالة</th>
                        <th>الإجراءات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'request/ajax_all_listall_request/'.$status,'columns_array'=>'{ "data": "الأسم" },{ "data": "الدائرة" },{ "data": "التاريخ" },{ "data": "المبلغ" },{ "data": "الحالة" },
				{ "data": "الإجراءات" }')); ?>
</div>
</body>
</html>



</script>

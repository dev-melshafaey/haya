<div class="row col-md-12">
<table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
    <tbody><tr>
      <td colspan="3" class="customhr">Service Request:</td>
    </tr>
	<tr>
      <td class="right"><label class="text-warning">Item Request :</label><br><strong><?php echo $rows->item;?></strong></td>
       <td class="right"><label class="text-warning">Department :</label><br><strong><?php
	   $this->db->select('list_name');
				$this->db->from('ah_listmanagement');						
				$this->db->where("list_type","departments");
				$this->db->where("list_id",$rows->department);
				$rowuser1 = $this->db->get(); 
				foreach($rowuser1->result() as $us){$department = $us->list_name;}
	   
	    echo $department;?></strong></td>
       <td class="right"><label class="text-warning">Browsing File‎ :</label><br><strong><?php  if($rows->Sfiles != ""){
				   $i=0;
				   $files = @explode(',',$rows->Sfiles);
				   foreach($files as $att){
					   if($att !=""){
						   $i++;
				   ?>
                   <div id="att_<?php echo $i;?>" style="float:right;">
              <?php echo $controller->getfileicon($att,base_url().'resources/service_request/'.$login_userid);?>
             
              </div>
              <?php } } }?></strong></td>
     
    </tr> 
   <tr>
   <td class="right"><label class="text-warning">Quantity :</label><br><strong><?php echo $rows->quantity;?></strong></td>
   <td class="right"><label class="text-warning">Cost :</label><br><strong><?php echo 'OMR ' .number_format ($rows->cost,3,'.',',');?></strong></td>
    <td class="right"><label class="text-warning">Total Amount :</label><br><strong><?php echo 'OMR ' .number_format ($rows->totalcost,3,'.',',');?></strong></td>
   </tr>
   <tr>
   <td class="right"><label class="text-warning">Status :</label><br><strong><?php if($rows->status =="0"){ echo "Urgent";}else{echo "Not Urgent";}?></strong></td>
   <td class="right" colspan="2"><label class="text-warning">provider suggest :</label><br><strong><?php echo $rows->suggest; ?></strong></td>
   </tr>
<?php if($rows->finance_userid >0){?>
    <tr>
      <td colspan="4" class="customhr">Finance Approval Details:</td>
    </tr>
	<tr>
      <td class="right"><label class="text-warning">Employee name   :</label><br><strong>
	  <?php
	  			$this->db->select('fullname');
				$this->db->from('ah_userprofile');						
				$this->db->where("userid",$rows->finance_userid);
				$rowuser1 = $this->db->get(); 
				foreach($rowuser1->result() as $us){$user = $us->fullname;}
				 echo $user;?></strong></td>
      <td class="right"><label class="text-warning">Process Status from Finance department :</label><br><strong><?php echo $rows->fin_status; ?></strong></td>
      <td class="right" ><label class="text-warning">Date :</label><br><strong><?php echo $rows->fin_date; ?></strong></td>
      <td class="right" ><label class="text-warning">Accepted Amount :</label><br><strong><?php echo 'OMR ' .number_format ($rows->currentArbitrage,3,'.',',');?></strong></td>
   </tr>
    <?php if( $rows->fin_status == "rejected"){?>
   <tr>
   <td class="right" ><label class="text-warning">Reason 1 :</label><br><strong><?php echo $rows->fin_reason1; ?></strong></td>
   <td class="right" ><label class="text-warning">Reason 2 :</label><br><strong><?php echo $rows->fin_reason2; ?></strong></td>
   <td></td>
   </tr>
   <?php }?>
<?php }
else{ ?>
 <tr>
      <td colspan="4" class="customhr">Finance Approval Details:</td>
    </tr>
 <tr>
      <td colspan="4" class="right" style="color:#F00; font-weight:bold;" >Finance Approval pending.</td>
    </tr>	
	
<?php }?> 


<?php if($rows->mang_id >0){?>
    <tr>
      <td colspan="4" class="customhr">Management Approval Details:</td>
    </tr>
	<tr>
      <td class="right"><label class="text-warning">Employee name  :</label><br><strong>
	  <?php
	  			$this->db->select('fullname');
				$this->db->from('ah_userprofile');						
				$this->db->where("userid",$rows->mang_id);
				$rowuser1 = $this->db->get(); 
				foreach($rowuser1->result() as $us){$user = $us->fullname;}
				 echo $user;?></strong></td>
      <td class="right"><label class="text-warning">Process Status  :</label><br><strong><?php echo $rows->mang_status; ?></strong></td>
      <td class="right" ><label class="text-warning">Date :</label><br><strong><?php echo $rows->mang_date; ?></strong></td>
      <td class="right" ><label class="text-warning">Accepted Amount :</label><br><strong><?php echo 'OMR ' .number_format ($rows->currentArbitrage,3,'.',',');?></strong></td>
   </tr>
    <?php if( $rows->mang_status == "rejected"){?>
   <tr>
   <td class="right" ><label class="text-warning">Reason 1 :</label><br><strong><?php echo $rows->mang_reason1; ?></strong></td>
   <td class="right" ><label class="text-warning">Reason 2 :</label><br><strong><?php echo $rows->mang_reason2; ?></strong></td>
   <td></td><td></td>
   </tr>
   <?php }?>
<?php }
else{ ?>
 <tr>
      <td colspan="4" class="customhr">Management Approval Details:</td>
    </tr>
 <tr>
      <td colspan="4" class="right" style="color:#F00; font-weight:bold;" >Management Approval pending.</td>
    </tr>	
	
<?php }?> 
<?php if($rows->pro_id >0){?>
    <tr>
      <td colspan="4" class="customhr">Procurement Approval Details:</td>
    </tr>
    <tr>
      <td class="right"><label class="text-warning">Employee name   :</label><br><strong>
	  <?php
	  			$this->db->select('fullname');
				$this->db->from('ah_userprofile');						
				$this->db->where("userid",$rows->pro_id);
				$rowuser1 = $this->db->get(); 
				foreach($rowuser1->result() as $us){$user = $us->fullname;}
				 echo $user;?></strong></td>
      <td class="right" ><label class="text-warning">Purchase Order No  :</label><br><strong><?php echo $rows->pruchase_order_no; ?></strong></td>
      <td class="right" ><label class="text-warning">Process Status  :</label><br><strong><?php echo $rows->pro_status; ?></strong></td>
      
      <td class="right" ><label class="text-warning">Date :</label><br><strong><?php echo $rows->pro_date; ?></strong></td>
   </tr>
   <tr>
      <td class="right" colspan="4"><label class="text-warning">Suggestion   :</label><br><strong><?php echo $rows->pro_suggest; ?></strong></td>
   </tr>
    <?php if( $rows->pro_status == "rejected"){?>
   <tr>
   <td class="right" ><label class="text-warning">Reason 1 :</label><br><strong><?php echo $rows->pro_reason1; ?></strong></td>
   <td class="right" ><label class="text-warning">Reason 2 :</label><br><strong><?php echo $rows->pro_reason2; ?></strong></td>
   <td></td><td></td>
   </tr>
   <?php }?>
<?php }else{?> 
 <tr>
      <td colspan="4" class="customhr">Procurement Approval Details:</td>
    </tr>
 <tr>
      <td colspan="4" class="right" style="color:#F00; font-weight:bold;" >Procurement Approval pending.</td>
    </tr>	

<?php } ?>   
</tbody></table> 
<div class="row" style="text-align: center;">
  <button type="button" id="" onClick="printthepage('user-profile');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
</div>
</div>

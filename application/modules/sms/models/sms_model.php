<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sms_model extends CI_Model
{
	/*
	*  Properties
	*/
	private $_table_users;
	private $_table_branchs;
	private $_table_listmanagement;
	private $_table_userprofile;
	private $_table_sms_management;
//-------------------------------------------------------------------

	/*
	 *  Constructor
	 */
	function __construct()
	{
		parent::__construct();

		//Get Table Names from Config 
		$this->_table_users 				= 	$this->config->item('table_users');
		$this->_table_branchs 				= 	$this->config->item('table_branchs');
		$this->_table_listmanagement 		= 	$this->config->item('table_listmanagement');
		$this->_table_userprofile 			= 	$this->config->item('table_userprofile');
		$this->_table_sms_management 		=  $this->config->item('table_sms_management');
	}
//-------------------------------------------------------------------
	/*
	* Search Applied Products if 
	* Return True 
	*/
	public function getInquiriesSms($type)
	{
		$this->db->where('sms_module','inquiry');
		$this->db->where('type',$type);
		$this->db->where('status',1);
		$this->db->select('*');
		$this->db->order_by('sms_order','ASC');
		
		$query = $this->db->get($this->_table_sms_management);
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	* Search Applied Products if 
	* Return True 
	*/
	public function sms_modal_info($tempid)
	{
		$query = $this->db->query("SELECT 
				`main`.`tempid` as hd_id,
				 CONCAT(`main_applicant`.`first_name`,' ',`main_applicant`.`middle_name`,' ',`main_applicant`.`last_name`,' ',`main_applicant`.`family_name`) as hd_name,
				`main`.`user_type` AS hd_type,
				`main_applicant`.`idcard` AS hd_idcard,
				`main_applicant`.`cr_number` AS hd_crnumber,
				 GROUP_CONCAT(`main_phone`.`phonenumber`) AS hd_phone FROM `main_applicant` INNER JOIN `main` ON (`main_applicant`.`tempid` = `main`.`tempid`) INNER JOIN `main_phone` ON (`main_phone`.`applicantid` = `main_applicant`.`applicantid`) WHERE `main`.`tempid`='".$tempid."' AND `main_applicant`.`idcard`!=0");
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}	
	

//-------------------------------------------------------------------
	/*
	 * 
	 * 
	 */
	public function update_db($smsid,$data)
	{
		$json_data	=	json_encode(array('data'=>$data));
		
		$this->db->where('sms_id',$smsid);
		$this->db->update('sms_management',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	 * 
	 * 
	 */
	public function addupdate_sms_template($templateid,$data)
	{
		if($templateid!='')
		{
			$json_data	=	json_encode(array('data'=>$data));
			
			$this->db->where('templateid',$templateid);
			$this->db->update('system_sms_template',$json_data,$this->session->userdata('userid'),$data);
		}
		else
		{
			$json_data	=	json_encode(array('data'=>$data));
			$this->db->insert('system_sms_template',$data,$json_data,$this->session->userdata('userid'));
		}
		
		return TRUE;
	}
	
//-------------------------------------------------------------------
	/*
	 * 
	 * 
	 */	
	public function smstemplate_ajax()
	{
		$conut =	0;
		$this->db->select('templateid,templatesubject,template,tempatedate,templatestatus');
		$this->db->where('delete_record','0');
		$this->db->order_by('templatesubject','ASC');
		$qx = $this->db->get('system_sms_template');

		foreach($qx->result() as $smsresult)
		{
			if($this->haya_model->check_other_permission(99,'u')==1)
			{
				$actions .='<a href="#1" id="'.$smsresult->templateid.'" onClick="add_update_sms_template(this);"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
			}
            
			if($this->haya_model->check_other_permission(99,'d')==1)
			{
				$actions .=' <a href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$smsresult->templateid.'" data-url="'.base_url().'sms/templatedelete/'.$smsresult->templateid.'"><i style="color:#F00;" class="icon-remove-circle"></i></a>';
			}
                
			$arr[] = array(
				"DT_RowId"		=>	$smsresult->templateid.'_durar_lm',				
				"عنوان الرسئل" 	=>	$smsresult->templatesubject,              
				"تاريخ" 		=>	arabic_date($smsresult->tempatedate),
				"عدد الرسالة" 	=>	arabic_date(strlen($smsresult->template)),
				"الإجراءات" 		=>	$actions
				);
				
			unset($actions);
		}
		
		$ex['data'] = $arr;
		
		echo json_encode($ex);
	}
//-------------------------------------------------------------------
	/*
	* 
	* 
	*/	
	public function templatedelete($tempid)
	{
		$json_data	=	json_encode(array('record'=>'delete','templateid'	=>	$tempid));
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('templateid', $tempid);
		$this->db->update('system_sms_template',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-----------------------------------------------------------------------

	/*
	*
	*
	*
	*/
	public function get_bulksms()
	{
		$this->db->select('sms_id,sms_order,status,sms_heading,sms_type,sms_value,type,sms_module,sms_remider,sms_reminder_counter,sms_register_count,sms_register');
		$query	=	$this->db->get('sms_management');
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-----------------------------------------------------------------------

	/*
	*
	*	Get Single SMS Template
	*	@param $templateid int
	*	return OBJECT
	*/
	public function get_single_sms_template($templateid)
	{
		$this->db->select('templateid,addedby,templatesubject,template,tempatedate,templatestatus,delete_record');
		$this->db->where('templateid', $templateid);
		$query	=	$this->db->get('system_sms_template');
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
/*-------------------------------------------------------------------------*/		
/* End of file sms_model.php */
/* Location: ./appliction/modules/sms/sms_model.php */
}
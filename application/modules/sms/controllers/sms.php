<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms extends CI_Controller {

	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;

//-----------------------------------------------------------------------

	/*
	* Constructor
	*/
	function __construct()
	{
		 parent::__construct();	
	
		// Loade Admin Model
		$this->load->model('sms_model','sms');
		
		
		$this->_data['module']			=	$this->haya_model->get_module();	
		
		// SET Login USER ID
		$this->_login_userid			=	$this->session->userdata('userid');
			
		$this->_data['login_userid']	=	$this->_login_userid;
		$this->_data['user_detail'] 	=	$this->haya_model->get_user_detail($this->_login_userid);
					
		// Load all types
		$this->_data['list_types']		=	$this->haya_model->get_listmanagment_types();
	}
//-----------------------------------------------------------------------

	/*
	* Home Page
	*/
	public function index()
	{
		// Load Users Listing Page
		$this->load->view('users-listing',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	* Inquiry SMS Form
	*/
	public function getInquirysms($type = 'sms')
	{
		$this->_data['type']	=	$type;
		
		$inqSms = $this->sms->getInquiriesSms($type);
		
		//For Saving
		if($this->input->post('submit'))
		{
			$form_data		=	$this->input->post();
			
			unset($form_data['submit']);
			
			$firstData	=	array();
			$count		=	count($this->sms->getInquiriesSms($type));
			
			for($i=1;$i<=$count;$i++)
			{
				$sms_reminder	=	'sms_reminder_type_'.$i;
				$reminder_count	=	'reminder_count_'.$i;
				$sms_value		=	'sms_value_'.$i;
				$sms_id			=	'sms_id_'.$i;

				$firstData['sms_remider'] 			= $form_data[$sms_reminder];
				$firstData['sms_reminder_counter'] 	= $form_data[$reminder_count];
				$firstData['sms_value'] 			= $form_data[$sms_value];
				$firstData['type'] 					= $form_data['type'];
				
				$condition 	=	array('sms_id'=>$form_data[$sms_id]);
				
				$return		=	$this->sms->update_db($form_data[$sms_id],$firstData);
			}
			
			if($return)
			{
				$this->session->set_flashdata('msg', 'S');	
			}
			else
			{
				$this->session->set_flashdata('msg', 'E');	
			}
			
			// UNSET ARRAY key
			$this->_data['page'] = 'addinqsms';
			redirect('sms/getInquirysms/'.$type);
		}
		
		$this->_data['inq_info_sms']	=	$inqSms;
		
		// View Inq Type Messages		
		$this->load->view('inqType', $this->_data);
	}

//-----------------------------------------------------------------------

	/*
	*	ADD / UPDATE SMS Template Module
	*/
	public function addupdate_sms_template()
	{
		$templateid = $this->input->post('template_id');
		
		if($this->input->post('from_form')=='1')
		{			
			$data = array(
			'addedby'			=>	$this->session->userdata('userid'),
			'templatesubject'	=>	$this->input->post('templatesubject'),
			'template'			=>	$this->input->post('template'));
			
			if($templateid!='')
			{
				$this->sms->addupdate_sms_template($templateid,$data);
			}
			else
			{
				$this->sms->addupdate_sms_template($templateid	=	NULL,$data);
			}
		}
		
		$this->_data['sms']	=	$this->get_single_sms_template($templateid); // Get SMS Template for Edit Records.
		
		$this->load->view("addupdate_sms_template",$this->_data);
	}
	
//-----------------------------------------------------------------------

	/*
	*	SMS Template Listing Page
	*/
	public function smstemplate()
	{
		$this->haya_model->check_permission($this->_data['module'],'v'); // Checking View Permission
		
		$this->load->view('smstemplate', $this->_data); // Loading Template View
	}
	
//-----------------------------------------------------------------------

	/*
	*	Bulk SMS Listing
	*/
	public function bulksms()
	{
		$this->load->view('bulksms',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*	Bulk SMS Listing
	*/
	public function smstemplate_ajax()
	{	
		return $this->sms->smstemplate_ajax();
	}
	
//-------------------------------------------------------------------
	/*
	* 
	* 
	*/
	public function smsmodal($id,$type)
	{
		if($type	==	'inquery')
		{
			$response			=	$this->sms->sms_modal_info($tempid);
			$this->_data['m'] 	=	$response[0];
		}
		else
		{
			$response			=	$this->sms->sms_modal_info($tempid);	
			$this->_data['m'] 	=	$response[0];
		}
		
		$tQuery = $this->db->query("SELECT templatesubject, template FROM system_sms_template WHERE templatestatus='1' ORDER BY templatesubject ASC");
		
		$this->_data['type'] 		=	$type;
		$this->_data['template'] 	=	$tQuery->result();
		
		$this->load->view('smsmodal',$this->_data);
	}	
//-------------------------------------------------------------------
	/* 
	* 
	* 
	*/	
	public function templatedelete($tempid)
	{
		if($tempid!='') 
		{
			$this->sms->templatedelete($tempid);
		}
	}
//------------------------------------------------------------------------

  	/**
   	* Dynamic Forms Listing Page
   	* @param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]	=	$this->haya_model->get_all_custom_form($moduleid);
		
		$this->_data["userid"]	=	$this->_login_userid;
		
		$this->_data['formid']	=	$moduleid;
		 
		// Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);	 
	 }
//-----------------------------------------------------------------------
/* End of file sms.php */
/* Location: ./application/modules/sms/sms.php */
}
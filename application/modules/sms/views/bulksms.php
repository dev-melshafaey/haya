<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<?php $segment = $this->uri->segment(3); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <form name="frm_bulk" id="frm_bulk" name="post" autocomplete="off">
        <div class="panel panel-default panel-block">
          <div class="col-md-12 bulk-sms-overlay" id="sendingmessage">الرجاء الانتظار<br>
            جاري إرسال الرسالة</div>
          <div class="col-md-7 setMarginx" >
            <div class="col-md-11 setMarginWithBorder">
              <div class="col-md-3">
                <label class="text-warning strong"></label>
              </div>
              <div class="col-md-1">
                <label class="text-warning">المراجعين</label>
              </div>
              <div class="col-md-1">
                <input type="radio" class="sms_module_id" data-id="murageen" id="sms_module_id1" value="1" name="sms_module_id" />
              </div>
              <div class="col-md-1">
                <label class="text-warning">تسجيل</label>
              </div>
              <div class="col-md-1">
                <input type="radio"  class="sms_module_id"  data-id="tasgeel" id="sms_module_id2" checked="checked" value="2" name="sms_module_id" />
              </div>
              <div class="col-md-1">
                <label class="text-warning">الموظفين</label>
              </div>
              <div class="col-md-1">
                <input type="radio" class="sms_module_id"  data-id="mozafeen" id="sms_module_id3" value="3" name="sms_module_id" />
              </div>
            </div>
            <div class="col-md-11 setMarginWithBorder allbulklist murageen tasgeel">
              <div class="col-md-3">
                <label class="text-warning strong">طبيعة المراجعين:</label>
              </div>
              <div class="col-md-1">
                <label class="text-warning">فردي</label>
              </div>
              <div class="col-md-1">
                <input type="radio"  id="applicant_type" name="applicant_type" value="فردي" />
              </div>
              <div class="col-md-1">
                <label class="text-warning">مشترك</label>
              </div>
              <div class="col-md-1">
                <label class="text-warning">
                  <input type="radio"  id="applicant_type" name="applicant_type" value="مشترك" />
                </label>
              </div>
               <div class="col-md-1">
                <label class="text-warning">جميع</label>
              </div>
              <div class="col-md-1">
                <label class="text-warning">
                  <input type="radio"  id="applicant_type"  checked="checked" name="applicant_type" value="جميع" />
                </label>
              </div>
            </div>
            <div class="col-md-11 setMarginWithBorder  allbulklist murageen tasgeel">
              <div class="col-md-3">
                <label class="text-warning strong">النوع:</label>
              </div>
              <div class="col-md-1">
                <label class="text-warning">ذكر</label>
              </div>
              <div class="col-md-1">
                <input type="radio" id="applicant_gender" value="ذكر" name="applicant_gender" />
              </div>
              <div class="col-md-1">
                <label class="text-warning">أنثى</label>
              </div>
              <div class="col-md-1">
                <input type="radio" id="applicant_gender" value="أنثى" name="applicant_gender" />
              </div>
              <div class="col-md-1">
                <label class="text-warning">جميع</label>
              </div>
              <div class="col-md-1">
                <label class="text-warning">
                  <input type="radio"  id="applicant_gender" checked="checked" name="applicant_gender" value="جميع" />
                </label>
              </div>
            </div>
            <div class="col-md-11 setMarginWithBorder allbulklist murageen tasgeel mozafeen ">
              <div class="col-md-3 ">
               <?php $branchs = $this->haya_model->get_multi_select_box('branchid','Branch'); ?>
                <label class="text-warning strong">الفروع (<?PHP echo $b['c']; ?>):</label>
              </div>
              <div class="col-md-8 multiscroll">
                <?php echo $branchs['h']; ?>
              </div>
            </div>
            <!-- <div class="col-md-11 setMarginWithBorder allbulklist murageen tasgeel">
              <div class="col-md-3">
               <?PHP $p = $this->haya_model->get_multi_select_box('provianceid','Province'); ?>
                <label class="text-warning strong">اختر المحافظة (<?PHP echo $p['c']; ?>):</label>
              </div>
              <div class="col-md-8 multiscroll"><?PHP echo $p['h']; ?>
              </div>
            </div> --> 
            <div class="col-md-11 setMarginWithBorder allbulklist murageen tasgeel">
              <div class="col-md-3">
                <label class="text-warning strong">الولاية (<span id="wilayacount">0</span>):</label>
                <?php $wilaya = $this->haya_model->get_multi_select_box('provianceid','FirstList','wilaya'); ?>
              </div>
              <div class="col-md-8 multiscroll" id="wilayalist"> <?php echo $wilaya['h']; ?></div>
            </div>
          </div>
          <div class="col-md-5">
          <?PHP $template = $this->haya_model->get_multi_select_box('pingpong','SMSTemplate','pingpong'); ?>
            <label class="text-warning strong" style="margin-top: 16px;">الرسائل النصية (<?PHP echo $template['c']; ?>):</label>
            <div class="col-md-11 setMarginWithBorder templatebox">
                
            	<?PHP echo $template['h']; ?>
            </div>
            <div class="col-md-11 setMarginWithBorder">
              <textarea name="bulkmessage" placeholder="الرسالة" style="resize: none; height: 310px;" id="bulkmessage" class="form-control"></textarea>
            </div>
            <div class="col-md-11 setMarginWithBorder">
            <button type="button" id="bulksmssendtoall" class="btn btn-success">الرسال</button>
              <button type="button" class="btn btn-warning" id="smscount">0/0</button>
            </div>
          </div>
          <br clear="all">
        </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>

<!-- /.modal-dialog -->
</div>
</body>
</html>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                  <?PHP if($this->haya_model->check_other_permission(99,'a')==1) { ?>
                  <div class="row table-header-row" style="text-align:right;">
                    <button onClick="add_update_sms_template(this);" class="btn btn-success" type="button" id="0">اضافة الرسائل النصية</button>
                  </div>
                  <?PHP } ?>
                  <table class="table table-bordered table-striped dataTable" id="tableSortable"
                                           aria-describedby="tableSortable_info">
                    <thead>
                      <tr role="row">
                        <th style="text-align:center;">عنوان الرسئل</th>
                        <th style="text-align:center;">تاريخ</th>
                        <th style="text-align:center;">عدد الرسالة</th>
                        <th style="text-align:center;">الإجراءات</th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    </tbody>
                  </table>
                </div>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'sms/smstemplate_ajax/'.$app_id,'columns_array'=>'
{ "data": "عنوان الرسئل" },
{ "data": "تاريخ" },
{ "data": "عدد الرسالة" },			
{ "data": "الإجراءات"}')); ?>

<!-- /.modal-dialog -->

</div>
</body>
</html>
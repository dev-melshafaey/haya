<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Yateem extends CI_Controller {

	/*
	* Properties
	*/
	private $_data			=	array();
	private $_login_userid	=	NULL;

//-----------------------------------------------------------------------

	/*
	 * Constructor
	 */
	function __construct()
	{
		 parent::__construct();	
	
		// Loade Admin Model
		$this->load->model('yateem_model','yateem');
		
		$this->load->model('inquiries/inquiries_model','inq');
		
		$this->_data['module']			=	$this->haya_model->get_module();
		$this->_login_userid			=	$this->session->userdata('userid');
		$this->_data['login_userid']	=	$this->_login_userid;	
		$this->_data['user_detail'] 	= 	$this->haya_model->get_user_detail($this->_login_userid);
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function getYateemNames()
	{
		$term = $this->input->get('term');
		
		$this->db->select('orphan_id,orphan_name');
		$this->db->from('ah_yateem_info');	
			
		if($term !="")
		{
			$where = 'orphan_name like '.$term.' or orphan_id like '.$term.'';
			$this->db->where($where);
		}
		
		$query = $this->db->get();
			
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $data)
			{
				$arr[] = array('id'=>$data->orphan_id,'label'=>$data->orphan_name,'value'=>$data->orphan_name);
			}
			
			echo json_encode($arr);

			$this->_data['charity_type_id'] 	=	205;	
			$this->_data['yateem_mother_data'] 	=	$this->yateem->getParentsData($id,2);
			$this->_data['yateem_banks_data'] 	=	$this->yateem->getBanksData($id);
			$this->_data['yateem_others_data'] 	=	$this->yateem->getOthersData($id);
			$this->_data['yateem_edu_data'] 	=	$this->yateem->getYateemDataById('ah_yateem_education',$id);
			$this->_data['yateem_can_data'] 	=	$this->yateem->getYateemDataById('ah_yateem_candidate',$id);
			$this->_data['yateem_docs'] 		=	$this->yateem->getYateemDocument($id);
			
			$this->load->view('yateem_details',$this->_data);
		}
	}

//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function details($id, $type)
	{		
		switch($type)
		{
			case 'yateem';
				$this->_data['yateem_data'] = $this->yateem->getYateemData($id);
				$this->_data['yateem_sisters'] = $this->yateem->getBroSisNames($id,'sister');
				$this->_data['yateem_brothers'] = $this->yateem->getBroSisNames($id,'brother');
				$this->_data['yateem_father_data'] = $this->yateem->getParentsData($id,1);
				$this->_data['yateem_mother_data'] = $this->yateem->getParentsData($id,2);
				$this->_data['yateem_banks_data'] = $this->yateem->getBanksData($id);
				$this->_data['yateem_others_data'] = $this->yateem->getOthersData($id);
				$this->_data['yateem_edu_data'] = $this->yateem->getYateemDataById('ah_yateem_education',$id);
				$this->_data['yateem_can_data'] = $this->yateem->getYateemDataById('ah_yateem_candidate',$id);
				echo $this->load->view('yateem_details', $this->_data);
			break;
			case 'passport_number';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail', $this->_data);
			break;
			case 'passport_number_wife';
				$this->_data['_applicant_data'] = $this->haya_model->getRequestInfo($id);	
				echo $this->load->view('idwithdetail', $this->_data);
			break;			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	public function getKafeelNames()
	{
		$term = $this->input->get('term');
		
		$this->db->select('sponser_id, sponser_name, sponser_id_number, sponser_picture');
		$this->db->from('ah_sponser_info');
		
		if(is_numeric($term))
		{
			$this->db->like('sponser_id_number',$term);	
		}
		else
		{
			$this->db->like('sponser_name',$term);
		}
		
		$this->db->where('step','1');
		$this->db->where('cancel','0');
		$this->db->like('sponsor_status','1');
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $data)
			{
			   $picURl = base_url().'resources/sponser/'.$data->sponser_id.'/'.$data->sponser_picture;
			   
			   $this->db->select("assign_id");
			   $this->db->from("assigned_orphan");
			   $this->db->where("sponser_id",$data->sponser_id);
			   $this->db->where("is_old","0");
			   //$this->db->where("merge_id",""); // Muzaffar : Comment this line 01-05-2017
			   $this->db->group_by("orphan_id");
			   $OrphanQuery = $this->db->get();
			   $OrphanRes = $OrphanQuery->num_rows();
			   $arr[] = array('id'=>$data->sponser_id,'orphan'=>$OrphanRes,'pic'=>$picURl,'spid'=>$data->sponser_id_number,'label'=>$data->sponser_name.' ('.arabic_date($data->sponser_id_number).') يتيم تحت هذا الكفيل ('.arabic_date($OrphanRes).')','value'=>$data->sponser_name);
			}
			
			echo json_encode($arr);
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function add_assign_data()
	{		
		$userid 	=	$this->session->userdata('userid');
		$branch_id 	=   $this->haya_model->get_branch_id($userid);			
		$postData 	=	$this->input->post();
		
		foreach($postData['yateem_id'] as $yateemkey => $yateemid)
		{
			$assignData['orphan_id']  			= 	$yateemid;
			$assignData['userid'] 				=	$userid;
			$assignData['user_branch_id'] 		=	$branch_id;
			$assignData['file_number']  		= 	$postData['file_number'];
			//$assignData['starting_date']  	=   $postData['starting_date_'.$yateemid]; Before IT WAS
			$assignData['starting_date']  		=   $postData['start_payment_date']; // Muzaffar Changed as Jamal Said
			$assignData['sponser_id'] 			=	$postData['sponser_id'];
			$assignData['details']  			= 	$postData['details'];
			$assignData['warranty']  			= 	$postData['warranty'];
			$assignData['payment_type']  		= 	$postData['payment_type'];
			$assignData['sponser_type']  		= 	$postData['sponser_type'];
			$assignData['monthly_payment']  	= 	$postData['monthly_payment'];
			$assignData['total_payment']  		=	$postData['total_payment'];
			$assignData['financial_year'] 		=	$postData['financial_year'];			
			$assignData['start_payment_date'] 	=	$postData['start_payment_date'];
			$assignData['end_payment_date'] 	=	$postData['end_payment_date'];
			$assignData['years'] 				=	$postData['years'];
			$assignData['yearly_amount'] 		=	($postData['years']	*	12) * $postData['monthly_payment'];
			$assignData['delete_record'] 		=	'0';
			$assignData['merge_id'] 			=	$yateemid.$postData['sponser_id']; // First yateem ID Second Kafeel ID
			
			$yateemQuery = $this->db->query("SELECT assign_id FROM assigned_orphan WHERE `orphan_id`='".$yateemid."' AND `sponser_id`='".$postData['sponser_id']."' AND is_old='0'");

			if($yateemQuery->num_rows() > 0)
			{
				$yatemmResult = $yateemQuery->row();
				$this->db->where('assign_id',$yatemmResult->assign_id);
				$this->db->update('assigned_orphan',json_encode($assignData),$userid,$assignData);
			}
			else
			{
				$this->db->insert('assigned_orphan',$assignData,json_encode($assignData),$userid);
			}	
		}
		
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
		redirect(base_url().'yateem/yateem_search_view');
		exit();
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function getKafeellist($orphan_id)
	{
		$this->_data['orphan_detail'] 	= $this->yateem->getYateemFullDetail($orphan_id);
		$this->_data['kafeel_list'] 	= $this->yateem->getKafeellist($orphan_id);
		
		$this->load->view('kafeel_list_orphan',$this->_data);
	}
//-----------------------------------------------------------------------

	/*
	*  Add / Edit Yateem Form Detail 
	*/
	function add_yateem($id='')
	{
		if(isset($id) && $id!="")
		{
			$this->_data['yateem_data'] 			=	$this->yateem->getYateemDataById($id); 			//	Get all Yateem Basic Data
			$this->_data['yateem_sisters'] 			=	$this->yateem->getBroSisNames($id,'sister'); 	//	Get all Yateem Sisters
			$this->_data['yateem_brothers'] 		=	$this->yateem->getBroSisNames($id,'brother');	//	Get all Yateem Brothers
			$this->_data['yateem_father_data'] 		=	$this->yateem->get_father_data($id); 			//	Get Yateem Father Detail
			$this->_data['yateem_mother_data'] 		=	$this->yateem->get_mother_data($id);			//	Get Yateem Mother Detail
			$this->_data['yateem_banks_data'] 		=	$this->yateem->getBanksData($id);				//	Get Bank Detail
			$this->_data['yateem_others_data'] 		=	$this->yateem->getOthersData($id);				//	Get Extra Information
			$this->_data['yateem_docs'] 			=	$this->yateem->getYateemDocument($id);			//	Get all required documents
			
			$this->_data['yateem_edu_data'] 		=	$this->yateem->get_yateem_others_data('ah_yateem_education',$id);
			$this->_data['yateem_can_data'] 		=	$this->yateem->get_yateem_others_data('ah_yateem_candidate',$id);
			$this->_data['yateem_sisters_brothers']	=	$this->yateem->getBroSisNamesNew($id,'sister');
		}
		
		$this->load->view('add-yateem-form',$this->_data);	//	Add YATEEM form Page
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function getYateemDetails($id)
	{
		$this->_data['yateem_data'] 				=	$this->yateem->getYateemDataById($id);
		$this->_data['yateem_sisters'] 				= 	$this->yateem->getBroSisNames($id,'sister');
		$this->_data['yateem_brothers'] 			= 	$this->yateem->getBroSisNames($id,'brother');
		$this->_data['yateem_father_data'] 			= 	$this->yateem->get_father_data($id);
		$this->_data['charity_type_id'] 			= 	205;	
		$this->_data['yateem_mother_data'] 			= 	$this->yateem->get_mother_data($id);
		$this->_data['yateem_banks_data'] 			= 	$this->yateem->getBanksData($id);
		$this->_data['yateem_others_data'] 			= 	$this->yateem->getOthersData($id);
		$this->_data['yateem_edu_data'] 			= 	$this->yateem->get_yateem_others_data('ah_yateem_education',$id);
		$this->_data['yateem_can_data'] 			= 	$this->yateem->get_yateem_others_data('ah_yateem_candidate',$id);
		$this->_data['yateem_docs'] 				= 	$this->yateem->getYateemDocument($id);
		$this->_data['yateem_sisters_brothers'] 	=	$this->yateem->getBroSisNamesNew($id,'sister');
		$this->_data['kafeel_list'] 				= 	$this->yateem->getKafeellist($id);
		$this->_data['yateem_servay_list'] 			=	$this->yateem->get_orphan_step2_info($id); // Get Orphan Step 2 Detail 
		$this->_data['yateem_documents_history'] 	=	$this->yateem->get_orphan_renew_history($id); // Get Orphan Renew Document Hiistory
		$this->_data['cancel_orphan_detail'] 		=	$this->yateem->get_cancel_orphan_detail($id,'ah_yateem_stop_detail'); // Get Orphan Stop Detail
		$this->_data['continue_orphan_detail'] 		=	$this->yateem->get_cancel_orphan_detail($id,'ah_yateem_start_detail'); // Get Orphan Conitnue Detail

		$this->load->view('yateem_details',$this->_data);	// View Detail all yateem Detils
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function renew_orphan($id)
	{
		if($this->input->post())
		{
			$orphan_id			=	$this->input->post('orphan_id');

			// RENEW the Orphan
			$renew_orphan['approved_date']	=	$this->input->post('approved_date');
			$renew_orphan['expire_date']	=	$this->input->post('expire_date');
			
			$this->yateem->renew_orphan($orphan_id,$renew_orphan);
			
			// UPDATE the History
			$last_orphan_history['last_approved_date']	=	$this->input->post('last_approved_date');
			$last_orphan_history['last_expiry_date']	=	$this->input->post('last_expire_date');
			$last_orphan_history['orphan_id']			=	$this->input->post('orphan_id');
			$last_orphan_history['renew_date']			=	date('Y-m-d');
			$last_orphan_history['user_id']				=	$this->_login_userid;
			
			// ADD Orphan Detail making History
			$this->yateem->update_orphan_renew_history($last_orphan_history);
			
			$alldocs = $this->haya_model->allRequiredDocument(205);
			
			if(!empty($alldocs))
			{
				foreach($alldocs as $doc)
				{
					$docname = 'doclist'.$doc->documentid;
					
					if(isset($_FILES["".$docname.""]["tmp_name"]) && $_FILES["".$docname.""]["tmp_name"]!="")
					{
						$docData['document_name'] 	=	$this->upload_file($orphan_id,$docname,'resources/yateem');
						
						$docData['orphan_id'] 		=	$this->input->post('orphan_id');
						$docData['doc_type_id'] 	=	$doc->documentid;
						$docData['userid'] 			=	$this->session->userdata('userid');
						$docData['user_branch_id'] 	=	"";

						 $this->check_docfile_history($doc->documentid,$this->input->post('orphan_id'),$docData);
					}
				}
			}
			
			echo 'تم تحديث تاريخ إنتهاء هذا التاريخ';
			exit();
		}
		else
		{
			$this->_data['yateem_data']	=	$this->yateem->getYateemDataById($id);
			
			$this->_data['orphan_id']	=	$id;	
			
			$this->load->view('renew-orphan',$this->_data);	// View Detail all yateem Detils
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function assignOrphan()
	{
		// GET all APPROVED yateem list.
		$this->_data['yateem']	=	$this->yateem->get_yateem_under_18_list();
		
		$this->load->view('assign-yateem-form',$this->_data);
	}
//--------------------------------------------------------------------------
	/*
	*
	*
	*/
	public function assignOrphanNew()
	{
		// GET all APPROVED yateem list.
		$this->_data['yateem']	=	$this->yateem->get_yateem_under_18_list();
		
		$this->load->view('assign-yateem-form-new',$this->_data);
	}
//--------------------------------------------------------------------------

	/*
	* Search Yateem who dnt have Kafeel
	*/	
	function search_yateem()
	{
		if (isset($_GET['keyword']))
		{
			$this->_data['yateem'] = $this->yateem->get_all_yateem_list($_GET['keyword']);
			
			echo $html	=	$this->load->view('yateem-search-listing',$this->_data,TRUE); // AJAX response Yateem listing
		}
    }
//-----------------------------------------------------------------------
	/*
	*
	* Add / Edit KAFEEL Form
	*/	
	function add_kafeel($id)
	{
		if($id)
		{
			$this->_data['kafeel_data'] =   $this->yateem->getKafeelInfo($id);
			$this->_data['kafeel_docs'] = 	$this->yateem->getKafeelDocument($id);
		}
		
		// Kafeel form View	
		$this->load->view('add-kafeel-form',$this->_data);	 // Add KAFEEL form
	}
//-----------------------------------------------------------------------
	/*
	* Upload Image/File
	*
	*/	
	function upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';
		}
		else
		{
			$path = './'.$folder.'/';
		}
			
		if (!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();
			
			if($thumb	==	TRUE)
			{
				$this->load->library('image_lib');
				
				$config['image_library']	=	'gd2';
				$config['source_image'] 	= 	$path . $image_data['file_name'];
				$config['maintain_ratio'] 	=	FALSE;
				$config['create_thumb'] 	=	FALSE;
				$config['width'] 			=	$width;
				$config['height'] 			=	$height;
				$config['new_image'] 		=	$path . 'thumb_'.$image_data['file_name'];
				
				$this->image_lib->initialize($config);
				$this->image_lib->resize_crop();
			}

			return $image_data['file_name'];
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function add_kafeel_data()
	{
		$userid 	=	$this->session->userdata('userid');
		$branch_id 	=   $this->haya_model->get_branch_id($userid);
			 
		$postData 	=	$this->input->post();
		
		if(isset($postData['old_sponser_id_number']))
		{
			if($postData['old_sponser_id_number']	==	$postData['sponser_id_number'])
			{
				$sponser_id_number	=	'0';
			}
			else
			{
				// Check Sponsor Already Exist
				$sponser_id_number	=	$this->yateem->this_is_exist('sponser_id_number',$postData['sponser_id_number'],'ah_sponser_info');
			}
			
			// IF Exist redirect the page and show message.
			if($sponser_id_number)
			{
				$this->load->library('user_agent');
				if ($this->agent->is_referral())
				{
					$url	=	 $this->agent->referrer();
				}
				
				$this->session->set_flashdata('error', '* ملاحظة : هذا اليتيم مسجل سابقا');
				redirect($url);
				exit();
			}
		}
				
		$sponserArr['sponser_name'] 				=	$postData['sponser_name'];
		$sponserArr['file_number'] 					=	$postData['file_number'];
		$sponserArr['sponser_id_number'] 			=	$postData['sponser_id_number'];
		$sponserArr['sponser_id_expire'] 			=	$postData['sponser_id_expire'];
		$sponserArr['sponser_designation'] 			=	$postData['sponser_designation'];
		$sponserArr['country_id'] 					=	$postData['country_id'];
		$sponserArr['city_id'] 						=	$postData['city_id'];
		$sponserArr['muhafiza_id'] 					=	$postData['muhafiza_id'];
		$sponserArr['sponser_town_id'] 				=	$postData['sponser_town_id'];
		$sponserArr['sponser_po_address'] 			=	$postData['sponser_po_address'];
		$sponserArr['sponser_pc_address'] 			=	$postData['sponser_pc_address'];
		$sponserArr['sponser_fax'] 					=	$postData['sponser_fax'];	
		$sponserArr['sponser_phone_number'] 		=	$postData['sponser_phone_number'];
		$sponserArr['sponser_phone_number_extra'] 	=	$postData['sponser_phone_number_extra'];
		$sponserArr['sponser_mobile_number'] 		=	$postData['sponser_mobile_number'];
		$sponserArr['bankid'] 						=	$postData['bankid'];	
		$sponserArr['bankbranchid'] 				=	$postData['bankbranchid'];
		$sponserArr['accountfullname'] 				=	$postData['accountfullname'];
		$sponserArr['sponser_nationalty'] 			=	$postData['sponser_nationalty'];
		$sponserArr['sponser_work_place'] 			=	$postData['sponser_work_place'];
		$sponserArr['place_name'] 					=	$postData['place_name'];
		$sponserArr['sponser_account_number'] 		=	$postData['sponser_account_number'];
		$sponserArr['sponser_email'] 				=	$postData['sponser_email'];
		$sponserArr['notes'] 						=	$postData['notes'];	
		$sponserArr['userid'] 						=	$this->session->userdata('userid');
		$sponserArr['user_branch_id'] 				=	$branch_id;
		$sponserArr['step'] 						=	$postData['step'];
		
		if($postData['reason_about_status'])
		{
			$sponserArr['reason_about_status'] 	=	$postData['reason_about_status'];
		}
			
		if($postData['sponsor_status']	==	'1')
		{
			$sponserArr['step']				= 	'1';
			$sponserArr['sponsor_status'] 	=	$postData['sponsor_status'];
			$sponserArr['approved_date'] 	=	date('Y-m-d');
		}
		
		if($postData['sponsor_status']	==	'0')
		{
			$sponserArr['step']				= 	'0';
			$sponserArr['sponsor_status']	=	$postData['sponsor_status'];
			$sponserArr['reject_date'] 	=	date('Y-m-d');
		}

		//sponser_po_address
		$sponser_id  = $this->check_id('sponser_id','ah_sponser_info','sponser_id',$postData,$sponserArr,'');
		
		// For Notification
		$notification_data	=	array();
		
		$notification_data['step']				=	$sponserArr['step']	;
		$notification_data['notification_to']	=	'SPONSOR';
		$notification_data['reffrence_id']		=	$this->input->post('sponser_id');
		$notification_data['status']			=	$sponserArr['sponsor_status'];
		
		$this->haya_model->add_action_for_notification($notification_data); // UPDATE Orphan Step
			
		if(!empty($_FILES))
		{
			if($_FILES["sponser_picture"]["tmp_name"])
			{
				$profileData['sponser_picture'] 	=	$this->upload_file($sponser_id,'sponser_picture','resources/sponser');
				$json_data	=	json_encode(array('data'	=>$profileData));
				$this->db->where('sponser_id',$sponser_id);
				$this->db->update('ah_sponser_info',$json_data,$this->session->userdata('userid'),$profileData);
			}
		}
			
		$alldocs = $this->haya_model->allRequiredDocument(206);
			
		if(!empty($alldocs))
		{
			foreach($alldocs as $doc)
			{
				$docname = 'doclist'.$doc->documentid;
				if(isset($_FILES["".$docname.""]["tmp_name"]) && $_FILES["".$docname.""]["tmp_name"]!="")
				{
					$docData['document_name'] 	=	$this->upload_file($sponser_id,$docname,'resources/sponser');
					$docData['sponser_id'] 		=	$sponser_id;
					$docData['doc_type_id'] 	=	$doc->documentid;
					$docData['userid'] 			=	$this->session->userdata('userid');
					$docData['user_branch_id'] 	=	$branch_id;
					
					$this->check_docfile($doc->documentid,'ah_kafeel_documents','sponser_id',$sponser_id,$docData);
				}
			}
		}
		
		$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
		redirect(base_url().'yateem/orphan_sponsorship');
		exit();
				
	}
//-----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	function kafeel_list($status	=	NULL,$orphan_id	=	NULL)
	{
		$this->_data['status']		=	$status;
		$this->_data['orphan_id']	=	$orphan_id;
		
		$this->load->view('kafeel-listing',$this->_data);		
	}

//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function getSponserDetails($id)
	{
		$this->_data['kafeel_data']				=   $this->yateem->getKafeelInfo($id);
		$this->_data['yateem_list'] 			= 	$this->yateem->get_yateeem_list($id,$type);
		$this->_data['sponsor_start_stop_info'] = 	$this->yateem->get_sponsor_start_stop_info($id);	//	GET Sponsor Information of STOP & START 
		
		//echo '<pre>'; print_r($this->_data['sponsor_start_stop_info']);
		$this->load->view('sponser_details',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function all_assignedsponser_list($status)
	{
		$all_users	=	$this->yateem->get_assignedsponser_list_status($status);

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				if($status == 'not')
				{
					$yeteem_count_url = '<a href="'.base_url().'yateem/yateem_search_view/'.$lc->sponser_id.'">(0)</a>';
				}
				else
				{
					$yeteem_count_url = '<a href="'.base_url().'yateem/yateem_search_view/'.$lc->sponser_id.'">('.$lc->sponserCount.')</a>';
				}
				
				$action		= '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getSponserDetails/'.$lc->sponser_id.'" ><i class="icon-eye-open"></i></a>';
				$action 	.= '<a class="iconspace" href="'.base_url().'yateem/add_kafeel/'.$lc->sponser_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';		$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_kafeel/'.$lc->sponser_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				
				$img = '<img src="'.base_url().'/resources/sponser/'.$lc->sponser_id.'/'.$lc->sponser_picture.'" width="50px" height="50px"> <br/>';
				
				$arr[] = array(
					"DT_RowId"		=>	$lc->sponser_id.'_durar_lm',
					"رقم الكفيل" 	=>	$lc->sponser_id,
					"اسم" 			=>	$img.$lc->sponser_name,
					"الجنسية" 		=>	is_set($this->haya_model->get_name_from_list($lc->sponser_nationalty)),
					"رقم الهوية" 	=>	is_set($lc->sponser_id_number),
					"عدد الأيتام" 	=>	is_set($yeteem_count_url),
					"الإجرائات" 		=>	$action);
					
					unset($action);
					unset($img);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function kafeel_assigned($id)
	{
		$this->_data['sponser_id'] = $id;
		
		$this->load->view('kafeel-assignedlisting',$this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function check_docfile($doc_type_id,$table,$wherwKey,$parent_id,$tableData){
		
		$json_data	=	json_encode(array('data'=>$tableData));
		//$whereValue = 'orphan_id='.$parent_id.' AND doc_type_id='.$doc_type_id.'';
		$wherClause =  array($wherwKey =>$parent_id, 'doc_type_id' => $doc_type_id);

		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($wherClause);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$this->db->where($wherClause);
			$this->db->update($table,$json_data,$this->session->userdata('userid'),$tableData);
				
		}
		else
		{
			$this->db->insert($table,$tableData,$json_data,$this->session->userdata('userid'));	
			return  $id = $this->db->insert_id();
		}
		
		
	
	}
//-------------------------------------------------------------------------------
	/*
	*
	*
	*/
	function check_docfile_history($doc_type_id,$parent_id,$tableData)
	{
		$json_data	=	json_encode(array('data'=>$tableData));

		$wherClause =  array('orphan_id' =>$parent_id, 'doc_type_id' => $doc_type_id);

		$this->db->select('*');
		$this->db->from('ah_yateem_documents');
		$this->db->where($wherClause);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$this->db->where($wherClause);
			$this->db->update('ah_yateem_documents',$json_data,$this->session->userdata('userid'),$tableData);
		}
		else
		{
			$this->db->insert('ah_yateem_documents',$tableData,$json_data,$this->session->userdata('userid'));	
			$yateem_id = $this->db->insert_id();
		}
		
		// For Making History
		$this->db->insert('ah_yateem_documents_history',$tableData,$json_data,$this->session->userdata('userid'));	
		return TRUE;
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function delete_yateeem($id)
	{
		//$postData = $this->input->post();
		$this->yateem->delete_yateem($id);		
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function delete_kafeel($id)
	{
		echo $this->yateem->delete_kafeel($id);	
			
		$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function all_sponser_list($status,$orphan_id	=	NULL)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];

		if($orphan_id)
		{
			$all_users	=	$this->yateem->get_sponser_list(NULL,$orphan_id);
		}
		else
		{
			$all_users	=	$this->yateem->get_sponser_list($status);
		}
		
		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				$action  = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getSponserDetails/'.$lc->sponser_id.'" ><i class="icon-eye-open"></i></a>';
				$action .= '<a class="iconspace" href="'.base_url().'yateem/add_kafeel/'.$lc->sponser_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_kafeel/'.$lc->sponser_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				
				$img = '<img src="'.base_url().'/resources/sponser/'.$lc->sponser_id.'/'.$lc->sponser_picture.'" width="50px" height="50px"> <br/>';
				$arr[] = array(
					"DT_RowId"		=>	$lc->sponser_id.'_durar_lm',
					"رقم الكفيل"	=>	arabic_date($lc->sponser_id),
					"اسم"			=>	$lc->sponser_name,
					"الجنسية"		=>	$this->haya_model->get_name_from_list($lc->sponser_nationalty),
					"رقم الهوية"	=>	arabic_date($lc->sponser_id_number),
					"تاريخ الكفالة"	=>	arabic_date(date('Y-m-d',strtotime($lc->created))),
					"عدد الأيتام"	=>	'<a href="'.base_url().'yateem/yateem_search_view/'.$lc->sponser_id.'">('.arabic_date($this->haya_model->get_total_yateem_by_sponser($lc->sponser_id)).')</a>',
					"الإجرائات"		=>	$action);
					unset($action);
					unset($img);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function yateem_assigned($status = '')
	{
		$this->_data['assigned_status'] = $status;
		$this->load->view('yatem-asssignedlisting',$this->_data);	
	}
	
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	public function all_yateem_list($id	=	NULL,$type	=		NULL,$status	=	NULL)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		
		if($id)
		{
			//Old Step 2
			//$all_users	=	$this->yateem->get_yateeem_list($id,$type,'2');
			$all_users	=	$this->yateem->get_yateeem_list($id,$type,'3');
		}
		else
		{
			
			$all_users	=	$this->yateem->get_yateeem_list_new($status,'3');
		}


		
		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				$action = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				//$action .= '<a class="iconspace" href="'.base_url().'yateem/add_yateem/'.$lc->orphan_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				//$action .= '<a class="iconspace" href="'.base_url().'yateem/change_kafeel/'.$lc->orphan_id.'"><i style="color:#C90;" class="myicon icon-male"></i></a>';
				//$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_yateeem/'.$lc->orphan_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				
				$img = '<img src="'.base_url().'">';
				$arr[] = array(
					"DT_RowId"				=>	$lc->orphan_id.'_durar_lm',
					"الرقم المدني لليتيم"	=>	is_set(arabic_date($lc->file_number)),
					"رقم اليتيم/الكفيل"		=>	$this->yateem->get_merge_id($lc->orphan_id),
					"اسم اليتيم" 			=>	is_set($lc->orphan_name),
					"الجنسية" 				=>	is_set($this->haya_model->get_name_from_list($lc->orphan_nationalty)),
					"تاريخ الميلاد" 			=>	is_set(arabic_date($lc->date_birth)),
					"الدولة" 				=>	is_set($this->haya_model->get_name_from_list($lc->country_id)),
					"المدينة" 				=>	is_set($this->haya_model->get_name_from_list($lc->city_id)),
					"تاريخ تقديم الطلب" 	=>	date('Y-m-d',strtotime($lc->created)),
					"أسم الكفيل"			=>	$this->haya_model->get_name_of_kafeel($lc->orphan_id),
					/* "عدد الكفيل" 		=> '<a href="'.base_url().'yateem/kafeel_list/0/'.$lc->orphan_id.'">('.arabic_date($this->haya_model->get_total_kafeel_by_orphan($lc->orphan_id)).')</a>', */
					"الإجرائات" 				=>	$action);
					unset($action);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
			
			//checkArraySize($arr);		
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	public function change_all_yateem_list($id	=	NULL,$type	=		NULL,$status	=	NULL)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		if($id)
		{
			$all_users	=	$this->yateem->get_yateeem_list($id,$type,'3');
		}
		else
		{
			$all_users	=	$this->yateem->get_yateeem_list_new($status,'3');
		}
		
		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				$total_kafeel	=	$this->haya_model->get_total_kafeel_by_orphan($lc->orphan_id);
				
				$action = '<a onclick="alatadad(this);" data-url="'.base_url().'yateem/stop_the_orphan/'.$lc->orphan_id.'"  href="#"><i style="color:#CC0000;" class="myicon icon-remove"></i></a>';
				$action .= '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				
				if($total_kafeel	>	0)
				{
					$action .= '<a class="iconspace" href="'.base_url().'yateem/change_kafeel/'.$lc->orphan_id.'"><i style="color:#C90;" class="myicon icon-male"></i></a>';
				}
				if($this->haya_model->get_name_of_kafeel($lc->orphan_id))
				{
					$sponsor_name	=	$this->haya_model->get_name_of_kafeel($lc->orphan_id);
				}
				else
				{
					$sponsor_name	=	'انتظار...';
				}

				$img = '<img src="'.base_url().'">';
				$arr[] = array(
					"DT_RowId"				=>	$lc->orphan_id.'_durar_lm',
					/*"رقم اليتيم"			=>	is_set(arabic_date($lc->orphan_id)),*/
					/*"رقم اليتيم/الكفيل"		=>	$this->yateem->get_merge_id($lc->orphan_id),*/
					"الرقم المدني لليتيم"	=>	$lc->file_number,
					"اسم اليتيم" 			=>	is_set($lc->orphan_name),
					"الجنسية" 				=>	is_set($this->haya_model->get_name_from_list($lc->orphan_nationalty)),
					"تاريخ الميلاد" 			=>	is_set(arabic_date($lc->date_birth)),
					"الدولة" 				=>	is_set($this->haya_model->get_name_from_list($lc->country_id)),
					"المدينة" 				=>	is_set($this->haya_model->get_name_from_list($lc->city_id)),
					"تاريخ الموافقة" 		=>	date('Y-m-d',strtotime($lc->approved_date)),
					"تاريخ الانتهاء" 		=>	date('Y-m-d',strtotime($lc->expire_date)),
					"أسم الكفيل"			=>	$sponsor_name,
					/* "عدد الكفيل" 		=> '<a href="'.base_url().'yateem/kafeel_list/0/'.$lc->orphan_id.'">('.arabic_date($this->haya_model->get_total_kafeel_by_orphan($lc->orphan_id)).')</a>', */
					"الإجرائات" 				=>	$action);
					unset($action);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);
		}
	}
//-------------------------------------------------------------------------------
 
	/*
	* Get Old Records
	*/	
	public function yateem_search_view()
	{
		$data	=	$this->input->post();
		
		if(!empty($data))
		{
			$this->_data['return_result']	=	$this->yateem->get_yateem_search_result($data);
			
			$this->load->view("yateem-search-view", $this->_data);
			
		}
		else
		{
			$this->load->view("yateem-search-view", $this->_data);
		}
	}
//-------------------------------------------------------------------------------
 
	/*
	* Kafeel Searching
	*/	
	public function kafeel_search_view()
	{
		$data	=	$this->input->post();
		
		if(!empty($data))
		{
			$this->_data['return_result']	=	$this->yateem->get_kafeel_search_result($data);
			
			$this->load->view("kafeel-search-view", $this->_data);
			
		}
		else
		{
			$this->load->view("kafeel-search-view", $this->_data);
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function all_yateemassigned_list($yateem_status)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		$all_users	=	$this->yateem->get_assignedyateeem_list_status($yateem_status);

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				$action = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				$action .= '<a class="iconspace" href="'.base_url().'yateem/add_yateem/'.$lc->orphan_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';		$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_yateeem/'.$lc->orphan_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				
				$img = '<img src="'.base_url().'">';
				$arr[] = array(
					"DT_RowId"		=>	$lc->orphan_id.'_durar_lm',
					"رقم اليتيم" 	=>	$lc->orphan_id,
					"اسم اليتيم" 	=>	$lc->orphan_name,
					"الجنسية" 		=>	$this->haya_model->get_name_from_list($lc->orphan_nationalty),
					"تاريخ الميلاد" 	=>	$lc->date_birth,
					"الدولة" 		=>	$this->haya_model->get_name_from_list($lc->country_id),
					"المدينة" 		=>	$this->haya_model->get_name_from_list($lc->city_id),
					"الإجرائات"		=>	$action);
					
					unset($action);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function all_assignedyateem_list($user_id	=	NULL)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		$all_users	=	$this->yateem->get_assignedyateeem_list();

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				
				$action .= '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				//$action .= '<a class="iconspace" href="'.base_url().'yateem/add_yateem/'.$lc->orphan_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				//$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_yateeem/'.$lc->orphan_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				
				$img = '<img src="'.base_url().'">';
				$arr[] = array(
					"DT_RowId"		=>	$lc->orphan_id.'_durar_lm',
					"اسم اليتيم" 	=>	is_set($lc->orphan_name),
					"الجنسية" 		=>	$this->haya_model->get_name_from_list($lc->orphan_nationalty),
					"تاريخ الميلاد" 	=>	is_set(arabic_date($lc->date_birth)),
					"الدولة" 		=>	$this->haya_model->get_name_from_list($lc->country_id),
					"اسم الكفيل" 	=>	is_set($lc->sponser_name),
					"الإجرائات" 		=>	$action);
					unset($action);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function check_id($id,$table,$key,$postData,$tableData,$ind)
	{
		if(isset($postData[$id]) && $postData[$id]!="" )
		{
			$json_data	=	json_encode(array('data'=>$tableData));

				if(is_array($postData[$id]))
				{
					$whereValue = $postData[$id][$ind];
				}
				else
				{
					$whereValue = $postData[$id];
				}
				
				$this->db->where($key,$whereValue);
				$this->db->update($table,$json_data,$this->session->userdata('userid'),$tableData);
				return $postData[$id];
		}
		else
		{
				$this->db->insert($table,$tableData);
				return $yateem_id = $this->db->insert_id();
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function assigned_yateem()
	{
		$this->load->view('assigned-yatem-listing',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function yateem_list($id,$type='',$status,$a	=	NULL,$b	=	NULL,$c	=	NULL,$d	=	NULL,$e	=	NULL)
	{
		$this->_data['sponser_id'] 	= $id;
		$this->_data['type'] 		= $type;
		$this->_data['status'] 		= $status;
		
		$this->load->view('yatem-listing',$this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function changing_kafeel_listing($id,$type='',$status,$a	=	NULL,$b	=	NULL,$c	=	NULL,$d	=	NULL,$e	=	NULL)
	{
		//$this->_data['sponser_id'] 	= $id;
		//$this->_data['type'] 			= $type;
		//$this->_data['status'] 		= $status;
		
		$this->load->view('change-kafeel-yatem-listing',$this->_data);		
	}

//--------------------------------------------------------------------------

	/*
	*
	*
	*/
	public function change_kafeel($orphan_id)
	{
		$this->_data['orphan_id']		=	$orphan_id;
		$this->_data['kafeel_list'] 	= $this->yateem->old_getKafeellist($orphan_id);
		
		$this->load->view('change-kafeel-for-yateem',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	function add_assign_data_new_kafeel()
	{		
		$userid 	=	$this->session->userdata('userid');
		$branch_id 	=   $this->haya_model->get_branch_id($userid);			
		$postData 	=	$this->input->post();
		
		$assignData['orphan_id']  			= 	$postData['orphan_id'];
		$assignData['userid'] 				=	$userid;
		$assignData['user_branch_id'] 		=	$branch_id;
		$assignData['file_number']  		= 	$postData['file_number'];
		$assignData['starting_date']  		=   date('Y-m-d');
		$assignData['sponser_id'] 			=	$postData['sponser_id'];
		$assignData['details']  			= 	$postData['details'];
		$assignData['warranty']  			= 	$postData['warranty'];
		$assignData['payment_type']  		= 	$postData['payment_type'];
		$assignData['sponser_type']  		= 	$postData['sponser_type'];
		$assignData['monthly_payment']  	= 	$postData['monthly_payment'];
		$assignData['total_payment']  		=	$postData['total_payment'];
		$assignData['financial_year'] 		=	$postData['financial_year'];			
		$assignData['start_payment_date'] 	=	$postData['start_payment_date'];
		$assignData['end_payment_date'] 	=	$postData['end_payment_date'];
		$assignData['years'] 				=	$postData['years'];
		$assignData['yearly_amount'] 		=	($postData['years']	*	12) * $postData['monthly_payment'];
		$assignData['delete_record'] 		=	'0';
			
		$yateemQuery = $this->db->query("SELECT * FROM assigned_orphan WHERE `orphan_id`='".$postData['orphan_id']."' AND is_old='0' AND created < NOW() LIMIT 1;");
			
		if($yateemQuery->num_rows() > 0)
		{
			$sponser_id = $yateemQuery->row()->sponser_id;
			
			$this->db->where('sponser_id',$sponser_id);
			$this->db->where('orphan_id',$postData['orphan_id']);
			
			$data	=	array('is_old'	=>	'1', 'end_date'	=>	date('Y-m-d'));
			$this->db->update('assigned_orphan',json_encode($data),$userid,$data);
		}
			
		$comments	=	array('orphan_id'	=>	$postData['orphan_id'],	'reason'	=>	$postData['reason'],	'comment'	=>	$postData['comment'],'userid'	=>	$this->_login_userid);

		$this->db->insert('assigned_orphan',$assignData,json_encode($assignData),$userid); // Insert all kafeel detail
		$this->db->insert('change_kafeel_detail',$comments,json_encode($comments),$userid);	// Insert Comments and reason
		
		/*$this->session->set_flashdata('success', 'لقد تم حذف السجلات');
		redirect(base_url().'yateem/yateem_list');
		exit();*/
		
		$this->load->library('user_agent');
		if ($this->agent->is_referral())
		{
			$url	=	 $this->agent->referrer();
		}
		
		$this->session->set_flashdata('msg', '* ملاحظة : تم حفط المعاملة بنجاح');
		redirect($url);
		exit();
	}
//-------------------------------------------------------------------------------

	/*
	* New Landing Page
	*/	
	public function orphan_sponsorship()
	{
		$branchid						=	$this->_data['user_detail']['profile']->branchid;
		$this->_data['moduleparent'] 	= 	224;
		$this->_data['permission'] 		= 	json_decode($this->_data['user_detail']['profile']->permissionjson,true);
		$this->_data['branchid']		=	$branchid;
		$this->_data['charity_type']	=	$charity_type;
		$this->_data['method_name']		=	'orphan_sponsorship_categories';
		
		$this->_data["sections"]  		=	$this->yateem->orphan_sponsorship_types('3',$branchid,NULL);

		// Those Orphans their age is equal or greater than 18
		$orphans		=	$this->yateem->get_yateeem_list_new("GREATER","3");

		if(!empty($orphans))
		{
			foreach($orphans	as $orphan)
			{	
				$orphan_id	=	$this->haya_model->check_notification_exist($orphan->orphan_id,$orphan->step,'ORPHAN',$orphan->orphan_status,'GREATER'); // Check if Befor this Orphan Exist
				
				if(!isset($orphan_id))
				{
					// For Notification
					$notification_data	=	array();
					
					$notification_data['step']				=	$orphan->step;
					$notification_data['notification_to']	=	'ORPHAN';
					$notification_data['reffrence_id']		=	$orphan->orphan_id;
					$notification_data['status']			=	$orphan->orphan_status;
					$notification_data['case']				=	'GREATER';
				
					$this->haya_model->add_action_for_notification($notification_data); // UPDATE Orphan Step
				}
			}
		}
		
		
		// For Expire Orphans
		$expire_orphans	=	$this->yateem->get_expire_yateeems_list("3","1");

		if(!empty($expire_orphans))
		{
			foreach($expire_orphans	as $orphan)
			{	
				$orphan_id	=	$this->haya_model->check_notification_exist($orphan->orphan_id,$orphan->step,'ORPHAN',$orphan->orphan_status,'EXPIRE'); // Check IS THIS Orphan Exist
				
				if(!isset($orphan_id))
				{
					// For Notification
					$notification_data	=	array();
					
					$notification_data['step']				=	$orphan->step;
					$notification_data['notification_to']	=	'ORPHAN';
					$notification_data['reffrence_id']		=	$orphan->orphan_id;
					$notification_data['status']			=	$orphan->orphan_status;
					$notification_data['case']				=	'EXPIRE';
				
					$this->haya_model->add_action_for_notification($notification_data); // UPDATE Orphan Step
				}
			}
		}

		$this->load->view('orphan-sponsorship-landingpage', $this->_data);
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories
	*/
	public function orphan_sponsorship_categories($branchid,$parent_id,$child_id,$section_name	=	NULL)
	{
		$branchid						=	$this->_data['user_detail']['profile']->branchid;
		$this->_data['branchid']		=	$branchid;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		$this->_data['section_name']	=	$section_name;

		$this->_data['method_name']		=	'orphan_sponsorship_countries';
		
		if($section_name	==	'YATEEM')
		{
			$this->_data['method_name']		=	'orphan_sponsorship_countries';
		}
		elseif($section_name	==	'GENERAL')
		{
			$this->_data['method_name']		=	'orphan_sponsorship_countries';
		}
		elseif($section_name	==	'KAFEEL')
		{
			$this->_data['method_name']		=	'sponsorship_countries';
		}
		else
		{
			$this->_data['method_name']		=	'updates_orphan_sponsorship_countries';
		}

		$this->_data["section_types"]   =	$this->yateem->orphan_sponsorship_types_categories($branchid,$parent_id);

		$this->load->view('orphan-sponsorship-landingpage-categories', $this->_data);
		
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Expired Land Page Categories LIST 
	*/
	public function expire_orphan_countries($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL,	$action	=	NULL)
	{
		$step	=	'3';
		if(isset($country))
		{
			if($country	==	'INSIDE')
			{
				$coloum_name	=	'country_id';
				$country_id		=	"`list_id`='200'";
				$counrty_parent	=	'0';
			}
			elseif($country	==	'OUTSIDE')
			{
				$coloum_name	=	'country_id !=';
				$country_id		=	"`list_id`!='200'";
				$counrty_parent	=	'0';
			}
			else
			{
				$coloum_name	=	'country_id';
				$country_id		=	'0';
				$counrty_parent	=	'0';
			}
		}
		
		if($action	==	'EXPIRE')
		{
			$this->_data["section_countries"]  	= 	$this->yateem->expire_orphan_countries($step,$branchid,'issuecountry',$country_id,$counrty_parent,"",$charity_type_id,$coloum_name,'0');

		}
		
		$this->_data['method_name']		=	'expire_orphan_provinces';
		
		$this->_data['section_name']		=	$section_name;
		
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;
		
		// View Countries List and show counts how many Orphans are Expires
		$this->load->view('expire-orphan-countries', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function expire_orphan_provinces($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		$step	=	3;
		// Get Listing Data from database for Orphans-Sponsorship
		$this->_data["section_provinces"]  	= 	$this->yateem->expire_orphan_provinces($step,$branchid,'issuecountry',$country,"",$charity_type_id,'2');

		
		$this->_data['method_name']			=	'expire_orphan_cities';
		
		$this->_data['section_name']		=	$section_name;
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;

		$this->load->view('expire-orphan-provinces', $this->_data);	
	}
//------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES LIST
	*/
	public function expire_orphan_cities($branchid,$orphan_type,$parent_id	=	NULL,$child_id,$section_name	=	NULL)
	{
		$step	=	'3';
		
		$this->_data['method_name']		=	'expire_orphan_lists';
		
		$this->_data["section_states"]	=	$this->yateem->expire_orphan_cities($step,$branchid,'issuecountry',$child_id,"",$charity_type_id,'2');
		
		$this->_data['section_name']	=	$section_name;
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		
		$this->load->view('expire-orphan-cities', $this->_data);	
	}
//--------------------------------------------------------------------------
	/*
	* Expire Orphans Listing
	*/
	public function expire_orphan_lists($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$orphan_type,$orphan_status)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['orphan_type']		=	$orphan_type;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		$this->_data['orphan_status']	=	$orphan_status;
		
		$this->load->view('expire-orphan-listing',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*
	* Expire Orphans Listing from AJAX
	*/	
	public function ajax_expire_yateem_list($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$orphan_status)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		$all_users	=	$this->yateem->get_expire_yateeems_list($step,$orphan_status,$child_id);

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				$action	=	'<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				$action	.=	'<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/renew_orphan/'.$lc->orphan_id.'"  href="#"><i class="myicon icon-mail-forward"></i></a>';

				//$url	=	'<a href="'.base_url().'yateem/yateem_approved_form/'.$lc->orphan_id.'">'.is_set($lc->orphan_name).'</a>';
				$url	=	is_set($lc->orphan_name);
				
				$img	=	'<img src="'.base_url().'">';
				
				$arr[] = array(
					"DT_RowId"				=>	$lc->orphan_id.'_durar_lm',
					"رقم المدني لليتيم"		=>	is_set(arabic_date($lc->file_number)),
					"اسم اليتيم" 			=>	$url,
					"الجنسية" 				=>	is_set($this->haya_model->get_name_from_list($lc->orphan_nationalty)),
					"تاريخ الميلاد" 			=>	is_set(arabic_date($lc->date_birth)),
					"الدولة" 				=>	is_set($this->haya_model->get_name_from_list($lc->country_id)),
					"المدينة" 				=>	is_set($this->haya_model->get_name_from_list($lc->city_id)),
					"تاريخ الموافقة" 		=>	date('Y-m-d',strtotime($lc->approved_date)),
					"تاريخ الانتهاء" 		=>	date('Y-m-d',strtotime($lc->expire_date)),
					"الإجرائات" 				=>	$action);
					
					unset($action);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Expired Land Page Categories LIST 
	*/
	public function cancel_sponsor_countries($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL,	$action	=	NULL)
	{
		$step	=	'1';
		if(isset($country))
		{
			if($country	==	'INSIDE')
			{
				$coloum_name	=	'country_id';
				$country_id		=	"`list_id`='200'";
				$counrty_parent	=	'0';
			}
			elseif($country	==	'OUTSIDE')
			{
				$coloum_name	=	'country_id !=';
				$country_id		=	"`list_id`!='200'";
				$counrty_parent	=	'0';
			}
			else
			{
				$coloum_name	=	'country_id';
				$country_id		=	'0';
				$counrty_parent	=	'0';
			}
		}
		
		if($action	==	'CANCEL')
		{
			$this->_data["section_countries"]  	= 	$this->yateem->cancel_sponsor_countries($step,$branchid,'issuecountry',$country_id,$counrty_parent,"",$charity_type_id,$coloum_name,'0');

		}
		
		$this->_data['method_name']		=	'cancel_sponsor_provinces';
		
		$this->_data['section_name']		=	$section_name;
		
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;
		
		// View Countries List and show counts how many Orphans are Expires
		$this->load->view('cancel-sponsor-countries', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function cancel_sponsor_provinces($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		$step	=	1;
		// Get Listing Data from database for Orphans-Sponsorship
		$this->_data["section_provinces"]  	= 	$this->yateem->cancel_sponsor_provinces($step,$branchid,'issuecountry',$country,"",$charity_type_id,'2');

		
		$this->_data['method_name']			=	'cancel_sponsor_cities';
		
		$this->_data['section_name']		=	$section_name;
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;

		$this->load->view('cancel-sponsor-provinces', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES LIST
	*/
	public function cancel_sponsor_cities($branchid,$orphan_type,$parent_id	=	NULL,$child_id,$section_name	=	NULL)
	{
		$step	=	'1';
		
		$this->_data['method_name']		=	'cancel_sponsor_lists';
		
		$this->_data["section_states"]	=	$this->yateem->cancel_sponsor_cities($step,$branchid,'issuecountry',$child_id,"",$charity_type_id,'2');
		
		$this->_data['section_name']	=	$section_name;
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		
		$this->load->view('cancel-sponsor-cities', $this->_data);	
	}
//--------------------------------------------------------------------------
	/*
	* Expire Orphans Listing
	*/
	public function cancel_sponsor_lists($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$orphan_type,$orphan_status)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['orphan_type']		=	$orphan_type;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		$this->_data['orphan_status']	=	$orphan_status;
		
		$this->load->view('cancel-sponsor-listing',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*
	* Expire Orphans Listing from AJAX
	*/	
	public function ajax_cancel_sponsor_list($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$orphan_status)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		$all_users	=	$this->yateem->get_cancel_sponsors_list($step,$child_id,$orphan_status);

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				$action = '<a onclick="alatadad(this);" data-url="'.base_url().'yateem/start_the_sponsor/'.$lc->sponser_id.'/START"  href="#"><i style="color:#008000;" class="myicon icon-refresh"></i></a> ';
				$action .= '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getSponserDetails/'.$lc->sponser_id.'" ><i class="icon-eye-open"></i></a>';
				//$action .= '<a class="iconspace" href="'.base_url().'yateem/add_kafeel/'.$lc->sponser_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				//$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_kafeel/'.$lc->sponser_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				
				$img = '<img src="'.base_url().'/resources/sponser/'.$lc->sponser_id.'/'.$lc->sponser_picture.'" width="50px" height="50px"> <br/>';
				$arr[] = array(
					"DT_RowId"		=>	$lc->sponser_id.'_durar_lm',
					"رقم الكفيل"	=>	arabic_date($lc->sponser_id),
					"رقم الهوية"	=>	arabic_date($lc->sponser_id_number),
					"اسم"			=>	$lc->sponser_name,
					"رقم الهاتف"	=>	$lc->sponser_phone_number,
					"الجنسية"		=>	$this->haya_model->get_name_from_list($lc->sponser_nationalty),
					"الإجرائات"		=>	$action);
					unset($action);
					unset($img);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Expired Land Page Categories LIST 
	*/
	public function cancelled_orphan_countries($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL,	$action	=	NULL)
	{
		$step	=	'3';
		if(isset($country))
		{
			if($country	==	'INSIDE')
			{
				$coloum_name	=	'country_id';
				$country_id		=	"`list_id`='200'";
				$counrty_parent	=	'0';
			}
			elseif($country	==	'OUTSIDE')
			{
				$coloum_name	=	'country_id !=';
				$country_id		=	"`list_id`!='200'";
				$counrty_parent	=	'0';
			}
			else
			{
				$coloum_name	=	'country_id';
				$country_id		=	'0';
				$counrty_parent	=	'0';
			}
		}

		if($action	==	'CANCEL')
		{
			$this->_data["section_countries"]  	= 	$this->yateem->cancel_orphan_countries($step,$branchid,'issuecountry',$country_id,$counrty_parent,"",$charity_type_id,$coloum_name,'0');
		}
		
		$this->_data['method_name']		=	'cancel_orphan_provinces';
		
		$this->_data['section_name']		=	$section_name;
		
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;
		
		// View Countries List and show counts how many Orphans are Expires
		$this->load->view('expire-orphan-countries', $this->_data);	
	}
	//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function cancel_orphan_provinces($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		$step	=	3;
		// Get Listing Data from database for Orphans-Sponsorship
		$this->_data["section_provinces"]  	= 	$this->yateem->cancel_orphan_provinces($step,$branchid,'issuecountry',$country,"",$charity_type_id,'2');

		
		$this->_data['method_name']			=	'cancel_orphan_cities';
		
		$this->_data['section_name']		=	$section_name;
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;

		$this->load->view('expire-orphan-provinces', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES LIST
	*/
	public function cancel_orphan_cities($branchid,$orphan_type,$parent_id	=	NULL,$child_id,$section_name	=	NULL)
	{
		$step	=	'3';
		
		$this->_data['method_name']		=	'cancel_orphan_lists';
		
		$this->_data["section_states"]	=	$this->yateem->cancel_orphan_cities($step,$branchid,'issuecountry',$child_id,"",$charity_type_id,'2');
		
		$this->_data['section_name']	=	$section_name;
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		
		$this->load->view('expire-orphan-cities', $this->_data);	
	}
//--------------------------------------------------------------------------
	/*
	* Expire Orphans Listing
	*/
	public function cancel_orphan_lists($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$orphan_type,$orphan_status)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['orphan_type']		=	$orphan_type;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		$this->_data['orphan_status']	=	$orphan_status;
		
		$this->load->view('cancel-orphan-listing',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	*
	* Expire Orphans Listing from AJAX
	*/
	public function ajax_cancel_yateem_list($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$orphan_status)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		$all_users	=	$this->yateem->get_cancel_yateems_list($step,$child_id,$orphan_status);

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				$action = '<a onclick="alatadad(this);" data-url="'.base_url().'yateem/start_the_orphan/'.$lc->orphan_id.'"  href="#"><i style="color:#008000;" class="myicon icon-refresh"></i></a> ';
				$action	.=	' <a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				//$action	.=	'<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/renew_orphan/'.$lc->orphan_id.'"  href="#"><i class="myicon icon-mail-forward"></i></a>';

				//$url	=	'<a href="'.base_url().'yateem/yateem_approved_form/'.$lc->orphan_id.'">'.is_set($lc->orphan_name).'</a>';
				$url	=	is_set($lc->orphan_name);
				
				$img	=	'<img src="'.base_url().'">';
				
				$arr[] = array(
					"DT_RowId"				=>	$lc->orphan_id.'_durar_lm',
					"رقم المدني لليتيم"		=>	is_set(arabic_date($lc->file_number)),
					"اسم اليتيم" 			=>	$url,
					"الجنسية" 				=>	is_set($this->haya_model->get_name_from_list($lc->orphan_nationalty)),
					"تاريخ الميلاد" 			=>	is_set(arabic_date($lc->date_birth)),
					"الدولة" 				=>	is_set($this->haya_model->get_name_from_list($lc->country_id)),
					"المدينة" 				=>	is_set($this->haya_model->get_name_from_list($lc->city_id)),
					"تاريخ الموافقة" 		=>	date('Y-m-d',strtotime($lc->approved_date)),
					"تاريخ الانتهاء" 		=>	date('Y-m-d',strtotime($lc->expire_date)),
					"الإجرائات" 				=>	$action);
					
					unset($action);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function orphan_sponsorship_countries($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		$step	=	'2';
		if(isset($country))
		{
			if($country	==	'INSIDE')
			{
				$coloum_name	=	'country_id';
				$country_id		=	"`list_id`='200'";
				$counrty_parent	=	'0';
			}
			elseif($country	==	'OUTSIDE')
			{
				$coloum_name	=	'country_id !=';
				$country_id		=	"`list_id`!='200'";
				$counrty_parent	=	'0';
			}
			else
			{
				$coloum_name	=	'country_id';
				$country_id		=	'0';
				$counrty_parent	=	'0';
			}
		}
		
		$country_type	=	$country;
		
		if($section_name	==	'YATEEM')
		{
			if($country	==	'REJECT')
			{
				$this->_data['method_name']		=	'reject_orphan_sponsorship_regions';
			}
			else
			{
				$this->_data['method_name']		=	'orphan_sponsorship_regions';
			}
			
		}
		else
		{
			if($country	==	'REJECT')
			{
				$this->_data['method_name']		=	'reject_orphan_sponsorship_regions';
			}
			else
			{
				$this->_data['method_name']		=	'sponsorship_regions';
			}	
		}
		
		
		if($country	==	'REJECT')
		{
			$this->_data["section_regions"]  	= 	$this->yateem->orphan_sponsorship_countries($step,$branchid,'issuecountry',$country_id,$counrty_parent,"",$charity_type_id,$coloum_name,'0');

		}
		else
		{
			$default	=	'2';
			$this->_data["section_regions"]  	= 	$this->yateem->orphan_sponsorship_countries($step,$branchid,'issuecountry',$country_id,$counrty_parent,"",$charity_type_id,$coloum_name,$default,$country_type);
		}
			
		// Get Listing Data from database for Orphans-Sponsorship
		// $this->_data["section_regions"]  	= 	$this->yateem->orphan_sponsorship_countries($step,$branchid,'issuecountry',$country_id,$counrty_parent,"",$charity_type_id,$coloum_name);
		$this->_data['section_name']		=	$section_name;
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;

		$this->load->view('orphan-sponsorship-countries', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function orphan_sponsorship_regions($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		// Get Listing Data from database for Orphans-Sponsorship
		$this->_data["section_regions"]  	= 	$this->yateem->orphan_sponsorship_regions('2',$branchid,'issuecountry',$country,"",$charity_type_id,'2');

		if($section_name	==	'YATEEM')
		{
			$this->_data['method_name']		=	'orphan_sponsorship_states';
		}
		else
		{
			$this->_data['method_name']		=	'sponsorship_states';
		}
		
		$this->_data['section_name']		=	$section_name;
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;

		$this->load->view('orphan-sponsorship-regions', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function reject_orphan_sponsorship_regions($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		// Get Listing Data from database for Orphans-Sponsorship
		$this->_data["section_regions"]  	= 	$this->yateem->orphan_sponsorship_regions('2',$branchid,'issuecountry',$country,"",$charity_type_id,'0');

		if($section_name	==	'YATEEM')
		{
			$this->_data['method_name']		=	'reject_orphan_sponsorship_states';
		}
		else
		{
			$this->_data['method_name']		=	'reject_sponsorship_states';
		}
		
		$this->_data['section_name']		=	$section_name;
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;

		$this->load->view('orphan-sponsorship-regions', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES LIST
	*/
	public function reject_orphan_sponsorship_states($branchid,$orphan_type,$parent_id	=	NULL,$child_id,$section_name	=	NULL)
	{
		$step	=	'2';
		
		if($section_name	==	'YATEEM')
		{
			$this->_data['method_name']		=	'reject_yateem_list_by_wilaya';
			$this->_data['orphan_status']	=	'0';
		}
		else
		{
			$this->_data['method_name']		=	'reject_kafeel_list_by_wilaya';
		}
		
		$this->_data["section_states"]	=	$this->yateem->orphan_sponsorship_states($step,$branchid,'issuecountry',$child_id,"",$charity_type_id,'0');
		$this->_data['section_name']	=	$section_name;
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		
		$this->load->view('orphan-sponsorship-states', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES LIST
	*/
	public function orphan_sponsorship_states($branchid,$orphan_type,$parent_id	=	NULL,$child_id,$section_name	=	NULL)
	{
		$step	=	'2';
		
		if($section_name	==	'YATEEM')
		{
			$this->_data['method_name']		=	'yateem_list_by_wilaya';
			$this->_data['orphan_status']	=	'2';
		}
		else
		{
			$this->_data['method_name']		=	'kafeel_list_by_wilaya';
		}
		
		$this->_data["section_states"]	=	$this->yateem->orphan_sponsorship_states($step,$branchid,'issuecountry',$child_id,"",$charity_type_id,'2');
		$this->_data['section_name']	=	$section_name;
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		
		$this->load->view('orphan-sponsorship-states', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function sponsorship_countries($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		$step	=	'0';
		
		if(isset($country))
		{
			if($country	==	'INSIDE')
			{
				$coloum_name	=	'country_id';
				$country_id		=	"`list_id`='200'";
				$counrty_parent	=	'0';
			}
			elseif($country	==	'OUTSIDE')
			{
				$coloum_name	=	'country_id !=';
				$country_id		=	"`list_id`!='200'";
				$counrty_parent	=	'0';
			}
			else
			{
				$coloum_name	=	'country_id';
				$country_id		=	'0';
				$counrty_parent	=	'0';
			}
		}
		
		//----------------------------------------------------------------------------------
		if($section_name	==	'YATEEM')
		{
			if($country	==	'REJECT')
			{
				$this->_data['method_name']		=	'reject_sponsorship_regions';
			}
			else
			{
				$this->_data['method_name']		=	'sponsorship_regions';
			}
			
		}
		else
		{
			if($country	==	'REJECT')
			{
				$this->_data['method_name']		=	'reject_sponsorship_regions';
			}
			else
			{
				$this->_data['method_name']		=	'sponsorship_regions';
			}	
		}
		
		
		if($country	==	'REJECT')
		{
			$reject_value	=	'0';
			$this->_data["section_regions"]  	= 	$this->yateem->sponsorship_countries($step,$branchid,'issuecountry',$country_id,$counrty_parent,"",$charity_type_id,$coloum_name,$reject_value);

		}
		else
		{
			$default	=	'2';
			$this->_data["section_regions"]  	= 	$this->yateem->sponsorship_countries($step,$branchid,'issuecountry',$country_id,$counrty_parent,"",$charity_type_id,$coloum_name,$default);
		}
		//----------------------------------------------------------------------------------
		
		// 0LD Coding
		/*if($section_name	==	'YATEEM')
		{
			$this->_data['method_name']		=	'orphan_sponsorship_regions';
		}
		else
		{
			$this->_data['method_name']		=	'sponsorship_regions';
		}*/
		

		// Get Listing Data from database for Orphans-Sponsorship
		//$this->_data["section_regions"]  	= 	$this->yateem->sponsorship_countries($step,$branchid,'issuecountry',$country_id,$counrty_parent,"",$charity_type_id,$coloum_name);
		$this->_data['section_name']		=	$section_name;
		
		//print_r($this->_data["section_regions"]);exit();
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;

		$this->load->view('orphan-sponsorship-countries', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function sponsorship_regions($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		$step	=	'0';
		
		// Get Listing Data from database for Orphans-Sponsorship
		$this->_data["section_regions"]  	= 	$this->yateem->sponsorship_regions($step,$branchid,'issuecountry',$country,"",$charity_type_id,'2');

		if($section_name	==	'YATEEM')
		{
			$this->_data['method_name']		=	'orphan_sponsorship_states';
		}
		else
		{
			$this->_data['method_name']		=	'sponsorship_states';
		}
		
		$this->_data['section_name']		=	$section_name;
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;

		$this->load->view('orphan-sponsorship-regions', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function reject_sponsorship_regions($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		$step	=	'0';
		
		// Get Listing Data from database for Orphans-Sponsorship
		$this->_data["section_regions"]  	= 	$this->yateem->sponsorship_regions($step,$branchid,'issuecountry',$country,"",$charity_type_id,'0');

		if($section_name	==	'YATEEM')
		{
			$this->_data['method_name']		=	'reject_orphan_sponsorship_states';
		}
		else
		{
			$this->_data['method_name']		=	'reject_sponsorship_states';
		}
		
		$this->_data['section_name']		=	$section_name;
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;

		$this->load->view('orphan-sponsorship-regions', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES LIST
	*/
	public function sponsorship_states($branchid,$orphan_type,$parent_id	=	NULL,$child_id,$section_name	=	NULL)
	{
		$step	=	'0';
		
		if($section_name	==	'YATEEM')
		{
			$this->_data['method_name']		=	'yateem_list_by_wilaya';
		}
		else
		{
			$this->_data['method_name']		=	'kafeel_list_by_wilaya';
			$this->_data['orphan_status']	=	'2';
		}
		
		$this->_data["section_states"]	=	$this->yateem->sponsorship_states($step,$branchid,'issuecountry',$child_id,"",$charity_type_id,'2');
		//print_r($this->_data["section_states"]);exit();
		$this->_data['section_name']	=	$section_name;
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	'0';
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		
		$this->load->view('orphan-sponsorship-states', $this->_data);	
	}
	//-------------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES LIST
	*/
	public function reject_sponsorship_states($branchid,$orphan_type,$parent_id	=	NULL,$child_id,$section_name	=	NULL)
	{
		$step	=	'0';

		if($section_name	==	'YATEEM')
		{
			$this->_data['method_name']		=	'reject_yateem_list_by_wilaya';
		}
		else
		{
			$this->_data['method_name']		=	'reject_kafeel_list_by_wilaya';
			$this->_data['orphan_status']	=	'0';
		}
		
		$this->_data["section_states"]	=	$this->yateem->sponsorship_states($step,$branchid,'issuecountry',$child_id,"",$charity_type_id,'0');
		//print_r($this->_data["section_states"]);exit();
		$this->_data['section_name']	=	$section_name;
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	'0';
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		
		$this->load->view('orphan-sponsorship-states', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function sponsors_waiting_countries($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		if(isset($country))
		{
			if($country	==	'INSIDE')
			{
				$coloum_name	=	'country_id';
				$country_id		=	"`list_id`='200'";
				$counrty_parent	=	'0';
			}
			elseif($country	==	'OUTSIDE')
			{
				$coloum_name	=	'country_id !=';
				$country_id		=	"`list_id`!='200'";
				$counrty_parent	=	'0';
			}
			else
			{
				$coloum_name	=	'country_id';
				$country_id		=	'0';
				$counrty_parent	=	'0';
			}
		}
		
		if($section_name	==	'WYATEEM')
		{
			$this->_data['method_name']		=	'orphan_sponsorship_regions';
		}
		else
		{
			$this->_data['method_name']		=	'sponsors_waiting_regions';
		}
		
		$step	=	'1';
		
		// Get Listing Data from database for Orphans-Sponsorship
		$this->_data["section_regions"]  	= 	$this->yateem->sponsorship_waiting_countries($step,$branchid,'issuecountry',$country_id,$counrty_parent,"",$charity_type_id,$coloum_name);
		$this->_data['section_name']		=	$section_name;
		
		//print_r($this->_data["section_regions"]);exit();
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;

		$this->load->view('orphan-sponsorship-countries', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function sponsors_waiting_regions($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		$step	=	'1';
		
		// Get Listing Data from database for Orphans-Sponsorship
		$this->_data["section_regions"]  	= 	$this->yateem->sponsorship_waiting_regions($step,$branchid,'issuecountry',$country,"",$charity_type_id);

		if($section_name	==	'YATEEM')
		{
			$this->_data['method_name']		=	'orphan_sponsorship_states';
		}
		else
		{
			$this->_data['method_name']		=	'sponsors_waiting_states';
		}
		
		$this->_data['section_name']		=	$section_name;
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;

		$this->load->view('orphan-sponsorship-regions', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES LIST
	*/
	public function sponsors_waiting_states($branchid,$orphan_type,$parent_id	=	NULL,$child_id,$section_name	=	NULL)
	{
		$step	=	'1';
		
		if($section_name	==	'YATEEM')
		{
			$this->_data['method_name']		=	'waiting_yateem_list_by_wilaya';
		}
		else
		{
			$this->_data['method_name']		=	'waiting_kafeel_list_by_wilaya';
		}
		
		$this->_data["section_states"]	=	$this->yateem->sponsors_waiting_states($step,$branchid,'issuecountry',$child_id,"",$charity_type_id);

		$this->_data['section_name']	=	$section_name;
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		
		$this->load->view('orphan-sponsorship-states', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function orphans_waiting_countries($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		$step	=	'3';
		if(isset($country))
		{
			if($country	==	'INSIDE')
			{
				$coloum_name	=	'country_id';
				$country_id		=	"`list_id`='200'";
				$counrty_parent	=	'0';
			}
			elseif($country	==	'OUTSIDE')
			{
				$coloum_name	=	'country_id !=';
				$country_id		=	"`list_id`!='200'";
				$counrty_parent	=	'0';
			}
			else
			{
				$coloum_name	=	'country_id';
				$country_id		=	'0';
				$counrty_parent	=	'0';
			}
		}
		
		if($section_name	==	'WYATEEM')
		{
			$this->_data['method_name']		=	'orphans_waiting_regions';
		}
		else
		{
			$this->_data['method_name']		=	'sponsorship_waiting_regions';
		}
		
		// Get Listing Data from database for Orphans-Sponsorship
		$this->_data["section_regions"]  	= 	$this->yateem->orphans_waiting_countries($step,$branchid,'issuecountry',$country_id,$counrty_parent,"",$charity_type_id,$coloum_name);
		
		$this->_data['section_name']		=	$section_name;
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;
		
		//echo '<pre>'; print_r($this->_data["section_regions"]);exit();

		$this->load->view('orphan-sponsorship-countries', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all Land Page Categories LIST
	*/
	public function orphans_waiting_regions($branchid,$parent_id,$child_id,$country	=	NULL,$section_name	=	NULL)
	{
		$step	=	'3';
		// Get Listing Data from database for Orphans-Sponsorship
		$this->_data["section_regions"]  	= 	$this->yateem->orphans_waiting_regions($step,$branchid,'issuecountry',$country,"",$charity_type_id);

		if($section_name	==	'WYATEEM')
		{
			$this->_data['method_name']		=	'orphan_waiting_states';
		}
		else
		{
			$this->_data['method_name']		=	'sponsorship_waiting_states';
		}
		
		$this->_data['section_name']		=	$section_name;
		$this->_data['branchid']			=	$branchid;
		$this->_data['parent_id']			=	$parent_id;
		$this->_data['child_id']			=	$child_id;

		$this->load->view('orphan-sponsorship-regions', $this->_data);	
	}
//-------------------------------------------------------------------------------

	/*
	* Get all SECTION CATEGORIES LIST
	*/
	public function orphan_waiting_states($branchid,$orphan_type,$parent_id	=	NULL,$child_id,$section_name	=	NULL)
	{
		$step	=	'3';
		
		if($section_name	==	'WYATEEM')
		{
			$this->_data['method_name']		=	'waiting_yateem_list_by_wilaya';
			$this->_data['orphan_status']	=	'1';
		}
		else
		{
			$this->_data['method_name']		=	'waiting_kafeel_list_by_wilaya';
		}
		
		$this->_data["section_states"]	=	$this->yateem->orphan_waiting_states($step,$branchid,'issuecountry',$child_id,"",$charity_type_id);
		$this->_data['section_name']	=	$section_name;
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		
		//echo '<pre>'; print_r($this->_data["section_states"]);exit();
		
		$this->load->view('orphan-sponsorship-states', $this->_data);
			
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function yateem_list_by_wilaya($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$orphan_type,$orphan_status)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['orphan_type']		=	$orphan_type;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		$this->_data['orphan_status']	=	$orphan_status;
		
		$this->load->view('yatem-listing-by-wilaya',$this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function reject_yateem_list_by_wilaya($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$orphan_type,$orphan_status)
	{

		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['orphan_type']		=	$orphan_type;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		$this->_data['orphan_status']	=	$orphan_status;
		$this->load->view('yatem-listing-by-wilaya',$this->_data);		
	}
	
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function waiting_yateem_list_by_wilaya($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$orphan_type,$orphan_status	=	'1')
	{
		$step	=	'3';
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['orphan_type']		=	$orphan_type;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		$this->_data['orphan_status']	=	$orphan_status;
		$this->_data['waiting_yateem']	=	'WAITING';
		
		$this->load->view('yatem-listing-by-wilaya',$this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function waiting_kafeel_list_by_wilaya($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['orphan_type']		=	$orphan_type;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		$this->_data['sponsor_status']	=	'1';
		
		$this->load->view('waiting-kafeel-listing-by-wilaya',$this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function kafeel_list_by_wilaya($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$orphan_type,$sponsor_status)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['orphan_type']		=	$orphan_type;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		$this->_data['sponsor_status']	=	$sponsor_status;
		
		$this->load->view('kafeel-listing-by-wilaya',$this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	function reject_kafeel_list_by_wilaya($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$orphan_type,$sponsor_status)
	{
		$this->_data['branchid']		=	$branchid;
		$this->_data['step']			=	$step;
		$this->_data['orphan_type']		=	$orphan_type;
		$this->_data['parent_id']		=	$parent_id;
		$this->_data['child_id']		=	$child_id;
		$this->_data['sponsor_status']	=	$sponsor_status;
		
		$this->load->view('kafeel-listing-by-wilaya',$this->_data);		
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	public function ajax_yateem_list_by_wilaya($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$orphan_status,$WAITING	=	NULL)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		$all_users	=	$this->yateem->get_yateeem_list_by_wilaya($step,$child_id,$orphan_status);

		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				$action	=	'<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				
				if(!$WAITING)
				{
					$url	=	'<a href="'.base_url().'yateem/yateem_approved_form/'.$lc->orphan_id.'">'.is_set($lc->orphan_name).'</a>';
				}
				else
				{
					$url	=	is_set($lc->orphan_name);
				}
				
				$img	=	'<img src="'.base_url().'">';
				
				$arr[] = array(
					"DT_RowId"				=>	$lc->orphan_id.'_durar_lm',
					"رقم المدني لليتيم"		=>	is_set(arabic_date($lc->file_number)),
					"اسم اليتيم" 			=>	$url,
					"الجنسية" 				=>	is_set($this->haya_model->get_name_from_list($lc->orphan_nationalty)),
					"تاريخ الميلاد" 			=>	is_set(arabic_date($lc->date_birth)),
					"الدولة" 				=>	is_set($this->haya_model->get_name_from_list($lc->country_id)),
					"المدينة" 				=>	is_set($this->haya_model->get_name_from_list($lc->city_id)),
					"تاريخ تقديم الطلب" 	=>	date('Y-m-d',strtotime($lc->created)),
					"الإجرائات" 				=>	$action);
					unset($action);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	public function ajax_kafeel_list_by_wilaya($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$sponsor_status)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];

		$all_users	=	$this->yateem->get_kafeel_list_by_wilaya($step,$child_id,$sponsor_status);
		
		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				$action  = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getSponserDetails/'.$lc->sponser_id.'" ><i class="icon-eye-open"></i></a>';
				//$action .= '<a class="iconspace" href="'.base_url().'yateem/add_kafeel/'.$lc->sponser_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				//$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_kafeel/'.$lc->sponser_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				$url	=	'<a class="iconspace" href="'.base_url().'yateem/approved_reject_kafeel/'.$lc->sponser_id.'">'.$lc->sponser_name.'</a>';
				
				$img = '<img src="'.base_url().'/resources/sponser/'.$lc->sponser_id.'/'.$lc->sponser_picture.'" width="50px" height="50px"> <br/>';
				$arr[] = array(
					"DT_RowId"		=>	$lc->sponser_id.'_durar_lm',
					/*"رقم الكفيل"	=>	arabic_date($lc->sponser_id),*/
					"رقم الهوية"	=>	arabic_date($lc->sponser_id_number),
					"اسم"			=>	$url,
					"الجنسية"		=>	$this->haya_model->get_name_from_list($lc->sponser_nationalty),
					"تاريخ الكفالة"	=>	arabic_date(date('Y-m-d',strtotime($lc->created))),
/*					"عدد الأيتام"	=>	'<a href="'.base_url().'yateem/yateem_search_view/'.$lc->sponser_id.'">('.arabic_date($this->haya_model->get_total_yateem_by_sponser($lc->sponser_id)).')</a>',
*/					"الإجرائات"		=>	$action);
					unset($action);
					unset($img);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	public function ajax_waiting_kafeel_list_by_wilaya($branchid,$step,$orphan_type,$parent_id	=	NULL,$child_id,$sponsor_status)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];

		$all_users	=	$this->yateem->get_kafeel_list_by_wilaya($step,$child_id,$sponsor_status);
		
		if(!empty($all_users))
		{
			foreach($all_users as $lc)
			{
				$action  = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getSponserDetails/'.$lc->sponser_id.'" ><i class="icon-eye-open"></i></a>';
				//$action .= '<a class="iconspace" href="'.base_url().'yateem/add_kafeel/'.$lc->sponser_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				//$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_kafeel/'.$lc->sponser_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				//$url	=	'<a class="iconspace" href="'.base_url().'yateem/approved_reject_kafeel/'.$lc->sponser_id.'">'.$lc->sponser_name.'</a>';
				
				if($lc->cancel	==	'0')
				{
					$status	=	'نشيط';
				}
				else
				{
					$status	=	'دي نشط';
				}
				
				$img = '<img src="'.base_url().'/resources/sponser/'.$lc->sponser_id.'/'.$lc->sponser_picture.'" width="50px" height="50px"> <br/>';
				$arr[] = array(
					"DT_RowId"			=>	$lc->sponser_id.'_durar_lm',
					/*"رقم الكفيل"		=>	arabic_date($lc->sponser_id),*/
					"رقم الهوية"		=>	arabic_date($lc->sponser_id_number),
					"اسم"				=>	$lc->sponser_name,
					"الجنسية"			=>	$this->haya_model->get_name_from_list($lc->sponser_nationalty),
					"تاريخ الموافقة"	=>	arabic_date(date('Y-m-d',strtotime($lc->approved_date))),
/*					"عدد الأيتام"		=>	'<a href="'.base_url().'yateem/yateem_search_view/'.$lc->sponser_id.'">('.arabic_date($this->haya_model->get_total_yateem_by_sponser($lc->sponser_id)).')</a>',
*/					"الحالة"			=>	$status,
					"الإجرائات"			=>	$action);

					unset($action);
					unset($img);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-------------------------------------------------------------------------------

	/*
	*
	*/
	function ajax_response()
	{
		$years	=	$this->input->post('years');//Get data from POST Method
		
		// Get Data from POST Method
		$this->_data['startdate'] 		=	strtotime($this->input->post('startdate'));
		$this->_data['enddate'] 		=	strtotime($this->input->post('enddate'));
		$this->_data['loopdate'] 		=	$this->_data['startdate'];
		
		// $this->_data['monthly_help']	=	$this->input->post('amount');
		$this->_data['monthly_help']	=	30;
		
		// Response for AJAX
		echo $html	=	$this->load->view("ajax-response-date-range",$this->_data,TRUE);
	}
//------------------------------------------------------------------------

  	/**
   	* Dynamic Forms Listing Page
   	* @param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]  	=	$this->haya_model->get_all_custom_form($moduleid);
		
		$this->_data["userid"] 	=	$this->_login_userid;
		
		$this->_data['formid']	=	$moduleid;
		 
		// Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);	 
	 }
//-----------------------------------------------------------------------
	/*
	* New Function For Approved/Reject Orphans
	*
	*/
	public function yateem_approved_form($id)
	{
		if(isset($id) && $id!="")
		{
			$this->_data['yateem_data'] 				=	$this->yateem->getYateemDataById($id); 			//	Get all Yateem Basic Data
			$this->_data['yateem_sisters'] 				=	$this->yateem->getBroSisNames($id,'sister'); 	//	Get all Yateem Sisters
			$this->_data['yateem_brothers'] 			=	$this->yateem->getBroSisNames($id,'brother');	//	Get all Yateem Brothers
			$this->_data['yateem_father_data'] 			=	$this->yateem->get_father_data($id); 			//	Get Yateem Father Detail
			$this->_data['yateem_mother_data'] 			=	$this->yateem->get_mother_data($id);			//	Get Yateem Mother Detail
			$this->_data['yateem_banks_data'] 			=	$this->yateem->getBanksData($id);				//	Get Bank Detail
			$this->_data['yateem_others_data'] 			=	$this->yateem->getOthersData($id);				//	Get Extra Information
			$this->_data['yateem_docs'] 				=	$this->yateem->getYateemDocument($id);			//	Get all required documents
			$this->_data['yateem_edu_data'] 			=	$this->yateem->get_yateem_others_data('ah_yateem_education',$id);
			$this->_data['yateem_can_data'] 			=	$this->yateem->get_yateem_others_data('ah_yateem_candidate',$id);
			$this->_data['yateem_sisters_brothers'] 	=	$this->yateem->getBroSisNamesNew($id,'sister');
			$this->_data['yateem_servay_list'] 			=	$this->yateem->get_orphan_step2_info($id); // Get Orphan Step 2 Detail
		}
		
		$this->load->view('approved-reject-yateem-form',$this->_data);	//	Add YATEEM form Page
	}
//----------------------------------------------------------------------- 
	/*
	*
	*
	*/
	public function approved_reject_kafeel($sponser_id)
	{
		if($sponser_id)
		{
			$this->_data['kafeel_data'] =   $this->yateem->getKafeelInfo($sponser_id);
			$this->_data['kafeel_docs'] = 	$this->yateem->getKafeelDocument($sponser_id);
		}
		
		$this->load->view('approved-reject-kafeel-form',$this->_data);
	}
//----------------------------------------------------------------------- 
	/*
	*
	*
	*/
	public function stop_the_orphan($id)
	{
		$this->_data['yateem_data'] 				=	$this->yateem->getYateemDataById($id);
		$this->_data['kafeel_list'] 				= 	$this->yateem->getKafeellist($id);
		
		$this->load->view('stop-the-orphan-form',$this->_data);
	}
//----------------------------------------------------------------------- 
	/*
	*
	*
	*/
	public function stop_the_spnosor($id,$action)
	{
		$this->_data['kafeel_data'] 	=   $this->yateem->getKafeelInfo($id);
		$this->_data['sponsor_action']	=	$action;
		
		$this->load->view('stop-start-the-sponsor-form',$this->_data);
	}
	
//----------------------------------------------------------------------- 
	/*
	*
	*
	*/
	public function start_the_orphan($id)
	{
		$this->_data['yateem_data'] 				=	$this->yateem->getYateemDataById($id);
		$this->_data['kafeel_list'] 				= 	$this->yateem->getKafeellist($id);
		
		$this->load->view('start-the-orphan-form',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	* cancel the Orphan Process
	* @param $orphan_id integer
	*/
	public function cancel_orphan_process($orphan_id)
	{
		$orphan_id	=	$this->input->post('orphan_id');
		$reason		=	$this->input->post('notes');
		
		$cancel_reason				=	array();
		$cancel_reason['orphan_id']	=	$orphan_id;
		$cancel_reason['notes']		=	$reason;
		$cancel_reason['userid']	=	$this->_login_userid;
				
		
		// (1) we have to Stop Sponsor if assign
		$this->yateem->stop_sponsor($orphan_id);
		
		// (2) Stop the Orphan
		$this->yateem->stop_orphan($orphan_id);
		
		// (3) Add CANCEL Reson into database
		$this->yateem->add_reason_for_stoping($cancel_reason,'ah_yateem_stop_detail');
		
		// For Notification
		$notification_data	=	array();
		
		$notification_data['step']				=	$this->input->post('step');
		$notification_data['notification_to']	=	'ORPHAN';
		$notification_data['reffrence_id']		=	$orphan_id;
		$notification_data['status']			=	$this->input->post('orphan_status');
		$notification_data['case']				=	"CANCEL";
		
		$this->haya_model->add_action_for_notification($notification_data); // UPDATE Orphan Step
		
		echo 'تم إلغاء اليتيم';
		exit();
	}
//-----------------------------------------------------------------------
	/*
	* Continue/Start the Orphan Process
	* @param $orphan_id integer
	*/
	public function continue_orphan_process($orphan_id)
	{
		$orphan_id	=	$this->input->post('orphan_id');
		$reason		=	$this->input->post('notes');
		
		$continue_reason				=	array();
		$continue_reason['orphan_id']	=	$orphan_id;
		$continue_reason['notes']		=	$reason;
		$continue_reason['userid']		=	$this->_login_userid;
		
		// (1) Stop the Orphan
		$this->yateem->start_orphan($orphan_id);
		
		// (2) Add CONTINUE Reson into database
		$this->yateem->add_reason_for_stoping($continue_reason,'ah_yateem_start_detail');
		
		// For Notification
		$notification_data	=	array();
		
		$notification_data['step']				=	$this->input->post('step');
		$notification_data['notification_to']	=	'ORPHAN';
		$notification_data['reffrence_id']		=	$orphan_id;
		$notification_data['status']			=	$this->input->post('orphan_status');
		
		$this->haya_model->add_action_for_notification($notification_data); // UPDATE Orphan Step
		
		echo 'وقد استمر اليتيم';
		exit();
	}
//-----------------------------------------------------------------------
	/*
	* cancel the Sonsor Process
	* @param $sponsor_id integer
	*/
	public function cancel_sponsor_process($sponsor_id)
	{
		$sponser_id			=	$this->input->post('sponser_id');
		$reason				=	$this->input->post('notes');
		$sponsor_action		=	$this->input->post('sponsor_action');
		
		$cancel_reason						=	array();
		$cancel_reason['sponser_id']		=	$sponser_id;
		$cancel_reason['notes']				=	$reason;
		$cancel_reason['sponsor_action']	=	$sponsor_action;
		$cancel_reason['userid']			=	$this->_login_userid;
		
		// (1) Stop the all Orphans by Spnosor
		$this->yateem->stop_all_orphans_by_sponsor($sponser_id);
		
		// (2) we have to Stop Sponsor if assign
		$this->yateem->cancel_the_spnosor($sponser_id);
		
		// (3) Add CANCEL Reson into database
		$this->yateem->add_reason_for_stoping_stoping($cancel_reason,'ah_sponsor_stop_start_detail');
		
		// For Notification
		$notification_data	=	array();
		
		$notification_data['step']				=	$this->input->post('step');
		$notification_data['notification_to']	=	'SPONSOR';
		$notification_data['reffrence_id']		=	$sponser_id;
		$notification_data['status']			=	$this->input->post('sponsor_status');
		$notification_data['case']				=	"CANCEL";
		
		$this->haya_model->add_action_for_notification($notification_data); // UPDATE Orphan Step
		
		echo 'تم إلغاء الرعاة';
		exit();
	}
//----------------------------------------------------------------------- 
	/*
	* 
	*
	*/
	public function start_the_sponsor($id,$action)
	{
		$this->_data['kafeel_data']		=	$this->yateem->getKafeelInfo($id);
		$this->_data['yateem_list']		=	$this->yateem->get_yateeem_list($id,$type);
		$this->_data['sponsor_action']	=	$action;
		
		$this->load->view('stop-start-the-sponsor-form',$this->_data);
	}
//-----------------------------------------------------------------------
	/*
	* cancel the Sonsor Process
	* @param $sponsor_id integer
	*/
	public function continue_sponsor_process($sponsor_id)
	{
		$sponser_id			=	$this->input->post('sponser_id');
		$reason				=	$this->input->post('notes');
		$sponsor_action		=	$this->input->post('sponsor_action');
		
		$continue_reason					=	array();
		$continue_reason['sponser_id']		=	$sponser_id;
		$continue_reason['notes']			=	$reason;
		$continue_reason['sponsor_action']	=	$sponsor_action;
		$continue_reason['userid']			=	$this->_login_userid;		
		
		// (1) we have to Stop Sponsor if assign
		$this->yateem->continue_the_spnosor($sponser_id);
		
		// (2) Add CANCEL Reason into database
		$this->yateem->add_reason_for_stoping_stoping($continue_reason,'ah_sponsor_stop_start_detail');
		
		// For Notification
		$notification_data	=	array();
		
		$notification_data['step']				=	$this->input->post('step');
		$notification_data['notification_to']	=	'SPONSOR';
		$notification_data['reffrence_id']		=	$sponser_id;
		$notification_data['status']			=	$this->input->post('sponsor_status');
		
		$this->haya_model->add_action_for_notification($notification_data); // UPDATE Orphan Step
		
		
		echo 'تم إلغاء الرعاة';
		exit();
	}
//-----------------------------------------------------------------------
	/*
	* 
	* 
	*/
	public function see_notify_orphans_sponsors($user_type,$status,$step,$type,$country,$case_param	=	NULL)
	{
		$this->_data['user_type']		=	$user_type;
		$this->_data['status']			=	$status	;
		$this->_data['step']			=	$step;
		$this->_data['type']			=	$type;
		$this->_data['country']			=	$country;
		$this->_data['case_param']		=	$case_param;
						
		if($user_type	==	'ORPHAN')
		{
			// Orphan Listing By Notificatios
			$this->load->view('see-notify-orphans',$this->_data);
		}
		else
		{
			// Sponsor Listing By Notificatios
			$this->load->view('see-notify-sponsors',$this->_data);
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	public function ajax_see_notify_sponsors($user_type,$status,$step,$type,$country,$case)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];

		$sponsors	=	$this->yateem->see_notify_orphans_sponsors($user_type,$status,$step,$type,$country,$case);

		if(!empty($sponsors))
		{
			foreach($sponsors as $lc)
			{		
				$read_notification	=	array(); // Define ARRAY
				
				$read_notification['read']				=	'1';
				$read_notification['user_who_view']		=	$this->_login_userid;
				
				$this->yateem->read_notification_orphans_sponsors($read_notification,$lc->sponser_id,$status,$step); // UPDATE the notification TO READ.
				
				$action  = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getSponserDetails/'.$lc->sponser_id.'" ><i class="icon-eye-open"></i></a>';

				$img = '<img src="'.base_url().'/resources/sponser/'.$lc->sponser_id.'/'.$lc->sponser_picture.'" width="50px" height="50px"> <br/>';
				$arr[] = array(
					"DT_RowId"		=>	$lc->sponser_id.'_durar_lm',
					/*"رقم الكفيل"	=>	arabic_date($lc->sponser_id),*/
					"رقم الهوية"	=>	arabic_date($lc->sponser_id_number),
					"اسم"			=>	$lc->sponser_name,
					"الجنسية"		=>	$this->haya_model->get_name_from_list($lc->sponser_nationalty),
					"تاريخ الكفالة"	=>	arabic_date(date('Y-m-d',strtotime($lc->created))),
/*					"عدد الأيتام"	=>	'<a href="'.base_url().'yateem/yateem_search_view/'.$lc->sponser_id.'">('.arabic_date($this->haya_model->get_total_yateem_by_sponser($lc->sponser_id)).')</a>',
*/					"الإجرائات"		=>	$action);
					unset($action);
					unset($img);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}

//-----------------------------------------------------------------------
	/*
	*
	*
	*/	
	public function ajax_see_notify_orphans($user_type,$status,$step,$type,$country,$case_param	=	NULL)
	{
		$text		=	$this->lang->line('orpahan');
		$labels		=	$text['orpahan']['listing'];
		
		$orphans	=	$this->yateem->see_notify_orphans_sponsors($user_type,$status,$step,$type,$country,$case_param);

		if(!empty($orphans))
		{
			foreach($orphans as $lc)
			{
				$read_notification	=	array(); // Define ARRAY
				
				$read_notification['read']				=	'1';
				$read_notification['user_who_view']		=	$this->_login_userid;
				
				$this->yateem->read_notification_orphans_sponsors($read_notification,$lc->orphan_id,$status,$step); // UPDATE the notification TO READ.
				
				$action	=	'<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				$url	=	is_set($lc->orphan_name);
				$img	=	'<img src="'.base_url().'">';
				
				$arr[] = array(
					"DT_RowId"				=>	$lc->orphan_id.'_durar_lm',
					"رقم المدني لليتيم"		=>	is_set(arabic_date($lc->file_number)),
					"اسم اليتيم" 			=>	$url,
					"الجنسية" 				=>	is_set($this->haya_model->get_name_from_list($lc->orphan_nationalty)),
					"تاريخ الميلاد" 			=>	is_set(arabic_date($lc->date_birth)),
					"الدولة" 				=>	is_set($this->haya_model->get_name_from_list($lc->country_id)),
					"المدينة" 				=>	is_set($this->haya_model->get_name_from_list($lc->city_id)),
					"تاريخ تقديم الطلب" 	=>	date('Y-m-d',strtotime($lc->created)),
					"الإجرائات" 				=>	$action);
					
					unset($action);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function get_all_orphan_by_status($step,$status,$country,$cancel,$type,$action	=	NULL)
	{
		$this->_data['step']		=	$step;
		$this->_data['status']		=	$status;
		$this->_data['country']		=	$country;
		$this->_data['cancel']		=	$cancel;
		$this->_data['type']		=	$type;
		$this->_data['action']		=	$action;
		
		$this->load->view('orphan-listing-by-wilaya',$this->_data);	
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function ajax_get_all_orphan_by_status($step,$status,$country,$cancel,$type,$action)
	{
		$all_orphans	=	$this->yateem->get_all_orphan_by_status($step,$status,$country,$cancel,$type,$action);
		if(!empty($all_orphans))
		{
			foreach($all_orphans as $lc)
			{
				$action	=	'<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$lc->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
				
				if(!$WAITING)
				{
					$url	=	'<a href="'.base_url().'yateem/yateem_approved_form/'.$lc->orphan_id.'">'.is_set($lc->orphan_name).'</a>';
				}
				else
				{
					$url	=	is_set($lc->orphan_name);
				}
				
				$img	=	'<img src="'.base_url().'">';
				
				$arr[] = array(
					"DT_RowId"				=>	$lc->orphan_id.'_durar_lm',
					"رقم المدني لليتيم"		=>	is_set(arabic_date($lc->file_number)),
					"اسم اليتيم" 			=>	$url,
					"الجنسية" 				=>	is_set($this->haya_model->get_name_from_list($lc->orphan_nationalty)),
					"تاريخ الميلاد" 			=>	is_set(arabic_date($lc->date_birth)),
					"الدولة" 				=>	is_set($this->haya_model->get_name_from_list($lc->country_id)),
					"المدينة" 				=>	is_set($this->haya_model->get_name_from_list($lc->city_id)),
					"تاريخ تقديم الطلب" 	=>	date('Y-m-d',strtotime($lc->created)),
					"الإجرائات" 				=>	$action);
					
					unset($action);
			}
			
			/*$ex['data'] = $arr;
			echo json_encode($ex);*/	
			
			checkArraySize($arr);		
		}
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function get_all_sponsors_by_status($step,$status,$country,$cancel,$type)
	{
		$this->_data['step']		=	$step;
		$this->_data['status']		=	$status;
		$this->_data['country']		=	$country;
		$this->_data['cancel']		=	$cancel;
		$this->_data['type']		=	$type;
		
		$this->load->view('sponsors-listing-by-status',$this->_data);	
	}
//-----------------------------------------------------------------------
	/*
	*
	*
	*/
	public function ajax_get_all_sponsors_by_status($step,$status,$country,$cancel,$type)
	{
		$all_sponsors	=	$this->yateem->get_all_sponsors_by_status($step,$status,$country,$cancel,$type);
		
		if(!empty($all_sponsors))
		{
			foreach($all_sponsors as $lc)
			{
				$action  = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getSponserDetails/'.$lc->sponser_id.'" ><i class="icon-eye-open"></i></a>';
				//$action .= '<a class="iconspace" href="'.base_url().'yateem/add_kafeel/'.$lc->sponser_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				//$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$lc->orphan_id.'" data-url="'.base_url().'yateem/delete_kafeel/'.$lc->sponser_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				$url	=	'<a class="iconspace" href="'.base_url().'yateem/approved_reject_kafeel/'.$lc->sponser_id.'">'.$lc->sponser_name.'</a>';
				
				$img = '<img src="'.base_url().'/resources/sponser/'.$lc->sponser_id.'/'.$lc->sponser_picture.'" width="50px" height="50px"> <br/>';
				$arr[] = array(
					"DT_RowId"		=>	$lc->sponser_id.'_durar_lm',
					/*"رقم الكفيل"	=>	arabic_date($lc->sponser_id),*/
					"رقم الهوية"	=>	arabic_date($lc->sponser_id_number),
					"اسم"			=>	$url,
					"الجنسية"		=>	$this->haya_model->get_name_from_list($lc->sponser_nationalty),
					"تاريخ الكفالة"	=>	arabic_date(date('Y-m-d',strtotime($lc->created))),
/*					"عدد الأيتام"	=>	'<a href="'.base_url().'yateem/yateem_search_view/'.$lc->sponser_id.'">('.arabic_date($this->haya_model->get_total_yateem_by_sponser($lc->sponser_id)).')</a>',
*/					"الإجرائات"		=>	$action);
					unset($action);
					unset($img);
			}
			
			$ex['data'] = $arr;
			echo json_encode($ex);			
		}
	}
//-----------------------------------------------------------------------

	public function banks_listing()
	{
		echo "Here are the Banks Listing page.";
	}
//-----------------------------------------------------------------------
/* End of file yateem.php */
/* Location: ./application/modules/yateem/yateem.php */
}
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
      	 <?php $this->load->view('common/globalfilter', array('type'=>'wilaya','b'=>$branchid,'option'=>'kafeel')); ?>
        <div class="nav nav-tabs panel panel-default panel-block">
          <div class="tab-pane list-group active">           
            <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th>رقم الكفيل</th>
                  <th>اسم</th>
                  <th>الجنسية</th>
                 <th>رقم الهوية</th>
                  <th>عدد الأيتام</th>
                  <th>الإجرائات</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'yateem/all_assignedsponser_list/'.$sponser_id.'','columns_array'=>'{ "data": "رقم الكفيل" },
                { "data": "اسم" },
                { "data": "الجنسية" },
                { "data": "رقم الهوية" },
				{ "data": "عدد الأيتام" },
				{ "data": "الإجرائات" }')); ?>
</div>
</body>
</html>
<div class="row">
  <div class="row col-md-12">
      <h4 class="panel-title customhr">بيانات الكفيل</h4>
      <br clear="all" />
    <div class="col-md-6 form-group">
      <label class="text-warning">الرقم المدني:</label>
      <strong><?PHP echo $kafeel_data->sponser_id_number; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم:</label>
      <strong><?PHP echo $kafeel_data->sponser_name; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الجنسية: </label>
      <strong><?php echo $this->haya_model->get_name_from_list($kafeel_data->sponser_nationalty); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">رقم الهاتف: </label>
      <strong><?php echo $kafeel_data->sponser_phone_number; ?> </strong> </div>
       <br clear="all" />
    <form action="" method="POST" id="stop_sponsor" name="stop_sponsor" enctype="multipart/form-data" autocomplete="off">
      <br clear="all" />
        <div class="col-md-12 form-group">
     <h4 class="panel-title customhr">الملاحظات:</h4>
	 <textarea id="notes" name="notes" style="width: 100%; height:100px;"></textarea>
	 <button type="button" class="btn btn-success btn-lg" onclick="stop_sponsor_submit();"><i class="icon-keyboard"></i> حفظ</button>
	</div>
	<input type="hidden" name="sponser_id" id="sponser_id" value="<?php echo $kafeel_data->sponser_id;?>"/>
    <input type="hidden" name="sponsor_action" id="sponsor_action" value="<?php echo $sponsor_action;?>"/>
    <input type="hidden" name="step" id="step" value="<?php echo $kafeel_data->step;?>"/>
    <input type="hidden" name="sponsor_status" id="sponsor_status" value="<?php echo $kafeel_data->sponsor_status;?>"/>
    </form>
  </div>
  <div class="col-md-6 form-group"><div id="show-msg"></div></div>
  <br clear="all" />
</div>
<script>
function stop_sponsor_submit()
{
	var	sponsor_action	=	$("#sponsor_action").val();
	
	if(sponsor_action	==	'START')
	{
		var	url	=	config.BASE_URL+'yateem/continue_sponsor_process';
	}
	else
	{
		var	url	=	config.BASE_URL+'yateem/cancel_sponsor_process';
	}
	
	var fd = new FormData(document.getElementById("stop_sponsor"));
	
	var request = $.ajax({
	  url: url,
	  type: "POST",
	  data: fd,
	  enctype: 'multipart/form-data',
	  dataType: "html",
	  processData: false,  // tell jQuery not to process the data
	  contentType: false ,  // tell jQuery not to set contentType
	  beforeSend: function(){},
	  success: function(msg)
	  {
		  location.reload();
	  }
	});
}

$(function(){
/****************************************************************************/
		$('.startpayment').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
		dateFormat:'yy-mm-dd',
		yearRange: "-90:+0",
        onClose: function(dateText, inst) { }
    });
/****************************************************************************/	
	 $(".startpayment").on("change",function(){
        var selected = $(this).val();
		var years	=	parseInt(1);

		var myDate = new Date(selected);
		console.log(myDate);
		myDate.setFullYear(myDate.getFullYear() + years);
		myDate.setDate(myDate.getDate() - years);
		
		// SET APPROVED & EXPIRE DATE VALUE INTO HIDDEN FIELDS
		$('#approved_date').val(selected);
		$('#expire_date').val($.datepicker.formatDate('yy-mm-dd', myDate));
		
		// Only showing Expiry date for View
		$('#expire').html($.datepicker.formatDate('yy-mm-dd', myDate));
    });
/****************************************************************************/
});
</script>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<style>
.th
{
background: #029625 !important;
color: white !important;
font-size: 14px !important;
}
</style>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12" style="padding:0px;">
        <div class="col-md-12" style="padding: 12px 0px 12px 0px; background-color: #CCC; border-bottom: 2px solid #FFF;">
        <div style="text-align:center;"><h4>بحث الكفيل</h4></div>
        <form action="<?php echo current_url();?>" method="POST" id="user_form" name="user_form" >
          <div class="col-md-3">
            <label class="text-warning">الرقم المدني</label>
            <input type="text" class="form-control req" name="sponser_id_number" id="sponser_id_number" placeholder="الرقم المدني" value=""/>
          </div>
          <div class="col-md-3">
            <label class="text-warning">اسم</label>
            <input type="text" class="form-control req" name="sponser_name" id="sponser_name" placeholder="اسم اليتيم" value=""/>
          </div>
          
          <div class="col-md-3">
            <label class="text-warning">الجنسية:</label>
           <?php echo $this->haya_model->create_dropbox_list('sponser_nationalty','nationality','','','req'); ?> </div>
          <div class="form-group col-md-3">
            <label class="text-warning"> الهاتف</label>
            <input type="text" class="form-control NumberInput req" maxlength="8" name="sponser_phone_number" id="sponser_phone_number" placeholder="الهاتف" value=""/>
          </div>
           <div class="col-md-3"><br><button type="submit" class="btn btn-danger">بحث</button></div>  
           <br clear="all"> 
           </form>
        </div>
        <br clear="all">
        <table class="table table-bordered table-striped dataTable">
          <thead>          
            <tr class="th">
<!--              <th>رقم الكفيل</th>-->
              <th>رقم الهوية</th>
              <th>اسم</th>
              <th>الجنسية</th>   
              <th>تاريخ الكفالة</th>
              <th>عدد الأيتام</th>
              <th>المرحلة</th>
              <th>الإجرائات</th>
             </tr>
          </thead>
          <tbody>
          <?php if(!empty($return_result)):?>
          
          <?php foreach($return_result as $res) { ?>
          <?php 
		  		if($res->sponsor_status	==	'0')
				{
					$sponsor_status	=	'رفض';
				}
				else if($res->sponsor_status	==	'1')
				{
					$sponsor_status	=	'وافق';
				}
				else
				{
					$sponsor_status	=	'قيد المراجعة';
				}
				
				if($res->cancel	==	'0' AND $res->step	==	'1')
				{
					$action = '<a onclick="alatadad(this);" data-url="'.base_url().'yateem/stop_the_spnosor/'.$res->sponser_id.'/STOP"  href="#"><i style="color:#CC0000;" class="myicon icon-off"></i></a> ';
				}
				else
				{
					$action	=	'';
				}
				
				$action  .= ' <a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getSponserDetails/'.$res->sponser_id.'" ><i class="icon-eye-open"></i></a>';
				$action .= '<a class="iconspace" href="'.base_url().'yateem/add_kafeel/'.$res->sponser_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
				//$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$res->orphan_id.'" data-url="'.base_url().'yateem/delete_kafeel/'.$res->sponser_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				
				$img = '<img src="'.base_url().'/resources/sponser/'.$res->sponser_id.'/'.$res->sponser_picture.'" width="50px" height="50px"> <br/>';
		 ?>
          <?php $steps	=	 config_item('sponsor_step');?>
            <tr>
              <!--<td class="center"><?php echo arabic_date($res->sponser_id);?></td>-->
              <td class="right"><?php echo arabic_date($res->sponser_id_number);?></td>
              <td class="right"><?php echo $res->sponser_name;?></td>
              <td class="right"><?php echo $this->haya_model->get_name_from_list($res->sponser_nationalty);?></td>
              <td class="right"><?php echo date('Y-m-d',strtotime($res->created));?></td>
              <td class="center"><?php echo '<a href="'.base_url().'yateem/yateem_list/'.$res->sponser_id.'">('.arabic_date($this->haya_model->get_total_yateem_by_sponser($res->sponser_id)).')</a>';?></td>
              <td class="center">
			 	<?php
                if($res->step	==	'0' AND $res->sponsor_status	==	'2' )
                {
                    echo '<strong>'.$steps[$res->step].'</strong>';
                }
                if($res->step	==	'0' AND $res->sponsor_status	==	'1' )
                {
                    echo '<strong>'.$steps['approve'].'</strong>';
                }
				 if($res->step	==	'0' AND $res->sponsor_status	==	'0' )
                {
                    echo '<strong>'.$steps['reject'].'</strong>';
                }
				if($res->step	==	'1' AND $res->cancel	==	'1' )
                {
                    echo '<strong>'.$steps['cancel'].'</strong>';
                }
				if($res->step	==	'1' AND $res->cancel	==	'0' )
                {
                    echo '<strong>'.$steps[$res->step].'</strong>';
                }
                ?>
              </td>
              <td class="right"><?php echo $action;?></td>
            </tr>
          <?php } ?>
          <?php endif;?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
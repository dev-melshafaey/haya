<?php $segment	=	$this->uri->segment(1);?>
<?php $text		=	$this->lang->line($segment);?>
<?php $labels	=	$text['users']['form'];?>

<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<style type="text/css">
.nav-tabs 
{
	margin-bottom: 14px !important;
}
</style>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
      <?php if($msg):?>
      <div class="col-md-12">
      	<div style="padding: 22px 20px !important; background:#c1dfc9;">
        	<h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
        </div>
      </div>
      <?php endif;?>
      <div class="col-md-12">
        <form action="<?php echo base_url();?>yateem/add_assign_data" method="POST" id="user_form" name="user_form" autocomplete="off">
          <input type="hidden" name="sponser_id_number" id="sponser_id_number" value="<?php echo $yateem_data->p_c_address; ?>"/>
          <input type="hidden" name="sponser_id" id="sponser_id">
          <input type="hidden" name="orphcount" id="orphcount" class="req" placeholder="اختر واحد كأدنى حد" value="">
          <div class="col-md-6 haya_white_background" style="padding-top: 21px;">
            <div class="form-group col-md-7">
              <label class="text-warning">اسم الكفيل / رقم الكفيل : </label>
              <input type="text" class="form-control req" name="kafeel_name" id="kafeel_name" placeholder="اسم الكفيل" value="<?php echo $yateem_data->p_c_address; ?>"/>
            </div>
            <div class="form-group col-md-1" id="sponsor-pic"> <img class="kafeelphoto" src=""> </div>
            <div class="form-group col-md-4">
              <label class="text-warning">السنة المالية : </label>
              <?PHP number_drop_box('financial_year',$yateem_data->financial_year,$title='السنة المالية',date('Y'),date('Y')+10,'السنة المالية'); ?>
            </div>
            <div class="form-group col-md-4">
              <label class="text-warning">مدة الكفالة : </label>
              <?PHP kafala_duration("warranty", $yateem_data->warranty); ?>
            </div>
            <div class="form-group col-md-4">
              <label class="text-warning">طريقة الدفع : </label>
              <?PHP echo $this->haya_model->create_dropbox_list('payment_type','payment_type',$item->user_relation,0,'req'); ?> </div>
            <div class="form-group col-md-4">
              <label class="text-warning">نوع الكفالة : </label>
              <?PHP echo $this->haya_model->create_dropbox_list('sponser_type','sponser_type',$item->user_relation,0,'req'); ?> </div>
            <div class="form-group col-md-6">
              <label class="text-warning">القيمة الشهرية : </label>
              <input type="text" class="form-control req NumberInput" name="monthly_payment" id="monthly_payment" placeholder="القيمة الشهرية" value="<?php echo $yateem_data->p_c_address; ?>"/>
            </div>
            <div class="form-group col-md-6">
              <label class="text-warning">سنة كاملة : </label>
              <input id="all_months"  type="checkbox" name="all_months" />
              <input type="text" class="form-control req" name="years" id="years" placeholder="الشهرية" value="<?php echo $yateem_data->years; ?>" style="display:none;"/>
            </div>
            <br clear="all"/>
            <div class="form-group col-md-6">
              <label class="text-warning">بداية الدفع: </label>
              <input type="text" class="form-control req startpayment" name="start_payment_date" id="start_payment_date" placeholder="نهاية الدفع" value="<?php echo $yateem_data->p_c_address; ?>"/>
            </div>
            <div class="form-group col-md-6">
              <label class="text-warning">نهاية الدفع: </label>
              <input type="text" class="form-control req" name="end_payment_date" id="end_payment_date" placeholder="بداية الدفع" value="<?php echo $yateem_data->p_c_address; ?>"/>
            </div>
            <div class="form-group col-md-6">
              <label class="text-warning">قيمة الكفلة (ر.ع): </label>
              <input type="text" class="form-control req" name="total_payment" id="total_payment" placeholder="قيمة الكفلة" value="<?php echo $yateem_data->p_c_address; ?>"/>
            </div>
            <div class="form-group col-md-6">
              <label class="text-warning">سنة كاملة : </label>
              <br>
              <input id="payment_months_all"  type="checkbox" value="1" checked="" name="payment_months_all" tabindex="54">
            </div>
            <div class="form-group col-md-12">
              <input type="button" id="user_data" class="btn btn-success btn-lg" name="user_data"  value="حفظ" />
            </div>
            <br clear="all">
          </div>
          
          <!----------------------->
          <div class="col-md-6 haya_border_left haya_white_background">
            <h4 class="haya_h4">تفاصيل بدء الكفالة وبيانات اليتيم :</h4>
            <input type="text" class="form-control" placeholder="البحث بالاسم" id="keyword"/>
            <br clear="all"/>
            <div class="form-group col-md-12 form-control" style="height: 450px; overflow-x: hidden; overflow-y: scroll;">
              <div id="list-response">
                <?php				
					foreach($yateem as $ya) 
					{ 
						$filepath = realpath('resources/yateem/'.$ya->orphan_id.'/thumb_'.$ya->orphan_picture);
						if(file_exists($filepath))
						{
							$photo = base_url().'resources/yateem/'.$ya->orphan_id.'/thumb_'.$ya->orphan_picture;
						}
						else
						{
							$photo = base_url().'resources/yateem/noimg.jpg';
						}
					if($ya->kafeelcount ==	0)
					{
					?>
                <div class="col-md-12 form-group yateembox" id="orphen<?php echo $ya->orphan_id; ?>" style="margin-bottom:0px; border-bottom:1px solid #CCC;">
                <div class="col-md-1" style="padding-top: 15px;">
                    <input type="checkbox" class="checkOrphenSelection" name="yateem_id[]" value="<?php echo $ya->orphan_id; ?>" style="float:left;">
                </div>
                <div class="col-md-2" style="background-image:url(<?php echo $photo;?>); height: 60px; background-position: center; background-size: cover; border: 1px solid #CCC;"></div>
                <div class="col-md-6"><a onclick="alatadad(this);" data-url="<?php echo base_url(); ?>yateem/getYateemDetails/<?PHP echo $ya->orphan_id; ?>" href="#"><?PHP echo $ya->orphan_name; ?></a> (<?PHP echo arabic_date(calcualteAge($ya->date_birth)); ?> العمر)<br>
                    <?php echo $ya->list_name; ?><br>
                    <a onclick="alatadad(this);" data-url="<?php echo base_url(); ?>yateem/getKafeellist/<?php echo $ya->orphan_id; ?>" href="#">الكفيل (<?PHP echo arabic_date($ya->kafeelcount); ?>)</a></div>
                <div class="col-md-3" style="padding-top:8px;">
                    <input type="text" id="start_date_<?php echo $ya->orphan_id; ?>" name="starting_date_<?php echo $ya->orphan_id; ?>" placeholder="تاريخ بداية الكفالة (<?PHP echo $ya->orphan_name; ?>)" class="form-control starting_date col-md-2">
                </div>
                  <br clear="all">
                </div>
                <?php }
				} ?>
              </div>
            </div>
            <br clear="all">
          </div>
          <div id="show-ajax-response"></div>
          <br />
          <br />
        </form>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>

<script type="text/javascript">

/****************************************************************************/
/****************************************************************************/
function changeVal()
{
	op_id = $("#orphan_id").val();
	$("#yateem_id").val(op_id);		
}
/****************************************************************************/
/****************************************************************************/
function check_warranty(obj){
	war_val = $(obj).val();
	if(war_val == 'yearly')
	{
		$("#yearly_check").show();
	}
	else
	{
		$("#yearly_check").hide();
	}
}
/****************************************************************************/
/****************************************************************************/
function changeVal2()
{
	op_id = $("#sponser_id").val();
	$("#sponser_id_number").val(op_id);
			
}
/****************************************************************************/
/****************************************************************************/
$(document).ready(function (){

	$('.checkOrphenSelection').click(function()
	{
		var checkOrphenValue	=	$(this).val();
		var orphCount 			=	$('.checkOrphenSelection:checked').length;
		
		if(orphCount > 0)
		{	
			$('#orphcount').val(orphCount);	
		}
		else
		{	
			$('#orphcount').val('');
		}
				
		if($(this).is(':checked'))
		{
			$('#orphen'+checkOrphenValue).addClass('yateemboxSelected');	
			$('#start_date_'+checkOrphenValue).addClass('req');
		}
		else
		{
			$('#orphen'+checkOrphenValue).removeClass('yateemboxSelected');	
			$('#start_date_'+checkOrphenValue).removeClass('req').removeClass('parsley-error');
		}
	});
/****************************************************************************/
/****************************************************************************/	
	$('.dp').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
		dateFormat:'yy-mm-dd',
		yearRange: "-90:+0",
        onClose: function(dateText, inst) { }
    });
		$('.startpayment').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
		dateFormat:'yy-mm-dd',
		yearRange: "-90:+0",
        onClose: function(dateText, inst) { }
    });
/****************************************************************************/
/****************************************************************************/	
	 $(".startpayment").on("change",function(){
        var selected = $(this).val();
		var years	=	parseInt($("#years").val());

		var myDate = new Date(selected);
		myDate.setFullYear(myDate.getFullYear() + years);
		myDate.setDate(myDate.getDate() - years);
	
		$('#end_payment_date').val($.datepicker.formatDate('yy-mm-dd', myDate));
		
		var enddate	=	$.datepicker.formatDate('yy-mm-dd', myDate);
		var amount	=	$("#monthly_payment").val();
		
		var muzi = $.ajax({
			url: config.BASE_URL+'yateem/ajax_response',
			type:"POST",
			dataType:"html",
			data:{startdate:selected,enddate:enddate,years:years,amount:amount},
			beforeSend: function()	{},			
			success: function(msg)
			{
				$('#show-ajax-response').html(msg);
			}
		});
    });
/****************************************************************************/
/****************************************************************************/
	$("#years").keyup(function ()
	{
		
			var years	=	$("#years").val();
			var amount	=	$("#monthly_payment").val();
			
			if(years)
			{
				var total	=	(parseInt(years)*12)*parseInt(amount);
				
				$("#total_payment").val(parseInt(total));
			}
			else
			{
				$("#total_payment").val(0);
			}
			
			$('.endpayment').datepicker( {
			changeMonth: true,
			changeYear: true,
			showButtonPanel: false,
			dateFormat:'yy-mm-dd',
			yearRange: "-90:+"+years,
			onClose: function(dateText, inst) { }
    	});

			$('.starting_date').datepicker( {
			changeMonth: true,
			changeYear: true,
			showButtonPanel: false,
			dateFormat:'yy-mm-dd',
			yearRange: "-90:+"+years,
			onClose: function(dateText, inst) { }
    	});
	});
/****************************************************************************/

/****************************************************************************/
	$(".endpayment").click(function (){
			var years	=	$("#years").val();
			$('.endpayment').datepicker( {
			changeMonth: true,
			changeYear: true,
			showButtonPanel: false,
			dateFormat:'yy-mm-dd',
			yearRange: "-90:+",
			onClose: function(dateText, inst) { }
    	});
	});
/****************************************************************************/
/****************************************************************************/	
	$("#orphan_name").autocomplete({
        source: config.BASE_URL+'yateem/getYateemNames',
        minLength: 0,
		open:function(event, ui) 		{	check_my_session();	},
        select: function (event, ui) 	{		
				$("#orphan_id").val(ui.item.id);
	
		}
    });
/****************************************************************************/
/****************************************************************************/	
	$("#yateem_id").autocomplete({
        source: config.BASE_URL+'yateem/getYateemNames',
        minLength: 0,
		open:function(event, ui) 		{	check_my_session();	},
        select: function (event, ui) 	{		
				$("#yateem_id").val(ui.item.id);
				$("#orphan_id").val(ui.item.id);
				$("#orphan_name").val(ui.item.value);
				setTimeout('changeVal()',500);		
		}
    });
/****************************************************************************/

/****************************************************************************/	
		$("#kafeel_name").autocomplete({
        	source: config.BASE_URL+'yateem/getKafeelNames',
       	 	minLength: 3,
			open:function(event, ui) {
				check_my_session();
			},
        	select: function (event, ui)
			{
				$("#sponser_id").val(ui.item.id);
				$("#sponser_id_number").val(ui.item.spid);	
			},
			focus: function(event, ui)
			{
				var amigo = '<a onclick="alatadad(this);" data-url="'+config.BASE_URL+'yateem/getSponserDetails/'+ui.item.id+'"><img class="kafeelphoto" src="'+ui.item.pic+'"></a>';
				$("#sponsor-pic").html(amigo);
				}	
			});
/****************************************************************************/
/****************************************************************************/
	$("#sponser_id_number").autocomplete({
        source: config.BASE_URL+'yateem/getKafeelNames',
        minLength: 0,
		open:function(event, ui)
		{
			check_my_session();
		},
        select: function (event, ui)
		{
			$("#sponser_id").val(ui.item.id);
			$("#sponser_id_number").val(ui.item.id);
			$("#kafeel_name").val(ui.item.value);
			
			setTimeout('changeVal2()',500);
		}
    });
/****************************************************************************/
/****************************************************************************/
	$("#all_months").click(function (){
			month_status = $(this).is(':checked');
			if(month_status){
				//$('.months').prop('checked', true);
				$("#years").show();
			}
			else{
				//$('.months').prop('checked',false);
				$("#years").hide();
			}
		
	});
/****************************************************************************/
/****************************************************************************/	
	$("#payment_months_all").click(function(){
			payment_check_status  = $(this).is(':checked');	
			//alert(payment_check_status);
			///payment_method
			
			if(payment_check_status)
			{
				$('.payment_method').prop('checked', true);
			}
			else
			{
				$('.payment_method').prop('checked',false);
			}
	})
/****************************************************************************/
/****************************************************************************/	
	    $("#company_string").autocomplete({source: config.BASE_URL+"company/search_company",
  open: function( event, ui ) {
	  $(".ui-autocomplete").css("z-index",2000)
	  },
	  select: function( event, ui ) {
		  $("#companyid").val(ui.item.id);
		  //$("#company_string").removeClass("req");
		  }
  });
/****************************************************************************/
/****************************************************************************/  
  var MIN_LENGTH = 3;
  	$("#keyword").keyup(function() {
		var keyword = $("#keyword").val();
		//if (keyword.length >= MIN_LENGTH) {
			$.get(config.BASE_URL+"yateem/search_yateem", { keyword: keyword } )
			  .done(function( data ) {
				$("#list-response").html(data);
			  });
		//}
	});
});
</script> 
<!-- /.modal-dialog -->

</div>
</body>
</html>
<div class="row">
  <div class="row col-md-12">
      <h4 class="panel-title customhr">نموذج بيانات الأيتام والرعاية</h4>
    <div class="col-md-6 form-group">
      <label class="text-warning">الرقم المدني لليتيم:</label>
      <strong><?PHP echo $yateem_data->file_number; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم اليتيم:</label>
      <strong><?PHP echo $yateem_data->orphan_name; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الجنسية: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_data->orphan_nationalty); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning"> صورة):</label>
      <?php $statmentURL = base_url().'resources/yateem/'.$yateem_data->orphan_id.'/'.$yateem_data->orphan_picture; //echo getFileResult($statmentURL,'',$statmentURL);?>
      <img src="<?php echo $statmentURL; ?>"  width="100"/> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">تاريخ الميلاد: </label>
      <strong><?PHP echo show_date($yateem_data->date_birth,5); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الدولة: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_data->country_id); ?> </strong> </div>
    <div class="col-md-12 form-group">
      <label class="text-warning">المدينة: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_data->city_id); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">ص.ب: </label>
      <strong><?PHP echo $yateem_data->po_adress; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الرمز البريدي: </label>
      <strong><?PHP echo $yateem_data->pc_adress; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">اليتيم أخوته ترتيب بين: </label>
      <strong><?PHP echo $yateem_data->orphan_arrangement_brothers; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">عدد أفراد الأسرة: </label>
      <strong><?PHP echo $yateem_data->total_family_memebers; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الهاتف/ منزل: </label>
      <strong><?PHP echo arabic_date($yateem_data->phone_number); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">هاتف ولي الامر: مع رمز الدولة: </label>
      <strong><?PHP echo arabic_date($yateem_data->home_number); ?></strong> </div>
      <table style="width: 100% !important">
          <?php
       $kafeel_list_count = sizeof($kafeel_list);
	 	if($kafeel_list_count > 0)
		{
			//print_r($kafeel_list);	  
	   ?>
          <tr>
            <td class="customhr panel-title" style="border-bottom:1px solid #CCC;"> الكفيل</td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <?PHP foreach($kafeel_list as $kafeel) { ?>
                <tr>
                  <td class="Y_value"><?PHP echo is_set($kafeel->sponser_name); ?> (<?PHP echo is_set(arabic_date($kafeel->sponser_id_number)); ?>)</td>
                  <td class="Y_value"><?PHP echo is_set($this->haya_model->get_name_from_list($kafeel->sponser_nationalty)); ?></td>
                  <td class="Y_value"><?PHP echo is_set($kafeel->sponser_designation); ?></td>
                  <td class="Y_value"><?PHP echo is_set($kafeel->sponser_town_id); ?></td>
                  <td class="Y_value"><?PHP echo is_set($this->haya_model->get_name_from_list($kafeel->city_id)); ?></td>
                </tr>
                <?PHP } ?>
              </table></td>
          </tr>
          <?PHP 
	  }?>
        </table>
    <form action="" method="POST" id="continue_orphan_process" name="continue_orphan_process" enctype="multipart/form-data" autocomplete="off">
      <h4 class="panel-title customhr">اليتيم انتهاء التفاصيل</h4>
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ الموافقة : </label>
        <strong><?php echo date('Y-m-d',strtotime($yateem_data->approved_date));?></strong> 
        </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ الانتهاء : </label>
        <strong><?php echo date('Y-m-d',strtotime($yateem_data->expire_date));?></strong> </div>
      <br clear="all" />
        <div class="col-md-12 form-group">
     <h4 class="panel-title customhr">سبب الإستكمال:</h4>
	 <textarea id="notes" name="notes" style="width: 100%; height:100px;"></textarea>
	 <button type="button" class="btn btn-success btn-lg" onclick="continue_orphan_submit();"><i class="icon-keyboard"></i> حفظ</button>
	</div>
	<input type="hidden" name="orphan_id" id="orphan_id" value="<?php echo $yateem_data->orphan_id;?>"/>
    <input type="hidden" name="step" id="step" value="<?php echo $yateem_data->step;?>"/>
    <input type="hidden" name="orphan_status" id="orphan_status" value="<?php echo $yateem_data->orphan_status;?>"/>
    </form>
  </div>
  <div class="col-md-6 form-group"><div id="show-msg"></div></div>
  <br clear="all" />
</div>
<script>
function continue_orphan_submit()
{
	var fd = new FormData(document.getElementById("continue_orphan_process"));
	
	var request = $.ajax({
	  url: config.BASE_URL+'yateem/continue_orphan_process',
	  type: "POST",
	  data: fd,
	  enctype: 'multipart/form-data',
	  dataType: "html",
	  processData: false,  // tell jQuery not to process the data
	  contentType: false ,  // tell jQuery not to set contentType
	  beforeSend: function(){},
	  success: function(msg)
	  {
		 /* $('#renew_orphan_submit').attr('disabled',true);
		  $("#show-msg").html(msg);*/
		  
		  location.reload();
	  }
	});
}

$(function(){
/****************************************************************************/
		$('.startpayment').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
		dateFormat:'yy-mm-dd',
		yearRange: "-90:+0",
        onClose: function(dateText, inst) { }
    });
/****************************************************************************/	
	 $(".startpayment").on("change",function(){
        var selected = $(this).val();
		var years	=	parseInt(1);

		var myDate = new Date(selected);
		console.log(myDate);
		myDate.setFullYear(myDate.getFullYear() + years);
		myDate.setDate(myDate.getDate() - years);
		
		// SET APPROVED & EXPIRE DATE VALUE INTO HIDDEN FIELDS
		$('#approved_date').val(selected);
		$('#expire_date').val($.datepicker.formatDate('yy-mm-dd', myDate));
		
		// Only showing Expiry date for View
		$('#expire').html($.datepicker.formatDate('yy-mm-dd', myDate));
    });
/****************************************************************************/
});
</script>
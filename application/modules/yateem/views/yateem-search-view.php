<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<style>
.th
{
background: #029625 !important;
color: white !important;
font-size: 14px !important;
}
</style>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12" style="padding:0px;">
        <div class="col-md-12" style="padding: 12px 0px 12px 0px; background-color: #CCC; border-bottom: 2px solid #FFF;">
        <div style="text-align:center;"><h4>بحث الأيتام</h4></div>
        <form action="<?php echo current_url();?>" method="POST" id="user_form" name="user_form">
          <div class="col-md-3">
            <label class="text-warning">رقم الملف</label>
            <input type="text" class="form-control req" name="file_number" id="file_number" placeholder="رقم الملف" value=""/>
          </div>
          <div class="col-md-3">
            <label class="text-warning"> اسم اليتيم </label>
            <input type="text" class="form-control req" name="orphan_name" id="orphan_name" placeholder="اسم اليتيم" value="<?php echo $yateem_data->orphan_name; ?>"/>
          </div>
          <div class="col-md-3">
            <label class="text-warning">تاريخ الميلاد:</label>
            <input name="date_birth" value="" placeholder="تاريخ الميلاد" id="date_birth" type="text" class="form-control req age_datepicker" set-age="applicant_age" />
          </div>
          <div class="form-group col-md-3">
            <label class="text-warning">الدولة</label>
          <?php echo $this->haya_model->create_dropbox_list('country_id','issuecountry','',"0",''); ?> </div>
          <div class="form-group col-md-3">
            <label class="text-warning">المحافظة</label>
            <?PHP echo $this->haya_model->create_dropbox_list('muhafiza_id','muhafiza','','',''); ?> </div>
          <div class="form-group col-md-3">
            <label class="text-warning">المدينة</label>
            <?PHP echo $this->haya_model->create_dropbox_list('city_id','wilaya','','',''); ?> </div>

          <div class="form-group col-md-3">
            <label class="text-warning">الهاتف/ منزل</label>
            <input type="text" class="form-control NumberInput req" maxlength="8" name="phone_number" id="phone_number" placeholder="الهاتف/ منزل" value=""/>
          </div>
          <div class="form-group col-md-3">
            <label class="text-warning">هاتف ولي الامر: مع رمز الدولة</label>
            <input type="text" class="form-control NumberInput req" maxlength="25" name="home_number" id="home_number" placeholder="هاتف ولي الامر: مع رمز الدولة" value="<?php echo $yateem_data->home_number; ?>"/>
          </div>
           <div class="col-md-3"><br><button type="submit" class="btn btn-danger">بحث</button></div>  
           <br clear="all"> 
           </form>
        </div>
        <br clear="all">
        <table class="table table-bordered table-striped dataTable">
          <thead>          
            <tr class="th">
                  <th>الرقم المدني لليتيم</th>
                  <!--<th>رقم اليتيم/الكفيل</th>-->
                  <th>اسم اليتيم</th>
                  <th>الجنسية</th>
                  <th>تاريخ الميلاد</th>
                  <th>الدولة</th>
                  <th>المدينة</th>
				  <!--<th>تاريخ تقديم الطلب</th>-->
				  <!--<th>أسم الكفيل</th>-->
                  <th class="center">المرحلة</th>
                  <th class="center">الإجرائات</th>
             </tr>
          </thead>
          <tbody>
          <?php if(!empty($return_result)):?>
          <?php foreach($return_result as $res) { ?>
		  <?php
			  	if($res->orphan_status	==	'0')
				{
					$orphan_status	=	'رفض';
				}
				else if($res->orphan_status	==	'1')
				{
					$orphan_status	=	'وافق';
				}
				else
				{
					$orphan_status	=	'قيد المراجعة';
				}
			  
                $action = '<a  onclick="alatadad(this);" data-url="'.base_url().'yateem/getYateemDetails/'.$res->orphan_id.'"  href="#"><i class="icon-eye-open"></i></a>';
                $action .= '<a class="iconspace" href="'.base_url().'yateem/add_yateem/'.$res->orphan_id.'"><i style="color:#C90;" class="icon-edit-sign"></i></a>';
                //$action .= '<a class="iconspace" href="'.base_url().'yateem/change_kafeel/'.$res->orphan_id.'"><i style="color:#C90;" class="myicon icon-male"></i></a>';
                //$action .= '<a class="iconspace" href="#deleteDiag" onClick="show_delete_diag(this);" id="'.$res->orphan_id.'" data-url="'.base_url().'yateem/delete_yateeem/'.$res->orphan_id.'/"><i style="color:#CC0000;" class="icon-remove-sign"></i></a>';	
				  $name_kafeel	=	$this->haya_model->get_name_of_kafeel($res->orphan_id);
				  
				  if($name_kafeel)
				  {
					  $waiting	=	"";
				  }
				  else
				  {
					  	if($res->age	>=	'18')
						{
							$waiting	=	" --> ايتام فوق 18 سنة ";
						}
						else
						{
					  		$waiting	=	" --> ايتام في الانتظار";
						}
				  }
				?>
              <?php $steps	=	 config_item('orphan_step');;?>
                <tr>
                  <td class="center"><?php echo arabic_date($res->file_number);?></td>
                  <!--<td class="center"><?php echo $this->yateem->get_merge_id($res->orphan_id);?></td>-->
                  <td class="right"><?php echo $res->orphan_name;?></td>
                  <td class="right"><?php echo $this->haya_model->get_name_from_list($res->orphan_nationalty);?></td>
                  <td class="right"><?php echo arabic_date($res->date_birth);?></td>
                  <td class="right"><?php echo $this->haya_model->get_name_from_list($res->country_id);?></td>
                  <td class="right"><?php echo $this->haya_model->get_name_from_list($res->city_id);?></td>
                  <!--<td class="right"><?php echo date('Y-m-d',strtotime($res->created));?></td>-->
                  <!--<td class="center"><?php echo $this->haya_model->get_name_of_kafeel($res->orphan_id);?></td>-->
                  <td class="center">
				  <?php
				  	if($res->step	==	'1' AND $res->cancel	==	'0')
					{
						echo '<strong>'.$steps[$res->step].'</strong>';
					}
					if($res->step	==	'2' AND $res->orphan_status	==	'0')
					{
						echo '<strong>'.$steps['reject'].'</strong>';
					}
					if($res->step	==	'2' AND $res->orphan_status	==	'1')
					{
						echo '<strong>'.$steps['approve'].'</strong>';
					}
					if($res->step	==	'2' AND $res->orphan_status	==	'2')
					{
						echo '<strong>'.$steps['defult'].'</strong>';
					} 
					if($res->step	==	'3' AND $res->cancel	==	'1')
					{
						echo '<strong>'.$steps['cancel'].'</strong>';
					}
					if($res->step	==	'3' AND $res->cancel	==	'0')
					{
							echo '<strong>'.$steps['approve'].$waiting.'</strong>';
					}

				  ?>
                  </td>
                  <td class="right"><?php echo $action;?></td>
                </tr>
              <?php } ?>
          <?php endif;?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
</div>
</body>
</html>
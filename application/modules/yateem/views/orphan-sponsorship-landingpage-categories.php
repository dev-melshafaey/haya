<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript');?>
<?php $this->load->view('common/menu'); ?>
<style>
a.tooltips {
  position: relative;
  display: inline;
}
a.tooltips span {
  position: absolute;
  width:140px;
  color: #FFFFFF;
  background: #000000;
  height: 30px;
  line-height: 30px;
  text-align: center;
  visibility: hidden;
  border-radius: 6px;
}
a.tooltips span:after {
  content: '';
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -8px;
  width: 0; height: 0;
  border-top: 8px solid #000000;
  border-right: 8px solid transparent;
  border-left: 8px solid transparent;
}
a:hover.tooltips span {
  visibility: visible;
  opacity: 0.8;
  bottom: 70px;
  left: 50%;
  margin-left: -76px;
  z-index: 999;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/csshake.css">
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <?php

		 foreach($section_types as $section) 
		 { 
		 	if($section['list_id']	==	'1735' || $section['list_id']	==	'1738')
			{
				$country_id			=	'/INSIDE';
				
				if($section['list_id']	==	'1735')
				{
					$user_type		=	'ORPHAN';
					$status			=	'2';
					$step			=	'2';
					$type			=	'INSIDE';
					$country		=	'200';
					$cancel			=	'0';
					$case_param		=	'';
					
					$count	=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel);
					$url	=	base_url().'yateem/get_all_orphan_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type;
				}
				if($section['list_id']	==	'1738')
				{
					$user_type		=	'SPONSOR';
					$status			=	'2';
					$step			=	'0';
					$type			=	'INSIDE';
					$country		=	'200';
					$cancel			=	'0';
					$case_param		=	'';
					
					$count	=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel);
					$url	=	base_url().'yateem/get_all_sponsors_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type;
				}
			}
			elseif($section['list_id']	==	'1734' || $section['list_id']	==	'1737')
			{
				$country_id			=	'/OUTSIDE';
				
				if($section['list_id']	==	'1734')
				{
					$user_type		=	'ORPHAN';
					$status			=	'2';
					$step			=	'2';
					$type			=	'OUTSIDE';
					$country		=	'200';
					$cancel			=	'0';
					$case_param		=	'';
					
					$count		=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel);
					$url		=	base_url().'yateem/get_all_orphan_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type;
				}
				if($section['list_id']	==	'1737')
				{
					$user_type		=	'SPONSOR';
					$status			=	'2';
					$step			=	'0';
					$type			=	'OUTSIDE';
					$country		=	'200';
					$cancel			=	'0';
					$case_param		=	'';
					
					$count		=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel);
					$url		=	base_url().'yateem/get_all_sponsors_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type;
				}
			}
			elseif($section['list_id']	==	'1736' || $section['list_id']	==	'1739')
			{
				$country_id			=	'/REJECT';
				
				if($section['list_id']	==	'1736')
				{
					$user_type		=	'ORPHAN';
					$status			=	'0';
					$step			=	'2';
					$type			=	'REJECT';
					$country		=	'200';
					$cancel			=	'0';
					$case_param		=	'';
					
					$count	=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel);
					$url	=	base_url().'yateem/get_all_orphan_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type;
				}
				if($section['list_id']	==	'1739')
				{
					$user_type		=	'SPONSOR';
					$status			=	'0';
					$step			=	'0';
					$type			=	'REJECT';
					$country		=	'200';
					$cancel			=	'0';
					$case_param		=	'';
					
					$count	=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel);
					$url	=	base_url().'yateem/get_all_sponsors_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type;
				}
			}
			elseif($section['list_id']	==	'1741')
			{
				$section_name	=	'WYATEEM';
				$user_type		=	'ORPHAN';
				$status			=	'1';
				$step			=	'3';
				$type			=	'ALL';
				$country		=	'0';
				$cancel			=	'0';
				$case_param		=	'';
				
				$count		=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel);
				$url		=	base_url().'yateem/get_all_orphan_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type;
			}
			elseif($section['list_id']	==	'1740')
			{
				$section_name	=	'WKAFEEL';
				$user_type		=	'SPONSOR';
				$status			=	'1';
				$step			=	'1';
				$type			=	'ALL';
				$country		=	'0';
				$cancel			=	'0';
				$case_param		=	'';
				
				$count		=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel);
				$url		=	base_url().'yateem/get_all_sponsors_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type;
				
			}
			else
			{
				$country_id			=	'/0';
				
				if($section['list_id']	==	'1749')
				{
					$user_type		=	'ORPHAN';
					$status			=	'2';
					$step			=	'2';
					$type			=	'ALL';
					$country		=	'0';
					$case_param		=	'';
					$cancel			=	'0';
					
					$count		=	$this->yateem->get_notification_count_approval($user_type,$step,$status,$type,$country,$cancel);
					$url		=	base_url().'yateem/get_all_orphan_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type;
					
				}
				if($section['list_id']	==	'1750')
				{
					$user_type		=	'SPONSOR';
					$status			=	'2';
					$step			=	'0';
					$type			=	'ALL';
					$country		=	'0';
					$case_param		=	'';
					$cancel			=	'0';
					
					$count	=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel);
					$url	=	base_url().'yateem/get_all_sponsors_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type;
				}
			}
			
			if($section['list_id']	==	'1774')
			{
				$method_name	=	'banks_listing';
			}
			
			if($section['list_id']	==	'1741')
			{
				$method_name	=	'orphans_waiting_countries';
			}
			elseif($section['list_id']	==	'1740')
			{
				$method_name	=	'sponsors_waiting_countries';
			}
			elseif($section['list_id']	==	'1747')
			{
				$method_name	=	'changing_kafeel_listing';
			}
			elseif($section['list_id']	==	'1742')
			{
				$method_name	=	'yateem_list/0/0/GREATER';
				$user_type		=	'ORPHAN';
				$status			=	'1';
				$step			=	'3';
				$type			=	'ALL';
				$country		=	'0';
				$cancel			=	'0';
				$case_param		=	'/GREATER';
					
				$count	=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel,'GREATER');
				$url	=	"";
			}
			elseif($section['list_id']	==	'1748')
			{
				$method_name	=	'assignOrphanNew';
				$count			=	0;
				$url			=	"";
			}
			
			$action	=	NULL;
			
			// METHOD name for EXPIRE ORPHANS
			if($section['list_id']	==	'1743')
			{
				$method_name	=	'expire_orphan_countries';
				$action			=	'EXPIRE';
				$user_type		=	'ORPHAN';
				$status			=	'1';
				$step			=	'3';
				$type			=	'ALL';
				$country		=	'0';
				$cancel			=	'0';
				$case_param		=	'/EXPIRE';
					
				$count	=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel,'EXPIRE');
				$url	=	base_url().'yateem/get_all_orphan_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type.'/'.$action;
			
			}
			elseif($section['list_id']	==	'1746')
			{
				$method_name	=	'cancelled_orphan_countries';
				$action			=	'CANCEL';
				
				$user_type		=	'ORPHAN';
				$status			=	'1';
				$step			=	'3';
				$type			=	'ALL';
				$country		=	'0';
				$cancel			=	'1';
				$case_param		=	'/CANCEL';
					
				$count	=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel,'CANCEL');
				$url	=	base_url().'yateem/get_all_orphan_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type;
			
			}
			elseif($section['list_id']	==	'1745')
			{
				$method_name	=	'cancel_sponsor_countries';
				$action			=	'CANCEL';
				
				$user_type		=	'SPONSOR';
				$status			=	'1';
				$step			=	'1';
				$type			=	'ALL';
				$country		=	'0';
				$cancel			=	'1';
				$case_param		=	'/CANCEL';
				
				$count	=	$this->yateem->get_notification_count_approval($user_type,$status,$step,$type,$country,$cancel,'CANCEL');
				$url	=	base_url().'yateem/get_all_orphan_by_status/'.$step.'/'.$status.'/'.$country.'/'.$cancel.'/'.$type;
			}
			
			
			$tharki	=	1;
			if($tharki	==	1)
			{
		 	?>
        <?php $module 		=	$this->haya_model->get_module_name_icon(0,$section['list_id']); ?>
        <div class="col-md-2 main_body" id="<?php echo $section['list_id'];?>">
          <div id="bingo<?php echo $section['list_id'];?>" class="col-md-12 center main_icon"><i  class="<?php echo $module->module_icon;?>"></i></div>
          <div class="col-md-12 center main_heading"><a href="<?php echo base_url();?>yateem/<?php echo $method_name;?>/<?php echo $branchid;?>/<?php echo $parent_id;?>/<?php echo $section['list_id'];?><?php echo $country_id;?>/<?php echo $section_name;?>/<?php echo $action;	?>"> <?php echo $section['name'];?> </a> <br />
            <?php if($count	>	0):?>
            <a href="<?php echo base_url();?>yateem/see_notify_orphans_sponsors/<?php echo $user_type.'/'.$status.'/'.$step.'/'.$type.'/'.$country.$case_param;?>" class="tooltips">
            <div class="shake-slow shake-constant shake-constant--hover"><i class="icon-bell-alt " style="color:#090;"></i></div>
            <span>(<?php echo (isset($count)	?	$count: '0');?>)</span> </a>&nbsp;&nbsp;&nbsp;
            <?php endif;?>
            <?php if($url):?><a href="<?php echo $url;?>"><span class="myicon icon-sort-by-alphabet-alt"></span></a><?php endif;?>
          </div>
        </div>
        <?php } 
		 		} ?>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script language="javascript">
$(function(){
	$('.main_body').mouseover(function()
	{
		var objid = $(this).attr("id");
		$('#bingo'+objid).animo( { animation: ['tada', 'bounce','spin'], duration:2 });
	}).mouseout(function()
	{
		$('.spin').animo("cleanse");
	});
})
</script>
</div>
</body>
</html>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<style>
a.tooltips {
  position: relative;
  display: inline;
}
a.tooltips span {
  position: absolute;
  width:140px;
  color: #FFFFFF;
  background: #000000;
  height: 30px;
  line-height: 30px;
  text-align: center;
  visibility: hidden;
  border-radius: 6px;
}
a.tooltips span:after {
  content: '';
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -8px;
  width: 0; height: 0;
  border-top: 8px solid #000000;
  border-right: 8px solid transparent;
  border-left: 8px solid transparent;
}
a:hover.tooltips span {
  visibility: visible;
  opacity: 0.8;
  bottom: 70px;
  left: 50%;
  margin-left: -76px;
  z-index: 999;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/csshake.css">

<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
      <?php if($msg):?>
      <div class="col-md-12">
        <div style="padding: 22px 20px !important; background:#c1dfc9;">
          <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
        </div>
      </div>
      <?php endif;?>
      <div class="col-md-12">
        <?php
		 foreach($sections as $section) 
		 {
			 if($section['list_id']	==	'1730')
			 {
				 $section_name	=	'YATEEM';
			 }
			 elseif($section['list_id']	==	'1731')
			 {
				 $section_name	=	'KAFEEL';
			 }
			 elseif($section['list_id']	==	'1732')
			 {
				 $section_name	=	'GENERAL';
			 }
			 else
			 {
				$section_name	=	'YATEEMORPHAN';
			 }
			 
		 	//$tharki = $this->haya_model->get_permission_ids($moduleparent,$section['list_id'],$permission);
			$tharki	=	1;
			if($tharki	==	1)
			{
		 	?>
        <?php $module 		 =	$this->haya_model->get_module_name_icon(0,$section['list_id']); ?>
        <div class="col-md-2 main_body" id="<?php echo $section['list_id'];?>">
          <div id="bingo<?php echo $section['list_id'];?>" class="col-md-12 center main_icon"><i  class="<?php echo $module->module_icon;?>"></i></div>
          <div class="col-md-12 center main_heading"><a href="<?php echo base_url();?>yateem/<?php echo $method_name;?>/<?php echo $branchid;?>/<?php echo $section['list_id'];?>/0/<?php echo $section_name;?>"> <?php echo $section['name'];?> </a> <br/>
            
            </div>
        </div>
        <?php 
			} 
		 		} 
			?>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script language="javascript">
$(function(){

	$('.main_body').mouseover(function()
	{
		var objid = $(this).attr("id");
		$('#bingo'+objid).animo( { animation: ['tada', 'bounce','spin'], duration:2 });
	}).mouseout(function()
	{
		$('.spin').animo("cleanse");
	});
})
</script>
</div>
</body>
</html>
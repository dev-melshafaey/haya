<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="panel panel-default panel-block">
          <div class="tab-content panel panel-default panel-block">
            <div class="tab-pane list-group active" id="tabsdemo-1">
              <div class="list-group-item">
                <div class="panel-heading text-overflow-hidden">
                  <div class="col-md-12">
                    <div class="row">
                      <table style="text-align:center;" class="table" id="tableSortable2" aria-describedby="tableSortable_info">
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                          <tr class="gradeA odd" bgcolor="#f5f5f5">
                            <td class=" sorting_1" colspan="2"><i class="<?php //echo $this->dashboard->get_icon('123');?>"></i>
                              <p><h1 style="float:none !important;">المحافظة</h1></p></td>
                          </tr>
                          <?php if($section_name	==	'YATEEM'):?>
                          <?php foreach($section_regions as $region) { ?>
                          <tr class="gradeA even">
                            <td><a href="<?php echo base_url();?>yateem/<?php echo $method_name;?>/<?php echo $branchid;?>/<?php echo $parent_id;?>/<?php echo $child_id;?>/<?php echo $region->list_id;?>/<?php echo $section_name;?>"><?php echo $region->list_name;?></a></td>
                            <td><?php echo $region->total;?></td>
                          </tr>
                          <?PHP } ?>
                          <?php else:?>
                          <?php //echo '<pre>'; print_r($section_regions);exit();?>
                          <?php foreach($section_regions as $region) { ?>
                          <tr class="gradeA even">
                            <td><a href="<?php echo base_url();?>yateem/<?php echo $method_name;?>/<?php echo $branchid;?>/<?php echo $parent_id;?>/<?php echo $child_id;?>/<?php echo $region['list_id'];?>/<?php echo $section_name;?>"><?php echo $region['name'];?></a></td>
                            <td><?php echo $region['y'];?></td>
                          </tr>
                          <?PHP } ?>
                          <?php endif;?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script language="javascript">
$(function(){
	$('.main_body').mouseover(function()
	{
		var objid = $(this).attr("id");
		$('#bingo'+objid).animo( { animation: ['tada', 'bounce','spin'], duration:2 });
	}).mouseout(function()
	{
		$('.spin').animo("cleanse");
	});
})
</script>
</div>
</body>
</html>
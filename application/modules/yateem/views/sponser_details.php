<?php 
if($kafeel_data->sponsor_status	==	'0')
{
	echo ' <h2 style="text-align:center;">تم رفض '.$kafeel_data->sponser_name.'</h2>';
	echo '<h2 style="text-align:center;">'.$kafeel_data->reason_about_status.'</h2>';
}
?>
<div class="row fox">
  <div class="col-md-12" style="direction:rtl !important;" id="sponsor_print">
    <table style="width: 100% !important">
      <tr>
        <td class="customhr panel-title" style="border-bottom:1px solid #CCC;">بيانات الكفيل</td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <!--<tr>
              <td class="y_head">رقم المولف:</td>
              <td class="Y_value"><?PHP echo arabic_date($kafeel_data->file_number); ?></td>
            </tr>-->
            <tr>
              <td class="y_head">اسم:</td>
              <td class="Y_value"><?php echo $kafeel_data->sponser_name; ?></td>
              <td rowspan="4" class="y_image"><?php $statmentURL = base_url().'resources/sponser/'.$kafeel_data->sponser_id.'/'.$kafeel_data->sponser_picture;?>
              <?php if($kafeel_data->sponser_picture):?>
               <div class="col-md-12" style="background-image:url(<?php echo $statmentURL; ?>); height: 125px;background-position: center;background-size: 133px;background-repeat: no-repeat;"> </div>
              <?php endif;?></td>
            </tr>
            <tr>
              <td class="y_head">الرقم المدني:</td>
              <td class="Y_value"><?php echo arabic_date($kafeel_data->sponser_id_number); ?></td>
            </tr>
            <tr>
              <td class="y_head">تاريخ انتها:</td>
              <td class="Y_value"><?php echo arabic_date($kafeel_data->sponser_id_expire); ?></td>
            </tr>
            <tr>
              <td class="y_head">الجنسية:</td>
              <td class="Y_value"><?php echo $this->haya_model->get_name_from_list($kafeel_data->sponser_nationalty); ?></td>
            </tr>
            <tr>
              <td class="y_head">الوظيفة:</td>
              <td class="Y_value"><?php echo $kafeel_data->sponser_designation; ?></td>
            </tr>
            <tr>
              <td class="y_head">رقم الهاتف:</td>
              <td class="Y_value"><?php echo $kafeel_data->sponser_phone_number; ?></td>
            </tr>
            <tr>
              <td class="y_head">رقم الهاتف:</td>
              <td class="Y_value"><?php echo $kafeel_data->sponser_phone_number_extra; ?></td>
            </tr>
            <tr>
              <td class="y_head">مكان العمل:</td>
              <td class="Y_value"><?php echo $this->haya_model->get_name_from_list($kafeel_data->sponser_work_place); ?></td>
            </tr>
            <tr>
              <td class="y_head">الجهة:</td>
              <td class="Y_value"><?php echo $this->haya_model->get_name_from_list($kafeel_data->place_name); ?></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td class="customhr panel-title" style="border-bottom:1px solid #CCC;">مكان العمل</td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="y_head">الدولة:</td>
              <td class="Y_value"><?PHP echo $this->haya_model->get_name_from_list($kafeel_data->country_id); ?></td>
              <td class="y_head">المحافظة:</td>
              <td class="Y_value"><?PHP echo $this->haya_model->get_name_from_list($kafeel_data->muhafiza_id); ?></td>
              <td class="y_head">المدينة:</td>
              <td class="Y_value"><?PHP echo $this->haya_model->get_name_from_list($kafeel_data->city_id); ?></td>
            </tr>
            <tr>
              <td class="y_head">البلدة:</td>
              <td class="Y_value"><?PHP echo arabic_date($kafeel_data->sponser_town_id); ?></td>
<!--              <td class="y_head">ص.ب:</td>
              <td class="Y_value"><?PHP echo arabic_date($kafeel_data->sponser_po_address); ?></td>-->
            </tr>
<!--            <tr>
              <td class="y_head">الرمز البريدي:</td>
              <td class="Y_value"><?PHP echo arabic_date($kafeel_data->sponser_pc_address); ?></td>
              <td class="y_head">االفاكس:</td>
              <td class="Y_value"><?PHP echo arabic_date($kafeel_data->sponser_fax); ?></td>
            </tr>-->
<!--            <tr>
              <td class="y_head">رقم الهاتف:</td>
              <td class="Y_value"><?PHP echo arabic_date($kafeel_data->sponser_phone_number); ?></td>
              <td class="y_head">مكان العمل:</td>
              <td class="Y_value"><?PHP echo arabic_date($kafeel_data->sponser_work_place); ?></td>
            </tr>-->
<!--            <tr>
              <td class="y_head">النتقال:</td>
              <td class="Y_value"><?PHP echo arabic_date($kafeel_data->sponser_mobile_number); ?></td>
              <td class="y_head">&nbsp;</td>
              <td class="Y_value">&nbsp;</td>
            </tr>-->
          </table></td>
      </tr>
<!--      <tr>
        <td class="customhr panel-title" style="border-bottom:1px solid #CCC;">البيانات بنكية</td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="y_head">اسم البنك:</td>
              <td class="Y_value"><?PHP echo $this->haya_model->get_name_from_list($kafeel_data->bankid); ?></td>
              <td class="y_head">الفرع:</td>
              <td class="Y_value"><?PHP echo $this->haya_model->get_name_from_list($kafeel_data->bankbranchid); ?></td>
              <td class="y_head">رقم الحساب:</td>
              <td class="Y_value"><?PHP echo arabic_date($kafeel_data->sponser_account_number); ?></td>
              <td class="y_head">اسم صاحب الحساب‎:</td>
              <td class="Y_value"><?PHP echo arabic_date($kafeel_data->accountfullname); ?></td>
            </tr>
          </table></td>
      </tr>-->
      <?PHP
      	$document = $this->haya_model->get_sponser_document($kafeel_data->sponser_id);	
		$documentSize = sizeof($document);
		if($documentSize > 0)
		{
	  ?>
      <tr>
        <td class="customhr panel-title" style="border-bottom:1px solid #CCC;">اترفق الوثائق والمستندات الازمة لطلب</td>
      </tr>
      <?PHP foreach($document as $ctid) { 
				$url = base_url().'resources/sponser/'.$kafeel_data->sponser_id.'/'.$ctid->document_name;
  ?>
      <tr>
        <td class="Y_value"><?PHP echo $ctid->documenttype; ?></td>
      </tr>
      <tr>
        <td class="Y_value"><?PHP echo filePreview($url,$ctid->document_name); ?></td>
      </tr>
      <?PHP } 
	  }
	  	$yateemCount = sizeof($yateem_list);
	 	if($yateemCount > 0)
		{  
	   ?>
      <tr>
        <td class="customhr panel-title" style="border-bottom:1px solid #CCC;">يتيم تحت هذا الكفيل</td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <?PHP foreach($yateem_list as $yl) { ?>
            <tr>
              <td class="Y_value"><?PHP echo is_set($yl->orphan_name); ?> (<?PHP echo is_set(arabic_date($yl->orphan_id)); ?>)</td>
              <td class="Y_value"><?PHP echo is_set($this->haya_model->get_name_from_list($yl->orphan_nationalty)); ?></td>
              <td class="Y_value"><?PHP echo is_set(arabic_date($yl->date_birth)); ?></td>
              <td class="Y_value"><?PHP echo is_set($this->haya_model->get_name_from_list($yl->country_id)); ?></td>
              <td class="Y_value"><?PHP echo is_set($this->haya_model->get_name_from_list($yl->city_id)); ?></td>
            </tr>
            <?PHP } ?>
          </table></td>
      </tr>
      <?php
	  }?>
    </table>
    <br clear="all" />
    <h4 class="panel-title customhr">إلغاء/إستكمال بيانات الكفيل</h4>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="y_head"><strong>تاريخ</strong></td>
              <td class="y_head"><strong>عمل</strong></td>
              <td class="y_head"><strong>الملاحظات</strong></td>
              <td class="y_head"></td>
            </tr>
            <?php if(!empty($sponsor_start_stop_info)):?>
            <?php foreach($sponsor_start_stop_info	as $detail):?>
            <?php
			if($detail->sponsor_action	==	'STOP')
			{
				$action	=	'إلغاء';
			}
			else
			{
				$action	=	'إستكمال';
			}
			$user_name	=	$this->haya_model->get_user_detail($orphan_detail->userid);
			?>
            <tr>
              <td class="Y_value"><?php echo $detail->submitted_date;?></td>
              <td class="Y_value"><?php echo $action;?></td>
              <td class="Y_value"><?php echo $detail->notes;?></td>
              <td class="Y_value"><?php echo $user_name['profile']->fullname; ?></td>
            </tr>
            <?php endforeach;?>
<?php endif;?>
          </table>
  </div>
  
  <div class="col-md-12 form-group">
        <h4 class="panel-title customhr">الملاحظات</h4>
    <div class="col-md-6 form-group">
      <strong><?PHP echo $kafeel_data->notes; ?></strong> </div>
      </div>
  <div class="col-md-12"  style="text-align:center !important;">
    <button type="button" class="btn btn-success" onclick="printthepage('sponsor_print');">طباعة</button>
  </div>
</div>
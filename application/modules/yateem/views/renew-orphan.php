<div class="row">
  <div class="row col-md-12">
    <form action="" method="POST" id="renew_orphan" name="renew_orphan" enctype="multipart/form-data" autocomplete="off">
      <h4 class="panel-title customhr">اليتيم انتهاء التفاصيل</h4>
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ الموافقة : </label>
        <strong><?php echo date('Y-m-d',strtotime($yateem_data->approved_date));?></strong> 
        </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ الانتهاء : </label>
        <strong><?php echo date('Y-m-d',strtotime($yateem_data->expire_date));?></strong> </div>
      <br clear="all" />
      <h4 class="panel-title customhr">تجديد اليتيم</h4>
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ الموافقة : </label>
        <strong><?php echo date('Y-m-d');?></strong> 
        <input type="text" class="form-control req startpayment" name="start_payment_date" id="start_payment_date" placeholder="نهاية الدفع" value="<?php echo date('Y-m-d'); ?>"/></div>
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ الانتهاء : </label>
        <strong id="expire"><?php echo date('Y-m-d', strtotime('+1 years'));?></strong> </div>
      <div class="col-md-12">
        <h4 class="yateem_h4">ارفاق الوثائق والمستندات الازمة لطلب</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
          <?PHP 
					$doccount = 0;
					foreach($this->inq->allRequiredDocument(205) as $ctid) { $doccount++; 
						$doc = $yateem_docs[$ctid->documentid];	
                        $url = 'resources/yateem/'.$doc->orphan_id.'/'.$doc->document_name;?>
          <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
            <div class="panel-heading" style="padding:10px 3px;" id="head<?PHP echo $ctid->documentid;?>">
              <h4 class="panel-title" style="font-size:15px;">
                <?PHP if($doc->orphan_id!='') { ?>
                <span class="icons" id="removeicons<?PHP echo $doc->document_id; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> <i onClick="removeDocument(this);" data-id="<?PHP echo $doc->document_id; ?>" data-remove="<?PHP echo $ctid->documentid; ?>" class="icon-remove-sign" style="color:#FF0000; cursor:pointer;"></i> </span>
                <?PHP } ?>
                <a style="width:95% !important;" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $ctid->documentid;?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
            </div>
            <div id="demo-collapse<?PHP echo $ctid->documentid;?>" class="panel-collapse collapse">
              <div class="panel-body" style="text-align:right;">
                <input type="file" name="doclist<?PHP echo $ctid->documentid;?>" placeholder="<?PHP echo $ctid->documenttype;?>" class="form-control <?PHP if($ctid->isrequired==1 && $doc->document_name=='') { echo 'req'; } ;?>">
              </div>
              
            </div>
          </div>
          <?PHP } ?>
        </div>
      </div>
      <input type="hidden" name="last_approved_date" id="last_approved_date" value="<?php echo date('Y-m-d',strtotime($yateem_data->approved_date));?>"/>
      <input type="hidden" name="last_expire_date" id="last_expire_date" value="<?php echo date('Y-m-d',strtotime($yateem_data->expire_date));?>"/>
      <input type="hidden" name="approved_date" id="approved_date" value="<?php echo date('Y-m-d');?>"/>
      <input type="hidden" name="expire_date" id="expire_date" value="<?php echo date('Y-m-d', strtotime('+1 years'));?>"/>
      <input type="hidden" name="orphan_id" id="orphan_id" value="<?php echo $orphan_id;?>"/>
    </form>
  </div>
  <div class="col-md-2 form-group">
    <input type="button" class="btn btn-success btn-lrg" name="submit"  id="renew_orphan_submit" onclick="renew_orphan_submit();" value="تجديد اليتيم" />
  </div>
  <div class="col-md-6 form-group"><div id="show-msg"></div></div>
  <br clear="all" />
</div>
<script>
function renew_orphan_submit()
{
	var fd = new FormData(document.getElementById("renew_orphan"));
	
	var request = $.ajax({
	  url: config.BASE_URL+'yateem/renew_orphan',
	  type: "POST",
	  data: fd,
	  enctype: 'multipart/form-data',
	  dataType: "html",
	  processData: false,  // tell jQuery not to process the data
	  contentType: false ,  // tell jQuery not to set contentType
	  beforeSend: function(){},
	  success: function(msg)
	  {
		  $('#renew_orphan_submit').attr('disabled',true);
		  $("#show-msg").html(msg);
	  }
	});
}

$(function(){
/****************************************************************************/
		$('.startpayment').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
		dateFormat:'yy-mm-dd',
		yearRange: "-90:+0",
        onClose: function(dateText, inst) { }
    });
/****************************************************************************/	
	 $(".startpayment").on("change",function(){
        var selected = $(this).val();
		var years	=	parseInt(1);

		var myDate = new Date(selected);
		console.log(myDate);
		myDate.setFullYear(myDate.getFullYear() + years);
		myDate.setDate(myDate.getDate() - years);
		
		// SET APPROVED & EXPIRE DATE VALUE INTO HIDDEN FIELDS
		$('#approved_date').val(selected);
		$('#expire_date').val($.datepicker.formatDate('yy-mm-dd', myDate));
		
		// Only showing Expiry date for View
		$('#expire').html($.datepicker.formatDate('yy-mm-dd', myDate));
    });
/****************************************************************************/
});
</script>
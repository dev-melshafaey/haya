<?PHP 
	$filepath = realpath('resources/yateem/'.$orphan_detail->orphan_id.'/thumb_'.$orphan_detail->orphan_picture);
	if(file_exists($filepath))
	{	$photo = base_url().'resources/yateem/'.$orphan_detail->orphan_id.'/thumb_'.$orphan_detail->orphan_picture; }
	else
	{	$photo = base_url().'resources/yateem/noimg.jpg'; }
?>

<div class="col-md-12" id="orphan_print">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
      <tr>
        <td class="yateem_td"><img src="<?PHP echo $photo; ?>" style="width:100px; float:right; margin-left: 13px;" >
          <h2 style="color:#029625 !important; font-weight:bold; letter-spacing:1px; margin: 5px !important;"><?PHP echo $orphan_detail->orphan_name; ?> (<?PHP echo arabic_date($orphan_detail->orphan_id); ?>)</h2>
          <h3 style="color:#029625 !important; font-weight:bold; letter-spacing:1px; margin: 5px !important;"><?PHP echo $orphan_detail->CountryName; ?>, <?PHP echo $orphan_detail->CityName; ?></h3>
          <h3 style="color:#029625 !important; font-weight:bold; letter-spacing:1px; margin: 5px !important;"><?PHP echo arabic_date($orphan_detail->phone_number); ?>, <?PHP echo arabic_date($orphan_detail->home_number); ?></h3></td>
      </tr>
      <?PHP foreach($kafeel_list as $k) { ?>
      <tr>
        <td  class="yateem_td"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="yateem_table">
            <tbody>
              <tr>
                <td width="40%" class="kafeel_td"><strong>اسم الكفيل :</strong> <?PHP echo $k->sponser_name; ?> (<?PHP echo arabic_date($k->sponser_id_number); ?>)</td>
                <td width="20%" class="kafeel_td"><strong>طريقة الدفع :</strong> <?PHP echo $k->payment_type; ?></td>
                <td width="20%" class="kafeel_td"><strong>نوع الكفالة :</strong> <?PHP echo $k->sponser_type; ?></td>
                <td width="20%" class="kafeel_td"><strong>تاريخ الكفالة :</strong> <?PHP echo arabic_date($k->starting_date); ?></td>
              </tr>
              <tr>
                <td class="kafeel_td"><strong>السنة المالية :</strong> <?PHP echo arabic_date($k->financial_year); ?></td>
                <td class="kafeel_td"><strong>القيمة الشهرية :</strong> <?PHP echo arabic_date($k->monthly_payment); ?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td class="kafeel_td"><strong>بداية الدفع :</strong> <?PHP echo arabic_date($k->start_payment_date); ?></td>
                <td class="kafeel_td"><strong>نهاية الدفع :</strong> <?PHP echo arabic_date($k->end_payment_date); ?></td>
                <td class="kafeel_td"><strong>قيمة الكفلة (ر.ع):</strong> <?PHP echo arabic_date($k->monthly_payment); ?></td>
                <td>&nbsp;</td>
              </tr>
            </tbody>
          </table></td>
      </tr>
      <?PHP } ?>
    </tbody>
  </table>
</div>

<?php //echo '<pre>'; print_r($yateem_data);?>
<?php 
if($yateem_data->orphan_status	==	'0')
{
	echo ' <h2 style="text-align:center;">تم رفض '.$yateem_data->orphan_name.'</h2>';
	echo '<h2 style="text-align:center;">'.$yateem_data->reason_about_status.'</h2>';
}

/*if($yateem_data->orphan_status	==	'1')
{
	echo ' <h2 style="text-align:center;">تمت الموافقة على '.$yateem_data->orphan_name.'</h2>';
	echo '<h2 style="text-align:center;">'.$yateem_data->reason_about_status.'</h2>';
}*/
//echo '<pre>';print_r($yateem_data);
$sponsor_name	=	$this->haya_model->get_name_of_kafeel($yateem_data->orphan_id);
?>

<div class="row">
  <div class="col-md-12 fox leftborder">
    <h4 class="panel-title customhr">نموذج بيانات الأيتام والرعاية</h4>
    <div class="col-md-6 form-group">
      <label class="text-warning">الرقم المدني لليتيم:</label>
      <strong><?PHP echo $yateem_data->file_number; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم اليتيم:</label>
      <strong><?PHP echo $yateem_data->orphan_name; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الجنسية: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_data->orphan_nationalty); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning"> صورة):</label>
      <?php $statmentURL = base_url().'resources/yateem/'.$yateem_data->orphan_id.'/'.$yateem_data->orphan_picture; //echo getFileResult($statmentURL,'',$statmentURL);?>
      <img src="<?php echo $statmentURL; ?>"  width="100"/> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">تاريخ الميلاد: </label>
      <strong><?PHP echo show_date($yateem_data->date_birth,5); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الدولة: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_data->country_id); ?> </strong> </div>
    <div class="col-md-12 form-group">
      <label class="text-warning">المدينة: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_data->city_id); ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">ص.ب: </label>
      <strong><?PHP echo $yateem_data->po_adress; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الرمز البريدي: </label>
      <strong><?PHP echo $yateem_data->pc_adress; ?> </strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">اليتيم أخوته ترتيب بين: </label>
      <strong><?PHP echo $yateem_data->orphan_arrangement_brothers; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">عدد أفراد الأسرة: </label>
      <strong><?PHP echo $yateem_data->total_family_memebers; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الهاتف/ منزل: </label>
      <strong><?PHP echo arabic_date($yateem_data->phone_number); ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">هاتف ولي الامر: مع رمز الدولة: </label>
      <strong><?PHP echo arabic_date($yateem_data->home_number); ?></strong> </div>
    <?php if($sponsor_name):?>
    <div class="col-md-12 form-group">
      <h4 class="panel-title customhr">أسم الكفيل	 : <?php echo $sponsor_name;?></h4>
    </div>
    <?php endif;?>
    <div class="col-md-12 form-group">
      <h4 class="panel-title customhr">الاخوة الذكور / الاخوة الاناث</h4>
      <table class="table table-bordered table-striped dataTable" id="females">
        <thead>
          <tr role="row">
            <th class="my_td right" style="width:50% !important;">الاسم</th>
            <th class="my_td right" style="width:20% !important;">جنس</th>
            <th class="my_td center" style="width:15% !important;">سنة الميلاد</th>
            <th class="my_td center" style="width:10% !important;">العمر</th>
          </tr>
        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
          <?php 
			  $total_males		=	0;
			  $total_females	=	0;
			  ?>
          <?php if(!empty($yateem_sisters_brothers)):?>
          <?php foreach($yateem_sisters_brothers as $sisters):?>
          <?php 
                  if($sisters->br_sis_type	==	'brother')
                  {
                      $gender	=	'بنين';
					  $total_males++;
                  }
                  else
                  {
                      $gender	=	'بنات';
					  $total_females++;
                  }
                  ?>
          <tr role="row">
            <td class="right" style="width:50% !important;"><?php echo $sisters->br_sis_name;?></td>
            <td class="center" style="width:20% !important;"><?php echo $gender;?></td>
            <td class="center" style="width:15% !important;"><?php echo $sisters->br_birthyear;?></td>
            <td class="center" style="width:10% !important;"><?php echo $sisters->br_age;?></td>
          </tr>
          <?php endforeach;?>
          <?php endif;?>
        </tbody>
      </table>
    </div>
    <div class="col-md-12 form-group">
      <div class="col-md-6 form-group">
        <label class="text-warning">عدد الذكور :</label>
        <strong><?php echo $total_males; ?></strong> </div>
      <div class="col-md-6 form-group">
        <label class="text-warning">عدد الاناث:</label>
        <strong><?php echo $total_females; ?></strong> </div>
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr">بيانات عن ولي الأمر</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
          <div class="col-md-6 form-group">
            <label class="text-warning">الاسم بالكامل:</label>
            <strong><?PHP echo $yateem_father_data->father_name; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">صلة القرابة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_father_data->parent_relationship); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">ص.ب: </label>
            <strong><?PHP echo $yateem_father_data->parent_po_address; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الرمز البريدي: </label>
            <strong><?PHP echo $yateem_father_data->parent_pc_address; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning"> رقم الهاتف: </label>
            <strong><?PHP echo arabic_date($yateem_father_data->father_phone_number); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">دولة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_father_data->parent_country_id); ?></strong> </div>
          <!--<div class="col-md-6 form-group">
      <label class="text-warning">منطقة: </label>
      <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_father_data->parent_region_id); ?></strong> </div>-->
          <div class="col-md-6 form-group">
            <label class="text-warning">ولاية: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_father_data->parent_wilaya_id); ?></strong> </div>
        </div>
      </div>
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr">بيانات الأم</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم الأم:</label>
            <strong><?PHP echo $yateem_mother_data->mother_name; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة المادية: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_mother_data->marital_status); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الهاتف: </label>
            <strong><?PHP echo $yateem_mother_data->mother_phone_number; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الرقم الدولي: </label>
            <strong><?PHP echo $yateem_mother_data->parent_country_code; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning"> رقم الهوية: </label>
            <strong><?PHP echo $yateem_mother_data->id_no; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">عنوان: </label>
            <strong><?PHP echo $yateem_mother_data->parent_address; ?></strong> </div>
        </div>
      </div>
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr"> البيانات بنكية</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم البنك:</label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_banks_data->bankid); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الفرع: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_banks_data->bankbranchid); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الحساب: </label>
            <strong><?PHP echo $yateem_banks_data->account_number; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم صاحب الحساب انجليزي: </label>
            <strong><?PHP echo $yateem_banks_data->account_title; ?></strong> </div>
		 <?php if($yateem_banks_data->credit_card_copy): ?>
            <div class="col-md-4 form-group">
            <label  class="text-warning">اضغط للعرض</label>
            <a class="fancybox-button" rel="gallery1" href="<?php echo base_url();?>/resources/yateem/<?php echo $yateem_data->orphan_id;?>/<?php echo $yateem_banks_data->credit_card_copy;?>"> <img src="<?php echo base_url();?>assets/images/filestypes/file_type_jpg.png" style="width:30px; height:30px" alt="" title="" oldtitle=""> </a> 
           </div>
          <?php endif;?>
        </div>
      </div>
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr"> بيانات أخرى</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
          <div class="col-md-6 form-group">
            <label class="text-warning">تاريخ وفاة الاب:</label>
            <strong><?PHP echo arabic_date($yateem_others_data->date_father_death); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">سبب الوفاة: </label>
            <strong><?PHP echo $yateem_others_data->reason_father_death; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">عدد أفراد الأسرة: </label>
            <strong><?PHP echo $yateem_others_data->family_members; ?></strong> </div>
          <!--<div class="col-md-6 form-group">
      <label class="text-warning">الاخوة: </label>
      <strong><?PHP echo $yateem_others_data->brothers; ?></strong> </div>--> 
          <!--<div class="col-md-6 form-group">
      <label class="text-warning">الأخوات: </label>
      <strong><?PHP echo $yateem_others_data->sisters; ?></strong> </div>-->
          <div class="col-md-6 form-group">
            <label class="text-warning">الظروف المعيششة للأسرة: </label>
            <strong><?PHP echo $yateem_others_data->living_condition_family; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الظروف السكينة: </label>
            <strong><?PHP echo $yateem_others_data->housing_condition; ?></strong> </div>
          <!--<div class="col-md-6 form-group">
      <label class="text-warning">الظروف الاقتصادية: </label>
      <strong><?PHP echo $yateem_others_data->economic_condition; ?></strong> </div>-->
          <div class="col-md-6 form-group">
            <label class="text-warning">مقدار الدخل الشهري: </label>
            <strong><?PHP echo $yateem_others_data->monthly_income; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">مصدر الدخل: </label>
            <strong><?PHP echo $yateem_others_data->source_income; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">عدد  الأخوة المكفين: </label>
            <strong><?PHP echo $yateem_others_data->number_brothers; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الجهات الكافلة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_others_data->foster_agencies); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">مكان العمل: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_others_data->sponser_work_place); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الجهة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_others_data->place_name); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">تقاعد: </label>
            <strong><?PHP echo $yateem_others_data->retirement_period; ?></strong> </div>
        </div>
      </div>
    </div>
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr"> بيانات المرحلة الدراسية</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم المدرسة:</label>
            <strong><?PHP echo $yateem_edu_data->school_name; ?></strong> </div>
          <!--<div class="col-md-6 form-group">
      <label class="text-warning">المرحلة الدراسية: </label>
      <strong><?PHP echo $yateem_edu_data->grade; ?></strong> </div>-->
          <div class="col-md-6 form-group">
            <label class="text-warning">نوع المدرسة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_edu_data->school_type); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">النتيجة آخر: </label>
            <strong><?PHP echo $yateem_edu_data->last_result; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الصف: </label>
            <strong><?PHP echo $yateem_edu_data->classs; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">اخر نتيجة صف دراسي: </label>
            <strong><?PHP echo $yateem_edu_data->order; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الحالة الصحية: </label>
            <strong><?PHP echo $yateem_edu_data->health_status; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">سنة التسجيل في المدرسة: </label>
            <strong>
            <?php  if($yateem_edu_data->joining_date) { echo date('Y-m-d',strtotime($yateem_edu_data->joining_date)); }?>
            </strong> </div>
        </div>
      </div>
    </div>
    <!--<div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr"> بيانات المرحلة الدراسية</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
    <div class="col-md-6 form-group">
      <label class="text-warning">اسم المدرسة:</label>
      <strong><?PHP echo $yateem_edu_data->school_name; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">المرحلة الدراسية: </label>
      <strong><?PHP echo $yateem_edu_data->grade; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">النتيجة آخر: </label>
      <strong><?PHP echo $yateem_edu_data->last_result; ?></strong> </div>
    <div class="col-md-6 form-group">
      <label class="text-warning">الصف: </label>
      <strong><?PHP echo $yateem_edu_data->classs; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">اخر نتيجة صف دراسي: </label>
      <strong><?PHP echo $yateem_edu_data->order; ?></strong> </div>
      <div class="col-md-6 form-group">
      <label class="text-warning">الحالة الصحية: </label>
      <strong><?PHP echo $yateem_edu_data->health_status; ?></strong> </div>
      
       </div>
      </div>
    </div>-->
    <div class="col-md-12 form-group">
      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <h4 class="panel-title customhr"> الجهة المرشحة بيانات</h4>
        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
          <div class="col-md-6 form-group">
            <label class="text-warning">اسم الجهة:</label>
            <strong><?PHP echo $yateem_can_data->candidate_name; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">الدولة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_can_data->candidate_country_id); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">المحافظة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_can_data->candidate_region_id); ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">المدينة: </label>
            <strong><?PHP echo $this->haya_model->get_name_from_list($yateem_can_data->candidate_city_id); ?></strong> </div>
          <!--<div class="col-md-6 form-group">
      <label class="text-warning">الترتيب: </label>
      <strong><?PHP echo $yateem_can_data->candidate_city_id; ?></strong> </div>-->
          <div class="col-md-6 form-group">
            <label class="text-warning">العنوان: </label>
            <strong><?PHP echo $yateem_can_data->candidate_address; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">صندوق البريد: </label>
            <strong><?PHP echo $yateem_can_data->candidate_pobox; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">بلدة: </label>
            <strong><?PHP echo $yateem_can_data->candidate_town; ?></strong> </div>
          <div class="col-md-6 form-group">
            <label class="text-warning">رقم الهاتف: </label>
            <strong><?PHP echo $yateem_can_data->candidate_phone_number; ?></strong> </div>
          <!--<div class="col-md-6 form-group">
      <label class="text-warning">الرمز: </label>
      <strong><?PHP echo $yateem_can_data->candidate_pincode; ?></strong> </div>--> 
        </div>
      </div>
    </div>
    <?php if($yateem_data->step	>	1):?>
    <div class="col-md-12">
      <h4 class="yateem_h4 text-warning" align="center">تفاصيل الإستبيان</h4>
      <h4 class="yateem_h4" >بعد إجراء البحث الاجتماعي والاطلاع على المستندات المؤيدة اتضح مايلي</h4>
      <div class="col-md-4 form-group">
        <label class="text-warning">الحالة الصحية:</label>
        <strong><?php echo $this->haya_model->get_name_from_list($yateem_servay_list->health_condition); ?> </strong> </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">الحالة السكنية:</label>
        <strong><?php echo $this->haya_model->get_name_from_list($yateem_servay_list->housing_condition); ?> </strong> </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">عدد أفراد الأسرة:</label>
        <strong><?php echo $this->haya_model->get_name_from_list($yateem_servay_list->numberofpeople); ?> </strong> </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">ترتيب مقدم الطلب بالأسرة:</label>
        <strong><?php echo $this->haya_model->get_name_from_list($yateem_servay_list->positioninfamily); ?> </strong> </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">الحالة الاقتصادية:</label>
        <strong><?php echo $this->haya_model->get_name_from_list($yateem_servay_list->economic_condition); ?> </strong> </div>
      <br clear="all">
      <h4 class="yateem_h4" >نوع الحالة</h4>
      <div class="col-md-4 form-group">
        <label class="text-warning">
          <?php if($yateem_servay_list->casetype):?>
          <?php echo $yateem_servay_list->casetype; ?>
          <?php endif;?>
        </label>
      </div>
      <?php 
        $jason_data	=	$yateem_servay_list->aps_another_income;
        $income		=	json_decode($jason_data);
        ?>
      <?php if($yateem_servay_list->casetype	==	'للحالات غير الضمانية'):?>
      <div class="col-md-4 form-group">
        <label class="text-warning">مصادر دخل أخرى:</label>
        <ul>
          <li><?php echo (!empty($income[4])	?	$income[4]	:	'-- --');?></li>
          <li><?php echo (!empty($income[5])	?	$income[5]	:	'-- --');?></li>
          <li><?php echo (!empty($income[6])	?	$income[6]	:	'-- --');?></li>
          <li><?php echo (!empty($income[7])	?	$income[7]	:	'-- --');?></li>
        </ul>
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">اجمالي الدخل الشهري:</label>
        <strong><?php echo $yateem_servay_list->aps_month;?></strong> </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">صافي الدخل الشهري للاسرة:</label>
        <strong><?php echo $yateem_servay_list->aps_year;?></strong> </div>
      <?php endif;?>
      <!--------------End First Condition------------------> 
      <!--------------Start Second Condition------------------>
      
      <?php if($yateem_servay_list->casetype	==	'للحالات الضمانية'):?>
      <div class="col-md-4 form-group">
        <label class="text-warning">مصادر دخل أخرى:</label>
                <ul>
          <li><?php echo (!empty($income[0])	?	$income[0]	:	'-- --');?></li>
          <li><?php echo (!empty($income[1])	?	$income[1]	:	'-- --');?></li>
          <li><?php echo (!empty($income[2])	?	$income[2]	:	'-- --');?></li>
          <li><?php echo (!empty($income[3])	?	$income[3]	:	'-- --');?></li>
        </ul>
      </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">الحالة الضمانية باسم:</label>
        <strong><?php echo $yateem_servay_list->aps_name;?></strong> </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">رقم الحاسب:</label>
        <strong><?php echo $yateem_servay_list->aps_account;?></strong> </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">رقم الملف:</label>
        <strong><?php echo $yateem_servay_list->aps_filename;?></strong> </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">الفئة:</label>
        <strong><?php echo $yateem_servay_list->aps_category;?></strong> </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">قيمة المعاش:</label>
        <strong><?php echo number_format($yateem_servay_list->aps_salaryamount,3);?></strong> </div>
      <div class="col-md-4 form-group">
        <label class="text-warning">تاريخ الربط:</label>
        <strong><?php echo date('Y-m-d',strtotime($yateem_servay_list->aps_date));?></strong> </div>
      <?php endif;?>
      <div class="col-md-4 form-group">
        <label class="text-warning">الغرض من المساعدة:</label>
        <strong><?php echo $yateem_servay_list->whyyouwant;?></strong> </div>
      <div class="col-md-6 form-group">
        <label class="text-warning">ملخص الحالة:</label>
        <strong><?php echo $yateem_servay_list->summary;?></strong> </div>
      <div class="col-md-6 form-group">
        <label class="text-warning">رأي الباحث الاجتماعي و مبرراته:</label>
        <strong><?php echo $yateem_servay_list->review;?></strong> </div>
    </div>
    <?php endif;?>
    <div class="col-md-12 form-group">
      <h4 class="panel-title customhr">الملاحظات</h4>
      <div class="col-md-6 form-group"> <strong><?PHP echo $yateem_data->notes; ?></strong> </div>
    </div>
    <div class="col-md-5 form-group"> <strong><?PHP echo arabic_date($yateem_data->ownershiptype_amount); ?></strong> </div>
    <div class="form-group col-md-12">
      <div class="tab-pane list-group active" id="tabsdemo-1">
        <div class="list-group-item">
          <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
            <?php if(!empty($yateem_documents_history)):?>
            <h4 class="panel-title customhr">تاريخ جميع الوثائق المرفقة</h4>
            <?php $counter	=	1;?>
            <?php foreach($yateem_documents_history	as	$history):?>
            <div id="att_<?php echo $counter;?>" class="col-md-4 form-group"> ( <?php echo $counter;?> )
              <label  class="text-warning"><?php echo $history->created;?></label>
              <a class="fancybox-button" rel="gallery1" href="<?php echo base_url();?>/resources/yateem/<?php echo $history->orphan_id;?>/<?php echo $history->document_name;?>"> <img src="<?php echo base_url();?>assets/images/filestypes/file_type_jpg.png" style="width:30px; height:30px" alt="" title="" oldtitle=""> </a> </div>
            <?php $counter++; endforeach;?>
            <?php endif;?>
          </div>
        </div>
        <table style="width: 100% !important">
          <?php
       $kafeel_list_count = sizeof($kafeel_list);
	 	if($kafeel_list_count > 0)
		{
			//print_r($kafeel_list);	  
	   ?>
          <tr>
            <td class="customhr panel-title" style="border-bottom:1px solid #CCC;"> الكفيل</td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <?PHP foreach($kafeel_list as $kafeel) { ?>
                <tr>
                  <td class="Y_value"><?PHP echo is_set($kafeel->sponser_name); ?> (<?PHP echo is_set(arabic_date($kafeel->sponser_id_number)); ?>)</td>
                  <td class="Y_value"><?PHP echo is_set($this->haya_model->get_name_from_list($kafeel->sponser_nationalty)); ?></td>
                  <td class="Y_value"><?PHP echo is_set($kafeel->sponser_designation); ?></td>
                  <td class="Y_value"><?PHP echo is_set($kafeel->sponser_town_id); ?></td>
                  <td class="Y_value"><?PHP echo is_set($this->haya_model->get_name_from_list($kafeel->city_id)); ?></td>
                </tr>
                <?PHP } ?>
              </table></td>
          </tr>
          <?PHP 
	  }?>
        </table>
        <table style="width: 100% !important">
          <?php
        $cancel_orphan_count = sizeof($cancel_orphan_detail);
	 	if($cancel_orphan_count > 0)
		{
			$counter	=	1;
			$user_name	=	$this->haya_model->get_user_detail($orphan_detail->userid);  
	    ?>
          <tr>
            <td class="customhr panel-title" style="border-bottom:1px solid #CCC;">إلغاء التفاصيل</td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="Y_value"><strong>رقم</strong></td>
                  <td class="Y_value"><strong>تاريخ الإلغاء</strong></td>
                  <td class="Y_value"><strong>سبب الإلغاء</strong></td>
                  <td class="Y_value"></td>
                </tr>
                <?php foreach($cancel_orphan_detail as $orphan_detail) { ?>
                <tr>
                  <td class="Y_value">( <?PHP echo $counter; ?> )</td>
                  <td class="Y_value"><?PHP echo $orphan_detail->submitted_date; ?></td>
                  <td class="Y_value"><?PHP echo $orphan_detail->notes; ?></td>
                  <td class="Y_value"><?php echo $user_name['profile']->fullname; ?></td>
                </tr>
                <?php $counter++;} ?>
              </table></td>
          </tr>
          <?PHP 
	  }?>
        </table>
       <table style="width: 100% !important">
          <?php
        $continue_orphan_count = sizeof($continue_orphan_detail);
	 	if($continue_orphan_count > 0)
		{
			$counter	=	1;
			$user_name	=	$this->haya_model->get_user_detail($orphan_detail->userid);  
	    ?>
          <tr>
            <td class="customhr panel-title" style="border-bottom:1px solid #CCC;">سبب الإستكمال</td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="Y_value"><strong>رقم</strong></td>
                  <td class="Y_value"><strong>تاريخ</strong></td>
                  <td class="Y_value"><strong>سبب الإلغاء</strong></td>
                  <td class="Y_value"><strong></strong></td>
                </tr>
                <?php foreach($continue_orphan_detail as $orphan_detail) { ?>
                <tr>
                  <td class="Y_value">( <?PHP echo $counter; ?> )</td>
                  <td class="Y_value"><?PHP echo $orphan_detail->submitted_date; ?></td>
                  <td class="Y_value"><?PHP echo $orphan_detail->notes; ?></td>
                  <td class="Y_value"><?php echo $user_name['profile']->fullname; ?></td>
                </tr>
                <?php $counter++;} ?>
              </table></td>
          </tr>
          <?PHP 
	  }?>
        </table>
      </div>
    </div>
  </div>
</div>
<?php $segment	=	$this->uri->segment(1);?>
<?php $text		=	$this->lang->line($segment);?>
<?php $labels	=	$text['users']['form'];?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<style type="text/css">
.nav-tabs 
{
	margin-bottom: 14px !important;
}
#completion_meter_1 {
float:left;
margin-top:-13px;
}
#completion_meter_2 {
float:left;
margin-top:-13px;
}
#completion_meter_3 {
float:left;
margin-top:-13px;
}
#completion_meter_4 {
float:left;
margin-top:-13px;
}
#completion_meter_5 {
float:left;
margin-top:-13px;
}
#completion_meter_6 {
float:left;
margin-top:-13px;
}
.p-bar {
    border: 2px solid #e3e3e3 !important;
    color:  #060 !important;
    height: 20px !important;
    width: 45% !important;
    float: left !important;
    margin-top: -16px !important;
    /* margin-bottom: -3px; */
    margin-left: -20px !important;
}
.ui-widget-header {
    border: 8px solid #060 !important;
}
	</style>
<section class="wrapper scrollable" style="color:">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <?php //echo '<pre>'; print_r($yateem_data);?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
      <form action="<?php echo base_url();?>ajax/add_yateem_data" method="POST" id="user_form" name="user_form" enctype="multipart/form-data" >
        <input type="hidden"  name="orphans_id" id="orphans_id" value="<?php echo $yateem_data->orphan_id; ?>"/>
        <input type="hidden"  name="step" id="step" value="<?php echo (isset($yateem_data->orphan_id) ? '2' : '1');?>"/>
        <div class="col-md-12">
          <div class="panel panel-default panel-block">
            <div class="list-group">
              <div class="list-group-item" id="input-fields">
                <div class="col-md-6 panel panel-default panel-block">
                  <h4>نموذج بيانات الأيتام والرعاية المطلوبة</h4>
                  <div class="form-group col-md-12"> 
                    <!--<h4>Orphans Data Form the desired Sponsorship</h4>-->
                    <label class="text-warning"> اسم اليتيم </label>
                    <input type="text" class="form-control req" name="orphans_name" id="orphans_name" placeholder="اسم اليتيم" value="<?php echo $yateem_data->orphan_name; ?>"/>
                  </div>
                  <div class="form-group  col-md-6">
                    <label class="text-warning">الجنسية</label>
                    <?php //$yateem_data->orphan_nationalty ?>
                    <?php echo $this->haya_model->create_dropbox_list('nationality','nationality',$yateem_data->orphan_nationalty,'req'); ?> </div>
                  <div class="form-group col-md-4">
                    <label class="text-warning">تاريخ الميلاد</label>
                    <input type="text" class="form-control age_datepicker_yateem req" name="date_birth" id="date_birth" placeholder="تاريخ الميلاد" value="<?php echo $yateem_data->date_birth; ?>"  set-age="orphan_age" />
                  </div>
                  <div class="col-md-2 form-group">
                    <label class="text-warning">عمر:</label>
                    <input name="orphan_age" value="<?php echo $yateem_data->orphan_age; ?>" placeholder="عمر" id="orphan_age" type="text" class="form-control req"  />
                  </div>
                  <div id="yateem-info" style="display:none;">
                    <div class="form-group col-md-12" >
                      <label for="basic-input">الصورة</label>
                      <input style="border: 0px !important; color: #d09c0d;" type="file" name="orphan_picture_doc" title="تحميل" tabindex="7">
                    </div>
                    <div class="col-md-12 form-group">
                      <label class="text-warning">الملاحظات:</label>
                      <textarea style="height:141px !important;" class="form-control" id="reason_detail" name="reason_detail" tabindex="6"></textarea>
                      <br class="clear:both;">
                    </div>
                  </div>
                  <h4>باالكامل العنوان</h4>
                  <div class="form-group col-md-4">
                    <label class="text-warning">الدولة</label>
                    <?php
					if($yateem_data->country_id)
					{
						$selected_country	=	$yateem_data->country_id;
					}
					else
					{
						$selected_country	=	'200';
					}

					$muhafiza_type	=	(isset($yateem_data->muhafiza_id)	?	'issuecountry':	'muhafiza');
					$city_type		=	(isset($yateem_data->city_id)	?	'issuecountry':	'wilaya');
					
					?>
                    <?php echo $this->haya_model->create_dropbox_list('country_id','issuecountry',$selected_country,"0",'req'); ?> </div>
                  <div class="form-group col-md-4">
                    <label class="text-warning">المحافظة</label>
                    <?PHP echo $this->haya_model->create_dropbox_list('muhafiza_id',$muhafiza_type,$yateem_data->muhafiza_id,$selected_country,'req'); ?> </div>
                  <div class="form-group col-md-4">
                    <label class="text-warning">المدينة</label>
                    <?PHP echo $this->haya_model->create_dropbox_list('city_id',$city_type,$yateem_data->city_id,$yateem_data->muhafiza_id,'req'); ?> </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">ص.ب</label>
                    <input type="text" class="form-control" name="p_o_box" id="p_o_box" placeholder="ص.ب" value="<?php echo $yateem_data->po_adress; ?>" onKeyUp="only_numeric(this);"/>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">الرمز البريدي</label>
                    <input type="text" class="form-control" name="p_c_address" id="p_c_address" placeholder="الرمز البريدي" value="<?php echo $yateem_data->pc_adress; ?>"/>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">ترتيب اليتيم بين إخوتة</label>
                    <input type="text" class="form-control req" name="arrangment" id="arrangment" placeholder="اليتيم أخوته ترتيب بين" value="<?php echo $yateem_data->orphan_arrangement_brothers; ?>" onKeyUp="only_numeric(this);"/>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">عدد أفراد الأسرة</label>
                    <input type="text" class="form-control req" name="family_members" id="family_members" placeholder="عدد أفراد الأسرة" value="<?php echo $yateem_data->total_family_memebers; ?>" onKeyUp="only_numeric(this);"/>
                    <!-- <select class="form-control req" name="family_members" id="family_members">
                      <?php for($i = 1; $i <= 20; $i++):?>
                      <option value="<?php echo $i;?>" <?php if($yateem_data->total_family_memebers	==	$i):?> selected="selected" <?php endif;?>><?php echo $i;?></option>
                      <?php endfor;?>
                    </select>--> 
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning"> الهاتف/ منزل </label>
                    <input type="text" class="form-control NumberInput req" maxlength="8" name="phone_number" id="phone_number" placeholder="عدد أفراد الأسرة" value="<?php echo $yateem_data->phone_number; ?>"/>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="text-warning">هاتف ولي الامر: مع رمز الدولة</label>
                    <input type="text" class="form-control NumberInput req" maxlength="8" name="home_number" id="home_number" placeholder="هاتف ولي الامر: مع رمز الدولة" value="<?php echo $yateem_data->home_number; ?>"/>
                  </div>
                  <div class="form-group col-md-12">
                    <label class="text-warning">البريد الإلكتروني</label>
                    <input type="text" class="form-control"  id="email" name="email" placeholder="البريد الإلكتروني" value="<?php echo $yateem_data->email; ?>" />
                  </div>
                  <div class="col-md-12 form-group">
                    <h4>الذكور / الإناث
                      <button type="button" id="add_females" class="btn btn-sm btn-success" style="float: left; margin-bottom: 8px;">اضافة</button>
                    </h4>
                    <table class="table table-bordered table-striped dataTable" id="females">
                      <thead>
                        <tr role="row">
                          <th class="my_td right" style="width:50% !important;">الاسم</th>
                          <th class="my_td right" style="width:20% !important;">جنس</th>
                          <th class="my_td center" style="width:15% !important;">سنة الميلاد</th>
                          <th class="my_td center" style="width:10% !important;">العمر</th>
                          <th class="my_td center" style="width:5% !important;">الإجراءات</th>
                        </tr>
                      </thead>
                      <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php
							if(!empty($yateem_sisters)){
								$apr_req = 0;
								foreach($yateem_sisters as $i =>$y_sis){
									$relationBox = 'sister'.$apr_req;
									?>
                      				<input type="hidden" name="female_id[]" value="<?php echo $y_sis->ah_y_br_sis_id; ?>" id="hidden_<?php echo $y_sis->ah_y_br_sis_id; ?>">
                      <tr id="females_<?php echo $y_sis->ah_y_br_sis_id; ?>">
                        <td><input type="text" class="form-control" id="females" placeholder="" value="<?php echo $y_sis->br_sis_name; ?>" name="females[]"></td>
                        <td><select name="br_sis_type[]" class="form-control">
                            <option value="brother" <?PHP if($y_sis->br_sis_type=='brother') { echo(' selected '); } ?>>بنين</option>
                            <option value="sister" <?PHP if($y_sis->br_sis_type=='sister') { echo(' selected '); } ?>>بنات</option>
                          </select></td>
                        <td><input type="text"  value="<?PHP echo $y_sis->br_birthyear; ?>" name="birthyear[]" data-id="<?php echo $relationBox; ?>" onBlur="populateAge(this);" class="form-control NumberInput <?PHP is_required($apr_req); ?>" placeholder="سنة الميلاد" maxlength="4"></td>
                        <td><input readonly name="age[]" type="text" class="form-control <?PHP echo $relationBox; ?> NumberInput <?PHP is_required($apr_req); ?>" id="age" placeholder="السن" value="<?PHP echo $y_sis->br_age; ?>" maxlength="2"></td>
                          
                        <td style="vertical-align: middle;" class="center"><i class="icon-remove-sign" data-id="<?php echo $y_sis->ah_y_br_sis_id; ?>" data-index="females_<?php echo $y_sis->ah_y_br_sis_id; ?>" onClick="delete_females(this);" style="color:#CC0000; cursor:pointer;"></i></td>
                      </tr>
                      <?php
								}	
							}
							else{
								$apr_req = 0;
								$relationBox = 'sister'.$apr_req;
								?>
                      <tr class="broandsis" id="<?PHP echo $relationBox; ?>">
                        <td><input name="females[]" value="" placeholder="الاسم" id="females" type="text" class="form-control"></td>
                        <td><select name="br_sis_type[]" class="form-control">
                            <option value="brother">بنين</option>
                            <option value="sister">بنات</option>
                          </select></td>
                        <td><input type="text"  value="<?PHP echo $apr->birthyear; ?>" name="birthyear[]" data-id="<?php echo $relationBox; ?>" onBlur="populateAge(this);" class="form-control NumberInput <?PHP is_required($apr_req); ?>" placeholder="سنة الميلاد" maxlength="4"></td>
                        <td><input readonly name="age[]" type="text" class="form-control <?PHP echo $relationBox; ?> NumberInput <?PHP is_required($apr_req); ?>" id="age" placeholder="السن" value="<?PHP echo $apr->age; ?>" maxlength="2"></td>
                        <td></td>
                      </tr>
                      <?php
							}
						?>
                        </tbody>
                      
                    </table>
                  </div>
                  <div class="form-group col-md-12">
                    <label for="basic-input">الصورة</label>
                    <input style="border: 0px !important; color: #d09c0d;" placeholder="الصورة" class="req" type="file" name="orphan_picture" title='تحميل'>
                  </div>
                </div>
                <div class="col-md-6" style="background-color:#FFF !important; padding:0px !important;">
                  <div class="tab-pane list-group active" id="tabsdemo-1">
                    <div class="list-group-item">
                      <div style="text-align:right;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                        <h4>اترفق الوثائق والمستندات الازمة لطلب</h4>
                        <div class="panel-group" id="demo-accordion" style="margin-top:10px;">
                          <?php 
					$doccount = 0;
					
					foreach($this->inq->allRequiredDocument(205) as $ctid) { $doccount++; 
						$doc = $ah_applicant_documents[$ctid->documentid];
						$url = 'resources/applicants/'.$ah_applicant->applicantcode.'/'.$doc->document;	?>
                          <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                            <div class="panel-heading" style="padding:10px 3px;" id="head<?PHP echo $ctid->documentid;?>">
                              <h4 class="panel-title" style="font-size:15px;">
                                <?PHP if($doc->appli_doc_id!='') { ?>
                                <span class="icons" id="removeicons<?PHP echo $doc->appli_doc_id; ?>" style="float: left; font-size:12px;"> <?PHP echo getFileResult($url,$ctid->documenttype,$url); ?> <i onClick="removeDocument(this);" data-id="<?PHP echo $doc->appli_doc_id; ?>" data-remove="<?PHP echo $ctid->documentid; ?>" class="icon-remove-sign" style="color:#FF0000; cursor:pointer;"></i> </span>
                                <?PHP } ?>
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion" href="#demo-collapse<?PHP echo $ctid->documentid;?>"><?PHP echo arabic_date($doccount); ?>. <?PHP echo $ctid->documenttype;?></a> </h4>
                            </div>
                            <div id="demo-collapse<?PHP echo $ctid->documentid;?>" class="panel-collapse collapse">
                              <div class="panel-body" style="text-align:right;">
                                <input type="file" name="doclist<?PHP echo $ctid->documentid;?>" placeholder="<?PHP echo $ctid->documenttype;?>" class="form-control <?PHP if($ctid->isrequired==1 && $doc->document=='') { echo 'req'; } ;?>">
                              </div>
                            </div>
                          </div>
                          <?PHP } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-------> 
                  <br clear="all" />
                  <div class="panel-group" id="demo-accordion-yateem" style="margin-top:10px;">
                    <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                      <div class="panel-heading" id="head1">
                        <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion-yateem" href="#demo-collapse-1">بيانات عن ولي الأمر</a>
                          <div id="progressbar-1" class="p-bar"></div>
                          <div id="completion_meter_1">0%</div>
                        </h4>
                      </div>
                      <div id="demo-collapse-1" class="panel-collapse collapse">
                        <div class="panel-body" style="text-align:right;">
                          <div class="tab-pane list-group">
                            <div class="form-group col-md-6">
                              <input type="hidden"  class="form-control" name="parent_id[]" title="test" id="parent_id" value="<?php echo $yateem_father_data->ah_yateem_parent_id; ?>"/>
                              <label class="text-warning">الاسم بالكامل</label>
                              <input type="text" class="form-control percentage-1 req"   name="father_name" id="father_name" placeholder="الاسم بالكامل" value="<?php echo $yateem_father_data->father_name; ?>"/>
                            </div>
                            <div class="form-group col-md-6">
                              <label class="text-warning">صلة القرابة</label>
                              <?PHP echo $this->haya_model->create_dropbox_list('parent_relationship','user_relation',$yateem_father_data->parent_relationship,0,'req'); ?> </div>
                            <div class="form-group col-md-4">
                              <label class="text-warning">ص.ب</label>
                              <input type="text" placeholder="ص.ب" class="form-control percentage-1"  name="parent_po_address" id="parent_po_address" value="<?php echo $yateem_father_data->parent_po_address; ?>" onKeyUp="only_numeric(this);"/>
                            </div>
                            <div class="form-group col-md-4">
                              <label class="text-warning">الرمز البريدي</label>
                              <input type="text" placeholder="الرمز البريدي" class="form-control percentage-1"  name="parent_pc_address" id="parent_pc_address" value="<?php echo $yateem_father_data->parent_pc_address; ?>"/>
                            </div>
                            <div class="form-group col-md-4">
                              <label class="text-warning">شارع</label>
                              <input type="text" class="form-control percentage-1"  name="street" id="street" placeholder="شارع" value="<?php echo $yateem_father_data->street; ?>"/>
                            </div>
                            <div class="form-group col-md-6">
                              <label class="text-warning">بلدة</label>
                              <input type="text" class="form-control percentage-1" name="town" id="town" placeholder="بلدة" value="<?php echo $yateem_father_data->town; ?>"/>
                            </div>
                            <div class="form-group col-md-6">
                              <label class="text-warning">رقم الهاتف</label>
                              <input type="text" class="form-control percentage-1 req"  onKeyUp="only_numeric(this);" name="father_phone_number" id="father_phone_number" placeholder="رقم الهاتف" value="<?php echo ((isset($yateem_father_data->father_phone_number) AND $yateem_father_data->father_phone_number != '0')  ? $yateem_father_data->father_phone_number : NULL); ?>"/>
                            </div>
                            <?php
								$parent_muhafiza_type	=	(isset($yateem_father_data->parent_muhafiza_id)	?	'issuecountry':	'muhafiza');
								$parent_wilaya_type		=	(isset($yateem_father_data->parent_wilaya_id)	?	'issuecountry':	'wilaya'); 
							?>
                            <div class="form-group col-md-4">
                              <label class="text-warning">دولة</label>
                              <?PHP echo $this->haya_model->create_dropbox_list('parent_country_id','issuecountry',$yateem_father_data->parent_country_id,"0",'req'); ?> </div>
                            <div class="form-group col-md-4">
                              <label class="text-warning">المحافظة</label>
                              <?PHP echo $this->haya_model->create_dropbox_list('parent_muhafiza_id',$parent_muhafiza_type,$yateem_father_data->parent_muhafiza_id,$yateem_father_data->parent_country_id,'req'); ?> </div>
                            <div class="form-group col-md-4">
                              <label class="text-warning">ولاية</label>
                              <?php echo $this->haya_model->create_dropbox_list('parent_wilaya_id',$parent_wilaya_type,$yateem_father_data->parent_wilaya_id,$yateem_father_data->parent_muhafiza_id,'req');?> </div>
                            <div class="form-group col-md-6">
                              <label class="text-warning">منطقة</label>
                              <input type="text" class="form-control percentage-1 req" name="area" id="area" placeholder="منطقة" value="<?php echo $yateem_father_data->area; ?>"/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                      <div class="panel-heading" id="head2">
                        <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion-yateem" href="#demo-collapse-2">بيانات الأم</a>
                          <div id="progressbar-2" class="p-bar"></div>
                          <div id="completion_meter_2"> 0%</div>
                        </h4>
                      </div>
                      <div id="demo-collapse-2" class="panel-collapse collapse">
                        <div class="panel-body" style="text-align:right;">
                          <div class="tab-pane list-group">
                            <div class="form-group col-md-4">
                              <input type="hidden"  class="form-control" name="parent_id[]" id="parent_mother_id" value="<?php echo $yateem_mother_data->ah_yateem_parent_id; ?>"/>
                              <label class="text-warning">اسم الأم</label>
                              <input type="text" class="form-control  percentage-2 req" name="mother_name" id="mother_name" placeholder="اسم الأم" value="<?php echo $yateem_mother_data->mother_name; ?>"/>
                            </div>
                            <div class="form-group  col-md-4">
                              <label for="basic-input">الحالة المادية</label>
                              <?php echo $this->haya_model->create_dropbox_list('marital_status','marital_status',$yateem_mother_data->marital_status,0,''); ?> </div>
                            <div class="form-group col-md-4">
                              <label class="text-warning">رقم الهاتف</label>
                              <input type="text" class="form-control percentage-2 req" name="mother_phone_number" id="mother_phone_number" placeholder="رقم الهاتف" value="<?php echo (isset($yateem_mother_data->mother_phone_number) AND $yateem_mother_data->mother_phone_number != '0' ? $yateem_mother_data->mother_phone_number : NULL); ?>" onKeyUp="only_numeric(this);"/>
                            </div>
                            <div class="form-group col-md-4">
                              <label class="text-warning">رمز الدولة</label>
                              <input type="text" class="form-control percentage-2" name="parent_country_code" id="parent_country_code" placeholder="رمز الدولة" value="<?php echo (isset($yateem_mother_data->parent_country_code) AND $yateem_mother_data->parent_country_code != '0' ? $yateem_mother_data->parent_country_code : NULL); ?>" onKeyUp="only_numeric(this);"/>
                            </div>
                            <div class="form-group col-md-4">
                              <label class="text-warning">الرقم المدني</label>
                              <input type="text" class="form-control percentage-2" name="id_no" id="id_no" placeholder="الرقم المدني" value="<?php echo (isset($yateem_mother_data->id_no) AND $yateem_mother_data->id_no != '0' ? $yateem_mother_data->id_no : NULL);?>" onKeyUp="only_numeric(this);"/>
                            </div>
                            <div class="form-group col-md-4">
                              <label class="text-warning">عنوان</label>
                              <input type="text" class="form-control percentage-2" name="parent_address" id="address" placeholder="عنوان" value="<?php echo $yateem_mother_data->parent_address; ?>"/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                      <div class="panel-heading" id="head3">
                        <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion-yateem" href="#demo-collapse-3">البيانات البنكية</a>
                          <div id="progressbar-3" class="p-bar"></div>
                          <div id="completion_meter_3"> 0%</div>
                        </h4>
                      </div>
                      <div id="demo-collapse-3" class="panel-collapse collapse">
                        <div class="panel-body" style="text-align:right;">
                          <div class="tab-pane list-group">
                            <input  type="hidden" class="form-control" name="ah_yateem_bank_id" id="ah_yateem_bank_id" placeholder="" value="<?php echo $yateem_banks_data->ah_yateem_bank_id; ?>"/>
                            <div class="col-md-3 form-group">
                              <label class="text-warning">اسم البنك:</label>
                              <?PHP echo $this->haya_model->create_dropbox_list('bankid','bank',$yateem_banks_data->bankid,0,'req'); ?> </div>
                            <div class="col-md-3 form-group">
                              <label class="text-warning">الفرع:</label>
                              <?PHP echo $this->haya_model->create_dropbox_list('bankbranchid','bank_branch',$yateem_banks_data->bankbranchid,$yateem_banks_data->bankid,'req'); ?> </div>
                            <div class="form-group col-md-3">
                              <label class="text-warning">رقم الحساب</label>
                              <input type="text" class="form-control percentage-3 req" name="account_number" id="account_number" placeholder="رقم الحساب" value="<?php echo (isset($yateem_banks_data->account_number) AND $yateem_banks_data->account_number != '0' ? $yateem_banks_data->account_number : NULL);?>" onKeyUp="only_numeric(this);"/>
                            </div>
                            <div class="form-group col-md-3">
                              <label class="text-warning">اسم صاحب الحساب</label>
                              <input type="text" class="form-control percentage-3 req" name="holder_name" id="holder_name" placeholder="اسم صاحب الحساب" value="<?php echo $yateem_banks_data->account_title; ?>"/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                      <div class="panel-heading" id="head4">
                        <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion-yateem" href="#demo-collapse-4">بيانات أخرى</a>
                          <div id="progressbar-4" class="p-bar"></div>
                          <div id="completion_meter_4"> 0%</div>
                        </h4>
                      </div>
                      <div id="demo-collapse-4" class="panel-collapse collapse">
                        <div class="panel-body" style="text-align:right;">
                          <div class="tab-pane list-group">
                            <input  type="hidden" class="form-control" name="ah_yateem_other_id" id="ah_yateem_other_id" placeholder="" value="<?php echo $yateem_others_data->ah_yateem_other_id; ?>"/>
                            <div class="form-group col-md-6">
                              <label class="text-warning">تاريخ وفاة الأب</label>
                              <input type="text" class="form-control dp percentage-4 req" name="date_father_death" id="date_father_death" placeholder="تاريخ وفاة الأب" value="<?php echo (isset($yateem_others_data->date_father_death) ? $yateem_others_data->date_father_death : NULL); ?>"/>
                            </div>
                            <div class="form-group  col-md-6">
                              <label class="text-warning">سبب الوفاة</label>
                              <input type="text" class="form-control percentage-4" name="reason_father_death" id="reason_father_death" placeholder="سبب الوفاة" value="<?php echo $yateem_others_data->reason_father_death; ?>"/>
                            </div>
                            <!--<div class="form-group col-md-3">
                    <label class="text-warning">عدد أفراد الأسرة</label>
                    <select name="family_members" class="form-control percentage-4">
						<?php for($i	=	1;	$i	<=	20;	$i++):?>
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php endfor;?>
                    </select>
                  </div>-->
                           <!-- <div class="form-group col-md-6" style="display:none !important;">
                              <label class="text-warning">الاخوة</label>
                              <input type="text" class="form-control percentage-4" name="brothers" id="brothers" placeholder="" value="<?php echo (isset($yateem_others_data->brothers) AND $yateem_others_data->brothers != '0' ? $yateem_others_data->brothers : NULL);?>"/>
                            </div>
                            <div class="form-group col-md-6" style="display:none !important;">
                              <label class="text-warning">الأخوات</label>
                              <input type="text" class="form-control percentage-4" name="sisters" id="sisters" placeholder="" value="<?php echo (isset($yateem_others_data->sisters) AND $yateem_others_data->sisters != '0' ? $yateem_others_data->sisters : NULL);?>"/>
                            </div>-->
                            <div class="form-group col-md-12">
                              <label class="text-warning">الظروف المعيششة للأسرة</label>
                              <input type="text" class="form-control percentage-4 req" name="living_condition_family" id="living_condition_family" placeholder="الظروف المعيششة للأسرة" value="<?php echo $yateem_others_data->living_condition_family; ?>"/>
                            </div>
                            <div class="form-group col-md-12">
                              <label class="text-warning">الظروف السكينة</label>
                              <input type="text" class="form-control percentage-4 req" name="housing_condition" id="housing_condition" placeholder="الظروف السكينة" value="<?php echo $yateem_others_data->housing_condition; ?>"/>
                            </div>
                            <div class="form-group col-md-6">
                              <label class="text-warning">الظروف الاقتصادية</label>
                              <input type="text" class="form-control percentage-4" name="economic_condition" id="economic_condition" placeholder="الظروف الاقتصادية" value="<?php echo $yateem_others_data->economic_condition; ?>"/>
                            </div>
                            <div class="form-group col-md-6">
                              <label class="text-warning"> مقدار الدخل الشهري </label>
                              <input type="text" class="form-control percentage-4" name="monthly_income" id="monthly_income" placeholder="مقدار الدخل الشهري" value="<?php echo (isset($yateem_others_data->monthly_income) AND $yateem_others_data->monthly_income != '0' ? $yateem_others_data->monthly_income : NULL);?>" onKeyUp="only_numeric(this);"/>
                            </div>
                            <div class="form-group col-md-12">
                              <label class="text-warning">مصدر الدخل</label>
                              <input type="text" class="form-control percentage-4" name="source_income" id="source_income" placeholder="مصدر الدخل" value="<?php echo $yateem_others_data->source_income; ?>"/>
                            </div>
                            <div class="form-group col-md-6">
                              <label class="text-warning">عدد  الأخوة المكفين</label>
                              <input type="text" class="form-control percentage-4" name="number_brothers" id="number_brothers" placeholder="عدد  الأخوة المكفين" value="<?php echo (isset($yateem_others_data->number_brothers) AND $yateem_others_data->number_brothers != '0' ? $yateem_others_data->number_brothers : NULL);?>" onKeyUp="only_numeric(this);"/>
                            </div>
                            <div class="form-group col-md-6">
                              <label class="text-warning">الجهات الكافلة</label>
                              <input type="text" class="form-control percentage-4" name="foster_agencies" id="foster_agencies" placeholder="الجهات الكافلة" value="<?php echo $yateem_others_data->foster_agencies; ?>"/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                      <div class="panel-heading" id="head5">
                        <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion-yateem" href="#demo-collapse-5">بيانات المرحلة الدراسية</a>
                          <div id="progressbar-5" class="p-bar"></div>
                          <div id="completion_meter_5"> 0%</div>
                        </h4>
                      </div>
                      <div id="demo-collapse-5" class="panel-collapse collapse">
                        <div class="panel-body" style="text-align:right;">
                          <div class="tab-pane list-group">
                            <div class="form-group col-md-12">
                              <input type="hidden" name="yateem_edu_id" id="yateem_edu_id" value="<?php echo $yateem_edu_data->yateem_edu_id; ?>">
                              <label class="text-warning">اسم المدرسة</label>
                              <input type="text" class="form-control percentage-5" name="school_name" id="school_name" placeholder="" value="<?php echo $yateem_edu_data->school_name; ?>"/>
                            </div>
                            <div class="form-group  col-md-6">
                              <label class="text-warning">المرحلة الدراسية</label>
                              <input type="text" class="form-control percentage-5" name="grade" id="grade" placeholder="" value="<?php echo (isset($yateem_edu_data->grade) AND $yateem_edu_data->grade != '0' ? $yateem_edu_data->grade : NULL);?>"/>
                            </div>
                            <div class="form-group  col-md-6">
                              <label class="text-warning">النتيجة آخر</label>
                              <input type="text" class="form-control percentage-5" name="last_result" id="last_result" placeholder="النتيجة آخر" value="<?php echo (isset($yateem_edu_data->last_result) AND $yateem_edu_data->last_result != '0' ? $yateem_edu_data->last_result : NULL);?>"/>
                            </div>
                            <div class="form-group  col-md-6">
                              <label class="text-warning">الصف</label>
                              <input type="text" class="form-control percentage-5" name="class" id="class" placeholder="الصف" value="<?php echo $yateem_edu_data->classs; ?>"/>
                            </div>
                            <div class="form-group  col-md-6">
                              <label class="text-warning">الترتيب</label>
                              <input type="text" class="form-control percentage-5" name="order" id="order" placeholder="الترتيب" value="<?php echo (isset($yateem_edu_data->order) AND $yateem_edu_data->order != '0' ? $yateem_edu_data->order : NULL);?>"/>
                            </div>
                            <div class="form-group  col-md-6">
                              <label class="text-warning">الحالة الصحية</label>
                              <input type="text" class="form-control percentage-5" name="health_status" id="health_Status" placeholder="الحالة الصحية" value="<?php echo $yateem_edu_data->health_status; ?>"/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default" style="border-bottom:1px solid #ddd;">
                      <div class="panel-heading" id="head6">
                        <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#demo-accordion-yateem" href="#demo-collapse-6">بيانات الجهة المرشحة</a>
                          <div id="progressbar-6" class="p-bar"></div>
                          <div id="completion_meter_6"> 0%</div>
                        </h4>
                      </div>
                      <div id="demo-collapse-6" class="panel-collapse collapse">
                        <div class="panel-body" style="text-align:right;">
                          <div class="tab-pane list-group">
                            <input type="hidden" name="yateem_cand_id" id="yateem_cand_id" value="<?php echo $yateem_can_data->yateem_cand_id; ?>">
                            <div class="form-group col-md-12">
                              <label class="text-warning">اسم الجهة</label>
                              <input type="text" class="form-control percentage-6" name="candidate_name" id="candidate_name" placeholder="اسم الجهة" value="<?php echo $yateem_can_data->candidate_name; ?>"/>
                            </div>
                            <?php
								
								$candidate_region_type		=	(isset($yateem_can_data->candidate_region_id)	?	'issuecountry':	'muhafiza');
								$candidate_city_type		=	(isset($yateem_can_data->candidate_city_id)	?	'issuecountry':	'wilaya'); 
							?>
                            <div class="form-group col-md-4">
                              <label class="text-warning">الدولة</label>
                              <?PHP echo $this->haya_model->create_dropbox_list('candidate_country_id','issuecountry',$yateem_can_data->candidate_country_id,0,''); ?> </div>
                            <div class="form-group col-md-4">
                              <label class="text-warning">المحافظة</label>
                              <?PHP echo $this->haya_model->create_dropbox_list('candidate_region_id',$candidate_region_type,$yateem_can_data->candidate_region_id,$yateem_can_data->candidate_country_id,''); ?> </div>
                            <div class="form-group col-md-4">
                              <label class="text-warning">المدينة</label>
                              <?PHP echo $this->haya_model->create_dropbox_list('candidate_city_id',$candidate_city_type,$yateem_can_data->candidate_city_id,$yateem_can_data->candidate_region_id); ?> </div>
                            <div class="form-group col-md-4">
                              <label class="text-warning">العنوان</label>
                              <input type="text" class="form-control percentage-6" name="candidate_address" id="candidate_address" placeholder="العنوان" value="<?php echo $yateem_can_data->candidate_address; ?>"/>
                            </div>
                            <div class="form-group col-md-3">
                              <label class="text-warning">بلدة</label>
                              <input type="text" class="form-control percentage-6" name="candidate_town" id="candidate_town" placeholder="بلدة" value="<?php echo $yateem_can_data->candidate_town; ?>"/>
                            </div>
                            <div class="form-group col-md-3">
                              <label class="text-warning">رقم الهاتف</label>
                              <input type="text" class="form-control percentage-6" name="candidate_phone_number" id="candidate_phone_number" placeholder="رقم الهاتف" value="<?php echo (isset($yateem_can_data->candidate_phone_number) AND $yateem_can_data->candidate_phone_number != '0' ? $yateem_can_data->candidate_phone_number : NULL);?>" onKeyUp="only_numeric(this);"/>
                            </div>
                            <div class="form-group col-md-2">
                              <label class="text-warning">صندوق البريد</label>
                              <input type="text" class="form-control percentage-6" name="candidate_pobox" id="candidate_pobox" placeholder="صندوق البريد" value="<?php echo $yateem_can_data->candidate_pobox; ?>" onKeyUp="only_numeric(this);"/>
                            </div>
                            
                            <!--<div class="form-group col-md-4">
                              <label class="text-warning">الرمز </label>
                              <input type="text" class="form-control percentage-6" name="candidate_pincode" id="candidate_pincode" placeholder="الرمز" value="<?php echo (isset($yateem_can_data->candidate_pincode) AND $yateem_can_data->candidate_pincode != '0' ? $yateem_can_data->candidate_pincode : NULL); ?>" onKeyUp="only_numeric(this);"/>
                            </div>-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-------> 
                  <br clear="all" />
                  <div class="col-md-12"  style="background-color:#FFF !important; padding:0px !important;">
                    <input type="button" id="user_data" class="btn btn-success btn-lg" name="user_data"  value="حفظ" />
                  </div>
                </div>
                <br clear="all">
              </div>
            </div>
          </div>
        </div>
        </div>
      </form>
    </div>
  </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script type="text/javascript">
$(document).ready(function (){
	//alert('ready');
	$('.dp').datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-80:+0",
		dateFormat:'yy-mm-dd',
    });
	
//-----------------------------------------------------------------------------------
	$( ".age_datepicker_yateem" ).datepicker({
			 showAnim:'slide',
			 changeMonth: true,
			  changeYear: true,
			  dateFormat:'yy-mm-dd',
			  yearRange: '-90:+0',
			  onSelect: function(selected,evnt) {
					 	birthday = selected
  						dob = new Date(selected);
						var today = new Date();
						var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
						var newage = age;
						var age_box = $(this).attr('data-durar');
						//var age_box	=	'applicant_age';
						var age_box = $(this).attr('set-age');
												
						if(newage >= 18)
						{
							$(this).val(' ');
							show_notification_error_end('The Age is greater than ١٨.');
							
							//show_notification_error_end('غير مطابق لشروط العمر‎ شروط العمر ما بين ١٨ - ٥٥');
							$('#'+age_box).val(' ');
							
							$('#orphan_picture_doc').addClass('req');
							$('#reason_detail').addClass('req');
							$('#yateem-info').toggle();
						}
						/*else if(newage > 55)
						{
							$(this).val(' ');
							show_notification_error_end("غير مطابق لشروط العمر‎ شروط العمر ما بين ١٨ - ٥٥");
							$('#'+age_box).val(' ');
						}*/
						else
						{
							$('#orphan_picture_doc').removeClass('req');
							$('#reason_detail').removeClass('req');
							
							$(this).val(selected);
							$('#'+age_box).val(parseInt(newage));
						}
						
						$(this).val(selected);
						$('#'+age_box).val(parseInt(newage));
					}
				});
//-----------------------------------------------------------------------------------
	
  for (i = 1; i <= 6; i++) {
  // get the number of [tab-1] input elements, and add the 'required' class:
  var required = $('.percentage-'+i).addClass('tab-'+i),
  // filter the [tab-1] input elements, keeping only those
  
  // with entered text:
	completed = required.filter(function() 
	{
     	return this.value.trim().length > 0;
	  
    	// remove the 'tab-1' class for completed fields:
    }).removeClass('tab-'+i);

  // if there are any completed inputs (to prevent any attempts to divide zero):
  	//if (completed.length) 
  	//{
	 	var percentComplete = Math.round((completed.length / required.length) * 100);
     	$('#completion_meter_'+i).html(percentComplete + "%");
		$( "#progressbar-"+i ).progressbar({
      		value: percentComplete
    	});	
	}
$('.percentage-1').on('keyup', function() {
	
  // get the number of [tab-1] input elements, and add the 'required' class:
  var required = $('.percentage-1').addClass('tab-1'),
  // filter the [tab-1] input elements, keeping only those
  
  // with entered text:
	completed = required.filter(function() 
	{
     	return this.value.trim().length > 0;
	  
    	// remove the 'tab-1' class for completed fields:
    }).removeClass('tab-1');

  // if there are any completed inputs (to prevent any attempts to divide zero):
  	//if (completed.length) 
  	//{
	 	var percentComplete = Math.round((completed.length / required.length) * 100);
     	$('#completion_meter_1').html(percentComplete + "%");
		$( "#progressbar-1" ).progressbar({
      		value: percentComplete
    	});
		//$("#status").animate( { width: percentComplete }, 100);
  	//}
	}).addClass('tab-1');
	
$('.percentage-1').on('keyup', function() {
	
  // get the number of [tab-1] input elements, and add the 'required' class:
  var required = $('.percentage-1').addClass('tab-1'),
  // filter the [tab-1] input elements, keeping only those
  
  // with entered text:
	completed = required.filter(function() 
	{
     	return this.value.trim().length > 0;
	  
    	// remove the 'tab-1' class for completed fields:
    }).removeClass('tab-1');

  // if there are any completed inputs (to prevent any attempts to divide zero):
  	//if (completed.length) 
  	//{
	 	var percentComplete = Math.round((completed.length / required.length) * 100);
     	$('#completion_meter_1').html(percentComplete + "%");
		$( "#progressbar-1" ).progressbar({
      		value: percentComplete
    	});
		//$("#status").animate( { width: percentComplete }, 100);
  	//}
	}).addClass('tab-1');
	
$('.percentage-2').on('keyup', function() {
	
  // get the number of [tab-1] input elements, and add the 'required' class:
  var required = $('.percentage-2').addClass('tab-2'),
  // filter the [tab-1] input elements, keeping only those
  
  // with entered text:
	completed = required.filter(function() 
	{
     	return this.value.trim().length > 0;
	  
    	// remove the 'tab-1' class for completed fields:
    }).removeClass('tab-2');
 
  // if there are any completed inputs (to prevent any attempts to divide zero):
  	//if (completed.length) 
  	//{
	 	var percentComplete = Math.round((completed.length / required.length) * 100);
     	$('#completion_meter_2').html(percentComplete + "%");
		
		$( "#progressbar-2" ).progressbar({
      		value: percentComplete
    	});
  	//}
	}).addClass('tab-2');
$('.percentage-3').on('keyup', function() {
	
  // get the number of [tab-1] input elements, and add the 'required' class:
  var required = $('.percentage-3').addClass('tab-3'),
  // filter the [tab-1] input elements, keeping only those
  
  // with entered text:
	completed = required.filter(function() 
	{
     	return this.value.trim().length > 0;
    	// remove the 'tab-1' class for completed fields:
    }).removeClass('tab-3');
 
  // if there are any completed inputs (to prevent any attempts to divide zero):
  	//if (completed.length) 
  	//{
	 	var percentComplete = Math.round((completed.length / required.length) * 100);
     	$('#completion_meter_3').html(percentComplete + "%");
		$( "#progressbar-3" ).progressbar({
      		value: percentComplete
    	});
  	//}
	}).addClass('tab-3');
$('.percentage-4').on('keyup', function() {
	
  // get the number of [tab-1] input elements, and add the 'required' class:
  var required = $('.percentage-4').addClass('tab-4'),
  // filter the [tab-1] input elements, keeping only those
  
  // with entered text:
	completed = required.filter(function() 
	{
     	return this.value.trim().length > 0;
	  
    	// remove the 'tab-1' class for completed fields:
    }).removeClass('tab-4');
 
  // if there are any completed inputs (to prevent any attempts to divide zero):
  	//if (completed.length) 
  	//{
	 	var percentComplete = Math.round((completed.length / required.length) * 100);
     	$('#completion_meter_4').html(percentComplete + "%");
		$( "#progressbar-4" ).progressbar({
      		value: percentComplete
    	});
  	//}
	}).addClass('tab-4');
$('.percentage-5').on('keyup', function() {
	
  // get the number of [tab-1] input elements, and add the 'required' class:
  var required = $('.percentage-5').addClass('tab-5'),
  // filter the [tab-1] input elements, keeping only those
  
  // with entered text:
	completed = required.filter(function() 
	{
     	return this.value.trim().length > 0;
	  
    	// remove the 'tab-1' class for completed fields:
    }).removeClass('tab-5');
 
  // if there are any completed inputs (to prevent any attempts to divide zero):
  	//if (completed.length) 
  	//{
	 	var percentComplete = Math.round((completed.length / required.length) * 100);
     	$('#completion_meter_5').html(percentComplete + "%");
		$( "#progressbar-5" ).progressbar({
      		value: percentComplete
    	});
  	//}
	}).addClass('tab-5');
$('.percentage-6').on('keyup', function() {
	
  // get the number of [tab-1] input elements, and add the 'required' class:
  var required = $('.percentage-6').addClass('tab-6'),
  // filter the [tab-1] input elements, keeping only those
  
  // with entered text:
	completed = required.filter(function() 
	{
     	return this.value.trim().length > 0;
	  
    	// remove the 'tab-1' class for completed fields:
    }).removeClass('tab-6');
 
  // if there are any completed inputs (to prevent any attempts to divide zero):
  	//if (completed.length) 
  	//{
	 	var percentComplete = Math.round((completed.length / required.length) * 100);
     	$('#completion_meter_6').html(percentComplete + "%");
		$( "#progressbar-6" ).progressbar({
      		value: percentComplete
    	});
  	//}
	}).addClass('tab-6');

	
});

</script> 
<!-- /.modal-dialog -->

</div>
</body>
</html>
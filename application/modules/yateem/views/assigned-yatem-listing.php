<?php

?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <div class="nav nav-tabs panel panel-default panel-block">
          <div class="tab-pane list-group active">           
            <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th><?php //echo $sponser_id; ?>اسم اليتيم</th>
                  <th>الجنسية</th>
                  <th>تاريخ الميلاد</th>
                 <th>الدولة</th>
                  <th>اسم الكفيل</th>
                  <th>الإجرائات</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'yateem/all_assignedyateem_list/','columns_array'=>'{ "data": "اسم اليتيم" },
                { "data": "الجنسية" },
                { "data": "تاريخ الميلاد" },
                { "data": "الدولة" },
				{ "data": "اسم الكفيل" },
				{ "data": "الإجرائات" }')); ?>
</div>
</body>
</html>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
      	<?php //$this->load->view('common/globalfilter', array('type'=>'help','b'=>$branchid,'c'=>$charity_type));?>
        <?php //$this->load->view('common/globalfilter', array('type'=>'wilaya','b'=>$branchid,'c'=>'yateem'));?>
        
        <div class="nav nav-tabs panel panel-default panel-block">
        
          <div class="tab-pane list-group active">
          <?php //$this->load->view("common/globalfilter",array('type'=>'users')); ?>
            <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
              <thead>
                <tr role="row">
                  <th>رقم الكفيل</th>
                  <th>رقم الهوية</th>
                  <th>اسم</th>
                  <th>رقم الهاتف</th>
                  <th>الجنسية</th>
                  <th>الإجرائات</th>
                </tr>
              </thead>
              <tbody role="alert" aria-live="polite" aria-relevant="all">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php $this->load->view('common/footer',array('ajax_url'=>base_url().'yateem/ajax_cancel_sponsor_list/'.$branchid.'/'.$step.'/'.$orphan_type.'/'.$parent_id.'/'.$child_id.'/'.$orphan_status,'columns_array'=>'
				{ "data": "رقم الكفيل" },
				{ "data": "رقم الهوية" },
                { "data": "اسم" },
				{ "data": "رقم الهاتف" },
                { "data": "الجنسية" },
				{ "data": "الإجرائات"}'));
?>
</div>
</body>
</html>
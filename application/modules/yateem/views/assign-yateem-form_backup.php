<?php $segment	=	$this->uri->segment(1);?>
<?php $text		=	$this->lang->line($segment);?>
<?php $labels	=	$text['users']['form'];?>

<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<style type="text/css">
.nav-tabs {
	margin-bottom: 14px !important;
}
</style>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <div class="col-md-12">
        <form action="<?php echo base_url();?>yateem/add_assign_data" method="POST" id="user_form" name="user_form" enctype="multipart/form-data" >
         <input type="hidden" name="sponser_id_number" id="sponser_id_number" value="<?php echo $yateem_data->p_c_address; ?>"/>
          <input type="hidden" name="sponser_id" id="sponser_id">
          <div class="col-md-6 haya_white_background">
            <h4 class="haya_h4"> حركة سداد المستحقات الشهرية :</h4>
            <div class="form-group col-md-12">
            	<label class="text-warning">اسم الكفيل / رقم الكفيل : </label>
              <input type="text" class="form-control req" name="kafeel_name" id="kafeel_name" placeholder="اسم الكفيل" value="<?php echo $yateem_data->p_c_address; ?>"/>
            
            </div>
            
            <div class="form-group col-md-4">
              <label class="text-warning">رقم الحرقة : </label>
              <input type="text" class="form-control NumberInput" name="process_number" id="process_number" placeholder="رقم الحرقة" value="<?php echo $yateem_data->p_c_address; ?>"/>
            </div>
           
           
            <div class="form-group col-md-3">
              <label class="text-warning">السنة المالية : </label>
              <?PHP number_drop_box('financial_year',$yateem_data->financial_year,$title='السنة المالية',date('Y'),date('Y')+10); ?>
            </div>
            <!------------------------------------->
            <br clear="all">
          
            <h4 class="haya_h4">تفاصيل بدء الكفالة وبيانات اليتيم :</h4>
            <div class="form-group col-md-4">
              <label class="text-warning"> رقم اليتيم : </label>
              <input type="text" class="form-control req" name="yateem_id" id="yateem_id" placeholder="رقم اليتيم" value="<?php echo $yateem_data->orphan_id; ?>"/>
            </div>
            <div class="form-group col-md-4">
              <label class="text-warning">تاريخ بداية الكفالة : </label>
              <input type="text" class="form-control dp req" name="starting_date" id="starting_date" placeholder=" تاريخ بداية الكفالة" value="<?php echo $yateem_data->date_birth; ?>"/>
            </div>
            <div class="form-group  col-md-4">
              <label class="text-warning">اسم اليتيم : </label>
				<input type="hidden" id="orphan_id" name="orphan_id">	
              <input type="text" class="form-control req" name="orphan_name" id="orphan_name" placeholder="اسم اليتيم" value="<?php echo $yateem_data->orphan_name; ?>"/>
            </div>
            <div class="form-group  col-md-12">
              <label class="text-warning">ملاحظات : </label>
              <textarea class="form-control" name="details" id="details" placeholder="ملاحظات"></textarea>
            </div>
            <h4 class="haya_h4">تفاصيل الكفالة :</h4>
            <div class="form-group col-md-12">
              <label class="text-warning">مدة الكفالة : </label>
              <input id="accounttype" type="radio" value="yearly" checked="" name="warranty" tabindex="54" onClick="check_warranty(this)">
              <label class="text-warning">سنوية : </label>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input id="accounttype" type="radio" value="open" name="warranty" tabindex="55" onClick="check_warranty(this)">
              <label class="text-warning">مفتوحة : </label>
            </div>
            <div class="form-group col-md-4">
              <label class="text-warning">طريقة الدفع : </label>
              <?PHP echo $this->haya_model->create_dropbox_list('payment_type','payment_type',$item->user_relation,0,''); ?> </div>
            <div class="form-group col-md-4">
              <label class="text-warning">نوع الكفالة : </label>
              <?PHP echo $this->haya_model->create_dropbox_list('sponser_type','sponser_type',$item->user_relation,0,''); ?> </div>
            <div class="form-group col-md-4">
              <label class="text-warning">القيمة الشهرية : </label>
              <input type="text" class="form-control req" name="monthly_payment" id="monthly_payment" placeholder="القيمة الشهرية" value="<?php echo $yateem_data->p_c_address; ?>"/>
            </div>
            
            <div class="form-group col-md-12">
            	<input type="button" id="user_data" class="btn btn-success btn-lg" name="user_data"  value="حفظ" />
            </div>
          </div>
          
          <!----------------------->
          <div class="col-md-6 haya_border_left haya_white_background">
            <h4 class="haya_h4"> المدفوعات الشهرية لليتيم : </h4>
            <div class="form-group col-md-12">
              <label class="text-warning">سنة كاملة : </label>
              <input id="all_months"  type="checkbox" value="1" checked="" name="all_months" tabindex="54">
            </div>
            <?PHP for($i=1; $i<=12; $i++) { ?>
            <div class="form-group col-md-3">
              <input id="asign_month<?PHP echo $i; ?>" class="months"  type="checkbox" value="<?PHP echo $i; ?>" name="months[]">
              <label class="text-warning"><?PHP echo show_date($i,8); ?></label>
            </div>
            <?PHP } ?>
            <h4 class="haya_h4">المدفوع من الكفيل : </h4>
            <div class="form-group col-md-6">
              <label class="text-warning">بداية الدفع: </label>
              <input type="text" class="form-control req dp" name="start_payment_date" id="start_payment_date" placeholder="نهاية الدفع" value="<?php echo $yateem_data->p_c_address; ?>"/>
            </div>
            <div class="form-group col-md-6">
              <label class="text-warning">نهاية الدفع: </label>
              <input type="text" class="form-control req dp" name="end_payment_date" id="end_payment_date" placeholder="بداية الدفع" value="<?php echo $yateem_data->p_c_address; ?>"/>
            </div>
            <div class="form-group col-md-6">
              <label class="text-warning">قيمة الكفلة (ر.ع): </label>
              <input type="text" class="form-control req" name="monthly_payment" id="monthly_payment" placeholder="قيمة الكفلة" value="<?php echo $yateem_data->p_c_address; ?>"/>
            </div>
           <div id="yearly_check"> 
            <div class="form-group col-md-6">
              <label class="text-warning">سنة كاملة : </label>
              <br>
              <input id="payment_months_all"  type="checkbox" value="1" checked="" name="payment_months_all" tabindex="54">
            </div>
            <br clear="all">
            <?PHP for($j=1; $j<=12; $j++) { ?>
            <div class="form-group col-md-3">
              <input id="asign_month<?PHP echo $j; ?>"  class="payment_method"  type="checkbox" value="<?PHP echo $j; ?>" name="payment_months[]">
              <label class="text-warning"><?PHP echo show_date($j,8); ?></label>
            </div>
            <?PHP } ?>
          </div>
          </div>
        </form>
      </div>
     	
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<script type="text/javascript">

function changeVal(){
	op_id = $("#orphan_id").val();
	$("#yateem_id").val(op_id);
			
}

function check_warranty(obj){
	war_val = $(obj).val();
	if(war_val == 'yearly'){
		$("#yearly_check").show();
	}
	else{
		$("#yearly_check").hide();
	}
}
function changeVal2(){
	op_id = $("#sponser_id").val();
	$("#sponser_id_number").val(op_id);
			
}
$(document).ready(function (){
	//alert('ready');
	$('.dp').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
		dateformat:'yy-mm-dd',
        onClose: function(dateText, inst) { }
    });
	
	$("#orphan_name").autocomplete({
        source: config.BASE_URL+'yateem/getYateemNames',
        minLength: 0,
		open:function(event, ui) 		{	check_my_session();	},
        select: function (event, ui) 	{		
				//alert(ui.item.value);
				//alert(ui.item.id);
				$("#orphan_id").val(ui.item.id);
				
				//console.log(ui.item+'item');
				//alert($(this).attr('id'));
			//beforeData(ui.item.value,$(this).attr('id'));		
		}
    });
	
	$("#yateem_id").autocomplete({
        source: config.BASE_URL+'yateem/getYateemNames',
        minLength: 0,
		open:function(event, ui) 		{	check_my_session();	},
        select: function (event, ui) 	{		
				//alert(ui.item.value);
				//alert(ui.item.id);
				$("#yateem_id").val(ui.item.id);
				$("#orphan_id").val(ui.item.id);
				$("#orphan_name").val(ui.item.value);
				console.log(ui.item+'item');
				//alert($(this).attr('id'));
				//changeVal();
				setTimeout('changeVal()',500);
			//beforeData(ui.item.value,$(this).attr('id'));		
		}
    });
	
		$("#kafeel_name").autocomplete({
        	source: config.BASE_URL+'yateem/getKafeelNames',
       	 	minLength: 3,
			open:function(event, ui) 		{	check_my_session();	},
        	select: function (event, ui) 	{
									
				$("#yateem_id").val(ui.item.id);
				$("#sponser_id").val(ui.item.id);
				$("#sponser_id_number").val(ui.item.spid);
		}
    });
	
	$("#sponser_id_number").autocomplete({
        source: config.BASE_URL+'yateem/getKafeelNames',
        minLength: 0,
		open:function(event, ui) 		{	check_my_session();	},
        select: function (event, ui) 	{		
				//alert(ui.item.value);
				//sponser_id
				$("#sponser_id").val(ui.item.id);
				$("#sponser_id_number").val(ui.item.id);
				$("#kafeel_name").val(ui.item.value);
				setTimeout('changeVal2()',500);
				//alert($(this).attr('id'));
			//beforeData(ui.item.value,$(this).attr('id'));		
		}
    });
	
	
	
	
	$("#all_months").click(function (){
			month_status = $(this).is(':checked');
			if(month_status){
				$('.months').prop('checked', true);
			}
			else{
				$('.months').prop('checked',false);
			}
		
	});
	
	$("#payment_months_all").click(function(){
			payment_check_status  = $(this).is(':checked');	
			//alert(payment_check_status);
			///payment_method
			
			if(payment_check_status){
				$('.payment_method').prop('checked', true);
			}
			else{
				$('.payment_method').prop('checked',false);
			}
			//if()
	})
	
});
</script> 
<!-- /.modal-dialog -->

</div>
</body>
</html>
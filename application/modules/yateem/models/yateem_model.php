<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Yateem_model extends CI_Model
{
	/*
	*  Properties
	*/
	private $_table_users;
	private $_table_users_documents;
	private $_table_listmanagement;
	private $_table_modules;
	private $_table_userprofile;
	private $_table_users_bankaccount;
	private $_table_users_communications;
	private $_table_users_family;
	private $_table_users_salary;
	private $_table_users_salary_detail;
	private $_table_branchs;
	private $_login_userid;
	
//-------------------------------------------------------------------

	/*
	 *  Constructor
	 */
	function __construct()
	{
		parent::__construct();

		// Get Table Names from Config 
		$this->_table_users 					= 	$this->config->item('table_users');
		$this->_table_users_documents 			= 	$this->config->item('table_users_documents');
		$this->_table_listmanagement 			= 	$this->config->item('table_listmanagement');
		$this->_table_modules 					= 	$this->config->item('table_modules');
		$this->_table_userprofile 				= 	$this->config->item('table_userprofile');
		$this->_table_users_bankaccount 		= 	$this->config->item('table_users_bankaccount');
		$this->_table_users_communications 		= 	$this->config->item('table_users_communications');
		$this->_table_users_family 				= 	$this->config->item('table_users_family');
		$this->_table_users_salary 				= 	$this->config->item('table_users_salary');
		$this->_table_users_salary_detail 		= 	$this->config->item('table_users_salary_detail');
		$this->_table_branchs 					= 	$this->config->item('table_branchs');
		$this->_login_userid					=	$this->session->userdata('userid');
		
		// If SESSION is empty SET by defult Arabic
		if($this->session->userdata('lang')	==	NULL)
		{
			$this->session->set_userdata('lang', 'arabic');
			$this->session->set_userdata('lang_code', 'ar');
		}

        // Change language
       if($this->session->userdata('lang') == 'english' || $this->uri->segment(3) == 'en')
       {
		   // Load ENGLISH Language File
		   $this->lang->load('project', 'english');
		   
		   // Set SESSION for English File name
		   $this->session->set_userdata('lang', 'english');
       }
       else
       {
		   // Load ENGLISH Language File
		   $this->lang->load('project', 'arabic');
       }
	}

//-------------------------------------------------------------------
	/**
	 * Valadting & Checking user session
	 * @access	public
	 * @return	Array with complete info of the user
	 */	
	public function get_user_detail($userid=0)
	{
		$this->db->select('`ah_users`.`userid`,`ah_users`.`branchid`,`ah_users`.`userroleid`,`ah_users`.`userstatus`,`mh_modules`.`module_controller`,`ah_userprofile`.`fullname`,`ah_userprofile`.`email`,`ah_userprofile`.`profilepic`,`ah_branchs`.`branchname`,`ah_branchs`.`branchaddress`,`ah_listmanagement`.`list_name`,`ah_listmanagement_1`.`list_name`');
		$this->db->from('ah_users');
		$this->db->join('mh_modules','mh_modules.moduleid = ah_users.landingpage','left');
		$this->db->join('ah_userprofile', 'ah_userprofile.userid = ah_users.userid','left');
		$this->db->join('ah_branchs', 'ah_branchs.branchid = ah_users.branchid','left');
		$this->db->join('ah_listmanagement', 'ah_listmanagement.list_id = ah_branchs.provinceid','left');
		$this->db->join('alhaya.ah_listmanagement AS ah_listmanagement_1', 'ah_listmanagement_1.list_id = ah_branchs.wiliayaid','left');
		$this->db->where('`ah_listmanagement`.`delete_record`','0');
		
		if($userid!=0 && $userid!='')
		{
			$this->db->where('`ah_users`.`userid`',$userid);
		}
		else
		{
			$this->db->where('`ah_users`.`userid`',$this->_login_userid);
		}
		
		$query = $this->db->get();
		
		if($query->num_rows() <= 0 )
		{
			redirect(base_url().'admin/login');
		}
		
		$userinfo = array('_userid'=>$this->_login_userid,'profile'=>$query->row());
		return $userinfo;
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function get_yateeem_list($id,$type,$step)
	{
		$this->db->select('ai.*');
        $this->db->from('ah_yateem_info as ai');
		$this->db->where('ai.delete_record','0');
		$this->db->where('ai.step',$step);
		$this->db->where('DATE(ai.expire_date) <','CURDATE()');
		
		if($id !="")
		{
			if($type =='branch')
			{
				$this->db->where('ai.user_branch_id',$id);
			}
			else
			{
				$this->db->join('assigned_orphan ao','ao.orphan_id = ai.orphan_id','Inner');
				$this->db->where('ao.sponser_id',$id);
				$this->db->where('ao.is_old','0');
			}
		}
		
		$this->db->order_by("orphan_id", "DESC");
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function get_yateeem_list_new($status,$step,$city_id	=	NULL)
	{
		if($step	!=	'1')
		{
			$approved_query_String	=	"AND DATE(expire_date) > CURDATE()";
		}
		else
		{
			$approved_query_String	=	"";
		}
			
		if($status	==	'NOT')
		{
			$query	=	$this->db->query("SELECT * FROM `ah_yateem_info` WHERE delete_record = '0' AND step=".$step." AND orphan_id  NOT IN (SELECT orphan_id FROM `assigned_orphan`) AND cancel='0' AND DATE(expire_date) > CURDATE() ORDER BY orphan_id DESC;");
		}
		elseif($status	==	'YES')
		{
			$query	=	$this->db->query("SELECT * FROM `ah_yateem_info` WHERE delete_record = '0' AND step='".$step."' AND orphan_id IN (SELECT orphan_id FROM `assigned_orphan`) AND cancel='0' AND DATE(expire_date) > CURDATE() ORDER BY orphan_id DESC;");
		}
		elseif($status	==	'GREATER')
		{
			$query	=	$this->db->query("SELECT *,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE step='".$step."' HAVING  age	>='18' AND cancel='0' AND DATE(expire_date) > CURDATE() ORDER BY orphan_id DESC;");
		}
		elseif($city_id)
		{
			$query	=	$this->db->query("SELECT * FROM `ah_yateem_info` WHERE delete_record = '0' AND step='".$step."' AND issuecountry='".$city_id."' AND cancel='0' AND DATE(expire_date) > CURDATE() ORDER BY orphan_id DESC;");
		}
		else
		{
			$query	=	$this->db->query("SELECT *,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE delete_record = '0' AND step='".$step."' AND cancel='0' ".$approved_query_String." HAVING  age	<'18' ORDER BY orphan_id DESC;");
		}
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* Get Yateem By searching Key Words
	*/
	public function get_yateem_search_result($data)
	{
		$this->db->select('*,,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age');
		$this->db->from('ah_yateem_info');
		
		if($data['file_number'])
		{
			$this->db->where("file_number",$data['file_number']);
		}
		if($data['orphan_name'])
		{
			$this->db->like("orphan_name",$data['orphan_name']);
		}
		if($data['date_birth'])
		{
			$this->db->where("date_birth",$data['date_birth']);
		}
		if($data['country_id'])
		{
			$this->db->where("country_id",$data['country_id']);
		}
		if($data['muhafiza_id'])
		{
			$this->db->where("muhafiza_id",$data['muhafiza_id']);
		}
		if($data['city_id'])
		{
			$this->db->where("city_id",$data['city_id']);
		}
		if($data['phone_number'])
		{
			$this->db->where("phone_number",$data['phone_number']);
		}
		if($data['home_number'])
		{
			$this->db->where("home_number",$data['home_number']);
		}

		
		if(array_filter($data)) 
		{
			$this->db->where("delete_record",'0');
			$this->db->order_by('orphan_id','DESC');
		}
		else
		{
			$this->db->limit(50);
			$this->db->where("delete_record",'0');
			$this->db->order_by('orphan_id','DESC');
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//----------------------------------------------------------------------

	/*
	* Get Kafeel By searching Key Words
	*/
	public function get_kafeel_search_result($data)
	{
		if($data['sponser_name'])
		{
			$sponsor_name	=	"AND sponser_name LIKE '%".$data['sponser_name']."%'";
		}

		if($data['sponser_id_number'])
		{
			$sponser_id_number		=	"AND sponser_id_number='".$data['sponser_id_number']."'";
		}
		if($data['sponser_nationalty'])
		{
			$sponser_nationalty		=	"AND sponser_nationalty='".$data['sponser_nationalty']."'";
		}
		if($data['sponser_phone_number'])
		{
			$sponser_phone_number	=	"AND sponser_phone_number='".$data['sponser_phone_number']."'";
		}

		$query	=	$this->db->query("SELECT *,(SELECT COUNT(sponser_id) AS total FROM `assigned_orphan` WHERE assigned_orphan.`sponser_id`=ah_sponser_info.`sponser_id`) AS sponserCount FROM `ah_sponser_info` WHERE delete_record='0' ".$sponsor_name." ".$sponser_id_number." ".$sponser_nationalty." ".$sponser_phone_number." ORDER BY sponser_id DESC;");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function get_assignedyateeem_list_status($status)
	{
		$this->db->select('ai.*');
        $this->db->from('ah_yateem_info as ai');
		$this->db->where('ai.delete_record','0');
		
		if($status == 'not')
		$this->db->join('assigned_orphan ao','ao.orphan_id != ai.orphan_id','Inner');
		else
		$this->db->join('assigned_orphan ao','ao.orphan_id = ai.orphan_id','Inner');
		$this->db->order_by("orphan_id", "DESC");
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
	
	function get_assignedyateeem_list()
	{
		$this->db->select('ai.*,si.sponser_name');
        $this->db->from('ah_yateem_info as ai');
		$this->db->where('ai.delete_record','0');
		//$this->db->join('ah_listmanagement','ah_listmanagement.list_id = ai.orphan_nationalty','Inner');
		$this->db->join('assigned_orphan ao','ao.orphan_id = ai.orphan_id','Inner');
		$this->db->join('ah_sponser_info si','si.sponser_id = ao.sponser_id','Inner');
		$this->db->order_by("orphan_id", "DESC");
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function getYateemDocument($yateem_id)
	{
		$this->db->select('*');
		$this->db->from('ah_yateem_documents');
		$this->db->where('orphan_id',$yateem_id);
		$query = $this->db->get();

		foreach($query->result() as $doc)
		{
			$arr[$doc->doc_type_id] = $doc;
		}
		
		return $arr;
	}
	
//--------------------------------------------------------------------

	/*
	*
	* Get Payments By Months
	* @param $month integer
	*/
	public function get_payment_by_month($month)
	{
		$this->db->select('payment_id,payment_month,amount,payment_date,userid');
		$this->db->from('orphsn_monthly_payments');
		$this->db->where('payment_month',$month);
		$query	=	$this->db->get();
		
		if($query->num_rows()	>	0)
		{
			return $guery->result();
		}
		
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function get_assignedsponser_list_status($status)
	{
		$select_sub = 'count(ao.sponser_id) as sponserCount';	
		
		$this->db->select('ai.*,'.$select_sub.'');
        $this->db->from('ah_sponser_info as ai');
		$this->db->where('ai.delete_record','0');
		if($status == 'not')
		$this->db->join('assigned_orphan ao','ao.sponser_id != ai.sponser_id','Inner');
		else
		$this->db->join('assigned_orphan ao','ao.sponser_id = ai.sponser_id','Inner');
		
		$this->db->group_by("ai.sponser_id");
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/
	function getKafeelDocument($document_id)
	{
		$this->db->select('*');
		$this->db->from('ah_kafeel_documents');
		$this->db->where('sponser_id',$document_id);
		$query = $this->db->get();
		
		foreach($query->result() as $doc)
		{
			$arr[$doc->doc_type_id] = $doc;
		}
		
		return $arr;
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/
	function getYateemData($id){
		$this->db->select('*');
        $this->db->from('ah_yateem_info as ai');
		$this->db->join('ah_listmanagement','ah_listmanagement.list_id = ai.orphan_nationalty','Inner');
		$this->db->join('ah_yateem_banksinfo` AS bi ','bi.yateem_id = ai.orphan_id','Inner');
		$this->db->join('ah_yateem_candidate AS ac ','ac.yateem_id = ai.orphan_id','Inner');
		$this->db->join('ah_yateem_education AS ae ','ae.yateem_id = ai.orphan_id','Inner');
		$this->db->join('ah_yateem_other_data AS at ','at.yateem_id = ai.orphan_id','Inner');
		 $this->db->order_by("orphan_id", "DESC");
		$this->db->where('ai.delete_record','0');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function getKafeellist($orphanid)
	{		
		$this->db->select('assigned_orphan.starting_date, assigned_orphan.financial_year, assigned_orphan.monthly_payment,
		assigned_orphan.warranty, assigned_orphan.start_payment_date, assigned_orphan.end_payment_date
		, sponsertype.list_name AS sponser_type
		, paymenttype.list_name AS payment_type
		, ah_sponser_info.sponser_name
		, ah_sponser_info.sponser_id_number
		, ah_sponser_info.sponser_town_id
		, ah_sponser_info.city_id
		, ah_sponser_info.sponser_nationalty
		, ah_sponser_info.sponser_designation');
        $this->db->from('assigned_orphan');
		$this->db->join('ah_listmanagement as paymenttype','paymenttype.list_id = assigned_orphan.payment_type');
		$this->db->join('ah_listmanagement` AS sponsertype ','sponsertype.list_id = assigned_orphan.sponser_type');
		$this->db->join('ah_sponser_info','ah_sponser_info.sponser_id = assigned_orphan.sponser_id');		
		$this->db->where('assigned_orphan.orphan_id',$orphanid);
		$this->db->where('assigned_orphan.is_old','1');
		$query = $this->db->get();
		
		return $query->result();
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function old_getKafeellist($orphanid)
	{		
		$this->db->select('assigned_orphan.starting_date, assigned_orphan.financial_year, assigned_orphan.monthly_payment,
		assigned_orphan.warranty, assigned_orphan.start_payment_date, assigned_orphan.end_payment_date,
		assigned_orphan.end_date,
		,sponsertype.list_name AS sponser_type
		,paymenttype.list_name AS payment_type
		,ah_sponser_info.sponser_name
		,ah_sponser_info.sponser_id_number
		,ah_sponser_info.sponser_town_id
		,ah_sponser_info.city_id
		,ah_sponser_info.sponser_nationalty
		,ah_sponser_info.sponser_designation');
        $this->db->from('assigned_orphan');
		$this->db->join('ah_listmanagement as paymenttype','paymenttype.list_id = assigned_orphan.payment_type');
		$this->db->join('ah_listmanagement` AS sponsertype ','sponsertype.list_id = assigned_orphan.sponser_type');
		$this->db->join('ah_sponser_info','ah_sponser_info.sponser_id = assigned_orphan.sponser_id');		
		$this->db->where('assigned_orphan.orphan_id',$orphanid);
		$this->db->where('assigned_orphan.is_old','1');
		$this->db->order_by('assigned_orphan.assign_id','DESC');
		$query = $this->db->get();
		
		return $query->result();
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function getKafeelInfo($id)
	{
		$this->db->select('ai.*,count(ao.sponser_id) as total_yateem');
		$this->db->from('ah_sponser_info ai');
		$this->db->join('assigned_orphan ao','ao.sponser_id = ai.sponser_id','left');
		$this->db->where('ai.sponser_id',$id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $return = $query->row();
		}
		else
		{
			//echo "else";
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/
	function getBroSisNames($id,$type)
	{
		$this->db->select('*');
		$this->db->from('ah_yateem_brothers_sisters as abs');
		$this->db->where('yateem_id',$id);
		$this->db->where('delete_record','0');
		$this->db->where('br_sis_type',$type);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/
	function getBroSisNamesNew($id,$type)
	{
		$this->db->select('*');
		$this->db->from('ah_yateem_brothers_sisters as abs');
		$this->db->where('yateem_id',$id);
		$this->db->where('delete_record','0');
		//$this->db->where('br_sis_type',$type);
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/
	function delete_yateem($id){
			
		$json_data	=	json_encode(array('record'=>'delete','orpahn_id'	=>	$id));
		
		$data	=	array('delete_record'=>'1');
		
		$this->db->where('orphan_id',$id);

		$this->db->update('ah_yateem_info',$json_data,$this->session->userdata('userid'),$data);
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function delete_kafeel($id){
			
		$json_data	=	json_encode(array('record'=>'delete','sponser_id'	=>	$id));
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('sponser_id',$id);
		$this->db->update('ah_sponser_info',$json_data,$this->session->userdata('userid'),$data);
		
	
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/
	function getParentsData($id,$type)
	{
		$this->db->select('*');
		$this->db->from('ah_yateem_parents as ayp');
		$this->db->where('ah_yateem_id',$id);
		$this->db->where('parent_relationship',$type);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
//------------------------------------------------------
	/*
	* Get Yateem Father Detail
	* @param $ah_yateem_id int
	* return OBJECT
	*/
	function get_father_data($ah_yateem_id)
	{
		$this->db->select('yateem_father_id,ah_yateem_id,userid,parent_relationship,father_name,parent_country_id,parent_muhafiza_id,parent_wilaya_id,parent_po_address,parent_pc_address,street,town,`area`,father_phone_number,parent_marital_status,id_no,parent_address,created');
		$this->db->from('ah_yateem_father');
		$this->db->where('ah_yateem_id',$ah_yateem_id);

		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
//------------------------------------------------------
	/*
	* Get Yateem Father Detail
	* @param $ah_yateem_id int
	* return OBJECT
	*/
	function get_mother_data($ah_yateem_id)
	{
		$this->db->select('yateem_mother_id,ah_yateem_id,userid,mother_name,mother_phone_number,marital_status,id_no,parent_country_code,parent_address');
		$this->db->from('ah_yateem_mother');
		$this->db->where('ah_yateem_id',$ah_yateem_id);

		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/
	function getBanksData($id)
	{
		$this->db->select('*');
		$this->db->from('ah_yateem_banksinfo  AS ab');
		$this->db->where('yateem_id',$id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function getOthersData($id)
	{
		$this->db->select('*');
		$this->db->from('ah_yateem_other_data');
		$this->db->where('yateem_id',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/
	function getYateemDataById($id){
	$this->db->select('*');
	$this->db->from('ah_yateem_info');
	$this->db->where('orphan_id',$id);
	$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
//----------------------------------------------------------
	/*
	* 
	*
	*/
	function get_yateem_others_data($table_name,$id)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where('yateem_id',$id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function getYateemFullDetail($orphanid)
	{
		$this->db->select('ah_yateem_info.orphan_name, ah_yateem_info.orphan_id, ah_yateem_info.orphan_picture, ah_yateem_info.date_birth,
		ah_yateem_info.home_number, ah_yateem_info.phone_number, country.list_name AS CountryName, city.list_name AS CityName');
        $this->db->from('ah_yateem_info');
		$this->db->join('ah_listmanagement AS country','country.list_id = ah_yateem_info.country_id');
		$this->db->join('ah_listmanagement` AS city ','city.list_id = ah_yateem_info.city_id');	
		$this->db->where('ah_yateem_info.orphan_id',$orphanid);		
		$query = $this->db->get();		
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/
	function get_sponser_list($status,$orphan_id	=	NULL)
	{
		if($status	==	'NOT')
		{
			$query	=	$this->db->query("SELECT *,(SELECT COUNT(sponser_id) AS total FROM `assigned_orphan` WHERE assigned_orphan.`sponser_id`=ah_sponser_info.`sponser_id`) AS sponserCount FROM `ah_sponser_info` WHERE delete_record = '0' AND sponser_id  NOT IN (SELECT sponser_id FROM `assigned_orphan`) ORDER BY sponser_id DESC;");
		}
		elseif($status	==	'YES')
		{
			$query	=	$this->db->query("SELECT *,(SELECT COUNT(sponser_id) AS total FROM `assigned_orphan` WHERE assigned_orphan.`sponser_id`=ah_sponser_info.`sponser_id`) AS sponserCount FROM `ah_sponser_info` WHERE delete_record = '0' AND sponser_id IN (SELECT sponser_id FROM `assigned_orphan`) ORDER BY sponser_id DESC;");
		}
		elseif($status	==	NULL AND isset($orphan_id))
		{
			$query	=	$this->db->query("SELECT
    `ah_sponser_info`.*,(SELECT COUNT(sponser_id) FROM ah_sponser_info WHERE sponser_id=`assigned_orphan`.`sponser_id`) AS sponserCount
FROM
    `assigned_orphan`
    INNER JOIN `ah_sponser_info` 
        ON (`assigned_orphan`.`sponser_id` = `ah_sponser_info`.`sponser_id`) WHERE assigned_orphan.`orphan_id` = '".$orphan_id."';");
		}
		else
		{
			$query	=	$this->db->query("SELECT *,(SELECT COUNT(sponser_id) AS total FROM `assigned_orphan` WHERE assigned_orphan.`sponser_id`=ah_sponser_info.`sponser_id`) AS sponserCount FROM `ah_sponser_info` WHERE delete_record = '0' ORDER BY sponser_id DESC;");
		}
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

//-------------------------------------------------------------------

	/*
	 *  Get ALL Types
	 *  return ARRAY
	 */
	function get_listmanagment_types()
	{
		$query = $this->db->query("SHOW COLUMNS FROM ".$this->_table_listmanagement." LIKE 'list_type'");
		$result = $query->row();
		
		if(!empty($result))
		{
			 return $option_array = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $result->Type));
		}
	}
//-------------------------------------------------------------------

	/*
	 *  Get ALL Types
	 *  return ARRAY
	 */	
	function get_sub_category($provinceid,$willayaid	=	NULL)
	{
		$this->db->select('list_id,list_name,list_type,list_parent_id');
		$this->db->from($this->_table_listmanagement);
		$this->db->where('delete_record','0');	
		$this->db->where('list_parent_id',$provinceid);
		$this->db->order_by("list_order", "ASC"); 
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$dropdown  = '<label>Select a Willaya:&nbsp;';
			$dropdown .= '<select name="wiliayaid" id="wiliayaid" placeholder="البرنامج المطلوب‎">';
			$dropdown .= '<option value="">البرنامج المطلوب‎</option>';
		
			foreach($query->result() as $row)
			{
				$dropdown .= '<option value="'.$row->list_id.'" ';
					
				if($willayaid !="")
				{
					if($willayaid	==	$row->list_id)
					{
						$dropdown .= 'selected="selected"';
					}
				}
						
						$dropdown .= '>'.$row->list_name.'</option>';
			}
				$dropdown .= '</select>';
				$dropdown .= '</label>';
				return $dropdown;
					
		}
		else
		{
			return 'No record is available;';
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Province Name
	 * Return OBJECT
	 */
		
	function get_willaya_province_name($id)
	{
		$this->db->select('list_name');
		$this->db->where('list_id',$id);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_listmanagement);
		
		if($query->num_rows() > 0)
		{
			return $query->row()->list_name;
		}

	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function get_dropbox_list_value($listtype='', $parent=0)
	{
		$this->db->select('list_id,list_name,list_type,list_other');
		$this->db->from($this->_table_listmanagement);	
		if($parent!='' && $parent!=0)
		{	$this->db->where('list_parent_id',$parent);	 }
		
		if($listtype!='' && $listtype!='0' && $listtype!='subcategory')
		{	$this->db->where('list_type',$listtype);	}
		
			$this->db->where('delete_record','0');
			$this->db->order_by("list_order", "ASC"); 			
			$query = $this->db->get();
			return $query->result();
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function get_name_from_list($id)
	{	
		$this->db->select('list_name');
		$this->db->where('list_id',$id);
		$this->db->where('delete_record','0');
		$this->db->from($this->_table_listmanagement);	 			
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row()->list_name;
		}
	}	
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function get_type_name($listype)
	{
		$lang_code = $this->session->userdata('lang_code');
		$type_all_list = $this->config->item('list_types');
		$list_type = $type_all_list[$listype];		
		return $list_type[$lang_code];
	}
//-------------------------------------------------------------------
	/*
	* Checking drop box for all list type 
	* Return complete dropbox with input field
	*/	
	function create_dropbox_list($name,$listtype,$selectedvalue='0',$parent='0',$isrequired='',$othervalue='',$index='')
	{		
		$heading = $this->get_type_name($listtype);
		if($index!='')
		{	$dropdown = '<select onChange="check_other_value(this);" name="'.$name.'[]" id="'.$name.$index.'" class="form-control '.$isrequired.'" placeholder="'.$heading.'">';	}
		else
		{	$dropdown = '<select onChange="check_other_value(this);" name="'.$name.'" id="'.$name.'" class="form-control '.$isrequired.'" placeholder="'.$heading.'">'; 		}
		$dropdown .= '<option value="">'.$heading.'</option>';		
		foreach($this->get_dropbox_list_value($listtype,$parent) as $row)
		{
				$dropdown .= '<option dataid="'.$row->list_other.'" value="'.$row->list_id.'" ';
				if($selectedvalue==$row->list_id)
				{	$dropdown .= 'selected="selected"';	}
				$dropdown .= '>'.$row->list_name.'</option>';
		}
		$dropdown .= '</select>';
		if($index!='')
		{	$dropdown .= '<input name="'.$name.'_text[]" id="'.$name.'_text'.$index.'" value="'.$othervalue.'" placeholder="'.$heading.'" type="text" class="otherinput form-control">';	}
		else
		{	$dropdown .= '<input name="'.$name.'_text" id="'.$name.'_text" value="'.$othervalue.'" placeholder="'.$heading.'" type="text" class="otherinput form-control">';	}
		return $dropdown;
	}

//-------------------------------------------------------------------
		/*
		* Checking view permission 
		* Return permission status 0,1
		*/
		function check_view_permission($moduleid)
		{
			$this->db->select('permissionjson');
			$this->db->from('ah_users');
			$this->db->where('userid',$this->_login_userid);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				$permissionResult = $query->row();
				$permission = json_decode($permissionResult->permissionjson,TRUE);
				$perm = $permission[$moduleid];
				return $perm['v'];				
			}
			else
			{
			   return 0;
			}
		}
//-------------------------------------------------------------------
	/*
	* Creating Parent Menu 
	* Return Parent menu html 
	*/
	function parentMenu()
	{
		$this->db->select('moduleid,module_name,module_icon,module_controller,module_parent');
		$this->db->from('mh_modules');
		$this->db->where('module_parent',0); 
		$this->db->where('module_status','A');
		$this->db->order_by("module_order", "ASC");
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{		
			$html = '<nav class="quick-launch-bar">';
			$html .= '<ul>';
			
			foreach($query->result() as $parentMenu)
			{				
				if($this->check_view_permission($parentMenu->moduleid)==1)
                {
                    $html .= '<li><a href="'.base_url().$parentMenu->module_controller.'"><i class="'.$parentMenu->module_icon.'"></i><span>'.$parentMenu->module_name.'</span></a></li>';
                }				
			}
			
			$query->free_result();
			$html .= '</ul>';
			$html .= '</nav>';
			echo $html;
		}
	}
//-------------------------------------------------------------------
	/*
	* Creating Child Menu 
	* Return child menu html
	* Parameter parent module id 
	*/
	function childMenu($parentid)
	{		
		$this->db->select('moduleid,module_parent,module_name,module_icon,module_controller');
		$this->db->from('mh_modules');
		$this->db->where('module_parent',$parentid); 
		$this->db->where('module_status','A');
		$this->db->where('show_on_menu','1');
		$this->db->order_by("module_order", "ASC");
		$childQuery = $this->db->get();		
		if($childQuery->num_rows() > 0)
		{	$childMenux = '';
			foreach($childQuery->result() as $childMenu)
			{
                if($this->check_view_permission($childMenu->moduleid)==1)
                {
					if($childMenu->moduleid==34)
					{
						$childMenux .= '<a class="add re_design_a" href="#globalDiag" onClick="alatadad(this);" data-url="'.base_url().'lease_programe/add_data_for_barnamij/" id="0" data-icon="'.$childMenu->module_icon.'" data-heading="'.$childMenu->module_name.'" data-color="#4096EE"><i class="'.$childMenu->module_icon.' resizeicon"><br><span class="resizeheading">'.$childMenu->module_name.'</span></i></a>';
					}
					
					if($childMenu->moduleid==2)
					{
						$childMenux .= '<a class="add re_design_a" href="#globalDiag" onClick="alatadad(this);" data-url="'.base_url().'listing_managment/addlistingview/" id="0" data-icon="'.$childMenu->module_icon.'" data-heading="'.$childMenu->module_name.'" data-color="#4096EE"><i class="'.$childMenu->module_icon.' resizeicon"><br><span class="resizeheading">'.$childMenu->module_name.'</span></i></a>';
					}
					else
					{
						$childMenux .= '<a href="'.base_url().$childMenu->module_controller.'" class="add re_design_a"> <i class="'.$childMenu->module_icon.' resizeicon"><br><span class="resizeheading">'.$childMenu->module_name.'</span></i></a>';
					}
					
				}
			}			
		}
		return $childMenux;
	}
//-------------------------------------------------------------------
	/**
	 * Get All Branches
	 *
	 * @access	public
	 * @return	object
	 */
	function get_all_branches()
	{
		$this->db->select('`ah_branchs`.`branchid`,`ah_branchs`.`branchname`,`ah_branchs`.`branchaddress`,`ah_listmanagement`.`list_name`,`ahl1`.`list_name` AS wilaya');
		$this->db->from('ah_branchs');
		$this->db->join('ah_listmanagement','`ah_listmanagement`.`list_id` = `ah_branchs`.`provinceid`','left');
		$this->db->join('ah_listmanagement AS ahl1','`ahl1`.`list_id` = `ah_branchs`.`wiliayaid`','left');
		$this->db->where('`ah_branchs`.`branchstatus`','1');
		$this->db->where('`ah_listmanagement`.`delete_record`','0');
		$this->db->order_by("`ah_branchs`.`branchname`", "ASC");
		return $q->result();		
	}
//-------------------------------------------------------------------
	/**
	 * Get Module Inforamtion
	 *
	 * @access	public
	 * @return	object
	 */
	function get_module($firstURL='',$secondURL='')
	{		
		$this->db->select('moduleid,module_parent,module_name,module_text,module_controller,module_icon');
		$this->db->from('mh_modules');
        if($firstURL=='')
		{	$firstURL = $this->uri->segment(1); 	}
        
		if($secondURL=='')
		{	$secondURL = $this->uri->segment(2);	}
		
        if($firstURL!='' && $secondURL=='')
        {	$this->db->where('module_controller',$firstURL);	}
        else if($secondURL!='')
        {   $fullURL = $firstURL.'/'.$secondURL;
            $this->db->like('module_controller',$fullURL);	}
			
            $this->db->where('module_status','A');
			$this->db->limit(1);
			$query = $this->db->get();			
			foreach($query->result() as $menuData)
			{
				$ppx['moduleid'] = $menuData->moduleid;
				if($menuData->module_parent==0)
				{	$ppx['module_parent'] = $menuData->moduleid;	}
				else
				{	$ppx['module_parent'] = $menuData->module_parent;	}
				$ppx['module_name'] = $menuData->module_name;
				$ppx['module_text'] = $menuData->module_text;
				$ppx['module_controller'] = $menuData->module_controller;
				$ppx['module_icon'] = $menuData->module_icon;
			}			
			$query->free_result();
			//Child Menu Results 
			$this->db->select('moduleid,module_parent,module_name,module_icon,module_controller');
			$this->db->from('mh_modules');
			$this->db->where('module_parent',$menuData->moduleid); 
			$this->db->where('module_status','A');
			$this->db->where('show_on_menu','1');
			$this->db->order_by("module_order", "ASC");
			$childQuery = $this->db->get();	
			$ppx['childMenus'] = $childQuery->result();	
			
			return $ppx;
	}
//-------------------------------------------------------------------	
	/**
	 * Get Module Inforamtion
	 *
	 * @access	public
	 * @return	object
	 */
	function get_module_name_icon($id)
	{
		$this->db->select('moduleid,module_name,module_icon');
		$this->db->from('mh_modules');
        $this->db->where('moduleid',$id);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->row();		
	}
//-------------------------------------------------------------------
	/**
	 * Checking Permission
	 *
	 * @access	public
	 * @return	object
	 */	
 	function check_permission($moduledata,$permtype='v')
    {       
        $moduleid = $moduledata['moduleid'];		  
        $this->db->select('permissionjson');
        $this->db->from('ah_users');
        $this->db->where('userid',$this->_login_userid);
        $query = $this->db->get();
		$userDetail = $this->get_user_detail($this->_login_userid);
		$url_to_go = base_url().$userDetail['profile']->module_controller;		
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $sp)
            {
                $perm = json_decode($sp->permissionjson,TRUE);
                $newperm = $perm[$moduleid];
                if($newperm['v']==0 && $permtype=='v')
                {	redirect($url_to_go);
                }
                else
                {	return $newperm[$permtype];	}	//It will return 0 or 1
            }
        }
        else
        {
            redirect(base_url());
        }
    }
//-------------------------------------------------------------------	
	/**
	 * Checking Other Permission
	 *
	 * @access	public
	 * @return	0/1
	 */	
 	function check_other_permission($moduleid,$type)
    {
        $this->db->select('permissionjson');
        $this->db->from('ah_users');
        $this->db->where('userid',$this->_login_userid);
        $query = $this->db->get();			
        if($query->num_rows() > 0)
        {  	$permResult = $query->row();
			$perm = json_decode($permResult->permissionjson,TRUE);
			$newperm = $perm[$moduleid];			
			return $newperm[$type];//It will return 0 or 1			
        }
        else
        {
           return 0;
        }
    }
//-------------------------------------------------------------------
	/**
	 * Checking Count from list management
	 *
	 * @access	public
	 * @return	number of count
	 */	
 	function dataCountFromList($type=0,$parent=0)
    {
        $this->db->select('list_id');
        $this->db->from('ah_listmanagement');
		if($type!=0 && $type!='')
		{	$this->db->where('list_type',$type);	}
		
		if($parent!=0 && $parent!='')
		{	$this->db->where('list_parent_id',$parent);	}
		
		$this->db->where('delete_record','0');
		
		$query = $this->db->get();
		return $query->num_rows();
    }
//-------------------------------------------------------------------
	/**
	 * Checking Count from list management
	 *
	 * @access	public
	 * @return	number of count
	 */		
	function module_dropbox_permission($name,$value)
    {
        $this->db->select('module_controller,module_name,moduleid');
        $this->db->from('mh_modules');
        $this->db->where('show_on_menu','1');
		$this->db->where('module_controller !=','#');
        $this->db->order_by("module_parent", "ASC");
		if($value!='')
		{	$valuex = $value; }
		else
		{	$valuex = 8; }
            $query = $this->db->get();
            $dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req" placeholder="صفحة الترحيب" >';
           
            foreach($query->result() as $row)
            {
                $dropdown .= '<option value="'.$row->moduleid.'" ';
                if($valuex==$row->moduleid)
                {
                    $dropdown .= 'selected="selected"';
                }
                $dropdown .= '>'.$row->module_name.'</option>';
            }
            $dropdown .= '</select>';
            echo($dropdown);
      }
//-------------------------------------------------------------------
	/**
	 * Checking Count from list management
	 *
	 * @access	public
	 * @return	number of count
	 */	
	 function module_dropbox($name,$value)
		{
			$this->db->select('moduleid,module_name');
			$this->db->from('mh_modules');
			$this->db->where('module_parent','0');
			$this->db->order_by("module_order", "ASC");
				$query = $this->db->get();
				$dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req" placeholder="اختر القسم" >';
				$dropdown .= '<option value="0">اختر القسم</option>';
				foreach($query->result() as $row)
				{
					$dropdown .= '<option value="'.$row->moduleid.'" ';
					if($value==$row->moduleid)
					{
						$dropdown .= 'selected="selected"';
					}
					$dropdown .= '>'.$row->module_name.'</option>';
				}
				$dropdown .= '</select>';
				echo($dropdown);
			
		}
//-------------------------------------------------------------------
	/**
	 * Activity Monitor of user
	 *
	 * @access	public
	 * @return	void
	 */	
	function activity_monitor($userid,$logtype)
	{
		$logArray = array('userid'=>$userid,'acttype'=>$logtype,'logip'=>$_SERVER['REMOTE_ADDR'],'logagent'=>$_SERVER['HTTP_USER_AGENT']);
		$this->db->insert($this->config->item('table_ahul'),$logArray);		
	}
//-------------------------------------------------------------------
	/**
	 * Module List
	 *
	 * @access	public
	 * @return	void
	 */	
	function allModule($parentid=0)
    {
        $this->db->select('moduleid,module_name,module_icon');
        $this->db->from('mh_modules');
        if($parentid!='' && $parentid!='0')
		{	$this->db->where('module_parent',$parentid);	}
		else
		{	$this->db->where('module_parent',0);
        	$this->db->where('module_status','A');			}
			$this->db->order_by("module_order", "ASC");
			$query = $this->db->get();
			return $query->result();
    }
//-------------------------------------------------------------------	
	/**
	 * Uploading Photos
	 *
	 * @access	public
	 * @return	photo name
	 */	
	function upload_file($filefield,$folderpath,$thumb=false,$w=0,$h=0)
	{		
		if (!is_dir($folderpath))
		{	mkdir($folderpath, 0777, true);	}
		
		$config['upload_path'] = $folderpath;
		$config['allowed_types'] = '*';
		$config['max_size']	= '5000';
		$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload($filefield))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
//-------------------------------------------------------------------
	/**
	 * Checking Count from tables
	 *
	 * @access	public
	 * @return	number of count
	 */	
 	function dataCount($table,$column,$value,$type,$sumcolumn=0)
    {
        switch($type)
		{
			case 'SUM';
				$SUMQuery = $this->db->query("SELECT SUM(".$sumcolumn.") as sum FROM `".$table."` WHERE `".$column."`='".$value."'");
				$SUMResult = $SUMQuery->row();
				return $SUMResult->sum;
			break;
			
			case 'COUNT';
				$SUMQuery = $this->db->query("SELECT ".$column." FROM `".$table."` WHERE `".$column."`='".$value."'");				
				return $SUMQuery->num_rows;
			break;
		}	
    }
//----------------------------------------------------------------------
	/*
	* Insert Banner Record
	* @param array $data
	* return True
	*/
	function add_banner($data)
	{
		$this->db->insert('system_images',$data);

		return TRUE;
	}
//-------------------------------------------------------------
	public function get_banner_detail($image_id)
	{
		$this->db->where('imageid',$image_id);
		
		$query = $this->db->get('system_images');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}	
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	public function update_status($image_id,$data)
	{
		$json_data	=	json_encode(array('record'=>'delete','imageid'	=>	$image_id));
		
		$data	=	array('delete_record'=>'1');
		
		$this->db->where('imageid', $image_id);

		$this->db->update('system_images',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	public function update_banner($image_id,$data)
	{
		$json_data	=	json_encode(array('data'=>$data));
		
		$this->db->where('imageid',$image_id);
		$this->db->update('system_images',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
	
	public function get_multi_select_box($name,$case,$type='')
	{
		switch($case)
		{	
			case 'Branch';				
				$query = $this->db->query("SELECT branchid,branchname FROM ah_branchs ORDER by branchname ASC;");
				foreach($query->result() as $nurn)
				{
					$html .= '<li class="checklist"><input type="checkbox" name="'.$name.'[]" value="'.$nurn->branch_id.'"> '.$nurn->branch_name.'</li>';
				}
				return array('c'=>arabic_date($query->num_rows()),'h'=>$html);
			break;
			/*case 'Province';
				$query = $this->db->query("SELECT REIGONID, REIGONNAME FROM election_reigons ORDER by REIGONNAME ASC;");
				foreach($query->result() as $nurn)
				{
					$html .= '<li class="checklist"><input class="electionreigons" type="checkbox" name="'.$name.'[]" value="'.$nurn->REIGONID.'"> '.$nurn->REIGONNAME.'</li>';
				}
				return array('c'=>arabic_date($query->num_rows()),'h'=>$html);
			break;*/
			case 'FirstList';
				$query = $this->db->query("SELECT list_id, list_name FROM ah_listmanagement WHERE list_type='".$type."' AND list_status='1' ORDER by list_order ASC;");
				foreach($query->result() as $nurn)
				{
					$html .= '<li class="checklist"><input type="checkbox" name="'.$name.'[]" value="'.$nurn->list_id.'"> '.$nurn->list_name.'</li>';
				}
				return array('c'=>arabic_date($query->num_rows()),'h'=>$html);
			break;
			case 'SMSTemplate';
				$query = $this->db->query("SELECT templatesubject, template FROM system_sms_template WHERE templatestatus='1' ORDER by templatesubject ASC;");
				foreach($query->result() as $nurn)
				{
					$html .= '<li class="checklist" onClick="setTemplateInBulk(\''.$nurn->template.'\');">'.$nurn->templatesubject.'</li>';
				}
				return array('c'=>arabic_date($query->num_rows()),'h'=>$html);
			break;
		}
	}
	
//-------------------------------------------------------------------
	/*
	 * Get All Bank Accounts Detail
	 * Return OBJECT
	 */
		
	function get_accnumber_by_id($accountid)
	{
		$this->db->select('accountnumber');
		$this->db->where('bankaccountid',$accountid);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_users_bankaccount);
		
		if($query->num_rows() > 0)
		{
			return $query->row()->accountnumber;
		}

	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/
	function get_all_yateem_list($search_string	=	NULL)
	{
		if(!is_numeric($search_string))
		{
			$search_string	=	"AND ya.orphan_name LIKE '%".$search_string."%'";
		}
		else
		{
			$search_string	=	"AND ya.file_number = '".$search_string."'";
		}
		
		$q = $this->db->query("SELECT ya.orphan_id, ya.orphan_name, ya.orphan_picture, ya.date_birth,TIMESTAMPDIFF(YEAR,ya.date_birth,CURDATE()) AS age, al.`list_name`, (SELECT COUNT(assign_id) FROM assigned_orphan WHERE orphan_id=ya.`orphan_id` AND is_old	=	'0') AS kafeelcount FROM ah_yateem_info AS ya, ah_listmanagement AS al WHERE ya.`country_id`=al.`list_id` AND ya.`step`='3' AND ya.`orphan_status`='1' $search_string HAVING age	< '18' ORDER BY kafeelcount ASC");
		return $q->result();
	}
	
//--------------------------------------------------------------------
	/*
	*
	*
	*/
	function get_yateem_under_18_list()
	{
		$query	=	$this->db->query("SELECT *,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE orphan_id NOT IN (SELECT orphan_id FROM `assigned_orphan` WHERE is_old='0') AND step=3 HAVING  age	< '18' AND cancel='0' AND DATE(expire_date) > CURDATE() ORDER BY orphan_id DESC;");
		return $query->result();
	}
	

//-------------------------------------------------------------------
	/*
	*
	*/
	public function get_kafeel_block_reason($orphan_id)
	{
		$this->db->select('*');
		$this->db->where('orphan_id',$orphan_id);
		
		$query = $this->db->get('change_kafeel_detail');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//---------------------------------------------------------------------------
	/*
	* Yateem List Less than 18
	* return OBJECT
	*/
	public function get_yateem_lessthan_18()
	{
		$this->db->select('ai.*');
        $this->db->from('ah_yateem_info as ai');
		$this->db->where('ai.delete_record','0');
		$this->db->where('ai.step',$step);
		$this->db->where('ai.orphan_age <','18');
		
		if($id !="")
		{
			if($type =='branch')
			{
				$this->db->where('ai.user_branch_id',$id);
			}
			else
			{
				$this->db->join('assigned_orphan ao','ao.orphan_id = ai.orphan_id','Inner');
				$this->db->where('ao.sponser_id',$id);
			}
		}
		
		$this->db->order_by("orphan_id", "DESC");
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
//---------------------------------------------------------------------------
	/*
	* Yateem List Greater than 18
	* return OBJECT
	*/
	public function get_yateem_greater_18()
	{
		$this->db->select('ai.*');
        $this->db->from('ah_yateem_info as ai');
		$this->db->where('ai.delete_record','0');
		$this->db->where('ai.step',$step);
		$this->db->where('ai.orphan_age >','18');
		
		if($id !="")
		{
			if($type =='branch')
			{
				$this->db->where('ai.user_branch_id',$id);
			}
			else
			{
				$this->db->join('assigned_orphan ao','ao.orphan_id = ai.orphan_id','Inner');
				$this->db->where('ao.sponser_id',$id);
			}
		}
		
		$this->db->order_by("orphan_id", "DESC");
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	*
	*/
	public function get_merge_id($orphan_id)
	{
		$this->db->select('merge_id');
		$this->db->where('orphan_id',$orphan_id);
		$this->db->where('is_old','0');
		
		$query = $this->db->get('assigned_orphan');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->merge_id;
		}
	}
//-------------------------------------------------------------------
	/*
	*
	*/
	public function this_is_exist($id,$value,$table)
	{
		$this->db->select($id);
		$this->db->where($id,$value);
		
		$query = $this->db->get($table);
		//echo $this->db->last_query();
		if($query->num_rows() > 0)
		{
			return '1';
		}
		else
		{
			return '0';
		}
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function orphan_sponsorship_types($step,$branchid,$parent_id	=	NULL)
	{
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='yateem_boxes' AND list_status='1' AND list_parent_id='0' AND delete_record='0'");
		$result_1	=	$query_1->result();

		$almasadaat	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				
				$data[]	=	array('name'	=>	$type->list_name,'list_id'	=> $type->list_id);
			}
		}
		
		return $data;
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function orphan_sponsorship_types_categories($branchid,$parent_id)
	{
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='yateem_boxes' AND list_parent_id='".$parent_id."' AND list_status='1' AND delete_record='0' ORDER BY list_order ASC");
		$result_1	=	$query_1->result();
		
		$almasadaat	=	array();
		
		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				
				$data[]	=	array('name'	=>	$type->list_name,'list_id'	=> $type->list_id);
			}
		}
		
		return $data;
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function orphan_sponsorship_countries($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name,$orphan_Status,$country_type	=	NULL)
	{
		if($country_id !=	'0' AND $list_parent_id	==	'0')
		{
			$query_string	=	"AND ".$country_id." AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}
		
		
		if($country_type	==	'OUTSIDE')
		{
			$country_string	=	"AND l.list_id != '200'";
		}
		if($country_type	==	'INSIDE')
		{
			$country_string	=	"AND l.list_id = '200'";
		}

		/*$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");

		$result_1	=	$query_1->result();
		$almasadaat	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$this->db->select('COUNT(orphan_id) as total,,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age');
				$this->db->from('ah_yateem_info');
				$this->db->where("delete_record",'0');
				$this->db->where("orphan_status",$orphan_Status);
				$this->db->where("step",$step);
				$this->db->where("country_id",$type->list_id);
				$this->db->having('age	< 18',false);
				$this->db->group_by('country_id','DESC');
				
				$query 		=	$this->db->get();

				$result_2	=	$query->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}

		return $data;*/
		
		// Modified BY M.Muzaffar 12-06-2017
		$query	=	$this->db->query("
		SELECT 
		l.`list_id`,
		l.`list_name` ,IFNULL(total,0) AS total
		FROM
		ah_listmanagement AS l 
		LEFT JOIN 
		(SELECT 
		COUNT(orphan_id) AS total,
		TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age,country_id
		FROM
		`ah_yateem_info` 
		WHERE `ah_yateem_info`.delete_record = '0' 
		AND ah_yateem_info.step = '".$step."' 
		AND ah_yateem_info.orphan_status = '".$orphan_Status."'
		AND ah_yateem_info.orphan_id NOT IN 
		(SELECT 
		orphan_id 
		FROM
		`assigned_orphan`) 
		GROUP BY ah_yateem_info.country_id 
		HAVING age <= '18') AS lnew 
		ON lnew.country_id = list_id 
		WHERE l.`list_type` = 'issuecountry' AND l.`delete_record` = '0' AND l.`list_parent_id` = '0'  ".$country_string."
		GROUP BY l.list_id ;");
		
		if($query->num_rows()	>	0)
		{
			return $query->result();
		}
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function orphan_sponsorship_regions($step,$branchid,$type,$list_parent_id,$section_status,$charity_type_id,$orphan_status)
	{
		if($list_parent_id)
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"";
		}

		/*$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");
		
		$result_1	=	$query_1->result();
		$almasadaat	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$this->db->select('COUNT(orphan_id) as total,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age');
				$this->db->from('ah_yateem_info');
				$this->db->where("delete_record",'0');
				$this->db->where("orphan_status",$orphan_status);
				$this->db->where("step",$step);
				$this->db->where("muhafiza_id",$type->list_id);
				//$this->db->where('expire_date <','CURDATE()'); // This is for checking Orphan is Expired or NOT.

				$this->db->having('age	<18',false);
				$this->db->group_by('muhafiza_id','DESC');

				$query = $this->db->get();

				$result_2	=	$query->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		
		return $data;*/
		
	 		// Modified BY M.Muzaffar 12-06-2017
	 		$query	=	$this->db->query("
			SELECT 
			l.`list_id`,
			l.`list_name` ,IFNULL(total,0) AS total
			FROM
			ah_listmanagement AS l 
			LEFT JOIN 
			(SELECT 
			COUNT(orphan_id) AS total,
			TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age,muhafiza_id
			FROM
			`ah_yateem_info` 
			WHERE `ah_yateem_info`.delete_record = '0' 
			AND ah_yateem_info.step = '".$step."'
			AND ah_yateem_info.orphan_status = '".$orphan_status."'
			AND ah_yateem_info.orphan_id NOT IN 
			(SELECT 
			orphan_id 
			FROM
			`assigned_orphan`) 
			GROUP BY ah_yateem_info.muhafiza_id 
			HAVING age <= '18') AS lnew 
			ON lnew.muhafiza_id = list_id 
			WHERE l.`list_type` = 'issuecountry'  AND l.`delete_record` = '0' AND l.`list_parent_id` = '".$list_parent_id."'
			GROUP BY L.list_id ;");
		
		if($query->num_rows()	>	0)
		{
			return $query->result();
		}
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function orphan_sponsorship_states($step,$branchid,$type,$list_parent_id,$child_id,$charity_type_id,$orphan_status)
	{
		if($list_parent_id)
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"";
		}
		
		
		/*$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");

		$result_1	=	$query_1->result();
		$almasadaat	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$this->db->select('COUNT(orphan_id) as total,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age');
				$this->db->from('ah_yateem_info');
				$this->db->where("delete_record",'0');
				$this->db->where("orphan_status",$orphan_status);
				$this->db->where("step",$step);
				$this->db->where("city_id",$type->list_id);
				//$this->db->where('expire_date <','CURDATE()'); // This is for checking Orphan is Expired or NOT.

				$this->db->having('age	<18',false);
				$this->db->group_by('city_id','DESC');
				$query = $this->db->get();

				$result_2	=	$query->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		return $data;*/
		
		// Modified BY M.Muzaffar 12-06-2017
		$query	=	$this->db->query("
		SELECT 
		l.`list_id`,
		l.`list_name` ,IFNULL(total,0) AS total
		FROM
		ah_listmanagement AS l 
		LEFT JOIN 
		(SELECT 
		COUNT(orphan_id) AS total,
		TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age,city_id
		FROM
		`ah_yateem_info` 
		WHERE `ah_yateem_info`.delete_record = '0' 
		AND ah_yateem_info.step = '".$step."' 
		AND ah_yateem_info.orphan_status = '".$orphan_status."' 
		AND ah_yateem_info.orphan_id NOT IN 
		(SELECT 
		orphan_id 
		FROM
		`assigned_orphan`) 
		GROUP BY ah_yateem_info.city_id 
		HAVING age <= '18') AS lnew 
		ON lnew.city_id = list_id 
		WHERE l.`list_type` = 'issuecountry' AND l.`delete_record` = '0' AND l.`list_parent_id` = '".$list_parent_id."'
		GROUP BY l.list_id;");
		
		if($query->num_rows()	>	0)
		{
			return $query->result();
		}
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function orphans_waiting_countries($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name)
	{
		if($country_id !=	'0' AND $list_parent_id	==	'0')
		{
			$query_string	=	"AND ".$country_id." AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}

		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");

		$result_1	=	$query_1->result();
		$almasadaat	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				/*$this->db->select('COUNT(orphan_id) as total');
				$this->db->from('ah_yateem_info');
				$this->db->where("delete_record",'0');
				$this->db->where("orphan_status",'1');
				$this->db->where("step",$step);
				$this->db->where("country_id",$type->list_id);
				$this->db->order_by('orphan_id','DESC');
				$query 		= $this->db->get();*/
				$query	=	$this->db->query("SELECT COUNT(orphan_id) as total,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE delete_record = '0' AND step='".$step."' AND orphan_status='1' AND country_id='".$type->list_id."' AND  orphan_id  NOT IN (SELECT orphan_id FROM `assigned_orphan` WHERE is_old='0') AND cancel='0' AND DATE(expire_date) > CURDATE() GROUP BY country_id HAVING  age	<'18';");
				
				//echo $this->db->last_query();
				$result_2	=	$query->row()->total;
				
				

				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}

		return $data;
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function orphans_waiting_regions($step,$branchid,$type,$list_parent_id,$section_status,$charity_type_id)
	{
		if($list_parent_id)
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"";
		}

		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");
		
		$result_1	=	$query_1->result();
		$almasadaat	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query	=	$this->db->query("SELECT COUNT(orphan_id) as total,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE delete_record = '0' AND step='".$step."'  AND orphan_status='1' AND muhafiza_id='".$type->list_id."' AND orphan_id  NOT IN (SELECT orphan_id FROM `assigned_orphan` WHERE is_old='0') AND cancel='0' AND DATE(expire_date) > CURDATE() GROUP BY muhafiza_id HAVING  age	<='18';");
				//echo ("SELECT COUNT(orphan_id) as total,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE delete_record = '0' AND step='".$step."'  AND orphan_status='1' AND muhafiza_id=".$type->list_id." AND orphan_id  NOT IN (SELECT orphan_id FROM `assigned_orphan` WHERE is_old='0') AND DATE(expire_date) > CURDATE() GROUP BY muhafiza_id HAVING  age	<='18';");
				//echo '<br>';
				$result_2	=	$query->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		
		//exit();
		
		return $data;
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function orphan_waiting_states($step,$branchid,$type,$list_parent_id,$child_id,$charity_type_id)
	{
		if($list_parent_id)
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"";
		}
		
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");

		$result_1	=	$query_1->result();
		$almasadaat	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{	
				$query	=	$this->db->query("SELECT COUNT(orphan_id) as total,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE delete_record = '0' AND step='".$step."'  AND orphan_status='1' AND city_id='".$type->list_id."' AND orphan_id  NOT IN (SELECT orphan_id FROM `assigned_orphan` WHERE is_old='0') AND cancel='0' AND DATE(expire_date) > CURDATE() GROUP BY city_id HAVING  age	<='18';");

				$result_2	=	$query->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		
		return $data;
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function get_yateeem_list_by_wilaya($step,$city_id	=	NULL,$orphan_status)
	{
		if($step	==	'2')
		{
			$approved_query_string	=	"";
		}
		else
		{
			$approved_query_string	=	"AND DATE(expire_date) > CURDATE()";
		}
		
		$query	=	$this->db->query("SELECT *,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE delete_record = '0' AND cancel='0' AND step='".$step."' AND orphan_status='".$orphan_status."' AND city_id='".$city_id."' AND orphan_id  NOT IN (SELECT orphan_id FROM `assigned_orphan` WHERE is_old='0') ".$approved_query_string." GROUP BY orphan_id HAVING  age	<'18';");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function get_expire_yateeems_list($step,$orphan_status,$city_id	=	NULL)
	{
		$expire_query_string	=	"AND DATE(expire_date) < CURDATE()";
		
		if($city_id)
		{
			$city_query_string	=	"AND city_id='".$city_id."'";
		}
		else
		{
			$city_query_string	=	"";
		}

		$query	=	$this->db->query("SELECT *,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE delete_record = '0' AND step='".$step."' AND orphan_status='1' ".$city_query_string."  ".$expire_query_string." GROUP BY orphan_id HAVING  age	<	'18';");

		//echo $this->db->last_query();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function sponsorship_countries($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name,$sponsor_status)
	{
		if($country_id !=	'0' AND $list_parent_id	==	'0')
		{
			$query_string	=	"AND ".$country_id." AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}

		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");

		$result_1	=	$query_1->result();
		$almasadaat	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{	
				$query	=	$this->db->query("SELECT COUNT(sponser_id) as total FROM ah_sponser_info WHERE country_id='".$type->list_id."' AND step = '".$step."' AND sponsor_status = '".$sponsor_status."' AND delete_record = '0' ORDER BY sponser_id DESC;");

				$result_2	=	$query->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		
		return $data;
		
		// Modified By Muhammad Muzaffar
		/*$query	=	$this->db->query("
		SELECT 
		l.`list_id`,
		l.`list_name`,
		IFNULL(total, 0) AS total 
		FROM
		ah_listmanagement AS l 
		LEFT JOIN 
		(SELECT 
		COUNT(sponser_id) AS total,country_id
		FROM
		ah_sponser_info 
		WHERE step = '".$step."' 
		AND sponsor_status = '".$sponsor_status."' 
		AND delete_record = '0'
		GROUP BY country_id 
		) AS lnew 
		ON lnew.country_id = list_id 
		WHERE l.`list_type` = 'issuecountry' 
		AND l.`delete_record` = '0' 
		AND l.`list_parent_id` = '0' 
		GROUP BY l.list_id ;");
		
		if($query->num_rows()	>	0)
		{
			$query->result();
		}
		*/																			

	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function sponsorship_regions($step,$branchid,$type,$list_parent_id,$section_status,$charity_type_id,$sponsor_status)
	{
		if($list_parent_id)
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"";
		}
		
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");
		
		$result_1	=	$query_1->result();
		$almasadaat	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query	=	$this->db->query("SELECT COUNT(sponser_id) as total FROM ah_sponser_info WHERE muhafiza_id='".$type->list_id."' AND step = '".$step."' AND sponsor_status = '".$sponsor_status."' AND delete_record = '0' ORDER BY sponser_id DESC;");
					
				$result_2	=	$query->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		
		return $data;
		
		// Modified By Muhammad Muzaffar
		/*$query	=	$this->db->query("
		SELECT 
		l.`list_id`,
		l.`list_name`,
		IFNULL(total, 0) AS total 
		FROM
		ah_listmanagement AS l 
		LEFT JOIN 
		(SELECT 
		COUNT(sponser_id) AS total,muhafiza_id
		FROM
		ah_sponser_info 
		WHERE step = '".$step."' 
		AND sponsor_status = '".$sponsor_status."' 
		AND delete_record = '0'
		GROUP BY muhafiza_id 
		) AS lnew 
		ON lnew.muhafiza_id = list_id 
		WHERE l.`list_type` = 'issuecountry' 
		AND l.`delete_record` = '0' 
		AND l.`list_parent_id` = '".$list_parent_id."' 
		GROUP BY l.list_id;");

		if($query->num_rows()	>	0)
		{
			$query->result();
		}
		*/
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function sponsorship_states($step,$branchid,$type,$list_parent_id,$child_id,$charity_type_id,$sponsor_status)
	{
		if($list_parent_id)
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"";
		}
		
		
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");
		//echo $this->db->last_query();
		$result_1	=	$query_1->result();
		$almasadaat	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query	=	$this->db->query("SELECT COUNT(sponser_id) as total FROM ah_sponser_info WHERE city_id='".$type->list_id."' AND step = '".$step."' AND sponsor_status = '".$sponsor_status."' AND delete_record = '0' ORDER BY sponser_id DESC;");
				$result_2	=	$query->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		return $data;
		
		// Modified By Muhammad Muzaffar
		/*$query	=	$this->db->query("
		SELECT 
		l.`list_id`,
		l.`list_name`,
		IFNULL(total, 0) AS total 
		FROM
		ah_listmanagement AS l 
		LEFT JOIN 
		(SELECT 
		COUNT(sponser_id) AS total,city_id
		FROM
		ah_sponser_info 
		WHERE step = '".$step."' 
		AND sponsor_status = '".$sponsor_status."' 
		AND delete_record = '0'
		GROUP BY city_id 
		) AS lnew 
		ON lnew.city_id = list_id 
		WHERE l.`list_type` = 'issuecountry' 
		AND l.`delete_record` = '0' 
		AND l.`list_parent_id` = '".$list_parent_id."' 
		GROUP BY l.list_id;");
		
		echo $this->db->last_query();exit();
		if($query->num_rows()	>	0)
		{
			$query->result();
		}*/
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function sponsorship_waiting_countries($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name)
	{
		$sponsor_status	=	'1';
		
		if($country_id !=	'0' AND $list_parent_id	==	'0')
		{
			$query_string	=	"AND ".$country_id." AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}

		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");

		$result_1	=	$query_1->result();
		
		$almasadaat	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query	=	$this->db->query("SELECT COUNT(sponser_id) as total FROM ah_sponser_info WHERE country_id='".$type->list_id."' AND step = '".$step."' AND delete_record = '0' AND sponsor_status = '1' AND cancel='0' AND sponser_id  NOT IN (SELECT sponser_id FROM `assigned_orphan` WHERE is_old='0') ORDER BY sponser_id DESC;");
				//echo $this->db->last_query();exit();
				$result_2	=	$query->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		
		return $data;
		
		// Modified By Muhammad Muzaffar
		/*$query	=	$this->db->query("
		SELECT 
		l.`list_id`,
		l.`list_name`,
		IFNULL(total, 0) AS total 
		FROM
		ah_listmanagement AS l 
		LEFT JOIN 
		(SELECT 
		COUNT(sponser_id) AS total,country_id
		FROM
		ah_sponser_info 
		WHERE step = '".$step."' 
		AND sponsor_status = '".$sponsor_status."' 
		AND delete_record = '0' AND sponser_id  NOT IN (SELECT sponser_id FROM `assigned_orphan` WHERE is_old='0')
		GROUP BY country_id 
		) AS lnew 
		ON lnew.country_id = list_id 
		WHERE l.`list_type` = 'issuecountry' 
		AND l.`delete_record` = '0' 
		AND l.`list_parent_id` = '0' 
		GROUP BY l.list_id ;");
		
		if($query->num_rows()	>	0)
		{
			$query->result();
		}
		*/
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function sponsorship_waiting_regions($step,$branchid,$type,$list_parent_id,$section_status,$charity_type_id)
	{
		$sponsor_status	=	'1';
		
		if($list_parent_id)
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"";
		}
		
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");
		
		$result_1	=	$query_1->result();
		$almasadaat	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query	=	$this->db->query("SELECT COUNT(sponser_id) as total FROM ah_sponser_info WHERE muhafiza_id='".$type->list_id."' AND step = '".$step."' AND delete_record = '0' AND sponsor_status = '1' AND cancel='0' AND sponser_id  NOT IN (SELECT sponser_id FROM `assigned_orphan` WHERE is_old='0') ORDER BY sponser_id DESC;");

				$result_2	=	$query->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		return $data;
		
		// Modified By Muhammad Muzaffar
		/*$query	=	$this->db->query("
		SELECT 
		l.`list_id`,
		l.`list_name`,
		IFNULL(total, 0) AS total 
		FROM
		ah_listmanagement AS l 
		LEFT JOIN 
		(SELECT 
		COUNT(sponser_id) AS total,muhafiza_id
		FROM
		ah_sponser_info 
		WHERE step = '".$step."' 
		AND sponsor_status = '".$sponsor_status."' 
		AND delete_record = '0' AND sponser_id  NOT IN (SELECT sponser_id FROM `assigned_orphan` WHERE is_old='0')
		GROUP BY muhafiza_id 
		) AS lnew 
		ON lnew.muhafiza_id = list_id 
		WHERE l.`list_type` = 'issuecountry' 
		AND l.`delete_record` = '0' 
		AND l.`list_parent_id` = '".$list_parent_id."' 
		GROUP BY l.list_id;");

		if($query->num_rows()	>	0)
		{
			$query->result();
		}
		*/
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function sponsors_waiting_states($step,$branchid,$type,$list_parent_id,$child_id,$charity_type_id)
	{
		if($list_parent_id)
		{
			$query_string	=	"AND list_parent_id='".$list_parent_id."'";
		}
		else
		{
			$query_string	=	"";
		}
		
		
		$query_1 	=	$this->db->query("SELECT list_id,list_name FROM `ah_listmanagement` WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ".$query_string." ");
		//echo $this->db->last_query();
		$result_1	=	$query_1->result();
		$almasadaat	=	array();

		if ($query_1->num_rows() > 0)
		{
			foreach($result_1 as $type)
			{
				$query	=	$this->db->query("SELECT COUNT(sponser_id) as total FROM ah_sponser_info WHERE city_id='".$type->list_id."' AND step = '".$step."' AND delete_record = '0' AND step = '".$step."' AND sponsor_status = '1' AND cancel='0' AND sponser_id  NOT IN (SELECT sponser_id FROM `assigned_orphan` WHERE is_old='0') ORDER BY sponser_id DESC;");
				$result_2	=	$query->row()->total;
				
				$almasadaat[$type->list_name]	=	 $result_2;
				
				$data[]	=	array('name'	=>	$type->list_name,'y'	=> (int)$result_2,'list_id'	=> $type->list_id);
			}
		}
		return $data;
	}
//--------------------------------------------------------------------
	/*
	* All Sponsors By Provinces
	* 
	*/	
	public function get_kafeel_list_by_wilaya($step,$city_id	=	NULL,$sponsor_status)
	{
		//$query	=	$this->db->query("SELECT *,(SELECT COUNT(sponser_id) AS total FROM `assigned_orphan` WHERE assigned_orphan.`sponser_id`=ah_sponser_info.`sponser_id`) AS sponserCount FROM `ah_sponser_info` WHERE city_id='".$city_id."' AND sponsor_status = '".$sponsor_status."' AND step = '".$step."' AND delete_record = '0' ORDER BY sponser_id DESC;");
		$query	=	$this->db->query("SELECT *,(SELECT COUNT(sponser_id) AS total FROM `assigned_orphan` WHERE assigned_orphan.`sponser_id`=ah_sponser_info.`sponser_id`) AS sponserCount FROM `ah_sponser_info` WHERE city_id='".$city_id."' AND sponsor_status = '".$sponsor_status."' AND step = '".$step."' AND delete_record = '0' AND cancel='0' AND sponser_id  NOT IN (SELECT sponser_id FROM `assigned_orphan` WHERE is_old='0') ORDER BY sponser_id DESC;");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
//------------------------------------------------------
	/*
	*
	*
	*/
	public function expire_orphan_countries($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name,$orphan_Status)
	{
		$query	=	$this->db->query("
		SELECT 
			l.list_id,
			l.list_name ,IFNULL(total,0) AS total
		FROM
			ah_listmanagement AS l 
		LEFT JOIN 
			(SELECT 
			COUNT(orphan_id) AS total,
			TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age,country_id
		FROM
			`ah_yateem_info` AS YI
		WHERE YI.delete_record = '0' 
		AND YI.step = '".$step."' 
		AND YI.orphan_status = '1'
		AND YI.expire_date < CURDATE() 
		AND YI.cancel = '0' 
		GROUP BY YI.country_id 
		HAVING age < '18') AS lnew 
		ON lnew.country_id = list_id 
		WHERE l.list_type = 'issuecountry' AND l.delete_record = '0' AND l.list_parent_id = '0'
		GROUP BY l.list_id ;");
		
		if ($query->num_rows() > 0)
		{
			return	$query->result();
		}

	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function expire_orphan_provinces($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name,$orphan_Status)
	{
		$query	=	$this->db->query("
		SELECT 
			l.list_id,
			l.list_name ,IFNULL(total,0) AS total
		FROM
			ah_listmanagement AS l 
		LEFT JOIN 
			(SELECT 
			COUNT(orphan_id) AS total,
			TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age,muhafiza_id
		FROM
			`ah_yateem_info` AS YI
		WHERE YI.delete_record = '0' 
		AND YI.step = '".$step."' 
		AND YI.orphan_status = '1'
		AND YI.expire_date < CURDATE()
		AND YI.cancel = '0'
		GROUP BY YI.country_id 
		HAVING age < '18') AS lnew 
		ON lnew.muhafiza_id = list_id 
		WHERE l.list_type = 'issuecountry' AND l.delete_record = '0' AND l.list_parent_id = '".$country_id."'
		GROUP BY l.list_id ;");
		
		//echo $this->db->last_query();exit();
		if ($query->num_rows() > 0)
		{
			return	$query->result();
		}

	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function expire_orphan_cities($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name,$orphan_Status)
	{
		$query	=	$this->db->query("
		SELECT 
			l.list_id,
			l.list_name ,IFNULL(total,0) AS total
		FROM
			ah_listmanagement AS l 
		LEFT JOIN 
			(SELECT 
			COUNT(orphan_id) AS total,
			TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age,city_id
		FROM
			`ah_yateem_info` AS YI
		WHERE YI.delete_record = '0' 
		AND YI.step = '".$step."' 
		AND YI.orphan_status = '1'
		AND YI.expire_date < CURDATE() 
		AND YI.cancel = '0' 
		GROUP BY YI.country_id 
		HAVING age < '18') AS lnew 
		ON lnew.city_id = list_id 
		WHERE l.list_type = 'issuecountry' AND l.delete_record = '0' AND l.list_parent_id = '".$country_id."'
		GROUP BY l.list_id ;");
		
		if ($query->num_rows() > 0)
		{
			return	$query->result();
		}

	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function cancel_orphan_countries($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name,$orphan_Status)
	{
		$query	=	$this->db->query("
		SELECT 
			l.list_id,
			l.list_name ,IFNULL(total,0) AS total
		FROM
			ah_listmanagement AS l 
		LEFT JOIN 
			(SELECT 
			COUNT(orphan_id) AS total,
			TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age,country_id
		FROM
			`ah_yateem_info` AS YI
		WHERE YI.delete_record = '0' 
		AND YI.step = '".$step."' 
		AND YI.orphan_status = '1' 
		AND YI.cancel = '1' 
		GROUP BY YI.country_id) AS lnew 
		ON lnew.country_id = list_id 
		WHERE l.list_type = 'issuecountry' AND l.delete_record = '0' AND l.list_parent_id = '0'
		GROUP BY l.list_id ;");
		
		//AND YI.expire_date < CURDATE()
		
		if ($query->num_rows() > 0)
		{
			return	$query->result();
		}

	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function cancel_orphan_provinces($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name,$orphan_Status)
	{
		$query	=	$this->db->query("
		SELECT 
			l.list_id,
			l.list_name ,IFNULL(total,0) AS total
		FROM
			ah_listmanagement AS l 
		LEFT JOIN 
			(SELECT 
			COUNT(orphan_id) AS total,
			TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age,muhafiza_id
		FROM
			`ah_yateem_info` AS YI
		WHERE YI.delete_record = '0' 
		AND YI.step = '".$step."' 
		AND YI.orphan_status = '1'
		AND YI.cancel = '1'
		GROUP BY YI.muhafiza_id) AS lnew 
		ON lnew.muhafiza_id = list_id 
		WHERE l.list_type = 'issuecountry' AND l.delete_record = '0' AND l.list_parent_id = '".$country_id."'
		GROUP BY l.list_id ;");
		
		//echo $this->db->last_query();
		//exit();
		if ($query->num_rows() > 0)
		{
			return	$query->result();
		}

	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function cancel_orphan_cities($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name,$orphan_Status)
	{
		$query	=	$this->db->query("
		SELECT 
			l.list_id,
			l.list_name ,IFNULL(total,0) AS total
		FROM
			ah_listmanagement AS l 
		LEFT JOIN 
			(SELECT 
			COUNT(orphan_id) AS total,
			TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age,city_id
		FROM
			`ah_yateem_info` AS YI
		WHERE YI.delete_record = '0' 
		AND YI.step = '".$step."' 
		AND YI.orphan_status = '1' 
		AND YI.cancel = '1' 
		GROUP BY YI.city_id) AS lnew 
		ON lnew.city_id = list_id 
		WHERE l.list_type = 'issuecountry' AND l.delete_record = '0' AND l.list_parent_id = '".$country_id."'
		GROUP BY l.list_id ;");
		
		//echo $this->db->last_query();
		//exit();
		if ($query->num_rows() > 0)
		{
			return	$query->result();
		}

	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function get_cancel_yateems_list($step,$city_id	=	NULL,$orphan_status)
	{
		//$expire_query_string	=	"AND DATE(expire_date) < CURDATE()";
		$expire_query_string	=	"";
		
		$query	=	$this->db->query("SELECT *,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE delete_record = '0' AND step='".$step."' AND orphan_status='1' AND cancel='1' AND city_id='".$city_id."'  ".$expire_query_string." GROUP BY orphan_id;");

		//echo $this->db->last_query(); exit();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	*
	*/
	function get_orphan_step2_info($orphan_id)
	{
		$this->db->select('*');
        $this->db->from('ah_yateem_servay');
		$this->db->where('orphan_id',$orphan_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->row();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	* Renew The Orphans
	*/	
	public function renew_orphan($orphan_id,$data)
	{
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('orphan_id',$orphan_id);
		$this->db->update("ah_yateem_info",$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------	
	/*
	*	FOR UPDATING ORPHAN RENEW HISTORY
	*	return TRUE
	*/
	function update_orphan_renew_history($data)
	{
		$this->db->insert('renew_orphans',$data,json_encode($data),$this->_login_userid);

		return TRUE;	
	}
//---------------------------------------------------------------
	/*
	*
	*/
	public function get_orphan_renew_history($orphan_id)
	{
		$this->db->select("document_id,document_name,orphan_id,doc_type_id,created");
		$this->db->from("ah_yateem_documents_history");
		$this->db->where("orphan_id",$orphan_id);
		$this->db->order_by("created","DESC");
		$query	=	$this->db->get();

		if($query->num_rows()	>	0)
		{
			return $query->result();
		}
	}
//---------------------------------------------------------------
	/*
	*
	*/
	public function stop_sponsor($orphan_id)
	{
		$yateemQuery = $this->db->query("SELECT * FROM assigned_orphan WHERE `orphan_id`='".$orphan_id."' AND is_old='0' LIMIT 1;");
			
		if($yateemQuery->num_rows() > 0)
		{
			$sponser_id = $yateemQuery->row()->sponser_id;
			
			$this->db->where('sponser_id',$sponser_id);

			$data	=	array('is_old'	=>	'1', 'end_date'	=>	date('Y-m-d'));
			$this->db->update('assigned_orphan',json_encode($data),$this->session->userdata('userid'),$data);
		}
		
		return TRUE;
	}
//---------------------------------------------------------------
	/*
	*
	*/
	public function stop_orphan($orphan_id)
	{
		$this->db->where('orphan_id',$orphan_id);
			
		$data	=	array('cancel'	=>	'1');
		$this->db->update('ah_yateem_info',json_encode($data),$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//---------------------------------------------------------------
	/*
	*
	*/
	public function start_orphan($orphan_id)
	{
		$this->db->where('orphan_id',$orphan_id);
			
		$data	=	array('cancel'	=>	'0');
		$this->db->update('ah_yateem_info',json_encode($data),$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//---------------------------------------------------------------
	/*
	*
	*/
	public function add_reason_for_stoping($cancel_reason,$table)
	{
		$this->db->insert($table,$cancel_reason,json_encode($cancel_reason),$this->session->userdata('userid'));	// Insert Comments and reason
		
		return TRUE;
	}
	
//---------------------------------------------------------------
	/*
	*
	*/
	public function get_cancel_orphan_detail($orphan_id,$table)
	{
		$this->db->select("id,orphan_id,notes,userid,submitted_date");
		$this->db->from($table);
		$this->db->where("orphan_id",$orphan_id);

		$query	=	$this->db->get();

		if($query->num_rows()	>	0)
		{
			return $query->result();
		}
	}
//---------------------------------------------------------------
	/*
	*
	*/
	public function stop_all_orphans_by_sponsor($sponser_id)
	{
		$this->db->where('sponser_id',$sponser_id);
		
		$data	=	array('is_old'	=>	'1', 'end_date'	=>	date('Y-m-d'));
		$this->db->update('assigned_orphan',json_encode($data),$this->session->userdata('userid'),$data);

		return TRUE;
	}
//---------------------------------------------------------------
	/*
	*
	*/
	public function cancel_the_spnosor($sponser_id)
	{
		$this->db->where('sponser_id',$sponser_id);
			
		$data	=	array('cancel'	=>	'1');
		$this->db->update('ah_sponser_info',json_encode($data),$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//---------------------------------------------------------------
	/*
	*
	*/
	public function add_reason_for_stoping_stoping($cancel_reason,$table)
	{
		$this->db->insert($table,$cancel_reason,json_encode($cancel_reason),$this->session->userdata('userid'));// Insert Comments and reason
		
		return TRUE;
	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function cancel_sponsor_countries($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name,$orphan_Status)
	{
		$query	=	$this->db->query("
		SELECT 
			l.list_id,
			l.list_name,
			IFNULL(total, 0) AS total 
		FROM
			ah_listmanagement AS l 
		LEFT JOIN 
			(SELECT 
			COUNT(sponser_id) AS total,
			country_id
			FROM
			ah_sponser_info AS SI
			WHERE step = '".$step."' 
			AND delete_record = '0' 
			AND sponsor_status = '1' 
			AND cancel = '1' 
			GROUP BY country_id) AS lnew 
		ON lnew.country_id = list_id 
		WHERE l.list_type = 'issuecountry' 
		AND l.delete_record = '0' 
		AND l.list_parent_id = '0' 
		GROUP BY l.list_id ;");
		
		if ($query->num_rows() > 0)
		{
			return	$query->result();
		}

	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function cancel_sponsor_provinces($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name,$orphan_Status)
	{
		$query	=	$this->db->query("
		SELECT 
			l.list_id,
			l.list_name,
			IFNULL(total, 0) AS total 
		FROM
			ah_listmanagement AS l 
		LEFT JOIN 
			(SELECT 
			COUNT(sponser_id) AS total,
			muhafiza_id
			FROM
			ah_sponser_info AS SI
			WHERE step = '".$step."' 
			AND delete_record = '0' 
			AND sponsor_status = '1' 
			AND cancel = '1' 
			GROUP BY muhafiza_id) AS lnew 
		ON lnew.muhafiza_id = list_id 
		WHERE l.list_type = 'issuecountry' 
		AND l.delete_record = '0' 
		AND l.list_parent_id = '".$country_id."' 
		GROUP BY l.list_id ;");
		
		if ($query->num_rows() > 0)
		{
			return	$query->result();
		}

	}
//------------------------------------------------------
	/*
	*
	*
	*/
	public function cancel_sponsor_cities($step,$branchid,$type,$country_id,$list_parent_id,$section_status,$charity_type_id,$coloum_name,$orphan_Status)
	{
		$query	=	$this->db->query("
		SELECT 
			l.list_id,
			l.list_name,
			IFNULL(total, 0) AS total 
		FROM
			ah_listmanagement AS l 
		LEFT JOIN 
			(SELECT 
			COUNT(sponser_id) AS total,
			city_id
			FROM
			ah_sponser_info AS SI
			WHERE step = '".$step."' 
			AND delete_record = '0' 
			AND sponsor_status = '1' 
			AND cancel = '1' 
			GROUP BY city_id) AS lnew 
		ON lnew.city_id = list_id 
		WHERE l.list_type = 'issuecountry' 
		AND l.delete_record = '0' 
		AND l.list_parent_id = '".$country_id."' 
		GROUP BY l.list_id ;");
		
		if ($query->num_rows() > 0)
		{
			return	$query->result();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	*
	*/	
	function get_cancel_sponsors_list($step,$city_id	=	NULL,$orphan_status)
	{
		$query	=	$this->db->query("SELECT * FROM `ah_sponser_info` WHERE delete_record = '0' AND city_id='".$city_id."' AND cancel='1' AND step='".$step."' ORDER BY sponser_id DESC;");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//---------------------------------------------------------------
	/*
	*
	*/
	public function continue_the_spnosor($sponser_id)
	{
		$this->db->where('sponser_id',$sponser_id);
			
		$data	=	array('cancel'	=>	'0');
		$this->db->update('ah_sponser_info',json_encode($data),$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//--------------------------------------------------------------------
	/*
	* GET the Sponsor Detail about cancellation and Countinue process
	* 
	* @param $sponser_id integer
	*/	
	function get_sponsor_start_stop_info($sponser_id)
	{
		$this->db->select('id,sponser_id,notes,submitted_date,userid,sponsor_action');
		$this->db->where('sponser_id',$sponser_id);
		$this->db->from('ah_sponsor_stop_start_detail');
		$query	=	$this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//--------------------------------------------------------------------
	/*
	* 
	* 
	* 
	*/	
	public function get_notification_count_approval($type,$status,$step,$country_type,$country_id,$cancel,$case	=	NULL)
	{
		if($country_type	==	'INSIDE')
		{
			if($type	==	'ORPHAN')
			{
				$query_string	=	"AND country_id = '".$country_id."' AND YA.`orphan_status`	= '".$status."'";
			}
			else
			{
				$query_string	=	"AND country_id = '".$country_id."' AND S.`sponsor_status`	= '".$status."'";
			}
		}
		elseif($country_type	==	'OUTSIDE')
		{
			if($type	==	'ORPHAN')
			{
				$query_string	=	"AND country_id != '".$country_id."' AND YA.`orphan_status`	= '".$status."'";
			}
			else
			{
				$query_string	=	"AND country_id != '".$country_id."' AND S.`sponsor_status`	= '".$status."'";
			}
		}
		elseif($country_type	==	'REJECT')
		{
			if($type	==	'ORPHAN')
			{
				$query_string	=	"AND YA.`orphan_status`	= '".$status."'";
			}
			else
			{
				$query_string	=	"AND S.`sponsor_status`	= '".$status."'";
			}
		}
		else
		{
			if($type	==	'ORPHAN')
			{
				$query_string	=	"AND YA.`orphan_status`	= '".$status."'";
			}
			else
			{
				$query_string	=	"AND S.`sponsor_status`	= '".$status."'";
			}
		}
		
		if($case	==	'GREATER')
		{
			$oprator	=	">";
		}
		else
		{
			$oprator	=	"<";
		}
		
		if($case	==	NULL)
		{
			$case_string	=	"`case` IS NULL";
		}
		else
		{
			$case_string	=	"`case` = '".$case."'";
		}
		
		if($type	==	'ORPHAN')
		{
			$query	=	$this->db->query("
			SELECT 
			COUNT(YA.step) AS total,
			TIMESTAMPDIFF(YEAR, YA.`date_birth`, CURDATE()) AS age,
			YA.`step`
			FROM
			`ah_yateem_info`  AS YA
			INNER JOIN 
			(SELECT 
			* 
			FROM
			`notifications` 
			WHERE `read` = '0' 
			AND notification_to = '".$type."' AND `status` = '".$status."' AND ".$case_string.") AS N 
			ON (
			N.reffrence_id = YA.`orphan_id`
			) 
			WHERE YA.`delete_record` = '0'
			".$query_string." 
			AND YA.`step` = '".$step."'
			AND YA.`cancel` = '".$cancel."' 
			GROUP BY YA.`step` 
			HAVING age ".$oprator." '18'");
		}
		else
		{
			$query	=	$this->db->query("
			SELECT 
			COUNT(S.sponser_id) AS total
			FROM
			ah_sponser_info AS S
			INNER JOIN 
			(SELECT 
			* 
			FROM
			`notifications` 
			WHERE `read` = '0' 
			AND notification_to = 'SPONSOR' 
			AND `status` = '".$status."' AND ".$case_string.") AS N 
			ON (N.reffrence_id = S.`sponser_id`) 
			WHERE S.delete_record = '0' 
			".$query_string." 
			AND S.step = '".$step."' 
			AND S.`cancel` = '".$cancel."' 
			AND S.sponsor_status = '".$status."' 
			GROUP BY S.step DESC;");
		}
		if($step	==	'3')
		{
			//echo $this->db->last_query();exit();
		}
		
		if($query->num_rows() > 0)
		{
			return $query->row()->total;
		}
	}
//--------------------------------------------------------------------
	/*
	* 
	* 
	* 
	*/	
	public function see_notify_orphans_sponsors($type,$status,$step,$country_type,$country_id	=	NULL,$case_param	=	NULL)
	{

		if($country_type	==	'INSIDE')
		{
			if($type	==	'ORPHAN')
			{
				$query_string	=	"AND country_id = '".$country_id."' AND YA.`orphan_status`	= '".$status."'";
			}
			else
			{
				$query_string	=	"AND country_id = '".$country_id."' AND S.`sponsor_status`	= '".$status."'";
			}
		}
		elseif($country_type	==	'OUTSIDE')
		{
			if($type	==	'ORPHAN')
			{
				$query_string	=	"AND country_id != '".$country_id."' AND YA.`orphan_status`	= '".$status."'";
			}
			else
			{
				$query_string	=	"AND country_id != '".$country_id."' AND S.`sponsor_status`	= '".$status."'";
			}
		}
		elseif($country_type	==	'REJECT')
		{
			if($type	==	'ORPHAN')
			{
				$query_string	=	"AND YA.`orphan_status`	= '".$status."'";
			}
			else
			{
				$query_string	=	"AND S.`sponsor_status`	= '".$status."'";
			}
		}
		else
		{
			if($type	==	'ORPHAN')
			{
				$query_string	=	"AND YA.`orphan_status`	= '".$status."'";
			}
			else
			{
				$query_string	=	"AND S.`sponsor_status`	= '".$status."'";
			}
		}
		
		if($case_param	==	'')
		{
			$case_string	=	"`case` IS NULL";
		}
		else
		{
			$case_string	=	"`case` = '".$case_param."'";
		}
		
		if($type	==	'ORPHAN')
		{
			$query	=	$this->db->query("
			SELECT 
			YA.*,
			TIMESTAMPDIFF(YEAR, YA.`date_birth`, CURDATE()) AS age
			FROM
			`ah_yateem_info`  AS YA
			INNER JOIN 
			(SELECT 
			* 
			FROM
			`notifications` 
			WHERE `read` = '0' 
			AND notification_to = '".$type."' AND `status` = '".$status."' AND ".$case_string.") AS N 
			ON (
			N.reffrence_id = YA.`orphan_id`
			) 
			WHERE YA.`delete_record` = '0'
			".$query_string." 
			AND YA.`step` = '".$step."' 
			GROUP BY YA.`orphan_id` DESC");
		}
		else
		{
			$query	=	$this->db->query("
			SELECT 
			S.*
			FROM
			ah_sponser_info AS S
			INNER JOIN 
			(SELECT 
			* 
			FROM
			`notifications` 
			WHERE `read` = '0' 
			AND notification_to = 'SPONSOR' 
			AND `status` = '".$status."') AS N 
			ON (N.reffrence_id = S.`sponser_id`) 
			WHERE S.delete_record = '0' 
			".$query_string." 
			AND S.step = '".$step."' 
			AND S.sponsor_status = '".$status."' 
			GROUP BY S.sponser_id DESC;");
		}
		
		//echo $this->db->last_query();exit();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//--------------------------------------------------------------------
	/*
	*
	* Renew The Orphans
	*/	
	public function read_notification_orphans_sponsors($read_notification,$reffrence_id,$status,$step)
	{
		$json_data	=	json_encode(array('data'	=>	$read_notification));
		
		$this->db->where('reffrence_id',$reffrence_id);
		$this->db->where('status',$status);
		$this->db->where('step',$step);
		
		$this->db->update("notifications",$json_data,$this->session->userdata('userid'),$read_notification);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	*
	*
	*/
	public function get_user_remaining_leaves($userid)
	{
		$this->db->select('attendence_id,userid,attendence_status,absent_type,reason,reason_text,document,late,attendence_date,delete_record');
		$this->db->where('userid',$userid);
		$query	=	$this->db->get("ah_attendence");
		
		if($query->num_rows()	>	0)
		{
			return $query->result();
		}
		
	}
//--------------------------------------------------------------------

//-------------------------------------------------------------------
	/*
	*
	*
	*/
	public function get_all_orphan_by_status($step,$status,$country,$cancel,$type,$action	=	NULL)
	{
		if($step	==	'2')
		{
			$approved_query_string	=	"";
		}
		else
		{
			$approved_query_string	=	"AND DATE(expire_date) > CURDATE()";
		}
		
		if($action	==	'EXPIRE')
		{
			$expire_oprator	=	">=";
		}
		else
		{
			$expire_oprator	=	"<";
		}
		
		if($type	==	'ALL')
		{
			$country	=	"";
		}
		else if($type	==	'INSIDE')
		{
			$country	=	"AND country_id='".$country."'";
		}
		else if($type	==	'OUTSIDE')
		{
			$country	=	"AND country_id!='".$country."'";
		}
		else
		{
			$country	=	"AND country_id='".$country."'";
		}
		
		$query	=	$this->db->query("SELECT *,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE delete_record = '0' AND step='".$step."' AND orphan_status='".$status."' ".$country." AND cancel='".$cancel."' AND orphan_id  NOT IN (SELECT orphan_id FROM `assigned_orphan` WHERE is_old='0') ".$approved_query_string." ".$expire_orphan_string." GROUP BY orphan_id HAVING  age	".$expire_oprator." '18';");
		
		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}	
//--------------------------------------------------------------------
	/*
	* All Sponsors By Provinces
	* 
	*/	
	public function get_all_sponsors_by_status($step,$status,$country,$cancel,$type)
	{
		if($type	==	'ALL')
		{
			$country	=	"";
		}
		else if($type	==	'INSIDE')
		{
			$country	=	"AND country_id='".$country."'";
		}
		else if($type	==	'OUTSIDE')
		{
			$country	=	"AND country_id!='".$country."'";
		}
		else
		{
			$country	=	"AND country_id='".$country."'";
		}
		
		$query	=	$this->db->query("SELECT *,(SELECT COUNT(sponser_id) AS total FROM `assigned_orphan` WHERE assigned_orphan.`sponser_id`=ah_sponser_info.`sponser_id`) AS sponserCount FROM `ah_sponser_info` WHERE sponsor_status = '".$status."' ".$country." AND step = '".$step."' AND delete_record = '0' AND cancel='".$cancel."' AND sponser_id  NOT IN (SELECT sponser_id FROM `assigned_orphan` WHERE is_old='0') ORDER BY sponser_id DESC;");

		//echo $this->db->last_query();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
//---------------- QUERIES FOR COUNTRIES ----------------------------
/*SELECT 
  l.`list_id`,
  l.`list_name` ,IFNULL(total,0) AS total
FROM
  ah_listmanagement AS l 
  LEFT JOIN 
    (SELECT 
      COUNT(orphan_id) AS total,
      TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age,country_id
    FROM
      `ah_yateem_info` 
    WHERE `ah_yateem_info`.delete_record = '0' 
      AND ah_yateem_info.step = '3' 
      AND ah_yateem_info.orphan_status = '1' 
      AND ah_yateem_info.orphan_id NOT IN 
      (SELECT 
        orphan_id 
      FROM
        `assigned_orphan`) 
    GROUP BY ah_yateem_info.country_id 
    HAVING age <= '18') AS lnew 
    ON lnew.country_id = list_id 
    WHERE l.`list_type` = 'issuecountry' AND l.`delete_record` = '0' AND l.`list_parent_id` = '0'
     GROUP BY l.list_id ;*/
//---------------- QUERIES FOR PROVINCES ----------------------------
/*
SELECT 
  l.`list_id`,
  l.`list_name` ,IFNULL(total,0) AS total
FROM
  ah_listmanagement AS l 
  LEFT JOIN 
    (SELECT 
      COUNT(orphan_id) AS total,
      TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age,muhafiza_id
    FROM
      `ah_yateem_info` 
    WHERE `ah_yateem_info`.delete_record = '0' 
      AND ah_yateem_info.step = '3' 
      AND ah_yateem_info.orphan_status = '1' 
      AND ah_yateem_info.orphan_id NOT IN 
      (SELECT 
        orphan_id 
      FROM
        `assigned_orphan`) 
    GROUP BY ah_yateem_info.muhafiza_id 
    HAVING age <= '18') AS lnew 
    ON lnew.muhafiza_id = list_id 
    WHERE l.`list_type` = 'issuecountry'  AND l.`delete_record` = '0' AND l.`list_parent_id` = '200'
     GROUP BY L.list_id ;
*/
//---------------- QUERIES FOR CITIES ----------------------------
/*
SELECT 
  l.`list_id`,
  l.`list_name` ,IFNULL(total,0) AS total
FROM
  ah_listmanagement AS l 
  LEFT JOIN 
    (SELECT 
      COUNT(orphan_id) AS total,
      TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age,city_id
    FROM
      `ah_yateem_info` 
    WHERE `ah_yateem_info`.delete_record = '0' 
      AND ah_yateem_info.step = '3' 
      AND ah_yateem_info.orphan_status = '1' 
      AND ah_yateem_info.orphan_id NOT IN 
      (SELECT 
        orphan_id 
    FROM
        `assigned_orphan`) 
    GROUP BY ah_yateem_info.city_id 
    HAVING age <= '18') AS lnew 
    ON lnew.city_id = list_id 
    WHERE l.`list_type` = 'issuecountry' AND l.`delete_record` = '0' AND l.`list_parent_id` = '1125'
    GROUP BY l.list_id;
*/
// NOTE: For less than 18 queries
/*$query	=	$this->db->query("SELECT *,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE step=".$step." HAVING  age	>='18' ORDER BY orphan_id DESC;"); // Greater > 18
$this->db->having('age	<=18',false); */ // Less Than < 18
//-----------------------------------------------------------------
/* End of file haya_model.php */
/* Location: ./appliction/modules/haya/haya_model.php */
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Haya extends CI_Controller {

	/*
	 * Properties
	 */
	private $_data	=	array();
	private $_login_userid	=	NULL;

//-----------------------------------------------------------------------

	/*
	 * Constructor
	 */
	function __construct()
	{
		 parent::__construct();	
	
		// Loade Admin Model
		$this->load->model('haya_model','haya');
		
		$this->_data['module']			=	$this->haya->get_module();
		
		// SET Login USER ID
		$this->_login_userid			=	$this->session->userdata('userid');

		$this->_data['login_userid']	=	$this->_login_userid;
		$this->_data['user_detail'] 	=	$this->haya->get_user_detail($this->_login_userid);
		
		// Load all types
		$this->_data['list_types']		=	$this->haya->get_listmanagment_types();
		
	}
	
//-----------------------------------------------------------------------

	/*
	*	Home Page
	*/
	public function get_sub_category()
	{
		$provinceid	=	$this->input->post('provinceid');

		// Get Willaya from province ID
		echo ($this->haya->get_sub_category($provinceid));
	}
	
//------------------------------------------------------------------------

  /**
   * Change language
   * @param $name string
   */
	 public function changeLanguage($name) 
	 {
		if ($name == 'english')
		{
				$this->session->set_userdata('lang', 'english');
				$this->session->set_userdata('lang_code', 'en');
		}
		else
		{
				$this->session->set_userdata('lang', 'arabic');
				$this->session->set_userdata('lang_code', 'ar');
		}
		
		echo '1';
	 }
//------------------------------------------------------------------------

  	/**
   	* Dynamic Forms Listing Page
   	* @param $moduleid string
   	*/
	 public function dynamic_forms_listing($moduleid) 
	 {
		$this->_data["flist"]  = $this->haya_model->get_all_custom_form($moduleid);
		
		$this->_data["userid"] = $this->_login_userid;
		
		$this->_data['formid']	=	$moduleid;
		 
		 // Load Dynamic Forms Listing 
		$this->load->view('dynamic-forms-listing',$this->_data);	 
	 }
//-----------------------------------------------------------------------
/* End of file haya.php */
/* Location: ./application/modules/haya/haya.php */
}

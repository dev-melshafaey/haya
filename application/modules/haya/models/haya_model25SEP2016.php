<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Haya_model extends CI_Model
{
	/*
	*  Properties
	*/
	private $_table_users;
	private $_table_users_documents;
	private $_table_listmanagement;
	private $_table_modules;
	private $_table_userprofile;
	private $_table_users_bankaccount;
	private $_table_users_communications;
	private $_table_users_family;
	private $_table_users_salary;
	private $_table_users_salary_detail;
	private $_table_branchs;
	private $_login_userid;
	private $_table_attendence;
	private $user_detail;
	
//-------------------------------------------------------------------

	/*
	 *  Constructor
	 */
	function __construct()
	{
		parent::__construct();

		//Get Table Names from Config 
		$this->_table_users 					= 	$this->config->item('table_users');
		$this->_table_users_documents 			= 	$this->config->item('table_users_documents');
		$this->_table_listmanagement 			= 	$this->config->item('table_listmanagement');
		$this->_table_modules 					= 	$this->config->item('table_modules');
		$this->_table_userprofile 				= 	$this->config->item('table_userprofile');
		$this->_table_users_bankaccount 		= 	$this->config->item('table_users_bankaccount');
		$this->_table_users_communications 		= 	$this->config->item('table_users_communications');
		$this->_table_users_family 				= 	$this->config->item('table_users_family');
		$this->_table_users_salary 				= 	$this->config->item('table_users_salary');
		$this->_table_users_salary_detail 		= 	$this->config->item('table_users_salary_detail');
		$this->_table_branchs 					= 	$this->config->item('table_branchs');
		$this->_table_attendence				=	$this->config->item('table_attendence');
		
		// Set Sesiion ID into Variable
		$this->session->sess_expiration = '21600';
		$this->_login_userid					=	$this->session->userdata('userid');
		
		// If SESSION is empty SET by defult Arabic
		if($this->session->userdata('lang')	==	NULL)
		{
			$this->session->set_userdata('lang', 'arabic');
			$this->session->set_userdata('lang_code', 'ar');
		}
		
        // Change language
       if($this->session->userdata('lang') == 'english' || $this->uri->segment(3) == 'en')
       {
		   // Load ENGLISH Language File
		   $this->lang->load('project', 'english');
		   
		   // Set SESSION for English File name
		   $this->session->set_userdata('lang', 'english');
       }
       else 
       {
		   // Load ENGLISH Language File
        	$this->lang->load('project', 'arabic');
       }
	   
      // $this->saveWhoisUsing();
	  if($this->_login_userid)
	  {
		  $this->user_detail 	= 	$this->get_user_detail($this->_login_userid);
	  }
	}
	
	function get_all_list_value($typeid,$stepid,$status)
	{
		$this->db->select("applicantid");
		$this->db->from("ah_applicant");
		$this->db->where("charity_type_id",$typeid);
		$this->db->where("application_status",$status);
		$this->db->where("step",$stepid);
				
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return '0';
		}
	}
	
	function get_permission_ids($parentid,$typeid,$permission)
	{
		$this->db->select('moduleid');
        $this->db->from('mh_modules');
        $this->db->where('module_parent',$parentid);
		$this->db->where('module_order',$typeid);
		$moduleid = $this->db->get()->row()->moduleid;
		return $permission[$moduleid]['v'];
	}
//-------------------------------------------------------------------
	/*
	 * 
	 */
	function get_all_old_value($typeid,$stepid)
	{
		$this->db->select("applicantid");
		$this->db->from("ah_applicant");
		$this->db->where("charity_type_id",$typeid);
		//$this->db->where("isOld","1"); Jamal told this condition only for complete step.
		$this->db->where("step >",$stepid);

		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return '0';
		}
	}	
//-------------------------------------------------------------------

	/*
	 * 
	 */	
	function saveWhoisUsing()
	{
		//$this->db->query("INSERT INTO `who_is_using` SET `userip`='".$_SERVER['REMOTE_ADDR']."', `computername`='".php_uname()."', `userdate`='".Date()."' ");
	}
	
//-------------------------------------------------------------------

	/*
	 * Get All Required Documents
	 * @param $charity_type_id_value integer
	 */	
	function allRequiredDocument($charity_type_id_value)
	{
		$this->db->select('documentid,documenttype,isrequired');
        $this->db->from('ah_document_required');
        $this->db->where('charity_type_id',$charity_type_id_value);
		$this->db->where('documentstatus',1);
        $this->db->order_by("documentorder", "ASC");
		
        $query = $this->db->get();
		return $query->result();
	}
//-------------------------------------------------------------------
	/**
	 * Valadting & Checking user session
	 * @access	public
	 * @return	Array with complete info of the user
	 */	
	public function get_user_detail($userid=0, $accountid=0)
	{
		$this->db->select('ah_users.*,`p.list_name as profession,l.level_name as level,`mh_modules`.`module_controller`,`ah_userprofile`.`fullname`,`ah_userprofile`.`manager_id`,`ah_userprofile`.`gender`,`ah_userprofile`.`email`,`ah_userprofile`.`profilepic`,`ah_branchs`.`branchname`,`ah_branchs`.`branchaddress`,`ah_listmanagement`.`list_name`,`ah_listmanagement_1`.`list_name`,`ah_userprofile`.`date_of_birth`,`ah_userprofile`.`joining_date`,`ah_userprofile`.`idcardNumber`');
		$this->db->from('ah_users');
		$this->db->join('mh_modules','mh_modules.moduleid = ah_users.landingpage','left');
		$this->db->join('ah_userprofile', 'ah_userprofile.userid = ah_users.userid','left');
		$this->db->join('ah_branchs', 'ah_branchs.branchid = ah_users.branchid','left');
		$this->db->join('ah_listmanagement', 'ah_listmanagement.list_id = ah_branchs.province','left');
		$this->db->join('ah_listmanagement AS ah_listmanagement_1', 'ah_listmanagement_1.list_id = ah_branchs.wilaya','left');
		$this->db->join('ah_listmanagement AS p', 'p.list_id = ah_userprofile.sub_profession','left');
		$this->db->join('ah_levels AS l', 'l.level_id = ah_users.level_id','left');
		$this->db->where('`ah_listmanagement`.`delete_record`','0');
		
		if($userid	!=	0 && $userid	!=	'')
		{
			$uid = $userid;
			$this->db->where('`ah_users`.`userid`',$userid);
		}
		else
		{
			$uid = $this->_login_userid;
			$this->db->where('`ah_users`.`userid`',$this->_login_userid);
		}
			$query = $this->db->get();
			$profile = $query->row();
						
			if($query->num_rows() <= 0 )
			{
				redirect(base_url().'admin/login');
			}
			
			$this->db->select("ah_users_bankaccount.`bankaccountid`,ah_users_bankaccount.`accountnumber`,ah_users_bankaccount.`accountfullname`,bank.`list_name` AS bankName, branch.`list_name` AS branchName ");
			$this->db->from("ah_users_bankaccount");
			$this->db->join('ah_listmanagement AS bank', 'ah_users_bankaccount.bankid=bank.list_id');
			$this->db->join('ah_listmanagement AS branch', 'ah_users_bankaccount.branchid=branch.list_id');
			$this->db->where('ah_users_bankaccount.userid',$profile->userid);
			
			if($accountid!=0 && $accountid!='')
			{
				$this->db->where('ah_users_bankaccount.bankaccountid',$accountid);
			}
			
			$this->db->where('ah_users_bankaccount.delete_record','0');
			$bankInfo = $this->db->get();
			
			if($bankInfo->num_rows <= 1)
			{
				$bankDetail = $bankInfo->row();
			}
			else
			{
				$bankDetail = $bankInfo->result();
			}
			
			//Extra Information
			$this->db->select("
				SUM(CASE WHEN record_type = 'EXTRA' THEN amount END ) AS extra_amount,
				SUM(CASE WHEN record_type = 'DEDUCTION' THEN amount END ) AS deducation_amount,
				SUM(CASE WHEN record_type = 'INSURANCE' THEN amount END ) AS insurance_amount,
				(SELECT basic_salary FROM ah_users_salary WHERE userid=".$profile->userid." ORDER BY salaryid DESC LIMIT 1) as basicsalary, 
				(SELECT SUM(b.amount) FROM ah_users_salary AS a, ah_users_salary_detail AS b WHERE a.`salaryid`=b.`salaryid` AND a.`userid`=".$profile->userid." ORDER BY a.`salaryid` DESC) as basic_extra,
				record_type");
				
			$this->db->from("ah_extra_things");
			$this->db->where("userid",$profile->userid);
			$this->db->order_by("extra_id","desc");
			$extra = $this->db->get();
			
			if($extra->num_rows > 0)
			{
				$extraRes 				=	$extra->row();
				$myarra['EXTRA'] 		=	$extraRes->extra_amount;
				$myarra['DEDUCTION'] 	=	$extraRes->deducation_amount;
				$myarra['INSURANCE'] 	=	$extraRes->insurance_amount;
				$myarra['BASIC'] 		=	$extraRes->basicsalary;
				$myarra['BASIC_EXTRA'] 	=	$extraRes->basic_extra;
			}
			
			$userinfo = array('_userid'=>$this->_login_userid,'profile'=>$profile,'bankinfo'=>$bankDetail,'money'=>$myarra);
			
			return $userinfo;
	}

//-------------------------------------------------------------------

	/*
	 *  Get ALL Types
	 *  return ARRAY
	 */
	function get_listmanagment_types()
	{
		$query	=	$this->db->query("SHOW COLUMNS FROM ".$this->_table_listmanagement." LIKE 'list_type'");
		$result	=	$query->row();
		
		if(!empty($result))
		{
			 return $option_array = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $result->Type));
		}
	}
	
//-------------------------------------------------------------------

	/*
	 *  Get ALL Types
	 *  return ARRAY
	 */
	function get_leave_types()
	{
		$query = $this->db->query("SHOW COLUMNS FROM ".$this->_table_attendence." LIKE 'absent_type'");
		$result = $query->row();
		
		if(!empty($result))
		{
			 return $option_array = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $result->Type));
		}
	}
	
	function multidimensional_search($parents, $searched) {
	 if (empty($searched) || empty($parents)) {
    return false;
  }

  foreach ($parents as $key => $value) {
    $exists = true;
    foreach ($searched as $skey => $svalue) {
      $exists = ($exists && isset($parents[$key][$skey]) && $parents[$key][$skey] == $svalue);
    }
    if($exists){ return $key; }
  }

  return false;
} 
	
	
	function get_sponser_document($kafeelid)
	{
		$this->db->select("ah_document_required.documenttype, ah_kafeel_documents.document_name, ah_kafeel_documents.created");
		$this->db->from("ah_document_required");
		$this->db->join("ah_kafeel_documents","ah_document_required.documentid=ah_kafeel_documents.doc_type_id");
		$this->db->where("ah_kafeel_documents.sponser_id",$kafeelid);
		$this->db->order_by("ah_document_required.documentorder","asc");
		$query = $this->db->get();
		return $query->result();
	}
//-------------------------------------------------------------------

	/*
	 *  Get ALL Types
	 *  return ARRAY
	 */	
	function get_sub_category($provinceid,$willayaid	=	NULL)
	{
		$this->db->select('list_id,list_name,list_type,list_parent_id');
		$this->db->from($this->_table_listmanagement);
		$this->db->where('delete_record','0');	
		$this->db->where('list_parent_id',$provinceid);
		$this->db->order_by("list_order", "ASC"); 
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$dropdown  = '<label>Select a Willaya:&nbsp;';
			$dropdown .= '<select name="wilayaid" id="wilayaid" placeholder="البرنامج المطلوب‎">';
			$dropdown .= '<option value="">البرنامج المطلوب‎</option>';
		
			foreach($query->result() as $row)
			{
				$dropdown .= '<option value="'.$row->list_id.'" ';
					
				if($willayaid !="")
				{
					if($willayaid	==	$row->list_id)
					{
						$dropdown .= 'selected="selected"';
					}
				}
						
						$dropdown .= '>'.$row->list_name.'</option>';
			}
				$dropdown .= '</select>';
				$dropdown .= '</label>';
				return $dropdown;
					
		}
		else
		{
			return 'No record is available;';
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Province Name
	 * Return OBJECT
	 */
		
	function get_willaya_province_name($id)
	{
		$this->db->select('list_name');
		$this->db->where('list_id',$id);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_listmanagement);
		
		if($query->num_rows() > 0)
		{
			return $query->row()->list_name;
		}

	}
	
	function get_branch_id($id)
	{
		if($id)
		{	$id = $id; }
		else
		{	$id = $this->session->userdata('userid'); }
		$this->db->select('branchid');
		$this->db->where('userid',$id);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_users);
		
		if($query->num_rows() > 0)
		{
			return $query->row()->branchid;
		}
		
	}
//-------------------------------------------------------------------

	/*
	 * 
	 */
	function get_dropbox_list_value($listtype='', $parent=0)
	{
		$this->db->select('list_id,list_name,list_type,list_other');
		$this->db->from($this->_table_listmanagement);
			
		if($parent!='' && $parent!=0)
		{	
			$this->db->where('list_parent_id',$parent);	 
		}
		else
		{
			$this->db->where('list_parent_id',0);
		}
		
		if($listtype!='' && $listtype!='0' && $listtype!='subcategory')
		{
			$this->db->where('list_type',$listtype);
		}
		
		$this->db->where('delete_record','0');
		$this->db->order_by("list_order", "ASC");
					
		$query = $this->db->get();
		
		return $query->result();
	}
//-------------------------------------------------------------------

	/*
	 * 
	 */
	function get_country_project($list_id,$listtype='', $parent=0)
	{
		$this->db->select('list_id,list_name,list_type,list_other');
		$this->db->from($this->_table_listmanagement);
		
		if($parent!='' && $parent!=0)
		{	
			$this->db->where('list_parent_id',$parent);	 
		}
		else
		{
			$this->db->where('list_parent_id',0);
		}
		
		if($listtype!='' && $listtype!='0' && $listtype!='subcategory')
		{
			$this->db->where('list_type',$listtype);
		}
		
		$this->db->where('list_id	!=',$list_id);
		
		$this->db->where('delete_record','0');
		$this->db->order_by("list_order", "ASC");
					
		$query = $this->db->get();
		
		return $query->result();
	}
//-------------------------------------------------------------------

	/*
	 * 
	 */
	function get_dropbox_list_value_muzi($listtype='', $parent=0)
	{
		$this->db->select('list_id,list_name,list_type,list_other');
		$this->db->from($this->_table_listmanagement);
			
		if($parent!='' && $parent!=0)
		{	
			$this->db->where('list_parent_id',$parent);	 
		}
		else
		{
			$this->db->where('list_parent_id',0);
		}
		
		if($listtype!='' && $listtype!='0' && $listtype!='subcategory')
		{
			$this->db->where('list_type',$listtype);
		}
		
		$this->db->where('delete_record','0');
		$this->db->order_by("list_order", "ASC");
					
		$query = $this->db->get();
		
		return $query->result();
	}	
	function get_professions($listtype='')
	{
		$this->db->select('list_id,list_name,list_type,list_other');
		$this->db->from($this->_table_listmanagement);
		
		if($listtype!='' && $listtype!='0' && $listtype!='subcategory')
		{
			$this->db->where('list_type',$listtype);
		}
		
		$this->db->where('delete_record','0');
		$this->db->order_by("list_order", "ASC");
					
		$query = $this->db->get();
		
		$all_sub_professions	=	array();
		if(!empty($query->result()))
		{
			foreach($query->result() as	$res)
			{
				$this->db->select('list_id,list_name,list_type,list_other');
				$this->db->from($this->_table_listmanagement);
				$this->db->where('list_parent_id',$res->list_id);
				$query = $this->db->get();
				
				$all_lists	=	$query->result();
				
				foreach($all_lists	as $list)
				{
					$all_sub_professions[]	=	array('list_id'=>$list->list_id,'list_name'=>$list->list_name);
				}	
			}
		}
		
		return $all_sub_professions;
	}
//-------------------------------------------------------------------

	/*
	 * 
	 */	
	function get_selected_departments($companyid)
	{
		if($companyid!='')
		{
			$this->db->select('departmentid');
			$this->db->from('ah_company_category');
			$this->db->where('companyid',$companyid);
						
			$query = $this->db->get();
			
			foreach($query->result() as $pdb)
			{
				$arr[$pdb->departmentid] = $pdb->departmentid;
			}
			
			return $arr;
		}
	}
	
//-------------------------------------------------------------------

	/*
	 * 
	 */
	function get_selected_supplier_types($companyid)
	{
		if($companyid!='')
		{
			$this->db->select('supplier_type_id');
			$this->db->from('ah_company_category');
			$this->db->where('companyid',$companyid);			
			$query = $this->db->get();
			foreach($query->result() as $pdb)
			{
				$arr[$pdb->supplier_type_id] = $pdb->supplier_type_id;
			}
			return $arr;
		}
	}
	
	
//-------------------------------------------------------------------

	/*
	 * 
	 */
	function get_name_from_list($id)
	{	
		$this->db->select('list_name');
		$this->db->where('list_id',$id);
		$this->db->where('delete_record','0');
		$this->db->from($this->_table_listmanagement);	 			
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row()->list_name;
		}
	}	
//-------------------------------------------------------------------

	/*
	 * 
	 */	
	function get_type_name($listype)
	{
		$lang_code = $this->session->userdata('lang_code');
		$type_all_list = $this->config->item('list_types');
		$list_type = $type_all_list[$listype];		
		return $list_type[$lang_code];
	}

//-------------------------------------------------------------------
	/*
	* Checking drop box for all list type 
	* Return complete dropbox with input field
	*/	
	function create_dropbox_list($name,$listtype,$selectedvalue='0',$parent='0',$isrequired='',$othervalue='',$index='',$dataassign='')
	{		
		$heading = $this->get_type_name($listtype);
		
		if($index!='')
		{
			$dropdown = '<select onChange="check_other_value(this);" name="'.$name.'[]" id="'.$name.$index.'" class="form-control '.$isrequired.'" placeholder="'.$heading.'" data-id="'.$dataassign.'">';
		}
		else
		{
			if($name	==	'list_subcategory')
			{
				$function	=	'select_item(this);';
			}
			else
			{
				$function	=	'check_other_value(this);';
			}
			
			$dropdown = '<select onChange="'.$function.'" name="'.$name.'" id="'.$name.'" class="form-control '.$isrequired.'" placeholder="'.$heading.'"  data-id="'.$dataassign.'">';
		}
		$dropdown .= '<option value="">'.$heading.'</option>';		
		foreach($this->get_dropbox_list_value($listtype,$parent) as $row)
		{
				$dropdown .= '<option dataid="'.$row->list_other.'" value="'.$row->list_id.'" ';
				if($selectedvalue==$row->list_id)
				{	$dropdown .= 'selected="selected"';	}
				$dropdown .= '>'.$row->list_name.'</option>';
		}
		$dropdown .= '</select>';
		if($index!='')
		{	$dropdown .= '<input name="'.$name.'_text[]" id="'.$name.'_text'.$index.'" value="'.$othervalue.'" placeholder="'.$heading.'" type="text" class="otherinput form-control">';	}
		else
		{	$dropdown .= '<input name="'.$name.'_text" id="'.$name.'_text" value="'.$othervalue.'" placeholder="'.$heading.'" type="text" class="otherinput form-control">';	}
		return $dropdown;
	}
	//-------------------------------------------------------------------
	/*
	* Checking drop box for all list type 
	* Return complete dropbox with input field
	*/	
	function create_dropbox_list_help($name,$listtype,$selectedvalue='0',$parent='0',$isrequired='',$othervalue='',$index='',$dataassign='')
	{		
		$heading = $this->get_type_name($listtype);
		
		if($index!='')
		{

			
			$dropdown = '<select onChange="check_other_value(this);" name="'.$name.'['.$index.']" id="'.$name.$index.'" class="form-control '.$isrequired.'" placeholder="'.$heading.'" data-id="'.$dataassign.'">';
		}
		else
		{
			if($name	==	'list_subcategory')
			{
				$function	=	'select_item(this);';
			}
			else
			{
				$function	=	'check_other_value(this);';
			}
			
			$dropdown = '<select onChange="'.$function.'" name="'.$name.'" id="'.$name.'" class="form-control '.$isrequired.'" placeholder="'.$heading.'"  data-id="'.$dataassign.'">';
		}
		$dropdown .= '<option value="">'.$heading.'</option>';		
		foreach($this->get_dropbox_list_value($listtype,$parent) as $row)
		{
				$dropdown .= '<option dataid="'.$row->list_other.'" value="'.$row->list_id.'" ';
				if($selectedvalue==$row->list_id)
				{	$dropdown .= 'selected="selected"';	}
				$dropdown .= '>'.$row->list_name.'</option>';
		}
		$dropdown .= '</select>';
		return $dropdown;
	}

//-------------------------------------------------------------------
		/*
		* Checking view permission 
		* Return permission status 0,1
		*/
		function check_view_permission($moduleid)
		{
			$this->db->select('permissionjson');
			$this->db->from('ah_users');
			$this->db->where('userid',$this->_login_userid);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				$permissionResult = $query->row();
				$permission = json_decode($permissionResult->permissionjson,TRUE);
				$perm = $permission[$moduleid];
				return $perm['v'];				
			}
			else
			{
			   return 0;
			}
		}
//-------------------------------------------------------------------
	/*
	* Creating Parent Menu 
	* Return Parent menu html 
	*/
	function parentMenu()
	{
		$this->db->select('moduleid,module_name,module_icon,module_controller,module_parent');
		$this->db->from('mh_modules');
		$this->db->where('module_parent',0); 
		$this->db->where('module_status','A');
		$this->db->where("moduleid != ",212);
		$this->db->where("moduleid != ",218);
		$this->db->where("moduleid != ",224);
		//$this->db->where("moduleid != ",217);
		$this->db->order_by("module_order", "ASC");
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{		
			$html = '<nav class="quick-launch-bar">';
			$html .= '<ul>';
			foreach($query->result() as $parentMenu)
			{				
				if($this->check_view_permission($parentMenu->moduleid)==1)
                {
					$manager_id	=	$this->user_detail['profile']->manager_id;
					
					if($parentMenu->moduleid	==	143 AND $manager_id	==	$this->_login_userid)
					{
						$html .= '<li id="Aireen'.$parentMenu->moduleid.'" class="mymainmenu" title="'.$parentMenu->module_name.'" data-url=><a href="'.base_url().$parentMenu->module_controller.'"><i class="'.$parentMenu->module_icon.' main_icons" ></i></a>';
					}
					else
					{
						$html .= '<li id="Aireen'.$parentMenu->moduleid.'" class="mymainmenu" title="'.$parentMenu->module_name.'" data-url=><a href="'.base_url().$parentMenu->module_controller.'"><i class="'.$parentMenu->module_icon.' main_icons" ></i></a>';
					}
					
					if($parentMenu->moduleid	==	143)
					{
						$this->db->select('leaveid');
						$this->db->where('approved','0');
						$query = $this->db->get('ah_leave_request');
		
						$html	.=	'<small id="hr-notifiction" >'.$query->num_rows().'</small>';
					}
					
					$html	.=	'</li>';
                }
				unset($navActive);
			}
			
			$query->free_result();
			$html .= '</ul>';
			$html .= '</nav>';
			echo $html;
		}
	}
	
	function rightMenu()
	{
		$this->db->select('moduleid,module_name,module_icon,module_controller,module_parent,module_type');
		$this->db->from('mh_modules');
		$this->db->where('show_in_right','1'); 
		$this->db->where('module_status','A');
		$this->db->where('moduleid != ',175);
		$this->db->where('moduleid != ',176);
		$this->db->order_by("module_order", "ASC");
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{		
			$html = '';
			foreach($query->result() as $parentMenu)
			{				
				if($this->check_view_permission($parentMenu->moduleid)==1)
                {
					$varresplace = str_replace('myicon','',$parentMenu->module_icon);
					if($parentMenu->module_type=='popup')
					{	$html .= '<li><a title="'.$parentMenu->module_name.'" href="#globalDiag" onClick="alatadad(this);" data-url="'.base_url().$parentMenu->module_controller.'" id="0" data-icon="'.$parentMenu->module_icon.'" data-heading="'.$parentMenu->module_name.'" data-color="#4096EE" class="add child_menu"><i class="nav-icon '.$varresplace.'"></i><span class="nav-text"></span></a></li>';	}
					else
					{	$html .= '<li><a  href="'.base_url().$parentMenu->module_controller.'"><i class="nav-icon '.$varresplace.'"></i><span class="nav-text"></span></a></li>';	}
                }
				unset($varresplace);
			}			
			$query->free_result();		
			echo $html;
		}
	}	
//-------------------------------------------------------------------
	/*
	* Creating Child Menu 
	* Return child menu html
	* Parameter parent module id 
	*/
	function childMenu($parentid,$controllername)
	{		
		$this->db->select('moduleid,module_parent,module_name,module_icon,module_controller,module_type,module_background,module_foreground');
		$this->db->from('mh_modules');
		$this->db->where('module_parent',$parentid); 
		$this->db->where('module_status','A');
		$this->db->where('show_on_menu','1');
		$this->db->order_by("module_order", "DESC");
		$childQuery = $this->db->get();	
			
		if($childQuery->num_rows() > 0)
		{	$childMenux = '';
		
			
			foreach($childQuery->result() as $childMenu)
			{
                if($this->check_view_permission($childMenu->moduleid)==1)
                {
					if($childMenu->module_type	==	'popup')
					{
						$childMenux .= '<a data-value="'.$childMenu->moduleid.'" style="background-color:'.$childMenu->module_background.' !important; color:'.$childMenu->module_foreground.' !important;" class="add ui-state-default re_design_a child_menu '.$childMenu->module_icon.'" title="'.$childMenu->module_name.'" href="#globalDiag" onClick="alatadad(this);" data-url="'.base_url().$childMenu->module_controller.'" id="0" data-icon="'.$childMenu->module_icon.'" data-heading="'.$childMenu->module_name.'" data-color="#4096EE"></a>';
					}
					else if($childMenu->module_type	==	'others')
					{
						$childMenux .= '<a data-value="'.$childMenu->moduleid.'" style="background-color:'.$childMenu->module_background.' !important; color:'.$childMenu->module_foreground.' !important;" href="'.base_url().$controllername.'/dynamic_forms_listing/'.$childMenu->moduleid.'" title="'.$childMenu->module_name.'" class="add ui-state-default re_design_a child_menu '.$childMenu->module_icon.'"></a>';
					}
					else
					{
						$childMenux .= '<a data-value="'.$childMenu->moduleid.'" style="background-color:'.$childMenu->module_background.' !important; color:'.$childMenu->module_foreground.' !important;" href="'.base_url().$childMenu->module_controller.'" title="'.$childMenu->module_name.'" class="add ui-state-default re_design_a child_menu '.$childMenu->module_icon.'"></a>';
					}				
				}
			}		
		}
		
		return $childMenux;
	}
//-------------------------------------------------------------------
	/**
	 * Get All Branches
	 *
	 * @access	public
	 * @return	object
	 */
	function get_all_branches()
	{
		$this->db->select('ah_branchs.branchid,ah_branchs.branchname,ah_branchs.branchaddress,ah_listmanagement.list_name,ahl1.list_name AS wilaya');
		$this->db->from('ah_branchs');
		$this->db->join('ah_listmanagement','ah_listmanagement.list_id = ah_branchs.province');
		$this->db->join('ah_listmanagement AS ahl1','ahl1.list_id = ah_branchs.wilaya');
		$this->db->where('ah_branchs.branchstatus','1');
		$this->db->where('ah_branchs.delete_record','0');
		$this->db->order_by("ah_branchs.branchname", "ASC");
		$q = $this->db->get();
		return $q->result();		
	}
	
	function get_branch_information($branchid)
	{
		$this->db->select('ah_branchs.branchid,ah_branchs.branchname,ah_branchs.branchaddress,ah_listmanagement.list_name,ahl1.list_name AS wilaya');
		$this->db->from('ah_branchs');
		$this->db->join('ah_listmanagement','ah_listmanagement.list_id = ah_branchs.province');
		$this->db->join('ah_listmanagement AS ahl1','ahl1.list_id = ah_branchs.wilaya');
		$this->db->where('ah_branchs.branchstatus','1');
		$this->db->where('ah_branchs.delete_record','0');
		$this->db->where('ah_branchs.branchid',$branchid);
		$this->db->order_by("ah_branchs.branchname", "ASC");
		return $this->db->get()->row();
	}
//-------------------------------------------------------------------
	/**
	 * Get Module Inforamtion
	 *
	 * @access	public
	 * @return	object
	 */
	function get_module($firstURL='',$secondURL='')
	{		
		$this->db->select('moduleid,module_parent,module_name,module_text,module_controller,module_icon');
		$this->db->from('mh_modules');
		
        if($firstURL=='')
		{
			$firstURL = $this->uri->segment(1);
		}
        
		if($secondURL=='')
		{
			$secondURL = $this->uri->segment(2);
		}
		
        if($firstURL!='' && $secondURL=='')
        {
			$this->db->where('module_controller',$firstURL);
		}
        else if($secondURL!='')
        {
			$fullURL = $firstURL.'/'.$secondURL;
            $this->db->like('module_controller',$fullURL);
		}
			
        $this->db->where('module_status','A');
		$this->db->limit(1);
		$query = $this->db->get();
		
			foreach($query->result() as $menuData)
			{
				$ppx['moduleid'] = $menuData->moduleid;
				if($menuData->module_parent==0)
				{	$ppx['module_parent'] = $menuData->moduleid;	}
				else
				{	$ppx['module_parent'] = $menuData->module_parent;	}
				$ppx['module_name'] = $menuData->module_name;
				$ppx['module_text'] = $menuData->module_text;
				$ppx['module_controller'] = $menuData->module_controller;
				$ppx['module_icon'] = $menuData->module_icon;
			}			
			$query->free_result();
			//Child Menu Results 
			$this->db->select('moduleid,module_parent,module_name,module_icon,module_controller');
			$this->db->from('mh_modules');
			$this->db->where('module_parent',$menuData->moduleid); 
			$this->db->where('module_status','A');
			$this->db->where('show_on_menu','1');
			$this->db->order_by("module_order", "ASC");
			$childQuery = $this->db->get();	
			$ppx['childMenus'] = $childQuery->result();	
			
			return $ppx;
	}
//-------------------------------------------------------------------	
	/**
	 * Get Module Inforamtion
	 *
	 * @access	public
	 * @return	object
	 */
	function get_module_name_icon($id,$getothercolumn=0)
	{
		$this->db->select('moduleid,module_name,module_icon,module_controller');
		$this->db->from('mh_modules');
		if($getothercolumn!=0)
		{	$this->db->where('module_list_matchid',$getothercolumn); }
		else
		{	$this->db->where('moduleid',$id); }
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->row();		
	}
//-------------------------------------------------------------------
	/**
	 * Checking Permission
	 *
	 * @access	public
	 * @return	object
	 */	
 	function check_permission($moduledata,$permtype='v')
    {       
        $moduleid = $moduledata['moduleid'];
				  
        $this->db->select('permissionjson');
        $this->db->from('ah_users');
        $this->db->where('userid',$this->_login_userid);
		
        $query 		=	$this->db->get();
		$userDetail =	$this->get_user_detail($this->_login_userid);
		$url_to_go 	=	base_url().$userDetail['profile']->module_controller;
				
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $sp)
            {
                $perm = json_decode($sp->permissionjson,TRUE);
				
                $newperm = $perm[$moduleid];
				
                if($newperm['v']==0 && $permtype=='v')
                {
					redirect($url_to_go);
                }
                else
                {
					return $newperm[$permtype];
				}	//It will return 0 or 1
            }
        }
        else
        {
            redirect(base_url());
        }
    }
//-------------------------------------------------------------------
	/**
	 * Checking Permission
	 *
	 * @access	public
	 * @return	object
	 */	
 	function check_permission_new($moduleid)
    {	  
        $this->db->select('permissionjson');
        $this->db->from('ah_users');
        $this->db->where('userid',$this->_login_userid);
		
        $query 		=	$this->db->get();
		$userDetail =	$this->get_user_detail($this->_login_userid);
		$url_to_go 	=	base_url().$userDetail['profile']->module_controller;
				
		if($query->num_rows() > 0)
		   {
				$sp = $query->row();
				$perm = json_decode($sp->permissionjson,TRUE);
				
				foreach($moduleid as $mid => $mvalue)
				{
					$arr[$mvalue] = $perm[$mvalue] ;
				}
				
				return $arr;            
			}
			else
			{
				return 0;
			}
    }	
//-------------------------------------------------------------------
	/**
	 * Checking Permission
	 *
	 * @access	public
	 * @return	object
	 */	

	public function all_permission_list($moduleid)
	   {
		   $userid = $this->session->userdata('userid');
		   $this->db->select('permission');
		   $this->db->from('system_permission');
		   $this->db->where('userid',$userid);
		   $query = $this->db->get();
		   if($query->num_rows() > 0)
		   {
				$sp = $query->row();
				$perm = json_decode($sp->permission,TRUE);
				
				foreach($moduleid as $mid => $mvalue)
				{
					$arr[$mvalue] = $perm[$mvalue] ;
				}
				
				return $arr;            
			}
			else
			{
				return 0;
			}
	   }
//-------------------------------------------------------------------	
	/**
	 * Checking Other Permission
	 *
	 * @access	public
	 * @return	0/1
	 */	
 	function check_other_permission($moduleid)
    {
        $this->db->select('permissionjson');
        $this->db->from('ah_users');
        $this->db->where('userid',$this->_login_userid);
        $query = $this->db->get();
		
       if($query->num_rows() > 0)
	   {
		   $sp 		=	$query->row();
		   $perm 	=	json_decode($sp->permissionjson,TRUE);
		   
		   foreach($moduleid as $mid => $mvalue)
		   {
				$arr[$mvalue] = $perm[$mvalue] ;
		   }
				
				return $arr;            
			}
			else
			{
				return 0;
			}
    }
//-------------------------------------------------------------------
	/**
	 * Checking Count from list management
	 *
	 * @access	public
	 * @return	number of count
	 */	
 	function dataCountFromList($type=0,$parent=0)
    {
        $this->db->select('list_id');
        $this->db->from('ah_listmanagement');
		if($type!=0 && $type!='')
		{	$this->db->where('list_type',$type);	}
		
		if($parent!=0 && $parent!='')
		{	$this->db->where('list_parent_id',$parent);	}
		
		$this->db->where('delete_record','0');
		
		$query = $this->db->get();
		return $query->num_rows();
    }
//-------------------------------------------------------------------
	/**
	 * Checking Count from list management
	 *
	 * @access	public
	 * @return	number of count
	 */		
	function module_dropbox_permission($name,$value)
    {
        $this->db->select('module_controller,module_name,moduleid');
        $this->db->from('mh_modules');
        $this->db->where('show_on_menu','1');
		$this->db->where('module_status','A');
		$this->db->where('module_controller !=','#');
        $this->db->order_by("module_parent", "ASC");
		if($value!='')
		{	$valuex = $value; }
		else
		{	$valuex = 8; }
            $query = $this->db->get();
            $dropdown = '<select  name="'.$name.'" id="'.$name.'" class="form-control req" placeholder="صفحة الترحيب" >';
           
            foreach($query->result() as $row)
            {
                $dropdown .= '<option value="'.$row->moduleid.'" ';
                if($valuex==$row->moduleid)
                {
                    $dropdown .= 'selected="selected"';
                }
                $dropdown .= '>'.$row->module_name.'</option>';
            }
            $dropdown .= '</select>';
            echo($dropdown);
      }
//-------------------------------------------------------------------
	/**
	 * Checking Count from list management
	 *
	 * @access	public
	 * @return	number of count
	 */	
	 function module_dropbox($name,$value)
		{
			$this->db->select('moduleid,module_name');
			$this->db->from('mh_modules');
			$this->db->where('moduleid	!=','8');
			$this->db->where('module_parent','0');
			$this->db->order_by("module_order", "ASC");
			
			$query 		= 	$this->db->get();
			$dropdown	= 	'<select onchange="set_type(this);" name="'.$name.'" id="'.$name.'" class="form-control req" placeholder="اختر القسم" >';
			$dropdown	.= 	'<option value="0">اختر القسم</option>';
				
				foreach($query->result() as $row)
				{
					$dropdown .= '<option value="'.$row->moduleid.'" ';
					
					if($value==$row->moduleid)
					{
						$dropdown .= 'selected="selected"';
					}
					
					$dropdown .= '>'.$row->module_name.'</option>';
				}
				
				$dropdown .= '</select>';
				echo($dropdown);
			
		}
//-------------------------------------------------------------------
	/**
	 * Activity Monitor of user
	 *
	 * @access	public
	 * @return	void
	 */	
	function activity_monitor($userid,$logtype)
	{
		$logArray = array('userid'=>$userid,'acttype'=>$logtype,'logip'=>$_SERVER['REMOTE_ADDR'],'logagent'=>$_SERVER['HTTP_USER_AGENT']);
		$this->db->insert($this->config->item('table_ahul'),$logArray);		
	}
//-------------------------------------------------------------------
	/**
	 * Module List
	 *
	 * @access	public
	 * @return	void
	 */	
	function allModule($parentid=0)
    {
        $this->db->select('moduleid,module_name,module_icon');
        $this->db->from('mh_modules');
		
        if($parentid!='' && $parentid!='0')
		{
			$this->db->where('module_parent',$parentid);
			
		}
		else
		{
			$this->db->where('module_parent',0);
        	
		}
		$this->db->where('module_status','A');
		$this->db->order_by("module_order", "ASC");
		$query = $this->db->get();
			
		return $query->result();
    }
//-------------------------------------------------------------------	
	/**
	 * Uploading Photos
	 *
	 * @access	public
	 * @return	photo name
	 */	
	function upload_file($filefield,$folderpath,$thumb=false,$w=0,$h=0)
	{	
		$folderpath	=	'./'.$folderpath;
			
		if (!is_dir($folderpath))
		{
			mkdir($folderpath, 0777, true);
		}
		
		$config['upload_path']		=	$folderpath;
		$config['allowed_types']	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload($filefield))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
//-------------------------------------------------------------------
	/**
	 * Checking Count from tables
	 *
	 * @access	public
	 * @return	number of count
	 */	
 	function dataCount($table,$column,$value,$type,$sumcolumn=0)
    {
        switch($type)
		{
			case 'SUM';
				$SUMQuery = $this->db->query("SELECT SUM(".$sumcolumn.") as sum FROM `".$table."` WHERE `".$column."`='".$value."'");
				$SUMResult = $SUMQuery->row();
				return $SUMResult->sum;
			break;
			
			case 'COUNT';
				$SUMQuery = $this->db->query("SELECT ".$column." FROM `".$table."` WHERE `".$column."`='".$value."'");				
				return $SUMQuery->num_rows;
			break;
		}	
    }
//----------------------------------------------------------------------
	/*
	* Insert Banner Record
	* @param array $data
	* return True
	*/
	function add_banner($data)
	{
		$this->db->insert('system_images',$data);

		return TRUE;
	}
//-------------------------------------------------------------------

	/*
	 * 
	 */
	public function get_banner_detail($image_id)
	{
		$this->db->where('imageid',$image_id);
		
		$query = $this->db->get('system_images');
		
		// Check if Result is Greater Than Zero
		if($query->num_rows() > 0)
		{
			return $query->row();
		}	
	}
//-------------------------------------------------------------------

	/*
	 * 
	 */	
	public function update_status($image_id,$data)
	{
		$json_data	=	json_encode(array('record'=>'delete','imageid'	=>	$image_id));
		
		$data	=	array('delete_record'=>'1');
		
		$this->db->where('imageid', $image_id);

		$this->db->update('system_images',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------------

	/*
	 * 
	 */
	public function update_banner($image_id,$data)
	{
		$json_data	=	json_encode(array('data'=>$data));
		
		$this->db->where('imageid',$image_id);
		$this->db->update('system_images',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
	
	public function get_multi_select_box($name,$case,$type='')
	{
		switch($case)
		{	
			case 'Branch';				
				$query = $this->db->query("SELECT branchid,branchname FROM ah_branchs ORDER by branchname ASC;");
				foreach($query->result() as $nurn)
				{
					$html .= '<li class="checklist"><input type="checkbox" name="'.$name.'[]" value="'.$nurn->branchid.'"> '.$nurn->branchname.'</li>';
				}
				return array('c'=>arabic_date($query->num_rows()),'h'=>$html);
			break;
			case 'FirstList';
				$query = $this->db->query("SELECT list_id, list_name FROM ah_listmanagement WHERE list_type='".$type."' AND list_status='1' AND delete_record='0' ORDER by list_order ASC;");
				foreach($query->result() as $nurn)
				{
					$html .= '<li class="checklist"><input type="checkbox" name="'.$name.'[]" value="'.$nurn->list_id.'"> '.$nurn->list_name.'</li>';
				}
				return array('c'=>arabic_date($query->num_rows()),'h'=>$html);
			break;
			case 'SMSTemplate';
				$query = $this->db->query("SELECT templatesubject, template FROM system_sms_template WHERE templatestatus='1' AND delete_record='0' ORDER by templatesubject ASC;");
				foreach($query->result() as $nurn)
				{
					$html .= '<li class="checklist" onClick="setTemplateInBulk(\''.$nurn->template.'\');">'.$nurn->templatesubject.'</li>';
				}
				return array('c'=>arabic_date($query->num_rows()),'h'=>$html);
			break;
		}
	}
	
//-------------------------------------------------------------------
	/*
	 * Get All Bank Accounts Detail
	 * Return OBJECT
	 */
		
	function get_accnumber_by_id($accountid)
	{
		$this->db->select('accountnumber');
		$this->db->where('bankaccountid',$accountid);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_users_bankaccount);
		
		if($query->num_rows() > 0)
		{
			return $query->row()->accountnumber;
		}

	}
//-------------------------------------------------------------------
	/*
	 * نتيجه البحث الاجتماعي
	 * Return OBJECT
	 */
		
	function module_button($moduleid,$applicantid,$savebuttonName,$caseStatus)
	{

		$html = '';
		
		if($caseStatus==0)
		{
			$html .= '<button type="button" id="'.$savebuttonName.'" class="btn btn-success"><i class="icon-hdd"></i> حفظ</button>  ';
		}
		if($applicantid!='' && $this->check_other_permission($moduleid,'v')==1)
		{
			$this->db->select('module_controller,module_name,module_icon');
			$this->db->where('moduleid',$moduleid);		
			$query = $this->db->get('mh_modules');
			if($query->num_rows() > 0)
			{
				$data = $query->row();
				$html .= '<button type="button" onClick="gototype(this);" data-url="'.base_url().$data->module_controller.'/'.$applicantid.'"  class="btn btn-warning"><i class="'.$data->module_icon.'"></i> '.$data->module_name.'</button>  ';
			}
		}
		
		return $html;

	}
//-------------------------------------------------------------------

	/*
	 *  Get ALL Types
	 *  return ARRAY
	 */
	function absent_types()
	{
		$query = $this->db->query("SHOW COLUMNS FROM ".$this->_table_attendence." LIKE 'absent_type'");
		$result = $query->row();
		
		if(!empty($result))
		{
			 return $type_array = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $result->Type));
		}
	}
//-------------------------------------------------------------------
	/*
	* Get all Users
	*
	*/
	public function get_all_users()
	{
		$this->db->select('
			'.$this->_table_users.'.userid,
			'.$this->_table_userprofile.'.fullname,
			'.$this->_table_userprofile.'.fathername
			');
        $this->db->from($this->_table_users);
		$this->db->join($this->_table_userprofile,$this->_table_userprofile.'.userid='.$this->_table_users.'.userid');
        $this->db->where($this->_table_users.'.userstatus',1);
		$this->db->where($this->_table_users.'.delete_record','0');
        $this->db->order_by($this->_table_userprofile.".fullname", "ASC");
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			 return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	*
	*
	*/
	public function getRequestInfo($applicant_id='')
	{
		if($applicant_id!='')
		{
			$this->db->select('*');
			$this->db->from('ah_applicant');			
			$this->db->where('applicantid',$applicant_id);
			$ah_applicant = $this->db->get();
			$arr['ah_applicant'] = $ah_applicant->row();
				
				$this->db->select('*');
				$this->db->from('ah_applicant_wife');			
				$this->db->where('applicantid',$applicant_id);
				$ah_applicant_wife = $this->db->get();
				foreach($ah_applicant_wife->result() as $wife)
				{
					$arr['ah_applicant_wife'][] = $wife;
				}
								
				
				$this->db->select('*');
				$this->db->from('ah_applicant_survayresult');			
				$this->db->where('applicantid',$applicant_id);
				$ah_applicant_survayresult = $this->db->get();
				$arr['ah_applicant_survayresult'] = $ah_applicant_survayresult->row();
				
				$this->db->select('*');
				$this->db->from('ah_applicant_relation');			
				$this->db->where('applicantid',$applicant_id);
				$ah_applicant_relation = $this->db->get();
				$arr['ah_applicant_relation'] = $ah_applicant_relation->result();	

				$this->db->select('*');
				$this->db->from('ah_applicant_family');			
				$this->db->where('applicantid',$applicant_id);
				$ah_applicant_family = $this->db->get();
				$arr['ah_applicant_family'] = $ah_applicant_family->result();
				
				$this->db->select('*');
				$this->db->from('ah_applicant_economic_situation');			
				$this->db->where('applicantid',$applicant_id);
				$ah_applicant_economic_situation = $this->db->get();
				$arr['ah_applicant_economic_situation'] = $ah_applicant_economic_situation->result();

				$this->db->select('*');
				$this->db->from('ah_applicant_documents');			
				$this->db->where('applicantid',$applicant_id);
				$ah_applicant_documents = $this->db->get();
				foreach($ah_applicant_documents->result() as $doc)
				{
					$arr['ah_applicant_documents'][$doc->documentid] = $doc;
				}
				
				
				$this->db->select('
					ah_applicant_decission.decissionid,
					ah_applicant_decission.applicantid,
					ah_applicant_decission.userid,
					ah_applicant_decission.decission,
					ah_applicant_decission.notes,
					ah_applicant_decission.decissionip,
					ah_applicant_decission.decissiontime,
					ah_applicant_decission.meeting_number,
					ah_applicant_decission.decission_amount,
					ah_applicant_decission.decission_doc,
					ah_applicant_decission.meeting_notes,
					ah_userprofile.fullname');
       			$this->db->from('ah_applicant_decission');
				$this->db->join('ah_userprofile','ah_userprofile.userid=ah_applicant_decission.userid');
				$this->db->where('ah_applicant_decission.applicantid',$applicant_id);
				$this->db->order_by("ah_applicant_decission.decissiontime", "DESC");
				$ah_applicant_decission = $this->db->get();
				$arr['ah_applicant_decission'] = $ah_applicant_decission->result();
				
				/****Notes Query*****/
				$this->db->select("ah_applicant_notes.notesdate, ah_applicant_notes.notes, ah_userprofile.fullname, ah_listmanagement.list_name");
				$this->db->from("ah_applicant_notes");
				$this->db->join("ah_userprofile","ah_applicant_notes.userid=ah_userprofile.userid");
				$this->db->join("ah_listmanagement","ah_applicant_notes.branchid=ah_listmanagement.list_id");
				$this->db->where("ah_applicant_notes.applicantid",$applicant_id);
				$my_notes = $this->db->get();
				$arr['notes'] = $my_notes->result();
				
				
				
				return $arr;						
											
		}
	}
	
//-------------------------------------------------------------------
	/*
	*
	*
	*/
	public function getQuote($quoteid)
	{
		$this->db->select('*');
		$this->db->from('ah_quote');			
		$this->db->where('quoteid',$quoteid);
		$ah_quote = $this->db->get();
		return $ah_quote->row();
	}
//-------------------------------------------------------------------
	/**
	 * Global Filter
	 *
	 * @access	public
	 * @return	object
	 */
/*-------------------------------------------------------------------------*/
	public function global_filter($gr)
	{	
		$branchlist 	=	$this->get_all_branches();
		$charity_type 	=	array();
		$urlForAct 		=	base_url().$this->uri->segment('1').'/'.$this->uri->segment('2').'/';
		$userid 		= $this->session->userdata('userid');
		$currentBranch 	= $this->get_branch_id($userid);
		
		switch($gr['type'])
		{			
			case 'help';	
				$gCount = $this->get_all_count_table('ah_applicant',array('isdelete'=>'0'));			
				$html 	= '<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
				$html  .= '<li class="haya_branch_list"><a href="'.base_url().'inquiries/transactions">عرض الكل<span class="badge '.badge_color($gCount).'" style="margin-right: 3px;">'.$gCount.'</span> </a></li>';
			    foreach($branchlist as $branch) 
				{ 
					$bCount = $this->get_all_count_table('ah_applicant',array('isdelete'=>'0','branchid'=>$branch->branchid));
					if($currentBranch==$branch->branchid)
					{	$html  .= '<li id="branch_'. $branch->branchid.'" class="haya_branch_list"><a data-id="'.$branch->branchid.'" data-count="'.$bCount.'" href="'.base_url().'inquiries/transactions/'.$branch->branchid.'/'.$gr['c'].'">'.$branch->branchname.'<span class="badge '.badge_color(1).'" style="margin-right: 3px;">'.$bCount.'</span> </a></li>';	}
					else
					{	$html  .= '<li class="haya_branch_list">'.$branch->branchname.'<span class="badge '.badge_color(0).'" style="margin-right: 3px;">'.$bCount.'</span> </li>';	}
						unset($bCount);
				}
				
				$html .= '</div>';
				$html .= '<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
				
				foreach($this->get_dropbox_list_value('charity_type') as $ctype)
				{
					if($ctype->list_id	==	78 || $ctype->list_id	==	79 || $ctype->list_id	==	80 || $ctype->list_id	==	81)
					{
						$module 		= $this->get_module_name_icon(0,$ctype->list_id);
						$charityCount 	= $this->get_all_count_table('ah_applicant',array('isdelete'=>'0','charity_type_id'=>$ctype->list_id,'branchid'=>$currentBranch));
						$html 		   .= '<li id="charity_'.$ctype->list_id.'" class="haya_chair_list '.$module->module_icon.' "><a style="margin-right: 5px; font-size:12px;" data-id="'.$ctype->list_id.'" data-count="'.$charityCount.'" href="'.base_url().'inquiries/transactions/'.$gr['b'].'/'.$ctype->list_id.'">'.$module->module_name.'<span class="badge '.badge_color($charityCount).'" style="margin-right: 3px;">'.$charityCount.'</span> </a></li>';
					}
				}
				$html .= '</div>';
				return $html;
			break;
			case 'wilaya';	
				
				if($gr['c'] == 'yateem')
				{
					$gCount = $this->get_all_count_table('ah_yateem_info',array('delete_record'=>'0'));		
					/*$html 	= '<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
					$html  .= '<li class="haya_branch_list"><a href="'.base_url().'yateem/yateem_list">عرض الكل<span class="badge '.badge_color($gCount).'" style="margin-right: 3px;">'.$gCount.'</span> </a></li>';
					
					foreach($branchlist as $branch) 
					{
						$bCount = $this->get_all_count_table('ah_yateem_info',array('delete_record'=>'0','user_branch_id'=>$branch->branchid));
						$html  .= '<li id="branch_'. $branch->branchid.'" class="haya_branch_list"><a data-id="'.$branch->branchid.'" data-count="'.$bCount.'" href="'.base_url().'yateem/yateem_list/'.$branch->branchid.'/branch'.$gr['c'].'">'.$branch->branchname.'<span class="badge '.badge_color($bCount).'" style="margin-right: 3px;">'.$bCount.'</span> </a></li>';
						unset($bCount);
					}
					
					$html .= '</div> <br />';*/
					
					$y_count 	 =	$this->get_assignedyateeem_list_status($age	=	NULL,NULL);
					$gCount		 =	$this->get_assignedyateeem_list_status($age	=	NULL,'YES');
					$gCount2 	 =	$this->get_assignedyateeem_list_status($age	=	NULL,'NOT');
					//$gCount3	 =	$this->get_assignedyateeem_list_status($age	=	'LESS','YES');
					$gCount4 	 =	$this->get_assignedyateeem_list_status($age	=	'GREATER',NULL);
					
					$html 		.=	'<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
					$html 		.=	'<li class="haya_branch_list"><a class="btn btn-warning" style="margin-right:2px;" href="'.base_url().'yateem/yateem_list"> الأيتــــام('.$y_count.') </a></li>';
					$html 		.=	'<li class="haya_branch_list"><a class="btn btn-warning" style="margin-right:2px;" href="'.base_url().'yateem/yateem_list/0/0/YES">تفویض یتیم ('.$gCount.') </a></li>';
					$html 		.=	'<li class="haya_branch_list"><a class="btn btn-warning" style="margin-right:2px;" href="'.base_url().'yateem/yateem_list/0/0/NOT">لم يتم تعيين يتيم ('.$gCount2.') </a></li>';
					$html 		.=	'<li class="haya_branch_list"><a class="btn btn-warning" style="margin-right:2px;" href="'.base_url().'yateem/yateem_list/0/0/GREATER">أكبر من أو يساوي ١٨ ('.$gCount4.') </a></li>';
					//$html 		.=	'<li class="haya_branch_list"><a class="btn btn-warning" style="margin-right:2px;" href="'.base_url().'yateem/yateem_list/0/0/NOT">أقل من 18 ('.$gCount4.') </a></li>';
					$html 		.=	'</div>';
				}
				else
				{
					/*$gCount = $this->get_all_count_table('ah_sponser_info',array('delete_record'=>'0'));			
					$html 	= '<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
					$html  .= '<li class="haya_branch_list"><a href="'.base_url().'yateem/kafeel_list">عرض الكل<span class="badge '.badge_color($gCount).'" style="margin-right: 3px;">'.$gCount.'</span> </a></li>';
					
					foreach($branchlist as $branch) 
					{
						$bCount = $this->get_all_count_table('ah_sponser_info',array('delete_record'=>'0','user_branch_id'=>$branch->branchid));
						$html  .= '<li id="branch_'. $branch->branchid.'" class="haya_branch_list"><a data-id="'.$branch->branchid.'" data-count="'.$bCount.'" href="'.base_url().'yateem/yateem_list/'.$branch->branchid.'/branch'.$gr['c'].'">'.$branch->branchname.'<span class="badge '.badge_color($bCount).'" style="margin-right: 3px;">'.$bCount.'</span> </a></li>';
						unset($bCount);
					}
					
					$html .= '</div>';*/
					
					$gCount 	 =	$this->get_assignedsponser_list_status('YES');
					$gCount2 	 =	$this->get_assignedsponser_list_status('NOT');
					$html 		.= '<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
					$html 		.= '<li class="haya_branch_list"><a <a class="btn btn-warning" style="margin-right:2px;" href="'.base_url().'yateem/kafeel_list/YES">تعيين كفيل ('.$gCount.') </a></li>';
					$html 		.= '<li class="haya_branch_list"><a class="btn btn-warning" style="margin-right:2px;" href="'.base_url().'yateem/kafeel_list/NOT">لم يتم تعيين كفيل ('.$gCount2.') </a></li>';
					$html 		.= '</div>';
				
				}
				
				return $html;
			break;
			case 'socialservay';	
				$gCount = $this->get_all_count_table('ah_applicant',array('isdelete'=>'0'));			
				$html 	= '<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
				$html  .= '<li class="haya_branch_list"><a href="'.base_url().'inquiries/socilservaylist">عرض الكل<span class="badge '.badge_color($gCount).'" style="margin-right: 3px;">'.$gCount.'</span> </a></li>';
			    foreach($branchlist as $branch) 
				{ 
					$bCount = $this->get_all_count_table('ah_applicant',array('isdelete'=>'0','branchid'=>$branch->branchid));
					$html  .= '<li id="branch_'. $branch->branchid.'" class="haya_branch_list"><a data-id="'.$branch->branchid.'" data-count="'.$bCount.'" href="'.base_url().'inquiries/socilservaylist/'.$branch->branchid.'/'.$gr['c'].'">'.$branch->branchname.'<span class="badge '.badge_color($bCount).'" style="margin-right: 3px;">'.$bCount.'</span> </a></li>';
			 		unset($bCount);
				}
				$html .= '</div>';
				$html .= '<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
				foreach($this->get_dropbox_list_value('charity_type') as $ctype)
				{
					if($ctype->list_id==78 || $ctype->list_id==79 || $ctype->list_id==80 || $ctype->list_id==81)
					{
						$module 		 =	$this->get_module_name_icon(0,$ctype->list_id);
						$charityCount 	 =	$this->get_all_count_table('ah_applicant',array('isdelete'=>'0','charity_type_id'=>$ctype->list_id));
						$html 			.=	'<li id="charity_'.$ctype->list_id.'" class="haya_chair_list '.$module->module_icon.' "><a style="margin-right: 5px; font-size:12px;" data-id="'.$ctype->list_id.'" data-count="'.$charityCount.'" href="'.base_url().'inquiries/socilservaylist/'.$gr['b'].'/'.$ctype->list_id.'">'.$module->module_name.'<span class="badge '.badge_color($charityCount).'" style="margin-right: 3px;">'.$charityCount.'</span> </a></li>';
					}
				}
				
				$html .= '</div>';
				return $html;
			break;
            case 'finaldecission';	
				$gCount = $this->get_all_count_table('ah_applicant',array('isdelete'=>'0','application_status != '=>'رفض'));			
				$html 	= '<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
				$html  .= '<li class="haya_branch_list"><a href="'.base_url().'inquiries/socilservaylist">عرض الكل<span class="badge '.badge_color($gCount).'" style="margin-right: 3px;">'.$gCount.'</span> </a></li>';
			    
				foreach($branchlist as $branch) 
				{ 
					$bCount = $this->get_all_count_table('ah_applicant',array('isdelete'=>'0','branchid'=>$branch->branchid,'application_status != '=>'رفض'));
					$html  .= '<li id="branch_'. $branch->branchid.'" class="haya_branch_list"><a data-id="'.$branch->branchid.'" data-count="'.$bCount.'" href="'.base_url().'inquiries/socilservaylist/'.$branch->branchid.'/'.$gr['c'].'">'.$branch->branchname.'<span class="badge '.badge_color($bCount).'" style="margin-right: 3px;">'.$bCount.'</span> </a></li>';
			 		unset($bCount);
				}
				$html .= '</div>';
				$html .= '<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
				foreach($this->get_dropbox_list_value('charity_type') as $ctype)
				{
					if($ctype->list_id	==	78 || $ctype->list_id	==	79 || $ctype->list_id	==	80 || $ctype->list_id	==	81)
					{
						$module 		 =	$this->get_module_name_icon(0,$ctype->list_id);
						$charityCount 	 =	$this->get_all_count_table('ah_applicant',array('isdelete'=>'0','application_status != '=>'رفض','charity_type_id'=>$ctype->list_id));
						$html 			.=	'<li id="charity_'.$ctype->list_id.'" class="haya_chair_list '.$module->module_icon.' "><a style="margin-right: 5px; font-size:12px;" data-id="'.$ctype->list_id.'" data-count="'.$charityCount.'" href="'.base_url().'inquiries/socilservaylist/'.$gr['b'].'/'.$ctype->list_id.'">'.$module->module_name.'<span class="badge '.badge_color($charityCount).'" style="margin-right: 3px;">'.$charityCount.'</span> </a></li>';
					}
				}
				
				$html .= '</div>';
				return $html;
			break;
			case 'sslist';	
				$gCount = $this->socialservayCount($branchid,$typeid);			
				$html 	= '<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
				$html  .= '<li class="haya_branch_list"><a href="'.base_url().'inquiries/socilservayresultfilledlist">عرض الكل<span class="badge '.badge_color($gCount).'" style="margin-right: 3px;">'.$gCount.'</span> </a></li>';
			    foreach($branchlist as $branch) 
				{ 
					$bCount = $this->socialservayCount($branch->branchid);
					$this->get_all_count_table('ah_applicant',array('isdelete'=>'0','branchid'=>$branch->branchid));
					$html .= '<li id="branch_'. $branch->branchid.'" class="haya_branch_list"><a data-id="'.$branch->branchid.'" data-count="'.$bCount.'" href="'.base_url().'inquiries/socilservayresultfilledlist/'.$branch->branchid.'/'.$gr['c'].'">'.$branch->branchname.'<span class="badge '.badge_color($bCount).'" style="margin-right: 3px;">'.$bCount.'</span> </a></li>';
			 		unset($bCount);
				}
				
				$html .= '</div>';
				$html .= '<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
				foreach($this->get_dropbox_list_value('charity_type') as $ctype)
				{
					if($ctype->list_id	==	78 || $ctype->list_id	==	79 || $ctype->list_id	==	80 || $ctype->list_id	==	81) {
					$module = $this->get_module_name_icon(0,$ctype->list_id);
					$charityCount = $bCount = $this->socialservayCount($branch->branchid,$ctype->list_id);
					$html .= '<li id="charity_'.$ctype->list_id.'" class="haya_chair_list '.$module->module_icon.' "><a style="margin-right: 5px; font-size:12px;" data-id="'.$ctype->list_id.'" data-count="'.$charityCount.'" href="'.base_url().'inquiries/socilservayresultfilledlist/'.$gr['b'].'/'.$ctype->list_id.'">'.$module->module_name.'<span class="badge '.badge_color($charityCount).'" style="margin-right: 3px;">'.$charityCount.'</span> </a></li>';
					}
				}
				
				$html .= '</div>';
				return $html;
			break;
            case 'rejectlist';	
				$gCount = $this->getRejectCount();			
				$html 	= '<div class="col-md-12" style="padding: 6px 0px; border-bottom: 1px solid #ddd;">';
				$html  .= '<li class="haya_branch_list"><a href="'.$urlForAct.'">عرض الكل<span class="badge '.badge_color($gCount).'" style="margin-right: 3px;">'.$gCount.'</span> </a></li>';
			    
                foreach($branchlist as $branch) 
				{ 
				    
					$bCount = $this->getRejectCount($branch->branchid);
					$html  .= '<li id="branch_'. $branch->branchid.'" class="haya_branch_list"><a data-id="'.$branch->branchid.'" data-count="'.$bCount.'" href="'.$urlForAct.$branch->branchid.'">'.$branch->branchname.'<span class="badge '.badge_color($bCount).'" style="margin-right: 3px;">'.$bCount.'</span> </a></li>';
			 		unset($bCount);
				}
				$html .= '</div>';
				$html .= '<div class="col-md-12" style="padding: 6px 0px; border-bottom: 1px solid #ddd;">';
				
				foreach($this->get_dropbox_list_value('charity_type') as $ctype)
				{
					if($ctype->list_id	==	78 || $ctype->list_id	==	79 || $ctype->list_id	==	80 || $ctype->list_id	==	81) 
					{
						$module 		= $this->get_module_name_icon(0,$ctype->list_id);
						$charityCount 	= $this->getRejectCount(0,$ctype->list_id);
						$html 		   .= '<li id="charity_'.$ctype->list_id.'" class="haya_chair_list '.$module->module_icon.' "><a style="margin-right: 5px; font-size:12px;" data-id="'.$ctype->list_id.'" data-count="'.$charityCount.'" href="'.$urlForAct.$gr['b'].'/'.$ctype->list_id.'">'.$module->module_name.'<span class="badge '.badge_color($charityCount).'" style="margin-right: 3px;">'.$charityCount.'</span> </a></li>';
					}
				}
				$html .= '</div>';
				return $html;
			break;		
		}
	}
//-------------------------------------------------------------------
	/*
	*
	*
	*/    
    public function getRejectCount($branchid=0,$typeid=0)
    {
        $this->db->select('applicantid');
        $this->db->from('ah_applicant');
        $this->db->where('isdelete','0');
        $this->db->where('application_status','رفض');
		
        if($branchid!='' && $branchid!=0)
        {
            $this->db->where('branchid',$branchid);
        }

        if($typeid!='' && $typeid!=0)
        {
            $this->db->where('charity_type_id',$typeid);
        } 
		       
        $query = $this->db->get();
        return $query->num_rows();
    }
//-------------------------------------------------------------------
	/*
	* Assigned/NOT Assigned Yateem Total Counts
	* @param $status string
	* return integer
	*/	
	function get_assignedyateeem_list_status($age,$status)
	{
		if($age	==	NULL AND $status	==	'NOT')
		{
			$query	=	$this->db->query("SELECT * FROM `ah_yateem_info` WHERE orphan_id  NOT IN (SELECT orphan_id FROM `assigned_orphan`) AND step='2' ORDER BY orphan_id DESC;");
		}
		elseif($age	==	NULL AND $status	==	'YES')
		{
			$query	=	$this->db->query("SELECT * FROM `ah_yateem_info` WHERE orphan_id IN (SELECT orphan_id FROM `assigned_orphan`) AND step='2' ORDER BY orphan_id DESC;");
		}
		elseif($age	==	'LESS')
		{
			$query	=	$this->db->query("SELECT * FROM `ah_yateem_info` WHERE orphan_id IN (SELECT orphan_id FROM `assigned_orphan`) AND step='2' AND orphan_age	<='18' ORDER BY orphan_id DESC;");
		}
		elseif($age	==	'GREATER')
		{
			$query	=	$this->db->query("SELECT *,TIMESTAMPDIFF(YEAR,date_birth,CURDATE()) AS age FROM `ah_yateem_info` WHERE step='2' HAVING  age	>='18' ORDER BY orphan_id DESC;");
		}
		else
		{
			$query	=	$this->db->query("SELECT * FROM `ah_yateem_info` WHERE step='2' ORDER BY orphan_id DESC;");
		}
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return	'0';
		}
	}
//-------------------------------------------------------------------
	/*
	*
	*
	*/	
	function get_assignedsponser_list_status($status)
	{

		if($status	==	'NOT')
		{
			$query	=	$this->db->query("SELECT sponser_id FROM `ah_sponser_info` WHERE delete_record = '0' AND sponser_id  NOT IN (SELECT sponser_id FROM `assigned_orphan`) ORDER BY sponser_id DESC;");
		}
		elseif($status	==	'YES')
		{
			$query	=	$this->db->query("SELECT sponser_id FROM `ah_sponser_info` WHERE delete_record = '0' AND sponser_id IN (SELECT sponser_id FROM `assigned_orphan`) ORDER BY sponser_id DESC;");
		}
		else
		{
			$query	=	$this->db->query("SELECT sponser_id FROM `ah_sponser_info` WHERE delete_record = '0' ORDER BY sponser_id DESC;");
		}
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
	}
//-------------------------------------------------------------------
	/*
	* 
	* 
	*/	
	public function socialservayCount($branchid=0,$typeid=0)
	{
		$this->db->select('ah_applicant.applicantid,ah_branchs.branchname,ah_applicant.charity_type_id,ah_applicant.applicantcode,ah_applicant.fullname,ah_applicant.registrationdate,ah_applicant.idcard_number,cti.list_name AS charity_type ,pro.list_name AS province ,wil.list_name AS wilaya');
		$this->db->from('ah_applicant');			
		$this->db->join('ah_listmanagement AS cti','cti.list_id = ah_applicant.charity_type_id');
		$this->db->join('ah_listmanagement AS pro','pro.list_id = ah_applicant.province');
		$this->db->join('ah_listmanagement AS wil','wil.list_id = ah_applicant.wilaya');
		$this->db->join('ah_branchs','ah_branchs.branchid = ah_applicant.branchid');
		$this->db->join('ah_applicant_survayresult','ah_applicant_survayresult.applicantid = ah_applicant.applicantid');
		
		if($branchid	!=	0)
		{
			$this->db->where("ah_applicant.branchid",$branchid);
		}
		
		if($charity_type	!=	0)
		{
			$this->db->where("ah_applicant.charity_type_id",$charity_type);
		}	
		
		$this->db->where("ah_applicant.isdelete",'0');
		$this->db->order_by('ah_applicant.registrationdate','DESC');
		$query = $this->db->get();
		
		return $query->num_rows();
	}
//-------------------------------------------------------------------
	/**
	 * Get All Count from table
	 * @access	public
	 * @return	object
	 */
/*-------------------------------------------------------------------------*/
	public function get_all_count_table($table,$where=array())
	{
		foreach($where as $wkey => $wvalue)
		{
			$this->db->where($wkey,$wvalue);
		}
		
		$this->db->from($table);
		
		return $this->db->count_all_results();
	}
//-------------------------------------------------------------------
	/**
	 * Get All Count from table
	 *
	 * @access	public
	 * @return	object
	 */
/*-------------------------------------------------------------------------*/
	public function save_steps($applicantid,$stepid)
	{
		$data = array('applicantid'=>$applicantid,'step_userid'=>$this->session->userdata('userid'),'step_number'=>$stepid);
		$this->db->insert('ah_applicant_steps',$data,json_encode($data),$this->_login_userid);
	}
/*---------------- -------------------------------------- -------------------*/
	/*
	*
	*
	*/
	public function update_steps($applicantid,$stepid)
	{
		$data = array('step'=>$stepid);

		$this->db->where('applicantid', $applicantid);
		$this->db->update('ah_applicant',json_encode($data),$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
/*-------------------------------------------------------------------------*/
	/*
	*
	*
	*/
	public function get_vacations()
	{
		$this->db->select('settingtitle,settingvalue');
		$this->db->where('settingsid','2');
		$this->db->or_where('settingsid','3');
		$this->db->or_where('settingsid','4');
		$this->db->or_where('settingsid','5');
		
		$query = $this->db->get('ah_settings');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get al users Attendence By Current Day
	 * @param $year integer
	 * Return OBJECT
	 */
		
	function get_userattendence_by_year($year,$userid)
	{
		$query = "SELECT 
		`attendence_date`
		,ah_attendence.`userid`
		,`ah_userprofile`.`fullname`
		, `ah_attendence`.`userid`
		,(SELECT COUNT(attendence_status) FROM `ah_attendence` WHERE attendence_status='P' AND userid=`ah_userprofile`.`userid`) AS present
		, (SELECT COUNT(reason) FROM `ah_attendence` WHERE reason='1' AND userid=`ah_userprofile`.`userid`) AS reason
		, (SELECT COUNT(reason) FROM `ah_attendence` WHERE reason='0' AND userid=`ah_userprofile`.`userid`) AS without_reason
		, (SELECT COUNT(absent_type) FROM `ah_attendence` WHERE absent_type='مهمة عمل' AND userid=`ah_userprofile`.`userid`) AS job_assignment
		, (SELECT COUNT(absent_type) FROM `ah_attendence` WHERE absent_type='بدون عزر' AND userid=`ah_userprofile`.`userid`) AS no_excuse
		, (SELECT COUNT(absent_type) FROM `ah_attendence` WHERE absent_type='أخرى بعزر' AND userid=`ah_userprofile`.`userid`) AS excuse
		, (SELECT COUNT(absent_type) FROM `ah_attendence` WHERE absent_type='الاجازة المرضية' AND userid=`ah_userprofile`.`userid`) AS sick
		, (SELECT COUNT(absent_type) FROM `ah_attendence` WHERE absent_type='الاجازة الاضطراية' AND userid=`ah_userprofile`.`userid`) AS leave_aladtaraah
		, (SELECT COUNT(absent_type) FROM `ah_attendence` WHERE absent_type='الاجازة الاعتيادية' AND userid=`ah_userprofile`.`userid`) AS regular_holiday
		, (SELECT COUNT(late) FROM `ah_attendence` WHERE late='1' AND userid=`ah_userprofile`.`userid`) AS late
		FROM
		  `ah_attendence`
		INNER JOIN `ah_userprofile` 
				ON (`ah_attendence`.`userid` = `ah_userprofile`.`userid`)
		WHERE YEAR(`attendence_date`) = '".$year."' ";
		if($userid!='' && $userid!=0)
		{	$query .= " AND  ah_attendence.`userid`='".$userid."'";	}
		$query .= " GROUP BY ah_attendence.`userid`;";
		//echo $query;
		$myQuery = $this->db->query($query);
		if($myQuery->num_rows() > 0)
		{
			return $myQuery->result();
			
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get al users Attendence By Current Day
	 * @param $year integer
	 * Return OBJECT
	 */
		
	function get_userattendence_by_month($month,$userid)
	{
		$query = "SELECT 
		`attendence_date`
		,ah_attendence.`userid`
		,`ah_userprofile`.`fullname`
		, `ah_attendence`.`userid`
		,(SELECT COUNT(attendence_status) FROM `ah_attendence` WHERE attendence_status='P' AND userid=`ah_userprofile`.`userid`) AS present
		, (SELECT COUNT(reason) FROM `ah_attendence` WHERE reason='1' AND userid=`ah_userprofile`.`userid`) AS reason
		, (SELECT COUNT(reason) FROM `ah_attendence` WHERE reason='0' AND userid=`ah_userprofile`.`userid`) AS without_reason
		, (SELECT COUNT(absent_type) FROM `ah_attendence` WHERE absent_type='مهمة عمل' AND userid=`ah_userprofile`.`userid`) AS job_assignment
		, (SELECT COUNT(absent_type) FROM `ah_attendence` WHERE absent_type='بدون عزر' AND userid=`ah_userprofile`.`userid`) AS no_excuse
		, (SELECT COUNT(absent_type) FROM `ah_attendence` WHERE absent_type='أخرى بعزر' AND userid=`ah_userprofile`.`userid`) AS excuse
		, (SELECT COUNT(absent_type) FROM `ah_attendence` WHERE absent_type='الاجازة المرضية' AND userid=`ah_userprofile`.`userid`) AS sick
		, (SELECT COUNT(absent_type) FROM `ah_attendence` WHERE absent_type='الاجازة الاضطراية' AND userid=`ah_userprofile`.`userid`) AS leave_aladtaraah
		, (SELECT COUNT(absent_type) FROM `ah_attendence` WHERE absent_type='الاجازة الاعتيادية' AND userid=`ah_userprofile`.`userid`) AS regular_holiday
		, (SELECT COUNT(late) FROM `ah_attendence` WHERE late='1' AND userid=`ah_userprofile`.`userid`) AS late
		FROM
		`ah_attendence`
		INNER JOIN `ah_userprofile` 
		ON (`ah_attendence`.`userid` = `ah_userprofile`.`userid`)
		WHERE MONTH(`attendence_date`) = '".$month."' ";
		if($userid!='' && $userid!=0)
		{
		$query .= " AND ah_attendence.`userid`='".$userid."' ";
		}
		$query .= " GROUP BY ah_attendence.`userid`;";
		$myQuery = $this->db->query($query);
		if($myQuery->num_rows() > 0)
		{
			return $myQuery->result(); 
		}
	}

//-------------------------------------------------------------------

   /*
	*
	*/
	public function print_icon($moduleid,$dataid)
	{
		$this->db->select('module_icon,module_name,module_controller,module_type');
		$this->db->where('moduleid',$moduleid);
		$query = $this->db->get('mh_modules');		
		
		if($query->num_rows() > 0)
		{	
			$res = $query->row();
			if($res->module_type=='popup')
			{
				return '&nbsp;<a href="#globalDiag"  data-icon="'.$res->module_icon.'" data-heading="'.$res->module_name.'" data-color="#4096EE" onclick="alatadad(this);" data-url="'.base_url().$res->module_controller.'/'.$dataid.'"><i class="'.$res->module_icon.'"></i></a>';
			}
			else
			{
				return '&nbsp;<a href="'.base_url().$res->module_controller.'/'.$dataid.'"><i class="'.$res->module_icon.'"></i></a>';
			}
		}
		
	}
//-------------------------------------------------------------------

   /*
	*
	*/	
	public function ah_company($companyid=0)
	{
		$this->db->select('companyid,english_name,arabic_name,cr_number');
		
		if($companyid	!=	'' && $companyid	!=	0)
		{
			$this->db->where('companyid',$companyid);
		}
		
		$this->db->where('delete_record','0');
		$query = $this->db->get('ah_company');
		
		if($companyid!='' && $companyid!=0)
		{
			return $query->row();
		}
		else
		{
			return $query->result();
		}
	}
	
//-------------------------------------------------------------------

	/*
	 *  Get ALL Types
	 *  return ARRAY
	 */
	function get_enum_list($table, $column)
	{
		$query	=	$this->db->query("SHOW COLUMNS FROM ".$table." LIKE '".$column."'");
		$result	=	$query->row();
				
		if(!empty($result))
		{
			 return $option_array = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $result->Type));
		}
	}
//-------------------------------------------------------------------

   /*
	*
	*/	
	function enum_dropbox($name,$value,$table,$column,$heading)
	{
		$enum_list	=	$this->get_enum_list($table,$column);
		$dropdown 	=	'<select  name="'.$name.'" id="'.$name.'" class="form-control req" placeholder="'.$heading.'" >';
		
		foreach($enum_list as $enum_key => $enum_value)
		{
			$dropdown .= '<option value="'.$enum_value.'" ';
			
			if($value	==	$enum_value)
			{
				$dropdown .= 'selected="selected"';
			}
			
			$dropdown .= '>'.$enum_value.'</option>';
		}
		
		$dropdown .= '</select>';
		echo($dropdown);
	}
//-------------------------------------------------------------------

	/*
	 *  Get ALL Types
	 *  return ARRAY
	 */	
	function get_items_by_cat_subcat($list_category,$list_subcategory)
	{
		$this->db->select('itemid,itemname');
		$this->db->from('ah_inventory');
		//$this->db->where('delete_record','0');	
		$this->db->where('list_category',$list_category);
		$this->db->where('list_subcategory',$list_subcategory);
		
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			$dropdown  = '<label>Select Item:&nbsp;';
			$dropdown .= '<select name="itemid" id="itemid" placeholder="العناصر">';
			$dropdown .= '<option value="">واختيار عناصر</option>';
				
			foreach($query->result() as $row)
			{
				$dropdown .= '<option value="'.$row->itemid.'" ';
				$dropdown .= '>'.$row->itemname.'</option>';
			}
			
			$dropdown .= '</select>';
			$dropdown .= '</label>';
			
			return $dropdown;	
		}
		else
		{
			return 'No Item is available;';
		}
	}
	
	
	public function get_list_from_ah_listmanagement($type)
	{
		$this->db->select("list_id,list_name");
		$this->db->from("ah_listmanagement");
		$this->db->where("list_type",$type);
		$this->db->where("list_status","1");
		$this->db->where("delete_record","0");
		$listQuery = $this->db->get();

		return $listQuery->result();
		/*$parent_ids	=	$this->check_parents_childs($type);
		
		$this->db->select("list_id,list_name");
		$this->db->from("ah_listmanagement");
		$this->db->where_in("list_parent_id",$parent_ids->ids);
		$listQuery = $this->db->get();
		
		echo $this->db->last_query();
		exit();*/

		//return $listQuery->result();
	}
	
	public function check_parents_childs($type)
	{
		$query	=	$this->db->query("SELECT GROUP_CONCAT(`list_id`) AS ids FROM `ah_listmanagement` WHERE `list_type`='".$type."' AND list_status='1' AND delete_record='0'");
		return $query->row()->ids;
	}
	
	public function admin_user_connt($roleid)
	{
		$this->db->select("userid");
		$this->db->from("ah_users");
		$this->db->where("userroleid",$roleid);
		$this->db->where("userstatus","1");
		$this->db->where("delete_record","0");
		$listQuery = $this->db->get();
		return $listQuery->num_rows();
	}
	
	public function ModuleName($moduleid)
	{
		$this->db->select("module_name");
		$this->db->from("mh_modules");
		$this->db->where("moduleid",$moduleid);
		$module = $this->db->get();
		$row = $module->row();
		return $row->module_name;
	}
	
	public function MakeInputType($type,$name,$required,$value,$key)
	{
		$placeholder	=	$name;
		$name 			=	$key;
			
		if($value!='' && $value!=0) { $value = $value; }
		if($required==1) { $required='req'; } else { $required='';	}
		switch($type)
		{
			case 'text';
				echo '<input type="text" placeholder="'.$placeholder.'" name="'.$name.'" class="form-control '.$required.'" value="'.$value.'"> ';
			break;
			case 'select';
				$lists	=	 $this->create_dropbox_list($name,$value,$selectedvalue='0',$parent='0',$required,$othervalue='',$index='',$dataassign='');
				//$lists	.=	 $this->create_dropbox_list($name,$value,$selectedvalue='0',$parent='0',$required,$othervalue='',$index='',$dataassign='');
				echo $lists;
			break;
			case 'textarea';
				//echo '<textarea placeholder="'.$name.'" name="'.$name.'" id="'.$name.'" class="form-control '.$required.'"></textarea>';
				echo '<textarea placeholder="'.$name.'" name="'.$name.'" id="'.$name.'" class="form-control '.$required.'">'.$value.'</textarea>';
			break;
			case 'number';
				echo '<input type="number" placeholder="'.$name.'" name="'.$name.'" class="form-control '.$required.'" value="'.$value.'"> ';
			break;
			case 'tel';
				echo '<input type="tel" placeholder="'.$name.'" name="'.$name.'" class="form-control '.$required.'" value="'.$value.'"> ';
			break;
			case 'date';
				echo '<input type="date" placeholder="'.$name.'" name="'.$name.'" class="form-control '.$required.'" value="'.$value.'"> ';
			break;
			case 'checkbox';
				$checkbox = $this->get_list_from_ah_listmanagement($value);
				$html = '<div class="col-md=12 custom_Illuminati_box">
				<input type="hidden" id="illuminati_checkbox" value="0" placeholder="'.$name.'" class="'.$required.'">
				';
				foreach($checkbox as $cb)
				{
					$html .= '<div class="col-md-12 custom_Illuminati_inner_peace"><input type="checkbox" class="imcc" onClick="illuminati_input_count(1);" placeholder="'.$name.'" name="'.$name.'[]" id="'.$name.'" value="'.$cb->list_id.'"> <label class="text-warning"> '.$cb->list_name.'</label></div>';
				}
					$html .= '</div>';
					echo $html;
			break;
			case 'radio';
				$checkbox = $this->get_list_from_ah_listmanagement($value);
				$html = '<div class="col-md=12 custom_Illuminati_box"><input type="hidden" placeholder="'.$name.'" id="illuminati_radio" value="0" class="'.$required.'">';
				foreach($checkbox as $cb)
				{
					$html .= '<div class="col-md-12 custom_Illuminati_inner_peace"><input type="radio"  class="imrc"  onClick="illuminati_input_count(2);" placeholder="'.$name.'" name="'.$name.'" id="'.$name.'" value="'.$cb->list_id.'"> <label  class="text-warning">'.$cb->list_name.'</label></div>';
				}
					$html .= '</div>';
					echo $html;
			break;
		}
	}
	
	public function illuminati($ill_id)
	{
		$this->db->select('*');
		$this->db->from('mh_modules');
		$this->db->where('moduleid',$ill_id);		
		$query = $this->db->get();
		return $query->row();
	}
	
	function illuminati_upload_file($user_id,$feildname,$folder,$thumb=FALSE,$width=NULL,$height=NULL)
	{
		if($user_id)
		{
			$path = './'.$folder.'/'.$user_id.'/';	
		}
		else
		{
			$path = './'.$folder.'/';	
		}
				
		if (!is_dir($path))
		{
			mkdir($path, 0777, true);
		}

		$config['upload_path'] 		=	$path;
		$config['allowed_types'] 	=	'*';
		$config['max_size']			=	'5000';
		$config['encrypt_name'] 	=	TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload($feildname))
		{
			$error = array('error' => $this->upload->display_errors());
			return $error	=	'';
		}
		else
		{
			$image_data = $this->upload->data();			
			return $image_data['file_name'];
		}
	}
//-------------------------------------------------------------------
	/*
	* Get All Dynamic/Custome Forms
	* @param $id integer
	* return OBJECT
	*/
	function get_all_custom_form($id=0)
	{
		$this->db->select("*");
		$this->db->from("mh_modules");
		$this->db->where("custom_form != ","");
		if($id==0)
		{
			$query = $this->db->get();
			return $query->result();
		}
		else
		{
			$this->db->where("moduleid", $id);
			$query = $this->db->get();
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	* Get Custom Form Data
	* @param $moduleid integer
	* @param $userid integer
	* return OBJECT
	*/

	function getCustomeFormValue($moduleid,$userid)
    {
		$this->db->select('mmdid,moduleid,userid,moduledata,custom_file_name,savingdate');
		$this->db->where('userid',$userid);
		$this->db->where('moduleid',$moduleid);
		
		$query = $this->db->get('mh_module_data');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------

	/*
	* Get List Name From List Management
	* @param $array ARRAY
	* return OBJECT
	*/

	function query_in($array)
    {
		$this->db->select('list_name');
		$this->db->where_in('list_id',$array);
		
		$query = $this->db->get('ah_listmanagement');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
    }
//-------------------------------------------------------------------	
	/*
	* 
	*
	*/
	function get_helps_by_appid($type,$id_number)
	{
		$gCount = $this->get_all_count_table('ah_applicant',array('isdelete'=>'0'));			

		$html .= '<div class="col-md-12" style="padding:6px 0px; border-bottom: 1px solid #ddd;">';
		
		foreach($this->get_dropbox_list_value('charity_type') as $ctype)
		{
			if($ctype->list_id==78 || $ctype->list_id==79 || $ctype->list_id==80 || $ctype->list_id==81)
			{
				$module 		= $this->get_module_name_icon(0,$ctype->list_id);
				$charityCount 	= $this->get_all_count_table('ah_applicant',array('isdelete'=>'0','charity_type_id'=>$ctype->list_id,'idcard_number'=>$id_number));
				$html 		   .= '<li id="charity_'.$ctype->list_id.'" class="haya_chair_list '.$module->module_icon.' "><a style="margin-right: 5px; font-size:12px;" href="#addingDiag" onClick="alatadad(this);" data-count="'.$charityCount.'" data-url="'.base_url().'inquiries/all_helps/'.$ctype->list_id.'/'.$id_number.'">'.$module->module_name.'<span class="badge '.badge_color($charityCount).'" style="margin-right: 3px;">'.$charityCount.'</span> </a></li>';
			}
		}
		
		$html .= '</div>';
		return $html;
	}
//-------------------------------------------------------------------

	/*
	* Get List Name From List Management
	* @param $array ARRAY
	* return OBJECT
	*/
	function get_total_kafeel_by_orphan($orphan_id)
	{
		$this->db->select('orphan_id');
		$this->db->where('orphan_id',$orphan_id);
		
		$query	=	$this->db->get('assigned_orphan');
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return	'0';
		}
		
	}
//-------------------------------------------------------------------

	/*
	* Get List Name From List Management
	* @param $array ARRAY
	* return OBJECT
	*/
	function get_total_yateem_by_sponser($sponser_id)
	{
		$this->db->select('sponser_id');
		$this->db->where('sponser_id',$sponser_id);
		
		$query	=	$this->db->get('assigned_orphan');
		
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return	'0';
		}
		
	}
//-------------------------------------------------------------------

	/*
	* Delete Dynamic Data Form Data
	* @param $mmdid int
	* return TRUE
	*/
	function delete_dynamic_form_data($mmdid)
	{
		$this->db->where('mmdid',$mmdid);
		$this->db->delete('mh_module_data');
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	* Manager Response Exist OR NOT
	* @param $data ARRAY
	* return TRUE
	*/

	function get_total_leaves($level_id)
    {
		$this->db->select('level_holiday');
		$this->db->where('level_id',$level_id);
		
		$query = $this->db->get('ah_levels');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->level_holiday;
		}
    }
	//-------------------------------------------------------------------
	/*
	* Manager Response Exist OR NOT
	* @param $data ARRAY
	* return TRUE
	*/

	function get_total_holidays($userid)
    {
		$this->db->select('total_holidays');
		if($userid!='')
		{
		$this->db->where('userid',$userid);
		}
		
		$query = $this->db->get('ah_users');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->total_holidays;
		}
    }
//-------------------------------------------------------------------
	/*
	* Manager Response Exist OR NOT
	* @param $data ARRAY
	* return TRUE
	*/

	function get_level_by_user($userid)
    {
		$this->db->select('level_id');
		if($userid!='')
		{	$this->db->where('userid',$userid);	}
		
		$query = $this->db->get('ah_users');
		
		if($query->num_rows() > 0)
		{
			return $query->row()->level_id;
		}
    }
//-------------------------------------------------------------------

	/*
	* 
	*/	
	public function deduct_holiday($userid,$holidays)
	{
		$json_data	=	json_encode(array('data'=>$data));
		
		$this->db->where('userid', $userid);
		$this->db->set('total_holidays', 'total_holidays-'.$holidays, FALSE);

		$this->db->update('ah_users',$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
	}
//-------------------------------------------------------------------
	/*
	 * Get Documents Detail BY Docid
	 * Return OBJECT
	 */
		
	function get_total_iztrayea_leaves($userid)
	{
		$date	=	date('Y');
		
		$this->db->select('COUNT(attendence_id) AS total');
		$this->db->where('userid',$userid);
		$this->db->where('absent_type','الاجازة الاضطراية');
		$this->db->where('YEAR(attendence_date)',$date);
		
		$query = $this->db->get($this->_table_attendence);
		
		if($query->num_rows() > 0)
		{
			return $query->row()->total;
		}
	}
//--------------------------------------------------------------------

	/*
	*
	*
	*
	*/
	public function get_yateem_counts()
	{
		$query	=	$this->db->query("SELECT * FROM `ah_yateem_info` WHERE delete_record = '0' AND step='1';");

		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}
//-------------------------------------------------------------------

	/*
	*  
	*/
	function get_name_of_kafeel($orphan_id)
	{	
		$this->db->select('sponser_id');
		$this->db->where('orphan_id',$orphan_id);
		$this->db->where('delete_record','0');
		$this->db->where('is_old','0');
		$this->db->from('assigned_orphan');	 			
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$sponser_id	=	$query->row()->sponser_id;
			
			$this->db->select('sponser_name');
			$this->db->where('sponser_id',$sponser_id);
			$this->db->where('delete_record','0');
			$this->db->from('ah_sponser_info');
			
			$kafeel_name = $this->db->get();
			
			if($kafeel_name->num_rows() > 0)
			{
				return $kafeel_name->row()->sponser_name;
			}
		}
	}
/*-------------------------------------------------------------------------*/
/* End of file haya_model.php */
/* Location: ./appliction/modules/haya/haya_model.php */
}
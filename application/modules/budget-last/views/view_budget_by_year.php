<div class="row col-md-12">
    <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
        <tbody>
            <tr>
                <td colspan="3" class=" btn-success text-right">الميزانية</td>
            </tr>
            <tr>
                <td class="right"><label class="text-warning">العام :</label><br><strong><?php echo $row->bd_year; ?></strong></td>
                <td class="right"><label class="text-warning">المبلغ الكلي:</label><br><strong><?php echo 'OMR ' . number_format($row->bd_totalamount, 3, '.', ','); ?></strong></td>
                <td class="right"><label class="text-warning">المبلغ المتبقي:</label><br><strong><?php echo 'OMR ' . number_format($row->bd_remainingAmount, 3, '.', ','); ?></strong></td>
            </tr> 


            <?php
            $sub = $this->budget->get_all_items($row->bd_id, '0');
            if (count($sub) > 0) {
                foreach ($sub as $rw) {
                    ?>
                <table class="table" cellpadding="0" cellspacing="0" style="border:1px solid #CCC;">
                    <tbody>
                        <tr>
                            <td colspan="3" class="btn-success text-right">التفاصيل:</td>
                        </tr>
                        <tr>
                            <td class="right"><label class="text-warning">العنوان </label><br><strong><?php echo $rw->bds_title; ?></strong></td>
                            <td class="right"><label class="text-warning">المبلغ الكلي </label><br><strong><?php echo 'OMR' . number_format($rw->bds_totalAmount, 3, '.', ','); ?></strong></td>
                            <td class="right"><label class="text-warning">اسم البنك </label><br><strong><?php echo $rw->bds_bankname_id; ?></strong></td>
                    <!--         <td class="right"><label class="text-warning">الفرع </label><br><strong><?php echo $rw->bds_branch_id; ?></strong></td>
                            <td class="right"><label class="text-warning">رقم حساب  </label><br><strong><?php echo $rw->bds_account_no; ?></strong></td>
                            <td class="right"><label class="text-warning">رقم الهاتف  </label><br><strong><?php echo $rw->bds_phone_no; ?></strong></td>
                            <td class="right"><label class="text-warning">رقم الفاكس  </label><br><strong><?php echo $rw->bds_fax_no; ?></strong></td>
                            <td class="right"><label class="text-warning">البريد الإلكتروني  </label><br><strong><?php echo $rw->bds_email_address; ?></strong></td>-->
                        </tr>
                        <?php
                        $sub1 = $this->budget->get_all_items($rw->bd_id, $rw->bds_id);
                        do {
                            if (count($sub1) > 0) {
                                foreach ($sub1 as $rw1) {
                                    ?>
                                    <tr>
                                        <td class="right"><label class="text-warning">العنوان </label><br><strong><?php echo $rw1->bds_title; ?></strong></td>
                                        <td class="right"><label class="text-warning">المبلغ الكلي </label><br><strong><?php echo 'OMR' . number_format($rw1->bds_totalAmount, 3, '.', ','); ?></strong></td>
                                        <td class="right"><label class="text-warning">اسم البنك </label><br><strong><?php echo $rw1->bds_bankname_id; ?></strong></td>
                                <!--         <td class="right"><label class="text-warning">الفرع </label><br><strong><?php echo $rw1->bds_branch_id; ?></strong></td>
                                        <td class="right"><label class="text-warning">رقم حساب  </label><br><strong><?php echo $rw1->bds_account_no; ?></strong></td>
                                        <td class="right"><label class="text-warning">رقم الهاتف  </label><br><strong><?php echo $rw1->bds_phone_no; ?></strong></td>
                                        <td class="right"><label class="text-warning">رقم الفاكس  </label><br><strong><?php echo $rw1->bds_fax_no; ?></strong></td>
                                        <td class="right"><label class="text-warning">البريد الإلكتروني  </label><br><strong><?php echo $rw1->bds_email_address; ?></strong></td>-->
                                    </tr>
                                <?php
                                }
                            }

                            $sub1 = $this->budget->get_all_items($rw1->bd_id, $rw1->bds_id);
                        } while (count($sub) <= 0);
                        ?>
                    </tbody></table>




            <?php }
        }
        ?>
        </tbody>
    </table> 
</div>

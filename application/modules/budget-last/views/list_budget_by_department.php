<?php $permissions = $this->haya_model->check_other_permission(array($module['moduleid'])); ?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<body class="dashboard-page">
    <?php $this->load->view('common/bodyscript'); ?>
    <?php $this->load->view('common/menu'); ?>
    <section class="wrapper scrollable">
        <?php $this->load->view('common/logo'); ?>
        <?php $this->load->view('common/usermenu'); ?>
        <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
        <?php $this->load->view('common/quicklunchbar'); ?>
        <div class="row">

            <div class="col-md-12">

                <div class="panel panel-default panel-block panel-title-block">
                    <div class="panel-heading">                  
                        <h4 class="tex-right" style=""> 
                            <?php
                            $lastValue = end(array_filter($breadcrumb));
                            foreach (array_filter($breadcrumb) as $index => $value) {
                                echo "<a href='" . base_url() . "budget/list_budget_by_department/$index'>" . $value . "</a>";
                                if ($lastValue !== $value) {
                                    echo " - ";
                                }
                            }
                            //echo implode(' - ', array_filter($breadcrumb)) 
                            ?>
                        </h4>                                          
                    </div>
                </div>
                <?php $msg = $this->session->flashdata('msg'); ?>
                <?php if ($msg): ?>
                    <div class="col-md-12">
                        <div style="padding: 22px 20px !important; background:#c1dfc9;">
                            <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg; ?></h4>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-md-12">
                    <div class="panel panel-default panel-block">
                        <div class="tab-content panel panel-default panel-block">
                            <div class="tab-pane list-group active" id="tabsdemo-1">
                                <div class="list-group-item">
                                    <div style="text-align:center;" id="tableSortable_wrapper" class="dataTables_wrapper form-inline" role="grid">
                                        <div class="row table-header-row">
                                            <a class="btn btn-success" style="float:right;" href="#globalDiag" onclick="alatadad(this);" data-url="<?php echo base_url(); ?>budget/add_budget_by_department/<?php echo $bd_id; ?>/<?php echo $bds_categoryId; ?>/<?php echo $rows["bds_id"]; ?>" id="0" data-icon="images/menu/team_icon.png" data-heading="إضافة">إضافة</a> 
                                            <!--<a class="btn btn-success" style="float:right;" href="<?php echo base_url(); ?>budget/banks/"  data-icon="images/menu/team_icon.png" data-heading="البنوك">البنوك</a> -->
                                        </div>
                                        <table class="table table-bordered table-striped dataTable" id="tableSortable" aria-describedby="tableSortable_info">
                                            <thead>
                                                <tr role="row">
                                                    <th>الاسم</th>
                                                    <th>المبلغ الكلي</th>
                                                    <th>المبلغ المتبقي</th>
                                                    <th>الرصيد الحالي</th>
                                                    <th>الإجراءات</th>
                                                </tr>
                                            </thead>
                                            <tbody role="alert" aria-live="polite" aria-relevant="all">                     
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php $this->load->view('common/footer', array('ajax_url' => base_url() . 'budget/ajax_all_budget_by_department/' . $bd_id . '/' . $bds_categoryId, 'columns_array' => '
{ "data": "Title" },
{ "data": "Total Amount" },{ "data": "Remaining Amount" },{ "data": "الرصيد الحالي" },
				{ "data": "الإجراءات" }')); ?>
</div>
</body>
</html>



</script>

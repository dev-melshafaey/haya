<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendence_model extends CI_Model
{
	/*
	*  Properties
	*/
	private $_table_users;
	private $_table_branchs;
	private $_table_listmanagement;
	private $_table_userprofile;
	private $_table_users_bankaccount;
	private	$_table_users_salary;
	private $_table_users_salary_detail;
	private $_table_users_communications;
	private $_table_users_family;
	private $_table_users_documents;
	private $_table_experiences;
	private $_table_attendence;
	
//-------------------------------------------------------------------

	/*
	 *  Constructor
	 */
	function __construct()
	{
		parent::__construct();

		// Get Table Names from Config 
		$this->_table_users 				= 	$this->config->item('table_users');
		$this->_table_branchs 				= 	$this->config->item('table_branchs');
		$this->_table_listmanagement 		= 	$this->config->item('table_listmanagement');
		$this->_table_userprofile 			= 	$this->config->item('table_userprofile');
		$this->_table_attendence			=	$this->config->item('table_attendence');
	}
//-------------------------------------------------------------------
	/*
	 * Get All Documents Detail BY userid
	 * Return OBJECT
	 */
		
	function get_all_experiences($userid)
	{
		$this->db->select('exe_id,userid,exe_title,country,start_date,end_date,detail,delete_record,add_date');
		$this->db->where('userid',$userid);
		$this->db->where('delete_record','0');
		
		$query = $this->db->get($this->_table_experiences);
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Documents Detail BY Docid
	 * Return OBJECT
	 */
		
	function get_experiences_detail($exeid)
	{
		$this->db->select('exe_id,userid,exe_title,country,start_date,end_date,detail,delete_record,add_date');
		$this->db->where('exe_id',$exeid);
		$query = $this->db->get($this->_table_experiences);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
//-------------------------------------------------------------------
	/*
	* Add Document Detail
	* @param $data ARRAY
	* return TRUE
	*/

	function add_attendence($data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->insert($this->_table_attendence,$data,$json_data,$this->session->userdata('userid'));
		
		return TRUE;
    }
//----------------------------------------------------------------------

	/*
	* Update Document Detail
	* return OBJECT
	*/

	function update_experience($exeid,$data)
    {
		$json_data	=	json_encode(array('data'	=>	$data));
		
		$this->db->where('exe_id',$exeid);
		$this->db->update($this->_table_experiences,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;
    }
//-------------------------------------------------------------------
	/*
	 * Delete Document
	 * @param $docid integer
	 * Return TRUE
	 */
		
	function delete_exeid($exe_id)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','exe_id'	=>	$exe_id));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('exe_id',$exe_id);

		$this->db->update($this->_table_experiences,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;

	}
//-------------------------------------------------------------------
	/*
	 * Get al users Attendence By Current Day
	 * @param $day integer
	 * Return OBJECT
	 */
		
	function get_attendence_by_day($date)
	{
		$query = $this->db->query("SELECT 
		`ah_userprofile`.`fullname`
		, `ah_attendence`.`userid`
		, `ah_attendence`.`attendence_status`
		, `ah_attendence`.`absent_type`
		, `ah_attendence`.`reason`
		, `ah_attendence`.`late`
		FROM
		`ah_attendence`
		INNER JOIN `ah_userprofile` 
		ON (`ah_attendence`.`userid` = `ah_userprofile`.`userid`)
		WHERE DATE(`attendence_date`) = '".$date."' 
		GROUP BY ah_attendence.`userid`;");
			
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get al users Attendence By Current Month
	 * @param $month integer
	 * Return OBJECT
	 */
		
	function get_attendence_by_month($month)
	{
		$query = $this->db->query("SELECT 
		`attendence_date`
		,`ah_userprofile`.`fullname`
		, `ah_attendence`.`userid`
		, `ah_attendence`.`attendence_status`
		, `ah_attendence`.`absent_type`
		,(SELECT COUNT(attendence_status) FROM `ah_attendence` WHERE attendence_status='A' AND userid=`ah_userprofile`.`userid`) AS absent
		,(SELECT COUNT(attendence_status) FROM `ah_attendence` WHERE attendence_status='P' AND userid=`ah_userprofile`.`userid`) AS present
		, (SELECT COUNT(reason) FROM `ah_attendence` WHERE reason='1' AND userid=`ah_userprofile`.`userid`) AS reason
		, (SELECT COUNT(late) FROM `ah_attendence` WHERE late='1' AND userid=`ah_userprofile`.`userid`) AS late
		FROM
		  `ah_attendence`
		INNER JOIN `ah_userprofile` 
		ON (`ah_attendence`.`userid` = `ah_userprofile`.`userid`)
		WHERE MONTH(`attendence_date`) = '".$month."' 
		GROUP BY ah_attendence.`userid`;");
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get al users Attendence By Current Day
	 * @param $year integer
	 * Return OBJECT
	 */
		
	function get_attendence_by_year($year)
	{
		$query = $this->db->query("SELECT 
		`attendence_date`
		, ah_attendence.`userid`
		, `ah_userprofile`.`fullname`
		, `ah_attendence`.`userid`
		, `ah_attendence`.`attendence_status`
		, `ah_attendence`.`absent_type`
		,(SELECT COUNT(attendence_status) FROM `ah_attendence` WHERE attendence_status='A' AND userid=`ah_userprofile`.`userid`) AS absent
		,(SELECT COUNT(attendence_status) FROM `ah_attendence` WHERE attendence_status='P' AND userid=`ah_userprofile`.`userid`) AS present
		, (SELECT COUNT(reason) FROM `ah_attendence` WHERE reason='1' AND userid=`ah_userprofile`.`userid`) AS reason
		, (SELECT COUNT(late) FROM `ah_attendence` WHERE late='1' AND userid=`ah_userprofile`.`userid`) AS late
		FROM
		`ah_attendence`
		INNER JOIN `ah_userprofile` 
		ON (`ah_attendence`.`userid` = `ah_userprofile`.`userid`)
		WHERE YEAR(`attendence_date`) = '".$year."' 
		GROUP BY ah_attendence.`userid` ;");
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
//-------------------------------------------------------------------
	/*
	 * Get Documents Detail BY Docid
	 * Return OBJECT
	 */
		
	function get_user_attend_detail($attendence_id)
	{
		$this->db->select('*');
		$this->db->where('attendence_id',$attendence_id);
		$query = $this->db->get($this->_table_attendence);
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	
//-------------------------------------------------------------------
	/*
	 * Delete User Attendence
	 * @param $attendence_id integer
	 * Return TRUE
	 */
		
	function delete_attendence($attendence_id)
	{
		$json_data	=	json_encode(array('record'	=>	'delete','attendence_id'	=>	$attendence_id));
		
		$data		=	array('delete_record'=>'1');
		
		$this->db->where('attendence_id',$attendence_id);

		$this->db->update($this->_table_attendence,$json_data,$this->session->userdata('userid'),$data);
		
		return TRUE;

	}
/*-------------------------------------------------------------------------*/		
/* End of file admin_model.php */
/* Location: ./appliction/modules/admin/admin_model.php */
}

?>
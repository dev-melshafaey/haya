<?php $segment		=	$this->uri->segment(1);?>
<?php $text			=	$this->lang->line($segment);?>
<?php $labels		=	$text['users']['form'];?>
<?php $absent		=	$this->config->item('absent_types');?>
<!doctype html>
<?php $this->load->view('common/header', array('module' => $module)); ?>
<style>
.btn-default .caret {
	border-top-color: #000 !important;
}
.btn {
	text-shadow: none !important;
}
.btn-default {
	color: #000 !important;
}
.btn-default {
	background-color: #FFF !important;
	border: 1px solid #029625 !important;
}
.dropdown-menu>.active>a, .dropdown-menu>.active>a:hover, .dropdown-menu>.active>a:focus 
{
	color: #000;
	background-color: #029625 !important;
}
.select-box 
{
	max-height: 100px !important;
	min-height: 28px !important;
}
</style>

<body class="dashboard-page">
<?php $this->load->view('common/bodyscript'); ?>
<?php $this->load->view('common/menu'); ?>
<section class="wrapper scrollable">
  <?php $this->load->view('common/logo'); ?>
  <?php $this->load->view('common/usermenu'); ?>
  <?php $this->load->view('common/titlebar', array('udata' => $user_info)); ?>
  <?php $this->load->view('common/quicklunchbar'); ?>
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('common/panel_block', array('module' => $module)); ?>
      <?php $msg	=	$this->session->flashdata('msg');?>
	<?php if($msg):?>
          <div class="col-md-12">
            <div style="padding: 22px 20px !important; background:#c1dfc9;">
                <h4 class="panel-title" style="font-size:15px; text-align:center;color: #029625!important;"><?php echo $msg;?></h4>
            </div>
          </div>
    <?php endif;?>
      <div class="col-md-12">
        <form action="<?php echo base_url();?>attendence/add_attendence" method="POST" id="attendence_form" name="attendence_form" enctype="multipart/form-data" >
          <input type="hidden" name="userid" id="userid" />
          
          <div class="col-md-6">
            <div class="panel panel-default panel-block">
              <div class="list-group">
                <div class="list-group-item" id="checkboxes-and-radios">
                 <h4>نظام حضوره</h4>
                  <div class="list-group-item" id="checkboxes-and-radios" style="padding: 15px 0px; direction:rtl;">
                   <!-- <h4 class="section-title preface-title text-warning mmxx" >الموظفين <span id="ux_count"></span></h4>-->
                    <div class="form-group" id="user_role" class="mmxx" style="overflow-y: scroll; overflow-x: hidden; height: 25em !important;">
                    <table class="table table-bordered table-striped dataTable" id="basicTable">
                      <thead>
                        <tr role="row">
                          <th class="right">اسم</th>
                          <th class="center"></th>
                        </tr>
                      </thead>
                      <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php foreach($this->users->get_all_users() as $u) { ?>
                        <tr role="row">
                          <td class="right"><strong><?php echo $u->fullname; ?> (<?php echo $u->list_name; ?>)</strong><br>
                            <small><?PHP echo $u->branchname; ?></small></td>
                          <td class="center"><input class="select-user required" name="userid" type="radio" value="<?php echo $u->userid; ?> "></td>
                        </tr>
                        <?PHP } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
          <div class="col-md-6">
            <div class="panel panel-default panel-block">
              <div class="list-group">
                <div class="list-group-item" id="input-fields">
                  <h4 id="reaming-reason-leaves"></h4>
                  <div class="col-md-6 form-group">
                    <label class="text-warning">تاريخ</label>
                    <input type="text" class="datepicker form-control required" name="attendence_date"  id="attendence_date"  placeholder="تاريخ" value=""/>
                  </div>
                  <?php if(!empty($all_users)):?>
                  <!--<div class="form-group col-md-4">
                    <label class="text-warning">جميع المستخدمين</label>
                    <select name="userid" id="userid" class="selectpicker form-control" data-live-search="true" placeholder="جميع المستخدمين" style="display: none;">
                      <option value="">جميع المستخدمين</option>
                      <?php foreach($all_users as $user):?>
                      <option value="<?php echo $user->userid;?>"><?php echo $user->fullname;?></option>
                      <?php endforeach;?>
                    </select>
                  </div>-->
                  <?php endif;?>
                  <div class="form-group  col-md-6">
                    <label class="text-warning">حالة</label>
                    <select name="attendence_status" id="attendence_status" class="form-control required" placeholder="حالة">
                      <option value="">حالة</option>
                      <option value="P" >حاضر</option>
                      <option value="A">غائب</option>
                    </select>
                  </div>
                  <div class="form-group  col-md-6" id="on_time" style="display:none;">
                    <label class="text-warning">في الوقت المحدد</label>
                    <select name="late" id="late" class="form-control">
                      <option value="">اختار</option>
                      <option value="1" >متأخر</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6" id="reason_status" style="display:none;">
                    <label class="text-warning">التفاصيل غائبة</label>
                    <br>
                    <input type="radio"  value="1" name="reason" id="reason_yes" />
                    سبب
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio"  value="0" name="reason" id="reason_no" />
                    بدون سبب </div>
                  <div class="form-group col-md-6" id="reason_types" style="display:none;">
                    <label class="text-warning">أنواع السبب</label>
                    <select name="absent_type" id="absent_type" class="form-control" placeholder="أنواع السبب">
                      <option value="">أنواع السبب</option>
                      <?php foreach($this->haya_model->get_leave_types() as $value):?>
                      	<option value="<?php echo $value;?>"><?php echo $value;?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                  <div class="form-group col-md-6" id="document" style="display:none;">
                    <label class="text-warning">وثيقة</label>
                    <input style="border: 0px !important; color: #d09c0d;" type="file" name="document" title='وثيقة'>
                  </div>
                  <div class="form-group col-md-12">
                    <label class="text-warning">ملاحظات</label>
                    <textarea class="form-control" name="reason_text" id="reason_text" style="margin: 0px; height: 177px; width: 763px;"></textarea>
                  </div>
                  <br clear="all" />
                  <input type="button" id="attendence_data_today" class="btn btn-success btn-lrg" name="attendence_data"  value="حفظ" />
                </div>
              </div>
            </div>
          </div>
          <!-- -------------------------------------------------- -->
          <!-- -------------------------------------------------- -->
          
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('common/footer'); ?>
<!-- /.modal-dialog --> 
<script>
$(function(){
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		/*yearRange: "-80:+0",*/
		yearRange: new Date().getFullYear() + ':' + new Date().getFullYear(),
		dateFormat:'yy-mm-dd',
		});
		
	$('#attendence_status').change(function(){
		if($('#attendence_status').val() == 'A')
		{
		   $("#reason_status").fadeIn();
		   $("#document").fadeIn();
		   
		   $("#on_time").fadeOut();
		}
		else
		{
			$("#reason_status").fadeOut();
			$("#document").fadeOut();
			 
			$("#on_time").fadeIn();
		}

	});
	
	$('.select-user').click(function()
	{
	    var radioValue = $("input[name='userid']:checked").val();
		
        if(radioValue)
		{	
			var request = $.ajax({
			url: config.BASE_URL+'attendence/check_reason',
			type: "POST",
			data: {userid:radioValue},
			dataType: "html",
			beforeSend: function(){	$('#ajax_action').show(); $(this).hide();	},
			success: function(msg)
			{
				$("#reaming-reason-leaves").html(msg+' الاجازة الاضطراية المتبقية');
			}
			});
			
			$("#userid").val(radioValue);
        }
	});

	$('#reason_yes').click(function()
	{
		$("#reason_types").fadeIn();
	});
	$('#reason_no').click(function()
	{
		$("#reason_types").fadeOut();
	});
});
</script>
</div>
</body>
</html>
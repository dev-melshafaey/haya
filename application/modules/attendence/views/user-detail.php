<?php $yearly_vacations	=	$this->haya_model->get_vacations();?>
<?php //$total_leaves		=	($yearly_vacations['0']->settingvalue + $yearly_vacations['1']->settingvalue + $yearly_vacations['2']->settingvalue + $yearly_vacations['3']->settingvalue);?>
<?php //$user_detail		=	$this->haya_model->get_userattendence_by_year(date('Y'),$userid);?>
<?php //$total_leaves		=	$this->haya_model->get_total_leaves($level_id);?>
<?php $total_leaves		=	$total_holidays;?>


<?php
if($month)
{
	$user_detail		=	$this->haya_model->get_userattendence_by_month(date('m'),$userid);
}
else
{
	$user_detail		=	$this->haya_model->get_userattendence_by_year(date('Y'),$userid);
}


?>

<div id="printable">
  <div class="row" style="direction:rtl !important;">
    <div class="col-md-12 setMarginx">
      <div class="list-group-item">
        <h3 class="section-title" align="center">التفریغ النھائی لغیاب و حضور موظفی الھیئتہ العمانیہ للاعمال الخیریتہ لعام <?php echo arabic_date(date('Y'));?></h3>
		<?php if(!isset($month)): ?>
        <?PHP if($userid!='' && $userid!=0) { ?> 
           <div class="form-group" >
              <h4>جميع الاجازات</h4>
              <table class="table table-bordered table-striped dataTable no-footer">
                <thead>
                  <tr>
                    <th class="reporthead">إجازة سنوية</th>
                    <th class="reporthead">إجازة الشهري</th>
                    <th class="reporthead">أجازة مرضية</th>
                    <th class="reporthead">أيام عطل</th>
                    <th class="reporthead">مجموع اجازة</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo $yearly_vacations['0']->settingvalue;?></td>
                    <td><?php echo $yearly_vacations['1']->settingvalue;?></td>
                    <td><?php echo $yearly_vacations['2']->settingvalue;?></td>
                    <td><?php echo $yearly_vacations['3']->settingvalue;?></td>
                    <td><?php echo $total_leaves;?></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <?PHP } ?>
        <?php endif;?>
        <?php if(!empty($user_detail)) { ?> 
        <div class="form-group">
          <table class="table table-bordered table-striped dataTable no-footer">
            <thead>
              <tr>
                <th class="reporthead">الاسم الكامل</th>
                <th class="reporthead">الاجازة الاعتيادية</th>
                <th class="reporthead">الاجازة الاضطراية</th>
                <th class="reporthead">الاجازة المرضية</th>
                <th class="reporthead">أخرى بعزر</th>
                <th class="reporthead">بدون عزر</th>
                <th class="reporthead">بدون سبب</th>
                <th class="reporthead">متأخر</th>
                <th class="reporthead">مهمة عمل</th>
                <th class="reporthead">جميع أجازة جدوى</th>
                <th class="reporthead">مجموع اجازة</th>
                <th class="reporthead">المتبقية</th>
              </tr>
            </thead>
            <tbody>
            
            <?PHP 
			foreach($user_detail as $ukey => $ud) { 
				
			?>
              <tr>
                <td><?php echo $ud->fullname;?></td>
                <td><?php echo $ud->regular_holiday;?></td>
                <td><?php echo $ud->leave_aladtaraah;?></td>
                <td><?php echo $ud->sick;?></td>
                <td><?php echo $ud->excuse;?></td>
                <td><?php echo $ud->no_excuse;?></td>
                <td><?php echo $ud->without_reason;?></td>
                <td><?php echo $ud->late;?></td>
                <td><?php echo $ud->job_assignment;?></td>
                <td><?php echo $avail_leaves	=	($ud->no_excuse + $ud->excuse + $ud->sick + $ud->leave_aladtaraah + $ud->regular_holiday + $ud->job_assignment + $ud->without_reason);?></td>
                <td><?php echo $total_leaves;?></td>
                <td><?php echo($total_leaves - $avail_leaves);?></td>
              </tr>
              <?PHP 
			  
			  } ?>
            </tbody>
          </table>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="row" style="text-align:center !important;">
  <button type="button" id="" onClick="printthepage('printable');" class="btn btn-default"><i class="icon-print"></i> طباعة </button>
</div>
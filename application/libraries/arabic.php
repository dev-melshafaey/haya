<?php
require_once APPPATH."/third_party/Arabic.php";

class arabic extends I18N_Arabic {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('UTC');
    }
}
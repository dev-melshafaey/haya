<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| General Language Items
|--------------------------------------------------------------------------
|
| Define all language items in this file
|
*/ 

$lang['users']		=	array(
							'users'		=> array(
											'form'	=>	array(
															'fullname'			=>	'الأسم بالكامل',
															'fathername'		=>	'أسم الأب',
															'userlogin'			=>	'اسم المستخدم',
															'userpassword'		=>	'كلمة المرور',
															'retype_password'	=>	'إعادة كلمة المرور',
															'userroleid'		=>	'نوع المستخدم في النظام',
															'branchid'			=>	'الفرع',
															'email'				=>	'البريد الإلكتروني',
															'gender'			=>	'الجنس',
															'maritialstatus'	=>	'الحالة الإجتماعية',
															'designation'		=>	'المهنة',
															'dateofbirth'		=>	'تاريخ الميلاد',
															'nationality'		=>	'الجنسية',
															'profilepic'		=>	'صورة الملف الشخصي',
															'resume'			=>	'السيرة الذاتية',
															'numberofdependens'	=>	'عدد أشخاص المعالين',
															'submit'			=>	'حفظ'
															),
											'listing'	=> array(
															'fullname'		=>	'الأسم الكامل',
															'fathername'	=>	'أسم الأب',
															'branchname'	=>	'اسم الفرع',
															'listname'		=>	'قائمة الاسم',
															'actions'	=>	'الإجراءات'
															)
											),
							'branchs'	=> array(
												'form'	=> array(
															'branchname'	=>	'اسم الفرع',
															'provinceid'	=>	'اختر المحافظة',
															'wiliayaid'		=>	'اختر الولاية',
															'branchaddress'	=>	'عنوان الفرع',
															'branchstatus'	=>	'الحالة',
															'submit'		=>	'حفظ'
															),
											'listing'	=>	array(
															'number'	=>	'الرقم',
															'name'		=>	'الإسم',
															'province'	=>	'منطقة',
															'willaya'	=>	'Willaya',
															'actions'	=>	'الإجراءات'
															)
													),
													'orpahan'	=> array(
												'form'	=> array(
															'branchname'	=>	'اسم اليتيم ',
															'provinceid'	=>	'الجنسية',
															'wiliayaid'		=>	'اختر الولاية',
															'branchaddress'	=>	'عنوان الفرع',
															'branchstatus'	=>	'الحالة',
															'submit'		=>	'حفظ'
															),
											'listing'	=>	array(
															'orphan_name'	=>	'اسم اليتيم ',
															'ophan_gender'	=>	'الجنسية',
															'orphan_dob'	=>	'تاريخ الميلاد',
															'country'	=>	'الدولة',
															'city'	=>	'المدينة',
															'actions'	=>	'الإجراءات'
															)
													)
													
													
													
								
											);
											
$lang['orpahan']		=	array(
							'orpahan'	=> array(
												'form'	=> array(
															'branchname'	=>	'اسم اليتيم ',
															'provinceid'	=>	'الجنسية',
															'wiliayaid'		=>	'اختر الولاية',
															'branchaddress'	=>	'عنوان الفرع',
															'branchstatus'	=>	'الحالة',
															'submit'		=>	'حفظ'
															),
											'listing'	=>	array(
															'orphan_name'	=>	'اسم اليتيم ',
															'ophan_gender'	=>	'الجنسية',
															'orphan_dob'	=>	'تاريخ الميلاد',
															'country'	=>	'الدولة',
															'city'	=>	'المدينة',
															'actions'	=>	'الإجراءات'
															)
													)
													
													
													
								
											);											
											
											
																
							
//-------------------------------------------------------------------
/* End of file english.php */
/* Location: ./application/language/english/english.php */
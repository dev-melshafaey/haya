<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| General Language Items
|--------------------------------------------------------------------------
|
| Define all language items in this file
|
*/ 

$lang['users']		=	array(
							'users'		=> array(
											'form'	=>	array(
															'fullname'			=>	'Full Name',
															'fathername'		=>	'Father Name',
															'userlogin'			=>	'User Name',
															'userpassword'		=>	'Password',
															'retype_password'	=>	'Re-Type Password',
															'userroleid'		=>	'User Role',
															'branchid'			=>	'Branch',
															'email'				=>	'Email',
															'gender'			=>	'Gender',
															'maritialstatus'	=>	'Marital Status',
															'designation'		=>	'Designation',
															'dateofbirth'		=>	'Date Of Birth',
															'nationality'		=>	'Nationality',
															'profilepic'		=>	'Profile Picture',
															'resume'			=>	'Resume',
															'numberofdependens'	=>	'Number of depends',
															'submit'			=>	'Submit'
															),
											'listing'	=> array(
															'fullname'			=>	'Full Name',
															'fathername'		=>	'Father Name',
															'branchname'		=>	'Branch Name',
															'listname'			=>	'List Name',
															'actions'			=>	'Actions'
															)
											),
							'branchs'	=> array(
												'form'	=> array(
															'branchname'	=>	'Branch Name',
															'provinceid'	=>	'Select Region',
															'wiliayaid'		=>	'Select Willaya',
															'branchaddress'	=>	'Branch Address',
															'branchstatus'	=>	'Status',
															'submit'			=>	'Submit'
															),
											'listing'	=>	array(
															'number'	=>	'Number',
															'name'		=>	'Name',
															'province'	=>	'Region',
															'willaya'	=>	'Willaya',
															'actions'	=>	'Actions'
															)
													)
												
											);

/* End of file english.php */
/* Location: ./application/language/english/english.php */